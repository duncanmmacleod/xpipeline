#!/bin/bash

# This script cleans up the X-Pipeline directory so that it matches an svnversion. 
# It requires no arguments.

# Emulate the result of "make clean" (which is not defined unless the configure
# script has alreay been run) by deleting all built files and mexecutables. 
# KLUDGE: mexa64 is the mex file extension for CIT; this may not work on other systems!
rm -rf trunk/matlab/build/*
rm -f trunk/matlab/share/fastclusterprop/*.mexa64

# Use svn commands to clean up everything else.
#svn update 
svn revert --depth=infinity .
svn status | awk '{print "rm -rf " $2}'  > ._delete
source ._delete 

