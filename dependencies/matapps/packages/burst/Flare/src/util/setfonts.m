function setfonts(fontSize)
% setfonts Set all text in a figure to specified fontSize.
%
% setfonts() Defaults to fontSize=14
%
% setfonts(fontSize) Use fontSize.

% Peter Kalmus
% peter.kalmus@ligo.org
% $LastChangedDate: 2008-03-05 11:53:14 -0500 (Wed, 05 Mar 2008) $
% $Rev: 4750 $ 

if nargin < 1
    fontSize = 14;
end

graphObjs = findall(gcf, '-property', 'FontWeight');
set(graphObjs, 'FontSize', fontSize);
set(graphObjs, 'FontWeight', 'bold');
