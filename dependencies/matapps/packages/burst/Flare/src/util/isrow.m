% ISROW True if array is a ROW vector.
%	ISROW(V) returns logical true (1) if V is a 1 x n vector
%	where n >= 0, and logical false (0) otherwise.
%

function	tf=isrow(v)
		tf=isvector(v)&&size(v,1)==1;
