function [numCol,colHead] = rdcolumnhead(colFile)

%RDCOLUMNHEAD - get column headings from file of ASCII data
%
%SYNOPSIS
%   [NUMCOL,COLHEAD] = rdcolumnhead(COLFILE)
%
%INPUT
%   COLFILE - data file arranged in columns
%
%OUTPUT
%   NUMCOL - # of columns/fields
%   COLHEAD - string with field names (space-delimited)
%
%AUTHOR
%   Keith Thorne <keith.thorne@ligo.org>
       
%  $Id: rdcolumnhead.m 6645 2010-12-09 16:09:14Z diego.menendez@LIGO.ORG $

% FIND comment character in file
% LOOP over lines in file
%   GET a line from the file
%   IF not a comment/header line
%       JUMP out of loop
%   ENDIF
%   SAVE line as possible header
% ENDLOOP
% GET column names from header
% SET position in file to beginning of previous line
% CLOSE log file
numCol = [];
colHead = [];
error(nargchk(1,1,nargin),'struct');
if(exist(colFile,'file') ~= 2)
    msgId = 'rdcolumnhead:noFile';
    error(msgId,'%s: file %s not found',msgId,colFile);
end
[comStyle,comChar] = getcommentstyle(colFile);
fid = fopen(colFile,'r');
while ~feof(fid)
    oldPos = ftell(fid);
    lineStr =fgetl(fid);
    if (strncmp(lineStr,comChar,1) == false)
        break
    end
    oldLine = lineStr;
end
headLine = oldLine(2:end);
colHead = strread(headLine,'%s');
numCol = numel(colHead);
fmtStr = [];
fclose(fid);
return
