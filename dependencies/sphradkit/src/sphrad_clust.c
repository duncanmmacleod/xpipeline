/*
 * Copyright (C) 2010  Mark Edwards
 *   (based on code written by Kipp Cannon, Copyright (C) 2006)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#include <fftw3.h>

#include "sphrad_clust.h"
#include "sphrad_misc.h"

/* Debugging specific includes*/
#include "memwatch.h"

int clustDebugLevel = 1;   /* GLOBAL VARIABLE: Set debug value to HIGH */

int doublecmp (const void* p1, const void* p2)
{
/*
For use with qsort:
dmpcompare a function to compare the elements in the array
It must return a value <,=,> 0 to indicate if the first value has that relationship to the second.
*/

double x=*(double*) p1;
double y=*(double*) p2;
return x <= y ? (x<y? -1:0) :1;
}

double prcValue(double *array, double prc,int size, int sort)
{
/* C version of the Matlab function of the same name 
 * Basically, it sorts the values in the array, assigns 
 * them percentage values and then interpolates to the correct
 * percentage asked for 
 *  -'sort' specifies whether array has already been sorted
 */
double actual = 0;
double prcDiff = 0;
double pointDiff = 0;

/* Array not yet sorted */
if (sort == 1)
  qsort(array, size, sizeof(double),doublecmp);

/* Determine index position */
actual = prc*size-0.5;

if (actual <= 0) 
  return array[0];

if (actual >= (size-1))
  return array[size-1];

/* Get fractional part of position */
prcDiff   = (actual-floor(actual));

/* Linearly interpolate */
pointDiff = (array[(int)ceil(actual)]-array[(int)floor(actual)]);

return (array[(int)floor(actual)]+pointDiff*prcDiff);
}


/* 
 *  Calculates thresholds for each of the defined regions
 */
double **calc_thresholds(double *tfMap, int nCols, int nRows,int *boundaries, int nRegions, double *clusterThresholds, int directions)
{
/* Simplist first, just have time oriented regions 
 *   - this means that boundaries holds the edges
 *     of each region, so 
 *	boundaries[n]<=timeIdx<boundaries[n+1]
 *	Also including 0 and N (width)
 */

/* Directions tells us how the regions are oriented:
 *	0 - regions use full row height, with boundaries set in column space  
 *	1 - regions use full column width, with boundaries set in row space
 *	2 - we have a patchwork of regions in both directions
 */
int breadth, height;
int breadthIdx, heightIdx;
int pixelCount;
int region, idx;
int regionPixels=0;
double *sortedArray = NULL;

double **thresholds = (double **)malloc(2*sizeof(double *));
if (directions == 2)
{
  thresholds[0]= (double *) malloc(nRegions*nRegions*sizeof(double));
  thresholds[1]= (double *) malloc(nRegions*nRegions*sizeof(double));
}
else
{
  thresholds[0]= (double *) malloc(nRegions*sizeof(double));
  thresholds[1]= (double *) malloc(nRegions*sizeof(double));
}

int nElements = nCols*nRows;
    
mexPrintf( "\tCluster map -> nRows - %d, nCols - %d\n", nRows, nCols);

mexPrintf("Boundaries: ");
for (idx=0; idx<nRegions;idx++)
  mexPrintf("%d-> (%d-%d) | ",idx+1,boundaries[idx], boundaries[idx+1]);
mexPrintf("\n");

/* Set appropriate direction for maximum boundary */
if (directions == 0)
{
  breadth = nCols;
  height = nRows;
}
else if (directions == 1)
{
  breadth = nRows;
  height = nCols;
}

/* Extract the pixels of interest 
 *  - positive frequencies and within time region 
 */
for (region=0; region < nRegions; region++)
  {
    if (boundaries[region] > breadth)
    {
      fprintf(stderr, "ERROR: Region %d->(%d) greater than width %d\n",region, boundaries[region],breadth);
      return NULL;
    }
 
    /* Number of pixels in this region */
    regionPixels = (boundaries[region+1] - boundaries[region])*height;
    /* Allocate memory for this sub-region */
    sortedArray = (double *) calloc(regionPixels,sizeof(double));
    
    if (!sortedArray)
    {
      fprintf(stderr,"\t ERROR: Cannot allocate memory for thresholding\n");
      return NULL;
    }
      
    pixelCount = 0;
    
    /* Walk over this region, assign pixels for threshold calculation, and tfmap */
    for (heightIdx=0;heightIdx < height;heightIdx++)
    {
      if (pixelCount >= regionPixels)
      {
	fprintf(stdout, "ERROR: too many pixels, on %d, should be %d\n", pixelCount,((boundaries[region+1] - boundaries[region]+1)*height) );
      }
      else
      {
      for (breadthIdx=boundaries[region];breadthIdx < boundaries[region+1];breadthIdx++)
	{
	  if (directions == 0)
	    idx = breadthIdx+breadth*heightIdx;
	  else
	    idx = heightIdx+height*breadthIdx;
	  
/* Pixel in this region */
/*	  fprintf(stdout, " %d ",idx);*/

	  if (idx < nElements)
	    sortedArray[pixelCount]=tfMap[idx];
	  else 
	    fprintf(stdout, "ERROR: Outside boundary (%d,%d)->%d", breadthIdx, heightIdx,idx);
	  
      pixelCount++;
	}
      }
    }

    /* Generate the percentage values for this region:
      *	- if we have one threshold, or they are the same, perform nearest neighbour clustering 
      *	- if we have two unique values then we are performing coronal clustering 
      */
    thresholds[0][region] = prcValue(sortedArray, clusterThresholds[0], regionPixels,1);
    thresholds[1][region] = prcValue(sortedArray, clusterThresholds[1], regionPixels,0);
    mexPrintf( "\n\t Region %d (%d pixels) Pcrtile value for %g%% (%g%%) -> %g (%g)\n",region, regionPixels, clusterThresholds[0],clusterThresholds[1] , thresholds[0][region], thresholds[1][region]);
    
    free(sortedArray);
  }
  
mexPrintf("Fini\n");
return thresholds;
}

/* Function to return thresholdIdx for pixel location */
int return_thresholdIdx(int colIdx, int rowIdx, int *boundaries, int nRegions, int directions)
{
int position, i;

/* Set correct direction */
if (directions == 0)
  position = colIdx;
else if (directions == 1)
  position = rowIdx;

for (i = 0; i < (nRegions - 1); i++)
  if ((position >= boundaries[i]) && (position < boundaries[i+1]))
    return i;
}

/* generate simple boundaries for each region */
int *set_boundaries(int nCols, int nRows, int nRegions, int directions)
{
/* Segment map into nRegions, using the directions flag to control 
 * how we segment, it can take 3 values:
 *	0 - segment across columns
 *	1 - segment down the rows
 *	2 - segment across and down, forming a pathwork of threshold regions
 */ 
int breadth;
int *boundaries = NULL;

if (directions == 2)
  boundaries = (int *) malloc ((nRegions+1)*(nRegions+1)*sizeof(int));
else
  boundaries = (int *) malloc ((nRegions+1)*sizeof(int));

if (!boundaries)
{
  fprintf(stderr, "ERRROR:  Unable to allocate memory for cluster boundary\n");
  return NULL;
}

/* Set appropriate direction for maximum boundary */
if (directions == 0)
  breadth = nCols;
else if (directions == 1)
  breadth = nRows;

boundaries[0] = 0;
boundaries[nRegions] = breadth;

int i;
for (i=1;i<(nRegions);i++)
  boundaries[i] = (int)round(breadth*((double)i/nRegions));

return boundaries;
}

/* 
 *  Methods to maintain cluster structures
 */
void *init_boundaryStruct(struct boundaryStruct *clusterEdges)
{
  /* Initialise structure to 'null' values */
  clusterEdges->clusterNumber = -1;
  clusterEdges->numPixels = 0;
  clusterEdges->minX = 100000;
  clusterEdges->maxX = -100000;
  clusterEdges->minY = 100000;
  clusterEdges->maxY = -100000;
  clusterEdges->clusterPower = 0.0;
  
  clusterEdges->maxPixelX = 0.0;
  clusterEdges->maxPixelY = 0.0;
  clusterEdges->maxPixelPower = 0.0;

  clusterEdges->nextCluster = NULL;
}

void *check_boundary(struct boundaryStruct *clusterEdges, int colIdx, int rowIdx, const int colSize, const int rowSize)
{
  if (clustDebugLevel > 1)
  {    
    mexPrintf( "\t  IN: colIdx %d, rowIdx %d\n", colIdx, rowIdx);
    mexPrintf( "\t    X boundary: (%d,%d)\n", clusterEdges->minX,clusterEdges->maxX );
    mexPrintf( "\t    Y boundary: (%d,%d)\n",clusterEdges->minY,clusterEdges->maxY);
  }
  
  /* Determine if new pixel is outside tfmap */
  if (colIdx < 0){
    colIdx = 0; mexPrintf("\t	colIdx too low\n");}
    
  if (rowIdx < 0){
    rowIdx = 0; mexPrintf("\t   rowIdx too low\n");}

  if (colIdx > colSize){
    colIdx = colSize; mexPrintf("\t   colIdx too big\n");}
    
  if (rowIdx > rowSize){
    rowIdx = rowSize; mexPrintf("\t   rowIdx too big\n");}

  /* Compare new pixel's position with stored */
  if (colIdx < clusterEdges->minX)
    clusterEdges->minX = colIdx;    
  if (colIdx > clusterEdges->maxX)
    clusterEdges->maxX = colIdx;
  if (rowIdx < clusterEdges->minY)
    clusterEdges->minY = rowIdx;
  if (rowIdx > clusterEdges->maxY)
    clusterEdges->maxY = rowIdx;  
}

void *print_boundaryStruct(struct boundaryStruct *clusterEdges)
{
  /* Print out structure for debugging */
  mexPrintf( "\t  Cluster %d: \n", clusterEdges->clusterNumber);
  mexPrintf( "\t    %d pixels \n",clusterEdges->numPixels);
  mexPrintf( "\t    X boundary: (%d, %d)\n", clusterEdges->minX, clusterEdges->maxX);
  mexPrintf( "\t    Y boundary: (%d, %d)\n", clusterEdges->minY, clusterEdges->maxY);
  mexPrintf( "\t    Cluster power: %g\n", clusterEdges->clusterPower);
  
  mexPrintf( "\t    Location of maxPixel: (%d, %d)\n", clusterEdges->maxPixelX, clusterEdges->maxPixelY);
  mexPrintf( "\t    Power of max pixel: %g\n", clusterEdges->maxPixelPower);
  
  if (clusterEdges->nextCluster != NULL)
    print_boundaryStruct(clusterEdges->nextCluster);
}

void *free_boundaryStruct(struct boundaryStruct *thisStruct)
{
  if (thisStruct->nextCluster != NULL)
    free_boundaryStruct(thisStruct->nextCluster);

  if (clustDebugLevel > 1)
    mexPrintf( "\t  Freeing cluster %d\n",thisStruct->clusterNumber);
  
  free(thisStruct);
}

/* Given a boundary structure, merge the requested elements */
void *merge_boundary(struct boundaryStruct *ptr[], int clusterOne, int clusterTwo)
{  
  /* When merging clusters, we need to do the following:
   *	- add number of pixels in cluster to be merged into main cluster,
   *	- re-compute boundary based on clusters
   *	- delete the entry for the cluster being merged,
   *	- re-engineer the linked list
   *	 
   */
  
  /* Merge number of cluster pixels */
  if (clustDebugLevel > 1)
    mexPrintf( "\t  Cluster pixels: %d  -> %d\n", ptr[clusterOne]->numPixels,ptr[clusterTwo]->numPixels);

  ptr[clusterOne]->numPixels += ptr[clusterTwo]->numPixels; 
  ptr[clusterTwo]->numPixels = 0;
  
  /* Merge cluster power */
  ptr[clusterOne]->clusterPower += ptr[clusterTwo]->clusterPower; 
  ptr[clusterTwo]->clusterPower = 0;

  
  /* Re-compute boundary */
  if (ptr[clusterOne]->minX > ptr[clusterTwo]->minX)
    ptr[clusterOne]->minX = ptr[clusterTwo]->minX;
  if (ptr[clusterOne]->maxX < ptr[clusterTwo]->maxX)
    ptr[clusterOne]->maxX = ptr[clusterTwo]->maxX;
  if (ptr[clusterOne]->minY > ptr[clusterTwo]->minY)
    ptr[clusterOne]->minY = ptr[clusterTwo]->minY;
  if (ptr[clusterOne]->maxY < ptr[clusterTwo]->maxY)
    ptr[clusterOne]->maxY = ptr[clusterTwo]->maxY;
  
  /* Need to remove clusterTwo from the linked list, so attach
   * the next cluster to clusterTwo-1, and free the memory associated with it 
   * Also need to check whether the cluster to be removed the first or last.
   *  - if so, need to handle carefully
   */
  /*
  if ((clusterTwo) == 0)
    
  ptr[clusterTwo-1]->nextCluster = ptr[clusterTwo+1]->nextCluster;
  
  free(ptr[clusterTwo]);
  ptr[clusterTwo] = 0;
  */
}


int merge_clusters(struct boundaryStruct *clusterEdges ,int *clusterMap, int clustNo, int rowSize, int colSize, int mergeDistance)
{
/* Merge pass - go over clusterMap and assign locale pixels as one */
/*   - combined close clusters  */

int xIdx, yIdx, clusterIdx;
int rowIdx, colIdx;
int newPixel, currentPixel;
struct  boundaryStruct *ptr[clustNo];
int clusterSwap[clustNo+1];

mexPrintf( "\t  Building cluster number array for processing\n");
ptr[0] = clusterEdges;

for (clusterIdx = 1; clusterIdx < clustNo; clusterIdx++)
    ptr[clusterIdx] = ptr[clusterIdx-1]->nextCluster;

/* Loop over all pixels */
for (rowIdx=0; rowIdx < rowSize; rowIdx++)
  for (colIdx=0; colIdx < colSize; colIdx++)
  {
    currentPixel 	= colIdx+rowIdx*colSize;
    
    /* As long as pixel is not background */
    if (clusterMap[currentPixel] > 0)
    {
      /* Compare the cluster number of all pixels within distance */
      for (yIdx = (rowIdx+mergeDistance); yIdx >= (rowIdx-mergeDistance); yIdx--)
	for (xIdx = (colIdx-mergeDistance); xIdx <= (colIdx+mergeDistance); xIdx++)
	{
	  newPixel = xIdx+yIdx*colSize;

	  if ((xIdx >= 0) && (xIdx < colSize) && (yIdx >= 0) && (yIdx < rowSize) && (clusterMap[newPixel] > 0) /*&& (clusterMap[newPixel] != clusterMap[currentPixel])*/)
	  {
	    if (clustDebugLevel > 1)
	      mexPrintf( "\t  New: %d, current: %d\n",clusterMap[newPixel], clusterMap[currentPixel] );
	    
	      if (clusterMap[newPixel] > clusterMap[currentPixel])
	      {
		if (clustDebugLevel > 1)
		  mexPrintf( "\t    Swap new: %d -> %d\n",clusterSwap[clusterMap[newPixel]] ,clusterMap[currentPixel]);

		clusterSwap[clusterMap[newPixel]] = clusterMap[currentPixel];
		clusterMap[newPixel] = clusterMap[currentPixel];
	      }
	      else
	      {
		if (clustDebugLevel > 1)
		  mexPrintf( "\t    Swap current: %d -> %d\n",clusterSwap[clusterMap[currentPixel]] ,clusterMap[newPixel]);		  
		clusterSwap[clusterMap[currentPixel]] = clusterMap[newPixel];
		clusterMap[currentPixel] = clusterMap[newPixel];
	      }
	  }
	}
    }
  }


/* Print out the clusterSwap vector to check it's values before messing with it */
if (clustDebugLevel > 1)
  for (clusterIdx=1;clusterIdx<=clustNo;clusterIdx++)
    mexPrintf( "\t    Swap: %d->%d\n",clusterIdx, clusterSwap[clusterIdx]);

/* Organise the clusterSwap vector so that each cluster
  points to its end cluster, not an intermediate */
int tempIdx=0;
for (clusterIdx=clustNo; clusterIdx>0;clusterIdx--)
{
  if (clusterSwap[clusterIdx] != clusterIdx)
  {
    tempIdx = clusterIdx;

    if (clustDebugLevel > 1)
       mexPrintf( "\t    Swap: %d -> %d\n",clusterSwap[tempIdx] , tempIdx);
    
    while (clusterSwap[tempIdx] != tempIdx)
    {
      tempIdx = clusterSwap[tempIdx];
    }
    clusterSwap[clusterIdx] = tempIdx;
  }
}

for (clusterIdx=1;clusterIdx<=clustNo;clusterIdx++)
{
  if (clustDebugLevel > 1)
    mexPrintf( "\t    Swap: %d->%d\n",clusterIdx, clusterSwap[clusterIdx]);
 
  if (clusterSwap[clusterIdx] != clusterIdx )
    merge_boundary(ptr, clusterSwap[clusterIdx]-1, clusterIdx-1); 
}

/* Loop over all pixels, setting clusters to correct value as we go */
int currentClust = 0;
int prevClust = 0;
for (rowIdx=0; rowIdx < rowSize; rowIdx++)
  for (colIdx=0; colIdx < colSize; colIdx++)
  {
    /* Get index into clusterMap */
    currentPixel = colIdx+rowIdx*colSize;
    
    if ((clusterMap[currentPixel] != 0))
    {
      currentClust = clusterMap[currentPixel];

      /* Compare cluster value against swap value */
      if ((clusterSwap[clusterMap[currentPixel]] != clusterMap[currentPixel]))
	clusterMap[currentPixel] = clusterSwap[clusterMap[currentPixel]];
    }
  }
  if (clustDebugLevel > 1)
	print_boundaryStruct(clusterEdges);
}

int surround_pixels(int xPos, int yPos, int xSize, int ySize)
{
return xPos+yPos;
}


 /*
  *  Returns an array specifying which of the surrounding pixels is non-zero
  *   and not in the same cluster as the current pixel
  */
int checkMap_NEW(double *clusterMap, int colIdx, int rowIdx, int currentPixelIdx, int colSize, int rowSize, int *blackPixels, int direction, int mergeDistance, double threshold)
{
/* Checks all pixels within 'distance' of the currentPixel for a 'black' pixel.
 *  (i.e. a pixel with a value greater than the threshold)
 *  Returns an array of 'directions' that can be looped over to checkMap
 * for further pixels in the chain.
 *
 * IN:
 *	clusterMap		- array containing values to be thresholded and clustered
 *	colIdx, rowIdx		- col and row of selected pixel
 *	currentPixelIdx		- 1d array index for pixel
 */

int tPixels = 0; 	/* set total number to zero */
int pixelCount = 0;

int xIdx, yIdx;
int newPixel;
  
/* Check all pixels within distance to see if they pass our threshold */
for (yIdx = (rowIdx+mergeDistance); yIdx >= (rowIdx-mergeDistance); yIdx--)
  for (xIdx = (colIdx-mergeDistance); xIdx <= (colIdx+mergeDistance); xIdx++)
  {
    if ((xIdx >= 0) && (xIdx < colSize) && (yIdx >= 0) && (yIdx < rowSize))
    {
      newPixel = xIdx+yIdx*colSize;
      
	if ((clusterMap[newPixel] > threshold) && (newPixel != currentPixelIdx))
	{
	  blackPixels[tPixels] = pixelCount;
	  tPixels++;
	}	  
    }
    
    pixelCount++;
  }

  
if (clustDebugLevel > 1)
{    
  mexPrintf( "\t  Total pixels: %d\n", tPixels);
  int i;
  mexPrintf( "\t  Surrounding: ",blackPixels[i]);
  for (i=0;i<tPixels;i++)
      mexPrintf( " %d ",blackPixels[i]);
  mexPrintf("\n");
}

/* Number of black pixels surrounding current pixel */
return tPixels;
}


/*
  * Given a start pixel and a clusterMap - recursively follows black pixels
  *  until end is found (no more black pixels). 
  */
 
int chainMap_NEW(double *tfMap, int *clusterMap,struct boundaryStruct *clusterEdges, const int colIdx, const int rowIdx, int currentPixel,/*int *currentPixel,*/ const int colSize, const int rowSize, int direction, int clustNo, int mergeDistance, double threshold)
{

int arrayBreadth = (mergeDistance*2)+1;
int blackPixels[arrayBreadth*arrayBreadth];
int idx;
int numPixels = 0;
int colShift = 0;
int rowShift = 0;
double clust = 0.0;
int totalPixels = 1;

if (clustDebugLevel > 1)
  mexPrintf( "\t  New chain ");

/* Assign cluster value to our pixel */
clusterMap[currentPixel] = clustNo;

/* Add pixel power into the cluster information */
clusterEdges->clusterPower += tfMap[currentPixel];

/* Remove it from the map  - REMOVING AS IT DOESN'T APPEAR TO BE NEEDED
tfMap[currentPixel] = 0;*/

/* Does this pixel enlarge the cluster boundary? */
check_boundary(clusterEdges, colIdx, rowIdx, colSize, rowSize);

/* Is this pixel the new maximum? */
if (tfMap[currentPixel] > clusterEdges->maxPixelPower)
{
   clusterEdges->maxPixelPower = tfMap[currentPixel];
   clusterEdges->maxPixelX = colIdx;
   clusterEdges->maxPixelY = rowIdx;
   if (clustDebugLevel > 1)
     mexPrintf( "\t    New maximum pixel: (%d, %d)->%g\n", clusterEdges->maxPixelX,clusterEdges->maxPixelY, clusterEdges->maxPixelPower);
}

/* Get black pixels surrounding our pixel */
numPixels = checkMap_NEW(tfMap,colIdx, rowIdx, currentPixel, colSize, rowSize, blackPixels, direction, mergeDistance, threshold);

  if (numPixels > 0)
  {
    /* If we have some black pixels, then loop over them */    
    for (idx=0;idx < numPixels;idx++)
    {
	rowShift = colShift = 0;
	
	rowShift = (int)(blackPixels[idx]/arrayBreadth)-mergeDistance;
	rowShift = -rowShift;
	colShift = (int)(blackPixels[idx]%(arrayBreadth))-mergeDistance;

	if (clustDebugLevel > 1)
	  mexPrintf( "\t    Direction: %d ->shifts: (%d, %d)\n", blackPixels[idx],rowShift, colShift);

	/* If clusterMap already filled for this pixel, do not chain on it */
	if (clusterMap[(colIdx+colShift)+(rowIdx+rowShift)*colSize] > 0)
	  totalPixels;
	else
	  totalPixels +=chainMap_NEW(tfMap, clusterMap, clusterEdges, (const int) colIdx+colShift, (const int)rowIdx+rowShift, (colIdx+colShift)+(rowIdx+rowShift)*colSize/*currentPixel*/ ,(const int) colSize, (const int) rowSize, blackPixels[idx], clustNo, mergeDistance, threshold);
	
      if (clustDebugLevel > 1)
	mexPrintf( "\t    ClustNo: %d, PixelNo: %d, Pos: (%d,%d) -> Idx: %d, Direction: %d\n\t shifts: (%d, %d)\n", clustNo, totalPixels, colIdx+colShift, rowIdx+rowShift, (colIdx+colShift)+(rowIdx+rowShift)*colSize, blackPixels[idx], rowShift, colShift);

      }
  }
/*  tfMap[currentPixel] = 0;*/
if (clustDebugLevel > 1)
  mexPrintf( "\t  End of chain\n");
return totalPixels;
}




 /*
  *  Returns an array specifying which of the surrounding pixels is non-zero
  *   and not in the same cluster as the current pixel
  */
int checkMap(double *tfMap, int colIdx, int rowIdx, int currentPixelIdx,/*int *currentPixel,*/ int colSize, int rowSize, int *blackPixels, int direction, double threshold)
{
/* Checks the 8 pixels surrounding currentPixel for a 'black' pixel.
 *  Returns an array of 'directions' that can be looped over to checkMap
 * for further pixels in the chain.
 *
 * IN:
 * 
 * A direction can also be passed in, and only the pixels in that direction
 * are searched - directions are stated as follows:
 *
 *              1|2|3
 *		4|0|5
 *		6|7|8
 * 
 *   with 0 meaning check all. 
 *
 * However, only return pixel directions that are different from the current
 * to save cycles.
 *	currentPixels[3]	- [0] = col, [1] = row, [2] = array idx
 */

int currentPixel[3];
currentPixel[0]= colIdx;
currentPixel[1]= rowIdx;
currentPixel[2]= currentPixelIdx;

int tPixels = 0; 	/* set total number to zero */
/*
if ((colIdx > colSize) || (colIdx < 0) || (rowIdx < 0))
  return 0;
*/

/* UpLeft,1 - directions including 1 are: 1, 2,3,4 & 6*/
if (((direction == 0) || (direction == 1) || (direction == 2) || (direction == 3) || (direction == 4) || (direction == 6)) /*&& (clusterMap[(currentPixel[0]-1)+(currentPixel[1]+1)*colSize] != clusterMap[currentPixel[2]])*/ && ((currentPixel[0]-1) >= 0 ) && ((currentPixel[1]+1) < rowSize))
{
  if (tfMap[(currentPixel[0]-1)+(currentPixel[1]+1)*colSize] > threshold)
  {
    blackPixels[tPixels] = 1;
    tPixels++;
  }
}

/* Up, 2 - directions including 2 are: 1, 2 & 3*/
if (((direction == 0) || (direction == 1) || (direction == 2) || (direction == 3)) /*&& (clusterMap[(currentPixel[0])+(currentPixel[1]+1)*colSize] != clusterMap[currentPixel[2]])*/ &&  ((currentPixel[1]+1) < rowSize)  )
{
    if (tfMap[currentPixel[0]+(currentPixel[1]+1)*colSize] > threshold)
    {
    blackPixels[tPixels] = 2; 
    tPixels++;
    }
}
  
/* UpRight,3 - directions including 3 are: 1, 2,3,5 & 8*/
  if (((direction == 0) || (direction == 1) || (direction == 2) || (direction == 3) || (direction == 5) || (direction == 8)) /*&& (clusterMap[(currentPixel[0]+1)+(currentPixel[1]+1)*colSize] != clusterMap[currentPixel[2]])*/ && ((currentPixel[0]+1) < colSize) && ((currentPixel[1]+1) < rowSize))
  {
    if (tfMap[(currentPixel[0]+1)+(currentPixel[1]+1)*colSize] > threshold)
    {
    blackPixels[tPixels] = 3; 
    tPixels++;
    }
  }

/* Left,4 - directions including 4 are: 1, 4 & 6*/
if (((direction == 0) || (direction == 1) || (direction == 4) || (direction == 6)) /*&& (clusterMap[(currentPixel[0]-1)+(currentPixel[1])*colSize] != clusterMap[currentPixel[2]])*/ && ((currentPixel[0]-1) >= 0))
{
    if (tfMap[(currentPixel[0]-1)+(currentPixel[1])*colSize] > threshold)
    {
    blackPixels[tPixels] = 4; 
    tPixels++;
    }
}

/*Right,5 - directions including 5 are: 3, 5 & 8*/
if (((direction == 0) || (direction == 3) || (direction == 5) || (direction == 8)) /*&& (clusterMap[(currentPixel[0]+1)+(currentPixel[1])*colSize] != clusterMap[currentPixel[2]])*/ && ((currentPixel[0]+1) < colSize))
{
    if (tfMap[(currentPixel[0]+1)+(currentPixel[1])*colSize] > threshold)
    {
    blackPixels[tPixels] = 5; 
    tPixels++;
    }
}

/*DownLeft, 6 - directions including 6 are: 1, 4, 6, 7 & 8*/
if (((direction == 0) || (direction == 1) || (direction == 4) || (direction == 6) || (direction == 7) || (direction == 8)) /*&& (clusterMap[(currentPixel[0]-1)+(currentPixel[1]-1)*colSize] != clusterMap[currentPixel[2]])*/ && ((currentPixel[0]-1) >= 0) && ((currentPixel[1]-1) >= 0))
{
    if (tfMap[(currentPixel[0]-1)+(currentPixel[1]-1)*colSize] > threshold)
    {
    blackPixels[tPixels] = 6; 
    tPixels++;
    }
}

/*Down, 7 - directions including 7 are: 6, 7 & 8*/
if (((direction == 0) || (direction == 6) || (direction == 7) || (direction == 8)) /*&& (clusterMap[(currentPixel[0])+(currentPixel[1]-1)*colSize] != clusterMap[currentPixel[2]])*/ && ((currentPixel[1]-1) >= 0))
{
    if (tfMap[(currentPixel[0])+(currentPixel[1]-1)*colSize] > threshold)
    {
    blackPixels[tPixels] = 7; 
    tPixels++;
    }
}

/*DownRight,8 - directions including 8 are: 3, 5, 6, 7 & 8*/
if (((direction == 0) || (direction == 3) || (direction == 5) || (direction == 6) || (direction == 7) || (direction == 8)) /*&& (clusterMap[(currentPixel[0]+1)+(currentPixel[1]-1)*colSize] != clusterMap[currentPixel[2]])*/&& ((currentPixel[0]+1) < colSize) && ((currentPixel[1]-1) >= 0))
{
    if (tfMap[(currentPixel[0]+1)+(currentPixel[1]-1)*colSize] > threshold)
    {
    blackPixels[tPixels] = 8; 
    tPixels++;
    }
}
  
if (clustDebugLevel > 1)
{    
  mexPrintf( "\t  direction in: %d ", direction );
  int i;
  for (i=0;i<tPixels;i++)
      mexPrintf( "\t   Surrounding: %d ",blackPixels[i]);
  mexPrintf("\n");
}

/* Number of black pixels surrounding current pixel, modified by direction */
return tPixels;
}

/*
  * Given a start pixel and a clusterMap - recursively follows black pixels
  *  until end is found (no more black pixels). 
  */
 
int chainMap(double *tfMap, int *clusterMap,struct boundaryStruct *clusterEdges, const int colIdx, const int rowIdx, int currentPixel,/*int *currentPixel,*/ const int colSize, const int rowSize, int direction, int clustNo, double threshold)
{

int blackPixels[]={0,0,0,0,0,0,0,0};
int idx;
int numPixels = 0;
int colShift = 0;
int rowShift = 0;
double clust = 0.0;
int totalPixels = 1;

if (clustDebugLevel > 1)
  mexPrintf( "\t  New chain ");

/* Assign cluster value to our pixel */
clusterMap[currentPixel] = clustNo;

/* Add pixel power into the cluster information */
clusterEdges->clusterPower += tfMap[currentPixel];

/* Remove it from the map - REMOVING AS IT DOESN'T APPEAR TO BE NEEDED
tfMap[currentPixel] = 0;*/

/* Is this pixel outside of our boundary? */
check_boundary(clusterEdges, colIdx, rowIdx, colSize, rowSize);

/* Is this pixel the new maximum? */
if (tfMap[currentPixel] > clusterEdges->maxPixelPower)
{
   clusterEdges->maxPixelPower = tfMap[currentPixel];
   clusterEdges->maxPixelX = colIdx;
   clusterEdges->maxPixelY = rowIdx;
   if (clustDebugLevel > 1)
     mexPrintf( "\t    New maximum pixel: (%d, %d)->%g\n", clusterEdges->maxPixelX,clusterEdges->maxPixelY, clusterEdges->maxPixelPower);
}

/* Get black pixels surrounding our pixel */
numPixels = checkMap(tfMap,colIdx, rowIdx, currentPixel, colSize, rowSize, blackPixels, direction, threshold);

  if (numPixels > 0)
  {
    /* If we have some black pixels, then loop over them */    
    for (idx=0;idx < numPixels;idx++)
    {
	rowShift = colShift = 0;
	if ((blackPixels[idx] == 1) || (blackPixels[idx] == 2) || (blackPixels[idx] == 3))
	  rowShift = +1;
	if ((blackPixels[idx] == 1) || (blackPixels[idx] == 4) || (blackPixels[idx] == 6))
	  colShift = -1;
	if ((blackPixels[idx] == 3) || (blackPixels[idx] == 5) || (blackPixels[idx] == 8))
	  colShift = +1;
	if ((blackPixels[idx] == 6) || (blackPixels[idx] == 7) || (blackPixels[idx] == 8))
	  rowShift = -1;

      /* If clusterMap already filled for this pixel, do not chain on it */
      if (clusterMap[(colIdx+colShift)+(rowIdx+rowShift)*colSize] > 0)
      {
	totalPixels;
      }
      else
      {
      totalPixels +=chainMap(tfMap, clusterMap, clusterEdges, (const int) colIdx+colShift, (const int)rowIdx+rowShift, (colIdx+colShift)+(rowIdx+rowShift)*colSize/*currentPixel*/ ,(const int) colSize, (const int) rowSize, blackPixels[idx], clustNo, threshold);
      }
      
      if (clustDebugLevel > 1)
	mexPrintf( "\t    ClustNo: %d, PixelNo: %d, Pos: (%d,%d) -> Idx: %d, Direction: %d\n", clustNo, totalPixels, colIdx+colShift, rowIdx+rowShift, (colIdx+colShift)+(rowIdx+rowShift)*colSize, blackPixels[idx]);
    }
  }
/*  tfMap[currentPixel] = 0;*/
if (clustDebugLevel > 1)
  mexPrintf( "\t  End of chain\n");
return totalPixels;
}


/* Generates clusters by following pixel chains.
 * Outline:
 *	- travel over tfmap
 *	- first pixel found that is over threshold, assign clusterNo to it
 *	- check surrounding pixels for 'black'
 *	- note position etc
 *	- for each of them, repeat above, assigning clusterNo and removing from map
 *	- do until no more black pixels
 *	- this cluster should then be completely done, so increment clusterNo,
 *	- As above pixels have been removed, loop around pixels until another passes threshold
 *	- this is a new cluster, so repeat above
 *
 *	A simple modification to this allows two thresholds to be defined, a peak threshold, and a
 * 	coronal threshold. First, the pixel value must be greater than the peak threshold to start
 * 	a cluster, then, adjacent pixels are tested against the coronal threshold
 *
 * Adaptation - do zonal thresholding:
 * 	each zone has a seperate threshold , but when chaining on a single cluster, the original 
 * 	threshold is use. It is only when we start a new cluster that we assign the threshold based
 *	on its location.
 */
int pixel_chain(int **clusterMapPtr, double **clusterStructPtr, double *tfMap, int colSize, int rowSize, double threshold[2], int mergeDistance, int userDebugLevel, int version)
{
  /* For each pixel, find surrounding 'black' pixels and follow them until they 
/* reach a clusterNo or no more plack pixels. */
int rowIdx, colIdx;
int positionInfo[3];
int pixelIdx;
int clustNo=0;
int clusterIdx;
int numPixels=0;

double *clusterStruct = NULL;

/* Set up floating pointers for the linked link swap */
struct boundaryStruct *currentBoundaryPtr= NULL;
struct boundaryStruct *prevBoundaryPtr= NULL;

/* Generate first boundary structure */
struct boundaryStruct *clusterEdges;
clusterEdges = (struct boundaryStruct *) malloc (sizeof(struct boundaryStruct));
if (clusterEdges == NULL)
{
  mexPrintf( "Unable to allocate memory for boundary structure\n");
  return -1;
}
init_boundaryStruct(clusterEdges);

/* Assign both floating pointers to this one */
currentBoundaryPtr = clusterEdges;
prevBoundaryPtr = clusterEdges;

/* Alter internal debug level based on user input*/ 
clustDebugLevel = userDebugLevel;

/* If second threshold not set up, make them equal
    - equivalent to turning off coronal clustering */
if (threshold[1] == 0)
  threshold[1] = threshold[0];

mexPrintf( "\t    Thresholds: %g - %g\n",threshold[0], threshold[1]);

/* Initiate map to hold cluster numbers */
int *clusterMap = NULL;
clusterMap = (int *) malloc(rowSize*colSize*sizeof(clusterMap));
if (clusterMap == NULL)
{
  mexPrintf( "Cannot allocate memory for clusterMap\n");
  return 0;
}

/* Get passed in pointer to point at malloced memory */
*clusterMapPtr = clusterMap;

/* Zero out the cluster array */
memset(clusterMap,0,sizeof(clusterMap)*rowSize*colSize);

/* Loop around pixels */
for (rowIdx=0; rowIdx < rowSize; rowIdx++)
  for (colIdx=0; colIdx < colSize; colIdx++)
  {
    pixelIdx 	= colIdx+rowIdx*colSize;

    /* If current pixel is part of a cluster */
    if ((tfMap[pixelIdx] > threshold[0]) && (clusterMap[pixelIdx] == 0))
    {
        if (clustDebugLevel > 1)
	  mexPrintf( "\t    Cluster number increase,from %d to %d\n",clustNo, clustNo+1);
	clustNo++;
	numPixels = 0;
	
	/* If pointer is blank, need to allocate memory for the next cluster structure */
	if (currentBoundaryPtr == NULL)
	{
	  currentBoundaryPtr = (struct boundaryStruct *) malloc (sizeof(struct boundaryStruct));

	  if (currentBoundaryPtr == NULL)
	  {
	    mexPrintf( "Unable to allocate memory for boundary structure\n");
	    return -1;
	  }

	  /* Copy pointer to previous cluster structure (i.e. link it in)*/ 
	  prevBoundaryPtr->nextCluster = currentBoundaryPtr;
	  init_boundaryStruct(currentBoundaryPtr);
	}
	
	currentBoundaryPtr->clusterNumber = clustNo;

      /* Check pixels surrounding our current pixel*/
	positionInfo[0] = colIdx;
	positionInfo[1] = rowIdx;
	positionInfo[2] = pixelIdx;
	
	/* Get lowest clusterNo attached to this pixel */
	if (clustDebugLevel > 1)
	  mexPrintf( "\t    ClustNo: %d,  (%d, %d) -> %d\n", clustNo, colIdx, rowIdx, pixelIdx);

	/* If version is 0, then use 'old' chaining code, else use the new one */
	if (version == 0)
	  numPixels = chainMap(tfMap, clusterMap, currentBoundaryPtr, positionInfo[0],positionInfo[1],positionInfo[2] , (const int)colSize, (const int)rowSize, 0, clustNo, threshold[1]);
	else
	  numPixels = chainMap_NEW(tfMap, clusterMap, currentBoundaryPtr, positionInfo[0],positionInfo[1],positionInfo[2] , (const int)colSize, (const int)rowSize, 0, clustNo, mergeDistance, threshold[1]);

	if (clustDebugLevel > 1)
	  mexPrintf( "\t    Cluster %d has %d pixels\n",clustNo, numPixels);
	
	currentBoundaryPtr->numPixels = numPixels;
	
	/* Swap pointers around in case we have another cluster*/
	prevBoundaryPtr = currentBoundaryPtr; 
	currentBoundaryPtr = prevBoundaryPtr->nextCluster;
    }
  }  
  
/* Dump cluster info out */
mexPrintf( "\t    Total number of clusters: %d\n",clustNo);

if ((clustNo > 0) && (clustDebugLevel == 2))
  print_boundaryStruct(clusterEdges);


if ((version == 0) && (mergeDistance > 0) && (clustNo > 0))
{
  mexPrintf( "\t    Merging clusters together using a pixel distance of: %d\n", mergeDistance);

  merge_clusters(clusterEdges ,clusterMap, clustNo, rowSize, colSize, mergeDistance);
}

/* Transfer cluster information for output */
if (clustNo > 0)
{
  int multiplier=0;
  clusterStruct = (double *) malloc(clustNo*NUM_CLUSTER_ELEMENTS*sizeof(double));

  if (clusterStruct == NULL)
  {
    mexPrintf( "Cannot allocate memory for cluster properties\n");
    return 0;
  }

  /* Get passed in pointer to point to malloc'ed memory */
  *clusterStructPtr = clusterStruct;

  /* Set up pointers ready for manipulation */
  currentBoundaryPtr = clusterEdges;
  prevBoundaryPtr = clusterEdges;

  while (currentBoundaryPtr != NULL)
  {
    prevBoundaryPtr 		=   currentBoundaryPtr;
    currentBoundaryPtr 		=   prevBoundaryPtr->nextCluster;

    clusterStruct[0+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->clusterNumber;
    clusterStruct[1+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->numPixels;
    clusterStruct[2+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->minX;
    clusterStruct[3+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->maxX;
    clusterStruct[4+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->minY;
    clusterStruct[5+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->maxY;
    clusterStruct[6+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->clusterPower;

    clusterStruct[7+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->maxPixelX;
    clusterStruct[8+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->maxPixelY;
    clusterStruct[9+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->maxPixelPower;
        
    multiplier++;
  }

/* Sort clusterStruct so that the loudest is top */
qsort(clusterStruct, clustNo, NUM_CLUSTER_ELEMENTS*sizeof(double), cmpMultiDouble);
}
else
{
  /* Allocate one row, but blank it */
  clusterStruct = (double *) malloc(NUM_CLUSTER_ELEMENTS*sizeof(double));
  memset(clusterStruct,0,sizeof(clusterStruct)*7);
}

/* Avoid dangling pointers */ 
currentBoundaryPtr = NULL;
prevBoundaryPtr = NULL;

/* Safely free memory assocaited with our linked list of clusters */
free_boundaryStruct(clusterEdges);

return clustNo;
}


/*     ATTEMPT NUMBER 1 
 * Simple, quick, dumb clusterer - starts in one corner and walks over array, 
 * assigning cluster numbers as it needs too. 
 * Problem is that disparate pixels are not clusters together. 
 * Hence the second stage merger above*/
 
/* Another way
 *  - sort array according to pixel values (keep position info too)
 *  - go down list, assigning each pixel a cluster number 
 *  - if one pixel is near another, assign same cluster number to it.
 *  - go over clusters, merging those in close proximity.
 */


/* Generates clusters by following pixel chains.
 * Outline:
 *	- travel over tfmap
 *	- first pixel found that is over threshold, assign clusterNo to it
 *	- check surrounding pixels for 'black'
 *	- note position etc
 *	- for each of them, repeat above, assigning clusterNo and removing from map
 *	- do until no more black pixels
 *	- this cluster should then be completely done, so increment clusterNo,
 *	- As above pixels have been removed, loop around pixels until another passes threshold
 *	- this is a new cluster, so repeat above
 *
 *	A simple modification to this allows two thresholds to be defined, a peak threshold, and a
 * 	coronal threshold. First, the pixel value must be greater than the peak threshold to start
 * 	a cluster, then, adjacent pixels are tested against the coronal threshold
 *
 * Adaptation - do zonal thresholding:
 * 	each zone has a seperate threshold , but when chaining on a single cluster, the original 
 * 	threshold is use. It is only when we start a new cluster that we assign the threshold based
 *	on its location.
 */
int regional_pixel_chain(int **clusterMapPtr, double **clusterStructPtr, double *tfMap, int colSize, int rowSize, double *clusterThresholds, int nRegions, int direction,int mergeDistance, int userDebugLevel, int version)
{
  /* For each pixel, find surrounding 'black' pixels and follow them until they 
/* reach a clusterNo or no more plack pixels. */
int rowIdx, colIdx;
int positionInfo[3];
int pixelIdx;
int thresholdIdx=0;
int clustNo=0;
int clusterIdx;
int numPixels=0;

double *clusterStruct = NULL;

/* Set up floating pointers for the linked link swap */
struct boundaryStruct *currentBoundaryPtr= NULL;
struct boundaryStruct *prevBoundaryPtr= NULL;

/* Set up region based clustering products */
int *boundaries = NULL;
double **thresholds = NULL;

/* Generate first boundary structure */
struct boundaryStruct *clusterEdges;
clusterEdges = (struct boundaryStruct *) malloc (sizeof(struct boundaryStruct));
if (clusterEdges == NULL)
{
  mexPrintf( "Unable to allocate memory for boundary structure\n");
  return -1;
}
init_boundaryStruct(clusterEdges);

/* Assign both floating pointers to this one */
currentBoundaryPtr = clusterEdges;
prevBoundaryPtr = clusterEdges;

/* Alter internal debug level based on user input*/ 
clustDebugLevel = userDebugLevel;

/* If second threshold not set up, make them equal
    - equivalent to turning off coronal clustering */
if (clusterThresholds[1] == 0)
  clusterThresholds[1] = clusterThresholds[0];

mexPrintf( "\t    Thresholds: %g - %g\n",clusterThresholds[0], clusterThresholds[1]);

/* Initiate map to hold cluster numbers */
int *clusterMap = NULL;
clusterMap = (int *) malloc(rowSize*colSize*sizeof(clusterMap));
if (clusterMap == NULL)
{
  mexPrintf( "Cannot allocate memory for clusterMap\n");
  return 0;
}

/**************************************/
/* Code for region based thresholding */
/**************************************/

    /* Dump out tfmap 
    mexPrintf("In Matrix: (%d,%d)\n",colSize, rowSize);
    int idx;
    for (rowIdx=0;rowIdx < rowSize/*10/;rowIdx++)
    {
	for (colIdx=0;colIdx < colSize/*10/;colIdx++)
	{
	  idx = colIdx+colSize*rowIdx;
	  fprintf(stdout, " %f ",tfMap[idx]);
	}
	mexPrintf("\n");
    }
    mexPrintf("\n");
   */
    
   /* Check number of regions */
   if ((nRegions >= colSize) || (nRegions <=0))
   {
     nRegions = (int) floor(colSize/2.);
     if  (nRegions < 1)
	nRegions = 1;
     fprintf(stdout,"\t ERROR: More regions than pixels, reseting to %d \n",nRegions);
   }
    
    /* Set thresholds for regions */
    boundaries = set_boundaries(colSize, rowSize,  nRegions, direction);
    thresholds = calc_thresholds(tfMap, colSize, rowSize,boundaries, nRegions, clusterThresholds, direction);
    
  
/* Get passed in pointer to point at malloced memory */
*clusterMapPtr = clusterMap;

/* Zero out the cluster array */
memset(clusterMap,0,sizeof(clusterMap)*rowSize*colSize);

/* Loop around pixels */
for (rowIdx=0; rowIdx < rowSize; rowIdx++)
  for (colIdx=0; colIdx < colSize; colIdx++)
  {
    pixelIdx 	= colIdx+rowIdx*colSize;
    thresholdIdx = return_thresholdIdx(colIdx, rowIdx, boundaries,nRegions, direction);
    /*
    fprintf(stdout,"\tThresholds for pixel %d -> (%g, %g)\n",pixelIdx,thresholds[0][thresholdIdx], thresholds[1][thresholdIdx] );
    */
    /* If current pixel is part of a cluster */
    if ((tfMap[pixelIdx] > thresholds[0][thresholdIdx]) && (clusterMap[pixelIdx] == 0))
    {
        if (clustDebugLevel > 1)
	  mexPrintf( "\t    Cluster number increase,from %d to %d\n",clustNo, clustNo+1);
	clustNo++;
	numPixels = 0;
	
	/* If pointer is blank, need to allocate memory for the next cluster structure */
	if (currentBoundaryPtr == NULL)
	{
	  currentBoundaryPtr = (struct boundaryStruct *) malloc (sizeof(struct boundaryStruct));

	  if (currentBoundaryPtr == NULL)
	  {
	    mexPrintf( "Unable to allocate memory for boundary structure\n");
	    return -1;
	  }

	  /* Copy pointer to previous cluster structure (i.e. link it in)*/ 
	  prevBoundaryPtr->nextCluster = currentBoundaryPtr;
	  init_boundaryStruct(currentBoundaryPtr);
	}
	
	currentBoundaryPtr->clusterNumber = clustNo;

      /* Check pixels surrounding our current pixel*/
	positionInfo[0] = colIdx;
	positionInfo[1] = rowIdx;
	positionInfo[2] = pixelIdx;
	
	/* Get lowest clusterNo attached to this pixel */
	if (clustDebugLevel > 1)
	  mexPrintf( "\t    ClustNo: %d,  (%d, %d) -> %d\n", clustNo, colIdx, rowIdx, pixelIdx);

	/* If version is 0, then use 'old' chaining code, else use the new one */
	if (version == 0)
	  numPixels = chainMap(tfMap, clusterMap, currentBoundaryPtr, positionInfo[0],positionInfo[1],positionInfo[2] , (const int)colSize, (const int)rowSize, 0, clustNo, thresholds[1][thresholdIdx]);
	else
	  numPixels = chainMap_NEW(tfMap, clusterMap, currentBoundaryPtr, positionInfo[0],positionInfo[1],positionInfo[2] , (const int)colSize, (const int)rowSize, 0, clustNo, mergeDistance, thresholds[1][thresholdIdx]);

	if (clustDebugLevel > 1)
	  mexPrintf( "\t    Cluster %d has %d pixels\n",clustNo, numPixels);
	
	currentBoundaryPtr->numPixels = numPixels;
	
	/* Swap pointers around in case we have another cluster*/
	prevBoundaryPtr = currentBoundaryPtr; 
	currentBoundaryPtr = prevBoundaryPtr->nextCluster;
    }
  }  
  
/* Dump cluster info out */
mexPrintf( "\t    Total number of clusters: %d\n",clustNo);

if ((clustNo > 0) && (clustDebugLevel == 2))
  print_boundaryStruct(clusterEdges);


if ((version == 0) && (mergeDistance > 0) && (clustNo > 0))
{
  mexPrintf( "\t    Merging clusters together using a pixel distance of: %d\n", mergeDistance);

  merge_clusters(clusterEdges ,clusterMap, clustNo, rowSize, colSize, mergeDistance);
}

/* Transfer cluster information for output */
if (clustNo > 0)
{
  int multiplier=0;
  clusterStruct = (double *) malloc(clustNo*NUM_CLUSTER_ELEMENTS*sizeof(double));

  if (clusterStruct == NULL)
  {
    mexPrintf( "Cannot allocate memory for cluster properties\n");
    return 0;
  }

  /* Get passed in pointer to point to malloc'ed memory */
  *clusterStructPtr = clusterStruct;

  /* Set up pointers ready for manipulation */
  currentBoundaryPtr = clusterEdges;
  prevBoundaryPtr = clusterEdges;

  while (currentBoundaryPtr != NULL)
  {
    prevBoundaryPtr 		=   currentBoundaryPtr;
    currentBoundaryPtr 		=   prevBoundaryPtr->nextCluster;

    clusterStruct[0+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->clusterNumber;
    clusterStruct[1+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->numPixels;

    clusterStruct[2+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->minX;
    clusterStruct[3+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->maxX;
    clusterStruct[4+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->minY;
    clusterStruct[5+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->maxY;
    
    clusterStruct[6+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->clusterPower;

    clusterStruct[7+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->maxPixelX;
    clusterStruct[8+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->maxPixelY;
    
    clusterStruct[9+NUM_CLUSTER_ELEMENTS*multiplier]	=   prevBoundaryPtr->maxPixelPower;
        
    multiplier++;
  }

/* Sort clusterStruct so that the loudest is top */
qsort(clusterStruct, clustNo, NUM_CLUSTER_ELEMENTS*sizeof(double), cmpMultiDouble);
}
else
{
  /* Allocate one row, but blank it */
  clusterStruct = (double *) malloc(NUM_CLUSTER_ELEMENTS*sizeof(double));
  memset(clusterStruct,0,sizeof(clusterStruct)*7);
}

/* Avoid dangling pointers */ 
currentBoundaryPtr = NULL;
prevBoundaryPtr = NULL;

/* Safely free memory assocaited with our linked list of clusters */
free_boundaryStruct(clusterEdges);


  free(boundaries);
  free(thresholds[0]);
  free(thresholds[1]);
  free(thresholds);
  
return clustNo;
}
