/*
 * Copyright (C) 2010  Mark Edwards
 *   (based on code written by Kipp Cannon, Copyright (C) 2006)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#include <fftw3.h>

#include "mat.h"
#include "sphrad.h"
#include "sphrad_ext.h"

#include <correlator.h>
#include <instrument.h>
#include <sh_series.h>
#include <inject.h>
#include <instruments.h>

/* Debugging specific includes*/
#include "memwatch.h"

int miscDebugLevel = 0;

int check_isnan(double x)
{
  /* Stop the compiler optimising the code */
  volatile double temp_x = x;
  return temp_x != x;
}

int check_isinf(double x)
{
  /* Stop the compiler optimising the code */  
  volatile double temp_x =x;
  if ((temp_x == x) && ((temp_x - x) != 0.0)) 
    return (x < 0.0 ? -1 : 1);
  else return 0;
}

int cross_product(double *inA, double *inB, double *out)
{
 out[0] = inA[1]*inB[2]-inA[2]*inB[1];
 out[1] = inA[2]*inB[0]-inA[0]*inB[2];
 out[2] = inA[0]*inB[1]-inA[1]*inB[0];
 return 1;
}

int cmpMultiDouble (const void* p1, const void* p2)
{
/*
For use with qsort:
Compare multiple columns in a multidimensional array,
specifically a clusterStructure that has 7 columns per row:
  Cluster energy at 6, 
  number of pixels at 1, 
  cluster number at 0
  so we sort on energy, then pixel numbers, then cluster number
*/

double* y= (double*) p1;
double* x= (double*) p2;

/* Get difference in energy */
double diff1 = y[6] - x[6]; 

return x[6] <= y[6] ? (x[6]<y[6]? -1:(x[1] <= y[1] ? (x[1]<y[1]? -1:0) :1)) :1;

}

int cmpMultiDoubleMP (const void* p1, const void* p2)
{
/* THIS SORTS ON MAX PIXEL
For use with qsort:
Compare multiple columns in a multidimensional array,
specifically a clusterStructure that has 10 columns per row:
  Max pixel power at 9
  Cluster energy at 6, 
  number of pixels at 1, 
  cluster number at 0
  so we sort on energy, then pixel numbers, then cluster number
*/

double* y= (double*) p1;
double* x= (double*) p2;

/* Get difference in max pixel power */
double diff1 = y[9] - x[9]; 

return x[9] <= y[9] ? (x[9]<y[9]? -1:(x[6] <= y[6] ? (x[6]<y[6]? -1:0) :1)) :1;

}


int cmpSingleDouble (const void* p1, const void* p2)
{
/*
For use with qsort:
Compare multiple columns in the multidimensional array
*/

double* x= (double*) p1;
double* y= (double*) p2;
/* Get difference in energy */
int diff1 = x[0] - y[0]; 

if (diff1) return diff1;
return x[1] - y[1];
}


/*Computes log(exp(a) + exp(b)) for values a and b while guarding against
 * overflow if exp(a) and or exp(b) are very large.
 */
double logsumexp(a, b)
{
   double d;
   d = (a>b)?a:b;

   return  log(exp(a - d) + exp(b - d)) + d;
}

/* Take string fragments and return filename */
/*  H1L1V1-OMEGA_EVENTS-0870123674-1892.txt  */
/* Detectors-SPHRAD_EVENTS-blockStartTime-Duration.txt */
char *make_filename(char * path, char ** dect, char *preName, int start, int duration, char *postName)
{	
  char *varName;
  varName = (char *)malloc(300*sizeof(char));

  if (!varName)
	{	
		fprintf(stderr, "\tMEM ERROR: make_filename has failed\n");
		return NULL;
	}
/* Check the content of the array dect to see if we are running 2 or 3 detectors */
/* the 3rd element of dect should be empty, so the first characer should be \0 */
/* UPDATE: Hardwired a check on the third detector, \0 was not always the first */
/* char for 2 det ...  */
  /* if (dect[2] == "V1") */

  /* kludge: this has to be changed (am running jobs so I don't want to break it too hard) */
  
   sprintf(varName, "%s/%s-%010d-%d%s", path, preName, start, duration, postName);
   return varName;

}


/* Create a hann window */
double *hann_window(const int N)
{
	double *window = malloc(N * sizeof(*window));
	int n;

	if(!window) 
	{
		free(window);
		return NULL;
	}

	memset(window, 0,  sizeof(*window));

	for (n=0; n < N; n++)
		window[n] = 0.5*(1.-cos((2.*M_PI*n)/(N-1.)));
		
	return window;
}






/*
 *   Get data about any potential candidate within this data segment
 *    - return central frequency, bandwidth and time
 */
double identify_candidate(const struct sh_series *segment, double deltaT)
{

int i,k;
complex double lMax;
complex double power = 0;
int lIdx = 0;
double cFreq = 0;
complex double totalPower = 0;
int positiveFreqs;

/*
  * Start by finding the approximate frequency of the trigger
  * - this is proportional to the order of the spherical harmonics 
  *   and the bandwidth is proportional to the number 'rung'
  */

 lIdx = 0;
 lMax = 0;
 
 for (i = 0; i < segment->l_max; i++)
 {
   if (cabs(lMax*lMax) < cabs(segment->coeff[i]*segment->coeff[i])) 
   {
     lMax = segment->coeff[i];
     lIdx = i;
   }
   power += (segment->coeff[i] * segment->coeff[i]);
 }
 
 /* (1/dt) = sample frequency, /2 gives Nuquist frequency */
 
 cFreq = (lIdx *((1./deltaT))/(segment->l_max));
 
 if (miscDebugLevel == 1)
    mexPrintf( "Max coeff power %g at idx %d (freq: %.1f), total zonal power: %g \n",(cabs(segment->coeff[lIdx]*/*conj*/(segment->coeff[lIdx]))),lIdx,cFreq, cabs(power));
 
 /* Test computing the power from all of the SH coefficients */
 lIdx = 0;
 lMax = 0;
 
 /* Determine index of highest positive coefficient */
 positiveFreqs = (sh_series_length(segment->l_max, segment->polar)-segment->l_max);
 positiveFreqs = ceil(positiveFreqs/2.0);
 positiveFreqs += segment->l_max;
 
 /* Only check "positive" frequencies */
 for (i = 0; i < positiveFreqs; i++)
  {
   if (cabs(lMax*lMax) < cabs(segment->coeff[i]*segment->coeff[i])) 
   {
     lMax = segment->coeff[i];
     lIdx = i;
   }
   totalPower += (segment->coeff[i] * segment->coeff[i]);
  }
  /* Approximate frequency via the maximal SH coeff 
   *  SH idx/total positive indices * sampleFreq/2
   */
  cFreq =(double) ((double)lIdx/(double)positiveFreqs)*(0.5/deltaT);
 
  if (miscDebugLevel == 1)
    mexPrintf( " - total SH power %g (Idx: %d, freq: %.1f) \n",cabs(totalPower),lIdx, cFreq);
 
 return cabs(totalPower);
}

/*
 *   Method to sum spherical harmonics
 *
 */
complex double sh_sum (struct sh_series *series)
{
/* Need to sum the power in the coefficients - assuming that Parseval's theorem 
 * holds, then it should be Sum((l=0-Inf) (Sum(-m<l<-m)) |f(lm)|^2))
 * So, all we need to do here is multiply the coefficient by its conjugate
 */
int cIdx = 0;
complex double result = 0;
  
for (cIdx=0;cIdx<sh_series_length(series->l_max, series->polar);cIdx++)
  result += (series->coeff[cIdx]*conj(series->coeff[cIdx]));

return result;
}

/* Dump out data in Omegas event format */
int output_omega_event(int followUp, FILE *outFile, struct analysisData *analysis, struct eventStruct *trigger[])
{
/* 
% network (H1, L1, V1) - blockStartTime - blockStopTime - livetime - time - frequency - duration - bandwidth
% modeTheta - modePhi - logSignal- logGlitch - logOdds - probSignal - probGlitch
% crossEnergy - crossIncoherentEnergy - plusEnergy - plusIncoherentEnergy
% nullEnergy - nullIncoherentEnergy - standardEnergy - standardIncoherentEnergy
*/
int dIdx, tIdx, idx;

for (tIdx = 0; tIdx < followUp; tIdx++)
{
  /* network */
  for (dIdx = 0; dIdx < analysis->nInstr; dIdx++)
  {
    fprintf(outFile, "%s", analysis->detName[dIdx]);
    if (dIdx < (analysis->nInstr-1))
      fprintf(outFile, ",");
  }

  /* block time information */
  fprintf(outFile, " %#020.9f  ", analysis->blockStartTime);
  fprintf(outFile, "%#020.9f  ", analysis->blockStopTime);
  fprintf(outFile, "%#.9f  ", (analysis->blockStopTime - analysis->blockStartTime) - (analysis->transientLen/analysis->sampleFrequency)*2.);

  /* cluster information */
  fprintf(outFile, "%#020.9f  ", trigger[tIdx]->time);
  fprintf(outFile, "%.16e  ", trigger[tIdx]->frequency);
  fprintf(outFile, "%.16e  ", trigger[tIdx]->duration);
  fprintf(outFile, "%.16e  ", trigger[tIdx]->bandwidth);

  /* sky position */
  fprintf(outFile, "%f  ", trigger[tIdx]->theta);
  fprintf(outFile, "%f  ", trigger[tIdx]->phi);

  /* Bayesian statistics, which we are not using 
  * So we are going to output zeros instead 
  * There are 5 elements altogether, and we are going to use one to output the number of pixels
  * in the cluster  */
   
  fprintf(outFile, "%g  ", 0.0);

  fprintf(outFile, "%f  ", (double) trigger[tIdx]->shE);
  fprintf(outFile, "%f  ", (double) trigger[tIdx]->numPixels);
  fprintf(outFile, "%f  ", (double) trigger[tIdx]->shI);

  fprintf(outFile, "%g  ", 0.0);

  /* (In)Coherent projection energies */
  fprintf(outFile, "%.16e  ", trigger[tIdx]->crossE);
  fprintf(outFile, "%.16e  ", trigger[tIdx]->crossI);
  fprintf(outFile, "%.16e  ", trigger[tIdx]->plusE);
  fprintf(outFile, "%.16e  ", trigger[tIdx]->plusI);
  fprintf(outFile, "%.16e  ", trigger[tIdx]->nullE);
  fprintf(outFile, "%.16e  ", trigger[tIdx]->nullI);
  fprintf(outFile, "%.16e  ", trigger[tIdx]->standardE);
  fprintf(outFile, "%.16e  ", trigger[tIdx]->standardI);
  
  fprintf(outFile, "%.16e\n", trigger[tIdx]->clusterEnergy);
}
}

/* 
 * Take two functions defined on the sphere expanded in terms of spherical harmonics, 
 *  and transform them to the spatial domain for multiplcation
 */
struct sh_series *convolve_sh_series(const struct sh_series *series1, const struct sh_series *series2)
{
/* Combine is either 'a' or 'm' for add or multiply respectively */
/* Coeffs -> add on grid -> coeffs */
/* Carried out in place, so series1 is replaced with the combined coefficients */

complex double *grid1, *grid2;
complex double *outGrid;
struct sh_series *out;

int bw, size;
int coeffIdx,gridIdx,j,k,i;
int orig_l_max = 0;

/* Get size of grid from the order of the spherical harmonics 
 * Double the size of the input sh coefficients so that we do not lose 
 * any information during the transforms 
 */
orig_l_max = series1->l_max;

/* Bandwidth of problem is one more than the order of the expansion 
 * or the sqrt of the length of the series
 */
bw = sqrt(sh_series_length(/*2**/orig_l_max, series1->polar));
size=2*bw;
/*
mexPrintf( "Data sizes: %d -> %d -> %d\n", orig_l_max, bw, size);
*/

/* Allocate memory for the grid */
grid1 = (complex double *) malloc(sizeof(complex double) * size * size);
grid2 = (complex double *) malloc(sizeof(complex double) * size * size);
outGrid = (complex double *) malloc(sizeof(complex double) * size * size);

out = sh_series_new_zero(/*2**/orig_l_max, series1->polar);

if (!out || !grid1 || !grid2 || !outGrid)
{
    fprintf(stderr, "MEM ERROR: Unable to allocate memory in convolve function\n");
    return NULL;
}

/* Resize the coefficients 
sh_series_resize(series1, 2*orig_l_max);
sh_series_resize(series2, 2*orig_l_max);
*/

/* Inverse transform to spatial domain */
FSHT_inverse(grid1, series1->coeff, bw);
FSHT_inverse(grid2, series2->coeff, bw);

/*
if (strcmp(combine, "add") == 0)
  for(gridIdx=0;gridIdx<(size * size);gridIdx++)
    outGrid[gridIdx] = creal(grid1[gridIdx]) + creal(grid2[gridIdx]);
else
*/

for(gridIdx=0;gridIdx<(size * size);gridIdx++)
    outGrid[gridIdx] = (grid1[gridIdx]) * (grid2[gridIdx]);

/* Forward transform */
FSHT_forward(out->coeff, outGrid, size);

/* In place copy - if resizing
FSHT_forward(series1->coeff, outGrid, size);
*/

/*
out = sh_series_resize(out, orig_l_max);
*/

free(outGrid);
free(grid1);
free(grid2);
return out;
}

/*  Take a gridded skymap and rotate it according to the GPS time of the analysis */
/*  We need to forward transform to harmonic domain, rotate, then inverse transform */
/*  May need to increase resolution to avoid loss of detail */

// double *gps_rotate(double *skyMap, int gridSize, int startGPSTime, double diffGPSTime)
// {

// complex double *skyGrid;
// struct sh_series *skySH;
// double *outGrid;

// int bw, size;
// int coeffIdx,gridIdx;
// double gmst;
// LIGOTimeGPS analysis_start;


// analysis_start = (LIGOTimeGPS) {
// 		    .gpsSeconds = startGPSTime,
// 		    .gpsNanoSeconds = 0,
// 		};

// /* A GPS time of (94669200,0.0) appears to return the original skymap */
// mexPrintf( "\tRotate->   GPSTime: %d, trigTime: %g\n",startGPSTime, diffGPSTime);
		
// /* Bandwidth of problem is one more than the order of the expansion, 
//  * the sqrt of the length of the series or half the grid length
//  */
// bw = gridSize / 2.;

// /* Allocate memory for the products */
// skyGrid = (complex double *) malloc(sizeof(complex double) * gridSize * gridSize);
// outGrid = (double *) malloc(sizeof(double) * gridSize * gridSize);

// if (!skyGrid || !skySH )
// {
//     fprintf(stderr, "MEM ERROR: gps_rotate failed \n");
//     return NULL;
// }

// skySH = sh_series_new_zero(bw, 0);

// /* Need to pass a complex array into the transform */
// for (gridIdx=0;gridIdx<gridSize*gridSize;gridIdx++)
//   skyGrid[gridIdx] = skyMap[gridIdx]+(I*0);

// /* Forward transform */
// FSHT_forward(skySH->coeff, skyGrid, gridSize);
  
// /* Rotate to the current GPS time */
// gmst = XLALGreenwichSiderealTime(XLALGPSAdd(&analysis_start, diffGPSTime), 0.0);
// mexPrintf( "\tRotation time: %g\n",gmst);
// sh_series_rotate_z(skySH, skySH, gmst);

// /* Inverse transform */
// FSHT_inverse(skyGrid, skySH->coeff, bw);

// for (gridIdx=0;gridIdx<gridSize*gridSize; gridIdx++)
//      outGrid[gridIdx] = creal(skyGrid[gridIdx]);

// /* Free memory */
// sh_series_free(skySH);
// free(skyGrid);

// return outGrid;
// }



/* 
 * Take two functions defined on the sphere expanded in terms of spherical harmonics, 
 *  and transform them to the spatial domain for multiplcation
 */
struct sh_series *convolve_sh_grid(const struct sh_series *series1, const double *grid2)
{
/* Coeffs ->synthesise-> add on grid ->transform-> coeffs */
/* Carried out in place, so series1 is replaced with the combined coefficients */

complex double *grid1;
complex double *outGrid;
struct sh_series *out;

int bw, size;
int coeffIdx,gridIdx,j,k,i;
int orig_l_max = 0;

/* Get size of grid from the order of the spherical harmonics 
 * Double the size of the input sh coefficients so that we do not lose 
 * any information during the transforms 
 */
orig_l_max = series1->l_max;

/* Bandwidth of problem is one more than the order of the expansion 
 * or the sqrt of the length of the series
 */
bw = sqrt(sh_series_length(/*2**/orig_l_max, series1->polar));
size=2*bw;
/*
mexPrintf( "Data sizes: %d -> %d -> %d\n", orig_l_max, bw, size);
*/

/* Allocate memory for the grid */
grid1 = (complex double *) malloc(sizeof(complex double) * size * size);
outGrid = (complex double *) malloc(sizeof(complex double) * size * size);

out = sh_series_new_zero(/*2**/orig_l_max, series1->polar);

if (!out || !grid1 || !outGrid )
{
    fprintf(stderr, "MEM ERROR: Unable to allocate memory in convolve function\n");
    return NULL;
}

/* Resize the coefficients 
sh_series_resize(series1, 2*orig_l_max);
sh_series_resize(series2, 2*orig_l_max);
*/

/* Inverse transform to spatial domain */
FSHT_inverse(grid1, series1->coeff, bw);

/*
if (strcmp(combine, "add") == 0)
  for(gridIdx=0;gridIdx<(size * size);gridIdx++)
    outGrid[gridIdx] = creal(grid1[gridIdx]) + creal(grid2[gridIdx]);
else
*/

for(gridIdx=0;gridIdx<(size * size);gridIdx++)
    outGrid[gridIdx] = (grid1[gridIdx]) * (grid2[gridIdx]);

/* Forward transform */
FSHT_forward(out->coeff, outGrid, size);

/* In place copy - if resizing
FSHT_forward(series1->coeff, outGrid, size);
*/

/*
out = sh_series_resize(out, orig_l_max);
*/

free(outGrid);
free(grid1);
return out;
}

/* 
 * Take two functions defined on the sphere expanded in terms of spherical harmonics, 
 *  and transform them to the spatial domain for multiplcation
 */
complex double *sh_to_grid(const struct sh_series *series1)
{
/* Coeffs ->synthesise-> add on grid ->transform-> coeffs */
/* Carried out in place, so series1 is replaced with the combined coefficients */

complex double *outGrid;

int bw, size;
int coeffIdx,gridIdx,j,k,i;
int orig_l_max = 0;

/* Get size of grid from the order of the spherical harmonics 
 * Double the size of the input sh coefficients so that we do not lose 
 * any information during the transforms 
 */
orig_l_max = series1->l_max;

/* Bandwidth of problem is one more than the order of the expansion 
 * or the sqrt of the length of the series
 */
bw = sqrt(sh_series_length(/*2**/orig_l_max, series1->polar));
size=2*bw;


/* Allocate memory for the grid */
outGrid = (complex double *) malloc(sizeof(complex double) * size * size);

if (!outGrid)
{
    fprintf(stderr, "MEM ERROR: Unable to allocate memory in convolve function\n");
    return NULL;
}

/* Resize the coefficients 
sh_series_resize(series1, 2*orig_l_max);
sh_series_resize(series2, 2*orig_l_max);
*/

/* Inverse transform to spatial domain */
FSHT_inverse(outGrid, series1->coeff, bw);

/*
if (strcmp(combine, "add") == 0)
  for(gridIdx=0;gridIdx<(size * size);gridIdx++)
    outGrid[gridIdx] = creal(grid1[gridIdx]) + creal(grid2[gridIdx]);
else
*/

/* In place copy - if resizing
FSHT_forward(series1->coeff, outGrid, size);
*/

/*
out = sh_series_resize(out, orig_l_max);
*/

return outGrid;
}

/*
 *  Function to return time lags for detectors
 */

/*

int *generate_timelags(int *timeShifts, int numberTimeShifts, int tau, int T, int nInstr)
{
int i,j,k;
    
mexPrintf("TimeShifts: \n\t");
for (i=0;i <numberTimeShifts;i++)
  mexPrintf( "%i: %i  ", i, timeShifts[i]);
  
mexPrintf( "\tT: %d samples\n\ttau: %d s\n",T, tau);

double nShifts =(double)taunumberTimeShifts - 1;
 
mexPrintf( "First nShifts: %f\n",nShifts);
int totalShifts = pow(nShifts, nInstr-1)-nShifts;

mexPrintf( "Rounded nShifts, %d of %d seconds\n",(int) totalShifts, timeShifts[1]);
*/

/* Det2 not 0, Det3 not 0, not Det2 */

/*
mexPrintf( "Time shifts to apply\n");
int detIdx[nInstr];
for (i=0;i<nInstr;i++)
  detIdx[i] = 0;

int pretty = 0;
int diff=0;
int *lags[totalShifts][nInstr-1];

for (i=0; i < totalShifts;i++)
{
*/
/*
0 - 00

1 - 4,8		7 - 8 , 4	13 - 12 , 4	19 - 16 , 4  [,8,12,20,24,28]
2 - 4,12	8 - 8 , 12	14 - 12 , 8	20 - 16 , 8  [,4,12,20,24,28]
3 - 4,16	9 - 8 , 16	15 - 12 , 16	21 - 16 , 12 [,4,8,20,24,28]
4 - 4,20	10- 8 , 20	16 - 12 , 20	22 - 16 , 20 [,4,8,12, 24,28]
5 - 4,24	11- 8 , 24	17 - 12 , 24	23 - 16 , 24 [,4,8,12,20,28]
6 - 4,28	12- 8 , 28	18 - 12 , 28	24 - 16 , 28 [,4,8,12,20,24]

Pick first non-zero time shift for second detector - 4s, idx 1
  - pick first non zero, non second detector value for third 8s, idx 2
    - pick ....
  - pick next non zero, non second detector value for third, until end reached, 12s, idx 3
    - pick ...
Pick next non-zero time shift for 2nd detector - 8s, idx 2
  - pick - 2s, idx 1
    -pick ...

Must ignore zero lag analysis, remove it from the vector list and subtract one from the 
number of shifts.

timeShift vector

DETECTOR 2:
floor(shiftIdx/numShifts)+1

This gives 1 - [0 (numShifts-1)], 2 - [numShifts (numshifts*2 -1)] etc
which equates to a direct index value

DETECTOR 3:
shiftIdx%numShifts



detIdx[0] = floor(i/(nShifts-1))+1;
detIdx[1] = i % (int)(nShifts-1)+1;

if (pretty != detIdx[0])
{
  mexPrintf( "\n"); 
  diff = 0;
}

if (detIdx[1] == detIdx[0])
{
  diff +=1;
}

pretty = detIdx[0];

lags[i][0]=detIdx[0];
lags[i][1]=detIdx[1]+diff;

mexPrintf( " (%d: %d, %d) ",i,lags[i][0]*tau, (lags[i][1])*tau);

}     
mexPrintf("\n");
return lags;
}
*/

/* Function to just return the indices for the time shifts */
/*
0: 4,8,12    1: 4,8,16    2:4,8,20     3:4,8,24     4:4,8,28
5: 4,12,8    6: 4,12,16   7:4,12,20    8:4,12,24    9:4,12,28
10: 4,16,8   11: 4,16,12  12:4,16,20   13:4,16,24   14:4,16,28
15: 4,20,8   16: 4,20,12  17:4,20,16   18:4,20,24   19:4,20,28
20: 4,24,8   21: 4,24,12  22:4,24,16   23:4,24,20   24:4,24,28
25: 4,28,8   26: 4,28,12  27:4,28,16   28:4,28,20   29:4,28,24
30: 8,4,12   31: 8,4,16   32:8,4,20    33:8,4,24    34:8,4,28*/

int *get_lagIndex( int index, int numberTimeShifts, int tau, int T, int nInstr)
{
int det2,det3;
  

int *lags;
lags = malloc((nInstr-1)*sizeof(int*));


if (!lags)	
{
  fprintf(stderr, "\tMEM ERROR: Can't allocate memory for lags\n");
  return NULL;
}

/* Not sure if this is needed - I'll remove for now
if ((numberTimeShifts-nInstr-1) < 0)
{
  fprintf(stderr, "Too few shifts for the number of detectors, exiting\n");
  return NULL;
}
*/

if (index == -1)
{
/* Zero Lag */
lags[0] = 0;
lags[1] = 0;
}
else
{
lags[0] = (int) floor(index/(numberTimeShifts-(nInstr-1)))+1;
lags[1] = (int) index % (numberTimeShifts-(nInstr-1))+1;

/* Why could this happen? */
if ( lags[0] <= lags[1])
  lags[1] += 1;
}

return lags;
}

/*
 * ============================================================================
 *
 *     Dump out internal products 
 *	- various functions to output internal variables,
 * ============================================================================
 */

/* Takes a mex varaible and some names, and saves the variable to 
 *   the mat file */

int write_to_mat(char *fileName, char *varName, mxArray *variable)
{
    int i,j,k;

    MATFile *outFile = NULL;
    outFile = matOpen(fileName,"u");
    
    if (outFile)
    {
      if (miscDebugLevel == 1)
      {
	mexPrintf( "WARNING: File already exists, opening for updating\n");
	mexPrintf( "\tThis operation may overwrite data in the mat file\n");
      }
    }
    else
    {
      if (miscDebugLevel == 1)
	  mexPrintf( "File does not exist - creating\n");
      outFile=matOpen(fileName,"w");      
    }
        
    if (matPutVariable(outFile, varName, variable) != 0)
    {
      mexPrintf( "Unable to write matlab file\n");
      matClose(outFile);
      return -1;
    }
    else 
      if (miscDebugLevel == 1)
	mexPrintf( "Wrote %s to %s\n",varName, fileName);
      
    if (matClose(outFile) != 0)
    {
      mexPrintf( "Write error \n");
      return -1;
    }
    else  
      if (miscDebugLevel == 1)
	mexPrintf( "Successfully closed\n");
return 0;    
}

/* 
 *  Dump event statistics out in Omega_EVENT format
 */
int write_events(char *aFileName, struct analysisData *inData, double *clusterStruct, int followUp)
{
  int instrIdx;
  int eventIdx;
  double theta=0.0;
  double phi=0.0;
  char *networkStr;
  FILE *omega_event;
  omega_event=fopen(aFileName,"a");
  
  if (omega_event != NULL)
  {
    /* No i/o error */
    
    networkStr = (char *) malloc(sizeof(char)*((inData->nInstr*2)+(inData->nInstr-1)*2));
    
    if (!networkStr)
	{
		fprintf(stderr, "\tMEM ERROR: write_events failed to allocate memory\n");
	}

    /* Generate  detector network */
    for (instrIdx=0; instrIdx<inData->nInstr;instrIdx++)
    {
      sprintf(networkStr, "%s%s", networkStr, inData->detName[instrIdx]);
      if (instrIdx < (inData->nInstr-1))
	sprintf(networkStr, "%s,", networkStr);
    }

    
    
    for (eventIdx=0; eventIdx < followUp; eventIdx++) 
    {
      /* Network string */
      fprintf(omega_event, " %s ", networkStr);
            
      /* block data and live time*/
      fprintf(omega_event, " %f ", inData->blockStartTime);
      fprintf(omega_event, " %f ", inData->blockStartTime+inData->blockLen);
      fprintf(omega_event, " %f ", inData->blockLen-(inData->transientLen*inData->sampleFrequency));
      
      /* event data */
      fprintf(omega_event, " %f ", inData->blockStartTime+(clusterStruct[(eventIdx*7)+5]-clusterStruct[(eventIdx*7)+4]/2.0));	/* event time */
      fprintf(omega_event, " %f ", (clusterStruct[(eventIdx*7)+3]-clusterStruct[(eventIdx*7)+2])/2.0);				/* event frequency */
      fprintf(omega_event, " %f ", (clusterStruct[(eventIdx*7)+5]-clusterStruct[(eventIdx*7)+4]));				/* event duration */
      fprintf(omega_event, " %f ", (clusterStruct[(eventIdx*7)+3]-clusterStruct[(eventIdx*7)+2]));				/* event bandwidth */
      fprintf(omega_event, " %f ", 0.0);											/* sky position */ 
      fprintf(omega_event, " %f ", 0.0);											/* of event */

      /* Energies */
      fprintf(omega_event, " %f ", clusterStruct[(eventIdx*7)+6]);								/* Energy of event */
	
      fprintf(omega_event, "\n");
    }
  }
  else
    return -1;
   
free(networkStr);
return 0;
}
 
 /* Dump out delay_product, so we can have a look at it */

void out_delay(mxArray *plhs[], struct correlator_network_plan_fd *fdplans, int polar_value)
{
  /* Setup outgoing */
  int i,j,k;
  int max_l = 0;
  int n = 0;
  double *delayProd = NULL;		/* output matrix */
  double *delayProdi = NULL;      	/* output matrix */
  
  int nopolar_shLength, polar_shLength,arrayIdx;
  struct sh_series *bob=NULL;

  max_l = fdplans->plans[0]->delay_product->l_max;
  n = fdplans->plans[0]->delay_product->n;

  bob = sh_series_new_zero(max_l,0); 
  
  polar_shLength = sh_series_length(max_l, 1);
  nopolar_shLength = sh_series_length(max_l, 0);
  
  plhs[0] = mxCreateDoubleMatrix(nopolar_shLength,n  ,mxCOMPLEX);
  delayProd=mxGetPr(plhs[0]);
  delayProdi=mxGetPi(plhs[0]);

  /* Loop over frequencies */
  for(i=0;i<n;i++)
  {  
   sh_series_zero(bob);

   if (polar_value == 0)
   {
      sh_series_rotate(bob, &fdplans->plans[0]->delay_product->series[i], fdplans->plans[0]->rotation_plan);
   }
   else
   {
      memcpy(bob->coeff, &fdplans->plans[0]->delay_product->series[i].coeff[0], polar_shLength*sizeof(*bob->coeff));
   }

   for (k=0;k<nopolar_shLength;k++)
   {
     arrayIdx = k+i*(nopolar_shLength);
     if (polar_value < 2)
     {
      /* Output rotated delay product */
      delayProd[arrayIdx] = creal(bob->coeff[k]);
      delayProdi[arrayIdx] = cimag(bob->coeff[k]);
     }
     else
     {
      /* Output delay product as it is - unrotated */
      if (k < polar_shLength)
      {
	  delayProd[arrayIdx] = creal( fdplans->plans[0]->delay_product->series[i].coeff[k]);
	  delayProdi[arrayIdx] = cimag(fdplans->plans[0]->delay_product->series[i].coeff[k]);
      }
     }
    }
   }
}

/* Write a SH series out */

void diagnostics_dump_sh_series(const struct sh_series *series, char *name)
{
	FILE *f = fopen(name, "w");

	sh_series_print(f, series);

	fclose(f);
}

/* Copies a complex array into the output mex variable */
int set_out(mxArray *plhs[], int outNo, int rows, int cols, complex double *shcoeffs)
{
int i, j, k;
int arrayIdx;
double *rData = NULL;	/* output matrix - real part */
double *iData = NULL;   /* output matrix - imaginary part */
    
/* outNo - index value of left-hand side */
plhs[outNo] = mxCreateDoubleMatrix(rows, cols, mxCOMPLEX);
rData=mxGetPr(plhs[outNo]);
iData=mxGetPi(plhs[outNo]);		       

  for (i=0; i<rows;i++)
  {
    for(k=0;k<cols;k++)
    {  
    arrayIdx = i*cols+k;
      if (arrayIdx > (rows * cols))
      {
	fprintf(stderr, "Error, out of bounds in memory array\n");
	return -1;
      }
    rData[arrayIdx] = creal(shcoeffs[arrayIdx]);
    iData[arrayIdx] = cimag(shcoeffs[arrayIdx]);
    }
  }
  return 0;
}

/* Copies a complex array into a mex variable */
mxArray *shseries_to_mx(mxArray *outVar, int rows, int cols, const void *shcoeffs)
{
int i, j, k;
int arrayIdx;
double *rData = NULL;	/* output matrix - real part */
double *iData = NULL;   /* output matrix - imaginary part */

const struct sh_series_array *manySeries;
const struct sh_series *oneSeries;

/*mxArray *outVar;*/
    
if (cols > 1)
  manySeries = (const struct sh_series_array*) shcoeffs;
else
  oneSeries = (const struct sh_series*) shcoeffs;
  

/* create matlab variable */
/*outVar = mxCreateDoubleMatrix(rows, cols, mxCOMPLEX);*/
rData=mxGetPr(outVar);
iData=mxGetPi(outVar);		       

  for (i=0; i<rows;i++)
  {
    for(k=0;k<cols;k++)
    {  
    arrayIdx = i*cols+k;
      if (arrayIdx > (rows * cols))
      {
	fprintf(stderr, "Error, out of bounds in memory array\n");
	return NULL;
      }
      
      if (cols > 1)
      {
	rData[arrayIdx] = creal(manySeries->coeff[arrayIdx]);
	iData[arrayIdx] = cimag(manySeries->coeff[arrayIdx]);
      }
      else
      {
	rData[arrayIdx] = creal(oneSeries->coeff[arrayIdx]);
	iData[arrayIdx] = cimag(oneSeries->coeff[arrayIdx]);      
      }
    }
  }
  return outVar;
}

mxArray *c_to_mx(mxArray *outVar, int rows, int cols, const void *array, int typeDouble)
{
int i, j, k;
int arrayIdx;

double *outData = NULL;		/* output matrix */

double *dTypedArray = (double *) array;
int *iTypedArray = (int *) array;

/* create matlab variable */
/* Passed array is of type integer */
outData=mxGetPr(outVar);

  for (i=0; i<rows;i++)
  {
    for(k=0;k<cols;k++)
    {  
    arrayIdx = i*cols+k;
      if (arrayIdx > (rows * cols))
      {
	fprintf(stderr, "Error, out of bounds in memory array\n");
	return NULL;
      }
    if (typeDouble == 0)
      outData[arrayIdx] = (double) iTypedArray[arrayIdx];
    else
      outData[arrayIdx] = dTypedArray[arrayIdx];
      
    }
  }
  return outVar;
}

mxArray *cc_to_mx(mxArray *outVar, int rows, int cols, const void *array)
{
int i, j, k;
int arrayIdx;

double *routData = NULL;		/* real part of output matrix */
double *ioutData = NULL;		/* real part of output matrix */

complex double *dTypedArray = (complex double *) array;

/* create matlab variable */
/* Passed array is of type integer */
routData=mxGetPr(outVar);
ioutData=mxGetPi(outVar);

  for (i=0; i<rows;i++)
  {
    for(k=0;k<cols;k++)
    {  
    arrayIdx = i*cols+k;
      if (arrayIdx > (rows * cols))
      {
	fprintf(stderr, "Error, out of bounds in memory array\n");
	return NULL;
      }
      routData[arrayIdx] = creal(dTypedArray[arrayIdx]);
      ioutData[arrayIdx] = cimag(dTypedArray[arrayIdx]);
      
    }
  }
  return outVar;
}


/* 'Wrapper' function to output data products out to a .mat fileName.
 * Keeps everything nice and tidy and is easier to debug
 * varType can be:
 *	=> 0 - Integer
 *	=> 1 - Double
 *	=> 2 - Complex SH series
 *	=> 3 - Complex array
 */
void product_output(char *fileName, char *varName,void *arrayPtr, int xSize, int ySize ,int varType)
{ 
  mxArray *outVar = NULL;
  if (miscDebugLevel == 1)
    mexPrintf( "Writing output to matlab file\n");
 
  /* Output the time-frequency map */
  if (miscDebugLevel == 1)
    mexPrintf( "Converting %s C struct to matlab variable\n", varName);
  
  if (varType < 2)
  {
    outVar = mxCreateDoubleMatrix(xSize,ySize, mxREAL);
    c_to_mx(outVar, xSize,ySize, arrayPtr, varType);
  }
  else
  {
    outVar = mxCreateDoubleMatrix(xSize,ySize, mxCOMPLEX);
    if (varType == 2)
    	shseries_to_mx(outVar, xSize, ySize, (const*) arrayPtr);
    else
	cc_to_mx(outVar, xSize,ySize, arrayPtr);
  }
  
  if (miscDebugLevel == 1)
    mexPrintf( "Writing out %s to %s\n", varName, fileName);
  
  write_to_mat(fileName, varName, outVar);
  mxDestroyArray(outVar);   
 
}


/* This is the function loop that actually does the work! 
 * Given the data and a set of parameters, it generates the correlations (cross and auto) between all of the
 * data streams for each FFTLen, and reduces the 2 dimensional data accordingly (i.e. sum over coefficients or frequency)
 * The element lenDiv gives the divisor for FFTLen, so a value of 2 would half the analysis length.
 * Used to make multi-resolution time-frequency maps.
 */
 
int cross_loop(struct sh_series_array **each_ts_map,struct sh_series_array *ts_map, struct sh_series *sky , struct analysisData *inData, char *analysisType, struct correlator_network_plan_fd *fdplans, double *endSeries[],struct detector_response *responseSH,  int *baseDectIdx, int *restrictedIdx, int lenDiv)
{
mexEvalString("drawnow;"); /* Make sure all text has been written out */

/* Actual work horse - calculates the correlation between each pair of detectors, based on the analysis type 
 * passed in. 
 */
  
int i,j,k;

struct sh_series_array *tf_map = NULL; 	/* time-frequency map */

/*struct sh_series_array *tfs_map[5];*/	/* time-freq-SH coefficient map */

 /*struct sh_series *sky = NULL;	*/	/* 2d sky power */

struct sh_series_array *eachSky = NULL;		/* 2d sky power */

int timeBlock = 0;
double tempPower = 0; 
double maxPower = 0;
int segmentIdx = 0;
int blockIdx = 0;
int instrIdx = 0;
	
/* Calculate the number of sections in the analysis for 50% overlap */
int FFTLen = inData->FFTLen / pow(2, lenDiv);
int nSections = inData->aLen[lenDiv];

mexPrintf("\n Test analysis length divisor\n\tSegments: %d \n\tFFTLen: %d\n\n", nSections, FFTLen);

/* Alter internal debug level based on user input */
miscDebugLevel = inData->debugLevel;

if (ts_map->l_max != sky->l_max)
  return 0;

int shLength = (int) sh_series_length(sky->l_max, sky->polar);
mexPrintf( "\t  shLength is : %d\n", shLength);

/* Anaylsis type */
mexPrintf( "\tPerforming %s analysis, between %d and %d Hz\n",analysisType, inData->freqRange[0], inData->freqRange[1]);

/*
 * Need to hold onto each of the correlations between detectors
 *  so we require a 3d array to hold the info.
 */
if (strcmp(analysisType, "restrictedUnsum") == 0)
{
  /* Set up variables for unsummed version */
 for (i = 0;i <fdplans->baselines->n_baselines; i++)
 {
   each_ts_map[i] = sh_series_array_new(nSections, ts_map->l_max, ts_map->polar);
   if (each_ts_map[i] == NULL)
   {
     mexPrintf( "Cannot allocate memory for each_ts_map\n");
     for (j = 0;j < i; j--) {
	sh_series_array_free(each_ts_map[j]);
     }
     return -1;
   }
 }
}

 /*******************************************************
 * Check restricted index for boundary and length error *
 *******************************************************/
int resFix = 0;
mexPrintf( "\t Checking restricted index for boundary errors\n");

if (restrictedIdx[0] < 0)
{
  restrictedIdx[1] += abs(restrictedIdx[0]);
  restrictedIdx[0] = 0;
  resFix = 1;
}
  
if (restrictedIdx[1] > inData->lenDataStreams)
{
  restrictedIdx[0] -= (restrictedIdx[1] - inData->lenDataStreams);
  restrictedIdx[1] = inData->lenDataStreams;
  resFix = 1;
}

if ((restrictedIdx[1] - restrictedIdx[0]) < FFTLen)
{
  int diff = (inData->FFTLen-(restrictedIdx[1] - restrictedIdx[0]));
  if ((restrictedIdx[0]-diff) > 0)
    restrictedIdx[0] -= diff ;
    
  if ((restrictedIdx[1]+diff) < inData->lenDataStreams)
    restrictedIdx[1] += diff;

  resFix = 1;
}

if (resFix == 1)
   mexPrintf( "\t Restricted boundaries fixed\n");
else
   mexPrintf( "\t Restricted boundaries fine\n"); 	

resFix = restrictedIdx[1]-restrictedIdx[0];
mexPrintf( "\tTime series samples analysed: %d->%d (len: %d)\n",restrictedIdx[0], restrictedIdx[1], resFix);

/* Fourier data structures */
double *tseries=NULL; 
complex double *fseries[inData->nInstr];
fftw_plan fftplans[inData->nInstr];

/* Create window */
double *window = hann_window(inData->FFTLen);

/* Create array to hold an analysis length of data, and zero it out */
tseries = malloc (inData->FFTLen * sizeof(*tseries));

for (instrIdx = 0; instrIdx < inData->nInstr; instrIdx++)
{
  fseries[instrIdx] = malloc (inData->FFTLen*sizeof(**fseries));

  /* Create the FFT transform plans with empty data, populate before executing */ 
  fftplans[instrIdx] = correlator_tseries_to_fseries_plan(tseries, fseries[instrIdx], inData->FFTLen);
 
  if ((fseries[instrIdx] == NULL) || (fftplans[instrIdx] == NULL))
  {
    fprintf( stderr, "MEM ERROR: Unable to allocate memory for FFT structures\n");
    return -1;
  }
}

/* Create the structure to hold the unsummed tf map */
if (strcmp(analysisType, "tfmap") == 0)
  tf_map = sh_series_array_new(fdplans->plans[0]->delay_product->n, fdplans->plans[0]->power_2d->l_max, fdplans->plans[0]->power_2d->polar);


if (strcmp(analysisType, "tfshmap") == 0)
  tf_map = sh_series_array_new(fdplans->plans[0]->delay_product->n, inData->sky_l_max, 0);

complex double *responseGrid;
complex double *skyGrid;
int dectIdx[inData->nInstr];

/* FSH Transformation variables */
int bw, size;
memset(tseries, 0,  inData->FFTLen*sizeof(*tseries));


  /* Main loop, calculates the correlation for each segment of data */ 
  mexPrintf( "\tStarting mainloop\n");
  for(blockIdx=restrictedIdx[0]; blockIdx+FFTLen <= restrictedIdx[1]; blockIdx+=FFTLen/2)
  {  
    /* Calculate shifted index value */
    dectIdx[0] = blockIdx+baseDectIdx[0];
    dectIdx[1] = blockIdx+baseDectIdx[1];    
    dectIdx[2] = blockIdx+baseDectIdx[2];
   
    /* Dump out data steam indexes */
    if (inData->debugLevel == 1)
      if (timeBlock==0)
      {
	mexPrintf( "\t  Inst1  |  Instr2  |  Instr3  \n");
	mexPrintf( "\t    %d    |    %d    |    %d  \n", dectIdx[0], dectIdx[1]%(inData->lenDataStreams-1), dectIdx[2]%(inData->lenDataStreams-1));
      }
      else /*if (blockIdx < (FFTLen*3))*/    
	mexPrintf( "\t    %d    |    %d    |    %d  \n", dectIdx[0], dectIdx[1]%(inData->lenDataStreams-1), dectIdx[2]%(inData->lenDataStreams-1));

    /* If blockIdx is close to end of data buffer, need to use 'endBuffer' which holds 
	the wrapped around data of end of buffer - beginning of buffer */  
    
    for (instrIdx=0;instrIdx<inData->nInstr;instrIdx++)
      if (((inData->lenDataStreams-dectIdx[instrIdx]%(inData->lenDataStreams-1)) < FFTLen ) && (instrIdx != 0))
      {
	if (inData->debugLevel == 1)
	{
	  mexPrintf( "\t (%d-%d, %d-%d - LEN: %d) ",dectIdx[instrIdx], dectIdx[instrIdx]+inData->FFTLen/2, 0, inData->FFTLen/2-1, (inData->FFTLen/2)+((dectIdx[instrIdx]+inData->FFTLen)%inData->lenDataStreams));
	  mexPrintf( "\t Using endSeries for Det%d\n",instrIdx);
	}
	  
	memcpy(tseries, &endSeries[instrIdx-1][0], inData->FFTLen*sizeof(*tseries));
	  
	if (inData->debugLevel == 1)
	   mexPrintf( "\ttseries: %g->%g, %g->%g, %g->%g \n", tseries[0], endSeries[instrIdx-1][0],tseries[1], endSeries[instrIdx-1][1],tseries[2], endSeries[instrIdx-1][2]);
	 
	 correlator_tseries_to_fseries(tseries, fseries[instrIdx], inData->FFTLen, fftplans[instrIdx]);      
      }
      else
      {

	/* Need to take into account that a divLen greater than 1 means that the time series data will take up less than
	 * inData->FFTLen, and alter the start sample accordingly (i.e. inData->FFTLen/2 - FFTLen/2)
	 */
/*	
	memcpy(&tseries[(inData->FFTLen/2 - FFTLen/2)], &inData->series[instrIdx][ dectIdx[instrIdx]%(inData->lenDataStreams-1)], /*inData->* FFTLen*sizeof(*tseries));

	for (i=0;i<inData->FFTLen;i++)
*/
	for (i=0;i<FFTLen;i++)	
	  tseries[i] = window[i] * inData->series[instrIdx][i+(dectIdx[instrIdx]%(inData->lenDataStreams-1))];
/**/	 
	correlator_tseries_to_fseries(tseries, fseries[instrIdx], inData->FFTLen, fftplans[instrIdx]);      
      }
      
  
    /* Compute angular distribution of cross power */
    if (strcmp(analysisType, "normal") == 0)
    {      
      /* compute integrated cross power */
      /* time domain 
      int time_series_length = 1024;

      correlator_network_integrate_power_td(sky, tseries, time_series_length, NULL, fdplans);*/

      correlator_allnetwork_integrate_power_fd(sky, fseries, fdplans); 
     
      /* Also, unsummed correlator for stats 
      unsummed_correlator_network_integrate_power_fd(eachSky, fseries, fdplans);
      */      
      memcpy(ts_map->series[timeBlock].coeff, sky->coeff, shLength*sizeof(*sky->coeff));
      
      if (each_ts_map)
	for (instrIdx=0;instrIdx < fdplans->baselines->n_baselines; instrIdx++)	
	{
	  sh_series_zero(&each_ts_map[instrIdx]->series[timeBlock]);
	  sh_series_add_into(&each_ts_map[instrIdx]->series[timeBlock], 1.0, fdplans->plans[instrIdx]->power_2d);
	}
    } 
    else if ((strcmp(analysisType, "restricted") == 0) || (strcmp(analysisType, "restrictedUnsum") == 0))
    {
      /* Call correlator with frequency range of interest - summed*/
      correlator_network_integrate_power_fd_range(sky, fseries, fdplans,inData->freqRange);
      memcpy(ts_map->series[timeBlock].coeff, sky->coeff, shLength*sizeof(*sky->coeff));

      for (instrIdx=0;instrIdx < fdplans->baselines->n_baselines; instrIdx++)	
      {
	sh_series_zero(&each_ts_map[instrIdx]->series[timeBlock]);
	sh_series_add_into(&each_ts_map[instrIdx]->series[timeBlock], 1.0, fdplans->plans[instrIdx]->power_2d);
      }
      
      /*
	memcpy(each_ts_map[instrIdx]->series[timeBlock].coeff, fdplans->plans[instrIdx]->power_2d->coeff, shLength*sizeof(*fdplans->plans[instrIdx]->power_2d->coeff));
	*/
    }
      else if (strcmp(analysisType, "fast_tfmap")==0)
    {
	SHsum_correlator_network_integrate_power_fd(sky, fseries, fdplans, inData->nPSpectrum);
	memcpy(ts_map->series[timeBlock].coeff, sky->coeff, shLength*sizeof(*sky->coeff));     
    }
      else if (strcmp(analysisType, "tfshmap")==0)
    {
	/* generate a time-frequency-spherical harmonic map - output 3d array */
	mexPrintf("Segment %d\n%", timeBlock);

        /* First return a 2d array - SH expansion for each frequency */
        decompose_correlator_network_integrate_power_fd(tf_map, fseries, fdplans);

        int nFreqs  = tf_map->n;
        int nCoeffs = sh_series_length(tf_map->l_max, tf_map->polar);

	mexPrintf("\t  Decomposed into:\n");
	mexPrintf("\t    nFreqs: [%d]\n", nFreqs);
	mexPrintf("\t	 nCoeffs: [%d]\n", nCoeffs);

        /* allocate memory for restricted map name */
        char *tfsmapName, *tfsmapFile;
        tfsmapName = (char *) malloc(40*sizeof(char));
        tfsmapFile = (char *) malloc(40*sizeof(char));

	if (!tfsmapName  || !tfsmapFile)
	{
		fprintf( stderr, "\tMEM ERROR: cross_loop failed to allocate memory\n");
		return -1;
	}


        sprintf(tfsmapName, "tfsmapSeg%d", (timeBlock+1));
        sprintf(tfsmapFile, "tfsmapSeg-%d.mat", (timeBlock+1));

        product_output(tfsmapFile, tfsmapName, tf_map, nCoeffs, nFreqs ,2);
        free(tfsmapName);
        free(tfsmapFile);
/*
 *         for (i=0; i<nFreqs; i++)
 *                 {
 *                           ts_map->series[timeBlock].coeff[i] = (tf_map->series[i].coeff[0]*conj(tf_map->series[i].coeff[0]));
 *                                     for (j=1; j<nCoeffs; j++)
 *                                                 ts_map->series[timeBlock].coeff[i] += (tf_map->series[i].coeff[j]*conj(tf_map->series[i].coeff[j]));
 *                                                         }
 *                                                         */


    }
      else if (strcmp(analysisType, "full") == 0)
    {
      /* Computes the full analysis for the time block.
       *  Consists of the following procedures:
       *    1) Generate tfmap, cluster and identify trigger candidates,
       *    2) Use trigger info to produce restricted tsmaps,
       *    3) Sum and transform to get skymap
       *    4) Find the bright patch of sky and report on it
       */
      
      /* 1) Generate time-frequency pixels and add them into the tfmap*/
      SHsum_correlator_network_integrate_power_fd(sky, fseries, fdplans);
      memcpy(ts_map->series[timeBlock].coeff, sky->coeff, shLength*sizeof(*sky->coeff));     

      /* Calcualte cross-power for each detector pair */  
      unsummed_correlator_network_integrate_power_fd(eachSky, fseries, fdplans);
      
      for(instrIdx = 0; instrIdx < fdplans->baselines->n_baselines ; instrIdx++)
	memcpy(each_ts_map[instrIdx]->series[timeBlock].coeff, eachSky->series[instrIdx].coeff, shLength*sizeof(*eachSky->series[instrIdx].coeff));
      
      /* Generate time-frequency map */
      SHsum_correlator_network_integrate_power_fd(sky, fseries, fdplans);
      memcpy(ts_map->series[timeBlock].coeff, sky->coeff, shLength*sizeof(*sky->coeff));     
          
    }
      else if (strcmp(analysisType, "tfmap") == 0)
    {
	/* generate a time-frequency map - calculate power in SH coeffs for each frequency and
	  use that as the level of power for each pixel */
      
	/* Returns a 2d array - SH expansion for each frequency */ 
	decompose_correlator_network_integrate_power_fd(tf_map, fseries, fdplans);
      
	/* The above SH expansion for the cross-correlation needs to be modified using the
	 * antenna response, so that we can form our statisitic 
	 */

	/* Experimental - will probably break horrendously
	sky = generate_full_tfmap(tf_map,fseries, fdplans, responseSH);
	*/
	
	/* Now, we need to calculate the power in each frequency bin for this section */
	/* I'm sure there is a better way to do this */
	/* --- Rearrange the correlator code --- */
	
	/* TESTING */
	int nFreqs  = tf_map->n;
	int nCoeffs = sh_series_length(tf_map->l_max, tf_map->polar);
		
	/* Long complicate pointer way! 
	complex double *theOutput = ts_map->series[timeBlock].coeff;
	complex double *theResults;
	complex double *theCoeffs;
	
	for (i=0; i<nFreqs; i++)
	{
	  theResults = tf_map->series[i].coeff;
	  *theOutput = 0;
	  for (j=0; j<nCoeffs; j++)
	  {
	    theCoeffs=theResults;
	    *theOutput += *theCoeffs * *theCoeffs;
	    *theResults++;

	  }
	  *theOutput++;
	}
	*/
	
	/* Easy loopy way 

	for (i=0; i<nFreqs; i++)
	{
	  ts_map->series[timeBlock].coeff[i] = (tf_map->series[i].coeff[0]*conj(tf_map->series[i].coeff[0]));
	  for (j=1; j<nCoeffs; j++)
	    ts_map->series[timeBlock].coeff[i] += (tf_map->series[i].coeff[j]*conj(tf_map->series[i].coeff[j]));
	}
	  ts_map->series[timeBlock].coeff[i] += cabs(tf_map->series[i].coeff[j]*conj(tf_map->series[i].coeff[j]));
	  memcpy(sky->coeff, &ts_map->series[timeBlock].coeff[0], tf_map->stride*sizeof(*sky->coeff));
	*/
      }/*	
      else if (strcmp(analysisType, "tfsmap") == 0)
    {
	/* generate a time-frequency-spherical harmonic map - output 3d array */
      
	/* Returns a 2d array - SH coeffs for each frequency  
	decompose_correlator_network_integrate_power_fd(tfs_map[timeBlock], fseries, fdplans);
	if (timeBlock == 2)
	{
	  for (i=0;i<tfs_map[2]->n;i++)
	      memcpy(ts_map->series[i].coeff, tfs_map[2]->series[i].coeff, shLength*sizeof(*tfs_map[2]->coeff));
	}
      
    }	*/

    /* Modification for using a non-azimuthally symmetric delay_product */
    if (inData->antennaResponse == 2)
      /*mod_*/correlator_network_integrate_power_fd(sky, fseries, fdplans);
  
      /* Correct due to earth's rotation using gpstime offset  
    
      aBob = XLALGPSAdd(bob, (i/4.)*inData->deltaT);
      gmst = XLALGreenwichSiderealTime(aBob,(REAL8)0.0); 
      sh_series_rotate_z(sky, sky, gmst);
      */
    
      if ((strcmp(analysisType, "tfmap") != 0) || (strcmp(analysisType, "tfsmap") != 0))
      {
	/* Look for signal */
	if (inData->debugLevel == 1)
	  mexPrintf("\tBlock %i: \n",timeBlock);
	
	tempPower = identify_candidate(sky, inData->deltaT);
	if (tempPower > maxPower)
	{
	  maxPower = tempPower;
	  segmentIdx = timeBlock;
	    
	  if (inData->debugLevel > 1)
	  {
	    /* Dump each of our correlations */
	    product_output("tsdata.mat", "HL", fdplans->plans[0]->power_2d, sh_series_length(fdplans->plans[0]->power_2d->l_max, fdplans->plans[0]->power_2d->polar),1, 2);
	    product_output("tsdata.mat", "HV", fdplans->plans[1]->power_2d, sh_series_length(fdplans->plans[1]->power_2d->l_max, fdplans->plans[1]->power_2d->polar),1, 2);
	    product_output("tsdata.mat", "LV", fdplans->plans[2]->power_2d, sh_series_length(fdplans->plans[2]->power_2d->l_max, fdplans->plans[2]->power_2d->polar),1, 2);
	    product_output("tsdata.mat", "HH", fdplans->plans[3]->power_2d, sh_series_length(fdplans->plans[3]->power_2d->l_max, fdplans->plans[3]->power_2d->polar),1, 2);
	    product_output("tsdata.mat", "LL", fdplans->plans[4]->power_2d, sh_series_length(fdplans->plans[4]->power_2d->l_max, fdplans->plans[4]->power_2d->polar),1, 2);
	    product_output("tsdata.mat", "VV", fdplans->plans[5]->power_2d, sh_series_length(fdplans->plans[5]->power_2d->l_max, fdplans->plans[5]->power_2d->polar),1, 2);
	  }
	  
	}
      }
/*    mexPrintf("Analysed block %d\n", timeBlock);*/
    timeBlock++;
  }


  /* If a restricted analysis, don't output power (no need), and don't reduce number of time blocks */
  segmentIdx = timeBlock;
  if (strcmp(analysisType, "restricted") != 0)
  {
    mexPrintf( "\tMax power was %g, in segment %i (%f seconds)\n", maxPower, segmentIdx,((double)segmentIdx/(double)timeBlock)*inData->blockLen);
    timeBlock--;
  }
  
  if (inData->debugLevel == 1)
    mexPrintf( "\tFreeing function memory\n");
  
  for(instrIdx = 0; instrIdx < inData->nInstr ; instrIdx++) 
  {
    free(fseries[instrIdx]);
    fftw_destroy_plan(fftplans[instrIdx]);
  }

  free (window);
  free (tseries);

  if (strcmp(analysisType, "tfmap") == 0)
    sh_series_array_free(tf_map);

return segmentIdx;
}



void *multiTF_analysis(struct analysisData *inData, int *lenScales, int nScales, struct correlator_network_baselines *baselines)
{
/* lenScales is sample length of each tfmap to make */
int i, scaleNo;
int nSegments;
int shLength;
int segmentIdx;
int instrIdx;
int eOrder[4];
int baseDectIdx[3]={0,0,0};
int blockIdx;
int timeBlock;
char *cMulti;
char *pathName;
char *matFileName;

double *tseries=NULL; 
complex double *fseries[inData->nInstr];
fftw_plan fftplans[inData->nInstr];


struct correlator_network_plan_fd *fdplans = NULL;
struct sh_series_array *tf_map = NULL; 	/* time-SH coefficient map */
struct sh_series *sky = NULL;			/* 2d sky power */


/* Very low resolution spherical harmonic map - for speed */
eOrder[0]=4;eOrder[1]=4;eOrder[2]=8;eOrder[3]=8;
eOrder[0]=-1;eOrder[1]=-1;eOrder[2]=-1;eOrder[3]=-1;

/* Path file name */
pathName = (char *)malloc(60*sizeof(char));
sprintf(pathName, "SPHRAD_TFMAP_DUMP");

matFileName = make_filename(inData->outputDirectory, inData->detName, pathName, (int)round(inData->blockStartTime), (int) round(inData->blockLen), ".mat");
mexPrintf( "\nFilename for matlab products: %s\n",matFileName);

/* Holds matlab variable name for this iteration */
cMulti = (char *) malloc(40*sizeof(char));

if (!pathName || !cMulti)
{	
	fprintf(stderr, "\tMEM ERROR: In multiTF_analysis\n");
	return NULL;
} 

for (scaleNo=0; scaleNo< nScales; scaleNo++)
{
  mexPrintf("\tCalculating time-frequency map for %d\n", lenScales[scaleNo]);
  /* Generate fdplan */
  if (eOrder[0] > 0)
	fdplans = user_correlator_network_plan_fd_new(baselines, lenScales[scaleNo], inData->deltaT, eOrder);
  else
	fdplans = correlator_network_plan_fd_new(baselines, lenScales[scaleNo], inData->deltaT);

    if (!fdplans || !fdplans->plans)
    {
      mexPrintf( "Failed to generate delay_matrix\n");
      return NULL;
    }

    /* Number of segments for this analysis length */
    nSegments = floor( ((double) inData->lenDataStreams/lenScales[scaleNo]) + ((double)inData->lenDataStreams-((double)(lenScales[scaleNo])/2.))/lenScales[scaleNo]);

    inData->nSections = nSegments;
    inData->FFTLen = lenScales[scaleNo];
    inData->aLen[0] = lenScales[scaleNo];
    timeBlock = 0;
    
    /* Create window */
    double *window = hann_window(inData->FFTLen);

    /* Create array to hold SH coefficient power by frequency*/
    tf_map = sh_series_array_new(nSegments, lenScales[scaleNo] - 1, 1);
    
    /* Create 'sky' */
    sky = sh_series_new_zero(tf_map->l_max, 1 );
    shLength = sh_series_length(sky->l_max, sky->polar);
	
    /* Generate tf-map for this analysis length */
    /* Create array to hold an analysis length of data, and zero it out */
    tseries = malloc (lenScales[scaleNo] * sizeof(*tseries));

    for (instrIdx = 0; instrIdx < inData->nInstr; instrIdx++)
    {
      fseries[instrIdx] = malloc (lenScales[scaleNo]*sizeof(**fseries));

      /* Create the FFT transform plans with empty data, populate before executing */ 
      fftplans[instrIdx] = correlator_tseries_to_fseries_plan(tseries, fseries[instrIdx],lenScales[scaleNo]);
    
      if ((fseries[instrIdx] == NULL) || (fftplans[instrIdx] == NULL))
      {
	mexPrintf( "MEM ERROR: Unable to allocate memory for FFT structures\n");
	return NULL;
      }
    }
 
    /* Main loop, calculates the correlation for each segment of data */ 
    mexPrintf( "\tLooping over segments\n");
    for(blockIdx=0; blockIdx+lenScales[scaleNo] <= inData->lenDataStreams; blockIdx+=lenScales[scaleNo]/2)
    {  
	/* Copy data */	
	for (instrIdx=0;instrIdx<inData->nInstr;instrIdx++)
	{/*
	    memcpy(&tseries[0], &inData->series[instrIdx][blockIdx], lenScales[i]*sizeof(*tseries));
    */
	    for (i=0;i<lenScales[scaleNo];i++)
	      tseries[i] = /*window[i] * */inData->series[instrIdx][i+blockIdx];
    	 
	    correlator_tseries_to_fseries(tseries, fseries[instrIdx],lenScales[scaleNo], fftplans[instrIdx]);      
	}

        /* generate strip */
	SHsum_correlator_network_integrate_power_fd(sky, fseries, fdplans, inData->nPSpectrum);
	memcpy(tf_map->series[timeBlock].coeff, sky->coeff, lenScales[scaleNo]*sizeof(*sky->coeff));     
 
    timeBlock++;
    }
    
    /* Cluster pixels on tfmap */
    mexPrintf( "\tArray size: (%d, %d)\n\n",tf_map->n, sh_series_length(tf_map->l_max, tf_map->polar));

    /* Create the name for the output structure */
    sprintf(cMulti, "TFMAP%i",lenScales[scaleNo]);
    product_output(matFileName, cMulti, tf_map, sh_series_length(tf_map->l_max, tf_map->polar), tf_map->n ,2);    

    /* Free everything needed for the next loop */
    for(instrIdx = 0; instrIdx < inData->nInstr ; instrIdx++) 
    {
      free(fseries[instrIdx]);
      fftw_destroy_plan(fftplans[instrIdx]);
    }

    free (window);
    free (tseries);
    
    user_correlator_network_plan_fd_free(fdplans);
    sh_series_array_free(tf_map);
    sh_series_free(sky);
    
}


}

void *multiTF_constant_analysis(struct analysisData *inData, int *lenScales, int nScales, struct correlator_network_baselines *baselines)
{
/* lenScales is sample length of each tfmap to make */
int i, scaleNo;
int nSegments;
int shLength;
int segmentIdx;
int instrIdx;
int eOrder[4];
int baseDectIdx[3]={0,0,0};
int blockIdx;
int timeBlock;
char *cMulti;
char *pathName;
char *matFileName;

double *tseries=NULL; 
complex double *fseries[inData->nInstr];
fftw_plan fftplans[inData->nInstr];

/* Set length of FFT to generate */
inData->FFTLen = lenScales[0];

/* Create window */
double *window = hann_window(inData->FFTLen);

struct correlator_network_plan_fd *fdplans = NULL;
struct sh_series_array *tf_map = NULL; 	/* time-SH coefficient map */
struct sh_series *sky = NULL;			/* 2d sky power */

/* Very low resolution spherical harmonic map - for speed */
/*eOrder[0]=8;eOrder[1]=8;eOrder[2]=18;eOrder[3]=18;
*/

eOrder[0]=-1;eOrder[1]=-1;eOrder[2]=-1;eOrder[3]=-1;


/* Path file name */
pathName = (char *)malloc(60*sizeof(char));
sprintf(pathName, "SPHRAD_TFMAP_C_DUMP");

matFileName = make_filename(inData->outputDirectory, inData->detName, pathName, (int)round(inData->blockStartTime), (int) round(inData->blockLen), ".mat");
mexPrintf( "\nFilename for matlab products: %s\n",matFileName);

/* Holds matlab variable name for this iteration */
cMulti = (char *) malloc(40*sizeof(char));

if (!pathName || !cMulti || !matFileName )
{
        fprintf(stderr, "\tMEM ERROR: In multiTF_analysis\n");
        return NULL;
}

/* Generate fdplan */
if (eOrder[0] > 0)
	fdplans = user_correlator_network_plan_fd_new(baselines, lenScales[0], inData->deltaT, eOrder);
else
	fdplans = correlator_network_plan_fd_new(baselines, lenScales[0], inData->deltaT);

if (!fdplans || !fdplans->plans)
{
    mexPrintf( "Failed to generate delay_matrix\n");
    return NULL;
}


/* Generate tf-map for this analysis length */
/* Create array to hold an analysis length of data, and zero it out */
tseries = malloc (lenScales[0] * sizeof(*tseries));

for (instrIdx = 0; instrIdx < inData->nInstr; instrIdx++)
{
  fseries[instrIdx] = malloc (lenScales[0]*sizeof(**fseries));

  /* Create the FFT transform plans with empty data, populate before executing */ 
  fftplans[instrIdx] = correlator_tseries_to_fseries_plan(tseries, fseries[instrIdx],lenScales[0]);
    
  if ((fseries[instrIdx] == NULL) || (fftplans[instrIdx] == NULL))
  {
    mexPrintf( "MEM ERROR: Unable to allocate memory for FFT structures\n");
    return NULL;
  }
}
 
inData->aLen[0] = lenScales[0];
 
for (scaleNo=0; scaleNo< nScales; scaleNo++)
{
  mexPrintf("\tCalculating time-frequency map for %d\n", lenScales[scaleNo]);

    /* Number of segments for this analysis length */
    nSegments = floor( ((double) inData->lenDataStreams/lenScales[scaleNo]) + ((double)inData->lenDataStreams-((double)(lenScales[scaleNo])/2.))/lenScales[scaleNo]);

    inData->nSections = nSegments;

/*    inData->aLen[0] = lenScales[scaleNo];
*/    timeBlock = 0;
    
    /* Create array to hold SH coefficient power by frequency*/
    tf_map = sh_series_array_new(nSegments, lenScales[0] - 1, 1);
    
    /* Create 'sky' */
    sky = sh_series_new_zero(tf_map->l_max, 1 );
    shLength = sh_series_length(sky->l_max, sky->polar);

    /* Main loop, calculates the correlation for each segment of data */ 
    mexPrintf( "\tLooping over segments\n");
    for(blockIdx=0; blockIdx+lenScales[scaleNo] <= inData->lenDataStreams; blockIdx+=lenScales[scaleNo]/2)
    {  
	/* Copy data */	
	for (instrIdx=0;instrIdx<inData->nInstr;instrIdx++)
	{
	    for (i=0; i<lenScales[0]; i++)
		tseries[i] = 0;

	    memcpy (&tseries[(lenScales[0]-lenScales[scaleNo])/2], &inData->series[instrIdx][blockIdx], lenScales[scaleNo]*sizeof(*tseries));
	    correlator_tseries_to_fseries(tseries, fseries[instrIdx],lenScales[0], fftplans[instrIdx]);      
	}

        /* generate strip */
	SHsum_correlator_network_integrate_power_fd(sky, fseries, fdplans, inData->nPSpectrum);
	memcpy(tf_map->series[timeBlock].coeff, sky->coeff, lenScales[0]*sizeof(*sky->coeff));     
 
    timeBlock++;
    }
    
    /* Cluster pixels on tfmap */
    mexPrintf( "\tArray size: (%d, %d)\n\n",tf_map->n, sh_series_length(tf_map->l_max, tf_map->polar));

    /* Create the name for the output structure */
    sprintf(cMulti, "TFMAPC%i",lenScales[scaleNo]);
    product_output(matFileName, cMulti, tf_map, sh_series_length(tf_map->l_max, tf_map->polar), tf_map->n ,2);    

    /* Free everything needed for the next loop */
    sh_series_array_free(tf_map);
    sh_series_free(sky);
}

/* Free data products */
for(instrIdx = 0; instrIdx < inData->nInstr ; instrIdx++) 
{
  free(fseries[instrIdx]);
  fftw_destroy_plan(fftplans[instrIdx]);
}

free (window);
free (tseries);
    
user_correlator_network_plan_fd_free(fdplans);
 

}

