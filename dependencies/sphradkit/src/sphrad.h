#define MAX_DETECTORS  4

#ifndef __SPHRAD_MULTI_H__
#define __SPHRAD_MULTI_H__

 
struct analysisData {
      int nInstr;			/* Number of instruments */
      int nDataStreams;			/* Number of data streams */
      int lenDataStreams;		/* Length of data stream (samples)*/
      double blockStartTime;		/* GPS start time of analysis */
      double blockStopTime;		/* GPS end time of analysis */
      double segmentStartTime;		/* GPS time of start of segment */
      double segmentDuration;		/* Duration of segment */
      double transientLen;		/* Length of data transients (s) */
      double blockLen;	 		/* Length of data block (s) */
      double fourierLen;		/* Length of fourier transforms (seconds) */
      int nSections;			/* Number of sections in analysis (50% overlap) */
      int freqRange[2];			/* If set, used for restricted search */
      double blurring[2];		/* Blurring radii in pixels for optional Gaussian blurring [sig,bck] */ 
      double clusterThresholds[2];	/* Thresholds to use in the cluster algorithm [core, halo]*/ 
      int nRegions;			/* Number of regions to use in region based thresholding */
      int regionDirection;		/* Region span direction, 0 - columns, 1 - rows, 2 - both */
      int codeVersion;			/* Which clustering codebase to use */
      int pixelDistance;		/* Maximum distance for pixels to be */
      double skyPos[2];			/* Sky location to analyse for targeted search */
      double *skyPos2;
      int skyPrior;			/* Flag to specify whether we have a sky map prior 
					 * >=1  - multiple sky positions (1 per injection) - sky position stretches to half way to the next injection
					 *  0   - single sky position
					 *  -1  - sky prior
					 */
      int testLocalisation;		/* Flag to specify whether we are testing a plurality of sky pointing statistics:
					 *  0   - no testing 
					 *  >=1	- as in the skyPrior variable, this defines how many sky locations need to be check for this data block
					 */
      int eOrder[4];			/* Order of harmonic expansion */
      int FFTLen;			/* Length of fourier transforms (samples) */
      int followUps;			/* Number of clusters to follow up */
      int sampleFrequency;		/* Sample frequency (Hz) */
      double deltaT;			/* time step between samples (s) */
      double freqRatio;			/* If we have an FFTLen that is not equal to the sample frequency, then this is the conversion rate */
      int antennaResponse;		/* Super variable ! */
      int tfScale;			/* Scale factors for multi-resolution time-frequency maps 1=1, 2=1,1/2, 3=1,1/2, 1/4 etc */
      int *aLen;			/* Analysis length  - Combination  of FFTLen and tfScale */
      int sky_l_max;			/* l-value of sky */
      int debugLevel;			/* User defined debug level for output verbosity */
      char *outputDirectory;		/* Directory to output data products to */
      char *analysisType;		/* Type of analysis to perform */
      char *detName[MAX_DETECTORS];	/* Detector names */
      double *series[MAX_DETECTORS];	/* Detector time series */
      double *nPSpectrum[MAX_DETECTORS];/* Noise power spectrum for each detector */
      double *timeLags;			/* Array of time lags to shift by */
      int nTimeLags;			/* Total number of time lags */
};

struct eventStruct {
    double time;			/* event time */
    double frequency;			/* event frequency */
    double duration;			/* event duration */
    double bandwidth;			/* event bandwidth */
    double theta;			/* sky position */ 
    double phi;				/* of event */
    double clusterEnergy;		/* Energy of clustered event */
    int    numPixels;			/* Number of pixels in cluster */
    double shE;				/* Coherent energy in spherical harmonics */
    double shI;				/* Incoherent part of energy */
    double crossE;			/* Projection likelihoods */
    double crossI;
    double plusE;
    double plusI;
    double standardE;
    double standardI;
    double nullE;
    double nullI;
};


#endif  /* __SPHRAD_MULTI_H__ */
