/*
 * Copyright (C) 2010  Mark Edwards
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* Number of elements present in the cluster structure */
#define NUM_CLUSTER_ELEMENTS 10

#ifndef __SPHRAD_CLUST_H__
#define __SPHRAD_CLUST_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#include <fftw3.h>

#include "mex.h"

/* Linked list to hold boundary values */
/* In the scheme of things, col = time, row =freq */
struct boundaryStruct
{
  int clusterNumber;
  int numPixels;
  int minX;				/* Min col */
  int maxX;				/* Max col */
  int minY;				/* Min row */
  int maxY;				/* Max row */
  double clusterPower;
  int maxPixelX;			/* Central col */
  int maxPixelY;  			/* Central row */
  double maxPixelPower;
  struct boundaryStruct *nextCluster;
};

struct clusterStruct
{
  double pixel_power;
  int pos[2];
  int clusterNumber;
};


int doublecmp (const void* , const void* );
double prcValue(double *, double ,int, int );

/* Region-based thresholding */
double **calc_thresholds( double *, int, int, int *, int, double *, int);
int return_thresholdIdx(int , int , int *, int , int);
int *set_boundaries(int , int , int , int);


void *init_boundaryStruct(struct boundaryStruct *);
void *check_boundary(struct boundaryStruct *, int , int, int, int );
void *print_boundaryStruct(struct boundaryStruct *);
void *free_boundaryStruct(struct boundaryStruct *);

int surround_pixels(int , int , int , int );

 /*
  *  Returns an array specifying which of the surrounding pixels is non-zero
  *   and not in the same cluster as the current pixel
  */
int checkMap(double *, int , int , int , int , int , int *, int , double );

/*
  * Given a start pixel and a clusterMap - recursively follows black pixels
  *  until end is found (no more black pixels). 
  */
 
int chainMap(double *, int *,struct boundaryStruct *, const int , const int , int , const int , const int , int , int , double );

/* ATTEMPT NUMBER 4  
 * Yet another go -this time, we are going to follow chains unrecursively
 * Outline:
 *	- travel over tfmap
 *	- first pixel found that is over threshold, assign clusterNo to it
 *	- check surrounding pixels for 'black'
 *	- note position etc
 *	- for each of them, repeat above, assigning clusterNo and removing from map
 *	- do until no more black pixels
 *	- this cluster should then be completely done, so increment clusterNo,
 *	- As above pixels have been removed, loop around pixels until another passes threshold
 *	- this is a new cluster, so repeat above
 */
int pixel_chain(int **, double **, double *, int , int , double[] , int , int , int );


/* Region based thresholding */
int regional_pixel_chain(int **, double **, double *, int , int , double *, int, int , int , int, int );

#endif  /* __SPHRAD_CLUST_H__ */
