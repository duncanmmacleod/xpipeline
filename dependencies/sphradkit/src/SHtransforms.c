/***************************************************************************
  **************************************************************************
  
                        library of S2kit 1.0

   Writen by Mark Edwards
   adapted from test_s2_semi_memo_inv.c
  
     S2Kit people:
     Peter Kostelec, Dan Rockmore
      {geelong,rockmore}@cs.dartmouth.edu
  
     Contact: Peter Kostelec
            geelong@cs.dartmouth.edu
  
     Copyright 2004 Peter Kostelec, Dan Rockmore
  
  ************************************************************************
  ************************************************************************/


/*
Compile into mex function to be called from Matlab.
Takes an array of spherical harmonic coefficients, performs the
inverse transform and returns the brightness map.
*/

#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <complex.h>
#include <complex.h>

#include "fftw3.h"
#include "makeweights.h"
#include "cospmls.h"
#include "FST_semi_memo.h"
#include "csecond.h"


/* Debugging specific includes */
#ifdef MEMWATCH
#include "memwatch.h"
#endif

#define max(A, B) ((A) > (B) ? (A) : (B))

/**************************************************/
/**************************************************/

/* Forward FSHT */
complex double *FSHT_forward(complex double *shcoeffs, complex double *grid, int size)
{
  int bw, cutoff;
  int i;
  double *rdata, *idata, *rresult, *iresult;
  double *seminaive_naive_tablespace, *trans_seminaive_naive_tablespace,
    *workspace;
  double **seminaive_naive_table, **trans_seminaive_naive_table;
  double *relerror, *curmax, granderror, grandrelerror;
  double total_time, for_time, inv_time;
  double tstart, tstop;
  fftw_plan dctPlan;
  fftw_plan fftPlan ;
  double *weights ;
  fftw_iodim dims[1], howmany_dims[1];
  int rank, howmany_rank ;
  granderror = 0.0;
  grandrelerror = 0.0; 

  /* Get size of grid data 
  size = sizeof(grid)/sizeof(grid[0]);
  
  fprintf(stdout, "grid size: %i",size);*/

  /* get bandwidth value */
  bw = (int) (size/2);
  
  /*
  fprintf(stdout, "Bandwidth: %d\n", bw);
  */
  
  /*** ASSUMING WILL SEMINAIVE ALL ORDERS ***/
  cutoff = bw ;

  total_time = 0.0;
  for_time = 0.0;
  inv_time = 0.0;


  /* allocate lots of memory */
  rdata = (double *) malloc(sizeof(double) * (size * size));
  idata = (double *) malloc(sizeof(double) * (size * size));
  rresult = (double *) malloc(sizeof(double) * (bw * bw));
  iresult = (double *) malloc(sizeof(double) * (bw * bw));
  /*
  shcoeffs = (complex double *) malloc(sizeof(complex double) * (bw * bw));
  */
  seminaive_naive_tablespace =
    (double *) malloc(sizeof(double) *
		      (Reduced_Naive_TableSize(bw,cutoff) +
		       Reduced_SpharmonicTableSize(bw,cutoff)));

  trans_seminaive_naive_tablespace =
    (double *) malloc(sizeof(double) *
		      (Reduced_Naive_TableSize(bw,cutoff) +
		       Reduced_SpharmonicTableSize(bw,cutoff)));
  
  workspace = (double *) malloc(sizeof(double) * 
				((8 * (bw*bw)) + 
				 (16 * bw)));

  /** space for errors **/
  relerror = (double *) malloc(sizeof(double) );
  curmax = (double *) malloc(sizeof(double) );

  /****
       At this point, check to see if all the memory has been
       allocated. If it has not, there's no point in going further.
  ****/

  if ( (rdata == NULL) || (idata == NULL) ||
       (rresult == NULL) || (iresult == NULL) ||
       (seminaive_naive_tablespace == NULL) ||
       (trans_seminaive_naive_tablespace == NULL) ||
       (workspace == NULL) || (shcoeffs == NULL))
    {
      perror("Error in allocating memory");
      exit( 1 ) ;
    }

  /* now precompute the Legendres */

  /*fprintf(stdout,"Generating seminaive_naive tables...\n");*/
  seminaive_naive_table = SemiNaive_Naive_Pml_Table(bw, cutoff,
						    seminaive_naive_tablespace,
						    workspace);
  

  /*fprintf(stdout,"Generating trans_seminaive_naive tables...\n");*/
  trans_seminaive_naive_table =
    Transpose_SemiNaive_Naive_Pml_Table(seminaive_naive_table,
					bw, cutoff,
					trans_seminaive_naive_tablespace,
					workspace);
  
  /* make array for weights, and construct fftw plans */
  weights = (double *) malloc(sizeof(double) * 4 * bw);

  /* make DCT plans -> note that I will be using the GURU
     interface to execute these plans within the routines*/

  /* forward DCT */
  dctPlan = fftw_plan_r2r_1d( 2*bw, weights, rdata,
			      FFTW_REDFT10, FFTW_ESTIMATE ) ;
      
  /*
    fftw "preamble" ;
    note that this plan places the output in a transposed array
  */
  rank = 1 ;
  dims[0].n = 2*bw ;
  dims[0].is = 1 ;
  dims[0].os = 2*bw ;
  howmany_rank = 1 ;
  howmany_dims[0].n = 2*bw ;
  howmany_dims[0].is = 2*bw ;
  howmany_dims[0].os = 1 ;
  
  /* forward fft */
  fftPlan = fftw_plan_guru_split_dft( rank, dims,
				      howmany_rank, howmany_dims,
				      rdata, idata,
				      workspace, workspace+(4*bw*bw),
				      FFTW_ESTIMATE );
  


  /* now make the weights */
  makeweights( bw, weights );
	
  /* Split the complex number */
  for (i=0;i<size*size;i++)
  {
    rdata[i] = creal(grid[i]); 
    idata[i] = cimag(grid[i]);
  }
  
  /* Check to see if either are NULL, if so this means that we are
    * dealing with purely imaginary/real data and must create the
    * appropriate array
    */
    if (rdata == NULL)
    {
      rdata = (double *) malloc(sizeof(double) * (size * size));
      memset(rdata, 0, size * size * sizeof(double));
    }
    else if (idata == NULL)
    {
      idata = (double *) malloc(sizeof(double) * (size * size));
      memset(idata, 0, size * size * sizeof(double));
    }
  
  
  /* now do the forward spherical transform */
  tstart = csecond();

  FST_semi_memo(rdata, idata,
		rresult, iresult,
		bw,
		seminaive_naive_table,
		workspace,
		0,
		cutoff,
		&dctPlan,
		&fftPlan,
		weights );

  tstop = csecond();
  total_time += (tstop - tstart);

  /*
  fprintf(stdout,"forward time \t = %.4e\n", tstop - tstart);
      		       
  fprintf(stdout,"Program: SHforward \n");
  fprintf(stdout,"Bandwidth = %d\n", bw);
  */
  
  /* Join the complex number */
  for (i=0;i<bw*bw;i++)
    shcoeffs[i] = (complex double) rresult[i]+I*iresult[i];

  fftw_destroy_plan( fftPlan );
  fftw_destroy_plan( dctPlan );

  free( weights );

  free(trans_seminaive_naive_table); free(seminaive_naive_table);
  free(curmax); free(relerror);  

  if (workspace == NULL)
	fprintf(stdout,"\n\n---------------------- workspace is broken ------------------------\n\n");
  else 
	fprintf(stdout,"\n\n---------------------- workspace is NOT broken ------------------------\n\n");
	
  free(workspace);
  free(trans_seminaive_naive_tablespace);
  free(seminaive_naive_tablespace);

  free(iresult); 
  free(rresult);
  
  free(idata);
  free(rdata);
  
  return shcoeffs ;
  
}

/* Inverse FSHT */
complex double *FSHT_inverse(complex double *grid, complex double *shcoeffs, int bw)
{
  int i, size, cutoff ;
  int rank, howmany_rank ;
  double *rcoeffs, *icoeffs, *rdata, *idata ;
  double *workspace, *weights ;
  double *seminaive_naive_tablespace, *trans_seminaive_naive_tablespace ;
  double **seminaive_naive_table, **trans_seminaive_naive_table;
  double tstart, tstop;
  fftw_plan idctPlan, ifftPlan ;
  fftw_iodim dims[1], howmany_dims[1];
  
  /*fprintf(stdout,"bandwidth of SH: %i\n",bw);*/
  
  size = 2*bw;

  /*** ASSUMING WILL SEMINAIVE ALL ORDERS ***/
  cutoff = bw ;

  /* allocate lots of memory */
  /* No need to malloc these as they already exist */
  rcoeffs = (double *) malloc(sizeof(double) * (bw * bw));
  icoeffs = (double *) malloc(sizeof(double) * (bw * bw));
  /*
  grid = (complex double *) malloc(sizeof(complex double) * (size * size));
  */
  rdata = (double *) malloc(sizeof(double) * (size * size));
  idata = (double *) malloc(sizeof(double) * (size * size));
  weights = (double *) malloc(sizeof(double) * 4 * bw);
  
  workspace = (double *) malloc(sizeof(double) * 
				((8 * (bw*bw)) + 
				 (10 * bw)));
								 
  seminaive_naive_tablespace =
    (double *) malloc(sizeof(double) *
		      (Reduced_Naive_TableSize(bw,cutoff) +
		       Reduced_SpharmonicTableSize(bw,cutoff)));

  trans_seminaive_naive_tablespace =
    (double *) malloc(sizeof(double) *
		      (Reduced_Naive_TableSize(bw,cutoff) +
		       Reduced_SpharmonicTableSize(bw,cutoff)));

		       
		       
  /****
       At this point, check to see if all the memory has been
       allocated. If it has not, there's no point in going further.
  ****/

  if ( (rdata == NULL) || (idata == NULL) ||
       (rcoeffs == NULL) || (icoeffs == NULL) ||
       (weights == NULL) ||
       (seminaive_naive_tablespace == NULL) ||
       (trans_seminaive_naive_tablespace == NULL) ||
       (workspace == NULL) )
    {
      perror("Error in allocating memory");
      exit( 1 ) ;
    }

  /* now precompute the Legendres */
  /* fprintf(stdout,"Generating seminaive_naive tables...\n"); */
  seminaive_naive_table = SemiNaive_Naive_Pml_Table(bw, cutoff,
						    seminaive_naive_tablespace,
						    workspace);

  /* fprintf(stdout,"Generating trans_seminaive_naive tables...\n"); */
  trans_seminaive_naive_table =
    Transpose_SemiNaive_Naive_Pml_Table(seminaive_naive_table,
					bw, cutoff,
					trans_seminaive_naive_tablespace,
					workspace);

  /* construct fftw plans */

  /* make iDCT plan -> note that I will be using the GURU
     interface to execute this plan within the routine*/
      
  /* inverse DCT */
  idctPlan = fftw_plan_r2r_1d( 2*bw, weights, rdata,
			       FFTW_REDFT01, FFTW_ESTIMATE );

  /*
    now plan for inverse fft - note that this plans assumes
    that I'm working with a transposed array, e.g. the inputs
    for a length 2*bw transform are placed every 2*bw apart,
    the output will be consecutive entries in the array
  */
  rank = 1 ;
  dims[0].n = 2*bw ;
  dims[0].is = 2*bw ;
  dims[0].os = 1 ;
  howmany_rank = 1 ;
  howmany_dims[0].n = 2*bw ;
  howmany_dims[0].is = 1 ;
  howmany_dims[0].os = 2*bw ;

  /* inverse fft */
  ifftPlan = fftw_plan_guru_split_dft( rank, dims,
				       howmany_rank, howmany_dims,
				       rdata, idata,
				       workspace, workspace+(4*bw*bw),
				       FFTW_ESTIMATE );

  /* now make the weights */
  makeweights( bw, weights );

  /* Split the complex number */
  for (i=0;i<bw*bw;i++)
  {
    rcoeffs[i] = creal(shcoeffs[i]); 
    icoeffs[i] = cimag(shcoeffs[i]);
  }

  /* Check to see if either are NULL, if so this means that we are
   * dealing with purely imaginary/real data and must create the
   * appropriate array
   */
  if (rcoeffs == NULL)
  {
    rcoeffs = (double *) malloc(sizeof(double) * (bw * bw));
    memset(rcoeffs, 0, bw * bw * sizeof(double));
  }
  else if (icoeffs == NULL)
  {
    icoeffs = (double *) malloc(sizeof(double) * (bw * bw));
    memset(icoeffs, 0, bw*bw*sizeof(double));
  }
  				   
  /* do the inverse spherical transform */
  tstart = csecond();
  InvFST_semi_memo(rcoeffs,icoeffs,
		   rdata, idata,
		   bw,
		   trans_seminaive_naive_table,
		   workspace,
		   0,
		   cutoff,
		   &idctPlan,
		   &ifftPlan );
  tstop = csecond();
  
  /* fprintf(stderr,"inv time \t = %.4e\n", tstop - tstart);
   fprintf(stdout,"about to write out samples\n");
   */
  
  /* Join the complex number */
  for (i=0;i<size*size;i++)
    grid[i] = (complex double) rdata[i]+I*idata[i];

  /* fprintf(stdout,"finished writing samples\n"); */
  
  /* now clean up */

  fftw_destroy_plan( ifftPlan );
  fftw_destroy_plan( idctPlan );

  free(trans_seminaive_naive_table);
  free(seminaive_naive_table);
  free(trans_seminaive_naive_tablespace);
  free(seminaive_naive_tablespace);
  free(workspace);
  free(weights);
  free(idata);
  free(rdata);
  free(icoeffs);
  free(rcoeffs);
  
  return grid;
  
}

