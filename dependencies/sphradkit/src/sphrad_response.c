/*
 * Copyright (C) 2010  Mark Edwards
 *   (based on code written by Kipp Cannon, Copyright (C) 2006)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "sphrad_response.h"
#include "sphrad.h"

#include <lal/DetResponse.h>
#include <lal/LALDatatypes.h>

/* Debugging specific includes*/
#include "memwatch.h"

/* Compute the standard antenna reponses */
static void Fzero(double *fplus, double *fcross, int numDet, const LALDetector **det, double theta, double phi)
{
	int detIdx;
  	
	for (detIdx=0;detIdx<numDet;detIdx++)
	  XLALComputeDetAMResponse(&fplus[detIdx], &fcross[detIdx], det[detIdx]->response, phi, M_PI_2 - theta, 0.0, 0.0);	  	
}

/* Expanded to do many detectors, well, as many as needed anyway */
static void FplusNDP(double *fplus, double *fcross, int numDet, const LALDetector **det, double theta, double phi)
{
	int detIdx;
  
	double tfplus[numDet];
	double psi;

	double fplusSum=0;
	double fcrossSum=0;
	double crossSum=0;
	
	for (detIdx=0;detIdx<numDet;detIdx++)
	{
	  XLALComputeDetAMResponse(&fplus[detIdx], &fcross[detIdx], det[detIdx]->response, phi, M_PI_2 - theta, 0.0, 0.0);	  
	  fplusSum += (fplus[detIdx] * fplus[detIdx]);
	  fcrossSum += (fcross[detIdx] * fcross[detIdx]);	  
	  crossSum += (fplus[detIdx]*fcross[detIdx]);
	}

	psi = (1./4.) * atan(2.0 * (crossSum) / (fplusSum - fcrossSum));
		
/*	
	mexPrintf( "atan check: %f\n", atan(1.2));
	mexPrintf( "Argument to atan check: %f\n",2.0 * (*fplus1 * fcross1 + *fplus2 * fcross2 + *fplus3 * fcross3) / (*fplus1 * *fplus1 + *fplus2 * *fplus2 - fcross1 * fcross1 - fcross2 * fcross2 - fcross3 * fcross3));
	
	mexPrintf("Det 1 info: \n\tF+: %f, \tFx: %f\n",*fplus1, fcross1);
	mexPrintf("Det 2 info: \n\tF+: %f, \tFx: %f\n",*fplus2, fcross2);
	mexPrintf("Det 3 info: \n\tF+: %f, \tFx: %f\n",*fplus3, fcross3);
*/	
	
	for (detIdx=0;detIdx<numDet;detIdx++)
	{
	  tfplus[detIdx] = fplus[detIdx];
	  fplus[detIdx]  = cos(2 * psi) * fplus[detIdx] + sin(2 * psi) * fcross[detIdx];
	  fcross[detIdx] = -sin(2 * psi)* tfplus[detIdx] + cos(2 * psi) * fcross[detIdx];
	}

	/* If |Fp|<|Fc|, rotate polarization by a further pi/4 */
	if (sqrt(fplusSum) < sqrt(fcrossSum))
	{
	  for (detIdx=0;detIdx<numDet;detIdx++)
	  {
	    tfplus[detIdx] = fplus[detIdx];
	    fplus[detIdx]  = fcross[detIdx];
	    fcross[detIdx] = -tfplus[detIdx];
	  }
	psi += M_PI/4;

	}
/*
	mexPrintf("Psi: %f\n", psi);

	mexPrintf( "Fx1: %f",fcross1);
	mexPrintf( "\tFx2: %f",fcross2);
	mexPrintf( "\tFx3: %f\n",fcross3);
*/
	
}


static double response_Nelement(double theta, double phi, void *data)
{
	/* Pull relevant info from structure */
	int detIdx;
	int numDet = ((struct response_Nelement_data *) data)->numDet;
	
  	double fplus[numDet];
	double fcross[numDet];
	
	double fplusSum=0;
	double fcrossSum=0;

	int fDet, sDet;
	
	FplusNDP(fplus, fcross, numDet, ((struct response_Nelement_data *) data)->det,theta, phi);

	fDet = ((struct response_Nelement_data *) data)->crossDet[0];
	sDet = ((struct response_Nelement_data *) data)->crossDet[1];
		
	for (detIdx=0;detIdx<numDet;detIdx++)
	{
	  fplusSum += (fplus[detIdx] * fplus[detIdx]);
	  fcrossSum += (fcross[detIdx] * fcross[detIdx]);	  
	}
	
	if (((struct response_Nelement_data *) data)->isPlus == 1)
	  return (fplus[fDet]*fplus[sDet])/ (fplusSum);
	else
	  return (fcross[fDet] *fcross[sDet])/ (fcrossSum);
	  
}

struct sh_series *responseN(const LALDetector **det, int numDet, unsigned int l_max, int *crossDet, int isPlus)
{
  /* crossDet - This is an array that hold the indices of the detectors  to generate the antenna response for
   *	Rather than calculating F1+/|F|^2 and F2+/|F|^2 separately, we are calculating F1+*F2+/|F|^2 in
   *	one go.
   *	Hopefully, we can further modify this to output the plus and cross responses in one go too.
   */
  
	struct sh_series *series = sh_series_new(l_max, 0);
	struct response_Nelement_data data = {
		.det  = det,
		.crossDet = crossDet,
		.numDet = numDet,
		.isPlus = isPlus,
	};
	
	return sh_series_from_realfunc(series, response_Nelement, &data);
}

/* Structure to hold the antenna factors for coherent calcualtion */
void detector_response_free(struct detector_response *response)
{
	int bIdx;
	int nBaselines;
	
	if(response)
	{
	  nBaselines = response[0].nBaselines;
	
	  for (bIdx = 0; bIdx < nBaselines; bIdx++)
	  {
	    sh_series_free(response[bIdx].plus);
	    sh_series_free(response[bIdx].cross);
	  }
 	free(response);
	}
}

struct detector_response *detector_response_new(const LALDetector *detectors[], int nInstr, unsigned int l_max)
{
int bIdx, Idx; 
int i,j;

int k=0;

int crossDet[2];
int nBaselines = (int) nInstr + nInstr * (nInstr - 1) / 2.;
	
struct detector_response *new = (struct detector_response*) malloc(nBaselines*sizeof(struct detector_response));	     
if(!new)
{
  free (new);	  
  return NULL;
}

for(i = 1; i < nInstr; i++)
  for(j = 0; j < i; j++, k++) 
  {
  /* Define detectors - same order as cross-correlation */
  crossDet[0] = /*j*/k;
  crossDet[1] = k /*i*/;

  /* Generate response */
  new[k].plus  = responseN(detectors,nInstr, l_max, crossDet, 1);
  new[k].cross = responseN(detectors,nInstr, l_max, crossDet, 0);
  new[k].nBaselines = nBaselines;
  new[k].nInstr = nInstr;
  }	
	  
for(i = 0; i < nInstr; i++)
  {
  /* Define detectors - same order as cross-correlation */
  crossDet[0] = i;
  crossDet[1] = i;

  /* Generate response */
  new[i+k].plus  = responseN(detectors,nInstr, l_max, crossDet, 1);
  new[i+k].cross = responseN(detectors,nInstr, l_max, crossDet, 0);
  new[i+k].nBaselines = nBaselines;
  new[k].nInstr = nInstr;
  }		  
	  
return new;
}

/***********************************************************************
 *     Functions to create/destroy the antenna response structure      *
 *        -> Gridded version                                           *
 ***********************************************************************/
void detector_response_grid_free(struct detector_response_grid *new)
{
  int idx;
  
  for (idx = 0 ; idx < new->nInstr; idx++)
  {
    /* Allocate memory for antenna response - odd is +, even is x*/
    free(new->polarisations[idx].cross);
    free(new->polarisations[idx].plus);
  }
  
  free(new->polarisations);
  free(new);
}

/* Given a detector network, generate the antenna response for each detector 
 * This is unweighted by the PSD, and not normalised.
 */
struct detector_response_grid *detector_response_grid_new(const LALDetector *detectors[], int nInstr, unsigned int l_max)
{

  /* Grid parameters */
  int gridSize = (l_max+1)*2;

  double theta;
  double phi;
  int thetaIdx, phiIdx;
  int idx;
  
  /* Antenna reponses for sky position */
  double fplus[nInstr];
  double fcross[nInstr];
    
  struct detector_response_grid *new = (struct detector_response_grid*) malloc(sizeof(struct detector_response_grid));

  if(!new)
  {
    free (new);
    return NULL;
  }
  
  new->nInstr     = nInstr;
  new->max_l      = l_max;
  new->gridSize   = (l_max+1)*2;
  new->nBaselines = (int) nInstr + nInstr * (nInstr - 1) / 2.;
     
  /* Allocate memory for antenna maps */
  new->polarisations = (struct detector_polarisations *) malloc(nInstr*sizeof(struct detector_polarisations));
  
  for (idx = 0 ; idx < nInstr; idx++)
  {
    /* Allocate memory for antenna response - odd is +, even is x*/
    new->polarisations[idx].cross  = (double *) malloc(gridSize*gridSize*sizeof(double));
    new->polarisations[idx].plus   = (double *) malloc(gridSize*gridSize*sizeof(double));

    /* Memory allocated with no problem? */
    if (!(new->polarisations[idx].cross || new->polarisations[idx].plus))
    {
      fprintf(stderr, "Cannot allocate memory for antenna response maps generation\n");
      return 0;
    }
  }
  
  /* Generate the antenna responses */
  for (thetaIdx = 0; thetaIdx < gridSize; thetaIdx ++)
    for (phiIdx = 0; phiIdx < gridSize; phiIdx ++)
    {
      theta = (thetaIdx/((double) gridSize))*M_PI;
      phi = (phiIdx/((double) gridSize))*M_PI*2;

      /* Calculate (+,x) for each detector 
      FplusNDP(fplus, fcross, nInstr, detectors,theta, phi);
      */
      
      /* Calculate (+,x) for each detector */
      Fzero(fplus, fcross, nInstr, detectors, theta, phi);
	
      
      /* Copy into the skymap  */
      for (idx=0;idx<nInstr;idx++)
      {
	new->polarisations[idx].plus[phiIdx+thetaIdx*gridSize] = fplus[idx];
	new->polarisations[idx].cross[phiIdx+thetaIdx*gridSize] = fcross[idx];
      }
    }
    return new;
}

/*********************************************************************************
 *       Take the single detector antenna responses, and noise weight them,      * 
 *                and normalise them                                             *
 ********************************************************************************/            

int form_response(double *ep/*Ptr2*/[], double *ex/*Ptr2*/[],const LALDetector **det,struct detector_response *f, int frequency, double **psd)
{
  /* On entering this function, we have the unwhitened, unnormalised 
   * antenna response,f(in),  (in the DPF) for each of the detectors, and a psd, S, for each.
   * We want to return the whitened, normalised response, e.
   * So, we must:
   *	1) whiten - divide f(in) by sqrt(S)
   *	2) calculate normalisation - |f|=sqrt((f1/sqrt(S))^2+(f3/sqrt(S))^2+(f3/sqrt(S))^2) (for 3 detectors)
   *	3) normalise - e=f(in)/|f|
   *
   * Now we've got our antenna factor just as we want it!
   */ 

  /* General counting variables */
  int idx = 0;
  int i, j, k;
  int pIdx;
  int gridSize = (f[0].plus->l_max+1)*4*(f[0].plus->l_max+1);

  /* Variables to hold summations and whitened responses */
  double fSumP, fSumX;
  double wfp[f[0].nInstr], wfx[f[0].nInstr];
/*
  double *ep[f[0].nBaselines], *ex[f[0].nBaselines];
  */
  /* Need antenna response for each detector, + and x */
  complex double *fSkyMap[f[0].nInstr*2];
/*
  /* Allocate memory for orthonormal responses 
  for (idx = 0; idx < f[0].nBaselines; idx++)
  {
    ep[idx] = (double *) malloc (gridSize*sizeof(double));
    ex[idx] = (double *) malloc (gridSize*sizeof(double));
    
    if (!(ep[idx] || ex[idx]))
     {
      fprintf(stderr, "Cannot allocate memory for stat generation\n");
      return 0;
    }   
/*    *epPtr[idx] = ep;
    *exPtr[idx] = ex;
  }
  
    *epPtr2 = ep;
    *exPtr2 = ex;
  */
  
  
  /* Loop over each antena response and synthesize a skymap from the coefficients*/
  for (idx = 0 ; idx < f[0].nInstr; idx++)
  {
    /* Allocate memory for antenna response - odd is +, even is x*/
    fSkyMap[idx*2] = (complex double *)malloc(gridSize*sizeof(complex double));
    fSkyMap[idx*2+1] = (complex double *)malloc(gridSize*sizeof(complex double));

    /* Memory allocated? */
    if (!(fSkyMap[idx] || fSkyMap[idx+1]))
    {
      fprintf(stderr, "Cannot allocate memory for stat generation\n");
      return 0;
    }

    /* Generate skymap from coefficients */
    FSHT_inverse(fSkyMap[idx*2], f[idx].plus->coeff, f[idx].plus->l_max+1);
    FSHT_inverse(fSkyMap[idx*2+1], f[idx].cross->coeff, f[idx].cross->l_max+1);
  }
  

  /* For each sky position we need to whiten and normalise */
  for (idx=0; idx < gridSize;idx++)
  {
    /* Clear sums for this sky position */
    fSumP = 0;
    fSumX = 0;
    
    /* Generate sum by looping around detectors 
     *  and keep a copy of the whitened f's as well
     */
    for (pIdx=0;pIdx<f[0].nInstr; pIdx++)
    {
      wfp[pIdx] = creal(fSkyMap[pIdx*2][idx]) /* /psd[pIdx][frequency]*/;
      wfx[pIdx] = creal(fSkyMap[pIdx*2+1][idx]) /* /psd[pIdx][frequency]*/;
         
      fSumP += wfp[pIdx]*wfp[pIdx]; 
      fSumX += wfx[pIdx]*wfx[pIdx];
      
    }
    /*
    fSumP = (fSumP);
    fSumX = (fSumX);
    */
    /* Form the whitened, normalised response */
    /* Cross terms */
    k = 0;
    for(i = 1; i < f[0].nInstr; i++)
      for(j = 0; j < i; j++, k++) 
      {
	/* *epPtr*/ ep[k][idx] = (wfp[j]*wfp[i])/fSumP;
	/* *exPtr*/ ex[k][idx] = (wfx[j]*wfx[i])/fSumX;
	
/*	ep[k][idx] = wfp[k];
	ex[k][idx] = wfx[k];*/
      }	
      
    /* Auto terms */      
    for(i = 0; i < f[0].nInstr; i++)
      {
	/* *epPtr */ ep[i+k][idx] = (wfp[i]*wfp[i])/fSumP;
	/* *exPtr */ ex[i+k][idx] = (wfx[i]*wfx[i])/fSumX;
	
/*	
	ep[i+k][idx] = wfp[i];
	ex[i+k][idx] = wfx[i];*/

	
      }		  
      /*
    for(i = 0; i < f[0].nInstr; i++)
    {
      ep[i][idx] = creal(fSkyMap[i*2][idx]) /* /psd[pIdx][frequency];
      ex[i][idx] = creal(fSkyMap[i*2+1][idx]) /* /psd[pIdx][frequency];
    }
*/

  }

  /* Free allocated memory */
  for (idx = 0 ; idx < (f[0].nInstr*2); idx++)
    free(fSkyMap[idx]);

  /*
  *epPtr2 = ep;
  *exPtr2 = ex;
  */
  
  return 1;

}

/* Pass in a detector_response_grid structure, and this will calculate the noise weighted
 * normalised version
 */
int form_response_grid(double *ep[], double *ex[], double *eta, struct detector_response_grid *F, int frequencyRange[2], double **psd)
{
  /* On entering this function, we have the unwhitened, unnormalised 
   * antenna response,f(in),  (in the DPF) for each of the detectors, and a psd, S, for each.
   * We want to return the whitened, normalised response, e.
   * So, we must:
   *	1) whiten - divide f(in) by sqrt(S)
   *	2) calculate normalisation - |f|=sqrt((f1/sqrt(S))^2+(f3/sqrt(S))^2+(f3/sqrt(S))^2) (for 3 detectors)
   *	3) normalise - e=f(in)/|f|
   *
   * Now we've got our antenna factor just as we want it!
   * Actually, this generates the entire thing on the fly.
   */ 

  /* General counting variables */
  int idx = 0;
  int i, j, k;
  int pIdx;

  /* Variables to hold summations and whitened responses */
  double fSumP, fSumX;
  double wfp[F->nInstr], wfx[F->nInstr];
  double avePSD[F->nInstr];
  
  /* PSD averaging over out narrow band signal of interest */
  for (i=frequencyRange[0];i<frequencyRange[1];i++)
   for (j=0; j<F->nInstr; j++)
    avePSD[j] += psd[j][i];

  for (j=0;j<F->nInstr; j++)
    avePSD[j] /= (frequencyRange[1]-frequencyRange[0]+1);
  
  
  /* For each sky position we need to whiten and normalise */
  for (idx=0; idx < F->gridSize*F->gridSize;idx++)
  {
    /* Clear sums for this sky position */
    fSumP = 0;
    fSumX = 0;
    
    /* Generate sum by looping around detectors 
     *  and keep a copy of the whitened f's as well
     */
    for (pIdx=0;pIdx<F->nInstr; pIdx++)
    {
      wfp[pIdx] = creal(F->polarisations[pIdx].plus[idx]) / avePSD[pIdx];
      wfx[pIdx] = creal(F->polarisations[pIdx].cross[idx]) / avePSD[pIdx];
      
      fSumP += wfp[pIdx]*wfp[pIdx]; 
      fSumX += wfx[pIdx]*wfx[pIdx];
    }

    eta[idx] = fSumX/fSumP;
    
    /* Form the whitened, normalised response */
    /* Cross terms */
    k = 0;
    for(i = 1; i < F->nInstr; i++)
      for(j = 0; j < i; j++, k++) 
      {
	ep[k][idx] = (wfp[j]*wfp[i])/fSumP;
	ex[k][idx] = (wfx[j]*wfx[i])/fSumX;

      }	
      
    /* Auto terms */
    for(i = 0; i < F->nInstr; i++)
      {
	ep[i+k][idx] = (wfp[i]*wfp[i])/fSumP;
	ex[i+k][idx] = (wfx[i]*wfx[i])/fSumX;	
      }
  }

  /* Free allocated memory */

  return 1;
}

int return_response(double ep[], double ex[], struct detector_response_grid *F, int arrayIdx, int frequency, double **psd)
{
  /* On entering this function, we have the unwhitened, unnormalised 
   * antenna response,f(in),  (in the DPF) for each of the detectors, and a psd, S, for each.
   * We want to return the whitened, normalised response, e.
   * So, we must:
   *	1) whiten - divide f(in) by sqrt(S)
   *	2) calculate normalisation - |f|=sqrt((f1/sqrt(S))^2+(f3/sqrt(S))^2+(f3/sqrt(S))^2) (for 3 detectors)
   *	3) normalise - e=f(in)/|f|
   *
   * Now we've got our antenna factor just as we want it!
   * Actually, this generates the entire thing on the fly.
   */ 

  /* General counting variables */
  int i, j, k;
  int pIdx;

  /* Variables to hold summations and whitened responses */
  double fSumP, fSumX;
  double wfp[F->nInstr], wfx[F->nInstr];

    /* Clear sums for this sky position */
    fSumP = 0;
    fSumX = 0;
    
    /* Generate sum by looping around detectors 
     *  and keep a copy of the whitened f's as well
     */
    for (pIdx=0;pIdx<F->nInstr; pIdx++)
    {
      wfp[pIdx] = creal(F->polarisations[pIdx].plus[arrayIdx]) / psd[pIdx][frequency];
      wfx[pIdx] = creal(F->polarisations[pIdx].cross[arrayIdx]) / psd[pIdx][frequency];
      
      fSumP += wfp[pIdx]*wfp[pIdx]; 
      fSumX += wfx[pIdx]*wfx[pIdx];
      
    }

    /* Form the whitened, normalised response */
    /* Same order as the correlator output */
    /* Cross terms */
    k = 0;
    for(i = 1; i < F->nInstr; i++)
      for(j = 0; j < i; j++, k++) 
      {
	ep[k] = (wfp[j]*wfp[i])/fSumP;
	ex[k] = (wfx[j]*wfx[i])/fSumX;
      }	
      
    /* Auto terms */      
    for(i = 0; i < F->nInstr; i++)
      {
	ep[i+k] = (wfp[i]*wfp[i])/fSumP;
	ex[i+k] = (wfx[i]*wfx[i])/fSumX;	
      }		  

return 1;
}

/*************************************/
/* MAJOR REWORK OF THE RESPONSE CODE */
/*************************************/


int converttodpf (double *Fp, double *Fc, int nInstr)
{
  /* Indexing variables */  
  int idx = 0;
  int i, j, k;
  int detIdx;

  /* Variables to hold summations and whitened responses */
  double tempFp;
  double tempFc;

  /* Clear sums for this sky position */
  double fSumP = 0;
  double fSumC = 0;
  double fplusSum=0;
  double fcrossSum=0;
  double crossSum=0;
  double psi=0;
  
  /* Form auto and cross sums */
  for (detIdx=0;detIdx<nInstr; detIdx++)
  {
    fSumP  	+= (Fp[detIdx] * Fp[detIdx]);
    fSumC  	+= (Fc[detIdx] * Fc[detIdx]);	  
    crossSum	+= (Fp[detIdx] * Fc[detIdx]);
  }

  /* Determine the angle of the DPF */
  psi = (1./4.) * atan((2.0 * crossSum)/(fSumP - fSumC));
  
  /* Form the new response vectors, noise weighted and rotated to the DPF */
  fSumP = 0; fSumC = 0;
  for (detIdx=0;detIdx<nInstr;detIdx++)
  {
    tempFp  = cos(2 * psi) * Fp[detIdx] + sin(2 * psi) * Fc[detIdx];
    tempFc = -sin(2 * psi) * Fp[detIdx] + cos(2 * psi) * Fc[detIdx];
    
    Fp[detIdx] = tempFp;
    Fc[detIdx] = tempFc; 
    
    fSumP  += (Fp[detIdx] * Fp[detIdx]);
    fSumC  += (Fc[detIdx] * Fc[detIdx]);	  
  }

  /* If |Fp|<|Fc|, rotate polarization by a further pi/4 */
  if ((fSumP) < (fSumC))
  {
    for (detIdx=0;detIdx<nInstr;detIdx++)
    {
      tempFp = Fp[detIdx];
      tempFc = Fc[detIdx];
      
      Fp[detIdx]  = Fc[detIdx];
      Fc[detIdx] = -tempFp;
    }
  psi += M_PI/4;
  }
  return 1;
}



/* Pass in a detector_response_grid structure, and this will calculate the noise weighted
 * normalised version
 */
int generate_response_grid(double *enull[],double *magFn, double *ep[],double *magFp, double *ex[], double *magFx,double *eta, struct detector_response_grid *antResponses ,const LALDetector **detectors, int gridSize, int nInstr, int *frequencyRange, double **coeffs)
{
  /* Using the given frequency range and coefficients, we are goind to generate the weighted normalised 
   * antenna responses, ep and ex.
   * 
   * So, we must:
   *	1) noise weight the response - divide F by sqrt(S)
   *	2) calculate normalisation - |f|=sqrt((f1/sqrt(S))^2+(f3/sqrt(S))^2+(f3/sqrt(S))^2) (for 3 detectors)
   *	3) normalise - e=f(in)/|f|
   *
   * Now we've got our antenna factor just as we want it!
   * Actually, this generates the entire thing on the fly.
   * Also sets up the null antenna response using cross(ep,ec)
   */ 

  /* General counting variables */
  int idx = 0;
  int i, j, k;
  int pIdx;

  /* Variables to hold summations and whitened responses */
  double fplus[nInstr];
  double fcross[nInstr];
  double wfp[nInstr], wfx[nInstr], wfn[nInstr];
  double wFp[nInstr], wFx[nInstr], wFn[nInstr];
  double avePSD[nInstr];

/* Output the antenna factors 
  double *OwFp[nInstr];
  double *OwFx[nInstr];
 
  for (j=0;j<nInstr; j++)
  {
   OwFp[j] = (double *) malloc(gridSize*gridSize*sizeof(double));
   OwFx[j] = (double *) malloc(gridSize*gridSize*sizeof(double));
  }
  */
  
  
  /* Sky position variables */
  int thetaIdx=0;
  int phiIdx=0;
  double theta=0;
  double phi=0;
  

  /* Clear sums for this sky position */
  double fSumP, fSumX, fSumN;
  double fplusSum=0;
  double crossSum=0;
  double psi=0;
  
  for (j=0;j<nInstr; j++)
    avePSD[j] = 0;
  
  mexPrintf("\tFreq Index: %d-%d\n",frequencyRange[0], frequencyRange[1] );
  
  /* PSD averaging over out narrow band signal of interest */
  for (i=frequencyRange[0];i<=frequencyRange[1];i++)
   for (j=0; j<nInstr; j++)
      avePSD[j] += coeffs[j][i];
/*    avePSD[j] += pow(coeffs[j][i],-2.);  */

  for (j=0;j<nInstr; j++)
    avePSD[j] /= (frequencyRange[1]-frequencyRange[0]+1);
  

  /* Generate the antenna responses over the sky */
  for (thetaIdx = 0; thetaIdx < (gridSize); thetaIdx++)
    for (phiIdx = 0; phiIdx < (gridSize); phiIdx++)
    {
	theta = (thetaIdx/((double) gridSize))*M_PI;
	phi = (phiIdx/((double) gridSize))*M_PI*2;
	idx = phiIdx +thetaIdx*gridSize;
	
	/* Calculate (+,x) for each detector from scratch */
	if (antResponses == NULL)
	  Fzero(fplus, fcross, nInstr,detectors, theta, phi);
	
	for (pIdx=0;pIdx<nInstr; pIdx++)
	{
	  /* If detectors structure is empty, use the pre-calculated response */
	  if (detectors == NULL)
	  {
	    fplus[pIdx]  = antResponses->polarisations[pIdx].plus[idx];
	    fcross[pIdx] = antResponses->polarisations[pIdx].cross[idx]; 
	  }


	  /* Noise weight antenna factors */
/*	  wfp[pIdx] = fplus[pIdx] / sqrt(avePSD[pIdx]);
	  wfx[pIdx] = fcross[pIdx]/ sqrt(avePSD[pIdx]);
*/	 
  	  wfp[pIdx] = fplus[pIdx]  * (avePSD[pIdx]);
	  wfx[pIdx] = fcross[pIdx] * (avePSD[pIdx]);
	  
	}
	
	/* Convert the whitened factors to the Dominant polarisation frame */	
	converttodpf (wfp, wfx, nInstr);
	
	/* Get a null vector */
	cross_product(wfp, wfx, wfn);

	fSumP = 0; fSumX = 0; fSumN = 0;
	for (pIdx=0;pIdx<nInstr;pIdx++)
	  {
	    wFp[pIdx] = wfp[pIdx];
	    wFx[pIdx] = wfx[pIdx];
	    wFn[pIdx] = wfn[pIdx];
	    /*
	    OwFp[pIdx][idx] = wFp[pIdx];
	    OwFx[pIdx][idx] = wFx[pIdx];
	    */
	    fSumP  += (wFp[pIdx] * wFp[pIdx]);
	    fSumX  += (wFx[pIdx] * wFx[pIdx]);	   
	    fSumN  += (wFn[pIdx] * wFn[pIdx]);
	  }
	

	/* Store the magnitudes for this pixel */
	magFp[idx] = fSumP;
	magFx[idx] = fSumX;
	magFn[idx] = fSumN;
	
	/* Calculate eta for contrained analysis */
	eta[idx] = fSumX/fSumP;
	
	/* Now form the normalised response */
	double tempep[nInstr], tempex[nInstr], tempenull[nInstr];
	for(i = 0; i < nInstr; i++)
	{
	    ep[i][idx] = (wFp[i])/sqrt(fSumP);
	    ex[i][idx] = (wFx[i])/sqrt(fSumX);

	    tempep[i] = ep[i][idx];
	    tempex[i] = ex[i][idx];
	}	

	/* Generate a null vector */	
	cross_product(tempep, tempex, tempenull);

	for (i=0; i < nInstr; i++)
		enull[i][idx] = tempenull[i];
	
	
  }
 
  /* Output orthonormal responses 
  if (0)
  {
     product_output("NEWresponse.mat", "HwFp", OwFp[0], gridSize, gridSize ,1);
     product_output("NEWresponse.mat", "HwFx", OwFx[0], gridSize, gridSize ,1);

     product_output("NEWresponse.mat", "LwFp", OwFp[1], gridSize, gridSize ,1);
     product_output("NEWresponse.mat", "LwFx", OwFx[1], gridSize, gridSize ,1);

     product_output("NEWresponse.mat", "VwFp", OwFp[2], gridSize, gridSize ,1);
     product_output("NEWresponse.mat", "VwFx", OwFx[2], gridSize, gridSize ,1);
  }
 
 for (j=0;j<nInstr; j++)
 {
   free(OwFp[j]);
   free(OwFx[j]);
 }
 */
 
  mexPrintf("\tPSD: %g, %g %g\n", avePSD[0], avePSD[1], avePSD[2]);
  
  return 1;
}
