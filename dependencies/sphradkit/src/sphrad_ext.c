			/*
 * Copyright (C) 2010  Mark Edwards
 *   (based on code written by Kipp Cannon, Copyright (C) 2006)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#include <fftw3.h>

#include <gsl/gsl_math.h>
#include <gsl/gsl_vector.h>

#include <correlator.h>
#include <instrument.h>
#include <sh_series.h>
#include <projection.h>
#include <inject.h>
#include <instruments.h>

#include "sphrad_ext.h"

/* Debugging specific includes*/ 
#include "memwatch.h"

/* REMOVE ONCE TESTING HAS COMPLETED */

double *my_sky_rotation_matrix(double alpha, double beta, double gamma)
{
	enum {
		x = 0,
		y = 1,
		z = 2
	};
	double *R = malloc(9 * sizeof(*R));

	if(!R)
		return NULL;

	R[3 * x + x] =  cos(gamma) * cos(beta) * cos(alpha) - sin(gamma) * sin(alpha);
	R[3 * x + y] = -cos(gamma) * cos(beta) * sin(alpha) - sin(gamma) * cos(alpha);
	R[3 * x + z] =  cos(gamma) * sin(beta);
	R[3 * y + x] =  sin(gamma) * cos(beta) * cos(alpha) - cos(gamma) * sin(alpha);
	R[3 * y + y] = -sin(gamma) * cos(beta) * sin(alpha) + cos(gamma) * cos(alpha);
	R[3 * y + z] =  sin(gamma) * sin(beta);
	R[3 * z + x] = -sin(beta) * cos(alpha);
	R[3 * z + y] =  sin(beta) * sin(alpha);
	R[3 * z + z] =  cos(beta);

	return R;
}



double *my_sh_series_rot_matrix(double theta, double phi)
{
	return my_sky_rotation_matrix(0, theta, phi);
}




/*
 * A network of baselines
 */

struct correlator_network_baselines *correlator_network_allbaselines_new(const struct instrument * const *instruments, int n_instruments)
{
	struct correlator_network_baselines *new = malloc(sizeof(*new));
	struct correlator_baseline **baselines = malloc((n_instruments +(int) (n_instruments * (n_instruments - 1) / 2.)) * sizeof(*baselines));
	int i, j, k;

	if(!new || !baselines) {
		free(new);
		free(baselines);
		return NULL;
	}

	/* Cross detectors */
	k = 0;
	for(i = 1; i < n_instruments; i++)
		for(j = 0; j < i; j++, k++)
			/* FIXME: what if this fails? */
			baselines[k] = correlator_baseline_new(instruments, i, j);

	/* Auto detectors */
	for(i = 0; i < n_instruments; i++)
	  baselines[i+(int) (n_instruments * (n_instruments - 1) / 2.)] = correlator_baseline_new(instruments, i, i);
		
	new->n_instruments = n_instruments;
	new->n_baselines = n_instruments + (int)  n_instruments * (n_instruments - 1) / 2.;
	new->baselines = baselines;

	return new;
}

/* Free all baselines */
void correlator_network_allbaselines_free(struct correlator_network_baselines *network)
{
	if(network) {
		int i;
		for(i = 0; i < network->n_baselines; i++)
			correlator_baseline_free(network->baselines[i]);
		free(network->baselines);
	}
	free(network);
}

/*
 * ============================================================================
 *
 *   Unsummed power
 *	- Calcualtes the cross-power for each set of detectors, but does
 *		not sum them
 *
 * ============================================================================
 */


struct sh_series_array *unsummed_correlator_network_integrate_power_fd(struct sh_series_array *sky, complex double **fseries, struct correlator_network_plan_fd *plan)
{
	int i, j, k;
	k = 0;
	for(i = 1; i < plan->baselines->n_instruments; i++)
		for(j = 0; j < i; j++, k++) 
		{
			correlator_baseline_integrate_power_fd(fseries[i], fseries[j], plan->plans[k]);
			memcpy(sky->series[k].coeff, plan->plans[k]->power_2d->coeff, sh_series_length(sky->l_max,sky->polar )* sizeof(*plan->plans[k]->power_2d->coeff));
		}
		
	/* Auto-correlation terms */
	k=plan->baselines->n_baselines - plan->baselines->n_instruments;
	for(i = 0; i < plan->baselines->n_instruments; i++, k++)
	{
	  correlator_baseline_integrate_power_fd(fseries[i], fseries[i], plan->plans[k]);
   	  memcpy(sky->series[k].coeff, plan->plans[k]->power_2d->coeff, sh_series_length(sky->l_max,sky->polar )* sizeof(*plan->plans[k]->power_2d->coeff));
	}

	
return sky;
}


/*
 * ============================================================================
 *
 *  FAST time-frequency map generation
 *	- makes a tfmap using the 1d power from the cross-correlator
 *	- very fast, but obviously inaccurate - only used to guide the 
 *	   search
 * ============================================================================
 */

/*
  Compute the inner product of an array of sh_series objects and a complex-valued vector.  The result is an sh_series object equal to
 * 	sum_{i} sh_series[i] * vector[i]
 */

struct sh_series *SHsum_sh_series_array_dotc(struct sh_series *result, struct sh_series *tempResult,const struct sh_series_array *delay, const complex double *fproduct, const struct sh_series_rotation_plan *rPlan, struct sh_series *sky)
{
	const int shIdx = delay->stride;
	const complex double *c = delay->coeff;
	const complex double *last = fproduct + delay->n;

	if((result->l_max != delay->l_max) || (result->polar != delay->polar))
		return NULL;

	int idx = 0;
	int j = 0;
	while(fproduct < last) {
		const complex double v = *fproduct++;
		int i;
		for(i = 0; i < shIdx; i++)
		    result->coeff[i] += *c++ * v;
		
		/* normalize */
		sh_series_scale(result, 1.0 / delay->n);

		/* Rotate over sky - This needs to happen for a full, not fast, tfmap
		sh_series_rotate(tempResult, result, rPlan);*/
		
		/* Add into skymap 
		for (idx=0;idx<sh_series_length(tempResult->l_max, tempResult->polar);idx++)
		  sky->coeff[j] += tempResult->coeff[idx];*/
		
		/* Need to sum the power in the coefficients - assuming that Parseval's theorem 
		 * holds, then it should be Sum((l=0-Inf) (Sum(-m<l<-m)) |f(lm)|^2))
		 * So, all we need to do here is multiply the coefficient by its conjugate
		 */
		for (idx=0;idx<sh_series_length(result->l_max, result->polar);idx++)
		  sky->coeff[j] += (result->coeff[idx]*conj(result->coeff[idx]));
		
		/* Blank result and increment j */
		sh_series_zero(result);
		j++;
	}

	return result;
}


struct sh_series *SHsum_correlator_baseline_integrate_power_fd(const complex double *freq_series_a, const complex double *freq_series_b, struct correlator_plan_fd *plan, struct sh_series *sky, double *psd_a, double *psd_b)
{
	int i;
	
	/* multiply the two frequency series */
	for(i = 0; i < plan->delay_product->n; i++)
		plan->fseries_product[i] = *freq_series_b++ * conj(*freq_series_a++);
		/** *psd_a++ * conj(*psd_b++);*/

	/* compute the inner product of the frequency series and the DFT'ed
	 * delay matrix */
	SHsum_sh_series_array_dotc(plan->power_1d, plan->power_2d,plan->delay_product, plan->fseries_product, plan->rotation_plan, sky);

	return sky;
}


struct sh_series *SHsum_correlator_network_integrate_power_fd(struct sh_series *sky, complex double **fseries, struct correlator_network_plan_fd *plan, double **psd)
{
	int i, j, k;
		
	/* Zero sky */
	sh_series_zero(sky);

	k = 0;
	for(i = 1; i < plan->baselines->n_instruments; i++)
		for(j = 0; j < i; j++, k++)
			SHsum_correlator_baseline_integrate_power_fd(fseries[i], fseries[j], plan->plans[k], sky, psd[i], psd[j]);

	/* Auto-correlation terms */
	k=plan->baselines->n_baselines - plan->baselines->n_instruments;
	for(i = 0; i < plan->baselines->n_instruments; i++, k++)
	  SHsum_correlator_baseline_integrate_power_fd(fseries[i], fseries[i], plan->plans[k], sky, psd[i], psd[i]);
	
	return sky;
}

/*
 * ============================================================================
 *
 *           Frequency range correlator
 *
 * ============================================================================
 */

/* A version of the power integrator that takes a range of frequencies to 
 * sum over, rather than all.
 * Takes an extra input - a 2 element array - specifying start index 
 */

struct sh_series *sh_series_array_dotc_range(struct sh_series *result, const struct sh_series_array *array, const complex double *vector, int *freqRange)
{	
	/* Sort out which frequency we are going to deal with */
	const complex double *first = vector + freqRange[0];
	const complex double *last  = vector + freqRange[1];

	const int n = array->stride;
	const complex double *c = array->coeff+n*freqRange[0];
	
	if((result->l_max != array->l_max) || (result->polar != array->polar))
		return NULL;

	sh_series_zero(result);
	while(first < last) {
		const complex double v = *first++;
		int i;
		for(i = 0; i < n; i++)
			result->coeff[i] += *c++ * v;
	}

	return result;
}

struct sh_series *correlator_baseline_integrate_power_fd_range(const complex double *freq_series_a, const complex double *freq_series_b, struct correlator_plan_fd *plan, int *freqRange)
{
	int i;

	/* Multiply the two frequency series when they are within our frequency range*/
	
	for(i = 0; i < plan->delay_product->n; i++)
	{
	  if ((i==freqRange[0]) || ((i -(plan->delay_product->n/2)) == (freqRange[0])))
		plan->fseries_product[i] = *freq_series_b++ * conj(*freq_series_a++);
	  else if ((i>freqRange[0]) && (i<freqRange[1]))
		plan->fseries_product[i] = *freq_series_b++ * conj(*freq_series_a++);
	  else if ((i<(plan->delay_product->n - freqRange[0])) && (i>(plan->delay_product->n -freqRange[1])))
		plan->fseries_product[i] = *freq_series_b++ * conj(*freq_series_a++);	  
	  else
		{
		plan->fseries_product[i] = 0;
		*freq_series_a++;
		*freq_series_b++;
		}
	}
		
	/* compute the inner product of the frequency series and the DFT'ed
	 * delay matrix */
	
	/*
	sh_series_array_dotc_range(plan->power_1d, plan->delay_product, plan->fseries_product, freqRange);
	*/
	sh_series_array_dotc(plan->power_1d, plan->delay_product, plan->fseries_product );
	/* FIXME:  to remove the correlator transient as in the time-domain
	 * case, or just generally apply a window, rather than computing
	 * the inner product directly, the products should be left
	 * un-summed (call _windowc() rather than _dotc()), the resulting
	 * array transformed to the time domain, and then windowed.  This
	 * is very costly. */

	/* normalize */
	sh_series_scale(plan->power_1d, 1.0 / plan->delay_product->n);

	/* rotate to Earth-fixed equatorial co-ordinates */
	sh_series_rotate(plan->power_2d, plan->power_1d, plan->rotation_plan);

	return plan->power_2d;
}

/* Lack of overloading and polymorphism in C is a PAIN IN THE BUM! */
struct sh_series *correlator_network_integrate_power_fd_range(struct sh_series *sky, complex double **fseries, struct correlator_network_plan_fd *plan, int *freqRange)
{
	int i, j, k;

	sh_series_zero(sky);
	k = 0;
	for(i = 1; i < plan->baselines->n_instruments; i++)
		for(j = 0; j < i; j++, k++) {
			correlator_baseline_integrate_power_fd_range(fseries[i], fseries[j], plan->plans[k], freqRange);
			sh_series_add_into(sky, 1.0 / plan->baselines->n_baselines, plan->plans[k]->power_2d);
		}

	/* Auto-correlation terms */
	k=plan->baselines->n_baselines - plan->baselines->n_instruments;
	for(i = 0; i < plan->baselines->n_instruments; i++, k++)
	{
	  correlator_baseline_integrate_power_fd_range(fseries[i], fseries[i], plan->plans[k], freqRange);
	  sh_series_add_into(sky, 1.0 / plan->baselines->n_baselines, plan->plans[k]->power_2d);
	}


	return sky;
}


/*
 * ============================================================================
 *
 *  FULL time-frequency map generation
 *	- makes a tfmap using the 2d power from the cross-correlator
 *	- spherical harmonic expansion for each frequency bin
 *	- very slow, but obviously accurate
 *	- used to form out statistic
 * ============================================================================
 */

/*
 * Compute the inner product of an array of sh_series objects and a
 * complex-valued vector.  The result is an sh_series object equal to
 *
 * 	sum_{i} sh_series[i] * vector[i]
 */

struct sh_series *decompose_sh_series_array_dotc(struct sh_series *result, struct sh_series *tempResult,const struct sh_series_array *delay, const complex double *fproduct, const struct sh_series_rotation_plan *rPlan, struct sh_series_array *tf_map)
{
	const int shIdx = delay->stride;
	const complex double *c = delay->coeff;
	const complex double *last = fproduct + delay->n;

	if((result->l_max != delay->l_max) || (result->polar != delay->polar))
		return NULL;

	/*sh_series_zero(result);*/
	int idx = 0;
	int j = 0;
	while(fproduct < last) {
		const complex double v = *fproduct++;
		int i;
		for(i = 0; i < shIdx; i++)
		{
		    /*result->coeff[i] = ccma(*c++, v, result->coeff[i]);*/
		    result->coeff[i] += *c++ * v;
		}
		
		/* normalize */
		sh_series_scale(result, 1.0 / delay->n);

		/* rotate to Earth-fixed equatorial co-ordinates */
		/*
		sh_series_rotate(&tf_map->series[j], result, rPlan);
		*/
		
		/* To do more than a single baseline, we need to rotate into plan,
		 * then add sky->2d into the tf_map, just in case there is already 
		 * data in there
		 */
		/* Rotate over sky */
		sh_series_rotate(tempResult, result, rPlan);
		
		/* Add into tf_map */
		/* Sum coefficients - use Parseval's theorem */
		for (idx=0;idx<sh_series_length(tempResult->l_max, tempResult->polar);idx++)
		  tf_map->series[j].coeff[idx] += tempResult->coeff[idx];
		
		/* Blank result and increment j */
	
		sh_series_zero(result);
		/*
		memset(result->coeff, 0, sh_series_length(result->l_max, result->polar) * sizeof result->coeff[0]);
*/

		j++;
	}

	return result;
}

struct sh_series_array *decompose_correlator_baseline_integrate_power_fd(const complex double *freq_series_a, const complex double *freq_series_b, struct correlator_plan_fd *plan, struct sh_series_array *tf_map)
{
	int i;
	/*
	struct sh_series_array *result = NULL;
	result = sh_series_array_new(plan->delay_product->n, plan->delay_product->l_max, plan->delay_product->polar);
	*/
	
	/* multiply the two frequency series */
	for(i = 0; i < plan->delay_product->n; i++)
		plan->fseries_product[i] = *freq_series_b++ * conj(*freq_series_a++);

	/* compute the inner product of the frequency series and the DFT'ed
	 * delay matrix */
	decompose_sh_series_array_dotc(plan->power_1d, plan->power_2d,plan->delay_product, plan->fseries_product, plan->rotation_plan, tf_map);

	/* FIXME:  to remove the correlator transient as in the time-domain
	 * case, or just generally apply a window, rather than computing
	 * the inner product directly, the products should be left
	 * un-summed (call _windowc() rather than _dotc()), the resulting
	 * array transformed to the time domain, and then windowed.  This
	 * is very costly. */

	/* normalize */
/*	sh_series_scale(plan->power_1d, 1.0 / plan->delay_product->n);*/

	/* rotate to Earth-fixed equatorial co-ordinates */
/*	sh_series_rotate(plan->power_2d, plan->power_1d, plan->rotation_plan);*/

	return tf_map;
}

struct sh_series_array *decompose_correlator_network_integrate_power_fd(struct sh_series_array *tf_map, complex double **fseries, struct correlator_network_plan_fd *plan)
{
	int i, j, k;
	/* Zero sky */
	memset(tf_map->coeff, 0, tf_map->n * tf_map->stride * sizeof(*tf_map->coeff));

	k = 0;
	for(i = 1; i < plan->baselines->n_instruments; i++)
		for(j = 0; j < i; j++, k++) {
			decompose_correlator_baseline_integrate_power_fd(fseries[i], fseries[j], plan->plans[k], tf_map);
			/*
			for (m = 0; m < tf_map->n; m++)
			  sh_series_add_into(sky, 1.0 / plan->baselines->n_baselines, plan->plans[k]->power_2d);
			*/
		}

	/* Auto-correlation terms */
	k=plan->baselines->n_baselines - plan->baselines->n_instruments;
	for(i = 0; i < plan->baselines->n_instruments; i++, k++)
	  decompose_correlator_baseline_integrate_power_fd(fseries[i], fseries[i], plan->plans[k], tf_map);
	
	return tf_map;
}


/* Calculate the cross-power, but decompose it so that each frequency is available to use.
 * Then modify it using the antenna response so that we can form the standard likelihood.
 */

struct sh_series *generate_full_tfmap(struct sh_series_array *tf_map, complex double **fseries, struct correlator_network_plan_fd *plan,  struct detector_response *responses)
{
int i, j, k, m;

struct sh_series *timeBinOut;
timeBinOut = sh_series_new_zero (tf_map->l_max, tf_map->polar);

/* Zero sky */
memset(tf_map->coeff, 0, tf_map->n * tf_map->stride * sizeof(*tf_map->coeff));

k = 0;
for(i = 1; i < plan->baselines->n_instruments; i++)
	for(j = 0; j < i; j++, k++) 
	{
	    /* For each detector pair form the cross-correlation then modify with the appropriate antenna
		reponse */
	    decompose_correlator_baseline_integrate_power_fd(fseries[i], fseries[j], plan->plans[k], tf_map);

	    /* Need to do this separately for each frequency bin */
	    for (m = 0; m < tf_map->n; m++)
	    {
		  /* Convolve skymap and antenna response */
		  /* Sum coefficients and add to time-frequency map */
		  timeBinOut->coeff[m] = sh_sum(convolve_sh_series(tf_map->series[m], responses[k].plus));
	    }
	}

return timeBinOut;
}


/*
 * Frequency-domain network correlator - modified to also correlator a single detector stream with itself
 */


struct sh_series *correlator_allnetwork_integrate_power_fd(struct sh_series *sky, complex double **fseries, struct correlator_network_plan_fd *plan)
{
	int i, j, k;

	sh_series_zero(sky);	
	k = 0;
	/* Cross terms */
	for(i = 1; i < plan->baselines->n_instruments; i++)
		for(j = 0; j < i; j++, k++) {
			correlator_baseline_integrate_power_fd(fseries[i], fseries[j], plan->plans[k]);
			sh_series_add_into(sky, 1.0 / plan->baselines->n_baselines, plan->plans[k]->power_2d);
		}

	/* Auto-terms */
	k=plan->baselines->n_baselines - plan->baselines->n_instruments;
	for(i = 0; i < plan->baselines->n_instruments; i++, k++)
	{
	  correlator_baseline_integrate_power_fd(fseries[i], fseries[i], plan->plans[k]);
	  sh_series_add_into(sky, 1.0 / plan->baselines->n_baselines, plan->plans[k]->power_2d);
	}


	return sky;
}


/******************************************************************
 *
 *
 *           BIG TESTING AREA 
 ******************************************************************/


/*
 * ============================================================================
 *
 *                              Correlation Plan
 *
 * ============================================================================
 */


/*
 * Time-domain correlation plan.
 */


struct correlator_plan_td *full_correlator_plan_td_new(const struct correlator_baseline *baseline, double delta_t)
{
	struct correlator_plan_td *new = malloc(sizeof(*new));
	gsl_vector *d_prime = gsl_vector_alloc(3);
	unsigned int a_l_max = projection_matrix_l_max(vector_magnitude(baseline->d) / 2, delta_t);
	unsigned int b_l_max = projection_matrix_l_max(vector_magnitude(baseline->d) / 2, delta_t);
	struct sh_series_array *proj_a = NULL;
	struct sh_series_array *proj_b = NULL;
	struct sh_series *sample_a = sh_series_new(a_l_max, 0);
	struct sh_series *sample_b = sh_series_new(b_l_max, 0);
	unsigned int power_l_max = correlator_power_l_max(baseline->instrument_a, baseline->instrument_b, delta_t);	/* FIXME: make sure power_l_max does not excede a_l_max + b_l_max or we're wasting cpu cycles */
	struct sh_series *product = sh_series_new(power_l_max, 0);
	struct sh_series *power_1d = sh_series_new(power_l_max, 0);
	struct sh_series *power_2d = sh_series_new(power_l_max, 0);
	double d_length = vector_magnitude(baseline->d);
	double *R = sh_series_rot_matrix(baseline->theta, baseline->phi);
	struct sh_series_product_plan *product_plan = NULL;
	struct sh_series_rotation_plan *rotation_plan = NULL;

	if(!new || !d_prime || !sample_a || !sample_b || !product || !power_1d || !power_2d || !R)
		goto error;

	/* set d_prime to +d_length/2 * \hat{z}, and compute projection
	 * matrix */
	gsl_vector_set(d_prime, 0, gsl_vector_get(baseline->instrument_a->phase_centre,0));
	gsl_vector_set(d_prime, 1, gsl_vector_get(baseline->instrument_a->phase_centre,1));
	gsl_vector_set(d_prime, 2, gsl_vector_get(baseline->instrument_a->phase_centre,2));
	proj_a = projection_matrix_delay(projection_matrix_n_elements(vector_magnitude(d_prime), delta_t), a_l_max, d_prime, delta_t);

	/* set d_prime to -d_length/2 * \hat{z}, and compute projection
	 * matrix */
	gsl_vector_set(d_prime, 0, gsl_vector_get(baseline->instrument_b->phase_centre,0));
	gsl_vector_set(d_prime, 1, gsl_vector_get(baseline->instrument_b->phase_centre,1));
	gsl_vector_set(d_prime, 2, gsl_vector_get(baseline->instrument_b->phase_centre,2));
	proj_b = projection_matrix_delay(projection_matrix_n_elements(vector_magnitude(d_prime), delta_t), b_l_max, d_prime, delta_t);

	product_plan = sh_series_product_plan_new(power_1d, sample_a, sample_b);

	rotation_plan = sh_series_rotation_plan_new(power_1d, R);

	if(!proj_a || !proj_b || !product_plan || !rotation_plan)
		goto error;

	new->baseline = baseline;
	new->delta_t = delta_t;
	new->transient = correlator_transient(proj_a, proj_b);
	new->proj_a = proj_a;
	new->proj_b = proj_b;
	new->sample_a = sample_a;
	new->sample_b = sample_b;
	new->product = product;
	new->power_1d = power_1d;
	new->power_2d = power_2d;
	new->product_plan = product_plan;
	new->rotation_plan = rotation_plan;

	free(R);
	gsl_vector_free(d_prime);
	return new;

error:
	sh_series_rotation_plan_free(rotation_plan);
	sh_series_product_plan_free(product_plan);
	free(R);
	sh_series_free(power_2d);
	sh_series_free(power_1d);
	sh_series_free(product);
	sh_series_free(sample_b);
	sh_series_free(sample_a);
	sh_series_array_free(proj_b);
	sh_series_array_free(proj_a);
	gsl_vector_free(d_prime);
	free(new);
	return NULL;
}


/*
 * Frequency-domain correlation plan.
 */


struct correlator_plan_fd *full_correlator_plan_fd_new(const struct correlator_baseline *baseline, int n, double delta_t)
{
	struct correlator_plan_fd *new = malloc(sizeof(*new));
	/* use the TD plan constructor to do the work */
	struct correlator_plan_td *tdplan = full_correlator_plan_td_new(baseline, delta_t);
	complex double *fseries_product = malloc(n * sizeof(*fseries_product));
	struct sh_series_array *delay_product = NULL;
	complex double phase_a, phase_b;
	int i;

	if(!new || !tdplan || !fseries_product) {
		free(new);
		correlator_plan_td_free(tdplan);
		free(fseries_product);
		return NULL;
	}

	delay_product = sh_series_array_new(n, tdplan->product->l_max, tdplan->product->polar);
	if(!delay_product)
		goto error;

	/* Fourier transform projection matrices.  First, zero-pad the
	 * matrices to match the input vector length, then forward
	 * transform */
	phase_a = I * 2 * M_PI * ((tdplan->proj_a->n - 1) / 2) / n;
	phase_b = I * 2 * M_PI * ((tdplan->proj_b->n - 1) / 2) / n;
	sh_series_array_resize_zero(tdplan->proj_a, n);
	sh_series_array_forward_fft(tdplan->proj_a);
	sh_series_array_resize_zero(tdplan->proj_b, n);
	sh_series_array_forward_fft(tdplan->proj_b);

	/* rotate the phases so that it is as though the elements were
	 * centred on 0.  Note that because the one vector will be
	 * complex-conjugated and then multiplied by the other, if the
	 * phase adjustement is the same for both then it need not be done
	 * at all since the phases will cancel out in the product. */
	if(phase_a != phase_b) {
		/* FIXME: do I have to handle the negative frequencies as
		 * negative frequencies, or can I treat them as >Nyquist
		 * positive frequencies?  It would simplify this stuff to
		 * just let the loop run up to n */
		for(i = 1; i < n / 2; i++) {
			sh_series_scale(&tdplan->proj_a->series[i], cexp(phase_a * i));
			sh_series_scale(&tdplan->proj_a->series[n - i], cexp(phase_a * -i));
			sh_series_scale(&tdplan->proj_b->series[i], cexp(phase_b * i));
			sh_series_scale(&tdplan->proj_b->series[n - i], cexp(phase_b * -i));
		}
		/* i = n / 2 */
		if(i == n - i) {
			/* then there is a Nyquist component */
			sh_series_scale(&tdplan->proj_a->series[i], cexp(phase_a * i));
			sh_series_scale(&tdplan->proj_b->series[i], cexp(phase_b * i));
		}
	}

	/* Compute and store their product.  Note that the "a" matrix is
	 * complex-conjugated so the "a" frequency series has to be
	 * conjugated in the correlator.  Note that the frequencies get
	 * inverted!  DC component is left in place, others are swapped,
	 * negative<-->positive */

	sh_series_conj(&tdplan->proj_a->series[0]);
	sh_series_product(&delay_product->series[0], &tdplan->proj_a->series[0], &tdplan->proj_b->series[0], tdplan->product_plan);
	for(i = 1; i < n; i++) {
		sh_series_conj(&tdplan->proj_a->series[i]);
		sh_series_product(&delay_product->series[n - i], &tdplan->proj_a->series[i], &tdplan->proj_b->series[i], tdplan->product_plan);
	}

/* We can cheat here:
 *  - instead of using the product_plan, we should be able to use
 * the FSHT. Let's try!
 */
/*
fprintf(stdout,"Testing spatial product\n");
int cIdx=0;
complex double *gridA, *gridB;
complex double *prod_grid;
*/

/* FSH Transformation variables */
/* DANGER WILL ROBINSON - ASSUMING proj_a and proj_b have same order */
/* DANGER - CHANGE ONCE WORKING*/
/*
int bw, size;

bw = tdplan->product->l_max+1; 
size = bw * 2;
fprintf(stdout,"bandwidth: %i\nsize: %i\n",bw, size);
*/

/* Allocate memory */
/*
gridA = (complex double *) malloc(sizeof(complex double) * (size*size));
gridB = (complex double *) malloc(sizeof(complex double) * (size*size));
prod_grid = (complex double *) malloc(sizeof(complex double) * (size*size));

struct sh_series *tmpCoeff = sh_series_new_zero(tdplan->product->l_max , 0);
struct sh_series *bob = sh_series_new_zero(tdplan->proj_a->l_max , 0);
struct sh_series *bob1 = sh_series_new_zero(tdplan->proj_b->l_max , 0);
*/

/* Do DC component first*/  
  
  /* Transform to spatial domain, mulitply them, transform back to harmonic */
/*
  sh_series_assign(bob, &tdplan->proj_a->series[0]);
  sh_series_resize(bob, bw-1);
  FSHT_inverse(gridA, bob->coeff, bw);

  fprintf(stdout,"\tFirst element from bob: (%g, %g)\n",creal(bob->coeff[0]), cimag(bob->coeff[0]));
  fprintf(stdout,"\tFirst element from proj_a: (%g, %g)\n",creal(tdplan->proj_a->series[0].coeff[0]), cimag(tdplan->proj_a->series[0].coeff[0]));
  fprintf(stdout,"\tFirst element from gridA: (%g, %g)\n",creal(gridA[0]), cimag(gridA[0]));

  sh_series_assign(bob1, &tdplan->proj_b->series[0]);
  sh_series_resize(bob1, bw-1);  
  FSHT_inverse(gridB, bob1->coeff, bw);

  fprintf(stdout,"\tFirst element from bob: (%g, %g)\n",creal(bob1->coeff[0]), cimag(bob1->coeff[0]));
  fprintf(stdout,"\tFirst element from proj_b: (%g, %g)\n",creal(tdplan->proj_b->series[0].coeff[0]), cimag(tdplan->proj_b->series[0].coeff[0]));
  fprintf(stdout,"\tFirst element from gridB: (%g, %g)\n",creal(gridB[0]), cimag(gridB[0]));

  
  
  for (cIdx=0;cIdx<(size*size);cIdx++)    
    prod_grid[cIdx] = gridA[cIdx] * gridB[cIdx]; 
*/
  /* From spatial to harmonic */
/*
  FSHT_forward(tmpCoeff->coeff, prod_grid, size); 
  
  fprintf(stdout,"\tSome elements from FSHT:  (%g, %g), (%g, %g), (%g, %g)\n",creal(tmpCoeff->coeff[0]), cimag(tmpCoeff->coeff[0]),creal(tmpCoeff->coeff[1]), cimag(tmpCoeff->coeff[1]),creal(tmpCoeff->coeff[2]), cimag(tmpCoeff->coeff[2]));
  fprintf(stdout,"\tSome elements from delay: (%g, %g), (%g, %g), (%g, %g)\n",creal(delay_product->series[0].coeff[0]), cimag(delay_product->series[0].coeff[0]),creal(delay_product->series[0].coeff[1]), cimag(delay_product->series[0].coeff[1]),creal(delay_product->series[0].coeff[2]), cimag(delay_product->series[0].coeff[2]));

  
free(gridA);
free(gridB);
free(prod_grid);
sh_series_free(tmpCoeff);
sh_series_free(bob);
sh_series_free(bob1);
*/

  /* Assign to delay_product */
/*  delay_product->series[0].coeff  = ; */
/*
sh_series_product(&delay_product->series[0], &tdplan->proj_a->series[0], &tdplan->proj_b->series[0], tdplan->product_plan);
for(i = 1; i < n; i++) {
	sh_series_conj(&tdplan->proj_a->series[i]);
	sh_series_product(&delay_product->series[n - i], &tdplan->proj_a->series[i], &tdplan->proj_b->series[i], tdplan->product_plan);
}
*/

	/* Apply one factor of 1/N to the delay product */
	sh_series_array_scale(delay_product, 1.0 / delay_product->n);
		
	/* Done */
	new->baseline = baseline;
	new->delta_t = delta_t;
	new->transient = tdplan->transient;
	new->delay_product = delay_product;
	new->fseries_product = fseries_product;
	new->power_1d = tdplan->power_1d;
	new->power_2d = tdplan->power_2d;
	new->rotation_plan = tdplan->rotation_plan;
	/*
	new->proj_a = tdplan->proj_a;
	new->proj_b = tdplan->proj_b;
	*/
	/* Clean up */
	sh_series_product_plan_free(tdplan->product_plan);
	sh_series_free(tdplan->product);
	sh_series_free(tdplan->sample_b);
	sh_series_free(tdplan->sample_a);
/*	sh_series_array_free(tdplan->proj_b);
	sh_series_array_free(tdplan->proj_a);
*/	return new;

error:
	sh_series_array_free(delay_product);
	free(fseries_product);
	correlator_plan_td_free(tdplan);
	free(new);
	return NULL;
}

/*
 * Frequency-domain correlation plan for a baseline network
 */


struct correlator_network_plan_fd *full_correlator_network_plan_fd_new(struct correlator_network_baselines *baselines, int tseries_length, double delta_t)
{
	struct correlator_network_plan_fd *new = malloc(sizeof(*new));
	struct correlator_plan_fd **plans = malloc(baselines->n_baselines * sizeof(*plans));
	int i;

	if(!new || !plans) {
		free(new);
		free(plans);
		return NULL;
	}

	for(i = 0; i < baselines->n_baselines; i++)
		/* FIXME: what if this fails? */
		plans[i] = full_correlator_plan_fd_new(baselines->baselines[i], tseries_length, delta_t);

	new->baselines = baselines;
	new->plans = plans;

	return new;
}


struct correlator_network_plan_fd *best_correlator_network_plan_fd_new(struct correlator_network_baselines *baselines, int tseries_length, double delta_t)
{
	struct correlator_network_plan_fd *new = malloc(sizeof(*new));
	struct correlator_plan_fd **plans = malloc(baselines->n_baselines * sizeof(*plans));
	int i, j;

	if(!new || !plans) {
		free(new);
		free(plans);
		return NULL;
	}

	for(i = 0; i < baselines->n_baselines; i++)
	{
		/* FIXME: what if this fails? */
		plans[i] = correlator_plan_fd_new(baselines->baselines[i], tseries_length, delta_t);
		if (!plans[i])
			goto error;
	}

	new->baselines = baselines;
	new->plans = plans;

	return new;

error:
	for (j=i; i==0; i--)
		correlator_plan_fd_free(plans[j]);
	return NULL;	
}


/**********************************************************************************
 *                                                                                *
 *         Correlator functions that allow then user to specify the order         *
 *         of the spherical harmonic expansion                                    *
 *                                                                                *
 *********************************************************************************/



void user_correlator_plan_fd_free(struct correlator_plan_fd *plan)
{
	if(plan) {
		free(plan->fseries_product);
		sh_series_array_free(plan->delay_product);
		sh_series_free(plan->power_1d);
		sh_series_free(plan->power_2d);
		sh_series_rotation_plan_free(plan->rotation_plan);
/*		sh_series_array_free(plan->proj_b);
		sh_series_array_free(plan->proj_a);
*/	}
	free(plan);
}


void user_correlator_network_plan_fd_free(struct correlator_network_plan_fd *plan)
{
	if(plan) {
		int i;
		for(i = 0; i < plan->baselines->n_baselines; i++)
			user_correlator_plan_fd_free(plan->plans[i]);
	}
	free(plan);
}

struct correlator_plan_td *user_correlator_plan_td_new(const struct correlator_baseline *baseline, double delta_t, int *eOrder)
{
	struct correlator_plan_td *new = malloc(sizeof(*new));
	gsl_vector *d_prime = gsl_vector_alloc(3);
	unsigned int a_l_max = eOrder[0];
	unsigned int b_l_max = eOrder[1];
	struct sh_series_array *proj_a = NULL;
	struct sh_series_array *proj_b = NULL;
	struct sh_series *sample_a = sh_series_new(a_l_max, 1);
	struct sh_series *sample_b = sh_series_new(b_l_max, 1);
	unsigned int power_l_max = eOrder[2];	/* FIXME: make sure power_l_max does not excede a_l_max + b_l_max or we're wasting cpu cycles */
	struct sh_series *product = sh_series_new(power_l_max, 1);
	struct sh_series *power_1d = sh_series_new(power_l_max, 1);
	struct sh_series *power_2d = sh_series_new(power_l_max, 0);
	double d_length = vector_magnitude(baseline->d);
	double *R = my_sh_series_rot_matrix(baseline->theta, baseline->phi);
	struct sh_series_product_plan *product_plan = NULL;
	struct sh_series_rotation_plan *rotation_plan = NULL;

	if(!new || !d_prime || !sample_a || !sample_b || !product || !power_1d || !power_2d || !R)
		goto error;

	/* set d_prime to +d_length/2 * \hat{z}, and compute projection
	 * matrix */
	gsl_vector_set(d_prime, 0, 0);
	gsl_vector_set(d_prime, 1, 0);
	gsl_vector_set(d_prime, 2, +d_length / 2);
	proj_a = projection_matrix_delay(projection_matrix_n_elements(vector_magnitude(d_prime), delta_t), a_l_max, d_prime, delta_t);

	/* set d_prime to -d_length/2 * \hat{z}, and compute projection
	 * matrix */
	gsl_vector_set(d_prime, 0, 0);
	gsl_vector_set(d_prime, 1, 0);
	gsl_vector_set(d_prime, 2, -d_length / 2);
	proj_b = projection_matrix_delay(projection_matrix_n_elements(vector_magnitude(d_prime), delta_t), b_l_max, d_prime, delta_t);

	product_plan = sh_series_product_plan_new(power_1d, sample_a, sample_b);

	rotation_plan = sh_series_rotation_plan_new(power_1d, R);

	if(!proj_a || !proj_b || !product_plan || !rotation_plan)
		goto error;

	new->baseline = baseline;
	new->delta_t = delta_t;
	new->transient = correlator_transient(proj_a, proj_b);
	new->proj_a = proj_a;
	new->proj_b = proj_b;
	new->sample_a = sample_a;
	new->sample_b = sample_b;
	new->product = product;
	new->power_1d = power_1d;
	new->power_2d = power_2d;
	new->product_plan = product_plan;
	new->rotation_plan = rotation_plan;

	free(R);
	gsl_vector_free(d_prime);
	return new;

error:
	sh_series_rotation_plan_free(rotation_plan);
	sh_series_product_plan_free(product_plan);
	free(R);
	sh_series_free(power_2d);
	sh_series_free(power_1d);
	sh_series_free(product);
	sh_series_free(sample_b);
	sh_series_free(sample_a);
	sh_series_array_free(proj_b);
	sh_series_array_free(proj_a);
	gsl_vector_free(d_prime);
	free(new);
	return NULL;
}

struct correlator_plan_fd *user_correlator_plan_fd_new(const struct correlator_baseline *baseline, int n, double delta_t, int *eOrder)
{
	struct correlator_plan_fd *new = malloc(sizeof(*new));
	/* use the TD plan constructor to do the work */
	struct correlator_plan_td *tdplan = user_correlator_plan_td_new(baseline, delta_t, eOrder);
	complex double *fseries_product = malloc(n * sizeof(*fseries_product));
	struct sh_series_array *delay_product = NULL;
	complex double phase_a, phase_b;
	int i;

	if(!new || !tdplan || !fseries_product) {
		free(new);
		correlator_plan_td_free(tdplan);
		free(fseries_product);
		return NULL;
	}

	delay_product = sh_series_array_new(n, tdplan->product->l_max, tdplan->product->polar);
	if(!delay_product)
		goto error;

	/* Fourier transform projection matrices.  First, zero-pad the
	 * matrices to match the input vector length, then forward
	 * transform */
	phase_a = I * 2 * M_PI * ((tdplan->proj_a->n - 1) / 2) / n;
	phase_b = I * 2 * M_PI * ((tdplan->proj_b->n - 1) / 2) / n;
	sh_series_array_resize_zero(tdplan->proj_a, n);
	sh_series_array_forward_fft(tdplan->proj_a);
	sh_series_array_resize_zero(tdplan->proj_b, n);
	sh_series_array_forward_fft(tdplan->proj_b);

	/* rotate the phases so that it is as though the elements were
	 * centred on 0.  Note that because the one vector will be
	 * complex-conjugated and then multiplied by the other, if the
	 * phase adjustement is the same for both then it need not be done
	 * at all since the phases will cancel out in the product. */
	if(phase_a != phase_b) {
		/* FIXME: do I have to handle the negative frequencies as
		 * negative frequencies, or can I treat them as >Nyquist
		 * positive frequencies?  It would simplify this stuff to
		 * just let the loop run up to n */
		for(i = 1; i < n / 2; i++) {
			sh_series_scale(&tdplan->proj_a->series[i], cexp(phase_a * i));
			sh_series_scale(&tdplan->proj_a->series[n - i], cexp(phase_a * -i));
			sh_series_scale(&tdplan->proj_b->series[i], cexp(phase_b * i));
			sh_series_scale(&tdplan->proj_b->series[n - i], cexp(phase_b * -i));
		}
		/* i = n / 2 */
		if(i == n - i) {
			/* then there is a Nyquist component */
			sh_series_scale(&tdplan->proj_a->series[i], cexp(phase_a * i));
			sh_series_scale(&tdplan->proj_b->series[i], cexp(phase_b * i));
		}
	}

	/* Compute and store their product.  Note that the "a" matrix is
	 * complex-conjugated so the "a" frequency series has to be
	 * conjugated in the correlator.  Note that the frequencies get
	 * inverted!  DC component is left in place, others are swapped,
	 * negative<-->positive */

	sh_series_conj(&tdplan->proj_a->series[0]);
	sh_series_product(&delay_product->series[0], &tdplan->proj_a->series[0], &tdplan->proj_b->series[0], tdplan->product_plan);
	for(i = 1; i < n; i++) {
		sh_series_conj(&tdplan->proj_a->series[i]);
		sh_series_product(&delay_product->series[n - i], &tdplan->proj_a->series[i], &tdplan->proj_b->series[i], tdplan->product_plan);
	}

	/* Apply one factor of 1/N to the delay product */
	sh_series_array_scale(delay_product, 1.0 / delay_product->n);
		
	/* Done */
	new->baseline = baseline;
	new->delta_t = delta_t;
	new->transient = tdplan->transient;
	new->delay_product = delay_product;
	new->fseries_product = fseries_product;
	new->power_1d = tdplan->power_1d;
	new->power_2d = tdplan->power_2d;
	new->rotation_plan = tdplan->rotation_plan;
/*	
	new->proj_a = tdplan->proj_a;
	new->proj_b = tdplan->proj_b;
*/	
	/* Clean up */
	sh_series_product_plan_free(tdplan->product_plan);
	sh_series_free(tdplan->product);
	sh_series_free(tdplan->sample_b);
	sh_series_free(tdplan->sample_a);
	free(tdplan);
/*	sh_series_array_free(tdplan->proj_b);
	sh_series_array_free(tdplan->proj_a);
*/	return new;

error:
	sh_series_array_free(delay_product);
	free(fseries_product);
	correlator_plan_td_free(tdplan);	
	sh_series_array_free(tdplan->proj_b);
	sh_series_array_free(tdplan->proj_a);
	free(new);
	return NULL;
}

struct correlator_network_plan_fd *user_correlator_network_plan_fd_new(struct correlator_network_baselines *baselines, int tseries_length, double delta_t, int *eOrder)
{
	struct correlator_network_plan_fd *new = malloc(sizeof(*new));
	struct correlator_plan_fd **plans = malloc(baselines->n_baselines * sizeof(*plans));
	int i;

	if(!new || !plans) {
		free(new);
		free(plans);
		return NULL;
	}

	for(i = 0; i < baselines->n_baselines; i++)
		/* FIXME: what if this fails? */
		plans[i] = user_correlator_plan_fd_new(baselines->baselines[i], tseries_length, delta_t, eOrder);

	new->baselines = baselines;
	new->plans = plans;

	return new;
}





