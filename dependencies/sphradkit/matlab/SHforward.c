/***************************************************************************
  **************************************************************************
  
                           S2kit 1.0

          A lite version of Spherical Harmonic Transform Kit

   Peter Kostelec, Dan Rockmore
   {geelong,rockmore}@cs.dartmouth.edu
  
   Contact: Peter Kostelec
            geelong@cs.dartmouth.edu
  
   Copyright 2004 Peter Kostelec, Dan Rockmore

   This file is part of S2kit.

   S2kit is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   S2kit is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with S2kit; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   See the accompanying LICENSE file for details.
  
  ************************************************************************
  ************************************************************************/


/*

Source code to test full spherical harmonic transform
using the seminaive and naive algorithms coded up during October, 1995.

In its current state, assuming that will seminaive at ALL orders.
If you wish to change this, modify the CUTOFF variable in this file, i.e.
cutoff = at what order to switch from semi-naive to naive algorithm

WILL PRECOMPUTE IN MEMORY EVERYTHING BEFORE DOING TRANSFORMS!

Idea is to record the execution time for the spectral->grid->spectral
roundtrip, after precomputations have been done.

The strategy is to generate random coefficients representing
a complete spherical harmonic series of funcions Y(m,l).
The ordering of the these coefficients is assumed to be the
same as that of the output of the FST_seminaive() routine, namely
  

          f(0,0) f(0,1) f(0,2) ... f(0,bw-1)
          f(1,1) f(1,2) ... f(1,bw-1)
          etc.
          f(bw-2,bw-2), f(bw-2,bw-1)
          f(bw-1,bw-1)
          f(-(bw-1),bw-1)
          f(-(bw-2),bw-2) f(-(bw-2),bw-1)
          etc.
          f(-2,2) ... f(-2,bw-1)
          f(-1,1) f(-1,2) ... f(-1,bw-1)

This means that there are (bw*bw) coefficients.

Once the coefficients are generated, the corresponding
function is synthesized using InvFST_semi_memo(), then
transformed (analyzed) using FST_semi_memo(). Timing data
is printed.

Sample call

% test_s2_semi_memo bw loops [error_file]


Appropriate timing data will be printed out.


NOTE: In this program, the coefficients generated are such that the grid points
(sample values) produced are REAL. The routines InvFST_semi_fly() and
FST_semi_fly() will take advantage of this. If you wish to change this,
change the 7th argument of each function further down from "1" to "0".
This is also documented in the file FST_semi_fly.c .

*/

#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "fftw3.h"
#include "makeweights.h"
#include "cospmls.h"
#include "FST_semi_memo.h"
#include "csecond.h"
#include "mex.h"

#define max(A, B) ((A) > (B) ? (A) : (B))

/**************************************************/
/**************************************************/

/* matlab gateway routine */
void mexFunction ( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
  FILE *errorsfp ;
  int bw, size, cutoff;
  int i, j, idx;
  int l, m, dummy;
  double *rdata, *idata, *rresult, *iresult;
  double *seminaive_naive_tablespace, *trans_seminaive_naive_tablespace,
    *workspace;
  double **seminaive_naive_table, **trans_seminaive_naive_table;
  double dumx, dumy, fudge;
  double *relerror, *curmax, granderror, grandrelerror;
  double realtmp, imagtmp,origmag, tmpmag;
  double total_time, for_time, inv_time;
  double tstart, tstop;
  double ave_error, ave_relerror, stddev_error, stddev_relerror;
  time_t seed;
  fftw_plan dctPlan;
  fftw_plan fftPlan ;
  double *weights ;
  fftw_iodim dims[1], howmany_dims[1];
  int rank, howmany_rank ;
  granderror = 0.0;
  grandrelerror = 0.0; 


  /* mex grid output */
  double *outGrid = NULL;
  double *outGridi = NULL;

  /* Check number of input and output parameters */
  if(nrhs != 1) {
    mexErrMsgIdAndTxt("SHinv:initiate:nrhs",
                      "one input required.",
		      "SHforward(grid)");
  }

  /* Get size of grid data */
  size = mxGetN(prhs[0]);    

  /* get bandwidth value */
  bw = ceil(size/2);
  
  /*** ASSUMING WILL SEMINAIVE ALL ORDERS ***/
  cutoff = bw ;

  total_time = 0.0;
  for_time = 0.0;
  inv_time = 0.0;


  /* allocate lots of memory */
/*  rdata = (double *) malloc(sizeof(double) * (size * size));
  idata = (double *) malloc(sizeof(double) * (size * size));
*/  rresult = (double *) malloc(sizeof(double) * (bw * bw));
  iresult = (double *) malloc(sizeof(double) * (bw * bw));

  seminaive_naive_tablespace =
    (double *) malloc(sizeof(double) *
		      (Reduced_Naive_TableSize(bw,cutoff) +
		       Reduced_SpharmonicTableSize(bw,cutoff)));

  trans_seminaive_naive_tablespace =
    (double *) malloc(sizeof(double) *
		      (Reduced_Naive_TableSize(bw,cutoff) +
		       Reduced_SpharmonicTableSize(bw,cutoff)));
  
  workspace = (double *) malloc(sizeof(double) * 
				((8 * (bw*bw)) + 
				 (16 * bw)));

  /** space for errors **/
  relerror = (double *) malloc(sizeof(double) );
  curmax = (double *) malloc(sizeof(double) );

  /****
       At this point, check to see if all the memory has been
       allocated. If it has not, there's no point in going further.
  ****/

  if ( /*(rdata == NULL) || (idata == NULL) ||*/
       (rresult == NULL) || (iresult == NULL) ||
       (seminaive_naive_tablespace == NULL) ||
       (trans_seminaive_naive_tablespace == NULL) ||
       (workspace == NULL) )
    {
      perror("Error in allocating memory");
      exit( 1 ) ;
    }

  /* now precompute the Legendres */

  fprintf(stdout,"Generating seminaive_naive tables...\n");
  seminaive_naive_table = SemiNaive_Naive_Pml_Table(bw, cutoff,
						    seminaive_naive_tablespace,
						    workspace);
  

  fprintf(stdout,"Generating trans_seminaive_naive tables...\n");
  trans_seminaive_naive_table =
    Transpose_SemiNaive_Naive_Pml_Table(seminaive_naive_table,
					bw, cutoff,
					trans_seminaive_naive_tablespace,
					workspace);
  
  /* make array for weights, and construct fftw plans */
  weights = (double *) malloc(sizeof(double) * 4 * bw);

  /* make DCT plans -> note that I will be using the GURU
     interface to execute these plans within the routines*/

  /* forward DCT */
  dctPlan = fftw_plan_r2r_1d( 2*bw, weights, rdata,
			      FFTW_REDFT10, FFTW_ESTIMATE ) ;
      
  /*
    fftw "preamble" ;
    note that this plan places the output in a transposed array
  */
  rank = 1 ;
  dims[0].n = 2*bw ;
  dims[0].is = 1 ;
  dims[0].os = 2*bw ;
  howmany_rank = 1 ;
  howmany_dims[0].n = 2*bw ;
  howmany_dims[0].is = 2*bw ;
  howmany_dims[0].os = 1 ;
  
  /* forward fft */
  fftPlan = fftw_plan_guru_split_dft( rank, dims,
				      howmany_rank, howmany_dims,
				      rdata, idata,
				      workspace, workspace+(4*bw*bw),
				      FFTW_ESTIMATE );
  


  /* now make the weights */
  makeweights( bw, weights );

  /* do the inverse spherical transform */
  tstart = csecond();

  /* grid data is entered as an argument here instead of calculating it */
  /* now get pointers to grid data*/
  rdata = mxGetPr(prhs[0]);
  idata = mxGetPi(prhs[0]);
		    
  /* Check to see if either are NULL, if so this means that we are
    * dealing with purely imaginary/real data and must create the
    * appropriate array
    */
    if (rdata == NULL)
    {
      rdata = (double *) malloc(sizeof(double) * (size * size));
      memset(rdata, 0, size * size * sizeof(double));
    }
    else if (idata == NULL)
    {
      idata = (double *) malloc(sizeof(double) * (size * size));
      memset(idata, 0, size*size*sizeof(double));
    }
  
  
  tstop = csecond();
  inv_time += (tstop - tstart);
  
  fprintf(stdout,"inv time \t = %.4e\n", tstop - tstart);

  /* now do the forward spherical transform */
  tstart = csecond();

  FST_semi_memo(rdata, idata,
		rresult, iresult,
		bw,
		seminaive_naive_table,
		workspace,
		0,
		cutoff,
		&dctPlan,
		&fftPlan,
		weights );

  tstop = csecond();
  for_time += (tstop - tstart);
  
  fprintf(stdout,"forward time \t = %.4e\n", tstop - tstart);
  
  total_time = inv_time + for_time;
    		       
  /* Setup output mex structure */
  plhs[0] = mxCreateDoubleMatrix(bw*bw, 1, mxCOMPLEX);
  outGrid=mxGetPr(plhs[0]);
  outGridi=mxGetPi(plhs[0]);		       

  /* Assign the brightness map to the output */
  for( i = 0 ; i < bw*bw ; i++ )
  {
    outGrid[i]  = rresult[i];
    outGridi[i] = iresult[i];
  }
  
  fprintf(stdout,"Program: SHforward \n");
  fprintf(stdout,"Bandwidth = %d\n", bw);

#ifndef WALLCLOCK
  fprintf(stdout,"Total elapsed cpu time :\t\t %.4e seconds.\n",
	  total_time);
#else
  fprintf(stdout,"Total elapsed wall time :\t\t %.4e seconds.\n",
	  total_time);
#endif

  fftw_destroy_plan( fftPlan );
  fftw_destroy_plan( dctPlan );

  free( weights );

  free(trans_seminaive_naive_table); free(seminaive_naive_table);
  free(curmax); free(relerror);  free(workspace);
  free(trans_seminaive_naive_tablespace);
  free(seminaive_naive_tablespace);

  free(iresult); 
  free(rresult);
  
/*  free(idata);
  free(rdata);
*/
  return;
  
}


