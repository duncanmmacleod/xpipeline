#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <kippmath.h>
#include <fftw3.h>
// #include <correlator.h>
#include <instrument.h>
#include <sh_series.h>
#include <inject.h>
#include <instruments.h>

#include <lal/Date.h>
#include <lal/DetResponse.h>
#include <lal/LALDatatypes.h>
#include <lal/LALSimulation.h>
#include <lal/LALError.h>
#include <lal/XLALError.h>

// /* Matlab specific includes */
// #include "mex.h"
// #include "mat.h"

/* Debugging specific includes */
/* #include "memwatch.h" */

/* SH kit specific includes */
#include "SHtransforms.h"

/* SphRad specific includes */
#include "sphrad.h"
#include "sphrad_misc.h"
#include "sphrad_clust.h"
// #include "sphrad_response.h"
#include "sphrad_skymap.h"
#include "sphrad_stat.h"
#include "sphrad_ext.h"
#include "sphrad_coherent.h"

#include <gsl/gsl_const.h>


/* Structure initialisation: define pointers to our data persistant products */
static struct correlator_network_baselines *baselines = NULL;
static struct correlator_network_plan_fd *fdplans = NULL;
struct instrument **instruments = NULL;

/* Declare subroutines */
static void myExitFcn();

/* MATLAB gateway routine */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
        mexEvalString("drawnow;"); /* Make sure that all text has been written out */

        long startAnalysis, stopAnalysis;
        startAnalysis = clock();

        /* Want info? */
        if (nrhs == 0)
        {
                
                mexPrintf("Generate a sky map:\n");
                mexPrintf("\tskyMap = sphrad(0, 1, SHcoeffs, l_value, polar_value, thetaRes, phiRes\n");

                mexPrintf("Insert b coeffs into a:\n");
                mexPrintf("\tSHcoeffs = sphrad(0, 2, a_SHcoeffs, b_SHcoeffs, a_polar, b_polar, a_l_value, b_l_value\n");

                mexPrintf("Resize coeffs to given l value:\n");
                mexPrintf("\tSHcoeffs = sphrad(0, 3, SHcoeffs, polar, old_l_value, new_l_value)\n");

                mexPrintf("Generate a response map:\n");
                mexPrintf("\tresponseCoeffs = sphrad(0, 4, det1, det2 [,isPlus, maxL])\n");

                mexPrintf("reSize coeffiecients\n");
                mexPrintf("\tSHcoeffs = sphrad(0, 5, SHcoeffs, polar, old_l_value, new_l_value)\n");

                mexPrintf("Test FSHT forward:\n");
                mexPrintf("\tSHcoeffs = sphrad(0, 6, grid)\n");

                mexPrintf("Test FSHT inverse:\n");
                mexPrintf("\tgrid = sphrad(0, 7, shcoeffs)\n");

                mexPrintf("Cluster a tfmap:\n");
                mexPrintf("\t[clusterMap, clusterProps] = sphrad(0, 9, tfmap, threshold [, mergeDistance, debugLevel, codeVersion, coronalThreshold, nRegions, direction])\n");

                return;
        }

        int option = 1;    /* Default value - Generate a sky map */
        int idx;           /* Use for local loops */
        int i, j, k, n, m; /* Use for iterarative loops */
        int new_test = 0;
        mwSize n_instr; /* Number of data streams */

        /* Get number of data stream */
        n_instr = mxGetScalar(prhs[0]);

        /* If zero, then we are going to use a sub-function, not the cross-correlation code */
        if (n_instr == 0)
        {
                option = mxGetScalar(prhs[1]);
                mexPrintf("Option chosen: %i\n", option);
        }
        else
                mexPrintf("%i instruments passed in\n", n_instr);

        /* Check if exit function registered */
        if (mexAtExit(myExitFcn))
                mexPrintf("Error, myExitFcn not registered properly");

        /* Check the value of option, determine what to call*/
        if (option == 1)
        {

                mexPrintf("Code to sky map\n");

                /* Gridded output */
                int theta, thetaRes;
                int phi, phiRes;
                complex double grid;
                int l_value, polar;

                /* grid output */
                double *outGrid = NULL;
                double *outGridi = NULL;

                /* get pointers to the sh coeff */
                double *series = NULL;  /* real SH coeffs */
                double *seriesi = NULL; /* imaginary SH coeffs */

                /* Check number of input and output parameters */
                if (nrhs != 7)
                {
                        mexErrMsgIdAndTxt("SphRadiometer:InvSH:nrhs",
                                          "7 inputs required.",
                                          "skyMap = sphrad(0, 1, SHcoeffs, l_value, polar_value, thetaRes, phiRes");
                }

                /* check that number of rows in first input argument is 1*/
                if (mxGetM(prhs[2]) != 1)
                {
                        mexErrMsgIdAndTxt("SphRadiometer:gridding:notRowVector",
                                          "SHCoeffs must be a row vector.");
                }

                series = mxGetPr(prhs[2]);
                seriesi = mxGetPi(prhs[2]);
                l_value = mxGetScalar(prhs[3]);
                polar = mxGetScalar(prhs[4]);
                thetaRes = mxGetScalar(prhs[5]);
                phiRes = mxGetScalar(prhs[6]);

                mexPrintf("l_value: %i\npolar: %i\n", l_value, polar);
                mexPrintf("theta: %i\nphi: %i\n", thetaRes, phiRes);

                struct sh_series *sky = NULL;
                sky = sh_series_new_zero(l_value, polar);

                for (j = 0; j < sh_series_length(l_value, polar); j++)

                {
                        sky->coeff[j] = series[j] + seriesi[j] * I;
                        /*  mexPrintf("%g%gi  <->  ",sky->coeff[j]);*/
                }

                plhs[0] = mxCreateDoubleMatrix(thetaRes, phiRes, mxCOMPLEX);
                outGrid = mxGetPr(plhs[0]);
                outGridi = mxGetPi(plhs[0]);

                /* Gridded output */
                mxArray *pGrid = mxCreateDoubleMatrix(thetaRes, phiRes, mxREAL);

                for (theta = 0; theta < thetaRes; theta++)
                {
                        for (phi = 0; phi < phiRes; phi++)
                        {
                                grid = sh_series_eval(sky, ((double)theta / thetaRes) * M_PI, ((double)phi / phiRes) * M_PI * 2);
                                outGrid[theta + thetaRes * phi] = creal(grid);
                                outGridi[theta + thetaRes * phi] = cimag(grid);

                                if ((phi < 4) && (theta < 4))
                                        mexPrintf("(%g, %g) -(%d,%d) - (%g, %gi)\n", ((double)theta / thetaRes) * M_PI, ((double)phi / phiRes) * M_PI * 2, theta + 1, phi + 1, creal(grid), cimag(grid));
                        }
                }

                mexPrintf("Done\n");
                return;
        }
        else if (option == 2)
        {
                int idx;
                double *a_rcoeffs, *a_icoeffs;
                double *b_rcoeffs, *b_icoeffs;
                int a_polar, b_polar;
                int a_l_max, b_l_max;

                /* Check number of input and output parameters */
                if (nrhs != 8)
                {
                        mexErrMsgIdAndTxt("SphRadiometer:InvSH:nrhs",
                                          "8 inputs required.",
                                          "SHcoeffs = sphrad(0, 2, a_SHcoeffs, b_SHcoeffs, a_polar, b_polar, a_l_value, b_l_value");
                }

                /* check that number of rows in first input argument is 1*/
                if (mxGetM(prhs[2]) != 1)
                {
                        mexErrMsgIdAndTxt("SphRadiometer:gridding:notRowVector",
                                          "a_SHcoeffs must be a row vector.");
                }

                /* check that number of rows in first input argument is 1*/
                if (mxGetM(prhs[3]) != 1)
                {
                        mexErrMsgIdAndTxt("SphRadiometer:gridding:notRowVector",
                                          "b_SHcoeffs must be a row vector.");
                }

                a_polar = mxGetScalar(prhs[2]);
                b_polar = mxGetScalar(prhs[3]);
                a_l_max = mxGetScalar(prhs[4]);
                b_l_max = mxGetScalar(prhs[5]);

                mexPrintf("aS: %i\nbS: %i\naL: %i\nbL: %i\n", a_polar, b_polar, a_l_max, b_l_max);

                int m_max = b_polar ? 0 : b_l_max;
                int l, m;
                complex double z = 1;

                /* mex SH output */
                double *rcoeffs = NULL;
                double *icoeffs = NULL;

                struct sh_series *a_coeffs = NULL;
                struct sh_series *b_coeffs = NULL;
                struct sh_series *out_coeffs = NULL;

                a_coeffs = sh_series_new_zero(a_l_max, a_polar);
                b_coeffs = sh_series_new_zero(b_l_max, b_polar);
                out_coeffs = sh_series_new_zero(a_l_max, a_polar);

                /* now get pointers to the spherical harmonics coefficients*/
                a_rcoeffs = mxGetPr(prhs[0]);
                a_icoeffs = mxGetPi(prhs[0]);

                b_rcoeffs = mxGetPr(prhs[1]);
                b_icoeffs = mxGetPi(prhs[1]);

                plhs[0] = mxCreateDoubleMatrix(mxGetN(prhs[0]), 1, mxCOMPLEX);
                rcoeffs = mxGetPr(plhs[0]);
                icoeffs = mxGetPi(plhs[0]);

                mexPrintf("Starting add\n");

                for (idx = 0; idx < mxGetN(prhs[0]); idx++)
                {
                        a_coeffs->coeff[idx] = a_rcoeffs[idx] + a_icoeffs[idx] * I;
                }

                for (idx = 0; idx < mxGetN(prhs[1]); idx++)
                {
                        b_coeffs->coeff[idx] = b_rcoeffs[idx] + b_icoeffs[idx] * I;
                }

                mexPrintf("Copied SH\n");

                if ((a_l_max < b_l_max) || (a_polar && !b_polar))
                {
                        mexPrintf("Cannot insert b into a, as b is larger\n");
                        return;
                }

                for (m = -m_max; m <= m_max; m++)
                {
                        complex double *dst = (complex double *)a_coeffs + sh_series_moffset(a_l_max, m);
                        complex double *src = (complex double *)b_coeffs + sh_series_moffset(b_l_max, m);
                        for (l = abs(m); l <= (int)b_l_max; l++)
                                *dst++ += z * *src++;
                }

                mexPrintf("Testout : %g-%gi\n", creal(a_coeffs->coeff[0]), cimag(a_coeffs->coeff[0]));

                /*
                   for(m = -m_max; m <= m_max; m++)
                   {
                        double *dstr = a_rcoeffs + sh_series_moffset(a_l_max, m);
                        double *srcr = b_rcoeffs + sh_series_moffset(b_l_max, m);
                        for(l = abs(m); l <= (int) b_l_max; l++)
                   *rcoeffs++ = *dstr++ + z* *srcr++;

                        double *dsti = a_icoeffs + sh_series_moffset(a_l_max, m);
                        double *srci = b_icoeffs + sh_series_moffset(b_l_max, m);
                        for(l = abs(m); l <= (int) b_l_max; l++)
                   *icoeffs++ = *dsti++ + z* *srci++;

                   }
                 */

                for (idx = 0; idx < mxGetN(prhs[0]); idx++)
                {
                        rcoeffs[idx] = creal(a_coeffs->coeff[idx]);
                        icoeffs[idx] = cimag(a_coeffs->coeff[idx]);
                }

                out_coeffs = sh_series_add_into(a_coeffs, 1, b_coeffs);
        }
        else if (option == 3)
        {
                /* Take SH coefficients and resize them to the given l value */
                int idx;
                double *a_rcoeffs, *a_icoeffs;
                int polar;
                int l_value;
                int new_l_value;

                /* Check number of input and output parameters */
                if (nrhs != 6)
                {
                        mexErrMsgIdAndTxt("SphRadiometer:InvSH:nrhs",
                                          "6 inputs required.",
                                          "SHcoeffs = sphrad(0, 3, SHcoeffs, polar, old_l_value, new_l_value)");
                }

                /* check that number of rows in first input argument is 1*/
                if (mxGetM(prhs[2]) != 1)
                {
                        mexErrMsgIdAndTxt("SphRadiometer:gridding:notRowVector",
                                          "SHcoeffs must be a row vector.");
                }

                polar = mxGetScalar(prhs[3]);
                l_value = mxGetScalar(prhs[4]);
                new_l_value = mxGetScalar(prhs[5]);

                mexPrintf("Polar: %i\nl_value: %i\nnew_l_value: %i\n", polar, l_value, new_l_value);

                /* mex SH output */
                double *rcoeffs = NULL;
                double *icoeffs = NULL;

                struct sh_series *a_coeffs = NULL;
                struct sh_series *b_coeffs = NULL;
                struct sh_series *out_coeffs = NULL;

                a_coeffs = sh_series_new_zero(l_value, polar);
                b_coeffs = sh_series_new_zero(new_l_value, polar);
                out_coeffs = sh_series_new_zero(new_l_value, polar);

                /* now get pointers to the spherical harmonics coefficients*/
                a_rcoeffs = mxGetPr(prhs[2]);
                a_icoeffs = mxGetPi(prhs[2]);

                plhs[0] = mxCreateDoubleMatrix(sh_series_length(new_l_value, polar), 1, mxCOMPLEX);
                rcoeffs = mxGetPr(plhs[0]);
                icoeffs = mxGetPi(plhs[0]);

                mexPrintf("Starting add\n");

                for (idx = 0; idx < mxGetN(prhs[2]); idx++)
                {
                        a_coeffs->coeff[idx] = a_rcoeffs[idx] + a_icoeffs[idx] * I;
                }

                mexPrintf("Copied SH\n");

                b_coeffs = sh_series_resize(a_coeffs, new_l_value);
                if (b_coeffs == NULL)
                {
                        mexPrintf("Resize failed, aborting\n");
                        return;
                }

                for (idx = 0; idx < sh_series_length(new_l_value, polar); idx++)
                {
                        mexPrintf("%g\t", creal(b_coeffs->coeff[idx]));
                        mexPrintf("%g\t", creal(a_coeffs->coeff[idx]));

                        rcoeffs[idx] = creal(b_coeffs->coeff[idx]);
                        icoeffs[idx] = cimag(b_coeffs->coeff[idx]);
                }
        }
        else if (option == 4)
        {
                double fplus1, fplus2, fplus3;
                double fcross1, fcross2, fcross3;

                const LALDetector *detectors[3];
                struct instrument *instruments[3];

                double theta, phi;
                int crossDet[2], isPlus;
                int k, i;

                double *outMatrix2 = NULL;  /* output matrix */
                double *outMatrixi2 = NULL; /* output matrix */

                char *detName[3] = {"H1", "L1", "V2"};
                char *pType[2] = {"Cross", "Plus"};

                int max_l = 90;
                crossDet[0] = mxGetScalar(prhs[2]);
                crossDet[1] = mxGetScalar(prhs[3]);

                if (nrhs < 4)
                        isPlus = 1;
                else
                        isPlus = mxGetScalar(prhs[4]);

                if (nrhs == 6)
                        max_l = mxGetScalar(prhs[5]);

                mexPrintf("Generating %s response for %s-%s, with %d coefficients\n", pType[isPlus], detName[crossDet[0]], detName[crossDet[1]], max_l);

                /* Set instruments up */
                instruments[0] = instrument_from_name("H1");
                instruments[1] = instrument_from_name("L1");
                instruments[2] = instrument_from_name("V2");

                /* Detectors */
                detectors[0] = XLALDetectorPrefixToLALDetector("H1");
                detectors[1] = XLALDetectorPrefixToLALDetector("L1");
                detectors[2] = XLALDetectorPrefixToLALDetector("V2");

                for (i = 0; i < 3; i++)
                {
                        mexPrintf("Detector %d:\n", i + 1);
                        mexPrintf("\tLat: %f", detectors[i]->frDetector.vertexLatitudeRadians);
                        mexPrintf("\tLong: %f\n", detectors[i]->frDetector.vertexLongitudeRadians);
                }

                /* Assign antenna reponse SH coefficients to output matrix */
                struct sh_series *response;

                /*  response = response3(detectors[0], detectors[1], detectors[2],max_l,det1,det2, isPlus);*/

                struct detector_response *HL = NULL;
                HL = detector_response_new(detectors, 3, max_l * 2);

                response = responseN(detectors, 3, max_l, crossDet, isPlus);

                mexPrintf("Response order: %d\n", response->l_max);

                mexPrintf("FULL(x): 1) (%g) 2) (%g) 3) (%g)\n", creal(HL[0].cross->coeff[0]), creal(HL[0].cross->coeff[1]), creal(HL[0].cross->coeff[2]));
                if (isPlus == 1)
                        mexPrintf("Cross:  1) (%g) 2) (%g) 3) (%g)\n", creal(response->coeff[0]), creal(response->coeff[1]), creal(response->coeff[2]));

                mexPrintf("FULL(+):  1) (%g) 2) (%g) 3) (%g)\n", creal(HL[0].plus->coeff[0]), creal(HL[0].plus->coeff[1]), creal(HL[0].plus->coeff[2]));
                if (isPlus == 0)
                        mexPrintf("Plus:     1) (%g) 2) (%g) 3) (%g)\n", creal(response->coeff[0]), creal(response->coeff[1]), creal(response->coeff[2]));

                mexPrintf("Generated response\n");

                plhs[0] = mxCreateDoubleMatrix(sh_series_length(response->l_max, response->polar), 1, mxCOMPLEX);
                outMatrix2 = mxGetPr(plhs[0]);
                outMatrixi2 = mxGetPi(plhs[0]);

                for (k = 0; k < sh_series_length(response->l_max, response->polar); k++)
                {
                        outMatrix2[k] = creal(response->coeff[k]);
                        outMatrixi2[k] = cimag(response->coeff[k]);
                }
                sh_series_free(response);

                detector_response_free(HL);

                instrument_free(instruments[2]);
                instrument_free(instruments[1]);
                instrument_free(instruments[0]);
        }
        else if (option == 5)
        {

                /* Take SH coefficients and resize them to the given l value */
                int idx;
                double *a_rcoeffs, *a_icoeffs;
                int polar;
                int l_value;
                int new_l_value;

                /* Check number of input and output parameters */
                if (nrhs != 6)
                {
                        mexErrMsgIdAndTxt("SphRadiometer:InvSH:nrhs",
                                          "6 inputs required.",
                                          "SHcoeffs = sphrad(0, 5, SHcoeffs, polar, old_l_value, new_l_value)");
                }

                /* check that number of rows in first input argument is 1*/
                if (mxGetM(prhs[2]) != 1)
                {
                        mexErrMsgIdAndTxt("SphRadiometer:gridding:notRowVector",
                                          "SHcoeffs must be a row vector.");
                }

                polar = mxGetScalar(prhs[3]);
                l_value = mxGetScalar(prhs[4]);
                new_l_value = mxGetScalar(prhs[5]);

                mexPrintf("Polar: %i\nl_value: %i\nnew_l_value: %i\n", polar, l_value, new_l_value);

                /* mex SH output */
                double *rcoeffs = NULL;
                double *icoeffs = NULL;

                struct sh_series *a_coeffs = NULL;
                struct sh_series *b_coeffs = NULL;
                struct sh_series *out_coeffs = NULL;

                a_coeffs = sh_series_new_zero(l_value, polar);
                b_coeffs = sh_series_new_zero(new_l_value, polar);
                out_coeffs = sh_series_new_zero(new_l_value, polar);

                /* now get pointers to the spherical harmonics coefficients*/
                a_rcoeffs = mxGetPr(prhs[2]);
                a_icoeffs = mxGetPi(prhs[2]);

                plhs[0] = mxCreateDoubleMatrix(sh_series_length(new_l_value, polar), 1, mxCOMPLEX);
                rcoeffs = mxGetPr(plhs[0]);
                icoeffs = mxGetPi(plhs[0]);

                mexPrintf("Starting add\n");

                for (idx = 0; idx < mxGetN(prhs[2]); idx++)
                {
                        a_coeffs->coeff[idx] = a_rcoeffs[idx] + a_icoeffs[idx] * I;
                }

                mexPrintf("Copied SH\n");

                b_coeffs = sh_series_resize(a_coeffs, new_l_value);
                if (b_coeffs == NULL)
                {
                        mexPrintf("Resize failed, aborting\n");
                        return;
                }

                for (idx = 0; idx < sh_series_length(new_l_value, polar); idx++)
                {
                        mexPrintf("%g-%gi\t", creal(b_coeffs->coeff[idx]), cimag(b_coeffs->coeff[idx]));
                        mexPrintf("%g-%gi\t", creal(a_coeffs->coeff[idx]), cimag(a_coeffs->coeff[idx]));

                        rcoeffs[idx] = creal(b_coeffs->coeff[idx]);
                        icoeffs[idx] = cimag(b_coeffs->coeff[idx]);
                }
        }
        else if (option == 6)
        {
                /* Test my SHtransform library */

                /* mex SH output */
                double *rData = NULL;
                double *iData = NULL;

                double *rCoeffs = NULL;
                double *iCoeffs = NULL;

                int gRows, gCols;
                int i, j, k;

                /* Check number of input and output parameters */
                if (!(nrhs == 3 || nrhs == 5))
                {
                        mexErrMsgIdAndTxt("SphRadiometer:InvSH:nrhs",
                                          "3 inputs required.",
                                          "SHcoeffs = sphrad(0, 6, grid [,GPSstarttime, extraGPSTime])");
                }

                /* check that number of rows in first input argument is 1*/
                gRows = mxGetM(prhs[2]);
                gCols = mxGetN(prhs[2]);

                complex double grid[gRows * gCols];
                complex double *shcoeffs;
                complex double *grid2;
                double *inGrid;

                mexPrintf("Testing\n");

                shcoeffs = (complex double *)malloc(sizeof(complex double) * sh_series_length(gRows / 2, 0));
                grid2 = (complex double *)malloc(sizeof(complex double) * (gRows * gCols));

                inGrid = (double *)malloc(sizeof(double) * gRows * gCols);

                /* now get pointers to real/imaginary parts*/
                rData = mxGetPr(prhs[2]);
                iData = mxGetPi(prhs[2]);

                if (iData == NULL)
                {
                        iData = (double *)malloc(sizeof(double) * (gRows * gCols));
                        memset(iData, 0, gRows * gCols * sizeof(double));
                }

                mexPrintf("Sizes: %i x %i\n", gRows, gCols);
                mexPrintf("First: %f-%fi\nSecond: %f-%fi\n", rData[0], iData[0], rData[1], iData[1]);

                for (i = 0; i < gRows; i++)
                        for (j = 0; j < gCols; j++)
                                grid[i + j * gCols] = rData[i + j * gCols] + I * iData[i + j * gCols];

                mexPrintf("First: %g-%gi\nSecond: %g-%gi\n", creal(grid[0]), cimag(grid[0]), creal(grid[1]), cimag(grid[1]));

                /* Test forward transform */
                FSHT_forward(shcoeffs, grid, gRows);

                plhs[0] = mxCreateDoubleMatrix((gRows / 2) * (gRows / 2), 1, mxCOMPLEX);
                rData = mxGetPr(plhs[0]);
                iData = mxGetPi(plhs[0]);

                for (i = 0; i < ((gRows / 2) * (gRows / 2)); i++)
                {
                        rData[i] = creal(shcoeffs[i]);
                        iData[i] = cimag(shcoeffs[i]);
                }

                /* Test inverse transform */
                FSHT_inverse(grid2, shcoeffs, gRows / 2);

                plhs[1] = mxCreateDoubleMatrix(gRows, gCols, mxCOMPLEX);
                rCoeffs = mxGetPr(plhs[1]);
                iCoeffs = mxGetPi(plhs[1]);

                for (i = 0; i < (gRows * gRows); i++)
                {
                        rCoeffs[i] = creal(grid2[i]);
                        iCoeffs[i] = cimag(grid2[i]);
                        inGrid[i] = creal(grid[i]);
                }

                /* Test the rotation code */
                if (nrhs == 5)
                {
                        double *rotGrid;
                        int startGPSTime;
                        double diffGPSTime;

                        startGPSTime = (int)mxGetScalar(prhs[3]);
                        diffGPSTime = mxGetScalar(prhs[4]);
                        mexPrintf("GPSTime: %d, diff time: %g\n", startGPSTime, diffGPSTime);

                        rotGrid = gps_rotate(inGrid, gRows, startGPSTime, diffGPSTime);

                        plhs[2] = mxCreateDoubleMatrix(gRows, gCols, mxREAL);
                        rCoeffs = mxGetPr(plhs[2]);

                        for (i = 0; i < (gRows * gRows); i++)
                                rCoeffs[i] = rotGrid[i];

                        free(rotGrid);
                }

                /*free(grid);*/
                free(shcoeffs);
                free(grid2);
                free(inGrid);
        }
        else if (option == 7)
        {
                /* Test my SHtransform library */
                /* Coeffs -> grid -> coeffs
                /* mex SH output */
                double *rData = NULL;
                double *iData = NULL;

                double *rCoeffs = NULL;
                double *iCoeffs = NULL;

                int gRows, gCols;
                int bw, size;
                int i, j, k;

                /* Check number of input and output parameters */
                if (nrhs != 3)
                {
                        mexErrMsgIdAndTxt("SphRadiometer:InvSH:nrhs",
                                          "3 inputs required.",
                                          "SHcoeffs = sphrad(0, 7, shcoeffs)");
                }

                /* check that number of rows in first input argument is 1*/
                gRows = mxGetM(prhs[2]);
                gCols = mxGetN(prhs[2]);

                if ((gRows == 0) || (gCols == 0))
                        return;

                mexPrintf("Input size: %d x %d\n", gRows, gCols);

                if (gRows < gCols)
                        bw = (int)sqrt(gCols);
                else
                        bw = (int)sqrt(gRows);

                size = 2 * bw;

                complex double *shcoeffs;
                complex double *grid2;

                shcoeffs = (complex double *)malloc(sizeof(complex double) * bw * bw);
                grid2 = (complex double *)malloc(sizeof(complex double) * size * size);

                if ((shcoeffs == NULL) || (grid2 == NULL))
                        return;

                /* now get pointers to real/imaginary parts*/
                rData = mxGetPr(prhs[2]);
                iData = mxGetPi(prhs[2]);

                if (iData == NULL)
                {
                        iData = (double *)malloc(sizeof(double) * bw * bw);
                        memset(iData, 0, bw * bw * sizeof(double));
                }

                mexPrintf("Sizes: %i x %i\n", gRows, gCols);
                mexPrintf("First: %f-%fi\nSecond: %f-%fi\n", rData[0], iData[0], rData[1], iData[1]);

                for (i = 0; i < (bw * bw); i++)
                        shcoeffs[i] = rData[i] + I * iData[i];

                mexPrintf("First: %g-%gi\nSecond: %g-%gi\n", creal(shcoeffs[0]), cimag(shcoeffs[0]), creal(shcoeffs[1]), cimag(shcoeffs[1]));

                /* Test inverse transform */
                FSHT_inverse(grid2, shcoeffs, bw);

                plhs[0] = mxCreateDoubleMatrix(size, size, mxCOMPLEX);
                rCoeffs = mxGetPr(plhs[0]);
                iCoeffs = mxGetPi(plhs[0]);

                for (i = 0; i < (size * size); i++)
                {
                        rCoeffs[i] = creal(grid2[i]);
                        iCoeffs[i] = cimag(grid2[i]);
                }

                /* Test forward transform */
                FSHT_forward(shcoeffs, grid2, size);

                /* Test inverse transform */
                FSHT_inverse(grid2, shcoeffs, bw);

                /* Test forward transform */
                FSHT_forward(shcoeffs, grid2, size);

                /* Test inverse transform */
                FSHT_inverse(grid2, shcoeffs, bw);

                /* Test forward transform */
                FSHT_forward(shcoeffs, grid2, size);

                /* Test inverse transform */
                FSHT_inverse(grid2, shcoeffs, bw);

                /* Test forward transform */
                FSHT_forward(shcoeffs, grid2, size);

                plhs[1] = mxCreateDoubleMatrix(bw * bw, 1, mxCOMPLEX);
                rData = mxGetPr(plhs[1]);
                iData = mxGetPi(plhs[1]);

                for (i = 0; i < (bw * bw); i++)
                {
                        rData[i] = creal(shcoeffs[i]);
                        iData[i] = cimag(shcoeffs[i]);
                }

                free(shcoeffs);
                free(grid2);
        }
        else if (option == 8)
        {
                int i, j, k;

                int numberTimeShifts = 8;
                int tau = 4;
                int T = 32;
                int nInstr = 3;
                int timeShifts[numberTimeShifts];
                for (i = 0; i < numberTimeShifts; i++)
                        timeShifts[i] = i * tau;

                mexPrintf("TimeShifts: \n\t");
                for (i = 0; i < numberTimeShifts; i++)
                        mexPrintf("%i: %i  ", i, timeShifts[i]);

                mexPrintf("\tT: %d samples\n\ttau: %d s\n", T, tau);

                /* "double" MUST occur 7 times. */
                double nShifts = (double)/*T / tau*/ numberTimeShifts - 1;

                mexPrintf("First nShifts: %f\n", nShifts);
                int totalShifts = pow(nShifts, nInstr - 1) - nShifts;

                mexPrintf("Rounded nShifts, %d of %d seconds\n", (int)totalShifts, timeShifts[1]);

                /* Det2 not 0, Det3 not 0, not Det2 */
                mexPrintf("Time shifts to apply\n");
                int detIdx[nInstr];
                for (i = 0; i < nInstr; i++)
                        detIdx[i] = 0;

                int pretty = 0;
                int diff = 0;
                int lags[totalShifts][nInstr - 1];

                for (i = 0; i < totalShifts; i++)
                { /*
                        0 - 00

                        1 - 4,8		7 - 8 , 4	13 - 12 , 4	19 - 16 , 4  [,8,12,20,24,28]
                        2 - 4,12	8 - 8 , 12	14 - 12 , 8	20 - 16 , 8  [,4,12,20,24,28]
                        3 - 4,16	9 - 8 , 16	15 - 12 , 16	21 - 16 , 12 [,4,8,20,24,28]
                        4 - 4,20	10- 8 , 20	16 - 12 , 20	22 - 16 , 20 [,4,8,12, 24,28]
                        5 - 4,24	11- 8 , 24	17 - 12 , 24	23 - 16 , 24 [,4,8,12,20,28]
                        6 - 4,28	12- 8 , 28	18 - 12 , 28	24 - 16 , 28 [,4,8,12,20,24]

                        Pick first non-zero time shift for second detector - 4s, idx 1
                        - pick first non zero, non second detector value for third 8s, idx 2
                        - pick ....
                        - pick next non zero, non second detector value for third, until end reached, 12s, idx 3
                        - pick ...
                        Pick next non-zero time shift for 2nd detector - 8s, idx 2
                        - pick - 2s, idx 1
                        -pick ...

                        Must ignore zero lag analysis, remove it from the vector list and subtract one from the
                        number of shifts.

                        timeShift vector

                        DETECTOR 2:
                        floor(shiftIdx/numShifts)+1

                        This gives 1 - [0 (numShifts-1)], 2 - [numShifts (numshifts*2 -1)] etc
                        which equates to a direct index value

                        DETECTOR 3:
                        shiftIdx%numShifts

                        */

                        detIdx[0] = floor(i / (nShifts - 1)) + 1;
                        detIdx[1] = i % (int)(nShifts - 1) + 1;

                        if (pretty != detIdx[0])
                        {
                                mexPrintf("\n");
                                diff = 0;
                        }

                        if (detIdx[1] == detIdx[0])
                        {
                                diff += 1;
                        }

                        mexPrintf(" (%d: %d, %d) ", i, detIdx[0] * tau, (detIdx[1] + diff) * tau);
                        pretty = detIdx[0];

                        lags[i][0] = detIdx[0];
                        lags[i][1] = detIdx[1] + diff;

                        mexPrintf(" (%d: %d, %d) ", i, lags[i][0] * tau, (lags[i][1]) * tau);
                }
                mexPrintf("\n");

                /*
                int *tLags = generate_timelags(&timeShifts,numberTimeShifts, tau, T,3);

                tLags = get_lagIndex(1, numberTimeShifts, tau, T, nInstr);
                mexPrintf("Index: 1 \n");

                mexPrintf("Det2: %d \t",4*tLags[0]);
                mexPrintf("Det3: %d\n",4*tLags[1]);


                tLags=get_lagIndex(8, numberTimeShifts, tau, T, nInstr);
                mexPrintf("Index: 8 \n");

                mexPrintf("Det2: %d \t",4*tLags[0]);
                mexPrintf("Det3: %d\n",4*tLags[1]);

                if (nrhs ==3)
                {
                tLags=get_lagIndex((int)mxGetScalar(prhs[2]), numberTimeShifts, tau, T, nInstr);
                mexPrintf("Index: %d \n", (int)mxGetScalar(prhs[2]));

                mexPrintf("Det2: %d \t",4*tLags[0]);
                mexPrintf("Det3: %d\n",4*tLags[1]);
                }
                */
        }
        else if (option == 9)
        {
                /* Test clustering algorithm */
                /* Input - tfmap */
                double *tfmap = NULL;
                int xSize;
                int ySize;
                double prc[2];
                double threshold[2];
                int mergeDistance = 0; /* Default of zero */
                int debugLevel = 1;    /* Highest level - 0 is lowest */
                int codeVersion = 0;   /* Default to old code */
                int nRegions = 1;      /* Default to whole map */
                int direction = 0;     /* Default to column span */

                /* Output - clusterMap and properties */
                int numElements = 10; /* Number of elements present in the cluster structure */

                int numClusters = 0;
                int *clusterMap = NULL;
                double *clusterStruct = NULL;

                double *returnMatrix = NULL;
                double *returnStruct = NULL;
                int i;

                /* Check number of input and output parameters */
                if (nrhs < 4)
                {
                        mexErrMsgIdAndTxt("SphRadiometer:InvSH:nrhs",
                                          "4 inputs required.",
                                          "[clusterMap, clusterProps] = sphrad(0, 9, tfmap, prc1 [, mergeDistance, debugLevel, codeVersion, prc2, nRegions])");
                }

                /* Get dimensions of tfmap */
                xSize = mxGetM(prhs[2]);
                ySize = mxGetN(prhs[2]);

                prc[0] = mxGetScalar(prhs[3]);
                prc[1] = prc[0];

                if (nrhs >= 5)
                {
                        mergeDistance = mxGetScalar(prhs[4]);
                        if (mergeDistance < 0)
                        {
                                mexPrintf("Illegeal negative value request.\n\t Setting merge distance to 0\n");
                                mergeDistance = 0;
                        }
                }

                if (nrhs >= 6)
                {
                        debugLevel = mxGetScalar(prhs[5]);
                        if (debugLevel > 2)
                        {
                                mexPrintf("Non-standard debug level specified (should be 0 (LOW) or 1(HIGH)).\n\t Setting it to HIGH\n");
                                debugLevel = 1;
                        }
                }

                if (nrhs >= 7)
                {
                        codeVersion = mxGetScalar(prhs[6]);
                        if (codeVersion > 2)
                        {
                                mexPrintf("Non-standard code version specified (should be 0 (OLD) or 1(NEW)).\n\t Setting it to OLD\n");
                                codeVersion = 0;
                        }
                }

                if (nrhs >= 8)
                        prc[1] = mxGetScalar(prhs[7]);

                if (nrhs >= 9)
                        nRegions = mxGetScalar(prhs[8]);

                if (nrhs >= 10)
                        direction = mxGetScalar(prhs[9]);

                mexPrintf("Input size: %d x %d\n", xSize, ySize);
                mexPrintf("Percentage: %g (%g)\n", prc[0], prc[1]);
                mexPrintf("Merge distance: %d\n", mergeDistance);
                mexPrintf("Debug level: %d\n", debugLevel);
                mexPrintf("Code version: %d\n", codeVersion);
                mexPrintf("Number of  regions: %d\n", nRegions);
                mexPrintf("Span direction: %d\n", direction);

                /* now get pointers to data*/
                tfmap = mxGetPr(prhs[2]);

                /* Call clustering code */
                /* To get out multiple arrays, we need too pass in the address of the pointer.
                * Thus, we define the pointer as int *clusterMap, and then pass in the pointers
                * address to the function using &clusterStruct.
                * Inside the function, once we have malloced the memory for the array, we assign
                * the address of the first element of the array to our pointer:
                *      *clusterMapPtr = clusterMap;
                *
                * For  double *clusterStruct, need to state that it is a pointer to an double array/
                * Thus, declare it as double **clusterStructPtr, and assign using:
                *	*clusterStructPtr = clusterStruct;
                * Then, when the function returns, the pointer now points at the malloced memory
                */

                double *sortedArray = NULL;
                sortedArray = (double *)malloc(xSize * ySize * sizeof(double));
                memcpy(sortedArray, tfmap, xSize * ySize * sizeof(double));

                threshold[0] = prcValue(sortedArray, prc[0], xSize * ySize, 1);
                threshold[1] = prcValue(sortedArray, prc[1], xSize * ySize, 0);

                mexPrintf("\nPcrtile value for %g (%g) -> %g (%g)\n", prc[0], prc[1], threshold[0], threshold[1]);

                /* Generate the percentage values
                *	- if we have one threshold, or they are the same, perform nearest neighbour clustering
                *	- if we have two unique values then we are performing coronal clustering
                */
                if (nRegions == 0)
                        numClusters = pixel_chain(&clusterMap, &clusterStruct, tfmap, xSize, ySize, threshold, mergeDistance, debugLevel, codeVersion);
                else
                        numClusters = regional_pixel_chain(&clusterMap, &clusterStruct, tfmap, xSize, ySize, prc, nRegions, direction, mergeDistance, debugLevel, codeVersion);

                mexPrintf("\n%d clusters", numClusters);
                mexPrintf(" for pcrtile value %g (%g) -> %g (%g)\n", prc[0], prc[1], threshold[0], threshold[1]);

                if (clusterMap != NULL)
                {
                        /* If the number of clusters is less then our followup brief, use all of them *
                        qsort(clusterStruct, 8, numClusters*sizeof(double), cmpMultiDouble);
                        */
                        if (nlhs > 0)
                        {
                                mexPrintf("Assigning cluster map\n");

                                /* Assign output pointer */
                                plhs[0] = mxCreateDoubleMatrix(xSize, ySize, mxREAL);
                                returnMatrix = mxGetPr(plhs[0]);

                                for (i = 0; i < ((xSize) * (ySize)); i++)
                                        returnMatrix[i] = (double)clusterMap[i];
                        }
                        free(clusterMap);
                }

                if (clusterStruct != NULL)
                {
                        if (nlhs > 1)
                        {
                                mexPrintf("Assigning cluster properties\n");
                                plhs[1] = mxCreateDoubleMatrix(numElements, numClusters, mxREAL);
                                returnStruct = mxGetPr(plhs[1]);

                                for (i = 0; i < (numClusters * numElements); i++)
                                        returnStruct[i] = clusterStruct[i];
                        }
                        free(clusterStruct);
                        free(sortedArray);
                }
                mexPrintf("Finished\n");
        }
        else if (option == 10)
        {
                double check = mxGetScalar(prhs[2]);
                mexPrintf("Checking number %g for Nan: %d\n", check, check_isnan(check));
                mexPrintf("Checking number %g for Inf: %d\n", check, check_isinf(check));
        }
        else if (option == 11)
        {
                /* Test my convolution function  */
                /* Coeffs -> grid -> coeffs */

                /* mex SH info */
                double *rData = NULL;
                double *iData = NULL;
                double *rData2 = NULL;
                double *iData2 = NULL;

                /* mex SH output */
                double *rCoeffs = NULL;
                double *iCoeffs = NULL;

                int gRows, gCols;
                int bw, size;
                int i, j, k;

                /* Check number of input and output parameters */
                if (nrhs != 5)
                {
                        mexErrMsgIdAndTxt("SphRadiometer:InvSH:nrhs",
                                          "5 inputs required.",
                                          "SHcoeffs = sphrad(0, 7, shcoeffs, shcoeffs2)");
                }

                /* check that number of rows in first input argument is 1*/
                gRows = mxGetM(prhs[2]);
                gCols = mxGetN(prhs[2]);

                if ((gRows == 0) || (gCols == 0))
                        return;

                mexPrintf("Input size: %d x %d\n", gRows, gCols);

                if (gRows < gCols)
                        bw = (int)sqrt(gCols);
                else
                        bw = (int)sqrt(gRows);

                size = 2 * bw;

                struct sh_series *shcoeffs, *shcoeffs2, *outSHCoeffs;
                complex double *grid2;

                outSHCoeffs = sh_series_new_zero(bw - 1, 0);
                shcoeffs = sh_series_new_zero(bw - 1, 0);
                shcoeffs2 = sh_series_new_zero(bw - 1, 0);

                if ((shcoeffs == NULL) || (shcoeffs2 == NULL) || (outSHCoeffs == NULL))
                        return;

                /* now get pointers to real/imaginary parts*/
                rData = mxGetPr(prhs[2]);
                iData = mxGetPi(prhs[2]);

                rData2 = mxGetPr(prhs[3]);
                iData2 = mxGetPi(prhs[3]);

                if (iData == NULL)
                {
                        iData = (double *)malloc(sizeof(double) * bw * bw);
                        memset(iData, 0, bw * bw * sizeof(double));
                }

                if (iData2 == NULL)
                {
                        iData2 = (double *)malloc(sizeof(double) * bw * bw);
                        memset(iData2, 0, bw * bw * sizeof(double));
                }

                mexPrintf("Sizes: %i x %i\n", gRows, gCols);
                mexPrintf("First: %f-%fi\nSecond: %f-%fi\n", rData[0], iData[0], rData[1], iData[1]);

                for (i = 0; i < (bw * bw); i++)
                {
                        shcoeffs->coeff[i] = rData[i] + I * iData[i];
                        shcoeffs2->coeff[i] = rData2[i] + I * iData2[i];
                }

                outSHCoeffs = convolve_sh_series(shcoeffs, shcoeffs2);

                if (outSHCoeffs == NULL)
                        return;

                plhs[0] = mxCreateDoubleMatrix(sh_series_length(outSHCoeffs->l_max, outSHCoeffs->polar), 1, mxCOMPLEX);
                rData = mxGetPr(plhs[0]);
                iData = mxGetPi(plhs[0]);

                for (i = 0; i < (bw * bw); i++)
                {
                        rData[i] = creal(outSHCoeffs->coeff[i]);
                        iData[i] = cimag(outSHCoeffs->coeff[i]);
                }

                sh_series_free(shcoeffs);
                sh_series_free(shcoeffs2);
        }
        else if (option == 12)
        {

                /* Test sky position locator
                * Given a skymap, this returns the (x,y) coordinate of
                * the loudest pixel_chain
                */

                /* Check number of input and output parameters */
                if (nrhs != 3)
                {
                        mexErrMsgIdAndTxt("SphRadiometer:InvSH:nrhs",
                                          "3 inputs required.",
                                          "sphrad(0, 12, skymap)");
                }

                /* Get dimensions of skymap */
                int gRows, gCols;
                gRows = mxGetM(prhs[2]);
                gCols = mxGetN(prhs[2]);

                /* Return if empty */
                if ((gRows == 0) || (gCols == 0))
                        return;

                mexPrintf("Input size: %d x %d\n", gRows, gCols);

                /* now get pointer to skymap*/
                double *rData = NULL;
                rData = mxGetPr(prhs[2]);

                double *position;
                position = sky_position(rData, gRows, gCols, 94669200, 0.0);

                mexPrintf("Location of maximum pixel power is: (%g, %g) -> %g\n", position[0], position[1], position[2]);

                free(position);
        }
        else if (option == 13)
        {
                /* Checking the new antenna response function for speed and memory issues */

                double fplus1, fplus2, fplus3;
                double fcross1, fcross2, fcross3;

                const LALDetector *detectors[3];
                struct instrument *instruments[3];

                int nBaselines = 6;
                int nInstr = 3;

                double theta, phi;
                int crossDet[2], isPlus;
                int k, i;

                double *outMatrix2 = NULL;  /* output matrix */
                double *outMatrixi2 = NULL; /* output matrix */

                char *detName[3] = {"H1", "L1", "V2"};
                char *pType[2] = {"Cross", "Plus"};

                int max_l = 90;

                crossDet[0] = mxGetScalar(prhs[2]);
                crossDet[1] = mxGetScalar(prhs[3]);

                if (nrhs < 4)
                        isPlus = 1;
                else
                        isPlus = mxGetScalar(prhs[4]);

                if (nrhs == 6)
                        max_l = mxGetScalar(prhs[5]);

                mexPrintf("Generating %s response for %s-%s, with %d coefficients\n", pType[isPlus], detName[crossDet[0]], detName[crossDet[1]], max_l);

                /* Set instruments up */
                instruments[0] = instrument_from_name("H1");
                instruments[1] = instrument_from_name("L1");
                instruments[2] = instrument_from_name("V2");

                /* Detectors */
                detectors[0] = XLALDetectorPrefixToLALDetector("H1");
                detectors[1] = XLALDetectorPrefixToLALDetector("L1");
                detectors[2] = XLALDetectorPrefixToLALDetector("V2");

                for (i = 0; i < 3; i++)
                {
                        mexPrintf("Detector %d:\n", i + 1);
                        mexPrintf("\tLat: %f", detectors[i]->frDetector.vertexLatitudeRadians);
                        mexPrintf("\tLong: %f\n", detectors[i]->frDetector.vertexLongitudeRadians);
                }

                struct detector_response *HL = NULL;

                if (isPlus)
                {
                        HL = detector_response_new(detectors, 3, max_l);

                        mexPrintf("FULL(x): 1) (%g) 2) (%g) 3) (%g)\n", creal(HL[0].cross->coeff[0]), creal(HL[0].cross->coeff[1]), creal(HL[0].cross->coeff[2]));
                        mexPrintf("FULL(+):  1) (%g) 2) (%g) 3) (%g)\n", creal(HL[0].plus->coeff[0]), creal(HL[0].plus->coeff[1]), creal(HL[0].plus->coeff[2]));
                }

                mexPrintf("Generated response\n");

                /* Now to test out modification code */
                double *ep[nBaselines], *ex[nBaselines];
                double *psd[3];
                int idx;

                int gridSize = (max_l + 1) * 2;

                for (idx = 0; idx < 3; idx++)
                        psd[idx] = (double *)malloc(1024 * sizeof(double));

                psd[0][45] = 2e-21;
                psd[1][45] = 4e-21;
                psd[2][45] = 3.5e-21;

                /* Allocate memory for orthonormal responses */
                for (idx = 0; idx < nBaselines; idx++)
                {
                        ep[idx] = (double *)malloc(gridSize * gridSize * sizeof(double));
                        ex[idx] = (double *)malloc(gridSize * gridSize * sizeof(double));

                        if (!(ep[idx] || ex[idx]))
                        {
                                fprintf(stderr, "\n******* Cannot allocate memory for stat generation**********\n\n");
                                return;
                        }
                }

                if (isPlus)
                        form_response(ep, ex, detectors, HL, 45, psd);
                else
                        form_response_grid(ep, ex, detectors, nInstr, max_l, 45, psd);

                int dims[3];
                dims[0] = gridSize;
                dims[1] = gridSize;
                dims[2] = 6;

                struct detector_response_grid *new_det = NULL;
                new_det = detector_response_grid_new(detectors, 3, max_l);

                /*
                double single_ep[6];
                double single_ex[6];

                for (idx=0;idx<1024;idx++)
                for(k=0;k<gridSize*gridSize;k++)
                return_response(single_ep, single_ex, new_det, k, 45, psd);
                */

                form_response_grid(ep, ex, new_det, 45, psd);

                plhs[0] = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxCOMPLEX);
                /*
                plhs[0] = mxCreateDoubleMatrix( gridSize, gridSize,mxCOMPLEX);
                */

                outMatrix2 = mxGetPr(plhs[0]);
                outMatrixi2 = mxGetPi(plhs[0]);

                for (k = 0; k < gridSize * gridSize; k++)
                {
                        outMatrix2[k] = new_det->polarisations[0].plus[k];
                        outMatrixi2[k] = new_det->polarisations[0].cross[k];

                        outMatrix2[gridSize * gridSize + k] = new_det->polarisations[1].plus[k];
                        outMatrixi2[gridSize * gridSize + k] = new_det->polarisations[1].cross[k];

                        outMatrix2[gridSize * gridSize * 2 + k] = new_det->polarisations[2].plus[k];
                        outMatrixi2[gridSize * gridSize * 2 + k] = new_det->polarisations[2].cross[k];

                        outMatrix2[gridSize * gridSize * 3 + k] = ep[0][k];
                        outMatrixi2[gridSize * gridSize * 3 + k] = ex[0][k];

                        outMatrix2[gridSize * gridSize * 4 + k] = ep[1][k];
                        outMatrixi2[gridSize * gridSize * 4 + k] = ex[1][k];

                        outMatrix2[gridSize * gridSize * 5 + k] = ep[2][k];
                        outMatrixi2[gridSize * gridSize * 5 + k] = ex[2][k];
                        /*
                        outMatrix2[gridSize*gridSize*3+k] = ep[3][k];
                        outMatrixi2[gridSize*gridSize*3+k] = ex[3][k];

                        outMatrix2[gridSize*gridSize*4+k] = ep[4][k];
                        outMatrixi2[gridSize*gridSize*4+k] = ex[4][k];

                        outMatrix2[gridSize*gridSize*5+k] = ep[5][k];
                        outMatrixi2[gridSize*gridSize*5+k] = ex[5][k];
                        */
                }

                if (isPlus)
                        detector_response_free(HL);

                instrument_free(instruments[2]);
                instrument_free(instruments[1]);
                instrument_free(instruments[0]);

                for (idx = 0; idx < 3; idx++)
                        free(psd[idx]);

                for (idx = 0; idx < nBaselines; idx++)
                {
                        free(ep[idx]);
                        free(ex[idx]);
                }

                detector_response_grid_free(new_det);
        }
        else if (option == 14)
        {
                /* Checking coherent energy calc */

                int nInstr = 3;

                int k, i;

                double *e;

                double *rd, *id;
                complex double *d;
                d = (complex double *)malloc(3 * sizeof(complex double));

                /* Check number of input and output parameters */
                if (nrhs != 4)
                {
                        mexErrMsgIdAndTxt("SphRadiometer:InvSH:nrhs",
                                          "4 inputs required.",
                                          "SHcoeffs = sphrad(0, 14, e, d)");
                }

                /* now get pointers to real/imaginary parts*/
                e = mxGetPr(prhs[2]);

                rd = mxGetPr(prhs[3]);
                id = mxGetPi(prhs[3]);

                for (i = 0; i < nInstr; i++)
                        d[i] = rd[i] + I * id[i];

                fprintf(stdout, "%g, %g, %g\n", e[0], e[1], e[2]);
                fprintf(stdout, "(%g,%g), (%g,%g), (%g,%g)\n", creal(d[0]), cimag(d[0]), creal(d[1]), cimag(d[1]), creal(d[2]), cimag(d[2]));

        } /* End */
        
        

        stopAnalysis = clock();
        mexPrintf("***********************************************\n");
        mexPrintf("*    segment analysis took %f seconds          *\n", ((double)(stopAnalysis - startAnalysis)) / CLOCKS_PER_SEC);
        mexPrintf("***********************************************\n");
        mexPrintf("*       segment analysis finished             *\n");
        mexPrintf("***********************************************\n");
}

/* Deallocates memory used during loop */
static void myExitFcn()
{
        mexPrintf("\nWarning, MEX-file is being unloaded, clearing persistant data products.\n");
        int idx;
        if (fdplans)
        {
                user_correlator_network_plan_fd_free(fdplans);
                mexPrintf("Done fdplans\n");
        }

        if (instruments)
        {
                for (idx = 0; idx < baselines->n_instruments; idx++)
                        instrument_free(instruments[idx]);
                free(instruments);
                mexPrintf("Done instruments\n");
        }

        if (baselines)
        {
                correlator_network_allbaselines_free(baselines);
                mexPrintf("Done baselines\n");
        }
}



