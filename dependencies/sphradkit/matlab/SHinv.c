/***************************************************************************
  **************************************************************************
  
                        Mex interface to S2kit 1.0

   Writen by Mark Edwards
   adapted from test_s2_semi_memo_inv.c
  
     S2Kit people:
     Peter Kostelec, Dan Rockmore
      {geelong,rockmore}@cs.dartmouth.edu
  
     Contact: Peter Kostelec
            geelong@cs.dartmouth.edu
  
     Copyright 2004 Peter Kostelec, Dan Rockmore
  
  ************************************************************************
  ************************************************************************/


/*
Compile into mex function to be called from Matlab.
Takes an array of spherical harmonic coefficients, performs the
inverse transform and returns the brightness map.
*/

#include "mex.h"
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "fftw3.h"
#include "makeweights.h"
#include "cospmls.h"
#include "FST_semi_memo.h"
#include "csecond.h"

#define max(A, B) ((A) > (B) ? (A) : (B))

/**************************************************/
/**************************************************/


/* matlab gateway routine */
void mexFunction ( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{

  int i, bw, size, cutoff ;
  int rank, howmany_rank ;
  double *rcoeffs, *icoeffs, *rdata, *idata ;
  double *workspace, *weights ;
  double *seminaive_naive_tablespace, *trans_seminaive_naive_tablespace ;
  double **seminaive_naive_table, **trans_seminaive_naive_table;
  double tstart, tstop;
  fftw_plan idctPlan, ifftPlan ;
  fftw_iodim dims[1], howmany_dims[1];
  
  /* mex grid output */
  double *outGrid = NULL;
  double *outGridi = NULL;

  /* Check number of input and output parameters */
  if(nrhs > 2) {
    mexErrMsgIdAndTxt("SHinv:initiate:nrhs",
                      "two inputs required.",
		      "SHinv(sh_coeff, bandwidth)");
  }

  if (nrhs ==2)
    /* bandwidth specified, so get its value */
    bw = mxGetScalar(prhs[1]);
  else
  {
    /* None specified, so get it from the data itself */
    if (mxGetN(prhs[0]) == 1)
      bw = (int)sqrt(mxGetM(prhs[0]));
    else
      bw = (int)sqrt(mxGetN(prhs[0]));  
  }
  
  fprintf(stdout,"bandwidth of SH: %i\n",bw);
  
  size = 2*bw;

  fprintf(stdout,"Grid size: %i\n",size);

  
  /*** ASSUMING WILL SEMINAIVE ALL ORDERS ***/
  cutoff = bw ;

  /* allocate lots of memory */
/* No need to malloc these as they already exist
  rcoeffs = (double *) malloc(sizeof(double) * (bw * bw));
  icoeffs = (double *) malloc(sizeof(double) * (bw * bw));
*/
  rdata = (double *) malloc(sizeof(double) * (size * size));
  idata = (double *) malloc(sizeof(double) * (size * size));
  weights = (double *) malloc(sizeof(double) * 4 * bw);
  
  workspace = (double *) malloc(sizeof(double) * 
				((8 * (bw*bw)) + 
				 (10 * bw)));
								 
  seminaive_naive_tablespace =
    (double *) malloc(sizeof(double) *
		      (Reduced_Naive_TableSize(bw,cutoff) +
		       Reduced_SpharmonicTableSize(bw,cutoff)));

  trans_seminaive_naive_tablespace =
    (double *) malloc(sizeof(double) *
		      (Reduced_Naive_TableSize(bw,cutoff) +
		       Reduced_SpharmonicTableSize(bw,cutoff)));

		       
		       
  /* Setup output mex structure */
  plhs[0] = mxCreateDoubleMatrix(size, size, mxCOMPLEX);
  outGrid=mxGetPr(plhs[0]);
  outGridi=mxGetPi(plhs[0]);		       
		       
  /****
       At this point, check to see if all the memory has been
       allocated. If it has not, there's no point in going further.
  ****/

  if ( (rdata == NULL) || (idata == NULL) ||
 /*      (rcoeffs == NULL) || (icoeffs == NULL) ||*/
       (outGrid == NULL) || (outGridi == NULL) ||
       (weights == NULL) ||
       (seminaive_naive_tablespace == NULL) ||
       (trans_seminaive_naive_tablespace == NULL) ||
       (workspace == NULL) )
    {
      perror("Error in allocating memory");
      exit( 1 ) ;
    }

  /* now precompute the Legendres */
  /* fprintf(stdout,"Generating seminaive_naive tables...\n"); */
  seminaive_naive_table = SemiNaive_Naive_Pml_Table(bw, cutoff,
						    seminaive_naive_tablespace,
						    workspace);

  /* fprintf(stdout,"Generating trans_seminaive_naive tables...\n"); */
  trans_seminaive_naive_table =
    Transpose_SemiNaive_Naive_Pml_Table(seminaive_naive_table,
					bw, cutoff,
					trans_seminaive_naive_tablespace,
					workspace);

  /* construct fftw plans */

  /* make iDCT plan -> note that I will be using the GURU
     interface to execute this plan within the routine*/
      
  /* inverse DCT */
  idctPlan = fftw_plan_r2r_1d( 2*bw, weights, rdata,
			       FFTW_REDFT01, FFTW_ESTIMATE );

  /*
    now plan for inverse fft - note that this plans assumes
    that I'm working with a transposed array, e.g. the inputs
    for a length 2*bw transform are placed every 2*bw apart,
    the output will be consecutive entries in the array
  */
  rank = 1 ;
  dims[0].n = 2*bw ;
  dims[0].is = 2*bw ;
  dims[0].os = 1 ;
  howmany_rank = 1 ;
  howmany_dims[0].n = 2*bw ;
  howmany_dims[0].is = 1 ;
  howmany_dims[0].os = 2*bw ;

  /* inverse fft */
  ifftPlan = fftw_plan_guru_split_dft( rank, dims,
				       howmany_rank, howmany_dims,
				       rdata, idata,
				       workspace, workspace+(4*bw*bw),
				       FFTW_ESTIMATE );

  /* now make the weights */
  makeweights( bw, weights );

  /* now get pointers to the spherical harmonics coefficients*/
  rcoeffs = mxGetPr(prhs[0]);
  icoeffs = mxGetPi(prhs[0]);
  
  /* Check to see if either are NULL, if so this means that we are
   * dealing with purely imaginary/real data and must create the
   * appropriate array
   */
  if (rcoeffs == NULL)
  {
    rcoeffs = (double *) malloc(sizeof(double) * (bw * bw));
    memset(rcoeffs, 0, bw * bw * sizeof(double));
  }
  else if (icoeffs == NULL)
  {
    icoeffs = (double *) malloc(sizeof(double) * (bw * bw));
    memset(icoeffs, 0, bw*bw*sizeof(double));
  }
  				 
  /* fprintf(stdout, "In data 1) : %f - %f\n",rcoeffs[0], icoeffs[0] );
   fprintf(stdout, "In data 2) : %f - %f\n",rcoeffs[1], icoeffs[1] );
  */
  
  /* do the inverse spherical transform */
  tstart = csecond();
  InvFST_semi_memo(rcoeffs,icoeffs,
		   rdata, idata,
		   bw,
		   trans_seminaive_naive_table,
		   workspace,
		   0,
		   cutoff,
		   &idctPlan,
		   &ifftPlan );
  tstop = csecond();
  
  /* fprintf(stderr,"inv time \t = %.4e\n", tstop - tstart);
   fprintf(stdout,"about to write out samples\n");
   */
  
  /* Assign the brightness map to the output */
  for( i = 0 ; i < size*size ; i++ )
  {
    outGrid[i]  = rdata[i];
    outGridi[i] = idata[i];
  }

  /* fprintf(stdout,"finished writing samples\n"); */
  
  /* now clean up */

  fftw_destroy_plan( ifftPlan );
  fftw_destroy_plan( idctPlan );

  free(trans_seminaive_naive_table);
  free(seminaive_naive_table);
  free(trans_seminaive_naive_tablespace);
  free(seminaive_naive_tablespace);
  free(workspace);
  free(weights);
  free(idata);
  free(rdata);

  return;
  
}
