% calibration_V3V4_compensation.m:  Compute calibration corrections for S5.
%
% This script computes the corrections and updated uncertainty estimates
% to be applied to LIGO S5 v3 calibrated h(t) data to incorporate the
% revised v4 calibration knowledge.  For details of the process, please see
% 
%   https://wiki.ligo.org/Bursts/XPipelineS5CalibrationUncertainties
%
% Patrick.Sutton at astro.cf.ac.uk

% %------------------------------------------------------------------------
% %   Output of script calibration_V3V4_compensation. 
% %------------------------------------------------------------------------
% %  
% % You should get the following output from running this script:
% %
% % Detector H1:
% %             Amplitude (ratio)				Phase (deg)
% %     Epoch	Mean Diff	Max Abs Diff	tot RMS err	Mean Diff	Max Abs Diff	tot RMS err	
% %     1       1.070441	0.069328        0.123735	3.772224	5.723260        8.779277
% %     2       1.071007	0.085778        0.133648	3.589346	8.404662        10.721863
% %     3       1.069952	0.078510        0.129104	3.569290	7.885900        10.320243
% %     4       1.065273	0.045513        0.112140	3.470887	6.420077        9.248642
% %     run     1.067625                    0.120043                            	9.589626
% % 
% % Detector H2:
% %             Amplitude (ratio)				Phase (deg)
% %     Epoch	Mean Diff	Max Abs Diff	tot RMS err	Mean Diff	Max Abs Diff	tot RMS err	
% %     1       1.087780	0.061603        0.123369	0.645717	3.564779    	6.317250
% %     2       1.084499	0.061158        0.123147	0.625803	3.988998        6.565981
% %     3       1.068833	0.042245        0.114933	0.642239	2.810163        5.924273
% %     4       1.121511	0.126707        0.165770	0.992772	6.793377        8.564460
% %     run     1.097883                    0.140859                                7.328358
% % 
% % Detector L1:
% %             Amplitude (ratio)				Phase (deg)
% %     Epoch	Mean Diff	Max Abs Diff	tot RMS err	Mean Diff	Max Abs Diff	tot RMS err	
% %     1       0.967872	0.039439        0.140041	3.688588	6.927055        8.111356
% %     2       0.961821	0.043980        0.141387	1.438513	2.798633        5.063827
% %     3       0.961904	0.038240        0.139708	1.074409	2.314374        4.813141
% %     run     0.962720                    0.139780                                5.389314
% %
% %------------------------------------------------------------------------
% %------------------------------------------------------------------------

% ---- Options.
makePlots = 0;
fmin = 40; 
fmax = 2000;

% ---- Delays [sec] in V3 calibration, from T1000227-v1.  
delay_H1 = [174,174,174,174]*1e-6; 
delay_H2 = [182,182,182,182]*1e-6; 
delay_L1 = [174,180,181]*1e-6; 
delay_nsamp = 3/16384;  %-- integer sample offset to be applied to V3 data

% % ---- V4 RMS uncertainties by frequency band: 40-2000Hz, 2-4kHz, 4-6kHz.
% %      Ampl as fraction, phase in deg.  From S5 calibration paper (NIM-A). 
% ampl_sigma_H1 = 1e-2*[10.4 15.4 24.2]; phase_sigma_H1 = [4.5 4.9 5.8]; 
% ampl_sigma_H2 = 1e-2*[10.1 11.2 16.3]; phase_sigma_H2 = [3.0 1.8 2.0];
% ampl_sigma_L1 = 1e-2*[14.4 13.9 13.8]; phase_sigma_L1 = [4.2 3.6 3.3];

% ---- V3 RMS uncertainties by frequency band: 40-2000Hz, 2-4kHz, 4-6kHz 
%      after V3->V4 difference taken into account.  From T1000227-v1, Table
%      2.  These are the numbers to be added in quadrature to the V3->V4
%      deviations; see
%        https://wiki.ligo.org/Bursts/XPipelineS5CalibrationUncertainties
%      Amplitude as fraction, phase in deg. 
ampl_sigma_H1 = 1e-2*[10.2 15.4 25.0]; phase_sigma_H1 = [5.6 5.8 7.7];
ampl_sigma_H2 = 1e-2*[10.5 11.5 17.4]; phase_sigma_H2 = [4.4 3.4 3.9];
ampl_sigma_L1 = 1e-2*[13.4 13.3 14.6]; phase_sigma_L1 = [3.4 2.9 6.2];

% ---- Additional calibration uncertainties associated with h(f) -> h(t) 
%      conversion, from T080242-00.  Columns:
%      Magnitude error (40Hz-5kHz), Magnitude error (5kHz-6.5kHz) [fraction].
%      Phase error (40Hz-5kHz), Phase error (5kHz-6.5kHz) [deg]. 
ampl_hoft_H1 = [0.01 0.055]; phase_hoft_H1 = [3.6 5.3]; 
ampl_hoft_H2 = [0.02 0.051]; phase_hoft_H2 = [2.8 3.2]; 
ampl_hoft_L1 = [0.01 0.056]; phase_hoft_L1 = [2.5 6]; 

% ---- H1 response functions.
H1respFileV3 = { ...
    'H-H1_CAL_REF_RESPONSE_DARM_ERR_S5_V3-815155213-9540481.txt', ...
    'H-H1_CAL_REF_RESPONSE_DARM_ERR_S5_V3-824695694-10348320.txt', ...
    'H-H1_CAL_REF_RESPONSE_DARM_ERR_S5_V3-835044014-8898240.txt', ...
    'H-H1_CAL_REF_RESPONSE_DARM_ERR_S5_V3-843942254-999999999.txt' ...
    };
H1respFileV4 = { ...
    'H-H1_CAL_REF_RESPONSE_DARM_ERR_S5_V4-815155213-9540481.txt', ...
    'H-H1_CAL_REF_RESPONSE_DARM_ERR_S5_V4-824695694-10348320.txt', ...
    'H-H1_CAL_REF_RESPONSE_DARM_ERR_S5_V4-835044014-8898240.txt', ...
    'H-H1_CAL_REF_RESPONSE_DARM_ERR_S5_V4-843942254-35091360.txt' ...
    };
% ---- H2 response functions.
H2respFileV3 = { ...
    'H-H2_CAL_REF_RESPONSE_DARM_ERR_S5_V3-815155213-9793975.txt', ...
    'H-H2_CAL_REF_RESPONSE_DARM_ERR_S5_V3-824949188-21189606.txt', ...
    'H-H2_CAL_REF_RESPONSE_DARM_ERR_S5_V3-846138794-999999999.txt', ...
    'H-H2_CAL_REF_RESPONSE_DARM_ERR_S5_V3-846138794-999999999.txt' ...
    };
H2respFileV4 = { ...
    'H-H2_CAL_REF_RESPONSE_DARM_ERR_S5_V4-815155213-9793975.txt', ...
    'H-H2_CAL_REF_RESPONSE_DARM_ERR_S5_V4-824949188-21189606.txt', ...
    'H-H2_CAL_REF_RESPONSE_DARM_ERR_S5_V4-846138794-7493220.txt', ...
    'H-H2_CAL_REF_RESPONSE_DARM_ERR_S5_V4-853632014-25401600.txt' ...
    };
% ---- L1 response functions.
L1respFileV3 = { ...
    'L-L1_CAL_REF_RESPONSE_DARM_ERR_S5_V3-816019213-8478614.txt', ...
    'L-L1_CAL_REF_RESPONSE_DARM_ERR_S5_V3-824497827-967431.txt', ...
    'L-L1_CAL_REF_RESPONSE_DARM_ERR_S5_V3-825465258-999999999.txt' ...
    };
L1respFileV4 = { ...
    'L-L1_CAL_REF_RESPONSE_DARM_ERR_S5_V4-816019213-8478614.txt', ...
    'L-L1_CAL_REF_RESPONSE_DARM_ERR_S5_V4-824497827-967431.txt', ...
    'L-L1_CAL_REF_RESPONSE_DARM_ERR_S5_V4-825465258-52445156.txt' ...
    };
% ---- Duration of calibration epochs.
epoch_dur_H1 = [9540481; 10348320; 8898240; 35091360];
epoch_dur_H2 = [9793975; 21189606; 7493220; 25401600];
epoch_dur_L1 = [8478614; 967431; 52445156];


% -------------------------------------------------------------------------
%    H1
% -------------------------------------------------------------------------

% ---- Compute differences in response functions.
disp(' ');
disp('Detector H1:');
mess = sprintf('\t\t\t%s\t\t\t\t%s','Amplitude (ratio)','Phase (deg)');
disp(mess);
mess = sprintf('\t%s\t%s\t%s\t%s\t%s','Epoch','Mean Diff','Max Abs Diff', ...
    'tot RMS err','Mean Diff','Max Abs Diff','tot RMS err');
disp(mess);
diff_H1 = [];
for epoch = 1:4
    fid = fopen(['S5calFiles/' H1respFileV3{epoch}]);
    V3 = textscan(fid,'%f %f %f','commentstyle','%');
    fclose(fid);
    fid = fopen(['S5calFiles/' H1respFileV4{epoch}]);
    V4 = textscan(fid,'%f %f %f','commentstyle','%');
    fclose(fid);
    V43_ampl{epoch} = V4{2}./V3{2};
    V43_phase{epoch} = unwrap(V4{3}-V3{3})*180/pi;
    V43d_phase{epoch} = unwrap(V4{3}-V3{3})*180/pi+360*delay_nsamp*V3{1};
    % ---- Compute mean and max(abs()) difference over analysis band.
    f = V3{1};
    ind = find(f>=fmin & f<=fmax);
    % ---- Amplitude: Compute mean V4:V3 ratio, which will be applied as DC
    %      shift of h(t) data.  Max abs deviation of V4:V3 from mean value
    %      will be added as quadrature-sum contribution of uncertainty.
    ampl_mean_diff = mean(V43_ampl{epoch}(ind));
    ampl_max_abs_diff = max(abs(V43_ampl{epoch}(ind) - mean(V43_ampl{epoch}(ind))));
    % ---- Total RMS amplitude uncertainty.
    ampl_err = norm([ampl_max_abs_diff,ampl_sigma_H1(1),ampl_hoft_H1(1)]);
    % ---- Phase: Will only apply integer time shift to h(t) data to
    %      compensate for V4:V3 differences.  Therefore uncertainty will be
    %      computed from max abs deviation of V4:V3 from the
    %      integer-sample-shifted V3.   
    phase_mean_diff = mean(V43d_phase{epoch}(ind));
    % phase_max_abs_diff = max(abs(V43d_phase{epoch}(ind) - mean(V43d_phase{epoch}(ind))));
    phase_max_abs_diff = max(abs(V43d_phase{epoch}(ind)));
    % ---- Total RMS phase uncertainty.
    phase_err = norm([phase_max_abs_diff,phase_sigma_H1(1),phase_hoft_H1(1)]);
    % ---- Output to screen.
    mess = sprintf('\t%d\t%f\t%f\t%f\t%f\t%f\t%f',epoch,ampl_mean_diff, ...
        ampl_max_abs_diff,ampl_err,phase_mean_diff,phase_max_abs_diff,phase_err);
    disp(mess);
    diff_H1 = [diff_H1; ampl_mean_diff,ampl_max_abs_diff,ampl_err,phase_mean_diff, ...
        phase_max_abs_diff,phase_err];
end
% ---- Corrections & uncertainties averaged over all epochs (over run).
mess = sprintf('\t%s\t%f\t\t\t%f\t\t\t\t\t%f','run', ...
    sum(diff_H1(:,1).*epoch_dur_H1)/sum(epoch_dur_H1), ...
    (sum(diff_H1(:,3).^2.*epoch_dur_H1)/sum(epoch_dur_H1)).^0.5, ...
    (sum(diff_H1(:,6).^2.*epoch_dur_H1)/sum(epoch_dur_H1)).^0.5);
disp(mess);

if makePlots
    linestyle = {'r-','b-','g-','k-'};
    figure; set(gca,'fontsize',16);
    subplot(3,1,1)
    for epoch = 1:4
        semilogx(V3{1},V43_ampl{epoch},linestyle{epoch});
        grid on; hold on
    end
    axis([40 2000 get(gca,'ylim')]);
    title('H1')
    subplot(3,1,2)
    for epoch = 1:4
        semilogx(V3{1},V43_phase{epoch},linestyle{epoch});
        grid on; hold on
    end
    axis([40 2000 -200 200]);
    subplot(3,1,3)
    for epoch = 1:4
        semilogx(V3{1},V43_phase{epoch}+360*delay_H1(epoch)*V3{1},linestyle{epoch});
        grid on; hold on
    end
    axis([40 2000 -5 25]);
end


% -------------------------------------------------------------------------
%    H2
% -------------------------------------------------------------------------

% ---- Compute differences in response functions.
disp(' ');
disp('Detector H2:');
% mess = sprintf('\t%s\t%s\t%s\t%s','Epoch','Mean Diff','Max Abs Diff','Std Diff');
% disp(mess);
mess = sprintf('\t\t\t%s\t\t\t\t%s','Amplitude (ratio)','Phase (deg)');
disp(mess);
mess = sprintf('\t%s\t%s\t%s\t%s\t%s','Epoch','Mean Diff','Max Abs Diff', ...
    'tot RMS err','Mean Diff','Max Abs Diff','tot RMS err');
disp(mess);
diff_H2 = [];
for epoch = 1:4
    fid = fopen(['S5calFiles/' H2respFileV3{epoch}]);
    V3 = textscan(fid,'%f %f %f','commentstyle','%');
    fclose(fid);
    fid = fopen(['S5calFiles/' H2respFileV4{epoch}]);
    V4 = textscan(fid,'%f %f %f','commentstyle','%');
    fclose(fid);
    V43_ampl{epoch} = V4{2}./V3{2};
    V43_phase{epoch} = unwrap(V4{3}-V3{3})*180/pi;
    V43d_phase{epoch} = unwrap(V4{3}-V3{3})*180/pi+360*delay_nsamp*V3{1};
    % ---- Compute mean and max(abs()) difference over analysis band.
    f = V3{1};
    ind = find(f>=fmin & f<=fmax);
    % ---- Amplitude: Compute mean V4:V3 ratio, which will be applied as DC
    %      shift of h(t) data.  Max abs deviation of V4:V3 from mean value
    %      will be added as quadrature-sum contribution of uncertainty.
    ampl_mean_diff = mean(V43_ampl{epoch}(ind));
    ampl_max_abs_diff = max(abs(V43_ampl{epoch}(ind) - mean(V43_ampl{epoch}(ind))));
    % ---- Total RMS amplitude uncertainty.
    ampl_err = norm([ampl_max_abs_diff,ampl_sigma_H2(1),ampl_hoft_H2(1)]);
    % ---- Phase: Will only apply integer time shift to h(t) data to
    %      compensate for V4:V3 differences.  Therefore uncertainty will be
    %      computed from max abs deviation of V4:V3 from the
    %      integer-sample-shifted V3.   
    phase_mean_diff = mean(V43d_phase{epoch}(ind));
    % phase_max_abs_diff = max(abs(V43d_phase{epoch}(ind) - mean(V43d_phase{epoch}(ind))));
    phase_max_abs_diff = max(abs(V43d_phase{epoch}(ind)));
    % ---- Total RMS phase uncertainty.
    phase_err = norm([phase_max_abs_diff,phase_sigma_H2(1),phase_hoft_H2(1)]);
    % ---- Output to screen.
    mess = sprintf('\t%d\t%f\t%f\t%f\t%f\t%f\t%f',epoch,ampl_mean_diff, ...
        ampl_max_abs_diff,ampl_err,phase_mean_diff,phase_max_abs_diff,phase_err);
    disp(mess);
    diff_H2 = [diff_H2; ampl_mean_diff,ampl_max_abs_diff,ampl_err,phase_mean_diff, ...
        phase_max_abs_diff,phase_err];
end
% ---- Corrections & uncertainties averaged over all epochs (over run).
mess = sprintf('\t%s\t%f\t\t\t%f\t\t\t\t\t%f','run', ...
    sum(diff_H2(:,1).*epoch_dur_H2)/sum(epoch_dur_H2), ...
    (sum(diff_H2(:,3).^2.*epoch_dur_H2)/sum(epoch_dur_H2)).^0.5, ...
    (sum(diff_H2(:,6).^2.*epoch_dur_H2)/sum(epoch_dur_H2)).^0.5);
disp(mess);

if makePlots
    linestyle = {'r-','b-','g-','k-'};
    figure; set(gca,'fontsize',16);
    subplot(3,1,1)
    for epoch = 1:4
        semilogx(V3{1},V43_ampl{epoch},linestyle{epoch});
        grid on; hold on
    end
    axis([40 2000 get(gca,'ylim')]);
    title('H2')
    subplot(3,1,2)
    for epoch = 1:4
        semilogx(V3{1},V43_phase{epoch},linestyle{epoch});
        grid on; hold on
    end
    axis([40 2000 -200 200]);
    subplot(3,1,3)
    for epoch = 1:4
        semilogx(V3{1},V43_phase{epoch}+360*delay_H2(epoch)*V3{1},linestyle{epoch});
        grid on; hold on
    end
    axis([40 2000 -5 25]);
end


% -------------------------------------------------------------------------
%    L1
% -------------------------------------------------------------------------

% ---- Compute differences in response functions.
disp(' ');
disp('Detector L1:');
mess = sprintf('\t\t\t%s\t\t\t\t%s','Amplitude (ratio)','Phase (deg)');
disp(mess);
mess = sprintf('\t%s\t%s\t%s\t%s\t%s','Epoch','Mean Diff','Max Abs Diff', ...
    'tot RMS err','Mean Diff','Max Abs Diff','tot RMS err');
disp(mess);
diff_L1 = [];
for epoch = 1:3
    fid = fopen(['S5calFiles/' L1respFileV3{epoch}]);
    V3 = textscan(fid,'%f %f %f','commentstyle','%');
    fclose(fid);
    fid = fopen(['S5calFiles/' L1respFileV4{epoch}]);
    V4 = textscan(fid,'%f %f %f','commentstyle','%');
    fclose(fid);
    V43_ampl{epoch} = V4{2}./V3{2};
    V43_phase{epoch} = unwrap(V4{3}-V3{3})*180/pi;
    V43d_phase{epoch} = unwrap(V4{3}-V3{3})*180/pi+360*delay_nsamp*V3{1};
    % ---- Compute mean and max(abs()) difference over analysis band.
    f = V3{1};
    ind = find(f>=fmin & f<=fmax);
    % ---- Amplitude: Compute mean V4:V3 ratio, which will be applied as DC
    %      shift of h(t) data.  Max abs deviation of V4:V3 from mean value
    %      will be added as quadrature-sum contribution of uncertainty.
    ampl_mean_diff = mean(V43_ampl{epoch}(ind));
    ampl_max_abs_diff = max(abs(V43_ampl{epoch}(ind) - mean(V43_ampl{epoch}(ind))));
    % ---- Total RMS amplitude uncertainty.
    ampl_err = norm([ampl_max_abs_diff,ampl_sigma_L1(1),ampl_hoft_L1(1)]);
    % ---- Phase: Will only apply integer time shift to h(t) data to
    %      compensate for V4:V3 differences.  Therefore uncertainty will be
    %      computed from max abs deviation of V4:V3 from the
    %      integer-sample-shifted V3.   
    phase_mean_diff = mean(V43d_phase{epoch}(ind));
    % phase_max_abs_diff = max(abs(V43d_phase{epoch}(ind) - mean(V43d_phase{epoch}(ind))));
    phase_max_abs_diff = max(abs(V43d_phase{epoch}(ind)));
    % ---- Total RMS phase uncertainty.
    phase_err = norm([phase_max_abs_diff,phase_sigma_L1(1),phase_hoft_L1(1)]);
    % ---- Output to screen.
    mess = sprintf('\t%d\t%f\t%f\t%f\t%f\t%f\t%f',epoch,ampl_mean_diff, ...
        ampl_max_abs_diff,ampl_err,phase_mean_diff,phase_max_abs_diff,phase_err);
    disp(mess);
    diff_L1 = [diff_L1; ampl_mean_diff,ampl_max_abs_diff,ampl_err,phase_mean_diff, ...
        phase_max_abs_diff,phase_err];
end
% ---- Corrections & uncertainties averaged over all epochs (over run).
mess = sprintf('\t%s\t%f\t\t\t%f\t\t\t\t\t%f','run', ...
    sum(diff_L1(:,1).*epoch_dur_L1)/sum(epoch_dur_L1), ...
    (sum(diff_L1(:,3).^2.*epoch_dur_L1)/sum(epoch_dur_L1)).^0.5, ...
    (sum(diff_L1(:,6).^2.*epoch_dur_L1)/sum(epoch_dur_L1)).^0.5);
disp(mess);

if makePlots
    linestyle = {'r-','b-','g-','k-'};
    figure; set(gca,'fontsize',16);
    subplot(3,1,1)
    for epoch = 1:3
        semilogx(V3{1},V43_ampl{epoch},linestyle{epoch});
        grid on; hold on
    end
    axis([40 2000 get(gca,'ylim')]);
    title('L1')
    subplot(3,1,2)
    for epoch = 1:3
        semilogx(V3{1},V43_phase{epoch},linestyle{epoch});
        grid on; hold on
    end
    axis([40 2000 -200 200]);
    subplot(3,1,3)
    for epoch = 1:3
        semilogx(V3{1},V43_phase{epoch}+360*delay_L1(epoch)*V3{1},linestyle{epoch});
        grid on; hold on
    end
    axis([40 2000 -5 25]);
end

