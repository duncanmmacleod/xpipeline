function data = makeasciiefffile(mdc,SN,injScale)
% MAKEASCIIEFFFILE - make ascii files of efficiency results for SN search
%
% MAKEASCIIEFFFILE reads the injction results files for X-Pipeline and cWB for
% the optically triggered supernova search and combines them into a simple ASCII
% file for a given MDC set, supernova, and injection scale.
%
% usage
%
% data = makeasciiefffile(mdc,SN,injScale)
%
%  mdc      String. Name of MDC set to process. See below for recognised values.
%  SN       String. Name of supernova to process. Recognised values are
%           'SN2007gr', 'SN2011dh'. Case sensitive. 
%  injScale Scalar. Injection scale to process. Value must match one of those
%           used for cWB file names; see e.g.
%             data_cwb/SN2007gr_SN2011dh_triggers_rho3.5/SN2007gr_H1H2L1_sgel2/
%
%  data     Array. Contains the data dumped to the output file; see below.
%
% The function produces an ascii file containing summary data on each injection.
% There is one row per injection, with columns:
%    1. injection number 
%    2. waveform type (see SNMDCINFO)
%    3. inclpass (inclination correction pass flag)
%    4. X - injection processed mask
%    5. X - significance 
%    6. X - weighted significance 
%    7. X - detected flag
%    8. cWB - injection processed mask
%    9. cWB - significance 
%   10. cWB - weighted significance 
%   11. cWB - detected flag
%   12. or(x-mask,cwb-mask)
%   13. or(x-det,cwb-det)
% 
% Recognised MDC sets depend upon the SN as follows:
%   SN2007gr: 'mueller5', 'numerical3', 'piro5', 'rotbar5', 'sgel2', 'sglin2'
%   SN2011dh: 'mueller_long', 'numerical_long', 'piro_long', 'rotbar_long',
%             'sgel_long', 'sglin_long' 
% 
% $Id$

% -----------------------------------------------------------------------------
%    User-specifiable parameters.
% -----------------------------------------------------------------------------

injDir       = '/Users/psutton/Documents/xpipeline/branches/linear/test/';
dataDir      = '/Users/psutton/Documents/xpipeline/branches/linear/test/efficiencies/data/';

% % ---- Base directories for matlab packages.
% %      DO THIS IN YOUR WORKSPACE BEFORE CALLING THE FUNCTION.
% matappsDir   = '/Users/psutton/Documents/matapps/';
% xpipelineDir = '/Users/psutton/Documents/xpipeline/branches/linear/';
% % ---- Set matlab path.
% addpath([matappsDir 'packages/simulation/BurstMDC/trunk']);
% addpath([matappsDir 'releases/utilities/misc/src/']);
% addpath([xpipelineDir 'searches/grb']);
% addpath([xpipelineDir 'searches/sn']);
% addpath([xpipelineDir 'utilities']);
% addpath([xpipelineDir 'share']);


% -----------------------------------------------------------------------------
%    SN analysis and MDC info.
% -----------------------------------------------------------------------------

disp('Collecting SN analysis and MDC info.');

% ---- Networks for each supernova, IN ORDER OF PREFERENCE, with associated
%      numbers for X-Pipeline and cWB runs:
%        run:          X-Pipeline run number for final closed-box results
%        loudestEvent: significance threshold for efficiency estimation
%        p:            approximate p-value (FAP) corresponding to loudestEvent
%        weight:       rescaling to be applied to trigger significance to allow
%                      for, e.g. visible-volume weighting
switch(lower(SN))
    
    case 'sn2007gr'
        
        % ---- X-Pipeline
        jj = 0;
        jj = jj+1; x.networks{jj} = 'H1H2L1V1'; x.run(jj) = 14; x.loudestEvent(jj,1) =  64.6368; x.p(jj) = 0.00168209; x.weight(jj,1) = 1;
        jj = jj+1; x.networks{jj} = 'H1H2L1';   x.run(jj) =  9; x.loudestEvent(jj,1) =  77.0654; x.p(jj) = 0.00168209; x.weight(jj,1) = 1;
        jj = jj+1; x.networks{jj} = 'H1H2V1';   x.run(jj) = 11; x.loudestEvent(jj,1) =  74.0876; x.p(jj) = 0.00165426; x.weight(jj,1) = 1;
        jj = jj+1; x.networks{jj} = 'H1H2';     x.run(jj) = 11; x.loudestEvent(jj,1) =  35.6827; x.p(jj) = 0.00177462; x.weight(jj,1) = 1;
        jj = jj+1; x.networks{jj} = 'L1V1';     x.run(jj) = 15; x.loudestEvent(jj,1) =  215.077; x.p(jj) = 0.00165426; x.weight(jj,1) = 1;
        % ---- cWB
        jj = 0;
        jj = jj+1; cwb.networks{jj} = 'H1H2L1V1'; cwb.run(jj) = NaN; cwb.loudestEvent(jj,1) =  0.5; cwb.p(jj) = NaN; cwb.weight(jj,1) = 1;
        jj = jj+1; cwb.networks{jj} = 'H1H2L1';   cwb.run(jj) = NaN; cwb.loudestEvent(jj,1) =  0.5; cwb.p(jj) = NaN; cwb.weight(jj,1) = 1;
        jj = jj+1; cwb.networks{jj} = 'H1H2V1';   cwb.run(jj) = NaN; cwb.loudestEvent(jj,1) =  0.5; cwb.p(jj) = NaN; cwb.weight(jj,1) = 1;
        jj = jj+1; cwb.networks{jj} = 'H1H2';     cwb.run(jj) = NaN; cwb.loudestEvent(jj,1) =  0.5; cwb.p(jj) = NaN; cwb.weight(jj,1) = 1;
        jj = jj+1; cwb.networks{jj} = 'L1V1';     cwb.run(jj) = NaN; cwb.loudestEvent(jj,1) =  0.5; cwb.p(jj) = NaN; cwb.weight(jj,1) = 1;

    case 'sn2011dh'
        
        % ---- X-Pipeline
        jj = 0;
        jj = jj+1; x.networks{jj} = 'G1V1'; x.run(jj) = 11; x.loudestEvent(jj) =  53.2827; x.p(jj) = 0.00177462; x.weight(jj) = 1;
        % ---- cWB
        jj = 0;
        jj = jj+1; cwb.networks{jj} = 'G1V1'; cwb.run(jj) = NaN; cwb.loudestEvent(jj) =  0.5; cwb.p(jj) = NaN; cwb.weight(jj) = 1;

end

% ---- Fetch info on the parameters for this MDC set.  
[types, shortTypes, nominalDistance, nominalHrss, scale1hrss] = snmdcinfo(mdc);


% -----------------------------------------------------------------------------
%    Process injection log data.
% -----------------------------------------------------------------------------
    
disp('Loading injection log file.');

% ---- Open and parse BurstMDC formatted log file. Put into same format
%      (variable names) as expected from X-Pipeline log file. The log files are
%      slow to load (~5 sec), so just load them once, for the first injection
%      scale. Do this by storing the required results as global variables that
%      will be available the next time this function is called (subsequent
%      injection scales).
global itype
global inclpass
global nInj
if isempty(itype)

    % ---- Read and parse BurstMDC log file.
    injFile = [injDir '/' 'BurstMDC-' SN '_' mdc '-Log.txt'];
    [nInj,mdcField,mdcData] = rdburstmdclog(injFile);
    gps_s  = floor(mdcData.EarthCtrGPS);
    gps_ns = round(1e9*(mdcData.EarthCtrGPS-gps_s));
    phi    = mdcData.External_phi;
    theta  = acos(mdcData.External_x);
    psi    = mdcData.External_psi;
    % ---- Extract injection data.
    hrssp  = mdcData.SimHpHp.^0.5;
    hrssc  = mdcData.SimHcHc.^0.5;
    % hrsspc = mdcData.SimHpHc;
    name   = mdcData.GravEn_SimID;
    ciota  = mdcData.Internal_x;
    iota   = acos(ciota);

    % ---- Loop over injections and extract waveform type.
    itype = zeros(nInj,1);
    for iInj = 1:nInj
        % ---- Assign a numerical label (1,2,...) to each injection by
        %      waveform type. 
        if strcmpi(mdc(1:4),'muel')
            tmp_type = find(strcmp(types,name{iInj}(1:15)));
        else
            tmp_type = find(strcmp(types,name{iInj}));
        end
        if ~isempty(tmp_type)
            itype(iInj) = tmp_type;
        end        
    end

    % ---- Assign flag to correct inclination angle distribution.
    % ---- Reset matlab random number generator seed to a value
    %      based on the SN and MDC set names. This will give us the
    %      same set of random numbers for a given SN-MDC
    %      combination every time we rerun this script.
    s = sum(double([SN mdc])); 
    rng(s);
    % ---- Select surviving injections randomly.
    inclpass = rand(length(iota),1) <= sin(iota);            

end


% -----------------------------------------------------------------------------
%    Load cWB analysis data for this injection scale, all networks.
% -----------------------------------------------------------------------------

disp('Loading cWB analysis data.');

% ---- Assign storage for cWB results.
cwb.injectionProcessedMask = zeros(nInj,length(cwb.networks));
cwb.significance           = zeros(nInj,length(cwb.networks));

% ---- Loop over all networks.
for inet = 1:length(cwb.networks)

    % ---- Directory holding results file.
    cwbDir = ['data_cwb/SN2007gr_SN2011dh_triggers_rho3.5/' ...
        SN '_' cwb.networks{inet} '_' mdc '/'];
    % % ---- Get list of injection scales processed (which also
    % %      fixes the file names). Again, we should only have to do this once.
    % cmdstr = ['ls ' cwbDir 'scale_factor_* | ' ...
    %     'awk -F_ ''{print $NF}'' | sort -n > cwbinjscales.txt'];
    % [status,result] = system(cmdstr);
    % injectionScale = load('cwbinjscales.txt');
    % nScale = length(injectionScale);

    % ---- Read the desired results file.
    %      Columns: injection_proc_mask, significance.
    fileName = [cwbDir 'scale_factor_' num2str(injScale)];
    disp(['Loading ' fileName]);
    tmp_cwb = load(fileName);
    cwb.injectionProcessedMask(:,inet) = tmp_cwb(:,1);
    cwb.significance(:,inet)           = tmp_cwb(:,2);

end

                
% -----------------------------------------------------------------------------
%    Load X-Pipeline analysis data for this injection scale, all networks.
% -----------------------------------------------------------------------------

disp('Loading X analysis data.');

% ---- Assign storage for X results.
x.injectionProcessedMask = zeros(nInj,length(x.networks));
x.significance           = zeros(nInj,length(x.networks));
% ---- The X results files are slow to load (~20 sec) but hold all injection
%      scales. So just load them once, for the first injection scale. Do this by
%      storing the results as global variables that will be available the next
%      time this function is called (subsequent injection scales). 
global xInjAssTrig
global xInjProcMask
global xInjScale
global xFoundIdx
global xMissedIdx
global xInjSig
if isempty(xInjAssTrig)
    % ---- Load results file for each network.
    for inet = 1:length(x.networks)
        % ---- Load "associatedinjection" file for this network.
        fileName = [dataDir '/' SN '-' x.networks{inet} '_run' num2str(x.run(inet)) ...
            '_closedboxassociatedTriggers_' mdc '.mat'];
        disp(['Loading ' fileName]);
        [xInjAssTrig{inet}, xInjProcMask{inet}, xInjScale{inet}, xFoundIdx{inet}, ...
            xMissedIdx{inet}] = loadinjassoctrigfile(fileName,true,0,nInj);
        % ---- Each element contains just one trigger (all zeros if no real
        %      trigger found associated with the injection). Pull out
        %      significance * pass value for each injection into an ordinary
        %      array. 
        xInjSig{inet} = reshape([xInjAssTrig{inet}.significance] .* [xInjAssTrig{inet}.pass],nInj,[]);
    end
else
    % ---- Do nothing - files already loaded on previous call.
end

% ---- Loop over all networks.
for inet = 1:length(x.networks)

    % ---- Extract injection results for this injection scale.
    idx = find(abs(xInjScale{inet}-injScale)/injScale<0.01);
    if isempty(idx)
        error(['Failed to find injection scale matching ' num2str(injScale) '.']);
    else
        % ---- Each element contains just one trigger (all zeros if no real trigger
        %      found associated with the injection). Pull out significance * pass
        %      value for each injection into an ordinary array.
        x.injectionProcessedMask(:,inet) = xInjProcMask{inet}(:,idx);
        x.significance(:,inet)           = xInjSig{inet}(:,idx);        
    end

end


% -----------------------------------------------------------------------------
%    Select triggers from highest-ranking network.
% -----------------------------------------------------------------------------

disp('Select triggers from highest-ranking network.');

% ---- cWB
for inet = 1:length(cwb.networks)

    % ---- Find highest-ranking network (earliest column) with nonzero mask.
    [val,col] = max(cwb.injectionProcessedMask,[],2);
    % ---- Set "master" injection processed mask: 1 if processed by any network.
    cwb.ipm = val;
    % ---- Convert column location to array index.
    idx = sub2ind(size(cwb.injectionProcessedMask),[1:nInj]',col);
    % ---- Record significance at this location into "master" significance array
    %      for this injection.
    cwb.sig = cwb.significance(idx);
    % ---- Apply weight factor for this network.
    cwb.wsig = cwb.sig .* cwb.weight(col);
    % ---- Are we above threshold for this network?
    cwb.det = cwb.wsig > cwb.loudestEvent(col);

end

% ---- X
for inet = 1:length(x.networks)

    % ---- Find highest-ranking network (earliest column) with nonzero mask.
    [val,col] = max(x.injectionProcessedMask,[],2);
    % ---- Set "master" injection processed mask: 1 if processed by any network.
    x.ipm = val;
    % ---- Convert column location to array index.
    idx = sub2ind(size(x.injectionProcessedMask),[1:nInj]',col);
    % ---- Record significance at this location into "master" significance array
    %      for this injection.
    x.sig = x.significance(idx);
    % ---- Apply weight factor for this network.
    x.wsig = x.sig .* x.weight(col);
    % ---- Are we above threshold for this network?
    x.det = x.wsig > x.loudestEvent(col);

end


% -----------------------------------------------------------------------------
%    Write output file.
% -----------------------------------------------------------------------------

% ---- Open file for text report.
fileName = [mdc '_' SN '_' num2str(injScale) '.txt'];
disp(['Writing output file ' fileName '.']);
fid = fopen(fileName,'w');
data = [ [1:nInj]' itype inclpass ...
    x.ipm x.sig x.wsig x.det ...
    cwb.ipm cwb.sig cwb.wsig cwb.det ...
    or(x.ipm,cwb.ipm) or(x.det,cwb.det)];
fprintf(fid,'%d %d %d %d %f %f %d %d %f %f %d %d %d\n',data');
fclose(fid);

disp('Finished.');

% ---- Done.
return
