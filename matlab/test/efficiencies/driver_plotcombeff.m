% ---- X only sets
corrIncl = false;
pipeline = 'X';
plotType = 'hrss';
%
corrIncl = false;
pipeline = 'X';
plotType = 'distance';
%
corrIncl = true;
pipeline = 'X';
plotType = 'hrss';
%
corrIncl = true;
pipeline = 'X';
plotType = 'distance';

% ---- X-cWB comparison:
corrIncl = false;
pipeline = 'all';
plotType = 'injsc';

% ---- Combined ULs:
corrIncl = true;
pipeline = 'OR';
plotType = 'hrss';
%
corrIncl = true;
pipeline = 'OR';
plotType = 'distance';

SN = 'SN2007gr'; mdc = 'rotbar5';