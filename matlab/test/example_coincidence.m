
% ---- I'll run this from the xpipeline source tree.
baseDir = '~/Documents/xpipeline/branches/linear/input/osn/segments/sn2007gr/';

% ---- Load S5/VSR1 segments.
H1 = xreadseglist([baseDir 'S5-H1_cat1.txt']);
H2 = xreadseglist([baseDir 'S5-H2_cat1.txt']);
L1 = xreadseglist([baseDir 'S5-L1_cat1.txt']);
V1 = xreadseglist([baseDir 'V1-SEGMENTS_INJCAT1.txt']);

% ---- X functions used start time - duration format for segments.  Strip
%      lists down to these.
H1 = H1(:,[2,4]);
H2 = H2(:,[2,4]);
L1 = L1(:,[2,4]);
V1 = V1(:,[2,4]);

% ---- Find all 4-fold coincidence segments.
HHLV = Coincidence4(H1(:,1),H1(:,2),H2(:,1),H2(:,2),L1(:,1),L1(:,2),V1(:,1),V1(:,2));
disp([' Total HHLV coincidence time: ' num2str(sum(HHLV(:,2))/86400) ' days']);

% ---- Find all times Virgo was NOT on.
nV1 = ComplementSegmentList(V1(:,1),V1(:,2),0,9999999999);

% ---- Find all H1-H1-L1-notV1 times.
HHLnV = Coincidence4(H1(:,1),H1(:,2),H2(:,1),H2(:,2),L1(:,1),L1(:,2),nV1(:,1),nV1(:,2));
disp([' Total HHLnV coincidence time: ' num2str(sum(HHLnV(:,2))/86400) ' days']);
