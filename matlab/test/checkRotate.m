% compare that RotateMap is consistent with RotateVector

% number of tests
N=10;

% generate random sky directions
phi0 = 2*pi*rand(N,1)-pi;
theta0 = pi*rand(N,1);

% convert to unit vector
V0 = CartesianPointingVector(phi0, theta0);

% generate random rotation angles
phi = 2*pi*rand(N,1)-pi;
theta = pi*rand(N,1);
psi = 2*pi*rand(N,1)-pi;
psi = zeros(size(psi));

% rotate in two different ways and compare
for ii = 1:N
  % first way, as used in makeGrid.m
  V1 = RotateMap(V0,phi(ii),theta(ii),psi(ii),0);
  % second way, using rotation around y and then z axis
  V2 = RotateVector(V0',[0 0 1],psi(ii));
  V2 = RotateVector(V2,[1 0 0],theta(ii)); 
  V2 = RotateVector(V2,[0 0 1],phi(ii))';
  % compare the two
  sqrt(sum(abs(V1).^2,2));
  sqrt(sum(abs(V1-V2).^2,2))
end