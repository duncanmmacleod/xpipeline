% Testing parameters
nSigma = 1.65;
nTrials = 1e4;
% end parameters

grbList=[];
runCell={'S6A','S6B','S6C','S6D'};
setCell={'analyzeGRBs','analyze3minGRBs'};
for iRun = 1:length(runCell)
  for iSet = 1:length(setCell)
    tmp = dir([runCell{iRun} '/' setCell{iSet} '/GRB*']);
    for iGRB = 1:length(tmp)
      tmp(iGRB).name = [runCell{iRun} '/' setCell{iSet} '/' tmp(iGRB).name];
    end
    grbList=[grbList(:)' tmp(:)'];
  end
end
nGRBs=length(grbList)

for iGRB = 1:nGRBs
  disp(['Analyzing ' grbList(iGRB).name])
  skyPosFiles=dir([grbList(iGRB).name '/input/sky_positions*.txt']);
  for iFile = 1:length(skyPosFiles)
    skyPositions = load([grbList(iGRB).name '/input/' skyPosFiles(iFile).name]);
    % ---- read sky position error from grb.param
    paramFile = fopen([grbList(iGRB).name '/grb.param']);
    paramStrCell = textscan(paramFile,'%s','delimiter',' ');
    paramStrCell = paramStrCell{1};
    iErr = find(strcmp(paramStrCell,'-e') | strcmp(paramStrCell,'--sky-pos-err'));
    skyPosError = str2num(paramStrCell{iErr+1});
    fclose(paramFile);
    % ---- read sites from channels
    channelFile = fopen([grbList(iGRB).name '/input/channels.txt']);
    channelsCell = textscan(channelFile,'%s%s','delimiter',' ');
    fclose(channelFile);
    clear sites
    for iChannel = 1:length(channelsCell{1})
      sites{iChannel} = channelsCell{1}{iChannel}(1);
    end
    % assume that the first sky position is correct and point directly at
    % the GRB
    skyPos0 = skyPositions(1,:);
    [minDelay theta phi]= testSkyGrid(nTrials,skyPositions,skyPos0,skyPosError/180* ...
                           pi*nSigma,sites);
    delayPercentile(iGRB,iFile,:) = prctile(minDelay,[90 99 99.9]);
  end
end
