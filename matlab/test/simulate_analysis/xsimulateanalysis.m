function [eff, injectionScales, effAll] = xsimulateanalysis(trigger, ...
    detectorsCell,cutTypes,cutThresholds,detectionStat,detectionThreshold, ...
    waveform,pathToFigs,analysis)
% XSIMULATEANALYSIS - simulate an X-Pipeline analysis to test efficiencies
%
% usage:
%
% [eff, scale, effAll] = xsimulateanalysis(trigger,detectorsCell,cutTypes, ...
%          cutThresholds,detStat,detThresh,waveform,pathToFigs,analysis)
%
% trigger       Struct specifying external trigger. Required fields:
%                 ra   Scalar. Right ascension [deg].
%                 dec  Scalar. Declination [deg].
%                 gps  One- or two-element vector: [gps] or [gps_min,gps_max].
%                 err  Scalar. Sky position uncertainty [deg].
%               For a GRB analysis, use gps=scalar, err>=0. For a SN analysis,
%               use gps=2-component vector (start and end times of on-source
%               window), err=0. This function cannot currently handle the case
%               of err>0 and length(gps)>1.
% detectorsCell Cell array of detectors, eg {'H1','L1','V1'}.
% cutTypes      Cell array with 6 elements describing the coherent consistency
%               tests to be applied. E.g. {'alphaIoverE','alphaEoverI', ...
%               'alphaEoverI', 'ratioIoverE','ratioEoverI','ratioEoverI'};
% cutThresholds Vector with 6 elements, holding the thresholds on the
%               corresponding cutTypes. 
% detStat       String. Name of the likelihood to be used as the detection
%               statistic. E.g.: 'loghbayesiancirc'; 
% detThresh     Scalar. Threshold on the detection statistic for an 
%               injection to be detected. 
% waveform      Struct. Required fields:
%                 set   String. Name of waveform set as listed on X-Pipeline web
%                       page. Used to read in associated figure files. E.g.
%                       'sglin2'. 
%                 type  String. Type of injections as recognised by
%                       XMAKEWAVEFORM. E.g. 'osnsearch'. 
%                 parameters  Cell array. Each element contains a complete set
%                       of the parameters of a given waveform (of type
%                       waveform.type) to simulate. Must be recognised by
%                       XMAKEWAVEFORM. E.g.  {'1e-21~0~Waveforms/SG235Q8d9.txt',
%                       '1e-21~0~Waveforms/SG1304Q8d9.txt'}. If more than one
%                       element, the the efficiency is computed by averaging
%                       over all parameter sets. 
%                 polarisation  String. Used to determine how the source
%                       inclination is used to adjust the injection amplitude.
%                       Recognised values are 'elliptical', 'linear', 'none',
%                       'sgel'. 
%               Optional fields:
%                 maxIncl Largest allowed inclination angle (deg).
%                 name  String. Used in fig file names produced by the web page.
%                       E.g. 'SG235Q8d9.txt'. Needed for opening those files.
%                 ul50  Scalar. Injection scale at which efficiency is 50% (50%
%                       UL from the search.)
%                 ul90  Scalar. Injection scale at which efficiency is 90% (90%
%                       UL from the search.)
% pathToFigs    String. Directory containing of fig files of analysis to
%               emulate. Use [] or '' if you do not wish to input any such
%               files.
% analysis      Struct. Required fields:
%                 pixelselection: one of 'bpp', 'matchbw', 'maxsnr', 'minNpix'
%                 mode: one of 'spectrogram', 'emulatefft', 'fft'
%                 type: one of 'GR', 'scalar'
%                 usenoise: true or false
%                 verbose: true or false
%                 spectrumfile: Name of file containing averaged noise spectra
%                   for all of the detectors. 
%                 N_initial: Initial length of the signal timeseries, in samples.
%                 fs: Scalar. Sample rate to use for simulation [Hz].
%                 delayTol: maximum allowed time delay error in sky frid
%                   (from .ini file) 
%                 testSkyPos: true or false
%               Optional fields:
%                 catalogDirectory: optional string specifying location of
%                   catalog directory for use by XMAKEWAVEFORM. If not supplied a
%                   default value is assumed. Default
%                   '/Users/psutton/Documents/xpipeline/branches/linear/utilities/waveforms/'.
%                 scatterplots: true or false. If true, scatter plots of
%                   coherent energies are made. Default true.
%                 skyGridFile: string. If specified, then sky grid is read from
%                   this file.
%
% eff           Vector. Predicted efficiencies at corresponding injection scales.
% scale         Vector. Injection scale for each efficiency.
% effAll        Cell array. Each element is a vector of predicted efficiencies
%               for the corresponding waveform in waveform.parameters.
%
% XSIMULATEANALYSIS simulates the X-Pipeline analysis of GW injections for a
% single GRB or supernova. It uses the sky location, measured/design noise spectra, and
% tuned consistency cut thresholds to estimate the detection efficiency as a
% function of amplitude for each of the waveform types tested.  This can be
% compared to the measured closed-box analysis results as a validation of the
% search.
% 
% Procedure:  
%   Construct network (detectors and noise spectra).
%   Construct sky grid.
%   Loop over waveform parameter sets. For each set:
%     Generate injection parameters (sky position, etc.).
%     Construct waveform timeseries.
%     Perform time-frequncy analysis to select TF pixels.
%     Compute antenna response functions for the network.
%     Compute unit vectors in dominant polarisation frame.
%     Compute projection operators Pab in dominant polarisation frame.
%     Loop over injection scales. For each scale:
%       Loop over injections. For each injection:
%         Compute delay errors for each grid point and detector.
%         Compute detector responses (including Gaussian noise and delay errors)
%         for each grid point. 
%         Compute coherent likelihoods, significance for each grid point.
%         Select best grid point as recovered location.
%         Assign event significance.
%       Apply cuts to compute efficiencies.
%       Add simulated triggers for ul amplitude to scatter plots.
%   Plot efficiency (averaged over all parameter sets) vs. injection scale.
%
% authors:
% Patrick J. Sutton <patrick.sutton@astro.cf.ac.uk>
% Scotty Coughlin
% Ioannis Kamaretsos
%
% TO-DOs:
% 0. Fix the noise generation to allow for use-specified noise curves or sampled
%    noise spectra files. Add O1 curves to SRD.
% 1. Fix reading of existing efficiency plots for overlay of predicted
%    efficiencies.
% 2. Fix reading of existing scatter plots for overlay of predicted
%    likelihoods.
% 3. [BIG] Write wrapper script with capability to read a grb run directory and
%    pull out trigger data, injections, etc. from grb.param, ini file, web page,
%    etc.   
% 4. Test on a Swift GRB for which X works well, determine TF analysis model
%    that best predicts likelihoods and time-frequency properties of injections
%    (eg FFT length, nPixels, duration, bw).
% 5. Repeat 4. with optimised TF analysis to (hopefully) verify efficiency
%    modelling for standard SGC, inspiral waveforms.
% 6. Test on some Fermi/IPN GRBs with large error boxes, attempt to verify sky
%    localisation behaviour.
% 7. Repeat 6. with different pointing statistics and try to find a better
%    statistic. 
% 8. Repeat 5. for a SN analysis to (hopefully) verify efficiency modelling for
%    that case too. Note that SN waveforms may require different TF analysis.
%
% Notes: 
%
% The different TF thresholding procedures (pixelselection and mode fields of
% the analysis struct) vary the hrss curves by tens of percent or more.
% Different choices give better efficiency matches for different waveforms. This
% could use more investigation!
%
% $Id$


% =========================================================================
%    Initialize various parameters, status reports.
% =========================================================================

% ----- Generate analytic values in limit of infinitesimal signal
%       duration, ignoring any bias towards selecting events near peaks
%       of sum-squared window function (the biases due to these two
%       assumptions may at least partially cancel).  Seems to give
%       results pretty much identical to sampled ratios, except
%       sigmoids slightly less sharp.
%       These corrections are applied manually; for analysis.mode 'fft',
%       and are ignored otherwise.
% disp('Analytically estimating corrections for 50% overlapping and modifiedhann window.');
min_sumw2 = 1.4577;
max_sumw2 = 2.9149;
x = [1:1024]';
ratio = min_sumw2 + (max_sumw2-min_sumw2)*(0.5*(1-cos(2*pi*x/2048))).^2;

% ---- Range of scale factors to be tested, below/above default injection scale.
if isfield(analysis,'injectionScales')
    % ---- Use user-specified injection scales.
    injectionScales = analysis.injectionScales;
else
    % ---- Define some default injection scales.
    injectionScales = [0.00100,0.00316,0.01000,0.01778,0.03162,0.05623,0.10000,0.13335,0.17783, ...
        0.23714,0.31623,0.42170,0.56234,0.74989,1.00000,1.33352,1.77828,2.37137, ...
        3.16228,4.21697,5.62341,7.49894,10.00000,13.33521,17.78279,23.71374, ...
        31.62278,56.23413,100.00000,316.22777,1000.00000];
end
% ---- Append 90% UL injection scale to end of list; this scale will be used for
%      scatter plots.
%injectionScales = [injectionScales, ul];
% ---- Prepare storage.
hrss = zeros(size(injectionScales));
eff = zeros(size(injectionScales));

% ---- Sample rate for timeseries.
fs = analysis.fs;


% =========================================================================
%    Set up detector network.
% =========================================================================

% ---- Network for analysis: convert cell array into a string. Also pull 
%      out site designators into a string and a tilde-delimited string.
detectorsStr = cell2mat(detectorsCell);  %-- e.g. 'H1L1V1'
siteStr = detectorsStr(1:2:end);         %-- e.g. 'HLV'
siteStrTilde = detectorsStr(1:end-1);
siteStrTilde(2:2:end) = '~';             %-- e.g. 'H~L~V'
% ---- Number of detectors.
Nifo = length(detectorsCell);

% ---- Load spectra from file or generate from design curves.
if isfield(analysis,'spectrumfile')
    error(['analysis.spectrumfile option not functional. ' ...
        'Need to produce same outputs as SRD option below.']);
    % ---- Load spectra from file.
    if analysis.verbose
        disp(['Reading spectrum file ' analysis.spectrumfile]);
    end
    try 
        load(analysis.spectrumfile);
        disp(['KLUDGE: spectrum file should include frequency information.']);
        favg = [0:4:2048]';
    catch
        error(['No spectrum file supplied.']);
    end
else
    % ---- Use design noise curve.
    if analysis.verbose
        disp('Using SRD noise curves ... '); 
    end
    % ---- SRD - Return science requirements design sensitivity 
    %      curve for the specified gravitational-wave detector.
    %      KLUDGE: Hardwire frequency range, design curve choices. These should
    %      be easy to input as optional parameters.
    fSnet = [0:8192]';
    Snet  = zeros(length(fSnet),Nifo);
    %fstop = zeros(1,Nifo);
    for ii = 1:Nifo
        switch detectorsCell{ii}
            case 'G1'
                designCurve = 'GEO-HF';
            case {'H1','L1'} 
                designCurve = 'aLIGO';
            case {'H2'} 
                error('H2 selected. Currently only simulating advanced network.'); 
            case 'K1'
                designCurve = 'KAGRA';
            case 'V1' 
                designCurve = 'aVirgo';
            otherwise
                error(['Detector ' detectorsCell{ii} ' not recognised.']);
        end
        % ---- Call SRD function. It does not work for f=0, and we have to
        %      manage frequencies below fstop carefully. Set all of these to
        %      value of spectrum at first frequency at/above fstop.
        warning off
        [tmp fstop] = SRD(designCurve, fSnet(2:end));
        warning on
        disp('KLUDGE: inflating noise by factor 3 for O1 testing.');
        tmp = tmp * 3^2;
        % ---- Add zero-frequency component (value arbitrary).
        tmp = [0; tmp];
        idx = find(fSnet<fstop(1));
        tmp(idx) = tmp(idx(end)+1);
        Snet(:,ii) = tmp;
    end
    % ---- SRD function returns S(f) for continuous data [units Hz^-1].
    %      For Snet we want discrete PSD which is S(f)*fs.
    Snet = Snet * fs;
end
% ---- Generate a coherent network noise spectrum to use for spectrogram
%      analyses.
Savg = sum(Snet.^(-1),2).^(-1);


% =========================================================================
%    Construct sky grid.
% =========================================================================

% ---- Construct sky grid around trigger location. Assume for the moment one of
%      two cases: either trigger.err = 0 (SN case) or a single GPS time (good
%      approximation for the GRB case).

if isfield(analysis,'skyGridFile')
    % ---- Read sky grid from the indicated file.
    %      KLUDGE: In a standard search we usually have multiple copies of the
    %      search grid to account for the source tracking across the sky. Here
    %      we're hoping one is close enough, but this *will* cause problems for
    %      Swift GRBs.
    skyGrid = load(analysis.skyGridFile);
    Theta   = skyGrid(:,1);
    Phi     = skyGrid(:,2);
    skyProb = skyGrid(:,3);
    Nsky = length(Theta);
elseif length(trigger.gps)==1
    % ---- GRB-style analysis. We make a single grid centred on the GRB ra,dec
    %      at the specified trigger time. This assumes that the on-source window
    %      is short enough that the GRB can't track far enough across the sky to
    %      sample significantly different network responses, so we can simulate 
    %      the event as instantaneous.
    % ---- Make a sky grid in ra,dec.
    [ra_search_deg,dec_search_deg,gridProb] = xmakeskygrid(num2str(trigger.ra), ...
        num2str(trigger.dec),num2str(trigger.gps),num2str(trigger.err), ...
        num2str(1.65),siteStrTilde,num2str(analysis.delayTol));
    % ---- Convert grid to Earth-fixed coordinates.
    [Phi,Theta] = radectoearth(ra_search_deg,dec_search_deg,trigger.gps);
    % ---- Number of grid points.
    Nsky = length(Phi);
elseif length(trigger.gps)==2 & trigger.err == 0
    if max(trigger.gps)-min(trigger.gps) < 86400
        % ---- On-source window is shorter than one day, so trigger arcs across
        %      less than one full cycle of the sky. 
        % ---- Determine end points of the arc. Note that Phi decreases with
        %      time!
        [Phi_max,Theta] = radectoearth(trigger.ra,trigger.dec,min(trigger.gps));
        [Phi_min,Theta] = radectoearth(trigger.ra,trigger.dec,max(trigger.gps));
    else
        % ---- On-source window is longer than one day, so trigger arcs across
        %      at least one full cycle of the sky. 
        [junk,Theta] = radectoearth(trigger.ra,trigger.dec,min(trigger.gps));
        Phi_min = 0;
        Phi_max = 2*pi;
    end
    % ---- Hardwire grid spacing to match default blockTime of X-Pipeline
    %      for aLIGO analyses (224sec after allowing for transient blocks).
    dPhi = 2*pi*224/86400; %-- about 1 deg
    Nsky = ceil((Phi_max-Phi_min)/dPhi);
    % ---- Make grid.
    Phi = [(Phi_min+dPhi/2):dPhi:Phi_max]';
    Theta = Theta * ones(size(Phi));
    % ---- Number of grid points. (Recompute to be sure.)
    Nsky = length(Phi);
    gridProb = ones(size(Phi));        
else
    error(['Current sky grid generation only handles case where ' ...
        'trigger.err==0 or length(trigger.gps)==1.']);
end
% ---- Number of sky points.
disp(['Using sky grid with ' num2str(Nsky) ' points.']);

% ---- Compute time delays of grid points wrt centre of Earth.
gridDelay = computeTimeShifts(detectorsCell,[Theta,Phi]);


% =========================================================================
%    Loop over waveforms and compute efficiencies.
% =========================================================================

plotOpen = false;
effPlotOpen = false;

% ---- Loop over distinct waveform types.
Nwf = length(waveform.parameters);
for iNwf = 1:Nwf;

    if analysis.verbose
        disp(' ')
        disp(['Processing waveform ' num2str(iNwf) ' of ' num2str(Nwf) '.']);
        disp(['Parameters: ' waveform.parameters{iNwf}]);
    end

    
    % =========================================================================
    %    New procedure ...
    % =========================================================================

    %  Generate simulation parameters. For now, generate these OTF. Later may
    %  add option  parsing them from an injection log file.
    %  - input: type, params, catDir (1 copy) to make h+, hx
    %  - generate: sky pos, polar, incl (Ninj copies)
    %    - if assume perfect localisation flag then inj pos = grid point.
    %  - time delays for inj positions
    % 
    %  make h+, hx (zero delay)
    % 
    %  TF analysis: Fourier transform waveform and restrict frequency range using
    %  zero-delay wf and net avg S. 
    % 
    %  for all sky grid positions do 
    %    Compute antenna response functions for the network.
    %    Compute unit vectors of dominant frame.
    %    Compute projection operators Pab in dominant frame.
    
    %  for each injection scale do
    %    if test sky pos then 
    %      for each injection do
    %        for all grid pos compute phase delay from gridDelay-injDelay (or vv)
    %        compute all likelihoods for all grid pos [using inj F+,x and delays]
    %        determine grid pos of max likelihood
    %        reset sky grid to recovered locations.
    
    % =========================================================================
    %    Generate injection parameters.
    % =========================================================================

    % ---- Generate injection parameters here (outside loop over injection  
    %      scales) so the same values are used for all injection scales, as in
    %      ordinary simulations.
    if analysis.verbose
        disp(['Generating injection parameters.']);
    end

    % ---- Generate sky positions of injections.
    if analysis.testSkyPos
        % ---- Determine number of injections.
        if isfield(analysis,'Ninj')
            Ninj = analysis.Ninj;
        else
            % ---- Default: number of injections = number of grid points.
            Ninj = Nsky;
        end    
        if length(trigger.gps)==2 & trigger.err == 0
            % ---- SN-style analysis. Generate injections along ring around the
            %      sky.
            injGPS = sort(min(trigger.gps) + rand(Ninj,1)*(max(trigger.gps)-min(trigger.gps)));
            [injPhi,injTheta] = radectoearth(trigger.ra,trigger.dec,injGPS);
        elseif length(trigger.gps)==1
            % ---- GRB-style analysis.
            [centerPhi,centerTheta] = radectoearth(trigger.ra,trigger.dec,trigger.gps);
            [injPhi, injTheta] = xjitterskyposition(centerPhi*ones(Ninj,1), ...
                centerTheta*ones(Ninj,1),trigger.err);
        else
            error('Currently only set up for GRB-style and SN-style analyses.');
        end
        % ---- Wrap injPhi to [-pi,pi) to match grid.
        idx = find(injPhi>=pi);
        injPhi(idx) = injPhi(idx)-2*pi;
    else
        % ---- If not testing sky localisation capability then put one injection
        %      at each sky grid point so that our efficiencies are an average
        %      over the grid. 
        Ninj = Nsky;
        injTheta = Theta;
        injPhi = Phi;
    end        
    % ---- Compute time delays of injection positions wrt centre of Earth.
    injDelay = computeTimeShifts(detectorsCell,[injTheta,injPhi]);
    
    % ---- Select random polarization angle for each injection.
    Psi = pi*rand(Ninj,1);
    
    % ---- Select random inclination angle for each injection. 
    cosIota = 2*rand(1,Ninj)-1;
    if isfield(waveform,'maxIncl')
        % ---- We want to restrict inclinations to within waveform.maxIncl
        %      degrees of 0 or 180. The sign() call below randomly sets
        %      inlcinations to negative values with 50% probability.
        minCosIota = cos(waveform.maxIncl * pi/180);
        cosIota = sign(2*rand(Ninj,1)-1) .* (minCosIota + (1-minCosIota)*rand(Ninj,1));
    end
    % ---- Determine wavefomr-polarisation-dependent amplitude factors.
    switch waveform.polarisation
        case 'elliptical'
            Ap = 0.5*(1+cosIota.^2);
            Ac = cosIota;
        case 'none'
            Ap = ones(Ninj,1);
            Ac = zeros(Ninj,1);
        case 'linear'
            Ap = 1-cosIota.^2; % = sin(Iota)^2
            Ac = zeros(Nsky,1);
        otherwise
            error(['Do not know how to handle polarisation ' waveform.polarisation '.']);
    end
    
    % ---- Compute antenna responses at injection locations. Note that we have
    %      not yet computed the frequency bins to be used, so we can't do the
    %      noise weighting of the antena responses yet. Do that later wen
    %      computing the network data.
    for a = 1:Nifo
        detector = detectorsCell{a};
        [Fp_temp Fc_temp, Fb_temp] = ComputeAntennaResponse(injPhi, injTheta, Psi, detector);
        % ---- Pack antenna responses for all detectors into one matrix, one
        %      sky position per row.
        injFp(:,a) = Fp_temp;
        injFc(:,a) = Fc_temp;
        injFb(:,a) = Fb_temp;
    end
    clear Fp_temp Fc_temp Fb_temp detector
    
    
    % =========================================================================
    %    Make injection time series for each polarisation.
    % =========================================================================
        
    if analysis.verbose
        disp(['Generating injection timeseries.']);
    end

    % ---- Generate waveform.
    parameters = waveform.parameters{iNwf};
    N = analysis.N_initial;
    T = N/fs;  
    if isfield(analysis,'catalogDirectory');
        catDir = analysis.catalogDirectory;
    else
        catDir = '/Users/psutton/Documents/xpipeline/branches/linear/utilities/waveforms/';
        if analysis.verbose
            warning(['Using hardwired catalog directory ' catDir]);
        end
    end
    % ---- Note: The offset in peak time is to avoid the injection having a 
    %      special position with respect to the FFT segmentation of the data. 
    [t,hp,hc,hb] = xmakewaveform(lower(waveform.type),parameters,T,T/2+0.5*rand(1), ...
        fs,'catalogDirectory',catDir);                                                                


    % =========================================================================
    %    Time-frequency analysis.
    % =========================================================================

    if analysis.verbose
        disp(['Setting time-frequency analysis parameters.']);
    end
    
    % ---- Default spectrogram parameters. The noverlap and wind choices match
    %      what's used in X-Pipeline. The Tfft and Npix values are arbitrary but
    %      typically work well. Tfft = 1/16 is in the middle of the rangle
    %      usually used in X, while Npix is smaller than usual -- only
    %      sine-Gaussians at modest SNRs are this compact.
    Tfft = 1/16;
    Npix = 25;
    noverlap = fs*Tfft/2;
    wind = modifiedhann(fs*Tfft);

    if strcmp(analysis.mode,'spectrogram')

        % ---- Optionally reset Tfft and/or Npix to match the signal.
        if strcmp(analysis.pixelselection,'matchbw')

            % ---- Reset Tfft to match bandwidth of signal. Works well for
            %      everything except those pesky chirping Piro waveforms ... 
            % ---- Compute noise-weighted bandwidth of the signal using
            %      network-averaged noise spectrum. 
            [snr , hrss, hpeak, Fchar, bw, Tchar, dur] = xoptimalsnr( ...
                [hp,hc,hb],0,fs,Savg,fSnet(1),fSnet(2)-fSnet(1));
            % ---- Pick FFT resolution matching bandwidth.
            tmp = -round(log2(bw));
            if tmp < -7
                tmp = -7;
            elseif tmp > -2
                tmp = -2;
            end
            Tfft = 2^tmp;
            disp(['Reset Tfft = ' num2str(Tfft) ' to match signal bandwidth.'])

        elseif strcmp(analysis.pixelselection,'maxsnr') || strcmp(analysis.pixelselection,'minNpix')

            % ---- Reset Tfft to maximise SNR2 packed into Npix pixels or
            %      minimise Npix for fixed SNR2.
            maxSNR2 = -Inf;
            mincount90 = Inf;
            for Tfft = 2.^[-7:-2]
                % ---- Reset TF parameters.
                noverlap = fs*Tfft/2;
                wind = modifiedhann(fs*Tfft);
                % ---- Select TF pixels to analyse by selecting them from a
                %      noise-weighted TF map. 
                % ---- Make spectrogram.
                [fhp,F] = spectrogram(hp,wind,noverlap,Tfft*fs,fs);
                [fhc,F] = spectrogram(hc,wind,noverlap,Tfft*fs,fs);
                [fhb,F] = spectrogram(hb,wind,noverlap,Tfft*fs,fs);
                map = abs(fhp).^2+abs(fhc).^2+abs(fhb).^2;
                % ---- Noise-weight map using network-averaged spectrum
                %      interpolated to frequencies of spectrogram. 
                map = map ./ repmat(interp1(fSnet,Savg,F),1,size(map,2));
                % ---- Standard spectrogram analysis.
                thresh = prctile(map(:),100*(1-Npix/prod(size(map))));
                idx = find(map > thresh);
                [irow,icol] = ind2sub(size(map),idx);
                % ---- Fractional SNR2 accumulated.
                SNR2 = sum(map(idx))/sum(map(:));
                if strcmp(analysis.pixelselection,'minNpix')
                    smap = sort(map(:),'descend');
                    csmap = cumsum(smap);
                    csmap = csmap/csmap(end);
                    count90 = find(csmap>=0.9);
                    if length(count90)>0
                        count90 = count90(1);
                        if count90<mincount90
                            mincount90 = count90;
                            Tfft_opt = Tfft;
                            Npix_opt = count90;
                        end
                    end
                elseif strcmp(analysis.pixelselection,'maxsnr')
                    if SNR2>maxSNR2
                        maxSNR2=SNR2;
                        Tfft_opt = Tfft;
                        Npix_opt = Npix;
                    end
                end
            end
            % ---- Reset to use optimal Tfft / Npix.
            Tfft = Tfft_opt;
            Npix = Npix_opt;
            noverlap = fs*Tfft/2;
            wind = modifiedhann(fs*Tfft);
            if strcmp(analysis.pixelselection,'minNpix')
                disp(['Reset Tfft = ' num2str(Tfft) ', Npix = ' num2str(Npix) ' to minimise Npix.'])
            elseif strcmp(analysis.pixelselection,'maxsnr')
                disp(['Reset Tfft = ' num2str(Tfft) ' to maximise SNR2.'])
            end
        end

        % ---- Select TF pixels to analyse by selecting them from a 
        %      noise-weighted TF map. 
        % ---- Make spectrogram.
        [fhp,F] = spectrogram(hp,wind,noverlap,Tfft*fs,fs);
        [fhc,F] = spectrogram(hc,wind,noverlap,Tfft*fs,fs);
        [fhb,F] = spectrogram(hb,wind,noverlap,Tfft*fs,fs);
        map = abs(fhp).^2+abs(fhc).^2+abs(fhb).^2;
        if analysis.usenoise
            % ---- Noise-weight map using network-averaged spectrum interpolated
            %      to frequencies of spectrogram. 
            map = map ./ repmat(interp1(fSnet,Savg,F),1,size(map,2));
        else
            % ---- Why would I want to do this???
            warning('Ignoring noise background in selecting TF pixels.');
        end
        % ---- Use spectrogram to select TF pixels.
        if strcmp(analysis.mode,'emulatefft') && exist('irow')
            % ---- irow defined above - Kludged analysis where we emulate single
            %      FFT.
            [val,icol] = max(sum(map,1));
            icol = icol*ones(size(irow));
            idx = sub2ind(size(map),irow,icol);
        elseif strcmp(analysis.pixelselection,'bpp')
            % ---- Keep loudest 1% of pixels, assuming a total SNR^2 of order 25.
            % ---- Normalise map to approx. SNR^2.
            map = 25^2/sum(map(:)) * map;
            bpp = 99;
            % thresh = prctile(map(:),bpp);
            thresh = -log(1-bpp/100);
            idx = find(map+1 > thresh);
            [irow,icol] = ind2sub(size(map),idx);
        else
            % ---- Standard spectrogram analysis.
            thresh = prctile(map(:),100*(1-Npix/prod(size(map))));
            idx = find(map > thresh);
            [irow,icol] = ind2sub(size(map),idx);
        end
        % ---- Fractional SNR2 accumulated.
        SNR2 = sum(map(idx))/sum(map(:));

        % ---- Select desried TF pixels.
        fhp = fhp(idx).'; %-- store as row vector (using non-conjugate transpose)
        fhc = fhc(idx).';
        fhb = fhb(idx).';
        % ---- Frequencies of kept pixels.
        f = F(irow);
        Nf = length(f);
        disp(['Using ' num2str(Nf) ' time-frequency pixels.']);
        disp(['Frequencies: ' num2str(sort(f(:).')) ]);
        SNR2 = sum(map(idx))/sum(map(:));
        disp(['Approx fractional SNR^2: ' num2str(SNR2)]);

        % ---- Reset TF variables according to Tfft actually used.
        T = Tfft;
        df = 1/Tfft;
        N = Tfft*fs;

    elseif strcmp(analysis.mode,'fft')

        % ---- Select frequency bins to analyse by selecting them from a single
        %      fft of the signal. Adjust the FFT length to approximately match
        %      the duration of the signal. 
        % ---- Compute duration and bandwidth of signal; use this to determine
        %      frequency range and resolution to use for FFTs.
        if analysis.usenoise
            % ---- Compute noise-weighted properties of the signal using
            %      network-averaged noise spectrum. 
            warning(['Current implementation of xoptimalsnr does not use ' ...
                'noise weighting for computing signal duration.']);
            [snr , hrss, hpeak, Fchar, bw, Tchar, dur] = xoptimalsnr( ...
                [hp,hc,hb],0,fs,Savg,fSnet(1),fSnet(2)-fSnet(1));
        else
            [snr , hrss, hpeak, Fchar, bw, Tchar, dur] = xoptimalsnr([hp,hc,hb],0,fs);
        end
        % ---- Keep time interval +/- 3*dur centred on Tchar, and rounded to a
        %      power of 2. 
        N = 2^nextpow2(6*dur*fs);
        startindex = round(Tchar*fs-N/2);
        hp = hp(startindex:(startindex+N-1));
        hc = hc(startindex:(startindex+N-1));
        hb = hb(startindex:(startindex+N-1));
        %
        % ---- Length of the FFT [s].
        T = N/fs;
        % ---- Frequency resolution of FFTs [Hz].
        df = fs/N;

        % ---- Fourier Transform waveforms and restrict frequency range. Store
        %      as row vectors.
        fhp = fft(hp).';
        fhc = fft(hc).'; 
        fhb = fft(hb).';
        % ---- (Positive) Frequencies at which ffts are sampled.
        f = [0+df:df:fs/2-df];
        %f = [0:df:fs/2, -fs/2+df:df:-df];
        % ---- Retain only positive frequencies.
        if analysis.usenoise
            % ---- Retain only positive frequencies within +/-const * bw of
            %      Fchar for the signal.
            index = find(f>=Fchar-6*bw & f<=min(Fchar+6*bw,2048));
        else
            % ---- All frequency bins noise-free so use "all" of them.
            index = find(f>=40 & f<min(2048,fs/2));
        end
        fhp = fhp(index);
        fhc = fhc(index);
        fhb = fhb(index);
        f = f(index);
        Nf = length(f);
        disp(['Using ' num2str(Nf) ' frequency bins.']);
        disp(['Frequencies: ' num2str(f) ]);

    else

        error(['analysis.mode = ' analysis.mode ' not recognised.']);

    end


    % =========================================================================
    %    Compute antenna response functions for the network.
    % =========================================================================

    % ---- Compute antenna responses.
    Fp = zeros(Nsky,Nifo);
    Fc = zeros(Nsky,Nifo);
    Fb = zeros(Nsky,Nifo);
    for a = 1:Nifo
        detector = detectorsCell{a};
        [Fp_temp Fc_temp, Fb_temp] = ComputeAntennaResponse(Phi, Theta, 0, detector);
        % ---- Pack antenna responses for all detectors into one matrix, one
        %      sky position per row.
        Fp(:,a) = Fp_temp;
        Fc(:,a) = Fc_temp;
        Fb(:,a) = Fb_temp;
    end
    clear Fp_temp Fc_temp Fb_temp detector

    % ---- Noise-spectrum weighting factor for each detector and frequency bin.
    for a = 1:Nifo
        w(a,:) = sqrt((N/2)*interp1(fSnet',Snet(:,a)',f));
    end

    % ---- Noise-weighted antenna responses.  Dimensions are (sky position) x
    %      (detector) x (frequency bin).
    Fpw = zeros(Nsky,Nifo,Nf);
    Fcw = zeros(Nsky,Nifo,Nf);
    Fbw = zeros(Nsky,Nifo,Nf);
    for j = 1:Nf
        Fpw(:,:,j) = Fp*diag(w(:,j).^(-1));
        Fcw(:,:,j) = Fc*diag(w(:,j).^(-1));
        Fbw(:,:,j) = Fb*diag(w(:,j).^(-1));
    end

    % ---- Convert to dominant polarization frame. (Dominant frame values are
    %      denoted with lowercase f.)
    fpw = zeros(Nsky,Nifo,Nf);
    fcw = zeros(Nsky,Nifo,Nf);
    fbw = zeros(Nsky,Nifo,Nf);
    for j = 1:Nf
        [fpw(:,:,j), fcw(:,:,j)] = convertToDominantPolarizationFrame( ...
            Fpw(:,:,j), ...
            Fcw(:,:,j)  ...
        );
        fbw = Fbw;
    end


    % =========================================================================
    %    Compute unit vectors of dominant frame.
    % =========================================================================

    % ---- Note: The circ and loghbayesiancirc likelihoods are defined as follows.
    % E^{R,L} = |d_+ +/- id_x|^2 / (f_+^2+f_x^2)           ; DPF, d_+ := f_+ \cdot d, etc.
    %         = [|d_+|^2 + |d_x|^2 -/+ 2Im(d_+^* d_x)] / (f_+^2+f_x^2) 
    %         = \sum_{a,b} d_a^* hat{e}^*_a hat{e}_b d_b   
    % where hat{e}^{R,L} = f_+ +/- i f_x / (f_+^2+f_x^2)^0.5
    % E_circ := max(E^R, E^L)
    % E_circ_bayes = 1/2 [ A/(1+A) * E^{R,L} - ln(1+A) ] 
    % A := \sigma^2 * (f_+^2+f_x^2) / 2
    % E_LHBC := ln( \sum_sigma exp{ max(E_circ_bayes^R , E_circ_bayes^L) } )
    sumfp2 = sum(fpw.^2,2);
    sumfc2 = sum(fcw.^2,2);
    sumfb2 = sum(fbw.^2,2);
    irat = sqrt(-1);
    for a = 1:Nifo
        ewp{a} = squeeze(fpw(:,a,:)./sqrt(sumfp2));
        ewc{a} = squeeze(fcw(:,a,:)./sqrt(sumfc2));  
        ewb{a} = squeeze(fbw(:,a,:)./sqrt(sumfb2));
        ewright{a} = squeeze((fpw(:,a,:) + irat * fcw(:,a,:))./sqrt(sumfp2+sumfc2));
        ewleft{a}  = squeeze((fpw(:,a,:) - irat * fcw(:,a,:))./sqrt(sumfp2+sumfc2));
    end


    % =========================================================================
    %    Compute projection operators Pab in dominant frame.
    % =========================================================================

    for a = 1:Nifo
        for b = 1:Nifo
            Pp{a,b} = ewp{a}.*ewp{b};                  % P plus
            Pc{a,b} = ewc{a}.*ewc{b};                  % P cross
            Pb{a,b} = ewb{a}.*ewb{b};                  % P scalar
            Pright{a,b} = conj(ewright{a}).*ewright{b};
            Pleft{a,b}  = conj(ewleft{a}).*ewleft{b};
            if a==b
                Ptot{a,b} = ones(Nsky,Nf);
            else
                Ptot{a,b} = zeros(Nsky,Nf);
            end
        end
    end

    
    % =========================================================================
    %    Loop over injection scales, performing simulated analysis.
    % =========================================================================

    % ---- Loop over injection scales.
    for kk=1:length(injectionScales)

        if analysis.verbose
            disp(['Processing injection scale ' num2str(kk) ' of ' ... 
                num2str(length(injectionScales)) '.']);
        end

        % ---- Current injection scale.
        scaleFactor = injectionScales(kk);

        % =========================================================================
        %    Sky localisation analysis.
        % =========================================================================

        %  For each injection scale:
        %    for each injection:
        %      - for all grid pos compute phase delay from gridDelay-injDelay 
        %      - compute detector responses
        %      - compute all likelihoods for all grid pos [using inj F+,x, delays]
        %      - select recovered sky position (grid pos of max likelihood or
        %        inj pos, depending on whether testing sky localisation)

        % ---- Storage for pointing info.
        injThetaRec{kk} = zeros(Ninj,1);
        injPhiRec{kk}   = zeros(Ninj,1);
        likelihood{kk}  = zeros(Ninj,18);

        % ---- Loop over injections.
        for iInj=1:Ninj

            % ---- Compute phase delay matrix for delays between selected
            %      injection's position and all sky grid points.  
            relativeDelay = gridDelay-repmat(injDelay(iInj,:),Nsky,1);
            delayMatrix = zeros(Nsky,Nifo,Nf);
            for ifreq=1:Nf
                delayMatrix(:,:,ifreq) = exp(1i*2*pi*f(ifreq)*relativeDelay);
            end

            % ---- Compute network response ... depends on injection scale,
            %      unlike real analysis. 
            % ---- Generate additive Gaussian noise.
            fn = randn(Nifo,Nf) + i*randn(Nifo,Nf);
            if analysis.usenoise
                % ---- Normalise to unit variance in each pixel.
                fn = fn / sqrt(2);
            else
                % ---- Optionally zero-out noise.
                fn = fn / 1e6;
            end
            % ---- Compute detector responses. Notes:
            %        i. injFp, etc. do not include noise weighting.
            %       ii. Dimensions of 3D quantities are:
            %           (sky position) x (detector) x (frequency bin).
            %      iii. Include inclination angle effects here for hp, hc.
            clear d
            for a = 1:Nifo
                d{a} = squeeze(delayMatrix(:,a,:)).*repmat(fn(a,:),Nsky,1) ...
                       + scaleFactor * ( ...
                           Ap(iInj) * injFp(iInj,a) * repmat(fhp./w(a,:),Nsky,1) .* squeeze(delayMatrix(:,a,:)) ...
                         + Ac(iInj) * injFc(iInj,a) * repmat(fhc./w(a,:),Nsky,1) .* squeeze(delayMatrix(:,a,:)) ...
                         +            injFb(iInj,a) * repmat(fhb./w(a,:),Nsky,1) .* squeeze(delayMatrix(:,a,:)) ...
                       );
            end

            % ---- Correct for TF windowing & overlapping (non-spectrogram case).
            % ---- In the actual search the TF maps are made using 50% overlap and
            %      modifiedhann window.  Separate studies (see norm_test*) indicate
            %      this increases measured trigger energies by a factor of
            %      approximately 1.5-3.  Applying scale factor corrections gives very
            %      good matches to Ep,Ip distributions for 90% UL inj amplitude. 
            % ---- Corrections are applied by supplying a vector 'ratio' of possible
            %      rescalings.  Each event is rescaled by random uniform selection of
            %      one of these sample values.
            % ---- Window correction factor.
            rescale = 1;
            if strcmp(analysis.mode,'fft')
                if exist('ratio') && ~isempty(ratio)
                    % ---- Random sampling of ratio corrections.  See
                    % ~/Documents/xpipeline/consistency_test_thresholds/norm_test_extended*
                    % disp('Applying rescaling corrections to energies.'); 
                    rescale = ratio(unidrnd(length(ratio),Nsky,1));
                else
                    warning('No corrections applied for time-frequency overlapping and windowing.');
                end
            end

            % ---- Compute all likelihoods for all grid points.

            % ---- Prior amplitudes for Bayesian likelihoods.
            %      ssigma: in xtimefrequencymap this is set to 10.^[-23:0.5:-21].
            %      However, that code uses the convention S = S(f) (continuous) rather
            %      than S = S[k] (discrete), where S[k] = fs*S(f). Therefore the
            %      weighted antenna responses differ as 
            %        Fpw_xtfm = Fp / S(f)^0.5 = fs^0.5 * Fpw_paper
            %      To compensate, we must use ssigma = fs^0.5 * 10.^[-23:0.5:-21].  
            ssigma = fs^0.5*10.^[-23:0.5:-21];
            % ssigma = 10.^[-19:0.5:-17]; %-- this gave better performance in some early tests

            % ---- Initialize storage.
            Ep     = zeros(Nsky,1); %-- plus energy
            Ec     = zeros(Nsky,1); %-- cross energy
            Eright = zeros(Nsky,1); %-- right-circularly polarised energy
            Eleft  = zeros(Nsky,1); %-- left-circularly polarised energy
            Etot   = zeros(Nsky,1); %-- total energy
            Icirc  = zeros(Nsky,1); %-- Note: Il = Ir = Icirc 
            Ip     = zeros(Nsky,1); %-- plus inc
            Ic     = zeros(Nsky,1); %-- plus inc
            Eb     = zeros(Nsky,1); %-- scalar (breathing mode) energy
            Ib     = zeros(Nsky,1);
            % ---- Bayesian statistics are initialised to -Inf for correct
            %      behaviour when marignlsing with logsumexp().
            Elhbcirc = -inf(Nsky,1);  %-- loghbayesiancirc statistic
            Elhb     = -inf(Nsky,1); %-- loghbayesian statistic
            Elhbb    = -inf(Nsky,1); %-- loghbayesian - scalar (breathing mode) statistic
            % ---- Temporary variables needed for computing Bayesian
            %      likelihoods. These hold summed-over-pixels likelihoods for
            %      each ssigma value.
            Elhbcirc_tmp = zeros(Nsky,2*length(ssigma));
            Elhb_tmp     = zeros(Nsky,length(ssigma));
            Elhbb_tmp    = zeros(Nsky,length(ssigma));

            % ---- Loop over frequencies.
            for j = 1:Nf
                % ---- Initialise temporary variables used in constructing
                %      Bayesian likelihoods. These are reset for every pixel.
                tmpp = 0;
                tmpc = 0;
                tmpr = 0;
                tmpl = 0;
                tmpb = 0;
                % ---- Compute projection likelihoods and their incoherent
                %      counterparts by looping over detector indices. 
                %      Note: size(Pp{1,1}) = Nsky x Nf
                for a = 1:Nifo
                    for b = 1:Nifo
                        Ep     = Ep     + rescale .* conj(d{a}(:,j)).*Pp{a,b}(:,j).*d{b}(:,j);
                        Ec     = Ec     + rescale .* conj(d{a}(:,j)).*Pc{a,b}(:,j).*d{b}(:,j);
                        Eright = Eright + rescale .* conj(d{a}(:,j)).*Pright{a,b}(:,j).*d{b}(:,j);
                        Eleft  = Eleft  + rescale .* conj(d{a}(:,j)).*Pleft{a,b}(:,j).*d{b}(:,j);
                        Etot   = Etot   + rescale .* conj(d{a}(:,j)).*Ptot{a,b}(:,j).*d{b}(:,j);
                        Eb     = Eb     + rescale .* conj(d{a}(:,j)).*Pb{a,b}(:,j).*d{b}(:,j); 
                        %
                        tmpp   = tmpp   + rescale .* conj(d{a}(:,j)).*Pp{a,b}(:,j).*d{b}(:,j);
                        tmpc   = tmpc   + rescale .* conj(d{a}(:,j)).*Pc{a,b}(:,j).*d{b}(:,j);
                        tmpr   = tmpr   + rescale .* conj(d{a}(:,j)).*Pright{a,b}(:,j).*d{b}(:,j);
                        tmpl   = tmpl   + rescale .* conj(d{a}(:,j)).*Pleft{a,b}(:,j).*d{b}(:,j);
                        tmpb   = tmpb   + rescale .* conj(d{a}(:,j)).*Pb{a,b}(:,j).*d{b}(:,j);
                    end
                    Ip    = Ip    + rescale .* conj(d{a}(:,j)).*Pp{a,a}(:,j).*d{a}(:,j);
                    Ic    = Ic    + rescale .* conj(d{a}(:,j)).*Pc{a,a}(:,j).*d{a}(:,j);
                    Icirc = Icirc + rescale .* conj(d{a}(:,j)).*Pright{a,a}(:,j).*d{a}(:,j);
                    Ib    = Ib    + rescale .* conj(d{a}(:,j)).*Pb{a,a}(:,j).*d{a}(:,j);
                end
                % ---- Apply Bayesian priors (amplitude ssigma, polarisation) to
                %      likelihoods and store for later marignalisation after
                %      summing over all pixels.   
                % ---- Loop over prior amplitudes. Recall 
                %      size(sumfp2) = Nsky x 1 x Nf, etc.
                for iii=1:length(ssigma)
                    % ---- loghbayesiancirc running sum over pixels. Columns
                    %      iii=1:length(ssigma) hold the right polarisation
                    %      likelihoods, columns length(ssigma)+1:2*length(ssigma) 
                    %      hold the left ones.
                    A = ssigma(iii)^2 * squeeze(sumfp2(:,1,j)+sumfc2(:,1,j))/2;
                    Elhbcirc_tmp(:,iii) = Elhbcirc_tmp(:,iii) + 1/2 * ( A./(1+A) .* tmpr - log(1+A) );
                    Elhbcirc_tmp(:,iii+length(ssigma)) = Elhbcirc_tmp(:,iii+length(ssigma)) ...
                        + 1/2 * ( A./(1+A) .* tmpl - log(1+A) );
                    % ---- loghbayesian running sum over pixels.
                    trtempp = (squeeze(sumfp2(:,1,j))*ssigma(iii)^2);
                    trtempc = (squeeze(sumfc2(:,1,j))*ssigma(iii)^2);
                    Elhb_tmp(:,iii) = Elhb_tmp(:,iii) + 0.5 * ( ...
                               (1 + (trtempp).^-1).^(-1) .* tmpp ...
                             + (1 + (trtempc).^-1).^(-1) .* tmpc ...
                             - log((1 + trtempp)) - log(1+trtempc) ...
                            );
                    % ---- loghbayesianscalar running sum over pixels.
                    trtemp  = (squeeze(sumfb2(:,1,j))*ssigma(iii)^2);
                    Elhbb_tmp(:,iii) = Elhbb_tmp(:,iii) + 0.5 * ( ...
                        (1 + (trtemp).^-1).^(-1) .* tmpb - log(1 + trtemp) );
                end
            
            end  %-- loop over frequencies
            %
            % ---- Bayesian likelihoods: marginalise over amplitude prior (all)
            %      and polarisations (circ).
            for iii = 1:length(ssigma)
                Elhbcirc = logsumexp(Elhbcirc,Elhbcirc_tmp(:,iii));
                Elhbcirc = logsumexp(Elhbcirc,Elhbcirc_tmp(:,iii+length(ssigma)));
                Elhb     = logsumexp(Elhb,Elhb_tmp(:,iii));
                Elhbb    = logsumexp(Elhbb,Elhbb_tmp(:,iii));
            end
            % ---- Bayesian likelihoods: apply sky prior (all).
            Elhbcirc = Elhbcirc + log(gridProb);
            Elhb     = Elhb     + log(gridProb);
            Elhbb    = Elhbb    + log(gridProb);

            % ---- Check that all computed likelihoods are real up to
            %      numerical error (tol=1e-9), and cast off imaginary parts.
            tol = 1e-9;
            if isrealenough(Etot,tol,'Etot');     Etot = real(Etot);     end
            if isrealenough(Ep,tol,'Ep');         Ep = real(Ep);         end
            if isrealenough(Ip,tol,'Ip');         Ip = real(Ip);         end
            if isrealenough(Ec,tol,'Ec');         Ec = real(Ec);         end
            if isrealenough(Ic,tol,'Ic');         Ic = real(Ic);         end
            if isrealenough(Eright,tol,'Eright'); Eright = real(Eright); end
            if isrealenough(Eleft,tol,'Eleft');   Eleft = real(Eleft);   end
            if isrealenough(Icirc,tol,'Icirc');   Icirc = real(Icirc);   end
            if isrealenough(Elhbcirc,tol,'Elhbcirc');   Elhbcirc = real(Elhbcirc);   end
            if isrealenough(Eb,tol,'Eb');         Eb = real(Eb);         end
            if isrealenough(Ib,tol,'Ib');         Ib = real(Ib);         end
            if isrealenough(Elhb,tol,'Elhb');     Elhb = real(Elhb);     end
            if isrealenough(Elhbb,tol,'Elhbb');   Elhbb = real(Elhbb);     end
            % ---- Compute desired likelihood using known relationships.
            Ecirc  = max([Eright,Eleft],[],2);
            En     = Etot - Ep - Ec;
            In     = Etot - Ic - Ip;
            Ecircn = Etot - Ecirc - En;
            Icircn = Etot - Icirc - In;
            Enb = Etot - Eb;
            Inb = Etot - Ib;
            % ---- Assign event significance.
            switch detectionStat
                case 'loghbayesianscalar'
                    signif = Elhbb;
                case 'loghbayesiancirc'
                    signif = Elhbcirc;
                case 'loghbayesian'
                    signif = Elhb;
                case 'circenergy'
                    signif = Ecirc;
                case 'plusenergy'
                    signif = Ep;
                case 'standard'
                    signif = Ep+Ec;
                otherwise
                    error(['Detection statistic ' detectionStat ' not recognised.']);
            end

            % ---- Compute experimental sky pointing statistics.
            testStatN  = Ecirc.*(1 - Ecircn./Icircn);
            testStatC  = Ecirc.*(1 - Icirc./Ecirc);
            testStatCN = Ecirc.*(1 - Ecircn./Icircn).*(1 - Icirc./Ecirc);
            %testStatCN = Ecirc.*(1 - Ecircn./Icircn).*(1 - (Icirc./Ecirc-1/length(detectorsCell)));

            % ---- Determine grid position of maximum desired likelihood.
            pointingName = analysis.pointingName;
            eval(['pointingStat = ' pointingName ';']);
            [val,ind] = max(pointingStat);

            % ---- Optionally reset recovered sky position.
            if analysis.testSkyLocalisation
                if length(trigger.gps)==1
                    % ---- GRB-style analysis: we'll use the
                    %      max-pointing-statistic grid point as the  
                    %      recovered location. Nothing further to do here!
                    ;
                elseif length(trigger.gps)==2 & trigger.err == 0
                    % ---- SN-style analysis: only one grid point is analysed
                    %      per job. So forcibly reset the recovered location to
                    %      be the closest grid point (which would be the one
                    %      used for that GPS time). 
                    [val,ind] = min(abs(injPhi(iInj)-Phi));
                end
            else
                % ---- "Ideal" analysis, here we force injections to be exactly
                %      on a grid point and that groid point to be selected. Not
                %      realistic, except for Swift GRBs, but useful for debugging!  
                ind = iInj;
            end
            
            % ---- Record recovered sky position and likelihoods for this
            %      position.
            injThetaRec{kk}(iInj) = Theta(ind);
            injPhiRec{kk}(iInj)   = Phi(ind);
            likelihood{kk}(iInj,:) = [Elhbcirc(ind) Ecirc(ind) Icirc(ind) ...
                Ecircn(ind) Icircn(ind), En(ind), In(ind), Elhb(ind), ...
                Ep(ind), Ip(ind), Ec(ind), Ic(ind), Etot(ind), ...
                Elhbb(ind), Eb(ind), Ib(ind), Enb(ind), Inb(ind)];
            likelihoodType = {'loghbayesiancirc','circenergy','circinc', ...
                'circnullenergy','circnullinc','nullenergy','nullinc',...
                'loghbayesian','plusenergy', ...
                'plusinc','crossenergy','crossinc','totalenergy',...
                'loghbayesianscalar','scalarenergy','scalarinc',...
                'scalarnullenergy','scalarnullinc'};
            % ---- KLUDGE: missing likelihoods:
            %      standard,energyitf1,energyitf2,energyitf3,skypositiontheta,skypositionphi

        end %-- loop over injections

        % ---- Optionally make some plots.
        if analysis.makeSkyPlots

            Vinj = CartesianPointingVector(injPhi,injTheta);
            Vrec = CartesianPointingVector(injPhiRec{kk},injThetaRec{kk});
            %
            tag = ['_' num2str(1000*analysis.delayTol) 'ms'];
            %
            figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
            hist(acos(sum(Vinj.*Vrec,2))*180/pi);
            xlabel('Angular error [deg]');
            ylabel('number of injections');
            title([pointingName ...
                   ' (' waveform.type ',' waveform.parameters{iNwf} ')' ...
                   ', injSc: ' num2str(kk) ...
                   ', med err = ' num2str(median(acos(sum(Vinj.*Vrec,2))*180/pi)) ' deg']);
            saveas(gcf,[pointingName '_hist' tag '.png'],'png');
            %
            figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
            for iInj=1:Ninj
                plot([injPhi(iInj);injPhiRec{kk}(iInj)],[injTheta(iInj);injThetaRec{kk}(iInj)],'-+');
                grid on; hold on;
            end
            xlabel('phi [rad]');
            ylabel('theta [rad]');
            title(['pointing statistic: ' pointingName]);
            xlim = get(gca,'xlim');
            ylim = get(gca,'ylim');
            [phi_center, theta_center] = radectoearth(trigger.ra,trigger.dec,trigger.gps);
            [isoLines baselineNames] = xisotimedelay([theta_center, phi_center], detectorsCell);
            lineStyle = {'gx','rx','mx','cx','kx'};
            for iLine=1:size(isoLines,1)
                idx = find(isoLines(iLine,:,2)>=xlim(1) & isoLines(iLine,:,2)<=xlim(2) ...
                    & isoLines(iLine,:,1)>=ylim(1) & isoLines(iLine,:,1)<=ylim(2) );
                plot(isoLines(iLine,idx,2),isoLines(iLine,idx,1),lineStyle{iLine});
                grid on; hold on;
                legendText{iLine} = [baselineNames{iLine} ' (' lineStyle{iLine} ')'];
            end
            legend(legendText);
            saveas(gcf,[pointingName '_scat' tag '.png'],'png');
            %
            figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
            % plot(likelihood{kk}(:,2)./likelihood{kk}(:,3),likelihood{kk}(:,4)./likelihood{kk}(:,5),'.');
            plot(likelihood{kk}(:,3)./likelihood{kk}(:,2),likelihood{kk}(:,4)./likelihood{kk}(:,5),'.');
            IcOEc   = prctile(likelihood{kk}(:,3)./likelihood{kk}(:,2),[50 90]); 
            EcnOIcn = prctile(likelihood{kk}(:,4)./likelihood{kk}(:,5),[50 90]); 
            legend(['median position: (' num2str(IcOEc(1)) ',' num2str(EcnOIcn(1)) ')']);
            xlim = get(gca,'xlim');
            ylim = get(gca,'ylim');
            if xlim(end)<=1 & ylim(end)<=1
                axis([0 1 0 1]);
            end
            grid on; hold on;
            plot([0;1],[EcnOIcn(2);EcnOIcn(2)],'m--','linewidth',2);
            plot([IcOEc(2);IcOEc(2)],[0;1],'m--','linewidth',2);
            % xlim = get(gca,'xlim');
            % ylim = get(gca,'ylim');
            % plot([1;xlim(2)],[1;1],'m--','linewidth',2);
            % plot([1;1],[1;ylim(1)],'m--','linewidth',2);
            title(['pointing statistic: ' pointingName]);
            % xlabel('Ecirc./Icirc');
            xlabel('Icirc./Ecirc');
            ylabel('Ecircnull./Icircnull');
            saveas(gcf,[pointingName '_cuts' tag '.png'],'png')

        end %-- if analysis.makeSkyPlots

        % ---- Efficiency using all cuts.
        switch analysis.type
            case 'scalar'
                % ---- This bit does not work!
                eff(kk) = mean( ...
                       (abs(nsigmanb) > cutThresholds(1)) ...
                     & (abs(nsigmab)  > cutThresholds(2)) ...
                     & (abs(fixedrnb) > cutThresholds(4)) ...
                     & (abs(fixedrb)  > cutThresholds(5)) ...
                     & (signif > cutThresholds(7))  ...
                );
            case 'circ'
                pass1 = applycut(En,In,cutThresholds(1),cutTypes{1});
                pass2 = applycut(Ecirc,Icirc,cutThresholds(2),cutTypes{2});
                pass3 = applycut(Ecircn,Icircn,cutThresholds(3),cutTypes{3});
                pass4 = applycut(En,In,cutThresholds(4),cutTypes{4});
                pass5 = applycut(Ecirc,Icirc,cutThresholds(5),cutTypes{5});
                pass6 = applycut(Ecircn,Icircn,cutThresholds(6),cutTypes{6});
                pass7 = (signif > cutThresholds(7)) ;
                eff(kk) = eff(kk) + 1/Nwf * mean( ...
                    pass1 & pass2 & pass3 & pass4 & pass5 & pass6 & pass7 ...
                    );
            case 'linear'
                pass1 = applycut(En,In,cutThresholds(1),cutTypes{1});
                pass2 = applycut(Ep,Ip,cutThresholds(2),cutTypes{2});
                pass3 = applycut(Ec,Ic,cutThresholds(3),cutTypes{3});
                pass4 = applycut(En,In,cutThresholds(4),cutTypes{4});
                pass5 = applycut(Ep,Ip,cutThresholds(5),cutTypes{5});
                pass6 = applycut(Ec,Ic,cutThresholds(6),cutTypes{6});
                pass7 = (signif > cutThresholds(7)) ;
                eff(kk) = eff(kk) + 1/Nwf * mean( ...
                    pass1 & pass2 & pass3 & pass4 & pass5 & pass6 & pass7 ...
                    );
        end
        % ---- Keep copy of efficiency  for this waveform.
        effAll{iNwf}(kk) = mean( pass1 & pass2 & pass3 & pass4 & pass5 & pass6 & pass7 );
        
    end  %-- loop over injection scales
    
    % ---- Scatter plots of (Ip,Ep), (Ic,Ec), (In,En), overlaid on web-page
    %      scatter plots if available. 
    if analysis.scatterplots

        if plotOpen==false 
            filename = [pathToFigs '/off_scatter_' waveform.set '_' waveform.name '_plusenergy_plusinc_connected.fig'];
            if exist(filename)==2
                fig1 = open(filename);
                set(gcf,'position',[200 500 560 420]);
            else
                display(['File not found: ' filename ]);
                fig1 = figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
            end
            filename = [pathToFigs '/off_scatter_' waveform.set '_' waveform.name '_crossenergy_crossinc_connected.fig'];
            if exist(filename)==2
                fig2 = open(filename);
                set(gcf,'position',[200 500 560 420]);
            else
                display(['File not found: ' filename ]);
                fig2 = figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
            end
            filename = [pathToFigs '/off_scatter_' waveform.set '_' waveform.name '_nullenergy_nullinc_connected.fig'];
            if exist(filename)==2
                fig3 = open(filename);
                set(gcf,'position',[200 500 560 420]);
            else
                display(['File not found: ' filename ]);
                fig3 = figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
            end
            plotOpen = true;
        end
        figure(fig1);
        hold on; grid on; legend off
        scatter(Ep,Ip,[],log10(signif));
        figure(fig2);
        hold on; grid on; legend off
        scatter(Ec,Ic,[],log10(signif));
        figure(fig3);
        hold on; grid on; legend off
        scatter(En,In,[],log10(signif));
        
    end  %-- loop over injection scales
   
end  %-- loop over waveforms


% =========================================================================
%    Make plots.
% =========================================================================

if analysis.makeFinalPlots
    % ---- Plot predicted efficiency, overlaid on actual measured search
    %      efficiency if available.
    filename = dir([pathToFigs 'eff_' waveform.set '.fig']);
    if exist([pathToFigs filename.name],'file')==2
        fig4 = open([pathToFigs filename.name]);
    else
        save workspace.mat
        error('Agh!');
        % figure; set(gca,'fontsize',16); set(gca,'xscale','log')
    end
    % ---- Remake plot in temrs of injection scale instead of hrss, since the web
    %      page gets the hrss wrong in the case of mixed waveform sets. Injection 
    %      scale is more robust.
    [X,Y] = datafromplot(gca);
    close(fig4);
    %
    % axis(gca);
    % grid on; hold on
    % semilogx(hrss_eff(1:(end-1)),eff(1:(end-1)),'b-o','markerfacecolor','g')
    % title(waveform.set)
    % xlabel('hrss amplitude (Hz^{-1/2})')
    % ylabel('efficiency')
    % legend('meas','meas UL','meas 50 %','alpha-stat',4)
    % 
    % ---- This bit is hardcoded for the OSN search ...
    disp(' ');
    disp('KLUDGE: using injection scales hard-coded for the OSN search.');
    injectionScalesMDC = [0.00100,0.00316,0.01000,0.01778,0.03162,0.05623,0.10000,0.13335,0.17783, ...
        0.23714,0.31623,0.42170,0.56234,0.74989,1.00000,1.33352,1.77828,2.37137, ...
        3.16228,4.21697,5.62341,7.49894,10.00000,13.33521,17.78279,23.71374, ...
        31.62278,56.23413,100.00000,316.22777,1000.00000];
    keep = [];
    for ii=1:length(Y)
        if numel(Y{ii})==numel(injectionScalesMDC)
            keep(end+1) = ii;
        end
    end
    if length(keep)~=2
        save workspace.mat
        error('More than two plots in efficiency figure with expected number of elements.');
    end
    % ---- Figure out which efficiency is without DQ vetoes (the higher one).
    if any(Y{keep(1)}>Y{keep(2)})
        y_noveto = Y{keep(1)};
        y = Y{keep(2)};
    else
        y_noveto = Y{keep(2)};
        y = Y{keep(1)};
    end
    % ---- Fresh plot.
    figure; set(gca,'fontsize',16); set(gca,'xscale','log')
    semilogx(injectionScalesMDC,y,'k-o','markerfacecolor','k','linewidth',2)
    grid on; hold on
    semilogx(injectionScalesMDC,y_noveto,'b-')
    semilogx(injectionScales(1:end-1),eff(1:end-1),'g-+','linewidth',2)
    legend('measured','measured (no vetoes)','predicted',2)
    xlabel('injection scale')
    ylabel('efficiency')
    title(waveform.set)
    axis([1e-3 1e3 0 1])
    saveas(gcf,['eff_' waveform.set '_' detectorsStr '_'  analysis.type '.png'],'png')

    % ---- Set output variables.
    injectionScales = injectionScales(1:end-1);
    eff = eff(1:end-1);
    for ii=1:Nwf
        effAll{ii} = effAll{ii}(1:end-1);
    end
end

% ---- Dump workspace for debugging.
if analysis.verbose
    save workspace.mat
end

% ---- Done.
return



% =========================================================================
%    Helper functions.
% =========================================================================

% function flag = isrealenough(x,threshold,name)
% % ISREALENOUGH - test for data being almost real and non-NaN
% %
% % usage
% %
% %   flag = isrealenough(x)
% %
% % x             Array of complex data.
% % threshold     Real scalar.
% % name          String. Name of data being tested. Used for output messagesany(isnan(.
% %
% % flag          Boolean. True if data obeys the conditions 
% %               max(abs(imag(x))./abs(x)) < threshold and no values of x are
% %               NaN.
% flag = true;
% x = x(:);
% if max(abs(imag(x))./abs(x)) >= threshold 
%     warning(['max(abs(imag(' name '))./abs(' name ')) = ' num2str(max(abs(imag(x))./abs(x))) ]);
%     flag = false;
% elseif any(isnan(real(x)))
%     warning(['NaN values for real(' name ')']);
%     flag = false;
% elseif any(isnan(imag(x)))
%     warning(['NaN values for imag(' name ')']);
%     flag = false;
% end
% return
