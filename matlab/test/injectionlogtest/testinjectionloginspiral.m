% ---- Matlab script to test injection logs for S6/VSR2-3 GRB search.
%      The input GRB list must be in chronological order.

% -----------------------------------------------------------------------------
%    User-specifiable parameters.
% -----------------------------------------------------------------------------

if ~exist('batch','var') || ~batch
    % ---- Set of GRBs to process.  Select one.
    % grbfile = 'test.txt'; 
    grbfile = 'grb_info.txt'; grbset = 'all';          %-- all GRBs.
    % grbfile = 'grb_info_Swift.txt'; grbset = 'swift';  %-- Swift-only GRBs
    % grbfile = 'grb_info_Fermi.txt'; grbset = 'fermi';  %-- Fermi GBM GRBs.

    % ---- Injection file to process.  Select one.
    % injfile = 'injection_csg100q9incljitter5.txt'; waveform = 'chirplet'; auxparams = '1.000e-02~1.000e+02~0.000e+00~0.000e+00'; maxincl = 5; f0 = 100; name = 'SGC100';
    % injfile = 'injection_csg150q9incljitter5.txt'; waveform = 'chirplet'; auxparams = '6.667e-03~1.500e+02~0.000e+00~0.000e+00'; maxincl = 5; f0 = 150;  name = 'SGC150'; 
    % injfile = 'injection_csg300q9incljitter5.txt'; waveform = 'chirplet'; auxparams = '3.333e-03~3.000e+02~0.000e+00~0.000e+00'; maxincl = 5; f0 = 300; name = 'SGC300';
    injfile = 'injection_insp1410incljitter30.txt'; waveform = 'inspiral'; auxparams = ''; maxincl = 30; f0 = 155; name = 'NSBH';
    % injfile = 'injection_insp1414incljitter30.txt'; waveform = 'inspiral'; auxparams = ''; maxincl = 30; f0 = 155; name = 'BNS';

    % ---- Process different networks separately.  Select one.
    targetnetwork = 'L1V1';

    % ---- Set matlab path
    addpath ~/xpipeline/trunk/misc/        
    addpath ~/xpipeline/trunk/share/       
    addpath ~/xpipeline/trunk/utilities/   
    addpath ~/xpipeline/trunk/searches/grb/
end

% -----------------------------------------------------------------------------


% ---- Open log file.
fid = fopen([name '_' targetnetwork '_' grbset '.log'],'w');

% ---- Auxiliary parameters to check.
auxparamsmat = tildedelimstr2numorcell(auxparams);

% ---- Start time of VSR3 (needed for modelling calibration uncertainties for V1).
tStartVSR3 = 965599215;
processedVSR3GRB = 0;  %-- flag to indicate switch from VSR2->VSR3 calibrations

% ---- Number of detectors in network.
nDetector = length(targetnetwork)/2;
clear detector
for ii = 1:nDetector
   detector{ii} = targetnetwork((ii-1)*2+1:(ii-1)*2+2);
end

% ---- Read and parse grb info file.
fidr = fopen(grbfile,'r');
grbinfo = textscan(fidr,'%s%s%s%f%f%f%s%s%s');
fclose(fidr);
grb       = grbinfo{1};
dir       = grbinfo{2};
network   = grbinfo{3};
gps       = grbinfo{4}; 
ra        = grbinfo{5};
dec       = grbinfo{6};
skyposerr = grbinfo{7};
injdistn  = grbinfo{8};
endoffset = grbinfo{9};

% ---- Timing test storage.
toffsetbin   = [-695:10:300]';
ntoffset_all = zeros(size(toffsetbin));
ntoffset_good = zeros(size(toffsetbin));
nanosecbin   = [5:10:1000]' * 1e6;
nnanosec_all = zeros(length(nanosecbin),nDetector);
deltatbin    = [0.005:0.01:1]';
% deltatbin    = [-100:100]*1e-2;
if nDetector==2
    ndeltat_all = zeros(length(deltatbin),1);
elseif nDetector==3
    ndeltat_all = zeros(length(deltatbin),3);
end
% ---- Polarization test storage.
psibin   = [0.5:1:100]' * pi/100;
npsi_all = zeros(length(psibin),nDetector);
% ---- Angular position storage.
radbin   = [0.5:1:100]' * 1/100;
nrad_all = zeros(size(radbin));
icdf_all = [];
azibin   = [0.5:1:100]' * 2*pi/100;
nazi_all = zeros(size(azibin));
X_old_1 = [];
X_old_4 = [];
% ---- Inclination storage.
% inclbin   = [-1+0.0005:0.001:1]';
if maxincl == 5
    inclbin   = [-1:0.0001:-0.995,0.995:0.0001:1]';
else
    inclbin   = [-1:0.01:-0.80,0.80:0.01:1]';
end
nincl_all = zeros(length(inclbin),nDetector);
% ---- Amplitude storage.
amplbin   = [0.005:0.01:1]';
% amplbin   = [0.05:0.1:2]';
nampl_all = zeros(length(amplbin),nDetector);
% ---- Mass bin storage.
massbin   = [0.005:0.01:1]';
nmass1_all = zeros(length(massbin),nDetector);
nmass2_all = zeros(length(massbin),nDetector);

% ---- Find GRBs with desired network.
desirednetwork = find(strcmp(network,targetnetwork));
% ---- Status report to log file ...
fprintf(fid,'%s\n',['---------------------------------------------------']);
fprintf(fid,'%s\n',['Processing network ' targetnetwork ' (' num2str(length(desirednetwork)) ' GRBs)']);
fprintf(fid,'%s\n',['  waveform: ' name]);
fprintf(fid,'%s\n',['   network: ' targetnetwork]);
fprintf(fid,'%s\n',['   GRB set: ' grbset]);
fprintf(fid,'%s\n');
% ---- ... and to screen.
disp(['---------------------------------------------------']);
disp(['Processing network ' targetnetwork ' (' num2str(length(desirednetwork)) ' GRBs)']);
disp(['  waveform: ' name]);
disp(['   network: ' targetnetwork]);
disp(['   GRB set: ' grbset]);
%disp(' ');

for igrb = desirednetwork(:).'
% for igrb = desirednetwork(1:2).'
 
    % ---- Status report.
    fprintf(fid,'%s\n',['---------------------------------------------------']);
    fprintf(fid,'%s\n',['Processing GRB ' grb{igrb}]);
    % disp(['Processing GRB ' grb{igrb}]);

    % ---- Read grb injection file.
    injectionFileName = [dir{igrb} 'input/' injfile];
    [injectionParameters] = readinjectionfile(injectionFileName);

    % ---- Number of injections.
    nInjection = size(injectionParameters,1);

    % ---- Separate each line (a string) into separate elements in a cell 
    %      array.  For actual GWB injections (instead of glitches) elements for
    %      jParam > 7 may be empty.
    for jParam=1:(7*nDetector)
        for iCell=1:nInjection
            [currentParameters{jParam}{iCell} injectionParameters{iCell}] = ...
                strtok(injectionParameters{iCell});
        end
    end

    % --------------------------------------------------------------------
    %    Run tests on injection parameters.
    % --------------------------------------------------------------------

    % ---- i.    injection time (first detector) - grb time: histogram on 
    %            [-700,+300] sec.  Test integer parts only (nanosec part 
    %            checked later).
    toffset = str2num(cell2mat(currentParameters{1}(:))) - floor(gps(igrb));
    ntoffset = hist(toffset,toffsetbin).';
    ntoffset_all = ntoffset_all + ntoffset;
    if isempty(strfind(dir{igrb},'3minalphaLin')) & (isempty(str2num(endoffset{igrb})) || (str2num(endoffset{igrb})<=60))
        ntoffset_good = ntoffset_good + ntoffset;
    end

    % ---- ii.   histogram of nanosec field of injection time (all detectors).
    for idet = 1:nDetector
        nanosec(:,idet) = str2double(currentParameters{(idet-1)*7+2}(:));
    end
    nnanosec = hist(nanosec,nanosecbin);
    nnanosec_all = nnanosec_all + nnanosec;
    if igrb==desirednetwork(1)
        nanosec_old = nanosec;
    elseif isequal(nanosec_old,nanosec)
        fprintf(fid,'%s\n',['Nanosec fields identical to first GRB.']);
    elseif ~isempty(strfind(dir{igrb},'3minalphaLin'))
        fprintf(fid,'%s\n',['Nanosec fields differ from first GRB: 3-min on-source window.']);
    elseif ~isempty(str2num(endoffset{igrb})) && (str2num(endoffset{igrb})>60)
        fprintf(fid,'%s\n',['Nanosec fields differ from first GRB: extended on-source window.']);
    else
        fprintf(fid,'%s\n',['Nanosec fields differ from first GRB: why unknown.']);
    end

    % ---- x.    verify than min(nanosec)>=0, max(nanosec)<=999999999.
    if any(nanosec(:)<0) | any(nanosec(:)>999999999)
        fprintf(fid,'%s\n',['WARNING: Invalid nanosec field for GRB ' grb{igrb}]);
    else
        fprintf(fid,'%s\n',['Nanosec fields valid.']);
    end

    % ---- iii.  difference in COE injection times for each detector pair, 
    %            compared to quadrature sum of timing & phase uncertainties for 
    %            that pair. 
    % ---- Phase & timing uncertainties for this detector pair.
    switch targetnetwork
        case 'H1L1V1'
            dphase = [10 10 2.865] * pi/180;
            dtime  = [20 20 8] * 1e-6;  
        case 'H1L1'
            dphase = [10 10] * pi/180;
            dtime  = [20 20] * 1e-6;  
        case {'H1V1','L1V1'}
            dphase = [10 2.865] * pi/180;
            dtime  = [20 8] * 1e-6;  
    end
    if nDetector==2
        % ---- Difference in integer part of GPS times.
        deltat = str2num(cell2mat(currentParameters{8}(:))) - str2num(cell2mat(currentParameters{1}(:)));
        % ---- Difference in nanosec part of GPS times.
        deltat = deltat + 1e-9*diff(nanosec,[],2);
        % ---- Compute quadrature-sum expected uncertainty.
        sigmat = (sum(dtime.^2+(dphase/(2*pi*f0)).^2))^0.5;
        deltat = normcdf(deltat,0,sigmat); 
        ndeltat = hist(deltat,deltatbin).';
        ndeltat_all = ndeltat_all + ndeltat;
    elseif nDetector==3
        % ---- Difference in integer part of GPS times.
        deltat(:,1) = str2num(cell2mat(currentParameters{8}(:))) - str2num(cell2mat(currentParameters{1}(:)));
        deltat(:,2) = str2num(cell2mat(currentParameters{15}(:))) - str2num(cell2mat(currentParameters{1}(:)));
        deltat(:,3) = str2num(cell2mat(currentParameters{15}(:))) - str2num(cell2mat(currentParameters{8}(:)));
        % ---- Difference in nanosec part of GPS times.
        deltat(:,1) = deltat(:,1) + 1e-9*(nanosec(:,2)-nanosec(:,1));
        deltat(:,2) = deltat(:,2) + 1e-9*(nanosec(:,3)-nanosec(:,1));
        deltat(:,3) = deltat(:,3) + 1e-9*(nanosec(:,3)-nanosec(:,2));
        % ---- Compute quadrature-sum expected uncertainty.
        sigmat = ([ ...
            sum(dtime([1,2]).^2+(dphase([1,2])/(2*pi*f0)).^2) , ...
            sum(dtime([1,3]).^2+(dphase([1,3])/(2*pi*f0)).^2) , ...
            sum(dtime([2,3]).^2+(dphase([2,3])/(2*pi*f0)).^2) ]).^0.5;
        sigmat = repmat(sigmat,nInjection,1); 
        deltat = normcdf(deltat,0,sigmat); 
        ndeltat = hist(deltat,deltatbin);
        ndeltat_all = ndeltat_all + ndeltat;
    end
    % ndeltat_all = hist(deltat/1e-3,deltatbin);

    % ---- vi.   polarization angle on [0,pi].
    for idet = 1:nDetector
        psi(:,idet) = str2double(currentParameters{(idet-1)*7+5}(:));
    end
    npsi = hist(psi,psibin);
    npsi_all = npsi_all + npsi;
    if igrb==desirednetwork(1)
        psi_old = psi;
    elseif isequal(psi_old,psi)
        fprintf(fid,'%s\n',['Psi fields identical to first GRB.']);
    else
        fprintf(fid,'%s\n',['Psi fields differ from first GRB.']);
    end

    % ---- Sky positions of injections.
    tinj = str2double(currentParameters{1}(:))+1e-9*str2double(currentParameters{2}(:));           
    phi = str2double(currentParameters{3}(:));
    theta = str2double(currentParameters{4}(:));
    Omega = CartesianPointingVector(phi,theta);
    % ---- Sky position and unit vector of GRB error box center at each 
    %      injection time (note that GRB tracks across the sky over on-
    %      source window.
    [phi_grb, theta_grb] = radectoearth(ra(igrb)*ones(nInjection,1),dec(igrb)*ones(nInjection,1),tinj);
    Omega_grb = CartesianPointingVector(phi_grb,theta_grb);
    % ---- Opening angle between injections and GRB central position.
    coslambda = sum(Omega .* Omega_grb,2);
    lambda = acos(coslambda);
    
    % ---- iv.   opening angle between injection position and error box center, 
    %            compared with error box probability distribution.
    % ---- Get injection distribution parameters.
    out = tildedelimstr2numorcell(injdistn{igrb});
    % ---- Description of the "--injdistrib" option in grb.py.
    %          Tilde delimited string of parameters describing 
    %          the injection distribution in the first circle [OPTIONAL].
    %          Formats are:
    %           1 parameter: 1-sigma containment of a fisher distribution
    %           3 parameters: lognormal distribution in degrees
    %           4 parameters: fisher distribution of statistical error and 
    %                core + tail fisher distribution of systematic error.
    %                [stat_sigma sys_core_sigma fraction_core sys_tail_sigma]
    %                all sigma are in degrees
    if length(out)==1
        % ---- Fisher distribution. Fisher distribution used in utilities/fisher.m:
        %                       k    
        %          P(x) = ---------------  sin(x) exp( k cos(x) )
        %                 exp(k) - exp(-k)
        %                           
        %               ~ k sin(x) exp( k [cos(x) - 1] )    for k >> 1
        %      Here k := 1/(0.66 * sigma)^2 with sigma^2 the variance in rad^2.
        kappa = 1/(0.66 * out*pi/180)^2;
        if out<=36 
            x = [0.005:0.010:5]' * out*pi/180;
            pdf = sin(x) .* exp( kappa*(cos(x)-1) );
        else 
            x = [0.005:0.010:pi]';
            pdf = sin(x) .* exp( kappa*(cos(x)-1) ) / (1 - exp(-2*kappa));
        end 
        cdf = cumsum(pdf)/sum(pdf);
        % icdf = robustinterp1(x,cdf,lambda); 
        icdf = interp1(x,cdf,lambda); 
        nrad = hist(icdf,radbin);
        nrad_all = nrad_all + nrad(:);
    elseif length(out)==4
        % ---- Two-component Fisher distribution.  Combined probability 
        %      distribution is the sum of separate distributions for core
        %      and tail, each using quadrature sum of statistical and 
        %      systematic errors.  See ASTROPHYSICAL JOURNAL SUPPLEMENT 
        %      SERIES, 122:503-518, 1999.
        sigma_stat = out(1)*pi/180;
        sigma_core = out(2)*pi/180;
        sigma_tail = out(4)*pi/180;
        alpha = out(3);  %-- fraction of prob in core
        % ---- Core distribution.
        k_core = 1/(0.66 * (sigma_core^2+sigma_stat^2)^0.5)^2;
        k_tail = 1/(0.66 * (sigma_tail^2+sigma_stat^2)^0.5)^2;
        % ----
        delta = 0.001;
        x = [delta/2:delta:1]' * pi;
        dx = x(2)-x(1);
        p_core =  k_core * sin(x) .* exp( k_core *(cos(x) - 1) );
        p_core = p_core / (sum(p_core)*dx);  %-- force correct normalization
        p_tail =  k_tail * sin(x) .* exp( k_tail *(cos(x) - 1) );
        p_tail = p_tail / (sum(p_tail)*dx);  %-- force correct normalization
        % ---- Combined distribution.
        pdf = alpha*p_core + (1-alpha)*p_tail;
        pdf = pdf / sum(pdf)*dx; %-- force correct normalization
        cdf = cumsum(pdf)/sum(pdf);
        % icdf = robustinterp1(x,cdf,lambda); 
        icdf = interp1(x,cdf,lambda); 
        icdf_all = [icdf_all, icdf];
        nrad = hist(icdf,radbin);
        nrad_all = nrad_all + nrad(:);
    else
        error(['Script not able to handle injection distributions of length ' num2str(length(out)) '.']);
    end
    % ---- Check for identical values between GRBs.
    if igrb==desirednetwork(1)
        icdf_old = icdf;
    elseif isequal(icdf_old,icdf)
        fprintf(fid,'%s\n',['Radial cdf offsets identical to first GRB.']);
    else
        fprintf(fid,'%s\n',['Radial cdf offsets differ from first GRB.']);
    end

    % ---- v.    angle between between arc from error box center to injection 
    %            position, and local north direction, on [0,pi].
    % ---- North pole Cartesian vector.
    Omega_np = [0,0,1];
    % ---- Angles between positions subtended from COE.
    a = lambda;                       %-- grb-inj
    b = acos(Omega_grb * Omega_np');  %-- grb-np
    c = acos(Omega * Omega_np');      %-- inj-np
    azi = acos( (cos(c) - cos(a).*cos(b)) ./ (sin(a).*sin(b)) ); 
    dphi = phi-phi_grb;
    ind = find( (dphi>=-pi & dphi<0) | (dphi>=pi & dphi<2*pi) );
    azi(ind) = 2*pi-azi(ind);
    nazi = hist(azi,azibin);
    nazi_all = nazi_all + nazi(:);
    % ---- Check for identical values between GRBs.
    if igrb==desirednetwork(1) 
        azi_old = azi;
    else
        if isequal(azi_old,azi)
            fprintf(fid,'%s\n',['Azimuthal offsets identical to first GRB.']);
        else
            fprintf(fid,'%s\n',['Azimuthal offsets differ from first GRB.']);
        end
    end
    % ---- Check for correlated values between GRBs.
    X = icdf .* cos(azi);
    Y = icdf .* sin(azi);
    if length(out)==1 & isempty(X_old_1)
        fprintf(fid,'%s\n','First small-error-box GRB.');
        X_old_1 = icdf .* cos(azi);
        Y_old_1 = icdf .* sin(azi);
    elseif length(out)==4 & isempty(X_old_4)
        fprintf(fid,'%s\n','First large-error-box GRB.');
        X_old_4 = icdf .* cos(azi);
        Y_old_4 = icdf .* sin(azi);
    else
        if length(out)==1
            X_old = X_old_1;
            Y_old = Y_old_1;
        elseif length(out)==4
            X_old = X_old_4;
            Y_old = Y_old_4;
        end
        if dot(X/norm(X),X_old/norm(X_old)) > 10*nInjection^(-0.5)
            fprintf(fid,'%s\n',['Sky X offsets highly correlated with first GRB of this type (' num2str(dot(X/norm(X),X_old/norm(X_old))) ').']);
        else 
            fprintf(fid,'%s\n',['Sky X offsets not correlated with first GRB of this type (' num2str(dot(X/norm(X),X_old/norm(X_old))) ').']);
        end
        if dot(Y/norm(Y),Y_old/norm(Y_old)) > 10*nInjection^(-0.5)
            fprintf(fid,'%s\n',['Sky Y offsets highly correlated with first GRB of this type (' num2str(dot(Y/norm(Y),Y_old/norm(Y_old))) ').']);
        else
            fprintf(fid,'%s\n',['Sky Y offsets not correlated with first GRB of this type (' num2str(dot(Y/norm(Y),Y_old/norm(Y_old))) ').']);
        end
    end

    % ---- vii.  verify waveform type.
    waveformCorrect = 1;
    for idet = 1:nDetector
        if any(strcmp(currentParameters{(idet-1)*7+6},waveform)==0)
            waveformCorrect = 0;
        end
    end
    if waveformCorrect 
        fprintf(fid,'%s\n','Waveform model correct.');
    else 
        fprintf(fid,'%s\n','WARNING: WRONG WAVEFORM MODEL!');
    end


    % ---- ix.   verify other waveform params.
    % auxParamsCorrect = 1;
    for idet = 1:nDetector
        for iinj = 1:nInjection
            params = tildedelimstr2numorcell(currentParameters{(idet-1)*7+7}{iinj});  
            mass1param(iinj,1) = params(1);
            mass2param(iinj,1) = params(2);
            inclparam(iinj,1)  = params(3);
            amplparam(iinj,1)  = params(4);
            % otherparams = params(2:end-1);
            % % ---- Check auxiliary parameters.
            % if ~isequal(otherparams,auxparamsmat)
            %     auxParamsCorrect = 0;
            % end
        end
        % ---- Record raw values for all detectors for comparison between GRBs.
        rawmass1(:,idet) = mass1param;
        rawmass2(:,idet) = mass2param;
        rawampl(:,idet) = amplparam;
        rawincl(:,idet) = inclparam;
        % ---- xii.   verify mass distributions. 
        % ---- Look up 1-sigma mass uncertainties, mass limits.
        switch name 
            case 'BNS'
                minmass1  = 0.9; 
                meanmass1 = 1.4; 
                maxmass1  = 3.0; 
                dmass1    = 0.2; 
                minmass2  = 0.9; 
                meanmass2 = 1.4; 
                maxmass2  = 3.0; 
                dmass2    = 0.2; 
                meanampl  = 10; 
            case 'NSBH'
                minmass1  = 0.9; 
                meanmass1 = 1.4; 
                maxmass1  = 3.0; 
                dmass1    = 0.4; 
                minmass2  = 2.0; 
                meanmass2 = 10; 
                maxmass2  = 25; 
                dmass2    = 6.0; 
                meanampl  = 20; 
        end
        % ---- Check mass distributions.  Rescale to unit mean, then 
        %      convert to CDF for Gaussian.
        lgap = normcdf(minmass1,meanmass1,dmass1);
        rgap = normcdf(maxmass1,meanmass1,dmass1);
        mass1param = normcdf(mass1param,meanmass1,dmass1);
        mass1param = (mass1param - lgap)/(rgap - lgap);
        nmass1 = histc(mass1param,massbin);
        nmass1_all(:,idet) = nmass1_all(:,idet) + nmass1;
        lgap = normcdf(minmass2,meanmass2,dmass2);
        rgap = normcdf(maxmass2,meanmass2,dmass2);
        mass2param = normcdf(mass2param,meanmass2,dmass2);
        mass2param = (mass2param - lgap)/(rgap - lgap);
        nmass2 = histc(mass2param,massbin);
        nmass2_all(:,idet) = nmass2_all(:,idet) + nmass2;
        % ---- viii. compare amplitude or distance parameter distribution (all 
        %            detectors) to amplitude calibration uncertainty for that 
        %            detector.
        % ---- Look up 1-sigma calibration amplitude uncertainty.
        switch detector{idet}
            case {'H1','L1'}
                cal = 0.2; 
            case {'V1'}
                if gps(igrb) < tStartVSR3
                    cal = 0.055;  %-- HrecV3, for VSR2
                else
                    cal = 0.07;   %-- HrecV3, for VSR3
                end
        end
        % ---- Check amplitude distribution.  Rescale to unit mean, then 
        %      convert to CDF for Gaussian.
        amplparam = amplparam/meanampl;
        amplparam = normcdf(amplparam,1,cal); 
        nampl = hist(amplparam,amplbin).';
        nampl_all(:,idet) = nampl_all(:,idet) + nampl;
        % ---- xi.   verify inclination angle distribution. 
        nincl = histc(cos(inclparam),inclbin);
        nincl_all(:,idet) = nincl_all(:,idet) + nincl;
    end
    % % ---- Report on auxiliary parameters.
    % if auxParamsCorrect
    %     fprintf(fid,'%s\n','Auxiliary parameters correct.');
    % else
    %     fprintf(fid,'%s\n','WARNING: WRONG AUXILIARY PARAMETERS!');
    % end
    % ---- Check for identical values between GRBs.
    if igrb==desirednetwork(1)
        rawmass1_old = rawmass1;
        rawmass2_old = rawmass2;
        rawampl_old = rawampl;
        rawincl_old = rawincl;
    elseif (~isempty(strfind(targetnetwork,'V1'))) && (gps(igrb) > tStartVSR3) && processedVSR3GRB==0
        % ---- Reset reference amplitudes to this GRB (uses VSR3 calibration uncertainty for V1).
        rawampl_old = rawampl;
        rawincl_old = rawincl;
        processedVSR3GRB = 1;
        fprintf(fid,'%s\n','First VSR3 GRB; resettting default amplitudes.');
        if isequal(rawincl_old,rawincl)
            fprintf(fid,'%s\n',['Inclinations identical to first GRB.']);
        else
            fprintf(fid,'%s\n',['Inclinations differ from first GRB.']);
        end
    else
        if isequal(rawampl_old,rawampl)
            fprintf(fid,'%s\n',['Amplitudes identical to first GRB.']);
        else
            fprintf(fid,'%s\n',['Amplitudes differ from first GRB.']);
        end
        if isequal(rawincl_old,rawincl)
            fprintf(fid,'%s\n',['Inclinations identical to first GRB.']);
        else
            fprintf(fid,'%s\n',['Inclinations differ from first GRB.']);
        end
        if isequal(rawmass1_old,rawmass1)
            fprintf(fid,'%s\n',['Mass 1 values identical to first GRB.']);
        else
            fprintf(fid,'%s\n',['Mass 1 values differ from first GRB.']);
        end
        if isequal(rawmass2_old,rawmass2)
            fprintf(fid,'%s\n',['Mass 2 values identical to first GRB.']);
        else
            fprintf(fid,'%s\n',['Mass 2 values differ from first GRB.']);
        end
    end

end

% % ---- It appears that each injection file used the same random number 
% %      seed, so that initially all psi angles are repeated from one 
% %      file to the next.  Since they're not jittered, they should be 
% %      identical, leading to a somple pattern.  Check for that:
% if max(max(mod(npsi_all,length(desirednetwork))))==0
%     fprintf(fid,'%s\n','Psi values repeated between injection files.');
% else
%     fprintf(fid,'%s\n','Psi values NOT repeated between injection files.');
% end

% ---- Save results.
save([name '_' targetnetwork '_' grbset '.mat']);

% ---- Close log file.
fprintf(fid,'%s\n',['---------------------------------------------------']);
fclose(fid);


