% ---- Matlab script to test injection logs for optically triggered supernova 
%      search.
%
%      $Id:$

% Input:
%  1) SN on-source window
%  2) SN sky position
% Checks:
%  1) gps_s distribution uniform over full on-source window
%  2) gps_ns uniform over [0,1] sec.
%  3) Angular offset from SN current position consistent with 0.
%  4) Polarisation angle uniform on [0,pi] or [0,2pi].
%  5) Waveform parameters (format: hrssp~hrssc~type): 
%     a) type recognised
%     b) uniform (discrete) distribution on type
%     c) hrssp, hrssc imply inclination iota that is uniform on [0,pi]
%        (known error).
%     d) hrssp, hrssc consistent with linear or elliptical polarisation
%        (except for mueller): output some interpretation of those
%        amplitudes  
%
%  Note that we have already checked the mechanics of generating h(t) by
%  comparing to X-Pipeline injections, so this script just needs to check
%  the parameter distributions. We use the BurstMDC logs as they hold more 
%  explicitly computed quantities (particularly the inclination) than the 
%  X-Pipeline logs.

% -----------------------------------------------------------------------------
%    User-specifiable parameters.
% -----------------------------------------------------------------------------

% ---- Base directories for matlab packages.
matappsDir   = '/Users/psutton/Documents/matapps/';
xpipelineDir = '/Users/psutton/Documents/xpipeline/branches/linear/';
injDir       = '/Users/psutton/Documents/xpipeline/branches/linear/test/';

% ---- Select supernova to process.
% SN = 'SN2007gr';
% SN = 'SN2011dh';

% ---- Plot options.
savePlots = true;
lineWidth = 2;

% -----------------------------------------------------------------------------
%    SN info.
% -----------------------------------------------------------------------------

% ---- MDC sets and trigger data for each SN.
switch SN
    case 'SN2007gr'
        ra  = 40.8666;
        dec = 37.3458;
        onsource = [870773630 871215278];
        mdc = {'mueller5', 'numerical3', 'piro5', 'rotbar5', 'sgel2', 'sglin2'};
        % mdc = {'mueller5'};
    case 'SN2008ax'
        ra  = 187.67;
        dec = 41.6374;
        onsource = [888467630 888576494];
        mdc = {'mueller_long', 'numerical_long', 'piro_long', 'rotbar_long', 'sgel_long', 'sglin_long'};
    case 'SN2008bk'
        ra  = 359.4601;
        dec = -32.5560;
        onsource = [889444814 890450510];
        mdc = {'mueller_long', 'numerical_long', 'piro_long', 'rotbar_long', 'sgel_long', 'sglin_long'};
    case 'SN2011dh'
        ra  = 202.52135;
        dec = 47.169806;
        onsource = [990780783 990912111];
        mdc = {'mueller_long', 'numerical_long', 'piro_long', 'rotbar_long', 'sgel_long', 'sglin_long'};
        % mdc = {'piro_long'};
end

% ---- Record distinct waveforms within each MDC set. shortTypes is an
%      arbitrary human-readable label used for plots and output file names. 
types = {};
shortTypes = {};
polarisation = {};
for imdc = 1:length(mdc)
    switch lower(mdc{imdc}(1:4))        
        case 'muel'
            types{end+1} = {...
                'Waveforms/L15-3', ...
                'Waveforms/N20-2', ...
                'Waveforms/W15-4'};
            shortTypes{end+1} = {'L15-3','N20-2','W15-4'};
            polarisation{end+1} = 'unpolarised';
        case 'nume'
            types{end+1} = { ...
                'Waveforms/processed_signal_s15a2o05_ls-plus.txt', ...
                'Waveforms/processed_signal_s15a2o09_ls-plus.txt', ...
                'Waveforms/processed_signal_s15a3o15_ls-plus.txt', ...
                'Waveforms/processed_s15-time-rhplus_matter-plus.txt', ...
                'Waveforms/processed_s15.0.h-plus.txt'};
            shortTypes{end+1} = {'s15a2o05','s15a2o09','s15a3o15','s15-Yakunin','S15-Ott'};
            polarisation{end+1} = 'linear';
        case 'piro'
            types{end+1} = {...
                'Waveforms/piroM10.0eta0.3fac0.2-plus.txt;Waveforms/piroM10.0eta0.3fac0.2-cross.txt', ...
                'Waveforms/piroM10.0eta0.6fac0.2-plus.txt;Waveforms/piroM10.0eta0.6fac0.2-cross.txt', ...
                'Waveforms/piroM5.0eta0.3fac0.2-plus.txt;Waveforms/piroM5.0eta0.3fac0.2-cross.txt', ...
                'Waveforms/piroM5.0eta0.6fac0.2-plus.txt;Waveforms/piroM5.0eta0.6fac0.2-cross.txt'};
            shortTypes{end+1} = {'M10.0eta0.3fac0.2','M10.0eta0.6fac0.2','M5.0eta0.3fac0.2','M5.0eta0.6fac0.2'};        
            polarisation{end+1} = 'elliptical';
        case 'rotb'
            types{end+1} = {...
                'Waveforms/outM0p2L60R10f400t100-plus.txt;Waveforms/outM0p2L60R10f400t100-cross.txt', ...
                'Waveforms/outM0p2L60R10f400t1000-plus.txt;Waveforms/outM0p2L60R10f400t1000-cross.txt', ...
                'Waveforms/outM0p2L60R10f800t100-plus.txt;Waveforms/outM0p2L60R10f800t100-cross.txt', ...
                'Waveforms/outM1p0L60R10f400t100-plus.txt;Waveforms/outM1p0L60R10f400t100-cross.txt', ...
                'Waveforms/outM1p0L60R10f400t1000-plus.txt;Waveforms/outM1p0L60R10f400t1000-cross.txt', ...
                'Waveforms/outM1p0L60R10f800t25-plus.txt;Waveforms/outM1p0L60R10f800t25-cross.txt'};
            shortTypes{end+1} = {'M0p2L60R10f400t100','M0p2L60R10f400t1000','M0p2L60R10f800t100',...
                'M1p0L60R10f400t100','M1p0L60R10f400t1000','M1p0L60R10f800t25'};
            polarisation{end+1} = 'elliptical';
        case 'sgel'
            types{end+1} = {...
                'Waveforms/SG1304Q8d9.txt;Waveforms/CG1304Q8d9.txt', ...
                'Waveforms/SG235Q8d9.txt;Waveforms/CG235Q8d9.txt'};
            shortTypes{end+1} = {'1304Q8d9','235Q8d9'};
            polarisation{end+1} = 'elliptical';
        case 'sgli'
            types{end+1} = {...
                'Waveforms/SG1304Q8d9.txt', ...
                'Waveforms/SG235Q8d9.txt'};
            shortTypes{end+1} = {'1304Q8d9','235Q8d9'};
            polarisation{end+1} = 'linear';
    end
    
end

% -----------------------------------------------------------------------------
%    Setup.
% -----------------------------------------------------------------------------

% ---- Set matlab path.
addpath([matappsDir 'packages/simulation/BurstMDC/trunk']);
addpath([matappsDir 'releases/utilities/misc/src/']);
addpath([xpipelineDir 'searches/grb']);
addpath([xpipelineDir 'searches/sn']);
addpath([xpipelineDir 'utilities']);
addpath([xpipelineDir 'share']);

% -----------------------------------------------------------------------------
%    Loop over injection log files.
% -----------------------------------------------------------------------------

for imdc = 1:length(mdc)

    disp('--------------------------------------------------------------');
    disp([SN ' - ' mdc{imdc}]);
    disp('--------------------------------------------------------------');
    
    % ---- Open and parse BurstMDC formatted log file. Put into same format
    %      (variable names) as expected from X-Pipeline log file.
    injFile = [injDir '/' 'BurstMDC-' SN '_' mdc{imdc} '-Log.txt'];
    [nInjection,mdcField,mdcData] = rdburstmdclog(injFile);
    gps_s  = floor(mdcData.EarthCtrGPS);
    gps_ns = round(1e9*(mdcData.EarthCtrGPS-gps_s));
    phi    = mdcData.External_phi;
    theta  = acos(mdcData.External_x);
    psi    = mdcData.External_psi;
    % ---- Extract injection data.
    hrssp = mdcData.SimHpHp.^0.5;
    hrssc = mdcData.SimHcHc.^0.5;
    % hrsspc = mdcData.SimHpHc;
    name  = mdcData.GravEn_SimID;
    ciota  = mdcData.Internal_x;
    iota   = acos(ciota);

    % --------------------------------------------------------------------
    %    Run tests on injection parameters.
    % --------------------------------------------------------------------

    % ---- i. Verify than all gps lie inside on-source window.
    if any(gps_s<onsource(1)) | any(gps_s>=onsource(2))
        warning('gps_s field extends beyond on-source region.');
    else
        disp('gps values valid.');
    end

    % ---- ii. Histogram of gps field of injection time.
    binwidth = 10000;
    gps_sbin  = [min(onsource(1),min(gps_s)):binwidth:max(onsource(2),max(gps_s)),Inf];
    ngps_s = histc(gps_s,gps_sbin);
    % ---- histc function and stairs plot function mangle the last bin,
    %      making the plot look strange.  Overwrite meaningless last bin 
    %      with second-last bin to make a sensible plot.
    gps_sbin(end)  = gps_sbin(end-1)+binwidth;
    ngps_s(end) = ngps_s(end-1);
    % ---- Expected mean value.
    mu = binwidth/100;
    % ---- Number expected in last partial bin.
    mu_last = floor((onsource(2)-gps_sbin(end-1))*100/binwidth);
    plotname = 'gps_s';
    figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
    set(gcf,'position',[ 1600 0 5*256 768])
    subplot(2,3,1)
    stairs(gps_sbin,ngps_s,'linewidth',lineWidth);
    grid on; hold on
    plot(onsource',[mu;mu],'g-','linewidth',lineWidth);
    axis([get(gca,'xlim') 0 110]);
    ylim = get(gca,'ylim');
    plot([onsource(1);onsource(1)],ylim(:),'g--','linewidth',lineWidth);
    plot([onsource(2);onsource(2)],ylim(:),'g--','linewidth',lineWidth);
    % ---- Make sure data is on top.
    stairs(gps_sbin,ngps_s,'linewidth',lineWidth);
    plot(gps_sbin(end-1)+binwidth/2,mu_last,'ro');
    title('gps-s injection time');
    xlabel('gps-s (s)');
    ylabel('number of injections');
    % set(gca,'ylim',[ylim(1)-5, ylim(2)]);
    
    % ---- iii. Verify than min(gps_ns)>=0, max(gps_ns)<=999999999.
    if any(gps_ns<0) | any(gps_ns>999999999)
        warning('Invalid gps_ns field.');
    else
        disp('gps_ns values valid.');
    end

    % ---- iv. Histogram of gps_ns field of injection time
    gps_nsbin = [0:10:1000]' * 1e6;
    ngps_ns = histc(gps_ns,gps_nsbin);
    % ---- Again, overwrite last bin to make plots work.
    gps_nsbin(end)  = gps_nsbin(end-1)+10*1e6;
    ngps_ns(end) = ngps_ns(end-1);
    % ---- Expected mean value and st.dev.    
    mu = nInjection/(length(gps_nsbin)-1);
    sig = mu.^0.5;
    plotname = 'gps_ns';
    % figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
    subplot(2,3,2)
    stairs(gps_nsbin,ngps_ns,'linewidth',lineWidth);
    grid on; hold on
    plot([0;1e9],[mu;mu],'g-','linewidth',lineWidth);
    plot([0;1e9],[mu-sig,mu+sig;mu-sig,mu+sig],'g--','linewidth',lineWidth);
    % ---- Make sure data is on top.
    stairs(gps_nsbin,ngps_ns,'linewidth',lineWidth);
    title('gps-ns injection time');
    xlabel('gps-ns (ns)');
    ylabel('number of injections');

    % ---- v. Verify that sky positions of injections coincides with SN position.
    gps = gps_s+gps_ns*1e-9;
    [ra_inj, dec_inj] = earthtoradec(phi,theta,gps);
    %[nsky, cent] = hist3([ra_inj-ra,dec_inj-dec]);
    ra_err = max(abs(ra_inj-ra));
    dec_err = max(abs(dec_inj-dec));
    disp(['Max error in ra:  ' num2str(ra_err) ' deg']);
    disp(['Max error in dec: ' num2str(dec_err) ' deg']);
    
    % ---- vi. Verify range of polarization angle is sensible.
    if any(psi<0) | any(psi>2*pi)
        warning('psi field extends beyond [0,2*pi].');
    elseif any(psi<0) | any(psi>pi)
        warning('psi field extends beyond [0,pi].');
    end

    % ---- vii. Verify that polarization angle is uniform on [0,2*pi].
    psibin = [0:0.05:1]*2*pi;
    npsi = histc(psi,psibin);
    % ---- Again, overwrite last bin to make plots work.
    psibin(end) = psibin(end-1)+0.05*2*pi;
    npsi(end) = npsi(end-1);
    % ---- Expected mean value and st.dev.    
    mu = nInjection/(length(psibin)-1);
    sig = mu.^0.5;
    plotname = 'psi';
    % figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
    subplot(2,3,3)
    plot([0;2*pi],[mu;mu],'g-','linewidth',lineWidth);
    grid on; hold on
    plot([0;2*pi],[mu-sig,mu+sig;mu-sig,mu+sig],'g--','linewidth',lineWidth);
    stairs(psibin,npsi,'linewidth',lineWidth);
    title('injection polarisation');
    xlabel('injection polarisation psi (rad)');
    ylabel('number of injections');

    % ---- Loop over injections and extract amplitude and waveform
    %      information.
    itype = zeros(nInjection,1);
    for iInj = 1:nInjection
        % ---- Assign a numerical label (1,2,...) to each injection by
        %      waveform type. 
        if strcmpi(mdc{imdc}(1:4),'muel')
            tmp_type = find(strcmp(types{imdc},name{iInj}(1:15)));
        else
            tmp_type = find(strcmp(types{imdc},name{iInj}));
        end
        if ~isempty(tmp_type)
            itype(iInj) = tmp_type;
        end        
    end

    % ---- viii. Verify each waveform matches known type and count
    %      occurences of each type. 
    disp('Validating waveform types.');
    if any(itype==0)
        idx = find(itype==0);
        for ii = idx(:)'
            warning(['Unrecognised waveform type: ' name{ii}]);
        end
    end
    namecount = zeros(1,length(types{imdc}));
    for jj = 1:length(types{imdc})
        namecount(jj) = sum(itype==jj);
    end
    if sum(namecount) ~= nInjection
        warning(['Mismatch between total number of injections and number ' ...
            'of identified injections.']);
    end
    % ---- Expected mean value and st.dev.    
    mu = nInjection/length(types{imdc});
    sig = mu.^0.5;
    plotname = 'name';
    % figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
    subplot(2,3,4)
    plot([1,(length(types{imdc})+1)]-0.5,[mu;mu],'g-','linewidth',lineWidth);
    grid on; hold on
    plot([1,(length(types{imdc})+1)]-0.5,[mu-sig,mu+sig;mu-sig,mu+sig],'g--','linewidth',lineWidth);
    stairs([1:length(types{imdc})+1]-0.5,[namecount,namecount(end)],'linewidth',lineWidth);
    title('waveform types');
    xlabel('injection waveform type');
    ylabel('number of injections');
    subplot(2,3,4)
    set(gca,'xtick',[1:length(types{imdc})])
    
    % ---- Infer inclination angle.
    switch polarisation{imdc}
        
        case 'elliptical'
            
            if any(hrssp<0.999*hrssc)  % 0.999 - allowance for rounding errors.
                warning('Amplitudes inconsistent with elliptical polarisation (hp<hc).');
            end
            
            % ---- Verify amplitude correct after accounting for inclination.
            switch lower(mdc{imdc}(1:4))
                case 'sgel'
                    % ---- Verify total hrss amplitude fixed to 1e-21
                    %      INCLUDING inclination effect. 
                    %      (This is unique to the sgel sets.)
                    clear optimalAmpl
                    for jj=1:length(types{imdc})
                        hrssTot = ((hrssp(itype==jj)).^2+(hrssc(itype==jj)).^2).^0.5;
                        optimalAmpl(jj) = median( hrssTot );
                        if (std(hrssTot./optimalAmpl(jj)) > 1e-4)
                            warning(['Waveform ' num2str(jj) ' amplitudes not fixed as expected.']);
                        end
                    end
                otherwise
                    % ---- Verify amplitude fixed after correcting for inclination.
                    %      (This is standard for MDC production.)
                    amplFactorP = 0.5*(1+ciota.^2);
                    amplFactorC = abs(ciota);
                    correctedAmplP = hrssp ./ amplFactorP;
                    correctedAmplC = abs(hrssc) ./ amplFactorC;
                    clear optimalAmplP optimalAmplC variationP variationC
                    for jj=1:length(types{imdc})
                        optimalAmplP(jj) = median(correctedAmplP(itype==jj));
                        variationP(jj) = std(correctedAmplP(itype==jj))/optimalAmplP(jj);
                        optimalAmplC(jj) = median(correctedAmplC(itype==jj));
                        variationC(jj) = std(correctedAmplC(itype==jj))/optimalAmplC(jj);
                        if variationP(jj) > 1e-2
                            disp(['Waveform type: ' num2str(jj) ]);
                            warning(['Waveform plus amplitudes do not vary with inclination as expected (var: ' num2str(variationP(jj)) ').']);
                        end
                        if variationC(jj) > 1e-2
                            disp(['Waveform type: ' num2str(jj) ]);
                            warning(['Waveform cross amplitudes do not vary with inclination as expected (var: ' num2str(variationC(jj)) ').']);
                        end
                    end
                    if (std(optimalAmplP./optimalAmplC) > 0.01)
                        warning('Waveform plus and cross amplitudes differ.');
                    end
                    disp(['Injection scale 1 amplitudes (+/x): ']);
                    for jj=1:length(types{imdc})
                        disp(['  waveform ' num2str(jj) ': ' num2str(optimalAmplP(jj)) ' / ' num2str(optimalAmplC(jj))]);
                    end
            end

        case 'linear'

            if max(abs(hrssc))>0
                warning('Amplitudes inconsistent with linear polarisation.');
            end
            
            switch lower(mdc{imdc}(1:4))
                case 'sgli'
                    % ---- sglin injections should have fixed amplitude of
                    %      1e-21, with an effective inclination of pi/2.
                    clear optimalAmplP 
                    for jj=1:length(types{imdc})
                        optimalAmplP(jj) = median(hrssp(itype==jj));
                        if (std(hrssp(itype==jj)./optimalAmplP(jj)) > 1e-4)
                            warning(['Waveform ' num2str(jj) ' amplitudes not fixed as expected.']);
                        end
                    end
                otherwise
                    clear optimalAmplP 
                    for jj=1:length(types{imdc})
                        % ---- Estimate optimal hrss.
                        optimalAmplP(jj) = median(abs(hrssp(itype==jj)./(sin(iota(itype==jj))).^2));
                    end
            end
            disp(['Injection scale 1 amplitudes (+): ']);
            for jj=1:length(types{imdc})
                disp(['  waveform ' num2str(jj) ': ' num2str(optimalAmplP(jj)) ]);
            end
            
        otherwise
            
            % ---- Inclination not used directly for mueller waveforms.
            %      Instead it is already included in the waveform, and
            %      recorded in the waveform name. Loop over waveform name
            %      and extract inclinations.
            iota = -ones(nInjection,1);
            for iInj = 1:nInjection
                idx = strfind(name{1},'theta');
                iota(iInj) = str2num(name{iInj}((idx(1)+5):(idx(1)+9)));
            end

    end
 
    % ---- xi. Verify that inclination angle is uniform on [0,pi]. (This
    %      is not the correct distribution, but it is how the MDCs were made.)
    iotabin = [0:0.05:1]*pi;
    if strcmp(polarisation{imdc},'unpolarised')
        iotabin = [0:0.01:1.05]*pi;
    end
    niota = histc(iota,iotabin);
    % ---- Massage last bin again.  In this case the procedure to derive
    %      ciota results in some being railed at exactly -1. These fall into 
    %      the last zero-width bin. Make sure these are plotted properly by
    %      combining with the previous bin. (The same happens at ciota=+1,
    %      but these are automatically included in the first bin.)
    tmp = niota(end-1) + niota(end);
    niota(end-1) = tmp;
    niota(end)   = tmp;
    % ---- Expected mean value and st.dev.    
    mu = nInjection/(length(iotabin)-1);
    if strcmp(polarisation{imdc},'unpolarised')
        mu = nInjection/10;
    end
    sig = mu.^0.5;
    plotname = 'iota';
    % figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
    subplot(2,3,5)
    plot([0;pi],[mu;mu],'g-','linewidth',lineWidth);
    grid on; hold on
    plot([0;pi],[mu-sig,mu+sig;mu-sig,mu+sig],'g--','linewidth',lineWidth);
    stairs(iotabin,niota,'linewidth',lineWidth);
    title('injection inclination');
    xlabel('injection inclination (rad)');
    ylabel('number of injections');

    if savePlots
        saveas(gcf,[SN '_' mdc{imdc} '.fig'],'fig');
        % saveas(gcf,[SN '_' mdc{imdc} '.png'],'png');
        eval(['export_fig ' SN '_' mdc{imdc} '.pdf']);
        [status,result] = system(['convert ' SN '_' mdc{imdc} '.pdf ' SN '_' mdc{imdc} '.png']);
    end

end
