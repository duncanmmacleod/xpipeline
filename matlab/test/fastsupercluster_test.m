% unit test that check that fastsupercluster and
% fastquadraticsupercluster do exactly the same thing


% create inputs: a 5 column matrix with [x y dx dy significance]
nRectangles = 1e6;
rectangles = [248*rand(nRectangles,1) 50*rand(nRectangles,1) ...
              rand(nRectangles,1) 10*rand(nRectangles,1) ...
              rand(nRectangles,1)];

% find masks for each algorithms
mask = fastsupercluster(rectangles);
maskQuadratic = fastquadraticsupercluster(rectangles);

disp(['Number of differences: ' ...
      num2str(sum(abs(mask-maskQuadratic)))]);
disp(['Kept/Total triggers: ' ...
      num2str(sum(mask)) '/' num2str(nRectangles)]);

if sum(abs(mask-maskQuadratic)) > 0
  error('Failed superclustering test')
end
