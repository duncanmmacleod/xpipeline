#!/usr/bin/env python

"""
Script for setting up X-Pipeline all-sky search jobs.
Adapted from sn.py.
$Id$
"""

# -------------------------------------------------------------------------
#      Setup.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, shutil, math, random, copy, getopt, re, string, time
import configparser, glob, operator
from glue import segments
from glue import segmentsUtils
from glue import pipeline
from glue.lal import CacheEntry
#from pylal.date import LIGOTimeGPS

__author__ = "Patrick Sutton <psutton@ligo.caltech.edu>, Michal Was <michal.was@ens.fr>"
__date__ = "$Date: 2014-04-11 04:13:41 -0700 (Fri, 11 Apr 2014) $"
__version__ = "$Revision: 4204 $"


# -------------------------------------------------------------------------

# ---- Function usage.
def usage():
    msg = """\
Usage:
  allsky.py [options]
  -p, --params-file <file>    Parameters (.ini) file [REQUIRED]
  -n, --grb-name <name>       Name of analysis, eg. S6A-H1L1V1 [REQUIRED]
  -g, --grb-time <gps>        Nominal central time of analysis window (GPS seconds) [REQUIRED]
  -r, --right-ascension <ra>  Right ascension of SN (degrees, from 0 to 360)
                              [OBSOLETE - TO BE DELETED]
  -d, --declination <decl>    Declination of SN (degrees, from -90 to 90)
                              [OBSOLETE - TO BE DELETED]
  -i, --detector <ifo>        Add detector to the network [REQUIRED]
  --priority <prio>           Intenger specifying the priority of the job.
                              Default value is 0, higher priority jobs are
                              submitted first to the cluster. [OPTIONAL]
  --disable-fast-injections   If set disables the fast processing of injections
                              time frequency map produced only for small window
                              around injection time [OPTIONAL]
  -c, --catalogdir            Specify location of waveform catalog files for
                              astrophysical injections (e.g., supernovae)
                              [OPTIONAL]
  --big-mem <memory>          Integer specifying the minimal memory requirement
                              for xmerge jobs in MB [OPTIONAL].  Default 1500.
                              Note that xsearch jobs have a hardwired minimal
                              memory requirement of 1000 MB.
  --use-merging-cuts <path>   Specify the location of a post-processing
                              directory from which coherent cuts
                              should be read and applied to triggers
                              at the the trigger collection stage
  --reuse-inj                 If --use-merging-cuts option also set will
                              reuse the injection from the previous cut-tuning
                              run. Use with caution no check is performed on
                              consistency between provided and needed injections.
  --long-inj                  If specified, then when making injection dags a
                              buffer will be used in comparing injection peak
                              time to block interval to determine if an
                              injection should be processed by multiple blocks.
                              Works only for waveform sets with names beginning
                              with adi-a, adi-b, adi-c, adi-d, adi-e, ebbh-a,
                              ebbh-d, ebbh-e, mva. See code for details.
  --smart-cluster             This will produce an extra set of DAGs that will run
                              the smart clustering for large production analyses.
  -h, --help                  Display this message and exit
"""
    print(msg, file=sys.stderr)


# -------------------------------------------------------------------------

def make_chunks_from_unused_debugged(scidataobj,length,trig_overlap=0,min_length=0,pad_data=0):

    """
    Create an extra chunk that uses up the unused data in the science segment.
    This is a simplified and debugged version of ScienceData.make_chunks_from_unused().
    Inputs:
      scidataobj: a ScienceData object containing a segment list.
      length: length of chunk in seconds.
      trig_overlap: length of time start generating triggers before the start of the
        unused data.
      min_length: the unused data must be greater than min_length to make a chunk.
      pad_data: exclude the first and last pad_data seconds of the segment when
        generating chunks
    """

    # ---- Loop over all segments.
    for seg in scidataobj:
        # ---- Check if there is unused data longer than the minimum chunk length.
        if seg.unused() > min_length:
            end = seg.end() - pad_data
            start = end - length
            trig_start = end - seg.unused() - trig_overlap
            if start > seg.start():
                seg.add_chunk(start, end, trig_start)
                seg.set_unused(0)


# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
params_file       = None
trigger_time      = None
ra                = None
decl              = None
detector          = []
grb_name          = None
mdc_path          = None
network_selection = False
sky_pos_err       = 0
condorPriority    = "0"
disableFastInjections = False
catalog_dir       = None
mergingCutsPath   = False
reUseInj          = False
minimalMem        = "3000"
longInjections    = False
smartCluster      = False

# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hp:g:r:d:i:n:m:se:c:"
# ---- Long form.
longop = [
   "help",
   "params-file=",
   "grb-time=",
   "right-ascension=",
   "declination=",
   "detector=",
   "grb-name=",
   "mdc-path=",
   "priority=",
   "network-selection",
   "disable-fast-injections",
   "sky-pos-err=",
   "catalogdir=",
   "big-mem=",
   "end-offset=",
   "use-merging-cuts=",
   "reuse-inj",
   "long-inj",
   "smart-cluster"
   ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- We will record the command line arguments in a file called grb.param.
#      This file is used by the web page python script which expects the short
#      form of the options to have been used.
command_string = 'allsky.py '

# ---- Parse command-line arguments.  Arguments are returned as strings, so
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-p", "--params-file"):
        params_file = a
        command_string = command_string + ' -p ' + a
    elif o in ("-g", "--grb-time"):
        trigger_time = float(a)
        command_string = command_string + ' -g ' + a
    elif o in ("-r", "--right-ascension"):
        ra = float(a)
        command_string = command_string + ' -r ' + a
    elif o in ("-d", "--declination"):
        decl = float(a)
        command_string = command_string + ' -d ' + a
    elif o in ("-i", "--detector"):
        detector.append(a)
        command_string = command_string + ' -i ' + a
    elif o in ("-n", "--grb-name"):
        grb_name = a
        command_string = command_string + ' -n ' + a
    elif o in ("-c", "--catalogdir"):
        catalog_dir = a
        command_string = command_string + ' -c ' + a
    elif o in ("-m", "--mdc-path"):
        mdc_path = a
        command_string = command_string + ' -m ' + a
        # elif o in ("-s", "--network-selection"):
        #   network_selection = True
        #   command_string = command_string + ' -s '
    elif o in ("--use-merging-cuts"):
        mergingCutsPath = a
        command_string = command_string + ' --use-merging-cuts ' + a
    elif o in ("--reuse-inj"):
        reUseInj = True
        command_string = command_string + ' --reuse-inj '
    elif o in ("--priority"):
        condorPriority = a
        command_string = command_string + ' --priority ' + a
        # elif o in ("-e", "--sky-pos-err"):
        #   sky_pos_err = float(a)
        #   command_string = command_string + ' -e ' + a
    elif o in ("--big-mem"):
        minimalMem = a
        command_string = command_string + ' --big-mem ' + a
    elif o in ("--disable-fast-injections"):
        disableFastInjections = True
        command_string = command_string + ' --disable-fast-injections '
    elif o in ("--long-inj"):
        longInjections = True
        command_string = command_string + ' --long-inj '
    elif o in ("--smart-cluster"):
        smartCluster = True
        command_string = command_string + ' --smart-cluster '
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not params_file:
    print("No parameter file specified.", file=sys.stderr)
    print("Use --params-file to specify it.", file=sys.stderr)
    sys.exit(1)
if not trigger_time:
    print("No central time specified.", file=sys.stderr)
    print("Use --grb-time to specify it.", file=sys.stderr)
    sys.exit(1)
if not ra:
    print("No right ascension specified.", file=sys.stderr)
    print("Use --right-ascension to specify it.", file=sys.stderr)
    sys.exit(1)
if not decl:
    print("No declination specified.", file=sys.stderr)
    print("Use --declination to specify it.", file=sys.stderr)
    sys.exit(1)
if not detector:
    print("No detectors specified.", file=sys.stderr)
    print("Use --detector to specify each detector in the network.", file=sys.stderr)
    sys.exit(1)
if not grb_name:
    print("No analysis name specified.", file=sys.stderr)
    print("Use --grb-name to specify it.", file=sys.stderr)
if reUseInj and not mergingCutsPath:
    print("Want to reuse injection path to previous tuning not provided", file=sys.stderr)
    print("Use --use-merging-cuts to specify the path to previous tuning", file=sys.stderr)
    sys.exit(1)


#------------------------------------------------------------------------------
#               Test to see if we are running on Atlas.
#------------------------------------------------------------------------------

# ---- Get partial hostname.
os.system('hostname > host.txt')
f = open('host.txt','r')
hostname=f.read()
f.close()

# ---- Get full hostame.
os.system('hostname -f > host.txt')
f = open('host.txt','r')
fullhostname=f.read()
f.close()

os.system('rm host.txt')
# ---- Check hostname and set flag if necessary.
if 'atlas' in fullhostname:
    atlasFlag = 1
elif 'h2' in hostname:
    atlasFlag = 1
elif 'coma' in fullhostname:
    atlasFlag = 1
else:
    atlasFlag = 0


#------------------------------------------------------------------------------
#             Status message.  Report all supplied arguments.
#------------------------------------------------------------------------------

print(file=sys.stdout)
print("####################################################", file=sys.stdout)
print("#             X-AllSky Search Pipeline             #", file=sys.stdout)
print("####################################################", file=sys.stdout)
print(file=sys.stdout)
print("Parsed input arguments:", file=sys.stdout)
print(file=sys.stdout)
print("     parameters file:", params_file, file=sys.stdout)
print("        central time:", trigger_time, file=sys.stdout)
print("       analysis name:", grb_name, file=sys.stdout)
print("    detector network:", detector, file=sys.stdout)
print("            mdc path:", mdc_path, file=sys.stdout)
print("         sky pos err:", sky_pos_err, file=sys.stdout)

if network_selection:
    print("   network selection: automatic", file=sys.stdout)
else:
    print("   network selection: manual", file=sys.stdout)

if mergingCutsPath:
    print("   Using cuts found in:", mergingCutsPath, file=sys.stdout)
if atlasFlag:
    print("    running on Atlas: yes", file=sys.stdout)
else:
    print("    running on Atlas: no", file=sys.stdout)
print(file=sys.stdout)

# ---- Write ASCII file holding allsky.py command.
pfile = open('grb.param','w')
pfile.write(command_string + "\n")
pfile.close()

summary_file = 'grb_summary.txt'
# ---- Append to summary file.
sfile = open(summary_file,'a')
sfile.write('\t'.join(['\nname','gps','ra','dec','network','numSky','numBG','analyse','\n']))
sfile.write('\t'.join([grb_name,str(trigger_time),str(ra),str(decl)]))
sfile.close()

# -----------------------------------------------------------------------------
#                            Preparatory.
# -----------------------------------------------------------------------------

# ---- Generate unique id tag.
os.system('uuidgen > uuidtag.txt')
f = open('uuidtag.txt','r')
uuidtag=f.read()
f.close()
os.system('rm uuidtag.txt')

# ---- Record the current working directory in a string.
cwdstr = "."

# ---- Make directory to store text files (segment lists, parameter
#      files, etc.) that will be input to X-Pipeline.  This is done
#      lsto minimize clutter in the working directory.
try: os.mkdir( 'input' )
except: pass  # -- Kludge: should probably fail with error message.

# ---- Find files which define merging cuts
mergingCutsString = ""
if mergingCutsPath:
    preCutFile = glob.glob(mergingCutsPath + '/*xmake_args_tuned_pre.txt')
    cutFile = glob.glob(mergingCutsPath + '/*xmake_args_tuned.txt')
    origResults = glob.glob(mergingCutsPath + '/*closedbox.mat')
    mergingCutsString = cutFile[0] + " " + origResults[0]
    if preCutFile:
        mergingCutsString = mergingCutsString + " " + preCutFile[0]

# ------------------------------------------------------------------------------
#                        Read configuration file.
# -----------------------------------------------------------------------------

# ---- Status message.
print("Parsing parameters (ini) file ...", file=sys.stdout)

# ---- Check the params_file exists
if not os.path.isfile(params_file):
    print("Error: non existent parameter file: ", \
       params_file, file=sys.stderr)
    sys.exit(1)

# ---- Create configuration-file-parser object and read parameters file.
cp = configparser.ConfigParser()
cp.read(params_file)

if cp.has_option('tags','version') :
    ini_version = cp.get('tags','version')
    print("Parameter file CVS tag:", ini_version, file=sys.stdout)

# ---- NOTE: The following reading of variables can be split up and
#      moved into the relevant sections of the script where the
#      variables are actually used.

# ---- Read needed variables from [parameters] and [background] sections.
# background_period = int(cp.get('background','backgroundPeriod'))
blockTime         =  int(cp.get('parameters','blockTime'))
whiteningTime     =  float(cp.get('parameters','whiteningTime'))
transientTime     =  4 * whiteningTime
onSourceBeginOffset = int(cp.get('parameters','onSourceBeginOffset'))
onSourceEndOffset   = int(cp.get('parameters','onSourceEndOffset'))
onSourceTimeLength = onSourceEndOffset - onSourceBeginOffset
# jobsPerWindow=int(math.ceil(float(onSourceTimeLength)/float(blockTime-2*transientTime)))
# onSourceWindowLength=2*transientTime+jobsPerWindow*(blockTime-2*transientTime)


# ---- Interval of data to be analysed.  Includes transientTime buffers.
start_time = int(trigger_time + onSourceBeginOffset - transientTime)
end_time = int(trigger_time + onSourceEndOffset + transientTime)
duration = int(end_time - start_time)

# ---- We will copy all ifo data and mdc frame caches to the location
#      stored in frameCacheAll.
frameCacheAll = 'input/framecache.txt'

# ---- Read [input] channel parameters.
detectorListLine  = cp.get('input','detectorList')
detectorList      = detectorListLine.split(',')
channelListLine   = cp.get('input','channelList')
channelList       = channelListLine.split(',')
frameTypeListLine = cp.get('input','frameTypeList')
frameTypeList     = frameTypeListLine.split(',')

# ---- Variables that may or may not be defined in .ini file:

# ---- Check for frame cache for real ifo data
try:
    dataFrameCache = cp.get('input','frameCacheFile')
except:
    print("Warning: No frameCacheFile file specified in " \
        "[input] section of configuration file.", file=sys.stdout)
    print("        A frameCache for the ifo data file will be " \
        "generated automatically.", file=sys.stdout)
    dataFrameCache = None

# ---- Check for a seed value for matlab's random number generator.
try:
    seed = int(cp.get('parameters','seed'))
except:
    seed = 931316785
    print("Warning: No seed specified in configuration file.", file=sys.stdout)
    print("         seed will be set to: ", seed, file=sys.stdout)

# ---- Matlab seed can take values between 0 and 2^31-2.
if (seed > pow(2,31)-2) or (seed < 0):
    print("Error: seed must have value between 0 and 2^31-2", file=sys.stderr)
    sys.exit(1)

# ---- Get datafind server.
datafind_server = cp.get('datafind','datafind_server')
# ---- Get datafind executable e.g., ligo_data_find.
datafind_exec = cp.get("datafind", 'datafind_exec')
# ---- Get segfind executable e.g., ligolw_segment_query.
segfind_exec = cp.get("datafind", "segfind_exec")
# ---- Get segs_from_cats executable e.g., ligolw_ligolw_segments_from_cats.
segs_from_cats_exec = cp.get("datafind", "segs_from_cats_exec")
# ---- Get ligolw_print executable e.g., ligolw_print.
ligolw_print_exec = cp.get("datafind", "ligolw_print_exec")

# ---- Status message.
print("... finished parsing parameters (ini) file.", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Validate list of detectors given.
# -------------------------------------------------------------------------

# ---- KLUDGE:TODO: Move this below automatic network selection so that we
#      only perform this once???

# ---- Status message.
print("Comparing requested network to list of known detectors ...", file=sys.stdout)

# ---- For each detector specified with the --detector option, compare to
#      the list of known detectors from the .ini file.  Keep only the requested
#      detectors and corresponding channel name and frame type.
#      KLUDGE:TODO: Should exit with error message if any of the detectors
#      is not recognized.
# ---- Indices of the detectors requested for this analysis.
keepIndex = []
for i in range(0,len(detector)) :
    for ii in range(0,len(detectorList)) :
        if detector[i] == detectorList[ii] :
            keepIndex.append(ii)
# ---- Sort indices so that order matches that used in ini file.
keepIndex.sort()
# ---- We now have a list of the indices of the detectors requested for the
#      analysis.  Keep only these.  Note that we over-write the 'detector'
#      list because we want to make sure the order matches the channel and
#      frameType lists.
detector  = []
channel   = []
frameType = []
for jj in range(0,len(keepIndex)):
    detector.append(detectorList[keepIndex[jj]])
    channel.append(channelList[keepIndex[jj]])
    frameType.append(frameTypeList[keepIndex[jj]])

# ---- Status message.
print("... finished validating network.          ", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Retrieve single-IFO segment lists for analysis period.
# -------------------------------------------------------------------------

# ---- Write time range to a temporary segment file named gps_range.txt.
#      We'll then read this file into a ScienceData object.  It's a hack, but
#      it seems that the only way to populate ScienceData objects is to read
#      segments from a file.
f=open('input/gps_range.txt', 'w')
time_range_string = '1 ' + str(start_time) + ' ' + str(end_time) + ' ' + str(duration) + '\n'
f.write(time_range_string)
f.close()
# ---- Read full analysis time range back in to a ScienceData object for easy manipulation.
analysis_segment = pipeline.ScienceData()
analysis_segment.read( 'input/gps_range.txt', blockTime )

# ---- Prepare storage for full segment lists.
full_segment_list = []
# ---- Prepare storage for lists of analysis and veto segment filenames.
analysis_seg_files = []
veto_seg_files     = []

# ---- If generateSegs ==0 then user should have supplied segment-list and
#      veto-list for each ifo used.
if int(cp.get('segfind','generateSegs'))==0:

    # ---- Loop over ifos we are considering.
    for ifoIdx in range(0,len(detector)):
        ifo = detector[ifoIdx]

        print('Retrieving analysis segment list for ', ifo)
        # ---- Get name of segment file and check that it exists.
        analysis_seg_files.append(cp.get(ifo,'segment-list'))
        if not os.path.isfile(analysis_seg_files[ifoIdx]):
            print("Error: non existent segment list file: ", \
                analysis_seg_files[ifoIdx], file=sys.stderr)
            sys.exit(1)

        print('Retrieving veto segment list for ', ifo)
        # ---- Get name of veto file and check that it exists.
        if cp.has_option(ifo, "veto-list"):
            veto_seg_files.append(cp.get(ifo, "veto-list"))
            if not os.path.isfile(veto_seg_files[ifoIdx]):
                print("Error: non existent veto list file: ", \
                    veto_seg_files[ifoIdx], file=sys.stderr)
                sys.exit(1)
        else:
            veto_seg_files.append("None")

# ---- If generateSegs ~= 0 then we will generate segment-list and veto-list
#      for each ifo.
else:

    print('Generating veto segment lists for all ifos ')
    # ---- Set output dir for cat 1,2,3,4,5 veto files
    segs_from_cats_output_dir = "input/"
    if not(segs_from_cats_output_dir.endswith('/')):
        segs_from_cats_output_dir = segs_from_cats_output_dir + "/"

    # ---- Run segs_from_cats_exec to determine cat 1,2,3,4,5 vetos
    segs_from_cats_call = ' '.join([ segs_from_cats_exec,
        "--segment-url", cp.get("segs_from_cats", "segment-url"),
        "--veto-file", cp.get("segs_from_cats", "veto-file"),
        "--gps-start-time", str(start_time),
        "--gps-end-time", str(end_time),
        "--output-dir", segs_from_cats_output_dir,
        "--separate-categories"])
    if cp.has_option("segs_from_cats","dmt-file"):
        segs_from_cats_call = ' '.join([segs_from_cats_call, "--dmt-file"])
    print(segs_from_cats_call, file=sys.stdout)
    os.system(segs_from_cats_call)

    for ifoIdx in range(0,len(detector)):
        ifo = detector[ifoIdx]

        for catIdx in [1,2,4]:

            # ---- Construct name of veto files.
            vetocat_filename = ''.join([segs_from_cats_output_dir,ifo,
                "-VETOTIME_CAT",str(catIdx),"-",str(start_time),"-",str(duration)])
            vetocat_filename_XML = vetocat_filename + ".xml"
            vetocat_filename_TXT = ''.join(["input/",ifo,"-veto-cat",str(catIdx),".txt"])

            # ---- Check file exists.
            if not(os.path.isfile(vetocat_filename_XML)):
                print("Error: Problem creating veto file:", \
                    vetocat_filename_XML, file=sys.stderr)
                sys.exit(1)

            # ---- Convert XML file to TXT format.
            segsToTxtCall = ' '.join([ ligolw_print_exec,
                "--table segment --column start_time --column end_time",
                "--delimiter ' '",
                vetocat_filename_XML,
                "| awk '{print NR  \" \" $1 \" \" $2 \" \" $2-$1}' >",
                vetocat_filename_TXT ])
            os.system(segsToTxtCall)

        print('Generating science segment list for ', ifo)
        seg_filename     = ''.join(["input/",ifo,"-seg"])
        seg_filename_XML = ''.join([seg_filename,".xml"])
        seg_filename_TXT = ''.join([seg_filename,".txt"])

        segFindCommand = ' '.join([segfind_exec,
            "--query-segments",
            "--segment-url", cp.get(ifo, "segment-url"),
            "--gps-start-time", str(start_time),
            "--gps-end-time", str(end_time),
            "--include-segments", cp.get(ifo, "include-segments"),
            "--output-file", seg_filename_XML ])
        if cp.has_option("segfind","dmt-file"):
            segfind_call = ' '.join([segfind_call, "--dmt-file"])
        print(segFindCommand, file=sys.stdout)
        os.system(segFindCommand);
        print('... finished generating segment list.')

        print('Converting segment list from XML to TXT file: ')
        segsToTxtCall = ' '.join([ ligolw_print_exec,
            "--table segment",
            "--column start_time",
            "--column end_time",
            "--delimiter",
            "' '",
            seg_filename_XML,
            "| awk '{print NR  \" \" $1 \" \" $2 \" \" $2-$1}' >",
            seg_filename_TXT ])
        os.system(segsToTxtCall)
        print('... finished converting segment list to TXT file.')

        # ---- Read in science and veto segments.
        sciseg   = segmentsUtils.fromsegwizard(open(seg_filename_TXT)); sciseg.coalesce()
        cat1veto = segmentsUtils.fromsegwizard(open("input/"+ifo+"-veto-cat1.txt")); cat1veto.coalesce()
        cat2veto = segmentsUtils.fromsegwizard(open("input/"+ifo+"-veto-cat2.txt")); cat2veto.coalesce()
        cat4veto = segmentsUtils.fromsegwizard(open("input/"+ifo+"-veto-cat4.txt")); cat4veto.coalesce()

        # ---- Subtract cat1veto flags from the science segments.
        sciseg_cat1 = sciseg - cat1veto; sciseg_cat1.coalesce()

        # ---- Write out newly constructed segment list.
        filename = ''.join(["input/",ifo,"_science_cat1.txt"])
        print('Writing file: ', filename)
        f = open(filename,"w"); segmentsUtils.tosegwizard(f,sciseg_cat1); f.flush(); f.close()
        analysis_seg_files.append(filename)

        # ---- Add cat2veto and cat4veto segments for each ifo.
        cat24veto = cat2veto + cat4veto; cat24veto.coalesce()
        # ---- Write out newly constructed veto list.
        filename = ''.join(["input/",ifo,"_cat24veto.txt"])
        print('Writing file: ', filename)
        f = open(filename,"w"); segmentsUtils.tosegwizard(f,cat24veto); f.flush(); f.close()
        veto_seg_files.append(filename)

# ---- Read in segment_lists to ScienceData object.
for ifoIdx in range(0,len(detector)):
    ifo = detector[ifoIdx]

    # ---- Read full segment list into a ScienceData object for easy
    #      manipulation.  Throw away science segment shorter than the
    #      blockTime value read from the configuration file.
    print('Reading segment list from file ', analysis_seg_files[ifoIdx])
    full_segment = pipeline.ScienceData()
    full_segment.read( analysis_seg_files[ifoIdx], blockTime )
    print('... finished reading segment list.')

    # ---- Now restrict to desired analysis time range around trigger_time.
    full_segment.intersection(analysis_segment)
    # ---- Finally, append segment for this detector to the full list.
    full_segment_list.append(full_segment)


# -------------------------------------------------------------------------
#          Choose appropriate likelihoods and lags for this network.
# -------------------------------------------------------------------------

print(" ", file=sys.stdout)

if len(detector) ==0:
    print("Error:No ifos in network!!!", file=sys.stderr)
    sys.exit(1)

elif len(detector) ==1:
    print("One ifo in network: ", detector, file=sys.stdout)
    lagType = []
    likelihoodType = "likelihoodType_1det1site"

elif len(detector) ==2:
    if detector.count('H1') and detector.count('H2'):
        print("Two aligned ifos in network: ", detector, file=sys.stdout)
        lagType = "lags_2det1site"
        likelihoodType = "likelihoodType_2det1site"
    else:
        print("Two misaligned ifos in network: ", detector, file=sys.stdout)
        lagType = "lags_2det2site"
        likelihoodType = "likelihoodType_2det2site"

elif len(detector) ==3:
    if detector.count('H1') and detector.count('H2'):
        print("Three ifos at two sites: ", detector, file=sys.stdout)
        lagType = "lags_3det2site"
        likelihoodType = "likelihoodType_3det2site"
    else:
        print("Three ifos at three sites: ", detector, file=sys.stdout)
        lagType = "lags_3det3site"
        likelihoodType = "likelihoodType_3det3site"

elif len(detector) ==4:
    if detector.count('H1') and detector.count('H2'):
        lagType = "lags_4det3site"
        likelihoodType = "likelihoodType_4det3site"
    else:
        print("Four ifos at four sites: ", detector, file=sys.stdout)
        lagType = "lags_4det4site"

elif len(detector) ==5:
    if detector.count('H1') and detector.count('H2'):
        print("Five ifos at four sites: ", detector, file=sys.stdout)
        lagType = "lags_5det4site"
        likelihoodType = "likelihoodType_5det4site"
    else:
        print("Five ifos at five sites: ", detector, file=sys.stdout)
        lagType = "lags_5det5site"
        likelihoodType = "likelihoodType_5det5site"

# -----------------------------------------------------------------------------
#               Construct tilde-separated list of sites
# -----------------------------------------------------------------------------

# ---- Initialise tilde-separated list of sites.
siteStrTilde = ''
for iDet in range(0,len(detector)):
    # ---- Get name of current site from current detector.
    siteTemp = detector[iDet][0]
    # ---- Add current site to list only if does not already appear in it.
    if siteStrTilde.count(siteTemp)==0:
        siteStrTilde = '~'.join([siteStrTilde,siteTemp])

# ---- Remove preceding tilde.
siteStrTilde = siteStrTilde[1:len(siteStrTilde)]

# -----------------------------------------------------------------------------
#               Construct tilde-separated list of detectors
# -----------------------------------------------------------------------------

# ---- Write strings (no-delimiter and tilde-delimited) listing detectors.
detectorStr = ''
detectorStrTilde = ''
for ifo in detector:
    detectorStr = detectorStr + ifo
    detectorStrTilde = detectorStrTilde + ifo + '~'
detectorStrTilde = detectorStrTilde[0:len(detectorStrTilde)-1]

# ---- Write tilde-delimited string listing frame types.
frameTypeStrTilde = ''
for frameTypeName in frameType:
    frameTypeStrTilde = frameTypeStrTilde + frameTypeName + '~'
frameTypeStrTilde = frameTypeStrTilde[0:len(frameTypeStrTilde)-1]

# ---- Append detector string to summary file.
sfile = open(summary_file,'a')
sfile.write('\t' + detectorStr)
sfile.close()

# -----------------------------------------------------------------------------
#         Having figured out network read in appropriate lag file.
# -----------------------------------------------------------------------------

# ---- We only need to read in a lag file if our network has 2 or more ifos.
if lagType:
    try:
        lagFile =  cp.get('background',lagType)
    except:
        print("Warning: No lagFile specified in configuration file.", file=sys.stdout)
        print("         No time lag jobs will be made.", file=sys.stdout)
        lagFile =  None

    if lagFile:
        if not os.path.isfile(lagFile):
            print("Error: non existant lag file: ",lagFile, file=sys.stderr)
            sys.exit(1)
else:
    lagFile = None

print("Using lag file: ", lagFile, file=sys.stdout)


# -----------------------------------------------------------------------------
#         Having figured out network read in appropriate likelihood types.
# -----------------------------------------------------------------------------

try:
    likelihoodTypeStr =  cp.get('parameters',likelihoodType)
except:
    print("Error: required likelihoodType not specified in " \
       "configuration file.", file=sys.stderr)
    sys.exit(1)

print("Using likelihoods: ", likelihoodTypeStr, file=sys.stdout)

# -------------------------------------------------------------------------
#      Write channel file.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing Matlab-formatted channel file ...      ", file=sys.stdout)

# ---- KLUDGE: ToDo: Add channelVirtualNames as an optional parameter
#      read from the parameter file and copied into the Matlab channel
#      file.  This will help with analysis of simulated data.
# ---- For each detector, write the
#      corresponding channel name and frame type to a file.
f=open('input/channels.txt', 'w')
for i in range(0,len(detector)) :
    f.write(detector[i] + ':' + channel[i] + ' ' + frameType[i] + '\n')
f.close()

# ---- Status message.
print("... finished writing channel file.        ", file=sys.stdout)
print(file=sys.stdout)

# -------------------------------------------------------------------------
#    Determine MDCs to process, write mdcChannelFiles and framecache file.
# -------------------------------------------------------------------------

# ---- Check for MDC sets.
if cp.has_option('mdc','mdc_sets') :

    # ---- Status message.
    print("Writing Matlab-formatted MDC channel files "\
        "and framecache files... ", file=sys.stdout)

    # ---- Get list of MDC sets to process.
    mdc_setsList = cp.get('mdc','mdc_sets')
    mdc_sets = mdc_setsList.split(',')
    # print >> sys.stdout, "mdc_sets:", mdc_sets

    # ---- Loop over MDC sets, making channel file and framecache (if needed)
    #      for each. Also copy log file to expected location and name.
    for setIdx in range(len(mdc_sets)) :

        set = mdc_sets[setIdx]
        print("Processing MDC set", set, file=sys.stdout)

        if cp.has_section(set) :

            # ---- Copy the MDC log file to input/ and rename it according to
            #      the usual convention.
            command = 'cp ' + cp.get(set,'logFileName') + ' input/injection_' + set + '.txt'
            os.system(command)

            # ---- Read channel parameters for this mdc set.
            mdcChannelListLine = cp.get(set,'channelList')
            mdcChannelList = mdcChannelListLine.split(',')
            mdcFrameTypeListLine = cp.get(set,'frameTypeList')
            mdcFrameTypeList = mdcFrameTypeListLine.split(',')
            # ---- Keep only info for detectors requested for the analysis.
            mdcChannel = []
            mdcFrameType = []
            for jj in range(0,len(keepIndex)):
                mdcChannel.append(mdcChannelList[keepIndex[jj]])
                mdcFrameType.append(mdcFrameTypeList[keepIndex[jj]])
            # ---- Write the MDC channel file for this MDC set (contains the
            #      MDC channel names and frame types for each detector).
            f=open('input/channels_' + set + '.txt', 'w')
            for i in range(0,len(detector)) :
                f.write(detector[i] + ':' + mdcChannel[i] + ' ' + mdcFrameType[i] + '\n')
            f.close()

            # ---- Find the MDC frames.
            # ---- First check to see if an MDC frame cache has been specified
            #      in the configuration file.
            try:
                mdcFrameCache = cp.get(set,'frameCacheFile')
            except:
                print("Warning: No frameCacheFile file specified " \
                    "in [" + set + "] section of configuration file.", file=sys.stdout)
                print("         A frameCache for the ifo data file " \
                    "will be generated automatically.", file=sys.stdout)
                mdcFrameCache = None

            if mdcFrameCache:

                # ---- Check that the frame cache file specified actually exists.
                if not os.path.isfile(mdcFrameCache):
                    print("Error: non existant framecache: ", mdcFrameCache, file=sys.stderr)
                    sys.exit(1)

                # ---- If the specified mdc frame cache exists then concat it
                #      with the other frame caches.
                command = 'cat ' + mdcFrameCache + '  >> ' + frameCacheAll
                os.system(command)

            else:

                # ---- The mdcFrameCache was not specified, so generate it here.
                #      Note that MDC frames are usually contain the h(t) channels
                #      for all detectors in a single frame. The "observatory"
                #      type is therefore usually multi-detector, e.g GHLV.
                #      The following code loops over the distinct observatory
                #      types; if only one, then it is assumed that all channels
                #      are stored in a single frame type.
                mdcFrameObsListLine = cp.get(set,'frameObsList')
                mdcFrameObsList = mdcFrameObsListLine.split(',')
                for i in range(len(mdcFrameObsList)) :

                    # ---- Status message.
                    print("Writing MDC framecache file for MDC set " \
                        + set + ", observatory " + mdcFrameObsList[i] + " ...", file=sys.stdout)

                    # ---- Clear away any pre-existing mdcframecache files.
                    os.system('rm -f mdcframecache_temp.txt')

                    # ---- Construct dataFind command.
                    dataFindCommand = ' '.join([datafind_exec,
                    "--observatory",mdcFrameObsList[i],
                    "--type",mdcFrameType[i],
                    "--gps-start-time", str(start_time),
                    "--gps-end-time",str(end_time),
                    "--url-type file",
                    "--lal-cache",
                    " > mdclalcache.txt"])

                    # ---- Issue dataFind command.
                    print("calling dataFind:", dataFindCommand)
                    os.system(dataFindCommand)
                    print("... finished call to dataFind.")

                    # ---- Convert lalframecache file to readframedata format.
                    print("calling convertlalcache:")
                    os.system('convertlalcache.pl mdclalcache.txt mdcframecache_temp.txt')
                    os.system('cat mdcframecache_temp.txt >> ' + frameCacheAll)
                    print("... finished call to convertlalcache.")

                    # ---- Clean up.
                    os.system('rm -f mdcframecache_temp.txt mdclalcache.txt')

                    # ---- Status message.
                    print("... finished writing MDC framecache file.", file=sys.stdout)
                    print(file=sys.stdout)

        else :

            print("Error: requested MDC set ", set, \
                " is not defined in the parameters file.  Exiting.", file=sys.stdout)
            print(file=sys.stdout)
            sys.exit(1)

    # ---- Status message.
    print("... finished writing MDC channel and frame files.   ", file=sys.stdout)
    print(file=sys.stdout)


# -------------------------------------------------------------------------
#    Make coincidence segment list for on-source, zero-lag segment.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing on-source event file and related files ... ", file=sys.stdout)

# ---- Now make segment lists for coincidence operation.  First determine
#      segment list for zero lag, and verify that the on-source time is
#      contained by a segment, else quit with error.
# ---- On source period: +/- minimumSegmentLength / 2 around trigger time.
on_source_start_time = int(trigger_time + onSourceBeginOffset - transientTime)
on_source_end_time = int(trigger_time + onSourceEndOffset + transientTime)

# ---- Write time range to a temporary segment file named on_source_interval.txt.
#      We'll then read this file into a ScienceData object.  It's a hack, but
#      it seems that the only way to populate ScienceData objects is to read
#      segments from a file.
f=open('input/on_source_interval.txt', 'w')
time_range_string = '1 ' + str(on_source_start_time) + ' ' \
    + str(on_source_end_time) + ' ' + str(on_source_end_time - on_source_start_time) + '\n'
# time_range_string = '1 ' + str(on_source_start_time) + ' ' \
#     + str(int(on_source_start_time + onSourceWindowLength)) + ' ' + str(blockTime) + '\n'
f.write(time_range_string)
f.close()

# ---- Read on-source time range into a "ScienceData" object.
on_source_segment = pipeline.ScienceData()
on_source_segment.read( 'input/on_source_interval.txt', blockTime )

# ---- Now get the intersection of all of the detector segment lists with
#      this on-source list.
coincidence_segment = copy.deepcopy(on_source_segment)
for det in full_segment_list:
    coincidence_segment.intersection(det)

# ---- Write "window" file. To-do: do we still need this?
fwin = open('input/window_on_source.txt', 'w')
fwin.write(str(int(trigger_time)))
fwin.close()

# ---- Split coincidence segment list into "chunks".
coincidence_segment.make_chunks(blockTime,2*transientTime)
make_chunks_from_unused_debugged(coincidence_segment,blockTime,2*transientTime)

# ---- Write coincidence blocks to "event" and "segment" files.
f=open('input/event_on_source.txt', 'w')
fseg = open('input/segment_on_source.txt', 'w')
for seg in coincidence_segment:
    for i in range(len(seg)):
        # ---- Write event to event file.
        time_range_string = str((seg.__getitem__(i).start() \
            + seg.__getitem__(i).end()) / 2) + '\n'
        f.write(time_range_string)
        # ---- Write segment to segment file.
        time_range_string = '0 ' + str(int(seg.__getitem__(i).start())) \
            + ' ' + str(int(seg.__getitem__(i).end())) \
            + ' ' + str(int(seg.__getitem__(i).end() \
            - seg.__getitem__(i).start())) + '\n'
        fseg.write(time_range_string)
        gpsBlockCenterTime = (seg.__getitem__(i).start()+seg.__getitem__(i).end())/2
f.close()
fseg.close()

# ---- Make copies of the on-source event & window files.  These are needed
#      by xmergegrbfiles, but are not used by allsky.py.
os.system('cp input/window_on_source.txt input/window_inj_source.txt')
os.system('cp input/event_on_source.txt input/event_inj_source.txt')

# ---- Status message.
print("... finished writing on-source event file.", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#     Write Matlab-formatted file with sphrad clustering parameters
# -------------------------------------------------------------------------

# ---- KLUDGE: This code works except ... the config parser by default lowers
#      the case of keys it reads in. The matlab function xparameters.m is
#      expecting mixed case key names, so it will not read the
#      sphradparameters.txt file produced by this code. So comment out this
#      section until a fix is put in.
#      NOTE: The proper fix is to make the config parser preserve case; see
#      e.g http://stackoverflow.com/questions/1611799/preserve-case-in-configparser
#      But this will break everything else that uses the config parser! Argh!
#      Once fixed, be sure to change the parameter files to point to the new
#      finle in the input/ directory.

# # ---- Note: If outputType is sphrad, then the ini file must contain a section
# #      which specifies the parameters required by sphrad.
# outputType=cp.get('parameters','outputType')
# if outputType == 'sphrad' :
#
#     # ---- Status message.
#     print >> sys.stdout, "Writing sphrad parameters file ..."
#
#     # ---- Parameters file for sphrad analysis.
#     f=open('input/sphradparameters.txt','w');
#
#     # ---- Write all options available in the [sphrad] section to this file.
#     sphradParams = cp.options('sphrad')
#     for i in range(0,len(sphradParams)) :
#         value = cp.get('sphrad',sphradParams[i])
#         f.write(sphradParams[i] + ': ' + value + '\n')
#     f.close()
#
# # ---- Status message.
# print >> sys.stdout, "... finished sphrad parameters file.     "
# print >> sys.stdout


# -------------------------------------------------------------------------
#      Write Matlab-formatted parameter files for on- and off-source.
# -------------------------------------------------------------------------

# ---- KLUDGE: Set sky grid and number of sky positions.
#      Not appropriate since all-sky search (with sphrad) will determine its
#      own effective grid, but xdetection and xmakeallskywebpage are still
#      expecting these to be defined. So we assign dummy values. Eventually
#      we will remove these variables.
numSky = 1
# ---- Set skyPositionList arg that will be written to matlab format
#      parameters files.
skyPositionList = '[' + str(decl) + ',' + str(ra) + ']'
# ---- Append to summary file.
sfile = open(summary_file,'a')
sfile.write('\t' + str(numSky))
sfile.close()

# ---- Write all options available in the parameters section to a file.
parameters = cp.options('parameters')

# ---- Parameters file for on-source analysis.
print("Writing on-source parameter file ... ", file=sys.stdout)
fParam=open('input/parameters_on_source_0.txt', 'w')
# ---- First write framecache file, channel file, event file, and sky position.
fParam.write('channelFileName:input/channels.txt' + '\n')
fParam.write('frameCacheFile:' + frameCacheAll + '\n')
fParam.write('eventFileName:input/event_on_source.txt' + '\n')
fParam.write('skyPositionList:' + skyPositionList + '\n')
fParam.write('skyCoordinateSystem:radec' + '\n')
fParam.write('likelihoodtype:' + likelihoodTypeStr + '\n')
# ---- Now write all of the other parameters from the parameters section.
#      We ignore the likelihoodType_* lines since this is handled above.
for i in range(0,len(parameters)) :
    if not(parameters[i].startswith("likelihoodtype")):
        value = cp.get('parameters',parameters[i])
        if parameters[i] == "circtimeslidestep":
            continue
        else:
            fParam.write(parameters[i] + ':' + value + '\n')
fParam.close()
# ---- Status message.
print("... finished writing on-source parameter file.", file=sys.stdout)
print(file=sys.stdout)

# ---- Parameters file for off-source analysis.
print("Writing off-source parameter file ... ", file=sys.stdout)
fParam=open('input/parameters_off_source_0.txt', 'w')
# ---- First write framecache file, channel file, event file, and sky position.
fParam.write('channelFileName:input/channels.txt' + '\n')
fParam.write('frameCacheFile:' + frameCacheAll + '\n')
fParam.write('eventFileName:input/event_off_source.txt' + '\n')
fParam.write('skyPositionList:' + skyPositionList + '\n')
fParam.write('skyCoordinateSystem:radec' + '\n')
fParam.write('likelihoodtype:' + likelihoodTypeStr + '\n')
# ---- Now write all of the other parameters from the parameters section.
#      We ignore the likelihoodType_* lines since this is handled above.
for i in range(0,len(parameters)) :
    if not(parameters[i].startswith("likelihoodtype")):
        value = cp.get('parameters',parameters[i])
        fParam.write(parameters[i] + ':' + value + '\n')
fParam.close()
# ---- Status message.
print("... finished writing off-source parameter file.", file=sys.stdout)
print(file=sys.stdout)


# -----------------------------------------------------------------------------
#      Write Matlab-formatted parameter files for OTF simulations.
# -----------------------------------------------------------------------------

# ---- Parameter files for on-the-fly simulated waveform analyses, if requested.
if cp.has_section('waveforms') :

    # ---- Status message.
    print("Writing OTF injection parameter files ...", file=sys.stdout)

    # ---- Read [injection] parameters.  If waveform_set is empty then no
    #      files will be written.
    waveform_set = cp.options('waveforms')
    # ---- Write one parameters file for each (waveform set, injection scale) pair.
    for set in waveform_set :
        if cp.has_section(set) & cp.has_option(set,'injectionScales') :
            # ---- This check lets you specify different injection scales and
            #      spacing for each waveform set.
            injectionScalesList = cp.get(set,'injectionScales')
            injectionScales = injectionScalesList.split(',')
        else :
            # ---- Otherwise, use the injection scales and spacing specified in
            #      the [injection] section.
            injectionScalesList = cp.get('injection','injectionScales')
            injectionScales = injectionScalesList.split(',')

        # ---- Write one parameter file holding all injection scales.
        scale_counter = 0
        # for injectionScale in injectionScales :
        fParam=open("input/parameters_simulation_" + set + "_" + str(scale_counter) + "_0.txt", 'w')
        # ---- First write framecache file, channel file, event file, and sky position.
        fParam.write('channelFileName:input/channels.txt' + '\n')
        fParam.write('frameCacheFile:' + frameCacheAll + '\n')
        fParam.write('eventFileName:input/event_on_source.txt' + '\n')
        fParam.write('catalogdirectory:input/' + '\n')
        fParam.write('skyPositionList:' + skyPositionList + '\n')
        fParam.write('skyCoordinateSystem:radec' + '\n')
        fParam.write('likelihoodtype:' + likelihoodTypeStr + '\n')
        # ---- Now write all of the other parameters from the parameters section.
        #      We ignore the likelihoodType_* lines since this is handled above.
        for i in range(0,len(parameters)) :
            if not(parameters[i].startswith("likelihoodtype")) :
                value = cp.get('parameters',parameters[i])
                if parameters[i] == "outputtype"  and value == "clusters" and not(disableFastInjections):
                    fParam.write('outputtype:injectionclusters\n')
                elif parameters[i] == "circtimeslidestep":
                    continue
                else:
                    fParam.write(parameters[i] + ':' + value + '\n')

        # ---- Write simulations info.  The specified injection file
        #      will be written later.
        fParam.write('injectionFileName:input/injection_' + set + '.txt' + '\n')
        fParam.write('injectionScales:' + injectionScalesList + '\n')
        fParam.close()

    # ---- Status message.
    print("... finished writing OTF injection parameter files.", file=sys.stdout)
    print(file=sys.stdout)


# -----------------------------------------------------------------------------
#      Write Matlab-formatted parameter files for MDC simulations.
# -----------------------------------------------------------------------------

# ---- We will write three sets of parameters files:  one on-source file,
#      one off-source file, andone injection file for each waveform set.

# ---- Parameter files for MDC waveform analyses, if requested.
if cp.has_option('mdc','mdc_sets') :

    # ---- Status message.
    print("Writing MDC injection parameter files ...", file=sys.stdout)

    # ---- Read [mdc] sets and injection scales.  If mdc_sets is empty or specifies
    #      unknown MDC sets then the script will have already exited when trying to
    #      write the mdcchannel file above.
    mdc_setsList = cp.get('mdc','mdc_sets')
    mdc_sets = mdc_setsList.split(',')
    # ---- Write one parameters file for each (mdc set, injection scale) pair.
    for set in mdc_sets :
        if cp.has_section(set) & cp.has_option(set,'injectionScales') :
            # ---- This check lets you specify different injection scales for
            #      each MDC set.
            injectionScalesList = cp.get(set,'injectionScales')
            injectionScales = injectionScalesList.split(',')
        else :
            # ---- Otherwise, use the injection scales specified in
            #      the [injection] section.
            injectionScalesList = cp.get('injection','injectionScales')
            injectionScales = injectionScalesList.split(',')

        # ---- Write one parameter file holding all injection scales.
        f=open("input/parameters_simulation_" + set + "_0_0.txt", 'w')
        # ---- First write framecache file, channel file, event file, and sky position.
        f.write('channelFileName:input/channels.txt' + '\n')
        f.write('frameCacheFile:' + frameCacheAll + '\n')
        f.write('eventFileName:input/event_on_source.txt' + '\n')
        f.write('skyPositionList:' + skyPositionList + '\n')
        f.write('skyCoordinateSystem:earthfixed' + '\n')
        f.write('likelihoodtype:' + likelihoodTypeStr + '\n')
        # ---- Now write all of the other parameters from the parameters section.
        #      We ignore the likelihoodType_* lines since this is handled above.
        for i in range(0,len(parameters)) :
            if not(parameters[i].startswith("likelihoodtype")):
                value = cp.get('parameters',parameters[i])
                if parameters[i] == "outputtype"  and value == "clusters" and not(disableFastInjections):
                    f.write('outputtype:injectionclusters\n')
                elif parameters[i] == "circtimeslidestep":
                    continue
                else:
                    f.write(parameters[i] + ':' + value + '\n')
        # ---- Write mdc info.
        f.write('mdcChannelFileName:input/channels_' + set + '.txt' + '\n')
        f.write('injectionFileName:input/injection_' + set + '.txt' + '\n')
        f.write('injectionScales:' + injectionScalesList + '\n')
        f.close()

    # ---- Status message.
    print("... finished writing MDC parameter files.     ", file=sys.stdout)
    print(file=sys.stdout)


# -------------------------------------------------------------------------
#    Copy waveform catalogs (if specified) to input/.
# -------------------------------------------------------------------------

if catalog_dir:
    print("Copying waveform catalogs to input/ ...", file=sys.stdout)
    cpCommand = ' '.join(['cp ' + catalog_dir + '/*.mat input/'])
    os.system(cpCommand)
    print("... finished copying waveform catalogs.", file=sys.stdout)
    print(file=sys.stdout)


# -------------------------------------------------------------------------
#    Make off-source coincidence segment lists for all lags.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing off-source event file ...          ", file=sys.stdout)

# ---- Keep track of number of off-source events.
numOff = 0

# ---- Now open lag file (required) and write event file for all non-zero lags.
#      Lag file will have one lag per detector; any of them may be zero.
if lagFile:
    f = open('input/event_off_source.txt', 'w')
    #f = open('input/window_off_source.txt', 'w')
    fseg = open('input/segment_off_source.txt', 'w')
    lag_list = open(lagFile,mode='r')
    for line in lag_list:
        # ---- Extract time lag for each detector.
        lags = line.split(None)
        if len(lags) != len(detector):
            print("Error: the lag file should have number of " \
            "columns equal to the the number of detectors we are analysing. " \
            " Lag file: ", lagFile, file=sys.stderr)
            sys.exit(1)
        # ---- Make a time-lagged copy of the segment lists.
        lag_full_segment_list = copy.deepcopy(full_segment_list)
        # ---- Time shift segment list of each detector.
        for i in range(len(detector)):
            for seg in lag_full_segment_list[i]:
                seg.set_start(seg.start()-int(lags[i]))  # -- SUBTRACT lag
                seg.set_end(seg.end()-int(lags[i]))  # -- SUBTRACT lag
        # ---- Now take coincidence of time-lagged segment lists.
        lag_coincidence_list = copy.deepcopy(lag_full_segment_list[0])
        for i in range(1,len(detector)):
            lag_coincidence_list.intersection(lag_full_segment_list[i])
        # ---- Split coincidence segment list into "chunks".
        lag_coincidence_list.make_chunks(blockTime,2*transientTime)
        make_chunks_from_unused_debugged(lag_coincidence_list,blockTime,2*transientTime)
        for seg in lag_coincidence_list:
            for i in range(len(seg)):
                # ---- Write event to event file.
                time_range_string = str((seg.__getitem__(i).start() \
                    + seg.__getitem__(i).end()) / 2)
                time_range_string = time_range_string + ' ' + line
                f.write(time_range_string)
                # ---- Write segment to segment file.
                time_range_string = '0 ' + str(int(seg.__getitem__(i).start())) \
                    + ' ' + str(int(seg.__getitem__(i).end())) \
                    + ' ' + str(int(seg.__getitem__(i).end() \
                    - seg.__getitem__(i).start())) + '\n'
                fseg.write(time_range_string)
                numOff = numOff + 1
    f.close()
    fseg.close()
else:
    print("Error: no lag file specified.", file=sys.stderr)
    sys.exit(1)

# ---- Compute the number of effective bkg trials we can make with the
#      background blocks. Fill window_off_source with that number of copies of
#      the trigger time.
# ---- Read the number of chunks in on-source from the event list.
event_on_file = open('input/event_on_source.txt')
event_on_list = event_on_file.readlines()
nOnEvents = len(event_on_list)
event_on_file.close()
# ---- Count the number of effective background trials.
nEffBkgTrials = int(math.floor(float(numOff)/float(nOnEvents)))
fwin = open('input/window_off_source.txt','w');
for iTrial in range(0,nEffBkgTrials):
    fwin.write(str(int(trigger_time)) + "\n")
fwin.close()
# ---- Throw away events that would form a last non-complete effective
#      background trial.
# ---- Read the number of chunks in on-source from the event list.
event_off_file = open('input/event_off_source.txt')
event_off_list = event_off_file.readlines()
event_off_file.close()
# ---- Re-write off-source list, writing a whole number of effective
#      background trials.
event_off_file = open('input/event_off_source.txt','w')
for iEvent in range(0,nEffBkgTrials*nOnEvents):
    event_off_file.write(event_off_list[iEvent])
event_off_file.close()
numOff = nEffBkgTrials*nOnEvents

# ---- Append to summary file.
sfile = open(summary_file,'a')
sfile.write('\t' + str(numOff))
sfile.close()

# ---- Status message.
print("... finished writing off-source event file.", file=sys.stdout)
print(file=sys.stdout)


# -----------------------------------------------------------------------------
#                   Find data frames, if necessary.
# -----------------------------------------------------------------------------

# ---- If dataFrameCache is specified and exists we will add it to frameCacheAll
if dataFrameCache:
    # ---- check the frameCache specified actually exists
    if not os.path.isfile(dataFrameCache):
        print("Error: non existant framecache: ",dataFrameCache, file=sys.stderr)
        sys.exit(1)

    # ---- if the specified mdc frame cache exists then concat it other frame caches
    command = 'cat ' + dataFrameCache + '  >> ' + frameCacheAll
    os.system(command)

# ---- If dataFrameCache is not specified in the config file, then call dataFind
#      for each detector, and convert to readframedata-formatted framecache file.
else:
    # ---- Status message.
    print("Writing framecache file for ifo data...", file=sys.stdout)
    os.system('rm -f framecache_temp.txt')
    # ---- Loop over detectors.
    for i in range(0,len(detector)):
        # ---- Construct dataFind command.
        dataFindCommand = ' '.join([datafind_exec, \
        "--observatory", detector[i][0], \
        "--type", frameType[i], \
        "--gps-start-time", str(start_time),  \
        "--gps-end-time", str(end_time),    \
        "--url-type file", "--gaps", "--lal-cache > lalcache.txt 2> ligo_data_find.err"])
        # ---- Issue dataFind command.
        print("calling dataFind:", dataFindCommand)
        os.system(dataFindCommand)
        os.system('cat ligo_data_find.err')
        print("... finished call to dataFind.")
        # ---- Convert lalframecache file to readframedata format.
        print("calling convertlalcache:")
        os.system('convertlalcache.pl lalcache.txt framecache_temp.txt')
        os.system('cat framecache_temp.txt >> ' + frameCacheAll)
        print("... finished call to convertlalcache.")

        # ---- Check that none of the missing frames overlap with
        #      our analysis segment lists.
        # ---- Read stderr log of ligo_data_find
        f = open('ligo_data_find.err','r')
        gaps_str=f.read()
        f.close()
        os.system('rm ligo_data_find.err')

        # ---- If there are any missing frames see if they overlap
        #      with our analysis segments.
        if gaps_str:
            # ---- The format of the ligo_data_find.err file depends on which data find executable
            #      we are using. Check which it is and act appropriately.
            if datafind_exec=='ligo_data_find':
                # ---- (trimmed) File contains one line of the form
                #          missing segments: [segment(866386702, 866393054), ..., segment(866477785, 866488107)]
                # ---- Now proceed in confidence.
                gaps_str = gaps_str.replace('segment','segments.segment')
                # ---- Strip out segment list from the strerr log of ligo_data_find
                gaps_segments_str = 'segments.segmentlist(' + gaps_str[ gaps_str.index('[') : gaps_str.index(']')+1 ] + ')'
                # ---- Read gap segments into segment list
                gaps_segments = eval(gaps_segments_str)
                gaps_segments.coalesce()
                # ---- Need to write out these gap segments so we can read them
                #      in again as ScienceData object, we really should choose
                #      one type of segment object and use it everywhere.
                f = open("gaps.txt","w"); segmentsUtils.tosegwizard(f,gaps_segments); f.flush(); f.close()
            elif datafind_exec=='gw_data_find':
                # ---- File is of the form
                #        Missing segments:
                #        866386702.000000 866393054.000000
                #        866396828.000000 866404754.000000
                #        866410903.000000 866412664.000000
                #        866413722.000000 866415440.000000
                #        866477785.000000 866488107.000000
                #      Use awk to turn this into a file with the segwizard format of gaps.txt generated above.
                awkcmdfile_str = '{ if (NR==1) print \"# seg start stop duration\"; else if ($2==int($2)) print NR-2, int($1), int($2), int($2)-int($1); else print NR-2, int($1), int($2)+1, int($2)-int($1)+1}'
                awkfile = open('./awkfile','w')
                awkfile.write(awkcmdfile_str)
                awkfile.close()
                os.system('awk -f awkfile ligo_data_find.err > gaps.txt')
                os.system('rm awkfile')
            else :
                print("Error: Do not know how to process gaps file produced by " + datafind_exec, file=sys.stderr)
                sys.exit(17)
            # ---- Need to write out these gap segments so we can read them
            #      in again as ScienceData object, we really should choose
            #      one type of segment object and use it everywhere.
            gaps_segments = pipeline.ScienceData()
            gaps_segments.read( 'gaps.txt', 0 )
            os.system('rm gaps.txt')
            # ---- Identify any overlap between missing frames and full_segment_list
            gaps_segments.intersection(full_segment_list[i])
            if gaps_segments:
                print("Error: missing frames in ", detector[i], file=sys.stderr)
                for jj in range(gaps_segments.__len__()):
                    time_range_string = str(jj) + ' ' + \
                        str(gaps_segments.__getitem__(jj).start()) \
                        + ' ' + str(gaps_segments.__getitem__(jj).end())  \
                        + ' ' + str(gaps_segments.__getitem__(jj).end() \
                        -gaps_segments.__getitem__(jj).start())
                    print(time_range_string)
                sys.exit(1)

    # ---- Clean up.
    os.system('rm -f framecache_temp.txt lalcache.txt')
    # ---- Set dataFrameCache variable to point to our new file.
    dataFrameCache = frameCacheAll
    # ---- Status message.
    print("... finished writing framecache file for ifo data", file=sys.stdout)
    print(file=sys.stdout)

# -------------------------------------------------------------------------
#      Write injection files for on-the-fly simulations, if needed.
# -------------------------------------------------------------------------

if cp.has_section('waveforms') :
    # ---- Read [injection] parameters.
    waveform_set = cp.options('waveforms')
    injectionInterval = cp.get('injection','injectionInterval')
    #print >> sys.stdout, "    waveform_set:", waveform_set
    print("Making injection files ...", file=sys.stdout)
    for set in waveform_set :
        waveforms = cp.get('waveforms',set)
        baseinjfilename = "injection_" + set + ".txt"
        injfilename = "input/injection_" + set + ".txt"
        print("    waveforms:  ", waveforms, file=sys.stdout)
        print("    injfilename:", injfilename, file=sys.stdout)
        if waveforms == baseinjfilename :
            command = ' '.join(["cp ", baseinjfilename, " input/"])
            os.system(command)
            print("    Using-premade injection file:", injfilename, file=sys.stdout)
        else :
            timeSegmentsString = str(on_source_start_time + transientTime) + "~" + str(on_source_end_time - transientTime)
            timeOffsetString = "0"
            # ---- Wrap waveforms string in single quotes to escape any ; characters,
            #      which are used for generating random injection parameters.
            tmpwaveforms = ''.join(["'", waveforms, "'"])
            # ---- Construct command to make injection file for this waveform set.
            make_injection_file_command = ' '.join(["xmakegrbinjectionfile",
            injfilename,
            tmpwaveforms,
            timeSegmentsString,
            timeOffsetString,
            injectionInterval,
            'NaN',
            'NaN',
            str(seed) ])
            # ---- Issue command to make injection file for this waveform set.
            # ---- We overwrite the original file with jittered versions if desired.
            print("    Writing :", injfilename, file=sys.stdout)
            print("    command :", make_injection_file_command, file=sys.stdout)
            os.system(make_injection_file_command)

            # ---- Apply calib uncertainties to injections if required.
            if int(cp.get('injection','miscalibrateInjections'))==1:
                miscalib_injection_command = ' '.join(["xmiscalibrategrbinjectionfile",
                injfilename,
                injfilename,
                detectorStrTilde,
                frameTypeStrTilde,
                '0' ])
                print("    Miscalibrating :", injfilename, file=sys.stdout)
                print(miscalib_injection_command, file=sys.stdout)
                os.system(miscalib_injection_command)

    print("... finished making injection files.", file=sys.stdout)
    print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Define special job classes.
# -------------------------------------------------------------------------

class XsearchJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An x search job
    """
    def __init__(self,cp):
        """
        cp = ConfigParser object from which options are read.
        """
        # ---- Get path to executable.
        os.system('which xdetection > path_file.txt')
        f = open('path_file.txt','r')
        xdetectionstr = f.read()
        f.close()
        os.system('rm path_file.txt')
        self.__executable = xdetectionstr

        # ---- Get condor universe from parameters file.
        self.__universe = cp.get('condor','universe')
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)
        self.__param_file = None

        # ---- Add required environment variables.
        if 'raven' in hostname:
            MCR_CACHE_PATH = '/tmp/'
        elif 'ldas' in hostname:
            MCR_CACHE_PATH = '/local/'
        else:
            print('Warning : ' + hostname + ' does not have a specific MCR_CACHE_ROOT setup, using default')
            MCR_CACHE_PATH = '/local/'
        self.add_condor_cmd('environment',"USER=$ENV(USER);HOME=$ENV(HOME);" \
            "LD_LIBRARY_PATH=$ENV(LD_LIBRARY_PATH);" \
            "XPIPE_INSTALL_BIN=$ENV(XPIPE_INSTALL_BIN);" \
            "PATH=/usr/bin:/bin;" \
            "MCR_CACHE_ROOT="+MCR_CACHE_PATH+"$ENV(USER)")

        # ---- Add accounting group flag.
        grouptag = 'ligo.' + \
                cp.get('condor','ProdDevSim') + '.' +\
                cp.get('condor','Era') + '.' +\
                cp.get('condor','Group') + '.' +\
                cp.get('condor','SearchType')
        self.add_condor_cmd('accounting_group',grouptag)

        # ---- Add username flag.
        self.add_condor_cmd('accounting_group_user',cp.get('condor','UserName'))

        # ---- Add priority specification.
        self.add_condor_cmd('priority',condorPriority)

        # ---- Add minimal memory request.
        self.add_condor_cmd('request_memory',minimalMem)

        # ---- Add mandatory request_disk request.
        minimalDiskSearch = "10M"
        self.add_condor_cmd('request_disk',minimalDiskSearch)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xsearch-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xsearch-$(cluster)-$(process).err')

        # If on Atlas, use getenv=true to pass variables
        if atlasFlag:
            self.add_condor_cmd('getenv',"true")

        # ---- Name of condor job submission file to be written.
        self.set_sub_file('xsearch.sub')

class XsearchNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    xsearch node
    """
    def __init__(self,job):
        """
        job = A CondorDAGJob.
        """
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)
        self.__x_jobnum = None
        self.__x_injnum = None

    # ---- Set parameters file.
    def set_param_file(self,path):
        self.add_var_arg(path)
        self.__param_file = path

    def get_param_file(self):
        return self.__param_file

    def set_x_jobnum(self,n):
        self.add_var_arg(str(n))
        self.__x_jobnum = n

    def get_x_jobnum(self):
        return self.__x_jobnum

    def set_output_dir(self,path):
        self.add_var_arg(path)
        self.__output_dir = path

    def get_output_dir(self,path):
        return self.__output_dir

    def set_x_injnum(self,n):
        self.add_var_arg(n)
        self.__x_injnum = n

    def get_x_injnum(self):
        return self.__x_injnum

class XmergeJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An x merge job
    """
    def __init__(self,cp):
        """
        cp = ConfigParser object from which options are read.
        """
        # ---- Get path to executable.
        os.system('which xmergegrbresults > path_file.txt')
        f = open('path_file.txt','r')
        xmergestr = f.read()
        f.close()
        os.system('rm path_file.txt')
        self.__executable = xmergestr

        # ---- Get condor universe from parameters file.
        self.__universe = cp.get('condor','universe')
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)

        # ---- Add name of 'output' directory as first argument.
        self.add_arg('output')

        # ---- Add required environment variables.
        self.add_condor_cmd('environment',"USER=$ENV(USER);HOME=$ENV(HOME);" \
            "LD_LIBRARY_PATH=$ENV(LD_LIBRARY_PATH)")

        # ---- Add accounting group flag.
        grouptag = 'ligo.' + \
                cp.get('condor','ProdDevSim') + '.' +\
                cp.get('condor','Era') + '.' +\
                cp.get('condor','Group') + '.' +\
                cp.get('condor','SearchType')
        self.add_condor_cmd('accounting_group',grouptag)

        # ---- Add username flag.
        self.add_condor_cmd('accounting_group_user',cp.get('condor','UserName'))

        # ---- Add priority specification.
        self.add_condor_cmd('priority',condorPriority)

        # ---- Add minimal memory request. Note that with current default minimalMem
        #      we never access the "else" statement below.
        if minimalMem :
            self.add_condor_cmd('request_memory',minimalMem)
        else :
            minimalMemMerge = "1500"
            self.add_condor_cmd('request_memory',minimalMemMerge)

        # ---- Add mandatory request_disk request.
        minimalDiskMerge = "10G"
        self.add_condor_cmd('request_disk',minimalDiskMerge)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xmerge-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xmerge-$(cluster)-$(process).err')

        # If on Atlas
        if atlasFlag:
            self.add_condor_cmd('getenv',"true")

        # ---- Name of condor job submission file to be written.
        self.set_sub_file('xmerge.sub')

class XmergeNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    merge results that were cut into blocks of less than maxInjNum jobs
    """
    def __init__(self,job):
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)

    def set_dir_prefix(self,path):
        self.add_var_arg(path)
        self.__dir_prefix = path

    def get_dir_prefix(self):
        return self.__dir_prefix

    def set_sn_flag(self,path):
        self.add_var_arg(path)
        self.__sn_flag = path

    def get_sn_flag(self):
        return self.__sn_flag

class XmergeClusteredJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An x merge clustered job
    """
    def __init__(self,cp,nDet):
        """
        cp = ConfigParser object from which options are read.
        """
        # ---- Get path to executable.
        if nDet==3:
            os.system('which xmergegrbclusteredresults > path_file.txt')
        else:
            os.system('which xmergegrbclusteredresultsTwoDets > path_file.txt')
        f = open('path_file.txt','r')
        xmergestr = f.read()
        f.close()
        os.system('rm path_file.txt')
        self.__executable = xmergestr

        # ---- Get condor universe from parameters file.
        self.__universe = cp.get('condor','universe')
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)

        # ---- Add name of 'output' directory as first argument.
        self.add_arg('output_clustered')

        # ---- Add required environment variables.
        self.add_condor_cmd('environment',"USER=$ENV(USER);HOME=$ENV(HOME);" \
            "LD_LIBRARY_PATH=$ENV(LD_LIBRARY_PATH)")

        # ---- Add priority specification.
        self.add_condor_cmd('priority',condorPriority)

        # ---- Add minimal memory request. Note that with current default minimalMem
        #      we never access the "else" statement below.
        if minimalMem :
            self.add_condor_cmd('request_memory',minimalMem)
        else :
            minimalMemMerge = "1500"
            self.add_condor_cmd('request_memory',minimalMemMerge)

        # ---- Add mandatory request_disk request.
        minimalDiskMerge = "10G"
        self.add_condor_cmd('request_disk',minimalDiskMerge)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xmergeclustered-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xmergeclustered-$(cluster)-$(process).err')

        # If on Atlas
        if atlasFlag:
            self.add_condor_cmd('getenv',"true")

        # ---- Name of condor job submission file to be written.
        self.set_sub_file('xmergeclustered.sub')

class XmergeClusteredNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    cluster and merge results that were cut into blocks of less than maxInjNum jobs
    """
    def __init__(self,job):
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)

    def set_dir_prefix(self,path):
        self.add_var_arg(path)
        self.__dir_prefix = path

    def get_dir_prefix(self):
        return self.__dir_prefix

    def set_sn_flag(self,path):
        self.add_var_arg(path)
        self.__sn_flag = path

    def get_sn_flag(self):
        return self.__sn_flag

    def set_sc_flag(self,path):
        self.add_var_arg(path)
        self.__sc_flag = path

    def get_sc_flag(self):
        return self.__sc_flag

# -------------------------------------------------------------------------
#    Preparations for writing dags.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing job submission files ... ", file=sys.stdout)

# ---- Retrieve job retry number from parameters file.
if cp.has_option('condor','retryNumber'):
    retryNumber = int(cp.get('condor','retryNumber'))
else :
    retryNumber = 0
if smartCluster == True:
    retryNumberClustered = 0
# ---- DAGman log file.
#      The path to the log file for condor log messages. DAGman reads this
#      file to find the state of the condor jobs that it is watching. It
#      must be on a local file system (not in your home directory) as file
#      locking does not work on a network file system.
log_file_on_source = cp.get('condor','dagman_log_on_source')
log_file_off_source = cp.get('condor','dagman_log_off_source')
if smartCluster == True:
    log_file_on_source_clustered = cp.get('condor','dagman_log_on_source')+'_clustered'
    log_file_off_source_clustered = cp.get('condor','dagman_log_off_source')+'_clustered'
if cp.has_section('waveforms') :
    log_file_simulations = cp.get('condor','dagman_log_simulations')
    if smartCluster == True:
        log_file_simulations_clustered = cp.get('condor','dagman_log_simulations')+'_clustered'
if cp.has_option('mdc','mdc_sets') :
    log_file_mdcs = cp.get('condor','dagman_log_mdcs')
    if smartCluster == True:
        log_file_mdcs_clustered = cp.get('condor','dagman_log_mdcs')+'_clustered'

# ---- Make directories to store the log files and error messages
#      from the nodes in the DAG
try: os.mkdir( 'logs' )
except: pass
# ---- Make directory to store the output of our test job.
#      NOT TO BE DONE FOR PRODUCTION RUNS!
try: os.mkdir( 'output' )
except: pass
try: os.mkdir( 'output/on_source' )
except: pass
try: os.mkdir( 'output/off_source' )
except: pass
# ---- now make the same directories only for clustered results.
if smartCluster == True:
    try: os.mkdir( 'output_clustered' )
    except: pass
    try: os.mkdir( 'output_clustered/on_source' )
    except: pass
    # ---- offsource will be divided in ten chunks.
    try: os.mkdir( 'output_clustered/off_source_1' )
    except: pass
    try: os.mkdir( 'output_clustered/off_source_2' )
    except: pass
    try: os.mkdir( 'output_clustered/off_source_3' )
    except: pass
    try: os.mkdir( 'output_clustered/off_source_4' )
    except: pass
    try: os.mkdir( 'output_clustered/off_source_5' )
    except: pass
    try: os.mkdir( 'output_clustered/off_source_6' )
    except: pass
    try: os.mkdir( 'output_clustered/off_source_7' )
    except: pass
    try: os.mkdir( 'output_clustered/off_source_8' )
    except: pass
    try: os.mkdir( 'output_clustered/off_source_9' )
    except: pass
    try: os.mkdir( 'output_clustered/off_source_10' )
    except: pass

# ---- Make directories to hold results of injection runs - one for each
#      waveform and injection scale.
if cp.has_section('waveforms') :
    waveform_set = cp.options('waveforms')
    for set in waveform_set :
        # ---- The user can specify different injection scales for each
        #      waveform set by including a section with that name in the ini
        #      file.  Look for one.  If no special section for this waveform,
        #      then use the default injection scales from [injection].
        if cp.has_section(set) & cp.has_option(set,'injectionScales') :
            injectionScalesList = cp.get(set,'injectionScales')
            injectionScales = injectionScalesList.split(',')
        else :
            injectionScalesList = cp.get('injection','injectionScales')
            injectionScales = injectionScalesList.split(',')
        scale_counter = 0
        for injectionScale in injectionScales :
            try: os.mkdir( 'output/simulations_' + set + '_' + str(scale_counter) )
            except: pass
            if smartCluster == True:
                try: os.mkdir( 'output_clustered/simulations_' + set + '_' + str(scale_counter) )
                except: pass
            scale_counter = scale_counter + 1
if cp.has_option('mdc','mdc_sets') :
    mdc_setsList = cp.get('mdc','mdc_sets')
    mdc_sets = mdc_setsList.split(',')
    for set in mdc_sets :
        # ---- The user can specify different injection scales for each
        #      waveform set by including a section with that name in the ini
        #      file.  Look for one.  If no special section for this waveform,
        #      then use the default injection scales from [injection].
        if cp.has_section(set) & cp.has_option(set,'injectionScales') :
            injectionScalesList = cp.get(set,'injectionScales')
            injectionScales = injectionScalesList.split(',')
        else :
            injectionScalesList = cp.get('injection','injectionScales')
            injectionScales = injectionScalesList.split(',')
        scale_counter = 0
        for injectionScale in injectionScales :
            try: os.mkdir( 'output/simulations_' + set + '_' + str(scale_counter) )
            except: pass
            if smartCluster == True:
                try: os.mkdir( 'output_clustered/simulations_' + set + '_' + str(scale_counter) )
                except: pass
            scale_counter = scale_counter + 1


# -------------------------------------------------------------------------
#      Write on-source dag.
# -------------------------------------------------------------------------
print("Writing on-source dag ... ", file=sys.stdout)
# ---- Create a dag to which we can add jobs.
dag = pipeline.CondorDAG(log_file_on_source + uuidtag)

# ---- Set the name of the file that will contain the DAG.
dag.set_dag_file( 'grb_on_source' )

# ---- Make instance of XsearchJob.
job = XsearchJob(cp)

# ---- Make instance of XmergeJob.
mergejob = XmergeJob(cp)
# ---- Put a single merge job node in mergejob.  This job will combine
#      into a single file the output of all of the off-source analysis jobs.
mergenode = XmergeNode(mergejob)
# ---- if clustering.
if smartCluster == True:
    print("Writing on-source clustering dag ... ", file=sys.stdout)
    print(log_file_on_source_clustered)
    dag_clustered = pipeline.CondorDAG(log_file_on_source_clustered + uuidtag)
    dag_clustered.set_dag_file( 'grb_on_source_clustered' )
    mergeclusteredjob = XmergeClusteredJob(cp,len(detector))
    mergeclusterednode = XmergeClusteredNode(mergeclusteredjob)

# ---- Make analysis jobs for all segments (currently 1) in the on-source
#      segment list.
segmentList = pipeline.ScienceData()
segmentList.read( 'input/segment_on_source.txt' , blockTime )

# ---- Read the number of chunks from the event list
event_on_file = open('input/event_on_source.txt')
event_on_list = event_on_file.readlines()
nOnEvents = len(event_on_list)
event_on_file.close()

# ---- Read from the parameters file how job results files are to be
#      distributed across nodes, and write that info to a file that
#      will be used by the post-processing scripts.
distributeOnSource = int(cp.get('output','distributeOnSource'))
if distributeOnSource == 1:
    jobNodeFileOnSource = cp.get('output','jobNodeFileOnSource')
    nodeList = open( jobNodeFileOnSource,'w')
    nodeList.write("Number_of_detectors %d\n"%(len(detector)))
    for i in range(0,len(detector)) :
        nodeList.write("%s\t"%(detector[i]))
    nodeList.write("\n")
    nodeList.write('event_on_source_file ')
    nodeList.write(cwdstr + "/input/event_on_source.txt \n")
    nodeList.write("Injection_file N/A \n")
    nodeList.write('Number_of_jobs ')
    nodeList.write("%d \n"%(len(segmentList)))
    nodeList.write('Number_of_jobs_per_node ')
    nodeList.write("%d \n"%(len(segmentList)))
    nodeList.write(cwdstr + " \n")

# ---- Add one node to the job for each segment to be analysed.
for i in range(len(segmentList)):
    node = XsearchNode(job)
    # ---- Parameters file:
    matlab_param_file = cwdstr + "/input/parameters_on_source_0.txt"
    node.set_param_file(matlab_param_file)
    node.set_x_jobnum(i)
    if distributeOnSource == 1 :
        nodeList.write(cwdstr + "/output/on_source" + "/results_%d.mat \n"%(i))
    # ---- On-source results files are always written to the local
    #      output/on_source directory - there are very few such jobs.
    node.set_output_dir( os.path.join( cwdstr + "/output/on_source" ) )
    node.set_x_injnum('0')
    mergenode.add_parent(node)
    node.set_retry(retryNumber)
    # ---- Prepend human readable description to node name.
    node.set_name("xdetection_on_source_seg" + str(i) + "_" + node.get_name())
    dag.add_node(node)

# ---- Supply remaining XmergeJob node parameters, add job to the dag.
mergenode.set_dir_prefix("on_source/")
mergenode.set_sn_flag("1 " + mergingCutsString)
mergenode.set_retry(retryNumber)
# ---- Prepend human readable description to node name.
mergenode.set_name("xmerge_on_source_" + mergenode.get_name())
dag.add_node(mergenode)
# ---- do the same if clustering.
if smartCluster == True:
    mergeclusterednode.set_dir_prefix("on_source/")
    mergeclusterednode.set_sn_flag("1 " + mergingCutsString)
    mergeclusterednode.set_sc_flag("1")
    mergeclusterednode.set_retry(retryNumberClustered)
    mergeclusterednode.set_name("xmergeclustered_on_source_" + mergeclusterednode.get_name())
    dag_clustered.add_node(mergeclusterednode)


# ---- Write out the submit files needed by condor.
dag.write_sub_files()
# ---- Write out the DAG itself.
dag.write_dag()
# ---- Delete used dag job
del dag
del job
del mergejob
if smartCluster == True:
    dag_clustered.write_sub_files()
    dag_clustered.write_dag()
    del dag_clustered
    del mergeclusteredjob
# --- close on source distribute file
if distributeOnSource == 1 :
    nodeList.close()

# -------------------------------------------------------------------------
#      Write off-source dag.
# -------------------------------------------------------------------------
print("Writing off-source dag ... ", file=sys.stdout)
# ---- Create a dag to which we can add jobs.
dag = pipeline.CondorDAG(log_file_off_source + uuidtag)

# ---- Set the name of the file that will contain the DAG.
dag.set_dag_file( 'grb_off_source' )

# ---- Make instance of XsearchJob.
job = XsearchJob(cp)

# ---- Make instance of XmergeJob.
mergejob = XmergeJob(cp)
# ---- Put a single merge job node in mergejob.  This job will combine
#      into a single file the output of all of the off-source analysis jobs.
mergenode = XmergeNode(mergejob)

# ---- Load off-source segments.
#segmentList = pipeline.ScienceData()
#segmentList.read( 'input/segment_off_source.txt', blockTime )

# ---- KLUDGE FIX: now we have option of limiting number of
#      background jobs using ini file we should ensure that
#      we only run the correct number of jobs.
event_off_file = open('input/event_off_source.txt')
nOffEvents = len(event_off_file.readlines())
event_off_file.close()
# ---- END OF KLUDGE FXI

# ---- Read how the user wants the results distributed (over nodes, or not)
#      and record this info to a file.
distributeOffSource = int(cp.get('output','distributeOffSource'))
if distributeOffSource == 1 :
    nodePath = cp.get('output','nodePath')
    onNodeOffSourcePath = cp.get('output','onNodeOffSourcePath')
    nNodes = int(cp.get('output','nNodes'))
    jobNodeFileOffSource = cp.get('output','jobNodeFileOffSource')
    nodeList = open( jobNodeFileOffSource,'w')
    nodeList.write("Number_of_detectors %d\n"%(len(detector)))
    for i in range(0,len(detector)) :
        nodeList.write("%s\t"%(detector[i]))
    nodeList.write("\n")
    nodeList.write('event_off_source_file ')
    nodeList.write(cwdstr + "/input/event_off_source.txt \n")
    nodeList.write("Injection_file N/A \n")
    nodeList.write('Number_of_jobs ')
    nodeList.write("%d \n"%(nOffEvents))
    nJobsPerNode = int(nOffEvents/nNodes) + 1
    nodeList.write('Number_of_jobs_per_node ')
    nodeList.write("%d \n"%(nJobsPerNode))
    nodeOffset = int(cp.get('output','numberOfFirstNode'));

# ---- Check how many segments are to be bundled into each job, and create
#      job nodes accordingly.
maxInjNum = int(cp.get('output','maxInjNum'))
if cp.has_option('output','maxOffNum'):
    maxOffNum = int(cp.get('output','maxOffNum'))
else :
    maxOffNum = maxInjNum

if maxOffNum == 0 :
    # ---- Each segment is to be analysed as a separate job.  There may be
    #      a lot of them, so we will check below if the output files are to
    #      be distributed over the cluster nodes.
    for i in range(nOffEvents):
        node = XsearchNode(job)
        matlab_param_file = cwdstr + "/input/parameters_off_source.txt"
        node.set_param_file(matlab_param_file)
        node.set_x_jobnum(i)
        if distributeOffSource == 1 :
            # ---- Write output result file to a cluster node.
            # ---- Record path to each output result file.  Write results from
            #      nJobsPerNode jobs on each node.
            jobNumber = int(i/nJobsPerNode) + nodeOffset
            while ~(os.path.isdir (nodePath + "%d/"%(jobNumber))) &1:
                print("Waiting for automount to unmount ...\n", file=sys.stdout)
                time.sleep(10)
            # ---- Write name of node before changing nodes.
            if 0 == i % nJobsPerNode:
                print("Node number %d"%(jobNumber), file=sys.stdout)
                nodeList.write(nodePath + "%d/ \n"%(jobNumber)  )
                # ---- Create directory for results files.
                fullPath = nodePath + "%d/"%(jobNumber) + \
                    onNodeOffSourcePath + "/off_source"
                if ~(os.path.isdir (fullPath))& 1 :
                    os.makedirs(fullPath)
                else :
                    print("**WARNING** path: " + fullPath \
                        + " already exists, previous results may be overwritten\n", file=sys.stdout)

            node.set_output_dir(os.path.join( fullPath))
            nodeList.write(fullPath + "/results_%d.mat \n"%(i) )
        else :
            # ---- Output file to local output/off_source directory.
            node.set_output_dir( os.path.join( cwdstr + "/output/off_source" ) )
        node.set_x_injnum('0')
        mergenode.add_parent(node)
        node.set_retry(retryNumber)
        # ---- Prepend human readable description to node name.
        node.set_name("xdetection_off_source_seg" + str(i) + "_" + node.get_name())
        dag.add_node(node)
else :
    # ---- This option bundles together segments so that maxOffNum segments
    #      are analysed by each condor job node.
    #      In this case all output files come back to the local output/off_source
    #      directory.
    nSets = int(math.ceil(float(nOffEvents)/float(maxOffNum)))
    for i in range(nSets) :
        node = XsearchNode(job)
        matlab_param_file = cwdstr + "/input/parameters_off_source_0.txt"
        node.set_param_file(matlab_param_file)
        node.set_x_jobnum("%d-"%(i*maxOffNum) + "%d"%(min((i+1)*maxOffNum-1,nOffEvents-1)))
        node.set_output_dir( os.path.join( cwdstr + "/output/off_source" ) )
        node.set_x_injnum('0')
        mergenode.add_parent(node)
        node.set_retry(retryNumber)
        # ---- Prepend human readable description to node name.
        node.set_name("xdetection_off_source_seg" + str(i) + "_" + node.get_name())
        dag.add_node(node)

# ---- Supply remaining XmergeJob node parameters, add job to the dag.
mergenode.set_dir_prefix("off_source/")
mergenode.set_sn_flag("1 " + mergingCutsString)
mergenode.set_retry(retryNumber)
# ---- Prepend human readable description to node name.
mergenode.set_name("xmerge_off_source_" + mergenode.get_name())
dag.add_node(mergenode)
# ---- do the same if clustering.
if smartCluster == True:
    print("Writing off-source clustered dag ... ", file=sys.stdout)
    dag_clustered = pipeline.CondorDAG(log_file_off_source_clustered + uuidtag)
    dag_clustered.set_dag_file( 'grb_off_source_clustered' )
    for p in range(1,11):
        mergeclusteredjob = XmergeClusteredJob(cp,len(detector))
        mergeclusterednode = XmergeClusteredNode(mergeclusteredjob)
        mergeclusterednode.set_dir_prefix("off_source_%i/" % p)
        mergeclusterednode.set_sn_flag("1 " + mergingCutsString)
        mergeclusterednode.set_sc_flag("1")
        mergeclusterednode.set_retry(retryNumberClustered)
        mergeclusterednode.set_name("xmergeclustered_off_source_%i" % p + mergeclusterednode.get_name())
        dag_clustered.add_node(mergeclusterednode)

# ---- Write out the submit files needed by condor.
dag.write_sub_files()
# ---- Write out the DAG itself.
dag.write_dag()
# ---- Delete used dag and jobs
del dag
del job
del mergejob
# --- Close file recording distribution of off-source results.
if distributeOffSource == 1 :
    nodeList.close()
if smartCluster == True:
    dag_clustered.write_sub_files()
    dag_clustered.write_dag()
    del dag_clustered
    del mergeclusteredjob

# -------------------------------------------------------------------------
#      Write on-the-fly simulations dags - one for each waveform set.
# -------------------------------------------------------------------------

# ---- All injection scales for a given waveform set will be handled by a
#      single dag.

# ---- Check for on-the-fly injection sets.
if cp.has_section('waveforms') :

    # ---- Status message.
    print("Writing on-the-fly injection dags ... ", file=sys.stdout)

    # ---- Read [injection] parameters.
    waveform_set = cp.options('waveforms')

    # ---- KLUDGE: Systematically remove these variables to simplify code.
    # ---- Read how distribute results on node and write to file
    distributeSimulation = int(cp.get('output','distributeSimulation'))
    maxInjNum = int(cp.get('output','maxInjNum'))
    if distributeSimulation == 1 :
        print('Error: use of distributeSimulation disabled.', file=sys.stderr)
        sys.exit(1)
    if maxInjNum == 0 :
        print('Error: use of maxInjNum == 0 disabled.', file=sys.stderr)
        sys.exit(1)

    # ---- Write one dag for each waveform set.
    for set in waveform_set :

        print('Writing dag for on-the-fly injection set: ' + set, file=sys.stdout)

        # ---- The user can specify different injection scales for each
        #      waveform set by including a section with that name in the ini
        #      file.  Look for one.  If no special section for this waveform,
        #      then use the default injection scales from [injection].
        if cp.has_section(set) & cp.has_option(set,'injectionScales') :
            injectionScalesList = cp.get(set,'injectionScales')
            injectionScales = injectionScalesList.split(',')
        else :
            injectionScalesList = cp.get('injection','injectionScales')
            injectionScales = injectionScalesList.split(',')

        # ---- Create a dag to which we can add jobs.
        dag = pipeline.CondorDAG( log_file_simulations + "_" + set + uuidtag )

        # ---- Set the name of the file that will contain the DAG.
        dag.set_dag_file( "grb_simulations_" + set )

        # ---- Make instance of XsearchJob.
        job = XsearchJob(cp)

        # ---- Make instance of XmergeJob.  This job will merge the results files
        #      produced by the several analysis nodes.
        mergejob = XmergeJob(cp)
        if smartCluster == True:
            print('Writing dag for on-the-fly clustered injection set: ' + set, file=sys.stdout)
            dag_clustered = pipeline.CondorDAG( log_file_simulations_clustered + "_" + set + uuidtag )
            dag_clustered.set_dag_file( "grb_simulations_clustered_" + set )
            mergeclusteredjob = XmergeClusteredJob(cp,len(detector))
        # ---- Read segment list from file.
        segmentList = pipeline.ScienceData()
        segmentList.read( 'input/segment_on_source.txt' , blockTime )

        # ---- Read injection file to determine number of injections for this
        #      set. Use system call to wc to figure out how many lines are in
        #      the injection file.
        os.system('wc input/injection_' + set + \
            '.txt | awk \'{print $1}\' > input/' + set + '.txt' )
        f = open('input/' + set + '.txt')
        numberOfInjections = int(f.readline())
        f.close()
        os.system('cat input/injection_' + set + \
                  '.txt | awk \'{printf \"%.9f\\n\",$1+$2*1e-9}\'' + \
                  ' > input/gps' + set + '.txt' )
        f = open('input/gps' + set + '.txt')
        injection_list_time = f.readlines()
        f.close()

        # ---- Initialize merge node for each injection scale.
        mergenode = []
        if smartCluster == True:
            mergeclusterednode = []
        for injectionScale in injectionScales :
            mergenode.append(XmergeNode(mergejob))
            if smartCluster == True:
                mergeclusterednode.append(XmergeClusteredNode(mergeclusteredjob))
        # ---- Skip injection trigger production if path to already available
        #      triggers provided.
        if not(reUseInj):
            # ---- Set buffer for handling long injections.
            longInjSegBuffer = 0
            if longInjections :
                if set.startswith('adi-a') :
                    longInjSegBuffer = 20
                if set.startswith('adi-b') :
                    longInjSegBuffer = 6
                if set.startswith('adi-c') :
                    longInjSegBuffer = 132
                if set.startswith('adi-d') :
                    longInjSegBuffer = 74
                if set.startswith('adi-e') :
                    longInjSegBuffer = 43
                if set.startswith('mva') :
                    longInjSegBuffer = 132
                if set.startswith('ebbh-a') :
                    longInjSegBuffer = 7
                if set.startswith('ebbh-d') :
                    longInjSegBuffer = 4
                if set.startswith('ebbh-e') :
                    longInjSegBuffer = 4
                if set.startswith('mono-a') :
                    longInjSegBuffer = 75
                if set.startswith('mono-b') :
                    longInjSegBuffer = 125
            # ---- Loop over segments.
            for iInj in range(numberOfInjections) :
                thisInjSeg = []
                for i in range(len(segmentList)):
                    if abs(float(event_on_list[i])-float(injection_list_time[iInj]))<=blockTime/2-transientTime+longInjSegBuffer :
                        thisInjSeg.append(i)
                # ---- Check if this injection falls into any segment. If so, add a node to analyse it.
                if thisInjSeg :
                    # ---- Sort segments.
                    thisInjSeg.sort()
                    # ---- Add node to job.
                    node = XsearchNode(job)
                    matlab_param_file = cwdstr + "/input/parameters_simulation_" + set + "_0_0.txt"
                    node.set_param_file(matlab_param_file)
                    node.set_x_jobnum("%d-"%(thisInjSeg[0]) + "%d"%(thisInjSeg[-1]))
                    node.set_output_dir( os.path.join( cwdstr + '/output/simulations_' + set + '_' ) )
                    node.set_x_injnum(str(iInj+1))
                    for scale_counter in range(len(injectionScales)) :
                        mergenode[scale_counter].add_parent(node)
                    node.set_retry(retryNumber)
                    # ---- Prepend human readable description to node name.
                    node.set_name("xdetection_simulation_" + set + "_inj" + str(iInj+1) + "_" + node.get_name())
                    # ---- Add job node to the dag.
                    dag.add_node(node)

        # ---- Loop over injection scales.
        for scale_counter in range(len(injectionScales)) :
            # ---- Point to already produced injection results if provided.
            if reUseInj:
                mergenode[scale_counter].set_dir_prefix(mergingCutsPath + '/../output/simulations_' \
                    + set + '_' + str(scale_counter) + '/')
            else :
                mergenode[scale_counter].set_dir_prefix('simulations_' + set + '_' + str(scale_counter) + '/')
            mergenode[scale_counter].set_sn_flag("1 " + mergingCutsString)
            mergenode[scale_counter].set_retry(retryNumber)
            # ---- Prepend human readable description to node name.
            mergenode[scale_counter].set_name("xmerge_simulation_" + set + "_injScale" \
                + str(scale_counter) + "_" + mergenode[scale_counter].get_name())
            dag.add_node(mergenode[scale_counter])
            if smartCluster == True:
                # ---- Point to already produced injection results if provided.
                if reUseInj:
                    mergeclusterednode[scale_counter].set_dir_prefix(mergingCutsPath + '/../output/simulations_' \
                        + set + '_' + str(scale_counter) + '/')
                else :
                    mergeclusterednode[scale_counter].set_dir_prefix('simulations_' + set + '_' + str(scale_counter) + '/')
                mergeclusterednode[scale_counter].set_sn_flag("1 " + mergingCutsString)
                mergeclusterednode[scale_counter].set_sc_flag("1")
                mergeclusterednode[scale_counter].set_retry(retryNumberClustered)
                # ---- Prepend human readable description to node name.
                mergeclusterednode[scale_counter].set_name("xmergeclustered_simulation_" + set + "_injScale" \
                    + str(scale_counter) + "_" + mergeclusterednode[scale_counter].get_name())
                dag_clustered.add_node(mergeclusterednode[scale_counter])

        # ---- Write out the submit files needed by condor.
        dag.write_sub_files()
        # ---- Write out the DAG itself.
        dag.write_dag()

        # ---- Delete used dag job
        del dag
        del job
        del mergejob
        if smartCluster == True:
            dag_clustered.write_sub_files()
            dag_clustered.write_dag()
            del dag_clustered
            del mergeclusteredjob

# -------------------------------------------------------------------------
#      Write MDC simulation dags - one for each MDC set.
# -------------------------------------------------------------------------

# ---- All injection scales for a given MDC set will be handled by a
#      single dag.

# ---- Check for MDC sets.
if cp.has_option('mdc','mdc_sets') :

    # ---- Status message.
    print("Writing MDC dags ... ", file=sys.stdout)

    # ---- Get list of MDC sets to process.
    mdc_setsList = cp.get('mdc','mdc_sets')
    mdc_sets = mdc_setsList.split(',')

    # ---- KLUDGE: Systematically remove these variables to simplify code.
    # ---- Read how distribute results on node and write to file
    distributeSimulation = int(cp.get('output','distributeSimulation'))
    if distributeSimulation == 1 :
        print('Error: use of distributeSimulation disabled.', file=sys.stderr)
        sys.exit(1)
    # ---- Check how many injections are to be bundled into each xdetection job.
    maxInjNum = int(cp.get('output','maxInjNum'))
    if maxInjNum == 0 :
        print('Error: use of maxInjNum == 0 disabled.', file=sys.stderr)
        sys.exit(1)

    # ---- Write one dag for each waveform set.
    for set in mdc_sets :

        print('Writing dag for MDC injection set: ' + set, file=sys.stdout)

        # ---- The user can specify different injection scales for each
        #      waveform set by including a section with that name in the ini
        #      file.  Look for one.  If no special section for this waveform,
        #      then use the default injection scales from [injection].
        if cp.has_section(set) & cp.has_option(set,'injectionScales') :
            injectionScalesList = cp.get(set,'injectionScales')
            injectionScales = injectionScalesList.split(',')
        else :
            injectionScalesList = cp.get('injection','injectionScales')
            injectionScales = injectionScalesList.split(',')

        # ---- Create a dag to which we can add jobs.
        dag = pipeline.CondorDAG( log_file_simulations + "_" + set + uuidtag)

        # ---- Set the name of the file that will contain the DAG.
        dag.set_dag_file( "grb_simulations_" + set )

        # ---- Make instance of XsearchJob.
        job = XsearchJob(cp)

        # ---- Make instance of XmergeJob.  This job will merge the results files
        #      produced by the several analysis nodes.
        mergejob = XmergeJob(cp)
        if smartCluster == True:
            print('Writing dag for MDC clustered injection set: ' + set, file=sys.stdout)
            dag_clustered = pipeline.CondorDAG( log_file_simulations_clustered + "_" + set + uuidtag )
            dag_clustered.set_dag_file( "grb_simulations_clustered_" + set )
            mergeclusteredjob = XmergeClusteredJob(cp,len(detector))
        # ---- Read segment list from file.
        segmentList = pipeline.ScienceData()
        segmentList.read( 'input/segment_on_source.txt' , blockTime )

        # ---- Read injection file to determine number of injections for this
        #      set. Use system call to wc to figure out how many lines are in
        #      the injection file.
        os.system('wc input/injection_' + set + '.txt | awk \'{print $1}\' > input/' + set + '.txt' )
        f = open('input/' + set + '.txt')
        numberOfInjections = int(f.readline())
        f.close()
        os.system('cat input/injection_' + set + \
                  '.txt | awk \'{printf \"%.9f\\n\",$1+$2*1e-9}\'' + \
                  ' > input/gps' + set + '.txt' )
        f = open('input/gps' + set + '.txt')
        injection_list_time = f.readlines()
        f.close()

        # ---- Initialize merge node for each injection scale.
        mergenode = []
        if smartCluster == True:
            mergeclusterednode = []
        for injectionScale in injectionScales :
            mergenode.append(XmergeNode(mergejob))
            if smartCluster == True:
                mergeclusterednode.append(XmergeClusteredNode(mergeclusteredjob))

        # ---- Skip injection trigger production if path to already available
        #      triggers provided.
        if not(reUseInj):
            # ---- Set buffer for handling long injections.
            longInjSegBuffer = 0
            if longInjections :
                if set.startswith('adi-a') :
                    longInjSegBuffer = 20
                if set.startswith('adi-b') :
                    longInjSegBuffer = 6
                if set.startswith('adi-c') :
                    longInjSegBuffer = 132
                if set.startswith('adi-d') :
                    longInjSegBuffer = 74
                if set.startswith('adi-e') :
                    longInjSegBuffer = 43
                if set.startswith('mva') :
                    longInjSegBuffer = 132
                if set.startswith('ebbh-a') :
                    longInjSegBuffer = 7
                if set.startswith('ebbh-d') :
                    longInjSegBuffer = 4
                if set.startswith('ebbh-e') :
                    longInjSegBuffer = 4
            # ---- Loop over segments.
            for iInj in range(numberOfInjections) :
                thisInjSeg = []
                for i in range(len(segmentList)):
                    if abs(float(event_on_list[i])-float(injection_list_time[iInj]))<=blockTime/2-transientTime+longInjSegBuffer :
                        thisInjSeg.append(i)
                # ---- Check if this injection falls into any segment. If so, add a node to analyse it.
                if thisInjSeg :
                    # ---- Sort segments.
                    thisInjSeg.sort()
                    # ---- Add node to job.
                    node = XsearchNode(job)
                    matlab_param_file = cwdstr + "/input/parameters_simulation_" + set + "_0_0.txt"
                    node.set_param_file(matlab_param_file)
                    node.set_x_jobnum("%d-"%(thisInjSeg[0]) + "%d"%(thisInjSeg[-1]))
                    node.set_output_dir( os.path.join( cwdstr + '/output/simulations_' + set + '_' ) )
                    node.set_x_injnum(str(iInj+1))
                    for scale_counter in range(len(injectionScales)) :
                        mergenode[scale_counter].add_parent(node)
                    node.set_retry(retryNumber)
                    # ---- Prepend human readable description to node name.
                    node.set_name("xdetection_simulation_" + set + "_inj" + str(iInj+1) \
                        + "_seg" + "%d-"%(thisInjSeg[0]) + "%d"%(thisInjSeg[-1]) \
                        + "_" + node.get_name())
                    # ---- Add job node to the dag.
                    dag.add_node(node)

        # ---- Loop over injection scales.
        for scale_counter in range(len(injectionScales)) :
            # ---- Point to already produced injection results if provided.
            if reUseInj:
                mergenode[scale_counter].set_dir_prefix(mergingCutsPath + '/../output/simulations_' \
                    + set + '_' + str(scale_counter) + '/')
            else :
                mergenode[scale_counter].set_dir_prefix('simulations_' + set + '_' + str(scale_counter) + '/')
            mergenode[scale_counter].set_sn_flag("1 " + mergingCutsString)
            mergenode[scale_counter].set_retry(retryNumber)
            # ---- Prepend human readable description to node name.
            mergenode[scale_counter].set_name("xmerge_simulation_" + set + "_injScale" \
                + str(scale_counter) + "_" + mergenode[scale_counter].get_name())
            dag.add_node(mergenode[scale_counter])

            if smartCluster == True:
                # ---- Point to already produced injection results if provided.
                if reUseInj:
                    mergeclusterednode[scale_counter].set_dir_prefix(mergingCutsPath + '/../output/simulations_' \
                        + set + '_' + str(scale_counter) + '/')
                else :
                    mergeclusterednode[scale_counter].set_dir_prefix('simulations_' + set + '_' + str(scale_counter) + '/')
                mergeclusterednode[scale_counter].set_sn_flag("1 " + mergingCutsString)
                mergeclusterednode[scale_counter].set_sc_flag("1")
                mergeclusterednode[scale_counter].set_retry(retryNumberClustered)
                # ---- Prepend human readable description to node name.
                mergeclusterednode[scale_counter].set_name("xmergeclustered_simulation_" + set + "_injScale" \
                    + str(scale_counter) + "_" + mergeclusterednode[scale_counter].get_name())
                dag_clustered.add_node(mergeclusterednode[scale_counter])

        # ---- Write out the submit files needed by condor.
        dag.write_sub_files()
        # ---- Write out the DAG itself.
        dag.write_dag()

        # ---- Delete used dag job
        del dag
        del job
        del mergejob
        if smartCluster == True:
            dag_clustered.write_sub_files()
            dag_clustered.write_dag()
            del dag_clustered
            del mergeclusteredjob

# -------------------------------------------------------------------------
#      Write single dag containing all jobs.
# -------------------------------------------------------------------------

# ---- Use grep to combine all dag files into a single dag, with the
#      PARENT-CHILD relationships listed at the end.
print("Combining all jobs into a single dag ...")
os.system('echo "DOT xpipeline_triggerGen.dot" > .dag_temp')
os.system('grep -h -v PARENT *.dag >> .dag_temp')
os.system('grep -h PARENT *.dag >> .dag_temp')
os.system('mv .dag_temp grb_alljobs.dag')


# -------------------------------------------------------------------------
#      Finished.
# -------------------------------------------------------------------------

# ---- Status message.
print("... finished writing job submission files. ", file=sys.stdout)
print(file=sys.stdout)

print("############################################", file=sys.stdout)
print("#           Completed.                     #", file=sys.stdout)
print("############################################", file=sys.stdout)

# ---- Append to summary file.
sfile = open(summary_file,'a')
sfile.write('\t 1')
sfile.close()

# ---- Exit cleanly
sys.exit( 0 )


# -------------------------------------------------------------------------
#      Leftover code samples.
# -------------------------------------------------------------------------

# # ---- Make data find job node.
# df_job = pipeline.LSCDataFindJob( 'cache','logs', cp )

# # ---- Make an empty list to hold the datafind jobs.
# df_list = []

# # ---- Loop over detectors and prepare a datafind job for each.
# for ifo in ['H1', 'H2', 'L1']:
#   df = pipeline.LSCDataFindNode( df_job )
#   df.set_start( 700000000 )
#   df.set_end( 700000100 )
#   df.set_observatory( ifo[0] )
#   df.set_type('RDS_R_L1')
#   df.set_post_script('/why/oh/why/oh/why.sh')
#   df.add_post_script_arg(df.get_output())
#   dag.add_node(df)
#   df_list.append(df)
