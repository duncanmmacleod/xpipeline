function xmakehtmlreport(matFileName)
% XMAKEHTMLREPORT - Create an html report page for one X-Pipeline analysis.
%
% usage:
%
%   xmakehtmlreport(matFileName)
%
% matFilename         String. Name of .mat file containing results of the
%                     post-processing as produced by XMAKEGRBWEBPAGE. See
%                     that function for details on the variables contained
%                     in the file.
%
% Comments will appear when the web page file name has a shtml extension
% and viewed through a web server like Apache; local opening of this web
% page won't display comments.html
%
% $Id$

% ---- Formatting of stdout messages from this function.
format short
format compact


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Check input arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check number of input arguments. Assign default values where needed.
if (nargin==1)
    disp(['Attempting to read data from the file ' matFileName ' ...']);
    % ---- Dangerous: dump everything from the matlab file into the local
    %      workspace with no control over what variable names are created
    %      or overwritten. 
    load(matFileName)
    disp('   ... done!');
else
    error('Too many or too few input arguments')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        Sanity checks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- We should only ever be making a web page for a single tuned set
%      of coherent consistency test thresholds. vetoPlus/Cross/NullRange
%      all have the same length, so check any one of them.
if length(vetoNullRange)>1
    msg = ['We should only make a web page for a single tuned set of coherent ' ...
           'consistency test thresholds. Number of thresholds is ' ...
           num2str(length(vetoNullRange)) '.'];
    error(msg)
end

% ---- If we are only analysing a single ifo we should not have coherent
%      consistency cuts; all veto*Range inputs should be zero.
% ---- KLUDGE: This check may not catch "pre" vetoes being nonzero ...
if length(analysis.detectorList) == 1 %#ok<*NODEF>
    if vetoNullRange || vetoPlusRange || vetoCrossRange
        error(['When running with a single ifo we cannot perform coherent ' ...
               'consistency tests. vetoNullRange, vetoPlusRange '...
               'and vetoCrossRange should each be zero'])
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  Report svnversions of codes used
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- This was determined in xmakegrbwebpage.
disp(['For processing we used svnversion      : ' analysis.svnversion_xdetection]);
disp(['For post processing we used svnversion : ' analysis.svnversion_grbwebpage]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  Open web page and write header info
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[fout,figures_dirName,figfiles_dirName] = xwritewebpageheader(user_tag,analysis);

% ---- Initialise captions for figures.
analysis.captions = [];
analysis.captions = xcaptionsforplots(analysis.captions); 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                     Write veto segment data 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Begin table listing veto segments.
fprintf(fout,'%s\n',['<br><table border=1 cellspacing="1" cellpadding="5">']);
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td><b> Detector </b></td>']);
fprintf(fout,'%s\n',['<td><b> Veto segment list</b></td>']);
fprintf(fout,'%s\n',['</tr>']);
% ---- Loop over ifos we have analysed.
for thisIfo = 1:length(analysis.detectorList)
    % ---- Find vetoSegList corresponding to thisIfo.
    thisVetoSegList = vetoSegList{strcmp(ifos,analysis.detectorList(thisIfo))};   %#ok<*USENS>
    % ---- Output to webpage.
    fprintf(fout,'%s\n',['<tr>']);
    fprintf(fout,'%s\n',['<td> ' analysis.detectorList{thisIfo} ' </td>']);
    fprintf(fout,'%s\n',['<td> ' thisVetoSegList ' </td>']);
    fprintf(fout,'%s\n',['</tr>']);
end
% ---- End the table.
fprintf(fout,'%s\n','</table><br>');

% ---- KLUDGE: We don't list the frequency bands to veto (if any).
disp('KLUDGE: Add information on freqVetoBands to the html report.');

% ---- Add links to the ini file and post-processing parameters in the web page.
fprintf(fout,'%s\n','<br>Ini file used:<br>');
fileList = dir('*.ini');
% ---- If more than one ini file is found, link all of them so that the
%      confusion over which one was used is visible.
for iFile=1:length(fileList)
    fprintf(fout,'<A HREF="%s">%s</A><br>\n',fileList(iFile).name,fileList(iFile).name);
end
% ---- Link the file with the post-processing parameters. Hard-coding
%      should work as this file is created automatically by xgrbwebpage.py.
fprintf(fout,'%s\n','<br>Post-processing parameters:<br>');
fprintf(fout,'<A HREF="%s">%s</A><br>\n','xgrbwebpage.param','xgrbwebpage.param');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Write time and sky position of GRB
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Retrieve rand seed used by randperm for this GRB.
disp(' ');
disp(['initial random seed: ' num2str(analysis.initialSeed)]);

% ---- Call helper function which generates the plots for the "Network
%      Sensitivity Plots" section of the webpage.
[analysis.FpList,analysis.FcList,analysis.dF]=...
    xwritenetworkinfo(analysis,fout,figures_dirName,figfiles_dirName);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Write summary html table showing optimal vetoes 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vetoTableFileName = [analysis.grb_name '_' user_tag '_vetoTable' vetoMethod '.html'];  

fprintf(fout,'%s\n','<h2>VetoTuning</h2>');

fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
%
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td><b>'                '</b></td>']);
fprintf(fout,'%s\n',['<td><b>' nullVetoType   '</b></td>']);
fprintf(fout,'%s\n',['<td><b>' plusVetoType   '</b></td>']);
fprintf(fout,'%s\n',['<td><b>' crossVetoType  '</b></td>']);
fprintf(fout,'%s\n',['<td><b> detection statistic  </b></td>']);
fprintf(fout,'%s\n',['</tr>']);
%
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td>  Optimal veto threshold  (' vetoMethod ')</td>']);
fprintf(fout,'%s\n',['<td>   ' num2str(vetoNullRange) ' </td>']);
fprintf(fout,'%s\n',['<td>   ' num2str(vetoPlusRange) ' </td>']);
fprintf(fout,'%s\n',['<td>   ' num2str(vetoCrossRange) ' </td>']);
fprintf(fout,'%s\n',['<td>   ' detectionStat ' </td>']);
fprintf(fout,'%s\n',['</tr>']);
%
fprintf(fout,'%s\n','</table><br>');

fprintf(fout,'%s\n',['<!--#include file="' vetoTableFileName '" -->']);

% --- Print out fixed ratio cuts if they have been tuned.
if not(isempty(regexp(vetoMethod,'median.+Cut.*')) && ...
       isempty(regexp(vetoMethod,'alpha.+Cut.*')))

    fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
    fprintf(fout,'%s\n',['<tr>']);
    fprintf(fout,'%s\n',['<td><b>'                '</b></td>']);
    fprintf(fout,'%s\n',['<td><b>' nullVetoType   '</b></td>']);
    fprintf(fout,'%s\n',['<td><b>' plusVetoType   '</b></td>']);
    fprintf(fout,'%s\n',['<td><b>' crossVetoType  '</b></td>']);
    fprintf(fout,'%s\n',['</tr>']);

    fprintf(fout,'%s\n',['<tr>']);
    fprintf(fout,'%s\n',['<td>  Optimal fixed ratio veto threshold  </td>']);
    fprintf(fout,'%s\n',['<td>   ' num2str(tunedPrePassCuts(1)) ' </td>']);
    fprintf(fout,'%s\n',['<td>   ' num2str(tunedPrePassCuts(2)) ' </td>']);
    fprintf(fout,'%s\n',['<td>   ' num2str(tunedPrePassCuts(3)) ' </td>']);
    fprintf(fout,'%s\n',['</tr>']);
    fprintf(fout,'%s\n','</table><br>');

    switch vetoMethod
        case {'medianLinCut','alphaLinCut'}
            vetoTableFileNamePre = [analysis.grb_name '_' user_tag '_vetoTablelinearCut.html'];  
        case {'medianLinCutCirc','alphaLinCutCirc'}
            vetoTableFileNamePre = [analysis.grb_name '_' user_tag '_vetoTablelinearCutCirc.html'];  
        case {'medianLinCutAmp','alphaLinCutAmp'}
            vetoTableFileNamePre = [analysis.grb_name '_' user_tag '_vetoTablelinearCutAmp.html'];  
        case {'medianLinCutScalar','alphaLinCutScalar'}
            vetoTableFileNamePre = [analysis.grb_name '_' user_tag '_vetoTablelinearCutScalar.html'];
        case 'medianAlphaCut'
            vetoTableFileNamePre = [analysis.grb_name '_' user_tag '_vetoTablealphaCut.html'];  
        case 'medianAlphaCutCirc'
            vetoTableFileNamePre = [analysis.grb_name '_' user_tag '_vetoTablealphaCutCirc.html'];
        case 'medianAlphaCutScalar'
            vetoTableFileNamePre = [analysis.grb_name '_' user_tag '_vetoTablealphaCutScalar.html']; 
        case 'medianAlphaCutAmp'
            vetoTableFileNamePre = [analysis.grb_name '_' user_tag '_vetoTablealphaCutAmp.html'];  
        otherwise
            warning(['Unrecognized pre-pass mode vetoMethod: ' vetoMethod ...
                '. Falling back to final veto table.' ])
            vetoTableFileNamePre = vetoTableFileName;
    end
    fprintf(fout,'%s\n',['<!--#include file="' vetoTableFileNamePre '" -->']);

end
   
% ---- Write to webpage lists of job numbers: discarded due to DQ; used for
%      tuning; used for upper limits / significance. Also write list of 
%      analysis times (FFT durations).  
xwriteonoffsourceinfo(fout,analysis,onSource,offJobSeed,dummyOnSourceJobNumber,...
    offJob_tuning,offJob_ulCalc,uniqueOffJobNumberFail);    

fprintf(fout,'%s\n','<br>On-source events<br>');

% ---- For a closed-box analysis, write information on the "loudest event"
%      to the web page. 
if (strcmp(analysis.type,'closedbox') || strcmp(analysis.type,'snrulclosedbox'))
    % ---- Write some output to webpage. 
    fprintf(fout,'%s\n',['For veto tuning, '...
    num2str(percentile_tuning) 'th percentile loudest '...
    'off-source event taken as dummy on-source. <br>']); 
    fprintf(fout,'%s\n',['For upper limit calculation, '...
    num2str(percentile_ulCalc) 'th percentile loudest '...
    'off-source event taken as dummy on-source, '... 
    'events taken ' ...
    'from background job number ' num2str(dummyOnSourceJobNumber) ...
    ', center time = '  num2str(onSource.centerTime(1)) ...
    ', time offsets = ' num2str(onSource.timeOffsets(1,:)) ...
    ', circ time slides = ' num2str(onSource.circTimeSlides(1,:)) ...
    ' <br>']);
    % ---- Safety check: we want the variance on the percentile 
    %      level p: p(1-p)/N to be smaller than some small factor
    %      squared: 0.1^2 (the allowed relative error, here 0.1)
    %      times the expected value squared: (1-p)^2.
    maxPercentile=max(percentile_tuning,percentile_ulCalc);
    minNjobs=min(length(offJob_tuning),length(offJob_ulCalc));
    if (1-maxPercentile/100)*0.1^2 < 1/minNjobs
        fprintf(fout,'%s\n',...
            ['<font color="red">***WARNING***</font> not enough' ...
            ' background jobs. The percentile levels have an absolute' ...
            ' error in their estimation of ~'...
            num2str(100*sqrt((1-maxPercentile/100)*maxPercentile/100/minNjobs)) ...
            '% <br>'] )
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Report deadtimes to webpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Begin table.
fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td><b> Data type </b></td>']);
fprintf(fout,'%s\n',['<td><b> Deadtime (s) </b></td>']);
fprintf(fout,'%s\n',['<td><b> Percentage of ' num2str(window.duration) 's analysis window </b></td>']);
fprintf(fout,'%s\n',['</tr>']);

% ---- Deadtime in real on-source.
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> On-source </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(onJobsDeadtime) ' </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(onJobsDeadtime*100/window.duration) ' </td>']);
fprintf(fout,'%s\n',['</tr>']);

% ---- Deadtime in dummy on-source (if we have closedbox).
if (strcmp(analysis.type,'closedbox') || ...
    strcmp(analysis.type,'snrulclosedbox'))
    % ---- Find idx of jobNumber corresponding to dummy onSource.
    dummyIdx = find(uniqueOffJobNumber == dummyOnSourceJobNumber);
    fprintf(fout,'%s\n',['<tr>']);
    fprintf(fout,'%s\n',['<td> Dummy on-source </td>']);
    fprintf(fout,'%s\n',['<td> ' num2str(offJobsDeadtime(dummyIdx)) ' </td>']);
    fprintf(fout,'%s\n',['<td> ' num2str(offJobsDeadtime(dummyIdx)*100/window.duration) ' </td>']);
    fprintf(fout,'%s\n',['</tr>']);
end

% ---- Deadtime in all off-source.
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> Off-source (' num2str(nJobFilesProcessed) ' jobs) </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(sum(offJobsDeadtime)) ' </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(sum(offJobsDeadtime)*100/(window.duration * nJobFilesProcessed)) ' </td>']);
fprintf(fout,'%s\n',['</tr>']);

% ---- Deadtime in off-source for tuning.
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> Off-source (' num2str(nOffJob_tuning) ' tuning jobs) </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(sum(offJobsDeadtime(offIdx_tuning))) ' </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(sum(offJobsDeadtime(offIdx_tuning))*100/(window.duration * nOffJob_tuning)) ' </td>']);
fprintf(fout,'%s\n',['</tr>']);

% ---- Deadtime in off-source for ulCalc.
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> Off-source (' num2str(nOffJob_ulCalc) ' UL calc jobs) </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(sum(offJobsDeadtime(offIdx_ulCalc))) ' </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(sum(offJobsDeadtime(offIdx_ulCalc))*100/(window.duration * nOffJob_ulCalc)) ' </td>']);
fprintf(fout,'%s\n',['</tr>']);

% ---- End table.
fprintf(fout,'%s\n','</table><br>');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          Make plots and tables of on-/off-source events
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Make scatter plot of on-source events.
try
    xwritelikelihoodscatterplots(fout,figures_dirName,figfiles_dirName,...
        analysis,onSourceAfterDQ,offSourceSelectedAfterDQ,vetoMethod,...
        ratioArray,medianE,medianI,'onsource',fixedRatioArray,fixedDeltaArray);
catch ME
    ME
    fprintf(fout,'%s\n</div>\n','***PLOT FAILED***');
end

% ---- Add table of loudest on-source events.
xwriteonsourceeventtable(fout,analysis,onSource,user_tag); 

% ---- Make scatter plot of off-source events.
try
    xwritelikelihoodscatterplots(fout,figures_dirName,figfiles_dirName,...
        analysis,onSourceAfterDQ,offSourceSelectedAfterDQ,vetoMethod,ratioArray,...
        medianE,medianI,'offsource',fixedRatioArray,fixedDeltaArray);
catch ME
    ME
    fprintf(fout,'%s\n</div>\n','***PLOT FAILED***');
end

% ---- Make histograms of trigger properties.
fprintf(fout,'%s\n','All triggers');
try
    xwritetriggerhists(fout,analysis,{offSourceSelectedBeforeDQ,onSource},...
        {'off source','on source'},figures_dirName,figfiles_dirName,'offon',2);
catch ME
    ME
    fprintf(fout,'%s\n</div>\n','***PLOT FAILED***');
end

% ---- Make histograms of trigger properties after DQ and window.
fprintf(fout,'%s\n','Triggers after applying DQ and window cut');
try
    xwritetriggerhists(fout,analysis,...
        {offSourceSelectedAfterDQandWindow,onSourceAfterDQandWindow},...
        {'off source after DQ/window cut','on source after DQ/window cut'},...
        figures_dirName,figfiles_dirName,'offonAfterDQandWindow',2);
catch ME
    ME
    fprintf(fout,'%s\n</div>\n','***PLOT FAILED***');
end

% ---- Make histograms of trigger properties after all cuts.
fprintf(fout,'%s\n',['Triggers after all cuts, including coherent cuts']);
try
    if isempty(onSourceAfterAllCuts.likelihood)
        xwritetriggerhists(fout,analysis,{offSourceSelectedAfterAllCuts},...
            {'off source after all cuts'},...
            figures_dirName,figfiles_dirName,'offonAfterAllCuts',1);
    else
        xwritetriggerhists(fout,analysis,{offSourceSelectedAfterAllCuts,...
            onSourceAfterAllCuts},...
            {'off source after all cuts','on source after all cuts'},...
            figures_dirName,figfiles_dirName,'offonAfterAllCuts',2);
    end
catch ME
    ME
    fprintf(fout,'%s\n</div>\n','***PLOT FAILED***');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   Report simulation results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Loop over injection sets tested.
for iWave = 1:length(nameCell)

    % ---- Status report.
    disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    disp(['Working on waveform: ' injectionSetNames{iWave}])


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                 Web page plots for this waveform
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- Write header for this injection.
    fprintf(fout,'%s<br>\n',['<h2 id="'  injectionSetNames{iWave} ...
        '" > Injected Waveform : ' injectionSetNames{iWave}  ...
        ' from injection file: ' injectionFilesCell{iWave} '</h2>']);

    % ---- Write some output to webpage.
    xwriteinjectioninfo(fout,injSeed,iWave,nInjections(iWave), ...
        nInjectionsBy2(iWave),injIdx_tuning{iWave},injIdx_ulCalc{iWave})

    plotNameEff = ['/eff_' injectionSetNames{iWave}];
    xwriteinjectioneffplots(fout,injectionScale(iWave,:),efficiency(iWave,:), ...
        analysisEff(iWave,:),nameCell{iWave},parametersCell{iWave},UL90p(iWave),UL50p(iWave), ...
        plotNameEff,figures_dirName,figfiles_dirName)

    % ---- Overlay scatter plot of injection clusters at the scale 
    %      closest to the 90% UL, or loudest injection scale if UL
    %      does not exist.
    % ---- Find the desired injection scale.
    %      THIS IS REPEATED FROM xmakegrbwebpage.
    if(~isnan(UL90p(iWave)))
        [~, iS1] = nanmin(abs(UL90p(iWave)-injectionScale(iWave,:)));
    else
        [~, iS1] = max(injectionScale(iWave,:));
    end
    % ---- Add to on- and off-source scatter plots.
    try
        [plotNameOn,plotNameOff] = ...
            xwriteinjectionscatterplots(fout,analysis,triggersForHist{iWave}{1},...
                injectionScale(iWave,iS1).*hrss(iWave),figures_dirName,...
                figfiles_dirName,injectionSetNames{iWave},parametersCellClean{iWave});
        % ---- Create cell array of captions for these plots.
        numCaptions = length(plotNameOn)+length(plotNameOff);
        injCaptions{1} = analysis.captions.efficiency; 
        for iCap = 2:numCaptions+1
            injCaptions{iCap} = analysis.captions.injection_scatter;
        end
        % ---- Add the figure to the web page.
        xaddfiguretable(fout,[plotNameEff plotNameOn plotNameOff], figures_dirName,injCaptions);
    catch ME
        ME
        fprintf(fout,'%s\n</div>\n','***PLOT FAILED***');
    end

    % ---- Add histograms of the trigger properties.
    try
        xwritetriggerhists(fout,analysis, ...
	    [{offSourceSelectedAfterDQ}; {onSourceAfterDQ}; triggersForHist{iWave}(:)], ...
	    [{'off source'}; {'on source'}; triggerNamesForHist{iWave}(:)], ...
            figures_dirName,figfiles_dirName, [injectionSetNames{iWave}],2)
    catch ME
        ME
        fprintf(fout,'%s\n</div>\n','***PLOT FAILED***');
    end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                     Histogram rate vs.significance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xwriteratevssig(fout,analysis,window,onSource,offSourceSelected,...
    binArray,figures_dirName,figfiles_dirName);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        Annoying triggers properties
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if not(isempty(regexp(vetoMethod,'None')))
    warning('Veto method is ''None''; skipping annoying triggers.')
else
    xwritedqtriggers(fout,user_tag,analysis,...
        max(onSource.significance .* onSource.pass),...
        offSourceSelectedAfterDQandWindow,...
        offSourceSelectedAfterAllCuts,onSource,figures_dirName,...
        figfiles_dirName);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Histogram of loudest off source events 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Pack sets of loudest events into a cell array for convenience.
offLoudestEventCell = {offLoudestEventPreVeto,offLoudestEventWindowCut, ...
                       offLoudestEventVetoSegs,offLoudestEvent};
offLoudestEventName = {'off source: all events','off source: after window cut',...
                       'off source: after data quality vetoes', ...
		       'off source: after coherent consistency cuts'};
xwriteloudestoffsource(fout,analysis,offLoudestEventCell,...
    offLoudestEventName,binArray,figures_dirName,figfiles_dirName);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              Summary Section
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Write header to web page.
fprintf(fout,'%s\n','<h2>Summary</h2>');

% ---- Overlay hrss upper limits on plots of the IFO amplitude spectra.
xwriteulplots(fout,analysis,cFreq,UL50p,hrss,figures_dirName,figfiles_dirName);

% ---- Some output to the screen: central frequency and 50/90% limits.
for idx = 1:length(cFreq)
    fprintf(1,'cFreq = %8.2f, 50%% UL = %e, 90%% UL = %e \n ', ...
        cFreq(idx),UL50p(idx),UL90p(idx)); 
end

% ---- Write summary table.
xwritesummarytable(fout,analysis,cFreq,UL50p,UL90p,hrss,...
    percentNans,injectionSetNames,hrss_unit,SNRlist,nullSNRlist);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          Produce summary file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

summaryFileName = [analysis.grb_name '_' user_tag '_' analysis.type '_summary.txt']
fsumm = fopen(summaryFileName,'w');
% ---- Construct string listing ifos used.
detectorListStr = '';
for thisIfo = 1:length(analysis.detectorList)
    detectorListStr = [detectorListStr analysis.detectorList{thisIfo}]; 
end
fprintf(fsumm,'%s\n',['GPS ifos wvfs 50%%UL 90%%UL tuningNameStr '...
                  'nullVetoType tunedNullVeto '...
                  'plusVetoType tunedPlusVeto '...
                  'crossVetoType tunedCrossVeto '...
                  ]);
for iWave = 1:length(nameCell)
    fprintf(fsumm,'%s\n',[num2str(analysis.gpsCenterTime) ' '...
                    detectorListStr ' '...
                    injectionSetNames{iWave} ' '...
                    num2str(UL50p(iWave)*hrss(iWave)) ' '...
                    num2str(UL90p(iWave)*hrss(iWave)) ' '...
                    tuningNameStr ' '...
                    nullVetoType ' ' num2str(vetoNullRange) ' '...
                    plusVetoType ' ' num2str(vetoPlusRange) ' '...
                    crossVetoType ' ' num2str(vetoCrossRange) ' '...
                    ]);
end
fclose(fsumm);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Close up and save variables.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- End of web page.
fprintf(fout,'%s\n','</body>');
fprintf(fout,'%s\n','</html>');
fclose(fout)

disp(['xmakehtmlreport completed successfully!']);

% ---- Done.
return
