function [plotNameOn,plotNameOff]=...
     xwriteinjectionscatterplots(fout,analysis,loudestInjEvents,...
     injectionHrssChosen,figures_dirName,figfiles_dirName,...
     injName,injParams)
% XWRITEINJECTIONSCATTERPLOTS - make scatter plots of injection triggers.
% 
% xwriteinjectionscatterplots produces scatter plots of injection triggers
% overlaid on similar plots of on and off source triggers. It is a helper 
% function for xmakegrbwebpage.m  
%
% Usage:
%  [plotNameOn,plotNameOff]=...
%     xwriteinjectionscatterplots(fout,analysis,loudestInjEvents,...
%     injectionHrssChosen,figures_dirName,figfiles_dirName,...
%     injName,injParams)   
%   
%  plotNameOn            Cell array of strings. Names of plots of injections 
%                        overlaid over on source scatter plot
%  plotNameOff           Cell array of strings. Names of plots of injections 
%                        overlaid over off source scatter plot
%
%  fout                  Pointer to open and writable webpage file  
%  analysis              Structure created by xmakegrbwebpage
%  loudestInjEvents      Structure containing loudest trigger associated with
%                        each injection at the chosen injection scale 
%  injectionHrssChosen  Double. Injection scale of injections we are going to
%                        plot 
%  figures_dirName       String. Name of output dir for figures 
%  figfiles_dirName      String. Name of output dir for figfiles 
%  injName               String. Name of the type of injection being plotted
%  injParams             String describing params of injection
% 
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(8, 8, nargin));

sourceType={'on','off'};

% ---- When analysing single ifo only, analysis.likelihoodPairs will be
%      empty and we will skip the main for loop of this function. 
%      We set default output here
plotNameOn  = '';
plotNameOff = '';

% ---- Likelihoods measured in pairs, (ePlus,iPlus), (eCross,iCross),
%      (eNull,iNull).  Loop over these pairs making plots.
for pairIdx=1:length(analysis.likelihoodPairs)

    fprintf(1,'%s\n',['Using ' ...
        analysis.likelihoodType{analysis.likelihoodPairs(pairIdx).energy} ...
        ' and '  ...
        analysis.likelihoodType{analysis.likelihoodPairs(pairIdx).inc} ...
        ' to make scatter plots...'])

    loudestInjEvents.likelihoodX = loudestInjEvents.likelihood(:,...
        analysis.likelihoodPairs(pairIdx).energy); 
    loudestInjEvents.likelihoodY = loudestInjEvents.likelihood(:,...
        analysis.likelihoodPairs(pairIdx).inc);  
  
    fprintf(1,'%s\n',[num2str(length(loudestInjEvents.significance)) ...
    ' injections plotted'])

    % ---- Loop over on and off source
    for sourceIdx = 1:length(sourceType);

        fprintf(1,'%s\n',['Making ' sourceType{sourceIdx} ...
            ' source plots with injections ']);

        % ---- Scatter plot of on or off source events plus injections.
        % ---- Open previously generated scatter plot of on or off source
        %      clusters.
        open([figfiles_dirName sourceType{sourceIdx}  'source_' ...
            analysis.likelihoodType{analysis.likelihoodPairs(pairIdx).energy} ...
            '_' ...
            analysis.likelihoodType{analysis.likelihoodPairs(pairIdx).inc} ...
            '_' analysis.clusterType '.fig']);
        hold on;

        % ---- retrieve legend from figure
        %      note that the location needs to be set again 
        legendStr = get(legend,'String');

        warning('OFF','MATLAB:log:logOfZero');

        % ---- Overlay injections on scatter plot of on-source events.
        scatter(loudestInjEvents.likelihoodX,...
            loudestInjEvents.likelihoodY,...
            40,log10(loudestInjEvents.significance),...
            's','filled','MarkerEdgeColor','k');

        % ---- Update legend
        legendStr{length(legendStr)+1} = ...
            ['injections with hrss ' num2str(injectionHrssChosen)];

        maxInjX = max(loudestInjEvents.likelihoodX);
        maxInjY = max(loudestInjEvents.likelihoodY);

        % ---- Format plot.
        X=xlim;
        Y=ylim;
        axis([ X(1) max([X(2); maxInjX ]) ...
        Y(1) max([Y(2); maxInjY ]) ]);

        hold off;

        % ---- Write legend
        legend(legendStr,'Location','SouthEast');

	% ---- Construct sensible filename for plots
        % on_scatter_SGC250Q8d9_OFFmdcInjection_nullenergy_nullinc_connected
        plotName = ['/' sourceType{sourceIdx} ...
            '_scatter_' injName '_' injParams '_' ...
            analysis.likelihoodType{analysis.likelihoodPairs(pairIdx).energy} ...
            '_' ...
            analysis.likelihoodType{analysis.likelihoodPairs(pairIdx).inc} ...
            '_' analysis.clusterType];

        % ---- Save plot.
        xsavefigure(plotName,figures_dirName,figfiles_dirName);

        % ---- Store plot names 
        if strcmp(sourceType{sourceIdx},'on')
            plotNameOn{pairIdx} = plotName;
        elseif strcmp(sourceType{sourceIdx},'off')
            plotNameOff{pairIdx} = plotName;
        else
            error('we have a problem with the on/off scatter plots')
        end

        warning('ON','MATLAB:log:logOfZero');
    end % -- end loop over on/off source
end % -- end loop over likelihoodTypes  


