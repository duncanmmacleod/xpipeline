function [GRB,killedTimes,liveTimes,grbMask,intersectLiveTimes,...
    finalKilledTimes] = xgrbnetwork(grbFile,ifos,segFiles,segType,...
        goodBefore,goodAfter,maxTimeKilled,tStart,tEnd,usertag)
% XGRBNETWORK - Determine which IFOs were on for each GRB in a list.
%
% ---- This script reads a list of grbs and segment lists from files,
%      and determines which ifos were on for each GRB subject to certain
%      conditions. 
%
%   usage: [GRB,killedTimes,liveTimes,grbMask,...
%           intersectLiveTimes,finalKilledTimes] = ...
%           xgrbnetwork(grbFile,ifos,segFiles,segType,...
%           goodBefore,goodAfter,maxTimeKilled,tStart,tEnd,usertag)
%
%   grbFile      ascii list containing GRB info taken from Isabel Leonor's
%                validated list available from
%                http://www.uoregon.edu/~ileonor/ligo/s5/grb/online/S5grbs_list_valid.html
%   ifos         cell array containing list of ifos to be analysed
%   segFiles     cell array containing path and names of segment files 
%                generated using segwizard, there should be one for each 
%                ifo to be analysed. The segment files should be listed in 
%                the same order as the ifos they correspond to. Segment
%                files should contain 4 columns of numerical data
%                corresponding to: segno start stop dur.
%                Segment 
%   segType      string, one of:
%                'dataQuality' : segFiles contain list of "good" times that 
%                                should be analysed
%                'veto'        : segFiles contain list of "bad" times that 
%                                should not be analysed
%   goodBefore    double, number of seconds before GRB time that we require 
%                 good DQ. The interval 
%                 [GRBtime - goodBefore,GRBtime + goodAfter] will be called
%                 the GRB interval.
%   goodAfter     double, number of seconds after GRB time that we require 
%                 good DQ.
%   maxTimeKilled double, maximum number of seconds in the GRB interval 
%                 that can be killed by DQ flags/vetoes before we decide to 
%                 ignore that ifo in our analysis
%   tStart        double, GPS start time of science run
%   tEnd          double, GPS end time of science run
%   usertag       string, for naming output files 
%    
%   GRB           cell array containing info from grbFile for GRBs that lie
%                 between tStart and tEnd
%   killedTimes   cell array, size numGRBs * numIfos. Contains segments
%                 within GRB interval killed by DQ flags/vetoes
%   liveTimes     cell array, size numGRBs * numIfos. Contains segments
%                 within GRB interval not killed by DQ flags/vetoes
%   grbMask       array, size numGRBs * numIfos. Element value of 1
%                 indicates given GRB should be analysed with given ifo.
%                 Element value of 0 indicates GRB should not be analysed
%                 with given ifo.
%   intersectLiveTimes cell array, size numGRBs. Contains the intersect of
%                 liveTimes segments from each ifo for a particular given 
%                 GRB              
%   finalKilledTimes cell array, size numGRBs. Contains the complement
%                 of intersectLiveTimes within the GRB interval
%
%  example:
%      [GRB,killedTimes,liveTimes,grbMask,...
%       intersectLiveTimes,finalKilledTimes] = ...
%       xgrbnetwork('currentgrbs_html_offline_S5.txt',{'H1' 'H2','L1'},...
%       {'S5-H1_cat1.txt','S5-H2_cat1.txt','S5-L1_cat1.txt'},...
%        'dataQuality',128,128,0,815155213,875250014,'TEST')
%
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(10, 10, nargin));


segTypesAllowed = {'dataQuality','veto'};
if not(max(strcmp(segType,segTypesAllowed)))
    error(['sourceType must be one of: ' segTypesAllowed])
end

numIfos = length(ifos);
numSegs = length(segFiles);
if not(numIfos==numSegs)
    error('Error: number of segFiles must equal number of ifos specified')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             Read GRB list
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fid = fopen(grbFile,'r');
% ---- Following line works for ascii output of Isabel's validated
% ---- list of GRBs: http://www.uoregon.edu/~ileonor/ligo/s5/grb/online/S5grbs_list_valid.html    
% ---- Read in RA and dec as strings in case they contain 'n/a'  
GRB = textscan(fid,'%d%s%s%f%s%s%s%s%s%s%s%s%s%s%s%s%s%s');
fclose(fid);

numberGRBs = length(GRB{1});
disp(['Read info on ' num2str(numberGRBs) ' GRBs.']);
% ---- GPS times of GRBs.
gps = GRB{4};

% ---- Discard GRBs outside of the time interval of interest, tStart to tEnd
C = Coincidence2(tStart,tEnd-tStart,gps,ones(size(gps)));

% ---- Replace original GRB cell array with one only containing GRBs
%      in time interval tStart to tEnd
% ---- Loop over columns in GRB cell
for iCol = 1:length(GRB)
    % ---- C(:,6) contains elements of GRB cell array which lay
    %      tStart and tEnd
    GRB{iCol} = GRB{iCol}(C(:,6));
end

numberGRBs = length(GRB{1});
disp(['There are ' num2str(numberGRBs) ' GRBs between ' ...
    num2str(tStart) ' and ' num2str(tEnd)]);
gps = GRB{4};

% ---- Create grbMask for GRBs occurring between tStart and tEnd;
%      Default is that we will analyse GRB and ifo
grbMask = ones(numberGRBs,numIfos);

% ---- Minimum required segment duration.
min_dur = goodBefore + goodAfter;

% ---- Load segment lists, compute complements
for jj = 1:numIfos 

    thisIfo = ifos{jj};

    disp(['Reading in segs for ' thisIfo]);

    % ---- Read in files, files should have four columns 
    %      corresponding to: segno start stop dur
    fid = fopen(segFiles{jj},'r');
    segData = ...
        textscan(fid,'%f%f%f%f','commentstyle','#');
    fclose(fid);

    if isempty(segData{1})
        error(['error reading contents from ' segFiles{jj}]);
    end
    segno = segData{1};
    start = segData{2};
    stop  = segData{3};
    dur   = segData{4};
  
    %% ---- Drop segments less than min_dur long.
    %k = find(dur < min_dur);
    %start(k) = []; segno(k) = []; stop(k) = []; dur(k) = [];
	
    % ---- Drop segments outside interval of interest.
    k = find(start+dur < tStart);
    start(k) = []; segno(k) = []; stop(k) = []; dur(k) = [];
	
    k = find(start < tStart);
    start(k) = tStart;
    % ---- This line was missing previously, check it's correct 
    dur(k) = start(k) + dur(k) - tStart;

    k = find(start > tEnd);
    start(k) = []; segno(k) = []; stop(k) = []; dur(k) = [];

    k = find(start + dur > tEnd);
    dur(k) = tEnd - start(k);

    % ---- Record segment list and compute complement of segment list.
    seg{jj} = [start dur];
    segComp{jj} = ComplementSegmentList(start,dur,tStart,tEnd);
end

% ---- Obtain list of "good" and "bad" times
if strcmp(segType,'dataQuality')
    badSegs  = segComp;
    goodSegs = seg;
else
    badSegs  = seg;
    goodSegs = segComp;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Compare GRB times to segment lists.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Loop over GRBs
for iGRB = 1:numberGRBs

    disp(['Looking at GRB' GRB{2}{iGRB} ]);

    % ---- Loop over ifos
    for iIfo = 1:numIfos 
  
        % ---- Look for times killed by badSegs
        C = Coincidence2(gps(iGRB)-goodBefore,min_dur,badSegs{iIfo}(:,1),badSegs{iIfo}(:,2));

        % ---- Record times within GRBregion killed by segments 
        if isempty(C)
            killedTimes{iGRB,iIfo} = [0,0];
            liveTimes{iGRB,iIfo}   = [gps(iGRB)-goodBefore,min_dur];
            % ---- if no times have been killed we look at this GRB
            %networkString{iGRB} = [networkString{iGRB}, ifos{iIfo}];
        else
            killedTimes{iGRB,iIfo} = [C(:,1) C(:,2)];   
            liveTimes{iGRB,iIfo}   = ComplementSegmentList(C(:,1),C(:,2),...
                gps(iGRB)-goodBefore,gps(iGRB)-goodBefore+min_dur);
        end
        % ---- There may > 1 segment of data killed within GRB region
        %      Find total time killed 
        totKilledTime{iGRB,iIfo}    = sum(killedTimes{iGRB,iIfo}(:,2));

        % ---- If more than maxTimeKilled for this GRB and ifo, set
        %      corresponding grbMask to 0
        if totKilledTime{iGRB,iIfo} > maxTimeKilled
            grbMask(iGRB,iIfo) = 0;
        end 

    end %-- End loop over ifos

    % ---- Find intersection of liveTimes from all ifos
    intersectLiveTimes{iGRB} = liveTimes{iGRB,1};
    for iIfo = 2:numIfos
        C = Coincidence2(intersectLiveTimes{iGRB}(:,1),intersectLiveTimes{iGRB}(:,2),...
                         liveTimes{iGRB,iIfo}(:,1),liveTimes{iGRB,iIfo}(:,2));

        intersectLiveTimes{iGRB} = [C(:,1), C(:,2)]; 
    end
       
    finalKilledTimes{iGRB} = ComplementSegmentList(...
        intersectLiveTimes{iGRB}(:,1),intersectLiveTimes{iGRB}(:,2),...
        gps(iGRB)-goodBefore,gps(iGRB)-goodBefore+min_dur); 
 

end %-- End loop over GRBs

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 Write file containing all GRBs in our interval.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

filename = ['grbs_all_' usertag '.dat'];
disp(['Writing list of GRBs to ' filename]);

fid = fopen(filename,'w');
% ---- Loop over GRBs
for iGRB = 1:numberGRBs


    fprintf(fid,'%s ',GRB{2}{iGRB});
    fprintf(fid,'%13.3f %g %g\n',[GRB{4}(iGRB,:), ...
        str2double(GRB{5}(iGRB)), str2double(GRB{6}(iGRB))]);
end %-- End loop over GRBs
fclose(fid);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            Summary table
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(1,['This table shows the number of seconds killed '...
    'in the interval [-%5.2f,+%5.2f] about each GRB by the '...
    'segments provided \n\n'] ,goodBefore,goodAfter);
fprintf(1,'       GRB  ');
for iIfo = 1:numIfos
    fprintf(1,'  %s   ',ifos{iIfo});
end
fprintf(1,'\n');
for iGRB = 1:numberGRBs
    fprintf(1,'%10s  ',GRB{2}{iGRB});
    for iIfo = 1:numIfos
        fprintf(1,'%5.2f  ', totKilledTime{iGRB,iIfo});
    end
    fprintf(1,'\n');
end


% ---- Old code used to write lists of GRBs in various networks
%      This will prob need to be done by hand now....
writeOutput = 0;

if writeOutput

    % ---- Array used to record number of GRBs in single, double, triple
    %      etc times can currently handle up to 6 ifos
    numGRB  = zeros(6,1);

    % ---- Write GRB lists to files by coincidence combination.
    coincidenceComb = unique(coincidenceString);
    numGRBCoincComb = zeros(length(coincidenceComb));

    for ii = 1:length(coincidenceComb)
        if (~isempty(coincidenceComb{ii}))
           % ---- Find all GRBs in this coincidence combination.
           numGRBCoincComb(ii) = ...
               sum(strcmp(coincidenceString,coincidenceComb{ii}));
           disp(['Network ' coincidenceComb{ii} ...
                 ': Contains ' num2str(numGRBCoincComb(ii)) ' GRBs']);

           % ---- calc values needed for GRB statistics
           % ---- number of ifos in this coinc type
           numIfos = length(coincidenceComb{ii})/2;
           numGRB(numIfos) = numGRB(numIfos) + numGRBCoincComb(ii);

           % ---- Write file listing these GRBs.
           filename = ['grbs_' coincidenceComb{ii} '_-' ...
               num2str(goodBefore) '_+' num2str(goodAfter) ...
               '_' usertag '.txt'];
           fid = fopen(filename,'w');
 
           for iGRB = find(strcmp(coincidenceString,coincidenceComb{ii})).';
                if or((~strcmp(GRB{5}{iGRB},'n/a')),...
                      (~strcmp(GRB{6}{iGRB},'n/a')))
                    fprintf(fid,'%s ',GRB{2}{iGRB});
                    fprintf(fid,'%13.3f %g %g\n',[GRB{4}(iGRB,:), ...
                        str2double(GRB{5}(iGRB)), str2double(GRB{6}(iGRB))]);
                else
                    disp(['GRB ' GRB{2}{iGRB} ...
                        ' had n/a for RA or Dec, removing it from list']) 
                    numGRBCoincComb(ii) = numGRBCoincComb(ii) - 1;
                end 
           end
           disp(['Network ' coincidenceComb{ii} ...
               ': Writing ' num2str(numGRBCoincComb(ii)) ...
               ' GRBs to ' filename]);
           disp(' ');
           fclose(fid); 
        end
    end

    % ---- loop over GRBs
    filename = ['grbs_all_-' num2str(goodBefore) '_+' ...
        num2str(goodAfter) '_' usertag '.html'];
    fid = fopen(filename,'w');
    fprintf(fid,'%s\n',['<table border=1 cellspacing="1" cellpadding="5">']);
    fprintf(fid,'%s\n',['<tr>'...
                    '<td><b>GRB name</b></td>'...
                    '<td><b>Science run</b></td>'...
                    '<td><b>Detector network</b></td>'...
                    '<td><b>Notes</b></td>'...
                    '<td><b>Results</b></td>'...
                    '</tr>'
                      ]);
 
    for iGRB =1:numberGRBs

        timeString = '';   
        if gps(iGRB) < tStartS5
            timeString = 'preS5     ';
        elseif and((gps(iGRB) >= tStartS5),(gps(iGRB) < tStartVSR1))
            timeString = 'S5preVirgo';
        elseif and((gps(iGRB) >= tStartVSR1),(gps(iGRB) < tEndVSR1))
            timeString = 'S5VSR1    ';
        elseif gps(iGRB) >= tEndVSR1
            timeString = 'postS5    ';
        else
            error('Something has gone wrong with determing when GRB occurred');
        end 

        fprintf(fid,'%s\n',[' <tr><td> '  GRB{2}{iGRB} ...
                            ' </td><td> ' timeString  ...
                            ' </td><td> ' coincidenceString{iGRB} ...
                            ' </td><td> ' ...
                            ' </td><td> ' ...
                            ' </td><tr>']); 
    end
    fprintf(fid,'%s\n','</table>');
    fclose(fid);

    disp('GRB network statistics:');
    disp(['Percentages are fractions of: ' num2str(numberGRBs) ]);

    for idx = 1:length(numGRB)
        disp(['Number of GRBs in ' num2str(idx) ' ifo times: ' ...
            num2str(numGRB(idx)) ' (' ...
            num2str(100 * numGRB(idx)/numberGRBs)  '%)'  ]);
    end

    disp(['Total number of GRBs in at least one ifo: ' ...
        num2str(sum(numGRB)) ' (' num2str(100 * sum(numGRB)/numberGRBs) '%)' ]);

end % -- End if writeOutput 

% ---- Done.
return;


