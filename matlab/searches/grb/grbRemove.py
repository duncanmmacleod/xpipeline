#!/usr/bin/env python

"""
Script for removing results files created by grb.py when
they are distributed on nodes.
$Id$
"""

import sys, os, shutil, math, random, copy, getopt, re, string, time
import configparser

# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
params_file = None
# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hd:"
# ---- Long form.
longop = [
    "distrib-file="
    ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-d", "--distrib-file"):
        distrib_file = a      
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not distrib_file:
    print("No distribution file specified.", file=sys.stderr)
    print("Use --distrib-file to specify it.", file=sys.stderr)
    sys.exit(1)






#--------------------------------------------------------------------------
#     Remove off source files
#--------------------------------------------------------------------------


nodeList = open( distrib_file,'r')
nodeList.readline()
nodeList.readline()
nodeList.readline()
nodeList.readline()
s= nodeList.readline()
m = re.search(' (\d+)',s)
numberOfJobs = int(m.string[m.start()+1:m.end()])
s= nodeList.readline()
m = re.search(' (\d+)',s)
numberOfJobsPerNode = int(m.string[m.start()+1:m.end()])

for node in range(1,int(math.ceil(float(numberOfJobs)/float(numberOfJobsPerNode))+1)):
    nodeList.readline()
    for i in range(1,min(numberOfJobsPerNode,numberOfJobs-(node-1)*numberOfJobsPerNode) +1):
        s= nodeList.readline()
        print(('Removing: ' + s[0:-2]))
        os.system('rm -f ' + s)
    m = re.search('/[_a-zA-Z0-9]+.mat',s)
    print(('Removing: ' + (m.string[0:m.start()])))
    os.system('rmdir ' + (m.string[0:m.start()]))

