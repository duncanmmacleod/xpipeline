#!/usr/bin/env python
"""
XCOPYRUN - copies and X-Pipeline run directory, excluding tempoary files

$Id$

"""
__author__  = 'Patrick Sutton <patrick.sutton@ligo.org>'
__date__    = '$Date$'
__version__ = '$Revision$'

import os, sys, getopt
import configparser


# ---- Function usage.
def usage():
    msg = """\
Usage: 
    xcopyrun [options]
    --source-dir <path>     [REQUIRED] Path to directory to be copied (absolute
                            or relative).
    --target-dir <path>     [REQUIRED] Path to copy (absolute or relative).
    -h, --help              Display this message and exit.

  e.g.,
    xcopyrun --source-dir  /home/gareth.jones/S5/GRB070508 --target-dir /home/patrick.sutton/test/GRB070508

This script copies the required files and directory structure but does not edit them.
If copying files from another user you will need to edit the accounting_group_user and
log entries in any .sub files in order to submit condor jobs.
"""
    print(msg, file=sys.stderr)


def xcopyrun(source_dir,target_dir):

    # -----------------------------------------------------------------------------
    #    
    # -----------------------------------------------------------------------------

    # ---- Procedure:
    #        1. Verify that source_dir exists.
    #        2. If target_dir exists, exit else create target_dir/output/.
    #        3. Copy source_dir/output/*.mat
    #        4. Copy source_dir/input/, source_dir/*.* 
    #        5. If source_dir/auto_web exists then create target_dir/auto_web/
    #        6. Copy source_dir/auto_web/*.*
    #        7. If exists source_dir/auto_web/*_figfiles/ copy to target_dir/auto_web/ 
    #        8. If exists source_dir/auto_web/*_figures/ copy to target_dir/auto_web/ 

    print('Copying X-Pipeline run files from source directory: ' + source_dir)
    print('  to target directory: ' + target_dir)
    print('  This may take several minutes.') 

    # ---- Ensure directory names end with '/'.
    if not(source_dir.endswith('/')):
        source_dir = source_dir + '/'
    if not(target_dir.endswith('/')):
        target_dir = target_dir + '/'

    # ---- Verify source directory exists.
    if not os.path.isdir(source_dir):
        print('Error: source directory does not exist.', file=sys.stderr)
        exit(21)

    # ---- Verify target directory DOES NOT exist already, and create it.
    if os.path.isdir(target_dir):
        print('Error: target directory already exists.', file=sys.stderr)
        exit(22)
    else:
        os.system('mkdir -p ' + target_dir)

    # ---- Copy source directory structure to target.
    #      This command requires source_dir to include a trailing '/'.
    os.system("rsync -a --include='*/' --exclude='*' " + source_dir + " " + target_dir)

    # # ---- Copy source_dir/output/*.mat
    # os.system('cp ' + source_dir + 'output/*.mat ' + target_dir + 'output/')

    # ---- Copy ALL contents of source_dir/output/ (including contents of subdirectories).
    os.system('cp -r ' + source_dir + 'output/ ' + target_dir)

    # ---- Copy source_dir/input/, source_dir/*.* 
    os.system('cp -r ' + source_dir + 'input/ ' + target_dir)
    os.system('cp    ' + source_dir + '*.*    ' + target_dir)

    # ---- If source_dir/auto_web exists then create target_dir/auto_web/ .
    if os.path.isdir(source_dir + 'auto_web'):
        #os.system('mkdir -p ' + target_dir + 'auto_web')
        # ---- Copy source_dir/auto_web/*.*
        os.system('cp ' + source_dir + 'auto_web/*.* ' + target_dir + 'auto_web')
        # ---- If exists source_dir/auto_web/*_figfiles/ or 
        #      source_dir/auto_web/*_figures/ copy to target_dir/auto_web/ 
        dir_contents = os.listdir(source_dir + 'auto_web')
        for name in dir_contents:
            #print name, " ", os.path.isdir(name), " ", name.endswith('_figfiles'), " ", name.endswith('_figures')
            if  name.endswith('_figfiles') or name.endswith('_figures') :
                os.system('cp -r ' + source_dir + 'auto_web/' + name + ' ' + target_dir + 'auto_web')


if __name__ == '__main__':
    # ---- This function is being executed directly as a script (as opposed to 
    #      being imported by another code). Parse the command line arguments 
    #      then run the xcopyrun() function.

    # -------------------------------------------------------------------------
    #      Parse the command line options.
    # -------------------------------------------------------------------------

    # ---- Initialize variables.
    source_dir = None
    target_dir = None

    # ---- Syntax of options, as required by the getopt command.
    # ---- Short form.
    shortop = "h"
    # ---- Long form.
    longop = [
        "help",
        "source-dir=",
        "target-dir="
        ]

    # ---- Get command-line arguments.
    try:
        opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
    except getopt.GetoptError:
        usage()
        sys.exit(1)

    # ---- Parse command-line arguments.  Arguments are returned as strings, so 
    #      convert type as necessary.
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit(0)
        elif o in ("--source-dir"):
            source_dir = a      
        elif o in ("--target-dir"):
            target_dir = a      
        else:
            print("Unknown option:", o, file=sys.stderr)
            usage()
            sys.exit(1)

    # ---- Check that all required arguments are specified, else exit.
    if not source_dir:
        print("No source directory specified. Use the --source-dir option to specify one:", file=sys.stderr)
        print() 
        usage()
        sys.exit(1)
    if not target_dir:
        print("No target directory specified. Use the --target-dir option to specify one:", file=sys.stderr)
        print() 
        usage()
        sys.exit(1)

    # ---- Call the xcopyrun() function.
    xcopyrun(source_dir,target_dir)


