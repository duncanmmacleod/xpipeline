function xjittermass(inFile,outFile,mstring)
% XJITTERMASS - Generated a population masses instead of fixed values
%
% XJITTERMASS modifies an X-Pipeline formatted injection
% log file to include uncertainty in masses for binary systems
%
% usage:
%
%  xjittermass(inFile,outFile,mstring)
%
%  inFile           String.  Name of input injection file to be modified.
%  outFile          String.  Name of output file with modified injections.
%  mstring         String.  Tilde delimited string giving the mean,
%                   standard deviation, minimal and maximal mass for the
%                   first compact object, then the second compact object
%                   and finally the minimal and maximal total mass for
%                   the binary. All masses are in solar mass unit.
%
% $Id$

% ---- Check input.
error(nargchk(3, 3, nargin, 'struct'))

if ~ischar(inFile) | ~ischar(outFile)
    error('Inputs inFile, outFile must strings.');
end


% ---- convert tilde strings into parameters vectors
mParams = tildedelimstr2numorcell(mstring);
% ---- check mass parameters length
if length(mParams) ~= 10
  error(['The mass parameters tilde string is wrong. 10 parameters are ' ...
         'expected found ' num2str(length(mParams))]);
end
% ---- separate mass parameters
m1 = mParams(1:4);
m2 = mParams(5:8);
mt = mParams(9:10);


% ---- Read and parse input injection file.
injection = readinjectionfile(0,1e10,inFile);
[gps_s, gps_ns, phi, theta, psi, temp_name, temp_parameters] = ...
    parseinjectionparameters(injection);
nInjection = length(gps_s);
% ---- Parser code foolishly outputs "name" and "parameters" as cell arrays
%      of 1x1 cell arrays of strings; reformat to remove extra layer of
%      cell arrays.
name = cell(nInjection,1);
parameters = cell(nInjection,1);
for ii = 1:nInjection
    name{ii} = temp_name{ii}{1};
    parameters{ii} = temp_parameters{ii}{1};
end

paramArray = cell(nInjection,1);
amplIndex = zeros(nInjection,1);
for ii = 1:nInjection
    switch lower(name{ii})
        case {'inspiral','inspiralsmooth','lalinspiral'}  
            % ---- Post-Newtonian inspiral.  Parameters are
            %      [mass1,mass2,iota,dist].  
            paramArray{ii} = tildedelimstr2numorcell(parameters{ii},1);
        otherwise
            error(['Current version of the function can only handle ' ... 
                   'lalinspiral, inspiral and inspiralsmooth waveform types.  See xmakewaveform.']); 
    end
end

% ---- Set the random number generator seed based on the name of the
% input file
randn('state',sum(inFile))

% ---- Check consistency of recovered masses and jittering parameters
tol=1e-3;
if abs(paramArray{1}{1} - m1(1))>tol || abs(paramArray{1}{2} - m2(1))>tol
  error(['Mass parameters are inconsistent. The file contains (m1,m2)=(' ...
         num2str(paramArray{1}{1}) ',' num2str(paramArray{1}{2}) ...
         ') and the jittering (m1,m2)=(' num2str(m1(1)) ',' num2str(m2(1)) ...
         ')']);
end

% don't jitter first injection, needed for post-processing reference
for iInj = 2:nInjection
  % ---- Generating random mass parameters
  newm1 = 0;
  newm2 = 0;
  k = 0;
  while k<100 && ( newm1 < m1(3) || newm1 > m1(4) || newm2 < m2(3) || ...
                   newm2 > m2(4) || newm1+newm2 < mt(1) || newm1+newm2 > mt(2))
    newm1 = m1(1) + m1(2)*randn(1);
    newm2 = m2(1) + m2(2)*randn(1);
    k = k + 1;
  end
  if k >= 100
    error(['The mass jittering did not converenge. Are the jittering ' ...
           ' parameters consistent?']);
  end
  paramArray{iInj}{1} = newm1;
  paramArray{iInj}{2} = newm2;
end



% ---- Write down the jittered injection parameters
numData = cell(1,1);
nameData = cell(1,1);
paramData = cell(1,1);
numData{1} = [gps_s(:,1), gps_ns(:,1), phi, theta, psi];
nameData{1} = name;
paramData{1} = cell(nInjection,1);
for ii = 1:nInjection
  paramData{1}{ii} = numorcell2tildedelimstr(paramArray{ii});
end

%----- Write jittered injection log file.
fid = fopen(outFile,'w');
for ii = 1:nInjection
  %----- Write signal type and parameters to output file.
  fprintf(fid,'%d %d %e %e %e ',numData{1}(ii,:));
  fprintf(fid,'%s ',nameData{1}{ii});
  fprintf(fid,'%s ',paramData{1}{ii});
  fprintf(fid,'\n');
end
fclose(fid);

% ---- Done.
return
