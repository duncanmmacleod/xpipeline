#!/usr/bin/env python


"""
Script that sets up postprocessing (xmakegrbwebpage) jobs for a single GRB. 
$Id$
"""

# ---- Import standard modules to the python path.
import sys, os, subprocess, getopt, string, re
import configparser
from glue import pipeline


# ---- Function usage.
def usage():
    msg = """\
Usage: 
  xgrbwebpage.py [options]
  -g, --grb-name <string>     [REQUIRED] Name of GRB, used to name the output files. 
  -d, --grb-dir <path>        [REQUIRED] Path to grb dir.
  -a, --auto-dir <path>       [REQUIRED] Path where to put auto_web dir; can be the same
                              as or different from --grb-dir. 
  -w, --web-dir <path>        [REQUIRED] Path to output web page.  We will create a dir 
                              <web-dir>/<grb-name>_<user-tag> to which we will copy the 
                              output html and figure files.
  -l, --log-dir <path>        [REQUIRED] Path to write condor log files to; must be on a 
                              local file system (not in your home directory). 
  -u, --user-tag <string>     [REQUIRED] Descriptive tag used in output file names.
  -c, --veto-method <type>    [REQUIRED] Type of veto cut to perform.  Must be one of 
                              '(linear|median|medianLin|alpha|medianAlpha|alphaLin)
                               (Cut|CutCirc|CutAmp|CutScalar)', e.g. 'medianAlphaCutCirc'.
  -p, --percentile <string>   [REQUIRED] String in "XX-YY" format, where XX is percentile 
                              of the background distribution to use for tuning and YY is 
                              the percentile to be used for upper limit estimation.
  --freq-veto <path>          Path to file with frequency bands to veto.
  -s, --add-stat <string>     Name an additional detection statistic for which the tuning
                              will be done.
  --priority <prio>           Integer specifying the priority of the condor jobs. Higher 
                              priority jobs are submitted to the cluster before lower 
                              priority jobs by the same user. Default value is 0.
  --big-mem <memory>          Integer specifying the minimal memory requirement for condor
                              jobs in MB.
  --tuning-waveforms <string> Tilde-delimited string of waveform names to be used for
                              tuning.  Waveforms must be a subset of the sets injected.
                              Names must be in *lower case*, and exactly match those in
                              the [waveforms] section of the .ini file, e.g. 
                              "csg500Q9incljitter5~csg1000q9incljitter5".
                              Default 'all' tunes using all waveforms.
  --user                      User's Albert.Einstein username. If not specified then the 
                              whoami system comand will be used to guess it.
  -e, --event-display <#>     Boolean (1 or 0) flag to enable event display generation. 
                              If 1, event display scripts and condor dags are automatically
                              run as part of 'post' scripts for closedbox and openbox results. 
  -b, --batch-mode            Flag required when running xgrbwebpage.py from xbatchgrbwebpage.py;
                              do not use when calling xgrbwebpage.py directly.
  -h, --help                  Display this message and exit.

Example:

xgrbwebpage.py  \\
    -g GRB190407575 \\
    -d /home/patrick.sutton/test_runs/examples_r5768/grb \\
    -a /home/patrick.sutton/test_runs/examples_r5768/grb \\
    -w /home/patrick.sutton/public_html/GRB/ \\
    -l /local/patrick.sutton/ \\
    -c alphaLinCutCirc \\
    -p 99-99 \\
    -s powerlaw \\
    -u r5768 \\
    --big-mem 8000 \\
    --user patrick.sutton

Notes: 
- All paths need to be global.  Use `pwd` instead of ./ to obtain a localpath 
  that is expanded to a global path by your shell.
- The GRB name can differ from the name supplied to grb.py.  However, it is 
  recommended to use the user-tag option below to create unique names for the 
  output and to keep the grb-name something sensible, e.g., GRB051211.
- Making auto-dir different from grb-dir is useful when post-processing 
  triggers generated in a different run or by another user for whose dirs you 
  lack write permission.

This script sets up the directory structure and creates the condor submit files
required to run postprocessing (xmakegrbwebpage) jobs on a cluster. Once you 
have run this script you will find that a shell script has been created:

   run_xmakegrbwebpageDAGJobs_<user-tag>_<grb-name>.sh : 
      run this script to launch the post processing condor jobs

"""
    print(msg, file=sys.stderr)


# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
params_file    = None
grb_list       = None
grbscript      = None
detectors      = []
stat_list      = ['default']
condorPriority = "0"
minimalMem     = False
batch_mode     = False
veto_method    = []
prePassFlag    = False
# ---- Guess the condor username.
UserName       = os.popen('whoami').read().rstrip()

# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hd:g:a:w:l:u:c:bs:p:e:"
# ---- Long form.
longop = [
   "help",
   "grb-dir=",
   "grb-name=",
   "auto-dir=",
   "web-dir=",
   "log-dir=",
   "user-tag=",
   "veto-method=",
   "batch-mode",
   "freq-veto=",
   "add-stat=",
   "percentile=",
   "priority=",
   "big-mem=",
   "tuning-waveforms=",
   "user=",
   "event-display="
   ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- We will record the command line arguments to grb.py in a file called
#      xgrbwebpage.param
command_string = 'xgrbwebpage.py '

# ---- Set default values for various optional inputs.
tuning_waveforms = ''
event_display = 0
freq_veto_file = 0

# ---- Parse command-line arguments.  Arguments are returned as strings, so 
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-d", "--grb-dir"):
        grb_dir = str(a)      
        command_string = command_string + ' -d ' + a
    elif o in ("-g", "--grb-name"):
        grb_name = str(a)     
        command_string = command_string + ' -g ' + a
    elif o in ("-a", "--auto-dir"):
        auto_dir = str(a)      
        command_string = command_string + ' -a ' + a
    elif o in ("-w", "--web-dir"):
        web_dir = str(a)      
        command_string = command_string + ' -w ' + a
    elif o in ("-l", "--log-dir"):
        log_dir = str(a)      
        command_string = command_string + ' -l ' + a
    elif o in ("-u", "--user-tag"):
        user_tag = str(a)      
        command_string = command_string + ' -u ' + a
    elif o in ("--freq-veto"):
        freq_veto_file = str(a)
        command_string = command_string + ' --freq-veto ' + a
    elif o in ("--add-stat", "-s"):
        stat_list.append(str(a))
        command_string = command_string + ' -s ' + a
    elif o in ("-c", "--veto-method"):
        veto_method = str(a)      
        command_string = command_string + ' -c ' + a
    elif o in ("-b", "--batch-mode"):
        batch_mode = True      
        command_string = command_string + ' -b '
    elif o in ("-p", "--percentile"):
        percentile_str = str(a)
        command_string = command_string + ' -p ' + a
        # Parse percentile string in order to pass it over to xmakegrbwebpage
        percentile_str = percentile_str.replace("-"," ")
    elif o in ("--priority"):
        condorPriority = a
        command_string = command_string + ' --priority ' + a
    elif o in ("--tuning-waveforms"):
        tuning_waveforms = a
        command_string = command_string + ' --tuning-waveforms ' + a
    elif o in ("-e","--event-display"):
        event_display = int(a)
        command_string = command_string + ' --event-display ' + a
    elif o in ("--big-mem"):
        minimalMem = a + 'M'
        command_string = command_string + ' --big-mem ' + a
    elif o in ("--user"):
        UserName = a
        command_string = command_string + ' --user ' + a
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not grb_dir:
    print("No grb-dir specified.", file=sys.stderr)
    print("Use --grb-dir to specify it.", file=sys.stderr)
    sys.exit(1)
if not grb_name:
    print("No grb-name specified.", file=sys.stderr)
    print("Use --grb-name to specify it.", file=sys.stderr)
    sys.exit(1)
if not auto_dir:
    print("No auto-dir specified.", file=sys.stderr)
    print("Use --auto-dir to specify it.", file=sys.stderr)
    sys.exit(1)
if not web_dir:
    print("No web-dir specified.", file=sys.stderr)
    print("Use --web-dir to specify it.", file=sys.stderr)
    sys.exit(1)
if not log_dir:
    print("No log-dir specified.", file=sys.stderr)
    print("Use --log-dir to specify it.", file=sys.stderr)
    sys.exit(1)
if not user_tag:
    print("No user-tag specified.", file=sys.stderr)
    print("Use --user-tag to specify it.", file=sys.stderr)
    sys.exit(1)
if not percentile_str:
    print("No percentile levels specified.", file=sys.stderr)
    print("Use --percentile to specify it.", file=sys.stderr)
    sys.exit(1)
if not veto_method:
    print("No veto-method specified.", file=sys.stderr)
    print("Use --veto-method to specify it.", file=sys.stderr)
    sys.exit(1)
# -- BEWARE: Regular expression documenation: A|B - When one pattern
#    completely matches, that branch is accepted. This means that once
#    A matches, B will not be tested further, even if it would produce
#    a longer overall match.
if not(re.match('(medianAlpha|alphaLin|medianLin|linear|median|alpha)(CutScalar|CutCirc|CutAmp|Cut)',veto_method) or veto_method == 'None'):
    print("veto-method is currently set to " + veto_method, file=sys.stderr)
    print("veto-method must be of the form '(medianAlpha|alphaLin|medianLin|linear|median|alpha)(CutScalar|CutCirc|CutAmp|Cut) or None'", file=sys.stderr)
    sys.exit(1)

if not freq_veto_file:
    freq_veto_file = 'None'

# ---- Check to see if we are running on Atlas
# ---- When running on Atlas we must add env flags to our .sub files

# ---- Get partial hostname 
os.system('hostname > host.txt')
f = open('host.txt','r')
hostname=f.read()
f.close()

# ---- Get full hostame 
os.system('hostname -f > host.txt')
f = open('host.txt','r')
fullhostname=f.read()
f.close()

os.system('rm host.txt')
# ---- Check hostname and set flag if necessary
if 'atlas' in fullhostname:
    atlasFlag = 1
elif 'h2' in hostname:
    atlasFlag = 1
elif 'coma' in fullhostname:
    atlasFlag = 1
else:
    atlasFlag = 0

# ---- Status message.  Report all supplied arguments.
print(file=sys.stdout)
print("####################################################", file=sys.stdout)
print("#                    xgrbwebpage                   #", file=sys.stdout)
print("####################################################", file=sys.stdout)
print(file=sys.stdout)
print("Parsed input arguments:", file=sys.stdout)
print(file=sys.stdout)
print("          grb dir          :", grb_dir, file=sys.stdout)
print("          grb name         :", grb_name, file=sys.stdout)
print("          auto dir         :", auto_dir, file=sys.stdout)
print("          web dir          :", web_dir, file=sys.stdout)
print("          log dir          :", log_dir, file=sys.stdout)
print("          user tag         :", user_tag, file=sys.stdout)
print("          veto method      :", veto_method, file=sys.stdout)
print("          stat list        :", stat_list, file=sys.stdout)
print("          percentile       :", percentile_str, file=sys.stdout)
print("          frequency veto   :", freq_veto_file, file=sys.stdout)
if tuning_waveforms:
    print("          cuts tuned using :", tuning_waveforms, file=sys.stdout) 
if event_display:
    print("          event displays   : automatic", file=sys.stdout)
if atlasFlag:
    print("          condor getenv    : true", file=sys.stdout)
else:
    print("          condor getenv    : false", file=sys.stdout)
print("          user ID          :", UserName, file=sys.stdout)
print(file=sys.stdout)

if not tuning_waveforms:
    print("No waveforms specified for tuning coherent cuts.", file=sys.stdout)
    print("Will use all injected waveforms for tuning.", file=sys.stdout)
if not event_display:
    print("No event display generation requested.", file=sys.stdout)
    print("Run xgrbeventdisplay manually to make event webpages.", file=sys.stdout)


# -------------------------------------------------------------------------
#      Preparatory.
# -------------------------------------------------------------------------

# ---- Generate unique id tag, this is used when naming dagman log file
os.system('uuidgen > uuidtag.txt')
f = open('uuidtag.txt','r')
uuidtag=f.read().rstrip()
f.close()
os.system('rm uuidtag.txt')

# ---- Check dir names end in '/'
if not(auto_dir.endswith('/')):
    auto_dir = auto_dir + '/'
if not(web_dir.endswith('/')):
    web_dir = web_dir + '/'
if not(log_dir.endswith('/')):
    log_dir = log_dir + '/'
if not(grb_dir.endswith('/')):
    grb_dir = grb_dir + '/'

# ---- Set injectionType. Since MDCs are no longer supported, only the 
#      'internalInj' type is now supported.
injectionType = 'internalInj'


#------------------------------------------------------------------------------
#                             create dir structure 
#------------------------------------------------------------------------------

# ---- Record cwd so we can come back later
initial_dir = os.getcwd();
if not(initial_dir.endswith('/')):
    initial_dir = initial_dir + '/'

if not(os.path.exists(log_dir)):
    print('Making log dir:' + log_dir)
    os.makedirs(log_dir)

# ---- Make web directory for this GRB, this is where we will put the
#      results webpage etc 
web_dir_full =  web_dir + grb_name + '_' + user_tag
print('Making web dir: ' + web_dir_full)
try:
    os.makedirs(web_dir_full)
except OSError:
    print('WARNING: web dir already exists, continuing...') 

print('Making auto_web dir: ' + auto_dir + 'auto_web') 
try:
    os.makedirs(auto_dir + 'auto_web')
except OSError:
    print('WARNING: auto_dir already exists or permission denied? Try to continue...') 

os.chdir(auto_dir + 'auto_web')

# ---- Write ASCII file holding xgrbwebpage.py command.
pfile = open('xgrbwebpage.param','w')
pfile.write(command_string + "\n")
pfile.close()

# ---- Making log dirs for condor jobs
print('making auto_web/logs dir')
try:
    os.mkdir('logs')
except OSError:
    print('WARNING: logs dir already exists or permission denied? Try to continue...')

#------------------------------------------------------------------------------
#                          read grb.params file       
#------------------------------------------------------------------------------

# ---- copy over grb.param file for future reference
os.system('cp ' + grb_dir + 'grb.param' + ' grb.param')

# ---- read in args from grb.param file 
grbParams = []
fin = open(grb_dir + 'grb.param')
grbParams = fin.read()
fin.close

# ---- remove pesky newline token
#      NB, it only counts as one char
if grbParams.endswith('\n'):
    grbParams = grbParams[0:len(grbParams)-1]

# ---- extract grb gps time from grb.params and use lal_tconvert to
#      write UTC time to a file
grbParamsList=grbParams.split(' ')
gpsStr = grbParamsList[grbParamsList.index('-g')+1]
print('Writing UTC time to tmpUTCtime_53ab6e3fe.txt')
timeCommand = 'lal_tconvert ' + gpsStr + ' > tmpUTCtime_53ab6e3fe.txt' 
os.system(timeCommand)

iniFile = grbParamsList[grbParamsList.index('-p')+1]

# ---- Create configuration-file-parser object and read parameters file.
if iniFile.find('/') == 0 : # check if the ini file found is a global path
    params_file = iniFile
    iniFile = iniFile[iniFile.rfind('/')+1:]
else : 
    params_file = grb_dir + '/' + iniFile
    iniFile = iniFile[iniFile.rfind('/')+1:]
print('Param file used: ' + params_file)
print('Ini file name: ' + params_file)
cp = configparser.ConfigParser()
cp.read(params_file)
possibleDetectors= ['H1','K1','L1','G1','V1']
veto_file_names = ''
# ---- Locate the veto files, if any.
if cp.has_option('segfind','generateSegs') and int(cp.get('segfind','generateSegs'))==0:
    # ---- Use pre-existing veto files, if they are listed in the .ini file.
    for ifoIdx in range(0,len(possibleDetectors)):
        ifo = possibleDetectors[ifoIdx]
        # ---- If veto_list_cat2 is provided for this ifo in the
        #      params-file we will use it. 
        if cp.has_option(ifo,"veto-list"):
            veto_file_names = veto_file_names + ' ' + cp.get(ifo, "veto-list")
        else:
            veto_file_names = veto_file_names + ' None'
else:
    # ---- If generateSegs ~= 0 or not specified then the segment-list and 
    #      veto-list were auto-generated and are available in a known location.
    #      Note that from r6446 the veto files will always be available in this 
    #      location and the generateSegs flag is no longer required. We keep 
    #      the check above in order to be able to do post-processing on events
    #      analysed with older versions.
    for ifoIdx in range(0,len(possibleDetectors)):
        ifo = possibleDetectors[ifoIdx]
        thisIfoVetoFileName = grb_dir + '/input/' + ifo + '_cat24veto.txt'
        if os.path.isfile(thisIfoVetoFileName):
            veto_file_names = veto_file_names + ' ' + thisIfoVetoFileName
        else :
            veto_file_names = veto_file_names + ' None'
print(" Using DQ flags listed below to kill triggers", file=sys.stdout)
print(veto_file_names, file=sys.stdout)
# ---- Retrieve job retry number from parameters file.
if cp.has_option('condor','retryNumber'):
    retryNumber = int(cp.get('condor','retryNumber'))
else:
    retryNumber = 1

#------------------------------------------------------------------------------
#               read channels.txt to find which ifos were analysed       
#------------------------------------------------------------------------------

# ---- Figure out what ifos we are using, this is used to determine which
#      veto cuts we should use.

channels_file = grb_dir + 'input/channels.txt'
if os.path.exists(channels_file):
    f = open(channels_file,'r')
    lines = f.readlines()
    for line in lines:
        if possibleDetectors.count(line[0:2]): 
            detectors.append(line[0:2])
else:
    print("Required channels file does not exist:  " + \
    channels_file, file=sys.stderr)
    sys.exit(1)

if not(detectors):
    print("Error getting ifo names from channels file:  " + \
    channels_file, file=sys.stderr)
    sys.exit(1)

print('We will analyse the following detectors: ',  detectors)

#------------------------------------------------------------------------------
#           symlink files and dirs required by xmakegrbwebpage from the 
#                              grb_dir to auto_dir     
#------------------------------------------------------------------------------

if grb_dir != auto_dir:
    # ---- sym link input files to auto_dir + 'input'
    #      If this dir already exists ln -s will not do anything
    print('sym linking /input from ' + grb_dir)
    os.system('ln -s ' + grb_dir + 'input ' + auto_dir + 'input')

    # ---- sym link output files to auto_dir + 'output'
    #      If this dir already exists ln -s will not do anything
    print('sym linking /output from ' + grb_dir)
    os.system('ln -s ' + grb_dir + 'output ' + auto_dir + 'output')

print('sym linking ' + iniFile  + ' from ' + grb_dir)
os.system('ln -s ' + params_file + ' ' + iniFile) 

#------------------------------------------------------------------------------
#              create auto.txt file required by xmakegrbwebpage       
#------------------------------------------------------------------------------

makeCommand = 'makeAutoFiles2.py -p ' +  iniFile
print('Running makeAutoFiles2.py:')
print(makeCommand)
os.system(makeCommand)

# ---- If minimalMem is not provided try to come up with some sensible value
if not(minimalMem):
    # ---- use 3000MB plus 6 times the size of the off-source mat file
    offSourceFileSize = os.path.getsize(auto_dir + 'output/off_source_0_0_merged.mat')/1e6
    minimalMem = str(int(round(3000 + 6*offSourceFileSize))) + 'M'

# ---- Add the appropriate job tag to you post processing script!
grouptag = 'ligo.' + \
                cp.get('condor','ProdDevSim') + '.' +\
                cp.get('condor','Era') + '.' +\
                cp.get('condor','Group') + '.' +\
                cp.get('condor','SearchType')


#------------------------------------------------------------------------------
#       set waveforms for tuning (if given), else use 'all'       
#------------------------------------------------------------------------------

if tuning_waveforms:
    vetoSet = tuning_waveforms
else:
    vetoSet = 'all'

#------------------------------------------------------------------------------
#      Figure out which veto cuts to perform based upon ifos available 
#------------------------------------------------------------------------------

if re.match('(linearCut.*|alphaCut.*)',veto_method):
    noCuts            = [0]
    nullOneSidedRange = [0,-1,-1.01,-1.03,-1.06,-1.1,-1.15,-1.2,-1.27,-1.35,-1.45,-1.57,-1.71,-1.9,-2.1,-2.4,-2.7,-3,-3.3,-3.6,-4]
    oneSidedRange     = [0,-1,-1.01,-1.03,-1.06,-1.1,-1.15,-1.2,-1.27,-1.35,-1.45,-1.57,-1.71,-1.9,-2.1,-2.4,-2.7,-3,-3.3,-3.6,-4]
    twoSidedRange     = [0,1,1.01,1.03,1.06,1.1,1.15,1.2,1.27,1.35,1.45,1.57,1.71,1.9,2.1,2.4,2.7,3,3.3,3.6,4]
elif re.match('median.*',veto_method) or re.match('alpha.+Cut.*',veto_method):
    noCuts            = [0] 
    nullOneSidedRange = [0,-0.5,-1,-1.5,-2,-2.5,-3,-3.5,-4,-4.5,-5,-5.5,-6,-6.5,-7] 
    oneSidedRange     = [0,-0.5,-1,-1.5,-2,-2.5,-3,-3.5,-4,-4.5,-5,-5.5,-6,-6.5,-7] 
    twoSidedRange     = [0,0.18,0.22,0.27,0.34,0.43,0.54,0.67,0.84,1.05,1.31,1.64,2.05,2.56,3.2,4.0,5.0,6.0,7.0]
    if re.match('median.+Cut.*',veto_method) or re.match('alpha.+Cut.*',veto_method):
        prePassFlag          = True
        nullOneSidedRangePre = [0,-1,-1.01,-1.03,-1.06,-1.1,-1.15,-1.2,-1.27,-1.35,-1.45,-1.57,-1.71,-1.9,-2.1,-2.4,-2.7,-3,-3.3,-3.6,-4]
        oneSidedRangePre     = [0,-1,-1.01,-1.03,-1.06,-1.1,-1.15,-1.2,-1.27,-1.35,-1.45,-1.57,-1.71,-1.9,-2.1,-2.4,-2.7,-3,-3.3,-3.6,-4]
        twoSidedRangePre     = [0,1,1.01,1.03,1.06,1.1,1.15,1.2,1.27,1.35,1.45,1.57,1.71,1.9,2.1,2.4,2.7,3,3.3,3.6,4]
elif re.match('None',veto_method):
    noCuts = [0]
else:
    print("something has gone wrong with veto-method:  " + \
    veto_method, file=sys.stderr)
    sys.exit(1)

# ---- initialise veto ranges
nullRange  = []
plusRange  = []
crossRange = []

# ---- choose vetoes to perform depending on detector combinations   
if len(detectors)==1 or re.match('None',veto_method):
    # ----- one detector, one site
    # +   : E+ only (no I+) so dont perform any cuts
    nullRange  = noCuts
    plusRange  = noCuts
    crossRange = noCuts
elif len(detectors)==2:
    # ---- two detectors, two sites
    # null : no cuts
    # +    : two-sided test
    # x    : two-sided test
    nullRange  = noCuts
    plusRange  = twoSidedRange
    crossRange = twoSidedRange
    if re.match('(.*Scalar|.*Circ|.*Amp)',veto_method):
        plusRange  = oneSidedRange
        crossRange = oneSidedRange 
else:
    # ----- >2 detectors at >= 2 sites
    # null : one-sided test, In must be > En
    # +   : two-sided test
    # x   : two-sided test
    nullRange  = nullOneSidedRange
    plusRange  = twoSidedRange
    crossRange = twoSidedRange
    if re.match('(.*Scalar|.*Circ|.*Amp)',veto_method):
        plusRange  = oneSidedRange
        crossRange = oneSidedRange 

if prePassFlag:
    # ---- choose vetoes to perform depending on detector combinations   
    if len(detectors)==1 or re.match('None',veto_method):
        # ----- one detector, one site
        # +   : E+ only (no I+) so dont perform any cuts
        nullRangePre  = noCuts
        plusRangePre  = noCuts
        crossRangePre = noCuts
    elif len(detectors)==2:
        # ---- two detectors, two sites
        # null : no cuts
        # +    : two-sided test
        # x    : two-sided test
        nullRangePre  = noCuts
        plusRangePre  = twoSidedRangePre
        crossRangePre = twoSidedRangePre
        if re.match('(.*Scalar|.*Circ|.*Amp)',veto_method):
            plusRangePre  = oneSidedRangePre
            crossRangePre = oneSidedRangePre 
    else:
        # ----- >2 detectors at >= 2 sites
        # null : one-sided test, In must be > En
        # +   : two-sided test
        # x   : two-sided test
        nullRangePre  = nullOneSidedRangePre
        plusRangePre  = twoSidedRangePre
        crossRangePre = twoSidedRangePre
        if re.match('(.*Scalar|.*Circ|.*Amp)',veto_method):
            plusRangePre  = oneSidedRangePre
            crossRangePre = oneSidedRangePre 

#------------------------------------------------------------------------------
#        Split up range of veto cuts between different condor jobs 
#------------------------------------------------------------------------------
# ---- Here we create strings which will be used as argument to 
#      xmakegrbwebpage. For each of the null, plus and cross likelihoods we
#      construct a string consisting of the vetoType and the range of veto 
#      cuts to test, e.g., nullEoverI [0,-1,-1.05]  
# ---- We parallelize the analysis by splitting the full range of plus and 
#      cross veto cuts across many separate xmakegrbwebpage condor jobs.

nullStr  = []
plusStr  = []
crossStr = []

nullVetoType = 'nullIoverE'
plusVetoType = 'plusEoverI'
crossVetoType = 'crossEoverI'
if re.match('.*Circ',veto_method):
    plusVetoType = 'circEoverI'
    crossVetoType = 'circnullIoverE'
if re.match('.*Amp',veto_method):
    plusVetoType = 'ampEoverI'
    crossVetoType = 'ampnullIoverE'
if re.match('.*Scalar',veto_method):
    plusVetoType = 'scalarEoverI'
    crossVetoType = 'scalarnullIoverE'

for plusIdx in range(len(plusRange)):

    # ---- If we are making cuts in all three likelihoods we will also
    #      split up our cross cuts
    if (len(nullRange)>1) and (len(crossRange)>1): 
        for crossIdx in range(len(crossRange)): 

            temp     = str(nullRange)
            # ---- str command turns list into a string, it introduces
            #      white space between elements (which are separated by
            #      commas anyway). We must remove this white space so that
            #      these strings are not misinterpreted by xmakegrbwebpage
            #      as separate arguments.
            nullStr.append(' ' + nullVetoType + ' ' + temp.replace(' ','') + ' ')  
            plusStr.append(' ' + plusVetoType + ' [' + str(plusRange[plusIdx]) + '] ')
            crossStr.append(' ' + crossVetoType + ' [' + str(crossRange[crossIdx]) + '] ')

    # ---- If we are only making cuts in one or two likelihoods we will only
    #      split up our plus cuts
    else:
        temp     = str(nullRange)
        nullStr.append(' ' + nullVetoType + ' ' + temp.replace(' ','') + ' ')
        temp     = str(crossRange)     
        crossStr.append(' ' + crossVetoType +' ' + temp.replace(' ','') + ' ')
        plusStr.append(' ' + plusVetoType + ' [' + str(plusRange[plusIdx]) + '] ')

#------------------------------------------------------------------------------
#        Split up range of veto cuts between different condor jobs 
#------------------------------------------------------------------------------
if prePassFlag :
    # ---- Same as above but for the pre pass with linear cut
    nullStrPre  = []
    plusStrPre  = []
    crossStrPre = []

    for plusIdx in range(len(plusRangePre)):

        # ---- If we are making cuts in all three likelihoods we will also
        #      split up our cross cuts
        if (len(nullRangePre)>1) and (len(crossRangePre)>1): 
            for crossIdx in range(len(crossRangePre)): 

                temp     = str(nullRangePre)
                # ---- str command turns list into a string, it introduces
                #      white space between elements (which are separated by
                #      commas anyway). We must remove this white space so that
                #      these strings are not misinterpreted by xmakegrbwebpage
                #      as separate arguments.
                nullStrPre.append(' ' + nullVetoType + ' ' + temp.replace(' ','') + ' ')  
                plusStrPre.append(' ' + plusVetoType + ' [' + str(plusRangePre[plusIdx]) + '] ')
                crossStrPre.append(' ' + crossVetoType + ' [' + str(crossRangePre[crossIdx]) + '] ')

        # ---- If we are only making cuts in one or two likelihoods we will only
        #      split up our plus cuts
        else:
            temp     = str(nullRangePre)
            nullStrPre.append(' ' + nullVetoType + ' ' + temp.replace(' ','') + ' ')
            temp     = str(crossRangePre)     
            crossStrPre.append(' ' + crossVetoType +' ' + temp.replace(' ','') + ' ')
            plusStrPre.append(' ' + plusVetoType + ' [' + str(plusRangePre[plusIdx]) + '] ')



# -------------------------------------------------------------------------
#      Define condor job classes.
# -------------------------------------------------------------------------

class XwebJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An xmakegrbwebpage job
    """
    def __init__(self,submitFileName):
        """
        """
        # ---- Get path to executable.
        os.system('which xmakegrbwebpage > path_file.txt')
        f = open('path_file.txt','r')
        xdetectionstr = f.read()
        f.close()
        os.system('rm path_file.txt')
        self.__executable = xdetectionstr

        # ---- Get condor universe from parameters file.
        self.__universe = 'vanilla'
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)

        # ---- Add required environment variables.
        self.add_condor_cmd('environment',"\"USER=$ENV(USER) HOME=$ENV(HOME) " \
            "PATH=$ENV(PATH) LIGOTOOLS=$ENV(LIGOTOOLS) " \
            "MCR_CACHE_ROOT=$$(CondorScratchDir) " \
            "X509_USER_PROXY=$ENV(HOME)/x509up.file" + uuidtag + "\"")

        # ---- Set 'getenv = true' for all cluster, otherwise ligolw_dq_query fails at multiple stages
        self.add_condor_cmd('getenv',"true")

        # ---- Add Accounting Group Flag.
        self.add_condor_cmd('accounting_group',grouptag)

        # ---- Add UserName Flag.
        self.add_condor_cmd('accounting_group_user',UserName)

        # ---- Add priority specification.
        self.add_condor_cmd('priority',condorPriority)

        # ---- Add memory request.
        if minimalMem:
            self.add_condor_cmd('request_memory',minimalMem)

        # ---- Add mandatory request_disk request.
        minimalDiskPost = "30G"
        self.add_condor_cmd('request_disk',minimalDiskPost)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xmakegrbwebpage-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xmakegrbwebpage-$(cluster)-$(process).err')

        # ---- Name of condor job submission file to be written.
        self.set_sub_file(submitFileName)

class XwebNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    An xmakegrbwebpage node
    """
    def __init__(self,job):
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)
        self.__args = None

    def set_dir_prefix(self,path):
        self.add_var_arg(path)
        self.__dir_prefix = path

    def get_dir_prefix(self):
        return self.__dir_prefix

    def set_args(self,args):
        self.add_var_arg(args)
        self.__args = args

    def get_args(self):
        return self.__args

class XtuneJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An xtunegrbwebpage job
    """
    def __init__(self):
        """
        """
        # ---- Get path to executable.
        os.system('which xtunegrbwebpage > path_file.txt')
        f = open('path_file.txt','r')
        xdetectionstr = f.read()
        f.close()
        os.system('rm path_file.txt')
        self.__executable = xdetectionstr

        # ---- Get condor universe from parameters file.
        self.__universe = 'vanilla'
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)

        # ---- Add required environment variables.
        self.add_condor_cmd('environment',"\"USER=$ENV(USER) HOME=$ENV(HOME) " \
            "MCR_CACHE_ROOT=$$(CondorScratchDir)\"")

        # ---- Add Accounting Group Flag.
        self.add_condor_cmd('accounting_group',grouptag)

        # ---- Add UserName Flag.
        self.add_condor_cmd('accounting_group_user',UserName)

        # ---- Add priority specification.
        self.add_condor_cmd('priority',condorPriority)

        # ---- Add mandatory request_disk request.
        minimalDiskTune = "10M"
        self.add_condor_cmd('request_disk',minimalDiskTune)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xtunegrbwebpage-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xtunegrbwebpage-$(cluster)-$(process).err')

        # ---If on Atlas, add getenv=true to sub file
        if atlasFlag:
            self.add_condor_cmd('getenv',"true")

        # ---- Name of condor job submission file to be written.
        self.set_sub_file('xtunegrbwebpage.sub')

class XtuneNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    An xtunegrbwebpage node
    """
    def __init__(self,job):
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)
        self.__args = None

    def set_dir_prefix(self,path):
        self.add_var_arg(path)
        self.__dir_prefix = path

    def get_dir_prefix(self):
        return self.__dir_prefix

    def set_args(self,args):
        self.add_var_arg(args)
        self.__args = args

    def get_args(self):
        return self.__args

# -----------------------------------------------------------------------------
#      Write dag which will run post post processing
# -----------------------------------------------------------------------------
# ---- The structure of our dag is as follows:
#      1. We run many xmakegrbwebpage jobs which each test a subset of the
#         total number of combinations of veto cuts and records the upper
#         limits each combination yields. These jobs do NOT produce plots,
#         webpage etc. (webjob)
#      2. We then run xtunegrbwebpage which chooses the combination of veto
#         cuts which minimizes the upper limit. (tunejob)
#      3. We then run a final xmakegrbwebpage job which uses the optimal 
#         combination of veto cuts and produces a webpage results page.
#         (finalwebjob)
#      For veto method (median|alpha)(Lin|Alpha)(Cut|CutCirc|CutAmp|CutScalar) step 1,2 are done twice:
#       i) once to tune a linear/alpha cut on coherent/incoherent energy (called pre-pass)
#      ii) a second time to tune a median/alpha cut on coherent/incoherent energy and 
#          choose the detection statistic, while keeping the linear/alpha cuts chosen in (i)

#
# ---- Create a dag to which we will add our jobs
dagman_log = log_dir
dag = pipeline.CondorDAG(dagman_log + uuidtag)

# ---- Set the name of the file that will contain the dag.
dag_name = 'grb_web'
dag.set_dag_file(dag_name)

#******************************************************************************
#         Start of pre-pass with linear cut
#******************************************************************************
if prePassFlag :
    # ---- select pre-pass veto method
    if re.match('medianLinCut',veto_method):
        veto_method_pre = re.sub('medianLinCut','linearCut',veto_method)
    elif re.match('medianAlphaCut',veto_method):
        veto_method_pre = re.sub('medianAlphaCut','alphaCut',veto_method)
    elif re.match('alphaLinCut',veto_method):
        veto_method_pre = re.sub('alphaLinCut','linearCut',veto_method)
    else:
        print("Veto method " + veto_method + " not recognized for pre pass mode", file=sys.stderr)
        sys.exit(1)

    # ---- Make instance of XwebJob.
    webjobpre = XwebJob('xmakegrbwebpage_vetoTest.sub')

    # ---- Make instance of XtuneJob. 
    tunejobpre = XtuneJob()
    # ---- Put a single tune job node in tunejob.  This job will tune
    #      veto cuts based on the the output of all the vetoTestOnly
    #      webjobs
    tunenodepre = XtuneNode(tunejobpre)

    #------------------------------------------------------------------------------
    #                Write jobpres for veto test xmakegrbwebpage jobpre
    #------------------------------------------------------------------------------

    # ---- Loop over detection statistic
    for statIdx in range(len(stat_list)):

        # ---- Add one nodepre to the webjobpre for each vetoTestOnly run of
        #      xmakegrbwebpage we will perform.
        for subIdx in range(len(nullStrPre)):
            webnodepre = XwebNode(webjobpre)

            # ---- Set args for each vetoTestOnly webnodepre
            args = grb_name + ' ' + user_tag + '_' + str(subIdx) + \
            ' auto.txt ' + veto_method_pre + \
            nullStrPre[subIdx] +  plusStrPre[subIdx] +  crossStrPre[subIdx] + \
            ' closedbox median '+ injectionType + \
            veto_file_names + ' ' + vetoSet + \
            ' vetoTestOnly ' + stat_list[statIdx] + \
            ' ' + percentile_str + ' ' + freq_veto_file
            webnodepre.set_args(args)

            # ---- Set retry number for each vetoTestOnly webnodepre
            webnodepre.set_retry(retryNumber)

            # ---- Each vetoTestOnly webnodepre will be a parent of the
            #      tunejobpre 
            tunenodepre.add_parent(webnodepre)

            # ---- Add each vetoTestOnly webnodepre to dag
            # ---- Prepend human readable description to nodepre name.
            webnodepre.set_name("xmakegrbwebpage_vetoTest_" + veto_method_pre + "_" + str(subIdx) + "_" + stat_list[statIdx] + "_" + webnodepre.get_name())
            dag.add_node(webnodepre)

    #------------------------------------------------------------------------------
    #                Write jobpres for xtunegrbwebpage jobpre
    #------------------------------------------------------------------------------
    # ---- Set args for final webnodepre which will calculate the upper limit
    #      using the optimal veto combination. Dummy veto cut values e.g.,
    #      __NULLVETO__, will be replaced with optimal values of cut by the
    #      xtunegrbwebpage jobpre.
    finalargs = grb_name + ' ' + user_tag + \
    ' auto.txt ' + veto_method_pre + \
    ' ' + nullVetoType + ' [__NULLVETO__] ' + \
    ' ' + plusVetoType + ' [__PLUSVETO__] ' + \
    ' ' + crossVetoType + ' [__CROSSVETO__] ' + \
    ' closedbox median '+ injectionType + \
    veto_file_names + \
    ' all tunedUpperLimit __DETECTIONSTAT__ ' + percentile_str + \
    ' ' + freq_veto_file

    # ---- Write these args to a file which will be passed to xtunegrbwebpage
    xmake_args_in = 'xmakegrbwebpage_' + grb_name + '_' + user_tag + \
    '_xmake_args_dummy_pre.txt'
    farg = open(xmake_args_in,'w')
    farg.write(finalargs)
    farg.close()

    # ---- Create name for output file to be created by xtunegrbwebpage.
    #      This file will have the dummy cut values, e.g., __NULLVETO__,
    #      replaced by the optimal values of this cut. This file will
    #      be used to run the final xmakegrbwebpage.
    xmake_args_out = 'xmakegrbwebpage_' + grb_name + '_' + user_tag + \
    '_xmake_args_tuned_pre.txt'

    # ---- Write arguments for xtunegrbwebpage
    tuneargs = auto_dir + 'auto_web ' \
    + grb_name + ' ' + \
    user_tag + ' ' + \
    xmake_args_in + ' ' + \
    xmake_args_out + ' ' + \
    veto_method_pre

    tunenodepre.set_args(tuneargs)
    tunenodepre.set_retry(retryNumber)
    # ---- Prepend human readable description to nodepre name.
    tunenodepre.set_name("xtunegrbwebpage_" + tunenodepre.get_name())
    dag.add_node(tunenodepre)
#******************************************************************************
#         End of pre-pass with linear cut
#******************************************************************************


# ---- Make instance of XwebJob.
webjob = XwebJob('xmakegrbwebpage_vetoTest.sub')

# ---- Make instance of XtuneJob. 
tunejob = XtuneJob()
# ---- Put a single tune job node in tunejob.  This job will tune
#      veto cuts based on the the output of all the vetoTestOnly
#      webjobs
tunenode = XtuneNode(tunejob)

#------------------------------------------------------------------------------
#                Write jobs for veto test xmakegrbwebpage job
#------------------------------------------------------------------------------

# ---- Loop over detection statistic
for statIdx in range(len(stat_list)):

    # ---- Add one node to the webjob for each vetoTestOnly run of
    #      xmakegrbwebpage we will perform.
    for subIdx in range(len(nullStr)):
        webnode = XwebNode(webjob)

        # ---- Set args for each vetoTestOnly webnode
        args = grb_name + ' ' + user_tag + '_' + str(subIdx) + \
        ' auto.txt ' + veto_method + \
        nullStr[subIdx] +  plusStr[subIdx] +  crossStr[subIdx] + \
        ' closedbox median '+ injectionType + \
        veto_file_names + ' ' + vetoSet + \
        ' vetoTestOnly ' + stat_list[statIdx] + \
        ' ' + percentile_str + ' ' + freq_veto_file
        webnode.set_args(args)

        # ---- Set retry number for each vetoTestOnly webnode
        webnode.set_retry(retryNumber)

        # ---- Each vetoTestOnly webnode will be a parent of the
        #      tunejob 
        tunenode.add_parent(webnode)

        # ---- Make the pre-pass parent of all vetoTestOnly webnodes if needed
        if prePassFlag:
            webnode.add_parent(tunenodepre)

        # ---- Add each vetoTestOnly webnode to dag
        # ---- Prepend human readable description to node name.
        webnode.set_name("xmakegrbwebpage_vetoTest_" + veto_method + "_" + str(subIdx) + "_" + stat_list[statIdx] + "_" + webnode.get_name())
        dag.add_node(webnode)

#------------------------------------------------------------------------------
#                Write jobs for xtunegrbwebpage job
#------------------------------------------------------------------------------
# ---- Set args for final webnode which will calculate the upper limit
#      using the optimal veto combination. Dummy veto cut values e.g.,
#      __NULLVETO__, will be replaced with optimal values of cut by the
#      xtunegrbwebpage job.
finalargs = grb_name + ' ' + user_tag + \
' auto.txt ' + veto_method + \
' ' + nullVetoType + ' [__NULLVETO__] ' + \
' ' + plusVetoType + ' [__PLUSVETO__] ' + \
' ' + crossVetoType + ' [__CROSSVETO__] ' + \
' closedbox median '+ injectionType + \
veto_file_names + \
' all tunedUpperLimit __DETECTIONSTAT__ ' + percentile_str + \
' ' + freq_veto_file

# ---- Write these args to a file which will be passed to xtunegrbwebpage
xmake_args_in = 'xmakegrbwebpage_' + grb_name + '_' + user_tag + \
'_xmake_args_dummy.txt'
farg = open(xmake_args_in,'w')
farg.write(finalargs)
farg.close()

# ---- Create name for output file to be created by xtunegrbwebpage.
#      This file will have the dummy cut values, e.g., __NULLVETO__,
#      replaced by the optimal values of this cut. This file will
#      be used to run the final xmakegrbwebpage.
xmake_args_out = 'xmakegrbwebpage_' + grb_name + '_' + user_tag + \
'_xmake_args_tuned.txt'

# ---- Write arguments for xtunegrbwebpage
tuneargs = auto_dir + 'auto_web ' \
+ grb_name + ' ' + \
user_tag + ' ' + \
xmake_args_in + ' ' + \
xmake_args_out + ' ' + \
veto_method

tunenode.set_args(tuneargs)
tunenode.set_retry(retryNumber)
# ---- Prepend human readable description to node name.
tunenode.set_name("xtunegrbwebpage_" + tunenode.get_name())
dag.add_node(tunenode)

#------------------------------------------------------------------------------
#                Write jobs for final xmakegrbwebpage job
#------------------------------------------------------------------------------

# ---- Write pre script which tries to set up the grid proxy for cluster nodes
pre_script = grb_name + '_' + user_tag + '_pre.sh'
fpre = open(pre_script,'w')
fpre.write('#!/bin/bash \n')
fpre.write('if [[ -e $X509_USER_PROXY ]]\nthen\n')
fpre.write('cp $X509_USER_PROXY $HOME/x509up.file' + uuidtag + ' \n')
fpre.write('fi\n')
fpre.close()
command = 'chmod +x ' + pre_script
os.system(command)

# ---- Write post script which will copy output of post processing
#      to web_dir_fill
post_script = grb_name  + '_' + user_tag + '_post.sh'
fpost = open(post_script,'w')
fpost.write('#!/bin/bash \n')
fpost.write('rm -f $HOME/x509up.file' + uuidtag + ' \n')
fpost.write('if (( $1 )) ; then exit $1 ; fi \n')

# query files to wake up file system
command = 'ls ' + grb_name  + '_' + user_tag + '* | wc \n' 
fpost.write(command)
fpost.write('sleep 30 \n\n')

# run event display code if requested
if event_display:
    fpost.write('xgrbeventdisplay.py -a ' + auto_dir + 'auto_web -t closedbox\n')
    fpost.write('cd ' + auto_dir + 'auto_web/event\n')
    fpost.write('condor_submit_dag grb_event_closedbox.dag\n')
    fpost.write('cd ' + auto_dir + 'auto_web/\n\n')

# copy files
command = 'cp -r ' + grb_name  + '_' + user_tag + '* ' + web_dir_full + '\n'
fpost.write(command)
command = 'cp ' + params_file  + ' ' + web_dir_full + '\n'
fpost.write(command)
command = 'cp xgrbwebpage.param ' + web_dir_full + '\n'
fpost.write(command)
fpost.close()
command = 'chmod +x '+ post_script
os.system(command)

# ---- Make instance of XwebJob
# ---- This will be the final xmakegrbwebpage job
finalwebjob = XwebJob('xmakegrbwebpage_tunedUL.sub')
finalwebnode = XwebNode(finalwebjob)
finalwebnode.set_args(auto_dir + '/auto_web/' + xmake_args_out)
finalwebnode.set_retry(retryNumber)
finalwebnode.add_parent(tunenode)
finalwebnode.set_post_script(post_script)
finalwebnode.add_post_script_arg('$RETURN')
finalwebnode.set_pre_script(pre_script)
# ---- Prepend human readable description to node name.
finalwebnode.set_name("xmakegrbwebpage_tunedUL_" + finalwebnode.get_name())
dag.add_node(finalwebnode)

# ---- Write out the submit files needed by condor.
dag.write_sub_files()
# ---- Write out the dag itself.
dag.write_dag()
# ---- Delete used dag webjob tunejob 
del dag
del webjob
del tunejob
del finalwebjob

# ---- Add line to .dag file to produce dot file
os.system('mv ' + dag_name + '.dag .temp_dag');
os.system('echo "DOT xpipeline_postProc.dot" > ' + dag_name + '.dag')
os.system('cat .temp_dag >> ' + dag_name + '.dag')
os.system('rm -f .temp_dag')

# ---- Write sub file for open box and ul box analysis. When written this file
#      will contain dummy veto cuts, e.g, __NULLVETO__. These will
#      be replaced by the optimal choice of veto cut by xtunegrbwebpage.
command = 'cp xmakegrbwebpage_vetoTest.sub xmakegrbwebpage_open.sub'
os.system(command)
command = 'cp xmakegrbwebpage_vetoTest.sub xmakegrbwebpage_ul.sub'
os.system(command)

# ---- Write open and ul post script that copy over the result to web page area
open_post_script = grb_name  + '_' + user_tag + '_open_post.sh'
fpost = open(open_post_script,'w')
fpost.write('#!/bin/bash \n')
fpost.write('rm -f $HOME/x509up.file' + uuidtag + ' \n\n')

# run event display code if requested
if event_display:
    fpost.write('xgrbeventdisplay.py -a ' + auto_dir + 'auto_web -t openbox\n')
    fpost.write('cd ' + auto_dir + 'auto_web/event\n')
    fpost.write('condor_submit_dag grb_event_openbox.dag\n')
    fpost.write('cd ' + auto_dir + 'auto_web/\n\n')

command = 'cp -r ' + grb_name  + '_' + user_tag + '_open* ' + web_dir_full + '\n'
fpost.write(command)
fpost.close()
command = 'chmod +x '+ open_post_script
os.system(command)
ul_post_script = grb_name  + '_' + user_tag + '_ul_post.sh'
command = 'cat ' + open_post_script + " | sed 's|_open\\*|_ul\\*|' > " + ul_post_script
os.system(command)
command = 'chmod +x '+ ul_post_script
os.system(command)

#------------------------------------------------------------------------------
#             write scripts to allow user to launch dag job
#------------------------------------------------------------------------------

# ---- Return to initial working dir.
os.chdir(initial_dir)

if (batch_mode):
    # ---- Open shell script that can be used to launch condor jobs. 
    #      If we are running xgrbwebpage.py from xbatchgrbwebpage.py
    #      this script may already exist (i.e., if the current GRB is
    #      not the first GRB to be processed) and we should append the
    #      new commands to the existing script.
    frun_name = 'run_xmakegrbwebpageDAGJob_' + user_tag + '_ALL.sh'
    print('Writing file: ', frun_name)
    if not os.path.isfile(frun_name):
        frun = open(frun_name,'w')
        os.system('chmod +x ' + frun_name)
        command = 'condor_submit_dag -usedagdir ' + auto_dir + 'auto_web/' + dag_name + '.dag '
        frun.write(command)
        frun.close()
    else:
        frun = open(frun_name,'a')
        command = auto_dir + 'auto_web/' + dag_name + '.dag '
        frun.write(command)
        frun.close()

else:
    # ---- Open shell script that can be used to launch condor jobs.
    frun_name = 'run_xmakegrbwebpageDAGJob_' + user_tag + '_' + grb_name + '.sh'
    frun = open(frun_name,'w')
    os.system('chmod +x ' + frun_name)

    print('Writing file: ', frun_name)
    # ---- Write lines to run this GRB's condor dag to our script file
    command = 'cd ' + auto_dir + 'auto_web/; condor_submit_dag ' + \
        dag_name + '.dag; cd ' + initial_dir + ';\n'
    frun.write(command)
    frun.close()

    print('Now run ' + frun_name + \
    ' to launch the xmakegrbwebpage DAG jobs', file=sys.stdout)

print(file=sys.stdout)
print(" ... finished.", file=sys.stdout)

