#!/usr/bin/env python

"""
Script for launching xpipeline triggered search jobs.
Patrick Sutton, adapted from time_slides.py by X. Siemens
$Id$
"""


# -------------------------------------------------------------------------
#      Setup.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, shutil, math, random, copy, getopt, re, string, time
import configparser
from glue import segments
from glue import pipeline
from glue.lal import CacheEntry
#from pylal.date import LIGOTimeGPS

__author__ = "Patrick Sutton <psutton@ligo.caltech.edu>"
__date__ = "$Date: 2008-01-09 07:13:49 -0800 (Wed, 09 Jan 2008) $"
__version__ = "$Revision: 1968 $"

# ---- Function usage.
def usage():
    msg = """\
Usage: 
  grb.sh [options]
  -p, --params-file <file>    parameters (.ini) file [REQUIRED]
  -g, --grb-time <gps>        GRB trigger time (GPS seconds) [REQUIRED]
  -i, --detector <ifo>        add detector to the network [REQUIRED]
  -s, --spacing <gridspacing> select a spacing of sky search [REQUIRED]
  -h, --help                  display this message and exit
"""
    print(msg, file=sys.stderr)


# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
params_file = None
trigger_time = None
spacing = None
detector = []

# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hp:g:i:s:"
# ---- Long form.
longop = [
   "help",
   "params-file=",
   "grb-time=",
   "detector=",
   "spacing="
   ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- Parse command-line arguments.  Arguments are returned as strings, so 
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-p", "--params-file"):
        params_file = a      
    elif o in ("-g", "--grb-time"):
        trigger_time = float(a)       
    elif o in ("-i", "--detector"):
        detector.append(a)
    elif o in ("-s", "--spacing"):
        spacing  = float(a)
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not params_file:
    print("No parameter file specified.", file=sys.stderr)
    print("Use --params-file to specify it.", file=sys.stderr)
    sys.exit(1)
if not trigger_time:
    print("No GRB trigger time specified.", file=sys.stderr)
    print("Use --grb-time to specify it.", file=sys.stderr)
    sys.exit(1)
if not detector:
    print("No detectors specified.", file=sys.stderr)
    print("Use --detector to specify each detector in the network.", file=sys.stderr)
    sys.exit(1)
if not spacing:
    print("No spacing specified.", file=sys.stderr)
    print("Use --spacing to specify it.", file=sys.stderr)
    sys.exit(1)

# ---- Status message.  Report all supplied arguments.
print(file=sys.stdout)
print("WARNING: The on- and off-source DAGs write to the same log file.  This should be fixed.", file=sys.stdout)
print(file=sys.stdout)
print("####################################################", file=sys.stdout)
print("#              X-GRB Search Pipeline               #", file=sys.stdout)
print("####################################################", file=sys.stdout)
print(file=sys.stdout)
print("Parsed input arguments:", file=sys.stdout)
print(file=sys.stdout)
print("     parameters file:", params_file, file=sys.stdout)
print("    GRB trigger time:", trigger_time, file=sys.stdout)   
print("    detector network:", detector, file=sys.stdout) 
print("  angle grid spacing:", spacing, file=sys.stdout) 
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Preparatory.
# -------------------------------------------------------------------------

# ---- Record the current working directory in a string.
cwdstr = os.getcwd( )
# print >> sys.stdout, "   current directory:", cwdstr   
# print >> sys.stdout

# ---- Make directory to store output text files (segment lists, parameter 
#      files, etc.) to minimize clutter in the working directory.
try: os.mkdir( 'input' )
except: pass  # -- Kludge: should probably fail with error message.

# ---- X-Pipeline installation directory.
XPIPELINE_ROOT = os.getenv('XPIPELINE_ROOT')


# -------------------------------------------------------------------------
#      Read configuration file.
# -------------------------------------------------------------------------

# ---- Status message.
print("Parsing parameters (ini) file ...", file=sys.stdout)   

# ---- Create configuration-file-parser object
cp = configparser.ConfigParser()
cp.read(params_file)

# ----  Read needed variables from [parameters] and [background] sections.
background_period = int(cp.get('background','backgroundPeriod'))
blockTime =  int(cp.get('parameters','blockTime'))
whiteningTime =  float(cp.get('parameters','whiteningTime'))
transientTime =  4 * whiteningTime
# radecgps =  cp.get('parameters','radecgps')
# print >> sys.stdout, "background_period:", background_period
# print >> sys.stdout, "blockTime:", blockTime
# print >> sys.stdout, "transientTime:", transientTime
# print >> sys.stdout, "lagFile:", lagFile 
# print >> sys.stdout, "radecgps:", radecgps 

# ---- Read [input] channel parameters. 
detectorListLine = cp.get('input','detectorList')
detectorList = detectorListLine.split(',')
channelListLine = cp.get('input','channelList')
channelList = channelListLine.split(',')
frameTypeListLine = cp.get('input','frameTypeList')
frameTypeList = frameTypeListLine.split(',')
# print >> sys.stdout, "detectorList:", detectorList
# print >> sys.stdout, "channelList:", channelList
# print >> sys.stdout, "frameTypeList:", frameTypeList

# ---- Variables that may or may not be defined in .ini file:
try:
    frameCache = cp.get('input','frameCacheFile')
except:
    print("Warning: variable frameCache not specified in configuration file.", file=sys.stdout)
    frameCache = None
try:
    lagFile =  cp.get('background','lagFile')
except:
    print("Warning: variable lagFile not specified in configuration file.", file=sys.stdout)
    lagFile =  None

# ---- Status message.
print("... finished parsing parameters (ini) file.", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Determine network.
# -------------------------------------------------------------------------

# ---- Status message.
print("Comparing requested network to list of known detectors ...", file=sys.stdout)

# ---- For each detector specified with the --detector option, compare to 
#      the list of known detectors from the .ini file.  Keep only the requested
#      detectors and corresponding channel name and frame type.
#      KLUDGE:TODO: Should exit with error message if any of the detectors 
#      is not recognized.
# ---- Indices of the detectors requested for this analysis.
keepIndex = [] 
for i in range(0,len(detector)) :
    for ii in range(0,len(detectorList)) :
        if detector[i] == detectorList[ii] :
            keepIndex.append(ii)
# ---- We now have a list of the indices of the detectors requested for the 
#      analysis.  Keep only these.  Note that we over-write the 'detector'
#      list because we want to make sure the order matches the channel and 
#      frameType lists.
detector = []
channel = []
frameType = []
for jj in range(0,len(keepIndex)):
    detector.append(detectorList[keepIndex[jj]])
    channel.append(channelList[keepIndex[jj]])
    frameType.append(frameTypeList[keepIndex[jj]])

# ---- Status message.
print("... finished validating network.          ", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Determine MDCs to process, write mdcChannelFiles.
# -------------------------------------------------------------------------

# ---- Check for MDC sets.
if cp.has_option('mdc','mdc_sets') :

    # ---- Status message.
    print("Writing Matlab-formatted MDC channel files ... ", file=sys.stdout)

    # ---- Get list of MDC sets to process.
    mdc_setsList = cp.get('mdc','mdc_sets')
    mdc_sets = mdc_setsList.split(',')
    # print >> sys.stdout, "mdc_sets:", mdc_sets

    # ---- Make MDC channels file for each set. 
    for set in mdc_sets : 
        # print >> sys.stdout, "set:", set
        if cp.has_section(set) :
            # ---- Read channel parameters for this mdc set.
            mdcChannelListLine = cp.get(set,'channelList')
            mdcChannelList = mdcChannelListLine.split(',')
            mdcFrameTypeListLine = cp.get(set,'frameTypeList')
            mdcFrameTypeList = mdcFrameTypeListLine.split(',')
            numberOfChannels = cp.get(set,'numberOfChannels')
            # print >> sys.stdout, "mdcChannelList:", mdcChannelList
            # print >> sys.stdout, "mdcFrameTypeList", mdcFrameTypeList
            # print >> sys.stdout, "numberOfChannels", numberOfChannels
            # ---- Keep only info for detectors requested for the analysis. 
            mdcChannel = []
            mdcFrameType = []
            for jj in range(0,len(keepIndex)):
                mdcChannel.append(mdcChannelList[keepIndex[jj]])
                mdcFrameType.append(mdcFrameTypeList[keepIndex[jj]])
            # print >> sys.stdout, "mdcChannel:", mdcChannel
            # print >> sys.stdout, "mdcFrameType", mdcFrameType
            # ---- Status message.
            # print >> sys.stdout, "Writing Matlab-formatted MDC channel file ...      "
            # ---- For each detector, write the 
            #      corresponding channel name and frame type to a file.
            f=open('input/channels_' + set + '.txt', 'w')
            for i in range(0,len(detector)) :
                f.write(detector[i] + ':' + mdcChannel[i] + ' ' + mdcFrameType[i] + '\n')
            f.close()
            # ---- Status message.
            # print >> sys.stdout, "... finished writing MDC channel file.        "
            # print >> sys.stdout
        else :
            print("ERROR: MDC set ", set, " is not defined in the parameters file.  Exiting.", file=sys.stdout)
            print(file=sys.stdout)
            sys.exit(28)

# ---- Status message.
print("... finished writing MDC channel files.   ", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Write channel file.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing Matlab-formatted channel file ...      ", file=sys.stdout)

# ---- For each detector, write the 
#      corresponding channel name and frame type to a file.
f=open('input/channels.txt', 'w')
for i in range(0,len(detector)) :
    f.write(detector[i] + ':' + channel[i] + ' ' + frameType[i] + '\n')
f.close()

# ---- Status message.
print("... finished writing channel file.        ", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Write Matlab-formatted parameters files.
# -------------------------------------------------------------------------

# ---- We will write three sets of parameters files:  one on-source file,
#      one off-source file, and (for each waveform set and injection scale)
#      one injections file.


# ---- Status message.
print("Writing Matlab-formatted parameter files ...", file=sys.stdout)

# ---- If frameCache is not defined, then we'll make one below, so fill 
#      in appropriate name in parameters files.
if not frameCache:
    frameCache_string = 'input/framecache.txt' 
else:
    frameCache_string = frameCache

# ---- Write all options available in the parameters section to a file.
parameters = cp.options('parameters')

# ---- Parameters file for on-source analysis.
f=open('input/parameters_on_source.txt', 'w')
# ---- First write framecache file, channel file, event file, and sky position. 
f.write('channelFileName:input/channels.txt' + '\n')
f.write('frameCacheFile:' + frameCache_string + '\n')
f.write('eventFileName:input/event_on_source.txt' + '\n')
f.write('skyPositionList:' + "%f"%(spacing) + '\n')
# ---- Now write all of the other parameters from the parameters section.
for i in range(0,len(parameters)) :
    value = cp.get('parameters',parameters[i])
    f.write(parameters[i] + ':' + value + '\n')
f.close()

# ---- Parameters file for off-source analysis.
f=open('input/parameters_off_source.txt', 'w')
# ---- First write framecache file, channel file, event file, and sky position.
f.write('channelFileName:input/channels.txt' + '\n')
f.write('frameCacheFile:' + frameCache_string + '\n')
f.write('eventFileName:input/event_off_source.txt' + '\n')
f.write('skyPositionList:' + "%f"%(spacing) + '\n')
# ---- Now write all of the other parameters from the parameters section.
for i in range(0,len(parameters)) :
    value = cp.get('parameters',parameters[i])
    f.write(parameters[i] + ':' + value + '\n')
f.close()

# ---- Parameter files for on-the-fly simulated waveform analyses, if requested.
if cp.has_section('waveforms') :
    # ---- Read [injection] parameters.  If waveform_set is empty then no 
    #      files will be written.
    waveform_set = cp.options('waveforms')
    injectionScalesList = cp.get('injection','injectionScales')
    injectionScales = injectionScalesList.split(',')
    # ---- Write one parameters file for each (waveform set, injection scale) pair.
    for set in waveform_set :
        scale_counter = 0
        for injectionScale in injectionScales :
            f=open("input/parameters_simulation_" + set + "_" + str(scale_counter) + ".txt", 'w')
            # ---- First write framecache file, channel file, event file, and sky position. 
            f.write('channelFileName:input/channels.txt' + '\n')
            f.write('frameCacheFile:' + frameCache_string + '\n')
            f.write('eventFileName:input/event_on_source.txt' + '\n')
            f.write('skyPositionList:' + "%f"%(spacing) + '\n')
            # ---- Now write all of the other parameters from the parameters section.
            for i in range(0,len(parameters)) :
                value = cp.get('parameters',parameters[i])
                f.write(parameters[i] + ':' + value + '\n')
            # ---- Write simulations info.
            f.write('injectionFileName:input/injection_' + set + '.txt' + '\n') 
            f.write('injectionScale:' + str(injectionScale) + '\n')
            f.write('catalogdirectory:' + XPIPELINE_ROOT  + '/waveforms\n') 
            f.close()
            scale_counter = scale_counter + 1

# ---- Parameter files for MDC waveform analyses, if requested.
if cp.has_option('mdc','mdc_sets') :
    # ---- Read [mdc] sets and injection scales.  If mdc_sets is empty or specifies
    #      unknown MDC sets then the script will have already exited when trying to
    #      write the mdcchannel file above. 
    mdc_setsList = cp.get('mdc','mdc_sets')
    mdc_sets = mdc_setsList.split(',')
    injectionScalesList = cp.get('injection','injectionScales')
    injectionScales = injectionScalesList.split(',')
    # ---- Write one parameters file for each (mdc set, injection scale) pair.
    for set in mdc_sets :
        scale_counter = 0
        for injectionScale in injectionScales :
            f=open("input/parameters_" + set + "_" + str(scale_counter) + ".txt", 'w')
            # ---- First write framecache file, channel file, event file, and sky position. 
            f.write('channelFileName:input/channels.txt' + '\n')
            f.write('frameCacheFile:' + frameCache_string + '\n')
            f.write('eventFileName:input/event_on_source.txt' + '\n')
            f.write('skyPositionList:' + "%f"%(spacing) + '\n')
            # ---- Now write all of the other parameters from the parameters section.
            for i in range(0,len(parameters)) :
                value = cp.get('parameters',parameters[i])
                f.write(parameters[i] + ':' + value + '\n')
            # ---- Write mdc info.
            f.write('mdcChannelFileName:input/channels_' + set + '.txt' + '\n')

            f.write('injectionFileName:input/injection_' + set + '.txt' + '\n')

            f.write('injectionScale:' + str(injectionScale) + '\n')
            f.close()
            scale_counter = scale_counter + 1


outputType=cp.get('parameters','outputType')
if (outputType == 'clusters') | ( outputType == 'sphericalclusters') :

    # ---- Status message.
    print("Writing Matlab-formatted extraction parameter files ...", file=sys.stdout)
    extractParamFileName =cp.get('parameters','extractParamFileName')
    # ---- Write all options available in the parameters section to a file.
    parameters = cp.options('extraction')

    # ---- Parameters file for on-source analysis.
    f=open(extractParamFileName, 'w')

    # ---- Now write all of the other parameters from the parameters section.
    for i in range(0,len(parameters)) :
        value = cp.get('extraction',parameters[i])
        f.write(parameters[i] + ':' + value + '\n')
    f.close()

# ---- Status message.
print("... finished writing parameter files.     ", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Retrieve single-IFO segment lists for analysis period.
# -------------------------------------------------------------------------

# ---- Status message.
print("Retrieving segment lists ... ", file=sys.stdout)

# ---- Interval of data to be analysed.
start_time = int(trigger_time - background_period / 2)
end_time = int(trigger_time + background_period / 2)
duration = int(end_time - start_time)
# print >> sys.stdout, "start_time:", start_time
# print >> sys.stdout, "  end_time:", end_time

# ---- Write time range to a temporary segment file named gps_range.txt.
#      We'll then read this file into a ScienceData object.  It's a hack, but 
#      it seems that the only way to populate ScienceData objects is to read 
#      segments from a file.
f=open('input/gps_range.txt', 'w')
time_range_string = '1 ' + str(start_time) + ' ' + str(end_time) + ' ' + str(duration) + '\n'
f.write(time_range_string)
f.close()

# ---- Read full analysis time range back in to a ScienceData object for easy manipulation.
analysis_segment = pipeline.ScienceData()
analysis_segment.read( 'input/gps_range.txt', blockTime )
# for seg in analysis_segment:
#     print seg.start(), seg.end()

# ---- Call segwizard for each detector.
#      The output from this stage is a list of ScienceData objects called 
#      full_segment_list which contains all of the single-IFO segment lists.
#      Use hardwired run number and hardwired data quality flags.
datafind_server = cp.get('datafind','server')

full_segment_list = []
for ifo in detector:
    # ---- Get full segment list for this detector (entire run).
    #    print 'calling: segwizard ' + run + ' ' + ifo + ' ' + dqflags + ' > .segments_all.txt' 
    #   os.system('segwizard ' + run + ' ' + ifo + ' ' + dqflags + ' > .segments_all.txt')
    #  print '... finished segwizard call.'
    # get data quality flags from parameter file
    dqflags = cp.get(ifo,'dqflags')
    print('calling: LSCsegFind ' +  ' --server=' + datafind_server + ' -i '+ ifo + ' -t ' + dqflags + ' -s %d'%(start_time) + ' -e %d'%(end_time) + ' -o segwizard> .segments_all.txt')
    os.system('LSCsegFind ' +  ' --server=' + datafind_server + ' -i '+ ifo + ' -t ' + dqflags + ' -s %d'%(start_time) + ' -e %d'%(end_time) + ' -o segwizard > .segments_all.txt')
    print('... finished LSCsegFind call.')

    # ---- Read full segment list into a ScienceData object for easy manipulation.
    #      Throw away science segment shorter than the blockTime value read from 
    #      the configuration file.
    full_segment = pipeline.ScienceData()
    full_segment.read( '.segments_all.txt', blockTime )
    # print 'length of full_segment:', len(full_segment)

    # ---- Now restrict to desired analysis time range around trigger_time.
    full_segment.intersection(analysis_segment)
    # for seg in full_segment:
    #     print seg.start(), seg.end()
    full_segment_list.append(full_segment)

# ---- Done with temporary segment files.  Go ahead and delete them.
os.system('rm .segments_all.txt')

# ---- Sanity check: dump segments for each ifo to screen. 
# print
# print "List of analysis segments for each detector", detector, ":"
# for det_list in full_segment_list:
#     for seg in det_list:
#         print seg.start(), seg.end()

# ---- Status message.
print("... finished retreiving segment lists.    ", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#    Make coincidence segment list for on-source, zero-lag segment.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing on-source event file ...          ", file=sys.stdout)

# ---- Now make segment lists for coincidence operation.  First determine
#      segment list for zero lag, and verify that the on-source time is
#      contained by a segment, else quit with error.
# ---- On source period: +/- minimumSegmentLength / 2 around trigger time.
on_source_start_time = int(trigger_time - blockTime / 2)
on_source_end_time = int(on_source_start_time + blockTime) 

# ---- Write time range to a temporary segment file named segment_on_source.txt.
#      We'll then read this file into a ScienceData object.  It's a hack, but 
#      it seems that the only way to populate ScienceData objects is to read 
#      segments from a file.
f=open('input/segment_on_source.txt', 'w')
time_range_string = '1 ' + str(on_source_start_time) + ' ' + str(on_source_end_time) + ' ' + str(blockTime) + '\n'
f.write(time_range_string)
f.close()

# ---- Read on-source time range into a "ScienceData" object.
on_source_segment = pipeline.ScienceData()
on_source_segment.read( 'input/segment_on_source.txt', blockTime )

# ---- Now get the intersection of all of the detector segment lists with this on-source list.
coincidence_segment = copy.deepcopy(on_source_segment)
for det in full_segment_list:
    coincidence_segment.intersection(det)

# ---- If on source interval is a coincidence segment, then the intersection of
#      this interval with the "not" coincidence should be empty.
not_coincidence_segment = copy.deepcopy(coincidence_segment)
not_coincidence_segment.invert()
overlap = not_coincidence_segment.intersection(on_source_segment)
if overlap != 0:
    print("Error: on-source period is not a coincidence segment of the specified network.")
    for seg in on_source_segment:
        print("on source period:", seg.start(), " ", seg.end())
    for seg in coincidence_segment:
        print("coincidence period:", seg.start(), " ", seg.end())
    sys.exit(2)

# ---- At this point, the ScienceData object on_source_segment contains the 
#      on-source zero-lag segment.  Write this to the on-source event file.
f=open('input/event_on_source.txt', 'w')
time_range_string = str(on_source_start_time + blockTime / 2) + '\n'
f.write(time_range_string)
f.close()

# ---- Status message.
print("... finished writing on-source event file.", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#    Make off-source coincidence segment lists for all lags.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing off-source event file ...          ", file=sys.stdout)

# ---- First remove the on-source times from all detector segment lists.
off_source_segment = copy.deepcopy(on_source_segment)
off_source_segment.invert()
for det in full_segment_list:
    det.intersection(off_source_segment)

# ---- Make event list for the zero-lag off-source times.
f = open('input/event_off_source.txt', 'w')
fseg = open('input/segment_off_source.txt', 'w')
# ---- Make a temporary copy of the segment lists.
lag_full_segment_list = copy.deepcopy(full_segment_list)
# ---- List of lags for each detector (0 in this case)
line = ' 0' * len(detector)  
# ---- Now take coincidence of segment lists.
lag_coincidence_list = copy.deepcopy(lag_full_segment_list[0])
for i in range(1,len(detector)):
    lag_coincidence_list.intersection(lag_full_segment_list[i])
# ---- Split coincidence segment list into "chunks".
lag_coincidence_list.make_chunks(blockTime,transientTime)
for seg in lag_coincidence_list:
    for i in range(len(seg)):
        time_range_string = str((seg.__getitem__(i).start() + seg.__getitem__(i).end()) / 2) + line + '\n'
        f.write(time_range_string)
        time_range_string = '0 ' + str(int(seg.__getitem__(i).start())) + ' ' + str(int(seg.__getitem__(i).end())) + ' ' + str(int(seg.__getitem__(i).end() - seg.__getitem__(i).start())) + '\n'
        fseg.write(time_range_string)
f.close()
fseg.close()

# ---- Now open lag file (if any) and write event file for all non-zero lags.
# ---- We'll supply network dependent-defaults.
#      Lag file will have one lag per detector; any of them may be zero.
if lagFile:
    f = open('input/event_off_source.txt', 'a')
    fseg = open('input/segment_off_source.txt', 'a')
    lag_list = open(lagFile,mode='r')
    for line in lag_list:
        # ---- Extract time lag for each detector.
        lags = line.split(None,len(detector)-1)
        print("lag set:", lags) 
        # ---- Make a time-lagged copy of the segment lists.
        lag_full_segment_list = copy.deepcopy(full_segment_list)
        # ---- Time shift segment list of each detector.
        for i in range(len(detector)):
            for seg in lag_full_segment_list[i]:
                seg.set_start(seg.start()-int(lags[i]))  # -- SUBTRACT lag
                seg.set_end(seg.end()-int(lags[i]))  # -- SUBTRACT lag
        # ---- Now take coincidence of time-lagged segment lists.
        lag_coincidence_list = copy.deepcopy(lag_full_segment_list[0])
        for i in range(1,len(detector)):
            lag_coincidence_list.intersection(lag_full_segment_list[i])
        # ---- Split coincidence segment list into "chunks".
        lag_coincidence_list.make_chunks(blockTime,transientTime)
        for seg in lag_coincidence_list:
            for i in range(len(seg)):
                # print seg.__getitem__(i).start(), seg.__getitem__(i).end()
                # time_range_string = str(seg.__getitem__(i).start()) + ' ' + str(seg.__getitem__(i).end()) + '\n'
                time_range_string = str((seg.__getitem__(i).start() + seg.__getitem__(i).end()) / 2) 
                time_range_string = time_range_string + ' ' + line 
                f.write(time_range_string)
                time_range_string = '0 ' + str(int(seg.__getitem__(i).start())) + ' ' + str(int(seg.__getitem__(i).end())) + ' ' + str(int(seg.__getitem__(i).end() - seg.__getitem__(i).start())) + '\n'
                fseg.write(time_range_string)
    f.close()

# ---- Status message.
print("... finished writing off-source event file.", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#    Find frames, if necessary.
# -------------------------------------------------------------------------

# ---- If frameCache is not defined, then call LSCdataFind for each 
#      detector, and convert to readframedata-formatted framecache file.
if not frameCache:
    # ---- Status message.
    print("Writing framecache file ...", file=sys.stdout)
    print("WARNING: framecache generation for MDC frames not yet supported.", file=sys.stdout)
    # ---- Clear away any pre-existing framecache files.
    os.system('rm -f framecache_temp.txt input/framecache.txt')
    # ---- Get LSCdataFind server from parameters file.
    datafind_server = cp.get('datafind','server')
    # ---- Loop over detectors.
    for i in range(0,len(detector)):
        # ---- Construct LSCdataFind command.
        LSCdataFindCommand = "LSCdataFind " \
        + " --server " + datafind_server \
        + " --observatory " + detector[i][0] \
        + " --type " + frameType[i] \
        + " --gps-start-time " + str(start_time)  \
        + " --gps-end-time " + str(end_time)    \
        + " --url-type file --lal-cache > lalcache.txt"
        # ---- Issue LSCdataFind command.
        print("calling LSCdataFind:", LSCdataFindCommand)
        os.system(LSCdataFindCommand)
        print("... finished call to LSCdataFind.")
        # ---- Convert lalframecache file to readframedata format.
        print("calling convertlalcache:")
        os.system('convertlalcache.pl lalcache.txt framecache_temp.txt')
        os.system('cat framecache_temp.txt >> input/framecache.txt')
        print("... finished call to convertlalcache.")
    # ---- Clean up.
    os.system('rm -f framecache_temp.txt lalcache.txt')
    # ---- Set frameCache variable to point to our new file.
    frameCache = 'input/framecache.txt'
    # ---- Status message.
    print("... finished writing framecache file.", file=sys.stdout)
    print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Write injection files for on-the-fly simulations, if needed..
# -------------------------------------------------------------------------

if cp.has_section('waveforms') :
    # ---- Read [injection] parameters. 
    waveform_set = cp.options('waveforms')
    nInjections = cp.get('injection','nInjections')
    multipleInjections = cp.get('parameters','multipleInjections')
    #print >> sys.stdout, "    waveform_set:", waveform_set
    print("Making injection files ...", file=sys.stdout)
    for set in waveform_set :
        waveforms = cp.get('waveforms',set)
        #print >> sys.stdout, "    set:", set,
        #print >> sys.stdout, "    waveforms:", waveforms
        # ---- Construct command to make injection file for this waveform set.
        make_injection_file_command = "xmakegrballskyinjectionfile " \
        + " input/injection_" + set + ".txt " \
        + waveforms + " " \
        + str(on_source_start_time + transientTime) + " " \
        + str(on_source_end_time - transientTime) + " " \
        + nInjections + " " \
        + multipleInjections + " "

        # ---- Issue command to make injection file for this waveform set.
        #print >>  sys.stdout, "make_injection_file_command:", make_injection_file_command
        os.system(make_injection_file_command)
    print("... finished making injection files.", file=sys.stdout)
    print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Define special job class.
# -------------------------------------------------------------------------

class XsearchJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An x search job
    """
    def __init__(self,cp):
        """
        cp = ConfigParser object from which options are read.
        """
        # ---- Get path to executable from parameters file.
        # self.__executable = cp.get('condor','xsearch')
        self.__executable = XPIPELINE_ROOT + "/xdetection.sh"
        # ---- Get condor universe from parameters file.
        self.__universe = cp.get('condor','universe')
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)
        self.__param_file = None

        # ---- Add required environment variables.
        # self.add_condor_cmd('environment',"USER=$ENV(USER);HOME=$ENV(HOME)")
        self.add_condor_cmd('environment',"USER=$ENV(USER);HOME=$ENV(HOME);XPIPELINE_ROOT=$ENV(XPIPELINE_ROOT);XPIPELINE_MATLAB_ROOT=$ENV(XPIPELINE_MATLAB_ROOT);LD_LIBRARY_PATH=$ENV(LD_LIBRARY_PATH)")

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xsearch-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xsearch-$(cluster)-$(process).err')

        # ---- Name of condor job submission file to be written.
        self.set_sub_file('xsearch.sub')

    # ---- All XsearchJob nodes use a common parameters file.  Set it here.
    # def set_param_file(self,path):
    #   self.add_arg(path)
    #   self.__param_file = path

    # def get_param_file(self):
    #   return self.__param_file

class XsearchNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    xseach node
    """
    def __init__(self,job):
        """
        job = A CondorDAGJob that can run an instance of lalapps_tmpltbank.
        """
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)
        self.__x_jobnum = None
        self.__x_injnum = None

    # ---- Set parameters file.
    def set_param_file(self,path):
        self.add_var_arg(path)
        self.__param_file = path

    def get_param_file(self):
        return self.__param_file

    def set_x_jobnum(self,n):
        self.add_var_arg(str(n))
        self.__x_jobnum = n

    def get_x_jobnum(self):
        return self.__x_jobnum

    def set_output_dir(self,path):
        self.add_var_arg(path)
        self.__output_dir = path

    def get_output_dir(self,path):
        return self.__output_dir

    def set_x_injnum(self,n):
        self.add_var_arg(n)
        self.__x_injnum = n

    def get_x_injnum(self):
        return self.__x_injnum


# -------------------------------------------------------------------------
#    Preparations for writing dags.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing job submission files ... ", file=sys.stdout)

# ---- DAGman log file.
#      The path to the log file for condor log messages. DAGman reads this
#      file to find the state of the condor jobs that it is watching. It
#      must be on a local file system (not in your home directory) as file
#      locking does not work on a network file system.
log_file_on_source = cp.get('condor','dagman_log_on_source')
log_file_off_source = cp.get('condor','dagman_log_off_source')
if cp.has_section('waveforms') :
    log_file_simulations = cp.get('condor','dagman_log_simulations')
if cp.has_option('mdc','mdc_sets') :
    log_file_mdcs = cp.get('condor','dagman_log_mdcs')

# ---- Make directories to store the log files and error messages 
#      from the nodes in the DAG
try: os.mkdir( 'logs' )
except: pass
# ---- Make directory to store the output of our test job.
#      NOT TO BE DONE FOR PRODUCTION RUNS!
try: os.mkdir( 'output' )
except: pass
try: os.mkdir( 'output/on_source' )
except: pass
try: os.mkdir( 'output/off_source' )
except: pass
if cp.has_section('waveforms') :
    waveform_set = cp.options('waveforms')
    injectionScalesList = cp.get('injection','injectionScales')
    injectionScales = injectionScalesList.split(',')
    for set in waveform_set :
        scale_counter = 0
        for injectionScale in injectionScales :
            try: os.mkdir( 'output/simulations_' + set + '_' + str(scale_counter) )
            except: pass
            scale_counter = scale_counter + 1
if cp.has_option('mdc','mdc_sets') :
    mdc_setsList = cp.get('mdc','mdc_sets')
    mdc_sets = mdc_setsList.split(',')
    injectionScalesList = cp.get('injection','injectionScales')
    injectionScales = injectionScalesList.split(',')
    for set in mdc_sets :
        scale_counter = 0
        for injectionScale in injectionScales :
            try: os.mkdir( 'output/' + set + '_' + str(scale_counter) )
            except: pass
            scale_counter = scale_counter + 1


# -------------------------------------------------------------------------
#      Write on-source dag.
# -------------------------------------------------------------------------

# ---- Create a dag to which we can add jobs.
dag = pipeline.CondorDAG(log_file_on_source)

# ---- Set the name of the file that will contain the DAG.
dag.set_dag_file( 'grb_on_source' )

# ---- Make instance of XsearchJob.
job = XsearchJob(cp)

# ---- Make analysis jobs for all segments (currently 1) in the on-source 
#      segment list.
segmentList = pipeline.ScienceData()
segmentList.read( 'input/segment_on_source.txt' , blockTime )

# ---- Read how distribute results on node and write to file
distributeOnSource = int(cp.get('output','distributeOnSource'))
if 1 == distributeOnSource :
    jobNodeFileOnSource = cp.get('output','jobNodeFileOnSource')
    nodeList = open( jobNodeFileOnSource,'w')
    nodeList.write("Number_of_detectors %d\n"%(len(detector)))
    for i in range(0,len(detector)) :
        nodeList.write("%s\t"%(detector[i]))
    nodeList.write("\n")
    nodeList.write('event_on_source_file ')
    nodeList.write(cwdstr + "/input/event_on_source.txt \n")
    nodeList.write("Injection_file N/A \n")
    nodeList.write('Number_of_jobs ')
    nodeList.write("%d \n"%(len(segmentList)))
    nodeList.write('Number_of_jobs_per_node ')
    nodeList.write("%d \n"%(len(segmentList)))
    nodeList.write(cwdstr + " \n")

for i in range(len(segmentList)):
    node = XsearchNode(job)
    # ---- Parameters file:
    matlab_param_file = cwdstr + "/input/parameters_on_source.txt"
    node.set_param_file(matlab_param_file)
    node.set_x_jobnum(i)
    if 1 == distributeOnSource :
        nodeList.write(cwdstr + "/output/on_source" + "/results_%d.mat \n"%(i))
    node.set_output_dir( os.path.join( cwdstr + "/output/on_source" ) )
    node.set_x_injnum('0')
    # # ---- Make sure all datafind jobs complete before running xpipeline jobs.
    #   for n in df_list:
    #     node.add_parent(n)
    dag.add_node(node)

# ---- Write out the submit files needed by condor.
dag.write_sub_files()
# ---- Write out the DAG itself.
dag.write_dag()
# ---- Delete used dag job
del dag
del job
# --- close on source distribute file
if 1 == distributeOnSource :
    nodeList.close()


# -------------------------------------------------------------------------
#      Write off-source dag.
# -------------------------------------------------------------------------

# ---- Create a dag to which we can add jobs.
dag = pipeline.CondorDAG(log_file_off_source)

# ---- Set the name of the file that will contain the DAG.
dag.set_dag_file( 'grb_off_source' )

# ---- Make instance of XsearchJob.
job = XsearchJob(cp)

# ---- Load off-source segments.
segmentList = pipeline.ScienceData()
segmentList.read( 'input/segment_off_source.txt' , blockTime )

# ---- Read how distribute results on node and write to file
distributeOffSource = int(cp.get('output','distributeOffSource'))
if 1 == distributeOffSource :
    nodePath = cp.get('output','nodePath')
    onNodeOffSourcePath = cp.get('output','onNodeOffSourcePath')
    nNodes = int(cp.get('output','nNodes'))
    jobNodeFileOffSource = cp.get('output','jobNodeFileOffSource')
    nodeList = open( jobNodeFileOffSource,'w')
    nodeList.write("Number_of_detectors %d\n"%(len(detector)))
    for i in range(0,len(detector)) :
        nodeList.write("%s\t"%(detector[i]))
    nodeList.write("\n")
    nodeList.write('event_off_source_file ')
    nodeList.write(cwdstr + "/input/event_off_source.txt \n")
    nodeList.write("Injection_file N/A \n")
    nodeList.write('Number_of_jobs ')
    nodeList.write("%d \n"%(len(segmentList)))
    nJobsPerNode = int(len(segmentList)/nNodes) + 1
    nodeList.write('Number_of_jobs_per_node ')
    nodeList.write("%d \n"%(nJobsPerNode))
    nodeOffset = int(cp.get('output','numberOfFirstNode'));

if (outputType == 'clusters') | (outputType == 'sphericalclusters') :
    segmentList='1'

for i in range(len(segmentList)):
    node = XsearchNode(job)
    matlab_param_file = cwdstr + "/input/parameters_off_source.txt"
    node.set_param_file(matlab_param_file)
    if (outputType == 'clusters') | ( outputType == 'sphericalclusters') :
        node.set_x_jobnum("0-%d"%(len(segmentList)-1))
    else :
        node.set_x_jobnum(i)
    if 1 == distributeOffSource:
        # write path for each result file, on each node write results from
        # nJobsPerNode jobs
        jobNumber = int(i/nJobsPerNode) + nodeOffset
        while ~(os.path.isdir (nodePath + "%d/"%(jobNumber))) &1:
            print("Waiting for automount to unmount ...\n", file=sys.stdout)
            time.sleep(10)
        # Write name of node before changing nodes
        if 0 == i % nJobsPerNode:
            print("Node number %d"%(jobNumber), file=sys.stdout)
            nodeList.write(nodePath + "%d/ \n"%(jobNumber)  )
            # Create directory for results files
            fullPath = nodePath + "%d/"%(jobNumber) + onNodeOffSourcePath + "/off_source"
            if ~(os.path.isdir (fullPath))& 1 :
                os.makedirs(fullPath)
            else :
                print("**WARNING** path: " + fullPath + " already exists, previous results may be overwritten\n", file=sys.stdout)

        node.set_output_dir(os.path.join( fullPath)) 
        nodeList.write(fullPath + "/results_%d.mat \n"%(i) )
    else :
        node.set_output_dir( os.path.join( cwdstr + "/output/off_source" ) )
    node.set_x_injnum('0')
    dag.add_node(node)

# ---- Write out the submit files needed by condor.
dag.write_sub_files()
# ---- Write out the DAG itself.
dag.write_dag()
# ---- Delete used dag job
del dag
del job
# --- close off source distribute file
if 1 == distributeOffSource :
    nodeList.close()


# -------------------------------------------------------------------------
#      Write on-the-fly simulations dags - one for each waveform set.
# -------------------------------------------------------------------------

# ---- All injection scales for a given waveform set will be handled by a 
#      single dag.
if cp.has_section('waveforms') :

    # ---- Read [injection] parameters. 
    waveform_set = cp.options('waveforms')
    injectionScalesList = cp.get('injection','injectionScales')
    injectionScales = injectionScalesList.split(',')

    # ---- Read how distribute results on node and write to file
    distributeSimulation = int(cp.get('output','distributeSimulation'))
    maxInjNum = int(cp.get('output','maxInjNum'))
    if 1 == distributeSimulation :
        nodePath = cp.get('output','nodePath')
        onNodeSimulationPath = cp.get('output','onNodeSimulationPath')
        nNodes = int(cp.get('output','nNodes'))
        jobNodeFileSimulationPrefix = cp.get('output','jobNodeFileSimulationPrefix')
        nodeOffset = int(cp.get('output','numberOfFirstNode'));
        jobNumber = nodeOffset-1

    # ---- Write one dag for each waveform set.
    for set in waveform_set :

        # ---- Create a dag to which we can add jobs.
        dag = pipeline.CondorDAG( log_file_simulations + "_" + set )

        # ---- Set the name of the file that will contain the DAG.
        dag.set_dag_file( "grb_simulations_" + set )

        # ---- Make instance of XsearchJob.
        job = XsearchJob(cp)

        # ---- Make analysis jobs for all segments in the on-source segment list.
        #      Read segment list from file.
        segmentList = pipeline.ScienceData()
        segmentList.read( 'input/segment_on_source.txt' , blockTime )

        # ---- Read injection file to determine number of injections for this set.
        #      Use system call to wc to figure out how many lines are in the injection file.
        os.system('wc input/injection_' + set + '.txt | awk \'{print $1}\' > input/' + set + '.txt' ) 
        f = open('input/' + set + '.txt')
        numberOfInjections = int(f.readline())
        f.close()

        # ---- Loop over segments.
        for i in range(len(segmentList)):
            # ---- Loop over injection scales.
            scale_counter = 0
            for injectionScale in injectionScales :
                if  0 == maxInjNum:
                    # ---- Create job node.
                    node = XsearchNode(job)
                    # ---- Assign first argument: parameters file
                    matlab_param_file = cwdstr + "/input/parameters_simulation_" + set + "_" + str(scale_counter) + ".txt"
                    node.set_param_file(matlab_param_file)
                    # ---- Assign second argument: job (segment) number 
                    node.set_x_jobnum(i)
                    # ---- Assign third argument: output directory
                    # ---- Check to see if distributing results files over nodes.
                    if 1 == distributeSimulation & 0 == maxInjNum:
                        # ---- Yes: determine output directories and make sure they exist.
                        #      Record directories in a file.
                        jobNumber = (jobNumber+1-nodeOffset)%nNodes + nodeOffset
                        nodeList = open( jobNodeFileSimulationPrefix + '_' + set +"_seg%d"%(i) + "_injScale" + injectionScale + '.txt'  ,'w')
                        nodeList.write("Number_of_detectors %d\n"%(len(detector)))
                        for j in range(0,len(detector)) :
                            nodeList.write("%s\t"%(detector[j]))
                        nodeList.write("\n")
                        nodeList.write('event_simulation_file ')
                        nodeList.write(cwdstr + "/input/event_on_source.txt \n")
                        nodeList.write('Injection_file ')
                        nodeList.write(cwdstr + "/input/injection_" + set + ".txt \n")
                        nodeList.write('Number_of_jobs ')
                        nodeList.write("%d \n"%(numberOfInjections))
                        #nJobsPerNode = int(numberOfInjections/nNodes) + 1
                        nJobsPerNode = numberOfInjections
                        nodeList.write('Number_of_jobs_per_node ')
                        nodeList.write("%d \n"%(nJobsPerNode))
                        # write path for each result file, on each node write results from
                        # nJobsPerNode jobs
                        #jobNumber = int((injectionNumber-1)/nJobsPerNode) + nodeOffset
                        while ~(os.path.isdir (nodePath + "%d/"%(jobNumber))) &1:
                            print("Waiting for automount to unmount ...\n", file=sys.stdout)
                            time.sleep(10)
                        # Write name of node before changing nodes
                        #if 0 == (injectionNumber-1) % nJobsPerNode:
                        print("Node number %d"%(jobNumber), file=sys.stdout)
                        nodeList.write(nodePath + "%d/ \n"%(jobNumber))
                        # Create directory for results files
                        fullPath = nodePath + "%d/"%(jobNumber) + onNodeSimulationPath + "simulations_" + set + '_' + str(scale_counter)
                        if ~(os.path.isdir (fullPath))& 1 :
                            os.makedirs(fullPath)
                        else:
                            print("**WARNING** path: " + fullPath + " already exists, previous results may be overwritten\n", file=sys.stdout)
                        node.set_output_dir(os.path.join( fullPath))
                        for injectionNumber in range(1,numberOfInjections+1) :
                            nodeList.write(fullPath + "/results_%d"%(i) +  "_%d.mat \n"%(injectionNumber) )
                        nodeList.close()
                    else :
                        # ---- No: Set output directory to local.
                        node.set_output_dir( os.path.join( cwdstr + '/output/simulations_' + set + '_' + str(scale_counter) ) )
                    # ---- Assign fourth argument: injection number 
                    #      KLUDGE: This will screw up injections if more than 
                    #      one segment; injection number iterates only over 
                    #      injections that fall within analysis interval.
                    node.set_x_injnum("1-%d"%(numberOfInjections))
                    dag.add_node(node)
                else :
                    for iInjRange in range(int(math.ceil(numberOfInjections/maxInjNum+1))) :
                        node = XsearchNode(job)
                        matlab_param_file = cwdstr + "/input/parameters_simulation_" + set + "_" + str(scale_counter) + ".txt"
                        node.set_param_file(matlab_param_file)
                        node.set_x_jobnum(i)
                        node.set_output_dir( os.path.join( cwdstr + '/output/' + set + '_' + str(scale_counter) ) )
                        node.set_x_injnum("%d"%(iInjRange*maxInjNum+1) + "-" + "%d"%(min((iInjRange+1)*maxInjNum,numberOfInjections)))
                        dag.add_node(node)
                if i > 1 :
                    print("WARNING: injection number specification does not work properly for injections into more than one segment.", file=sys.stdout)
                # ---- Add job node to the dag.

                # ---- Continue on to the next injection scale.
                scale_counter = scale_counter + 1

        # ---- Write out the submit files needed by condor.
        dag.write_sub_files()
        # ---- Write out the DAG itself.
        dag.write_dag()

        # ---- Delete used dag job
        del dag
        del job


# -------------------------------------------------------------------------
#      Write MDC simulation dags - one for each MDC set.
# -------------------------------------------------------------------------

# ---- All injection scales for a given MDC set will be handled by a 
#      single dag.

# ---- Check for MDC sets.
if cp.has_option('mdc','mdc_sets') :

    # ---- Status message.
    print("Writing MDC job dag files ... ", file=sys.stdout)

    # ---- Get list of MDC sets to process.
    mdc_setsList = cp.get('mdc','mdc_sets')
    mdc_sets = mdc_setsList.split(',')
    injectionScalesList = cp.get('injection','injectionScales')
    injectionScales = injectionScalesList.split(',')

    # ---- Read how distribute results on node and write to file
    distributeSimulation = int(cp.get('output','distributeSimulation'))
    maxInjNum = int(cp.get('output','maxInjNum'))
    if 1 == distributeSimulation :
        nodePath = cp.get('output','nodePath')
        onNodeSimulationPath = cp.get('output','onNodeSimulationPath')
        nNodes = int(cp.get('output','nNodes'))
        jobNodeFileSimulationPrefix = cp.get('output','jobNodeFileSimulationPrefix')
        nodeOffset = int(cp.get('output','numberOfFirstNode'));
        jobNumber = nodeOffset-1


    # ---- Write one dag for each waveform set.
    for set in mdc_sets :

        # ---- Create a dag to which we can add jobs.
        dag = pipeline.CondorDAG( log_file_simulations + "_" + set )

        # ---- Set the name of the file that will contain the DAG.
        dag.set_dag_file( "grb_" + set )

        # ---- Make instance of XsearchJob.
        job = XsearchJob(cp)

        # ---- Make analysis jobs for all segments in the on-source segment list.
        #      Read segment list from file.
        segmentList = pipeline.ScienceData()
        segmentList.read( 'input/segment_on_source.txt' , blockTime )

        # ---- Read ini file to determine number of injections for this set.
        numberOfInjections = int(cp.get(set,'numberOfChannels'))

        # ---- Loop over segments.
        for i in range(len(segmentList)):
            # ---- Loop over injection scales.
            scale_counter = 0
            for injectionScale in injectionScales :
                if  0 == maxInjNum:
                    # ---- Create job node.
                    node = XsearchNode(job)
                    # ---- Assign first argument: parameters file
                    matlab_param_file = cwdstr + "/input/parameters_" + set + "_" + str(scale_counter) + ".txt"
                    node.set_param_file(matlab_param_file)
                    # ---- Assign second argument: job (segment) number 
                    node.set_x_jobnum(i)
                    # ---- Assign third argument: output directory
                    # ---- Check to see if distributing results files over nodes.
                    if 1 == distributeSimulation & 0 == maxInjNum:
                        # ---- Yes: determine output directories and make sure they exist.
                        #      Record directories in a file.
                        jobNumber = (jobNumber+1-nodeOffset)%nNodes + nodeOffset
                        nodeList = open( jobNodeFileSimulationPrefix + '_' + set +"_seg%d"%(i) + "_injScale" + injectionScale + '.txt'  ,'w')
                        nodeList.write("Number_of_detectors %d\n"%(len(detector)))
                        for j in range(0,len(detector)) :
                            nodeList.write("%s\t"%(detector[j]))
                        nodeList.write("\n")
                        nodeList.write('event_simulation_file ')
                        nodeList.write(cwdstr + "/input/event_on_source.txt \n")
                        nodeList.write('Injection_file ')
                        nodeList.write(cwdstr + "/input/injection_" + set + ".txt \n")
                        nodeList.write('Number_of_jobs ')
                        nodeList.write("%d \n"%(numberOfInjections))
                        #nJobsPerNode = int(numberOfInjections/nNodes) + 1
                        nJobsPerNode = numberOfInjections
                        nodeList.write('Number_of_jobs_per_node ')
                        nodeList.write("%d \n"%(nJobsPerNode))
                        # write path for each result file, on each node write results from
                        # nJobsPerNode jobs
                        #jobNumber = int((injectionNumber-1)/nJobsPerNode) + nodeOffset
                        while ~(os.path.isdir (nodePath + "%d/"%(jobNumber))) &1:
                            print("Waiting for automount to unmount ...\n", file=sys.stdout)
                            time.sleep(10)
                        # Write name of node before changing nodes
                        #if 0 == (injectionNumber-1) % nJobsPerNode:
                        print("Node number %d"%(jobNumber), file=sys.stdout)
                        nodeList.write(nodePath + "%d/ \n"%(jobNumber))
                        # Create directory for results files
                        fullPath = nodePath + "%d/"%(jobNumber) + onNodeSimulationPath + "_" + set + '_' + str(scale_counter)
                        if ~(os.path.isdir (fullPath))& 1 :
                            os.makedirs(fullPath)
                        else:
                            print("**WARNING** path: " + fullPath + " already exists, previous results may be overwritten\n", file=sys.stdout)
                        node.set_output_dir(os.path.join( fullPath))
                        for injectionNumber in range(1,numberOfInjections+1) :
                            nodeList.write(fullPath + "/results_%d"%(i) +  "_%d.mat \n"%(injectionNumber) )
                        nodeList.close()
                    else :
                        # ---- No: Set output directory to local.
                        node.set_output_dir( os.path.join( cwdstr + '/output/' + set + '_' + str(scale_counter) ) )
                        # ---- Assign fourth argument: injection number 
                        #      KLUDGE: This will screw up injections if more than 
                        #      one segment; injection number iterates only over 
                        #      injections that fall within analysis interval.
                    node.set_x_injnum("1-%d"%(numberOfInjections))
                    dag.add_node(node)
                else :
                    for iInjRange in range(int(math.ceil(numberOfInjections/maxInjNum+1))) :
                        node = XsearchNode(job)
                        matlab_param_file = cwdstr + "/input/parameters_" + set + "_" + str(scale_counter) + ".txt"
                        node.set_param_file(matlab_param_file)
                        node.set_x_jobnum(i)
                        node.set_output_dir( os.path.join( cwdstr + '/output/' + set + '_' + str(scale_counter) ) )
                        node.set_x_injnum("%d"%(iInjRange*maxInjNum+1) + "-" + "%d"%(min((iInjRange+1)*maxInjNum,numberOfInjections)))
                        dag.add_node(node)
                if i > 1 :
                    print("WARNING: injection number specification does not work properly for injections into more than one segment.", file=sys.stdout)
                    # ---- Add job node to the dag.

                    # ---- Continue on to the next injection scale.
                scale_counter = scale_counter + 1

        # ---- Write out the submit files needed by condor.
        dag.write_sub_files()
        # ---- Write out the DAG itself.
        dag.write_dag()

        # ---- Delete used dag job
        del dag
        del job


# -------------------------------------------------------------------------
#      Finished.
# -------------------------------------------------------------------------


# ---- Status message.
print("... finished writing job submission files. ", file=sys.stdout)
print(file=sys.stdout)

print("############################################", file=sys.stdout)
print("#           Completed.                     #", file=sys.stdout)
print("############################################", file=sys.stdout)


# ---- Exit cleanly
sys.exit( 0 )


# -------------------------------------------------------------------------
#      Leftover code samples.
# -------------------------------------------------------------------------


# # ---- Make data find job node.
# df_job = pipeline.LSCDataFindJob( 'cache','logs', cp )

# # ---- Make an empty list to hold the datafind jobs.
# df_list = []

# # ---- Loop over detectors and prepare a datafind job for each.
# for ifo in ['H1', 'H2', 'L1']:
#   df = pipeline.LSCDataFindNode( df_job )
#   df.set_start( 700000000 )
#   df.set_end( 700000100 )
#   df.set_observatory( ifo[0] )
#   df.set_type('RDS_R_L1')
#   df.set_post_script('/why/oh/why/oh/why.sh')
#   df.add_post_script_arg(df.get_output())
#   dag.add_node(df)
#   df_list.append(df)


