function xmergegrbclusteredresultsTwoDets(outputDir,targetDir,snFlag,tunedParamsFile,origResults,preTunedParamsFile,SCflag)
% XMERGEGRBCLUSTEREDRESULTSTWODETS - Wrapper to xmergegrbfiles.
%
% xmergegrbresults is a wrapper to xmergegrbfiles to combine the output files
% of multiple X-Pipeline GRB jobs into a single file.
%
% usage:
%
% xmergegrbresults(outputDir,targetDir,snFlag,tunedParamsFile,origResults,preTunedParamsFile)
%
%   outputDir  String specifying path to "output" directory in which  
%              "target" directory lives.
%   targetDir  String specifying name of "target" directory that contains
%              files to be merged.
%   SCflag     String 0 or 1. If 0, no superclustering [OPTIONAL]
%   snFlag     String. If non zero some sanity checks are skipped to
%              accomodate pecularities of the supernova search [OPTIONAL]
%   preTunedParamsFile 
%              String specifying the path of the file containing the first
%              stage ("pre") tuning command line parameters for
%              xmakegrbwebpage. Actually an '.digest' is appended to the
%              provided file name and that file is read. The digest
%              should be produced alongside the full command line when
%              the post-processing dag is run. [OPTIONAL]
%   tunedParamsFile
%              String. Same as above but for the second stage of tuning. [OPTIONAL]
%   origResults
%              String. Path to the results of the post-pocessing. [OPTIONAL]
%
%
% $Id: xmergegrbresults.m 4490 2014-09-17 11:00:58Z patrick.sutton@LIGO.ORG $

% ---- By default 
if nargin < 2.99
  snFlag = 0;
  SCflag = 0;
else
  if nargin == 3
    snFlag = str2num(snFlag);
    SCflag = 0;
  end
  if nargin == 4
    snFlag = str2num(snFlag);
    SCflag = 1;
  end
  if nargin > 4.01 & nargin < 6.99
    disp(nargin);
    snFlag = str2num(snFlag);
    SCflag = 0;
  end
  if nargin == 7
    snFlag = str2num(snFlag);
    SCflag = str2num(SCflag);
  end
end

% ---- Ensure that targetDir has trailing "/" (not needed for outputDir).
if targetDir(end) ~= '/'
    targetDir = [targetDir '/'];
end

% ---- cd to output directory.
cd(outputDir);

% ---- Remove any pre-existing merged file.
[status,result] = system(['rm -f ' targetDir '*merged.mat']);

% ---- Make cell array list of directories matching targetDir string.
[status,result] = system(['ls -d --color=none ' targetDir]);
DIR_LIST = strread(result,'%s');



% ---- For each target directory get list of output files and run merge
%      function on them. 
for ii = 1:length(DIR_LIST)
    [status,FILE_LIST] = system(['ls --color=none ' DIR_LIST{ii} '| grep mat']);
    if nargin < 5
      if SCflag==1
          if strcmp(DIR_LIST{1},'on_source/')==1 | strcmp(DIR_LIST{1},'off_source_1/')==1 | strcmp(DIR_LIST{1},'off_source_2/')==1 | strcmp(DIR_LIST{1},'off_source_3/')==1 | strcmp(DIR_LIST{1},'off_source_4/')==1 | strcmp(DIR_LIST{1},'off_source_5/')==1 | strcmp(DIR_LIST{1},'off_source_6/')==1 | strcmp(DIR_LIST{1},'off_source_7/')==1 | strcmp(DIR_LIST{1},'off_source_8/')==1 | strcmp(DIR_LIST{1},'off_source_9/')==1 | strcmp(DIR_LIST{1},'off_source_10/')==1
              xoffsourceunion_S_2det(DIR_LIST{ii}, FILE_LIST);
          else
              xinjunion_S_2det(DIR_LIST{ii}, FILE_LIST);
          end
      end
      xmergegrbfiles(DIR_LIST{ii},FILE_LIST,snFlag);
    elseif nargin == 5
      disp(['Apply pre-set coherent cuts found in:' ...
            tunedParamsFile ', ' origResults ]);
      if SCflag==1
          if strcmp(DIR_LIST{1},'on_source/')==1 | strcmp(DIR_LIST{1},'off_source_1/')==1 | strcmp(DIR_LIST{1},'off_source_2/')==1 | strcmp(DIR_LIST{1},'off_source_3/')==1 | strcmp(DIR_LIST{1},'off_source_4/')==1 | strcmp(DIR_LIST{1},'off_source_5/')==1 | strcmp(DIR_LIST{1},'off_source_6/')==1 | strcmp(DIR_LIST{1},'off_source_7/')==1 | strcmp(DIR_LIST{1},'off_source_8/')==1 | strcmp(DIR_LIST{1},'off_source_9/')==1 | strcmp(DIR_LIST{1},'off_source_10/')==1
              xoffsourceunion_S_2det(DIR_LIST{ii}, FILE_LIST);
          else
              xinjunion_S_2det(DIR_LIST{ii}, FILE_LIST);
          end
      end
      xmergegrbfiles(DIR_LIST{ii},FILE_LIST,snFlag,...
                     tunedParamsFile,origResults);
    else
      disp(['Apply pre-set coherent two-shape cuts found in:' ...
            tunedParamsFile ', ' origResults ', ' ...
            preTunedParamsFile]);
      if SCflag==1
          if strcmp(DIR_LIST{1},'on_source/')==1 | strcmp(DIR_LIST{1},'off_source_1/')==1 | strcmp(DIR_LIST{1},'off_source_2/')==1 | strcmp(DIR_LIST{1},'off_source_3/')==1 | strcmp(DIR_LIST{1},'off_source_4/')==1 | strcmp(DIR_LIST{1},'off_source_5/')==1 | strcmp(DIR_LIST{1},'off_source_6/')==1 | strcmp(DIR_LIST{1},'off_source_7/')==1 | strcmp(DIR_LIST{1},'off_source_8/')==1 | strcmp(DIR_LIST{1},'off_source_9/')==1 | strcmp(DIR_LIST{1},'off_source_10/')==1
              xoffsourceunion_S_2det(DIR_LIST{ii}, FILE_LIST);
          else
              xinjunion_S_2det(DIR_LIST{ii}, FILE_LIST);
          end
      end
      xmergegrbfiles(DIR_LIST{ii},FILE_LIST,snFlag,...
                     tunedParamsFile,origResults,preTunedParamsFile);
    end
end

% ---- It's a rude function that doesn't return to the starting directory.
cd('..');

% ---- Done.
return

