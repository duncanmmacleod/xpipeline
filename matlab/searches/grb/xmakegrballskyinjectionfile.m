function xmakegrballskyinjectionfile(fileName,waveform,startTimeString, ...
    endTimeString,nInjectionsString,nMultipleInjectionsString)
% XMAKEGRBALLSKYINJECTIONFILE - Make an X-Pipeline file for all-sky injections. 
%
%     xmakegrballskyinjectionfile(fileName,waveform,startTime,endTime,)
%
% fileName	String.  Name of output injection file.
% waveform  String.  Comma-delimited list of waveforms to inject. An
%           exclamation '!' separates the type of waveform from its 
%           parameters.  Parameters are a tilde-delimited string. Type and
%           parameters are in xmakewaveform format (see below for
%           examples). 
% startTime String.  GPS start time of injection period.
% endTime   String.  GPS end time of injection period.
% number    String.  Number of injections
% 
% Injections occur randomly between starttime and endtime.   
% The file is made by calling xmakegwbinjectionfile using 'uniform'
% distribution of polarization angles and random draws of the waveform type
% for each injection.
%
% All of the input arguments are strings because this function needs to be
% compiled for use with the automated data analysis pipeline.
%
% % ---- Sample input arguments to the function
% fileName = 'injection_SG.txt'
% waveform = 'DFM!10~A3B5G4,DFM!10~A4B5G4'
% startTimeString = '700000000'
% endTimeString = '700000100'
% nInjectionsString = '234'
%
% original write: Patrick J. Sutton 2007.03.09
%
% $Id$

% ---- Convert string of waveform names into cell array.
waveform_cell = strread(waveform,'%s','delimiter',',');

% ---- Convert cell array of MDC-style waveform names into waveform type
%      and parameter values in xmakewaveform format.
% [type,params] = GetMDCWaveformParameters(waveform_cell);
% for jType = 1:length(type)
%    signal{jType,1} = type{jType};
%    signal{jType,2} = params{jType};
% end
for jType = 1:length(waveform_cell)
    [a b] = strread(waveform_cell{jType},'%s %s','delimiter','!');
    signal{jType,1}= a{1};
    signal{jType,2}= b{1};
end

% ---- Cook up injection times.
startTime = str2num(startTimeString);
endTime = str2num(endTimeString);
nInjections = str2num(nInjectionsString);
nMultiple = str2num(nMultipleInjectionsString);
if(0==nMultiple)
    
peakTime = startTime+rand(nInjections,1)*(endTime - startTime);

else
    duration=endTime - startTime;
    peakTime = startTime + mod((1:nInjections)'*duration/(nMultiple +rand(1,1)),duration);
    
%     tWindow = 0.2;
%     tRes=0.01;
%     duration=endTime - startTime;
%     nTimeBins=ceil(duration/tRes)+1;
%     peakTime=zeros(nInjections,1);
%     for iInj=1:nInjections
%         if (0==mod(iInj-1,nMultiple))
%             display(['Zeroing at injection : ' num2str(iInj)]);
%             occupiedTimes=zeros(nTimeBins,1);
%         end
%         counter=0;
%         injRelativeTime=-1;
%         while counter <100
%             counter=counter+1;
%             tryTime=rand(1,1)*duration;
%             1+round(tryTime/tRes)
%             if( 0==occupiedTimes(1+round(tryTime/tRes)))
%                 injRelativeTime=tryTime;
%                 occupiedTimes(1+(max(round(tryTime/tRes)-tWindow/tRes,0) : ...
%                     min(round(tryTime/tRes)+tWindow,nTimeBins-1))) =1;
%                 counter=101;
%             end
%         end
%         if(-1 == injRelativeTime)
%             error('Did not find a random injection spot after 100 trials, try with lower nMultipleInjectionString');
%         end
%         peakTime(iInj) =  startTime + injRelativeTime;
%     end
end

% ---- Make the GWB injection file.
xmakegwbinjectionfile(fileName,peakTime,'isotropic','uniform',signal,'skyCoordinateSystem','radec');

% ---- Done.
return

