function [] = xwriteratevssig(fout,analysis,window,onSource,offSource,...
    binArray,figures_dirName,figfiles_dirName)
% XWRITERATEVSSIG - Plot cumulative distribution of event rate vs. significance.
% 
% xwriteratevssig produces a cumulative distribution plot of trigger
% significance against rate.  It is a helper function for xmakegrbwebpage.
%
% Usage:
%
%  xwriteratevssig(fout,analysis,window,onSource,offSource,...
%      binArray,figures_dirName,figfiles_dirName)
%
%   fout             Pointer to open and writable webpage file  
%   analysis         Structure created by xmakegrbwebpage
%   window           Structure containing duration field, which gives
%                    the duration of the on-source region in seconds.
%   onSource         Structure containing onSource events
%   offSource        Structure containing offSource events
%   binArray         Vector of bins used to divide up significance when making
%                    histrograms
%   figures_dirName  String, path of output dir for figures where figures will
%                    be saved
%   figfiles_dirName String, path of output dir for figures where figfiles will
%                    be saved
%
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(8, 8, nargin));

fprintf(fout,'%s\n','<h2>Rate vs. significance</h2>');

% ---- Initialize histograms.
histOff = [];
histOn = [];
histOffCut = [];
histOnCut = [];
% ---- Off-source clusters before vetos.
histOff = histc(offSource.significance, binArray);
% ---- On-source clusters before vetos.
histOn = histc(onSource.significance, binArray);
% ---- Off-source clusters after vetos.
histOffCut = histc(...
     offSource.significance.*offSource.pass,...
     binArray);
% ---- On-source clusters after vetos.
histOnCut = histc(...
     onSource.significance.*onSource.pass,...
     binArray);

numOffJobs = length(unique(offSource.jobNumber))
numOnJobs  = length(unique(onSource.jobNumber))

if numOnJobs ~= 1
    error('We should only have one on-source segment'); 
end

% ---- Make histogram.
figure; set(gca,'FontSize',20);
% ---- Normalise 'off source: all events' rate by total time
%      analysed for triggers in off-source.
totalTimeOff = numOffJobs * (analysis.onSourceWindowLength - 2 * analysis.transientTime)
loglog(binArray,flipud(cumsum(flipud(histOff)))/totalTimeOff,... 
    'b','LineWidth',2);
hold on; grid;
% ---- Normalise 'off source: after vetoing' rate by duration
%      of on-source analysis region multiplied by the number of
%      off-source segments analysed.
totalTimeOffWindow = numOffJobs * window.duration;
loglog(binArray,flipud(cumsum(flipud(histOffCut)))/totalTimeOffWindow,...
    'r','LineWidth',2);
% ---- Normalise 'on source: all events' rate by total time
%      analysed for triggers in on-source.
totalTimeOn = numOnJobs * (analysis.onSourceWindowLength - 2 * analysis.transientTime)
loglog(binArray,flipud(cumsum(flipud(histOn)))/totalTimeOn,...
    'g','LineWidth',2);
% ---- Normalise 'on source: after vetoing' rate by duration
%      of on-source analysis region.
totalTimeOnWindow = numOnJobs * window.duration;
loglog(binArray,flipud(cumsum(flipud(histOnCut)))/totalTimeOnWindow,...
    'm','LineWidth',2);
xlabel('significance');
ylabel('rate (Hz)');
legend(['off source: all events    (' num2str(totalTimeOff) 's)'],...
       ['off source: after all cuts (window, DQ, coherent) (' num2str(totalTimeOffWindow) 's)'],...
       ['on source: all events     (' num2str(totalTimeOn) 's)'],...
       ['on source: after all cuts (window, DQ, coherent)  (' num2str(totalTimeOnWindow) 's)']);

% ---- Save plot
plotNameHist = ['/hist_rate_vs_sig_' analysis.clusterType];
xsavefigure(plotNameHist,figures_dirName,figfiles_dirName);

% ---- Put all plots into a table.
xaddfiguretable(fout,plotNameHist,figures_dirName,analysis.captions.ratevssig);
clear plotNameHist

% ----------------------------
iEnergyItf1=find(strcmp(analysis.likelihoodType,'energyitf1'));
iEnergyItf2=find(strcmp(analysis.likelihoodType,'energyitf2'));
iEnergyItf3=find(strcmp(analysis.likelihoodType,'energyitf3'));
if(not(isempty(iEnergyItf1)) && not(isempty(iEnergyItf2)))
  frequencyThr = 200;
  lowFreqMask = offSource.peakFrequency <= frequencyThr;
  passChoiceList = {'windowDQcut','allCuts'};
  for iPassChoice = 1:2
    passChoice = passChoiceList{iPassChoice};
    switch passChoice
      case 'windowDQcut'
      offSourcePass = offSource.passWindow & offSource.passVetoSegs;
      fprintf(fout,'%s\n',['<h3>Single detector properties after DQ and ' ...
                          'window cut</h3>']);
      case 'allCuts'
      offSourcePass = logical(offSource.pass);
      fprintf(fout,'%s\n',['<h3>Single detector properties after all ' ...
                          'cuts (including coherent tests)</h3>']);
    end
  % ---- Histogram of low frequency triggers
  binArray=[exp(0:0.1:log(1e5))];
  histE1 = histc(offSource.likelihood(offSourcePass & lowFreqMask,iEnergyItf1) ...
                 , binArray);
  histE2 = histc(offSource.likelihood(offSourcePass & lowFreqMask,iEnergyItf2) ...
                 , binArray);
  figure;set(gca,'FontSize',20);
  loglog(binArray,flipud(cumsum(flipud(histE1)))/totalTimeOffWindow,... 
         'b','LineWidth',2);
  hold on; grid;
  loglog(binArray,flipud(cumsum(flipud(histE2)))/totalTimeOffWindow,... 
         'g','LineWidth',2);
  if(not(isempty(iEnergyItf3)))
    histE3 = histc(offSource.likelihood(offSourcePass & lowFreqMask,iEnergyItf3) ...
                   , binArray);
    loglog(binArray,flipud(cumsum(flipud(histE3)))/totalTimeOffWindow,... 
           'r','LineWidth',2);
    legend(['Energy ' analysis.detectorList{1}],['Energy ' analysis.detectorList{2}],...
           ['Energy ' analysis.detectorList{3}]);
  else
    legend(['Energy ' analysis.detectorList{1}],['Energy ' analysis.detectorList{2}]);
  end
  title(['low freq trigger (<=' num2str(frequencyThr) 'Hz) rate after ' passChoice]);
  xlabel('Energy');ylabel('Rate (Hz)');
  xlim([1 1e5]);

  % ---- Save plot
  plotNameHist{1} = ['/hist_rate_vs_energy12_lowFreq_' analysis.clusterType '_' passChoice];
  disp(plotNameHist{1})
  xsavefigure(plotNameHist{1},figures_dirName,figfiles_dirName);
  captionCell{1} = analysis.captions.ratevsenergy_lowfreq;

  % ---- Histogram of high frequency triggers
  histE1 = histc(offSource.likelihood(offSourcePass & not(lowFreqMask),iEnergyItf1), binArray);
  histE2 = histc(offSource.likelihood(offSourcePass & not(lowFreqMask),iEnergyItf2), binArray);
  figure;set(gca,'FontSize',20);
  loglog(binArray,flipud(cumsum(flipud(histE1)))/totalTimeOffWindow,... 
         'b','LineWidth',2);
  hold on; grid;
  loglog(binArray,flipud(cumsum(flipud(histE2)))/totalTimeOffWindow,... 
         'g','LineWidth',2);
  if(not(isempty(iEnergyItf3)))
    histE3 = histc(offSource.likelihood(offSourcePass & not(lowFreqMask),iEnergyItf3) ...
                   , binArray);
    loglog(binArray,flipud(cumsum(flipud(histE3)))/totalTimeOffWindow,... 
           'r','LineWidth',2);
    legend(['Energy ' analysis.detectorList{1}],['Energy ' analysis.detectorList{2}],...
           ['Energy ' analysis.detectorList{3}]);
  else
    legend(['Energy ' analysis.detectorList{1}],['Energy ' analysis.detectorList{2}]);
  end
  title(['high freq trigger (>' num2str(frequencyThr) 'Hz)rate after ' passChoice]);
  xlabel('Energy');ylabel('Rate (Hz)');
  xlim([1 1e5]);
  
  % ---- Save plot
  plotNameHist{2} = ['/hist_rate_vs_energy12_highFreq_' analysis.clusterType ...
                 '_' passChoice];
  xsavefigure(plotNameHist{2},figures_dirName,figfiles_dirName);
  captionCell{2} = analysis.captions.ratevsenergy_highfreq;

  % ---- All triggers ITF1 vs ITF2 
  C={[0 binArray],[0 binArray]};
  N=hist3([offSource.likelihood(offSourcePass,iEnergyItf2) ...
           offSource.likelihood(offSourcePass,iEnergyItf1)],...
          'Edges', C);
  cmap=colormap;
  cmap(1,:)=1;
  N=log10(N);
  N(isinf(N))=-1;
  % KLUDGE to get that only N==0 pixels are white
  nColors=size(cmap,1);
  maxNbins=max(N(:));
  N=ceil(N/maxNbins*nColors)*maxNbins/nColors;
  % end KLUDGE
  figure;set(gca,'FontSize',20);
  pcolor(C{1},C{2},N);colormap(cmap);colorbar
  xlabel(['Energy ' analysis.detectorList{1}] );ylabel(['Energy ' analysis.detectorList{2}]);
  set(gca,'xscale','log')
  set(gca,'yscale','log')
  title(['Off source triggers after ' passChoice])

  % ---- Save plot
  plotNameHist{3} = ['/hist_rate_vs_energy1vs2_' analysis.clusterType '_' passChoice];
  xsavefigure(plotNameHist{3},figures_dirName,figfiles_dirName);
  captionCell{3} = analysis.captions.energy1vs2;
  
  % ---- add the two other projections when we have 3 detectors
  if(not(isempty(iEnergyItf3)))
    % ---- All triggers ITF1 vs ITF3 
    C={[0 binArray],[0 binArray]};
    N=hist3([offSource.likelihood(offSourcePass,iEnergyItf3) ...
             offSource.likelihood(offSourcePass,iEnergyItf1)],...
            'Edges', C);
    cmap=colormap;
    cmap(1,:)=1;
    N=log10(N);
    N(isinf(N))=-1;
    % KLUDGE to get that only N==0 pixels are white
    nColors=size(cmap,1);
    maxNbins=max(N(:));
    N=ceil(N/maxNbins*nColors)*maxNbins/nColors;
    % end KLUDGE
    figure;set(gca,'FontSize',20);
    pcolor(C{1},C{2},N);colormap(cmap);colorbar
    xlabel(['Energy ' analysis.detectorList{1}] );ylabel(['Energy ' analysis.detectorList{3}]);
    set(gca,'xscale','log')
    set(gca,'yscale','log')
    title(['Off source triggers after ' passChoice ])
  
    % ---- Save plot
    plotNameHist{4} = ['/hist_rate_vs_energy1vs3_' analysis.clusterType '_' passChoice];
    xsavefigure(plotNameHist{4},figures_dirName,figfiles_dirName);
    captionCell{4} = analysis.captions.energy1vs2;

    % ---- All triggers ITF2 vs ITF3 
    C={[0 binArray],[0 binArray]};
    N=hist3([offSource.likelihood(offSourcePass,iEnergyItf3) ...
             offSource.likelihood(offSourcePass,iEnergyItf2)],...
            'Edges', C);
    cmap=colormap;
    cmap(1,:)=1;
    N=log10(N)+0.01;
    N(isinf(N))=0;
    % KLUDGE to get that only N==0 pixels are white
    nColors=size(cmap,1);
    maxNbins=max(N(:));
    N=ceil(N/maxNbins*nColors)*maxNbins/nColors;
    % end KLUDGE
    figure;set(gca,'FontSize',20);
    pcolor(C{1},C{2},N);colormap(cmap);colorbar
    xlabel(['Energy ' analysis.detectorList{2}] );ylabel(['Energy ' analysis.detectorList{3}]);
    set(gca,'xscale','log')
    set(gca,'yscale','log')
    title(['Off source triggers after ' passChoice])
  
    % ---- Save plot
    plotNameHist{5} = ['/hist_rate_vs_energy2vs3_' analysis.clusterType '_' passChoice];
    xsavefigure(plotNameHist{5},figures_dirName,figfiles_dirName);
    captionCell{5} = analysis.captions.energy1vs2;

  end

  % ---- Put all plots into a table.
  xaddfiguretable(fout,plotNameHist,figures_dirName,captionCell);
  clear plotNameHist
  end % --- loop over choice of cuts applied to plots
end


% -- Done
