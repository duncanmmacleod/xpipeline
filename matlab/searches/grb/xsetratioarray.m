function [ratioArray deltaArray]=...
    xsetratioarray(analysis,...
                   plusVetoType,crossVetoType,nullVetoType,...
                   plusVeto,crossVeto,nullVeto)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      Set up ratioArray 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ---- ratioArray is a square array with the same number of columns 
%      as the likelihood variable. Each nonzero element is a threshold
%      to be applied to a pair of likehoods;

numberOfLikelihoods = length(analysis.likelihoodType);
ratioArray = zeros(numberOfLikelihoods,numberOfLikelihoods);
deltaArray = zeros(numberOfLikelihoods,numberOfLikelihoods);

% ---- Do we have both null likelihoods? 
if and(analysis.iNullIndex,analysis.eNullIndex)
  if strcmp(nullVetoType,'nullIoverE')   
    ratioArray(analysis.iNullIndex,analysis.eNullIndex) = ...
        nullVeto;  %-- Inull/Enull > XXX;
    disp(['Using Inull/Enull ratio cut of ' ...
          num2str(ratioArray(analysis.iNullIndex,analysis.eNullIndex))])
  elseif strcmp(nullVetoType,'nullEoverI')
    ratioArray(analysis.eNullIndex,analysis.iNullIndex) = ... 
        nullVeto;  %-- Enull/Inull > XXX;
    disp(['Using Enull/Inull ratio cut of ' ...
          num2str(ratioArray(analysis.eNullIndex,analysis.iNullIndex))])
  end
end           

% ---- Do we have both plus likelihoods? 
if and(analysis.iPlusIndex,analysis.ePlusIndex)
  if strcmp(plusVetoType,'plusIoverE')   
    ratioArray(analysis.iPlusIndex,analysis.ePlusIndex) = ...
        plusVeto;  %-- Iplus/Eplus > XXX;
    disp(['Using Iplus/plus ratio cut of ' ...
          num2str(ratioArray(analysis.iPlusIndex,analysis.ePlusIndex))])
  elseif strcmp(plusVetoType,'plusEoverI')
    ratioArray(analysis.ePlusIndex,analysis.iPlusIndex) = ... 
        plusVeto;  %-- Eplus/Iplus > XXX;
    disp(['Using Eplus/Iplus ratio cut of ' ...
          num2str(ratioArray(analysis.ePlusIndex,analysis.iPlusIndex))])
  elseif strcmp(plusVetoType,'circEoverI')
    ratioArray(analysis.ePlusIndex,analysis.iPlusIndex) = ... 
        plusVeto;  %-- Ecirc/Icirc > XXX;
    disp(['Using Ecirc/Icirc ratio cut of ' ...
          num2str(ratioArray(analysis.ePlusIndex,analysis.iPlusIndex))])
  elseif strcmp(plusVetoType,'scalarEoverI')
    ratioArray(analysis.ePlusIndex,analysis.iPlusIndex) = ... 
        plusVeto;  %-- Escalar/Iscalar > XXX;
    disp(['Using Escalar/Iscalar ratio cut of ' ...
          num2str(ratioArray(analysis.ePlusIndex,analysis.iPlusIndex))])
  elseif strcmp(plusVetoType,'ampEoverI')
    ratioArray(analysis.ePlusIndex,analysis.iPlusIndex) = ... 
        plusVeto;  %-- Eamp/Iamp > XXX;
    disp(['Using Eamp/Iamp ratio cut of ' ...
          num2str(ratioArray(analysis.ePlusIndex,analysis.iPlusIndex))])
  end
end           

% ---- Do we have both cross likelihoods? 
if and(analysis.iCrossIndex,analysis.eCrossIndex)
  if strcmp(crossVetoType,'crossIoverE')   
    ratioArray(analysis.iCrossIndex,analysis.eCrossIndex) = ... 
        crossVeto;  %-- Icross/Ecross > XXX;
    disp(['Using Icross/Ecross ratio cut of ' ...
          num2str(ratioArray(analysis.iCrossIndex,analysis.eCrossIndex))])
  elseif strcmp(crossVetoType,'crossEoverI')
    ratioArray(analysis.eCrossIndex,analysis.iCrossIndex) = ... 
        crossVeto;  %-- Ecross/Icross > XXX;
    disp(['Using Ecross/Icross ratio cut of ' ...
          num2str(ratioArray(analysis.eCrossIndex,analysis.iCrossIndex))])
  elseif strcmp(crossVetoType,'circnullIoverE') || strcmp(crossVetoType,'circnullregIoverE')
    ratioArray(analysis.iCrossIndex,analysis.eCrossIndex) = ... 
        crossVeto;  %-- Icircnull/Ecircnull > XXX;
    disp(['Using Icircnull/Ecircnull ratio cut of ' ...
          num2str(ratioArray(analysis.iCrossIndex,analysis.eCrossIndex))])
  elseif strcmp(crossVetoType,'scalarnullIoverE')
    ratioArray(analysis.iCrossIndex,analysis.eCrossIndex) = ... 
        crossVeto;  %-- Iscalarnull/Escalarnull > XXX;
    disp(['Using Iscalarnull/Escalarnull ratio cut of ' ...
          num2str(ratioArray(analysis.iCrossIndex,analysis.eCrossIndex))])
  elseif strcmp(crossVetoType,'ampnullIoverE') 
    ratioArray(analysis.iCrossIndex,analysis.eCrossIndex) = ... 
        crossVeto;  %-- Iampnull/Eampnull > XXX;
    disp(['Using Iampnull/Eampnull ratio cut of ' ...
          num2str(ratioArray(analysis.iCrossIndex,analysis.eCrossIndex))])
  end
end           
