function [output]= xgetgrbresults(parent_dir,userTag,boxType)
% xgetgrbresults - concatenates contents of .mat files produced by xmakegrbwebpage
% for multiple GRBs. 

% Usage:
%  parent_dir          path to dir containing GRB analysis dirs.
%  userTag             string, should match userTag used when running analysis.
%  boxType             string used to determine which type of results to look
%                      at, should be either 'open' or 'closed'.
%
%  output              cellarray, each element is a structure containing 
%                      results for a single GRB.
%
% E.g., output = xgetgrbresults('~/S5/MDC/S5MDC_rerun/H1H2L1_percentile_95pc_50pc',...
%   'S5yr1_MDC_rerun_95pc_50pc_r2584','closed')
%
% $Id$     

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(3, 3, nargin));

if ~max(strcmp(boxType,{'open','closed'}))
    error('boxType must be either open or closed')
end

% ---- List all dirs off the parent_dir.
dir_struct = dir([parent_dir '/GRB*']);

outIdx = 1
output={};
% ---- Loop over each GRB* dir.
for iGRB = 1:length(dir_struct)
    disp(['Working on ' dir_struct(iGRB).name]);
    auto_path = [parent_dir '/' dir_struct(iGRB).name '/auto_web/'];
    matFile = dir([auto_path dir_struct(iGRB).name '_' userTag '_' boxType 'box.mat' ]);
    if length(matFile) == 0
        warning('No matlab files found')
    elseif length(matFile) > 1
        error(['One ' boxType 'matlab file expected, more found:' matFile.name ])
    else
        matFilePath = [auto_path matFile.name];
        fprintf(1,'%s',['Loading ' matFilePath '...']);
        output{outIdx} = load(matFilePath);
        outIdx = outIdx + 1;
        fprintf(1,'%s\n','done!');
    end

end

% -- done
