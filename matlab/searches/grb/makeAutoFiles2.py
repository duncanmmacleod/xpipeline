#!/usr/bin/env python

"""
Helper function for xgrbwebpage.py, this script creates the text files 
needed by xmakegrbwebpage.m
$Id$
"""
# -------------------------------------------------------------------------
#      Setup.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, subprocess, getopt
import configparser

# ---- Function usage.
def usage():
    msg = """\
Usage: 
  makeAutoFiles2.py [options]
  -p, --params-file <file>    parameters (.ini) file [REQUIRED]
  -h, --help                  display this message and exit
"""
    print(msg, file=sys.stderr)

# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
params_file = None

# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hp:"
# ---- Long form.
longop = [
   "help",
   "params-file=",
   ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- Parse command-line arguments.  Arguments are returned as strings, so 
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-p", "--params-file"):
        params_file = a      
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not params_file:
    print("No parameter file specified.", file=sys.stderr)
    print("Use --params-file to specify it.", file=sys.stderr)
    sys.exit(1)

# ---- Status message.  Report all supplied arguments.
print(file=sys.stdout)
print("####################################################", file=sys.stdout)
print("#               makeAutoFiles2.py                  #", file=sys.stdout)
print("####################################################", file=sys.stdout)
print(file=sys.stdout)
print("Parsed input arguments:", file=sys.stdout)
print(file=sys.stdout)
print("     parameters file:", params_file, file=sys.stdout)
print(file=sys.stdout)

# -------------------------------------------------------------------------
#      Read parameters file.
# -------------------------------------------------------------------------

# ---- Status message.
print("Parsing parameters (ini) file ...", file=sys.stdout)

# ---- Create configuration-file-parser object
cp = configparser.ConfigParser()
cp.read(params_file)

# ---- Read [injection] parameters, get waveform names 
waveform_set = cp.options('waveforms')
if cp.has_option('mdc','mdc_sets') :
    mdcList = cp.get('mdc','mdc_sets')
    mdc_set = mdcList.split(',')
    waveform_set.extend(mdc_set)

# ---- Get on and off source result .mat filenames
(status,offsource) = subprocess.getstatusoutput('ls ../output | grep merged.mat | grep source | grep off')
(status,onsource)  = subprocess.getstatusoutput('ls ../output | grep merged.mat | grep source | grep on')
off_files = offsource.split('\n')
on_files  = onsource.split('\n')

# ---- Check that we only have one on and one off source results .mat file.
#      If we get this wrong then xmakegrbwebpage will misinterpret the
#      auto files.
if len(off_files) > 1 :
    print('Error: there should be exactly one ' + \
    'off*merged.mat file in output/ ', file=sys.stderr)
    sys.exit(1)

if len(on_files) > 1 :
    print('Error: there should be exactly one ' + \
    'on*.mat file in output/ ', file=sys.stderr)
    sys.exit(1)

# ---- Loop over waveforms, write auto_<set>.txt file for each.
for set in waveform_set :
    autoFilename = 'auto_' + set + '.txt'
    autoFile = open(autoFilename,'w')
    print('Writing ' + autoFilename, file=sys.stdout)
    autoFile.write('../output/' + offsource + "\n")
    (status,outstr) = subprocess.getstatusoutput('ls -v ../output | grep .mat | grep ' + set + '_ ')
    outfiles = outstr.split('\n')
    autoFile.write(subprocess.getoutput('ls -v ../output | grep .mat | grep ' + set + '_ |wc -l') + "\n")
    for outfile in outfiles :
        autoFile.write('../output/' + outfile + "\n")
    autoFile.write('../output/' + onsource + "\n")
    autoFile.close()

# ---- Get backgroundPeriod from params_file
backgroundPeriod = cp.get('background','backgroundPeriod')

# ---- Write auto.txt file
autoFilename = 'auto.txt'
autoFile = open(autoFilename,'w')
print('Writing ' + autoFilename, file=sys.stdout)
autoFile.write('../output/' + offsource + "\n")
autoFile.write('../output/' + onsource + "\n")
autoFile.write("%d"%(len(waveform_set)) + "\n")
for set in waveform_set :
    autoFile.write('auto_' + set + '.txt\n')
for set in waveform_set :
    autoFile.write('../input/injection_' + set +'.txt\n')
autoFile.write(backgroundPeriod + '\n')
autoFile.write('../input/' + "\n")
autoFile.close()

# -- Done!
