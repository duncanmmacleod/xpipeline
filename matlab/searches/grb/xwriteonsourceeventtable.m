function []=xwriteonsourceeventtable(fout,analysis,onSource,user_tag)
% XWRITEONSOURCEEVENTTABLE - write table of loudest on-source events.
% 
% xwriteonsourceeventtable writes a table of the loudest events in the 
% on-source data before and after vetoes are applied.  It is a helper 
% function for xmakegrbwebpage.m 
%
% Usage: 
%
%    xwriteonsourceeventtable(fout,analysis,onSource,user_tag)
%
%  fout            Pointer to open and writable webpage file
%  analysis        Structure created by xmakegrbwebpage
%  onSource        Structure containing onSource events
%  user_tag        String. Description of current analysis.
%
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(4, 4, nargin));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Make two tables, one of loudest events and one of loudest events 
%                           passing vetoes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for thisTable = 1:2

    % ---- Sort events using significance
    if (thisTable == 1)
        [s I] = sort(onSource.significance,'descend');
    elseif (thisTable == 2)
        [s I] = sort(onSource.significance.*onSource.pass,'descend');
    end

    % ---- write lists of quantities sorted by significance
    sLL  = onSource.likelihood(I,:);
    sBB  = onSource.boundingBox(I,:);
    sAT  = onSource.analysisTime(I);
    sNB  = onSource.nPixels(I);

    if (thisTable == 1)
        % ---- top ten of all onSource events
        if (strcmp(analysis.type,'closedbox') || ...
            strcmp(analysis.type,'snrulclosedbox'))
            fprintf(fout,'%s\n',['<h3> Table of loudest dummy on-source events </h3>']);
        else
            fprintf(fout,'%s\n',['<h3> Table of loudest on-source events <h3>']);
        end
        fprintf(fout,'%s\n',[' Rows in <font color="red">red</font> indicate ' ...
            'events that failed one or more of the consistency or DQ vetoes.']);
        nLoudestClusters = min(10,length(onSource.analysisTime));
     elseif (thisTable == 2)
         % ---- top ten of all onSource events surviving cuts
         if (strcmp(analysis.type,'closedbox') || ...
              strcmp(analysis.type,'snrulclosedbox'))
              fprintf(fout,'%s\n',['<h3> Table of loudest surviving dummy on-source events </h3>']);
         else
              fprintf(fout,'%s\n',['<h3> Table of loudest surviving on-source events <h3>']);
         end
         fprintf(fout,'%s\n',['All events in this table have ' ...
              'passed all consistency and DQ vetoes'])
         nLoudestClusters = min(10,sum(onSource.pass));
    end

    fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
    fprintf(fout,'%s\n','<br>');

    % ---- Write html table headers.
    fprintf(fout,'%s\n','<tr>');
    fprintf(fout,'%s','<td><b>significance</b></td>');
    fprintf(fout,'%s','<td><b> probability </b></td>');
    fprintf(fout,'%s','<td><b> probability (zero lag only)</b></td>');
    for likelihoodIdx=1:length(analysis.likelihoodType)
        fprintf(fout,'%s', [ '<td><b>' ...
            analysis.likelihoodType{likelihoodIdx} '</b></td>']);
    end
    fprintf(fout,'%s', [...
        '<td><b> peak time <br> GPS </b></td>' ...
        '<td><b> peak frequency<br>(Hz)</b></td>' ...
        '<td><b> start time <br> GPS </b></td>' ...
        '<td><b>duration<br>(ms)</b></td>' ...
        '</td><td><b>lowest frequency<br>(Hz)</b></td>' ...
        '<td><b> highest frequency <br>(Hz)</b></td>' ...
        '<td><b>1/analysis time</b></td>' ...
        '<td><b>number of <br>TF pixels</b></td>']);
    fprintf(fout,'%s\n','</tr>');

    % ---- Write event data.
    for iC=1:nLoudestClusters
        % ---- Color an event red if it fails a veto test.
        if onSource.pass(I(iC))
            rowcolor = 'white';
        else
            rowcolor = 'red';
        end

	event_prefix = [analysis.grb_name '_' analysis.type '_' user_tag];
	if (thisTable == 1)
	    event_webpage =  ['./events/' event_prefix '_event_' num2str(iC) '/' event_prefix '_' num2str(iC) '.html'];
	elseif (thisTable == 2)
	    event_webpage =  ['./events/' event_prefix '_event_pass' num2str(iC) '/' event_prefix '_pass' num2str(iC) '.html'];
	end

        fprintf(fout,['<tr bgcolor="' rowcolor '">']);
        % ---- Significance.
        fprintf(fout,'<td><A HREF="%s">%g</A></td>',event_webpage,onSource.significance(I(iC)));
        % ---- Probability.
        fprintf(fout,'<td>%g</td>',onSource.probability(I(iC)));
        % ---- Probability using zero lag background only.
        fprintf(fout,'<td>%g</td>',onSource.probabilityZeroLag(I(iC)));
        % ---- All likelihoods.
        for likelihoodIdx=1:length(analysis.likelihoodType)
            fprintf(fout,'<td>%g</td>',sLL(iC,likelihoodIdx));
        end
        % ---- Peak time-frequency.
        fprintf(fout,'<td>%9.5f</td>',onSource.peakTime(I(iC)));
        fprintf(fout,'<td>%g</td>',onSource.peakFrequency(I(iC)));
        % ---- Time-frequency info.
        fprintf(fout,[...
            '<td>%9.5f</td>'...
            '<td>%3.1f</td>'...
            '<td>%g</td>'...
            '<td>%g</td>'...
            '<td>%g</td>' ...
            '<td>%g</td></tr>\n'], ...
            sBB(iC,1),...
            sBB(iC,3)*1000, ...
            sBB(iC,2),...
            sBB(iC,2)+sBB(iC,4),...
            1/sAT(iC),...
            sNB(iC));
    end % ---- end loop over loudesnt events

    % ---- Close table and clean up.
    fprintf(fout,'%s\n','</table><br>');
    fprintf(fout,'%s\n','<br>');
    clear sLL sBB sAT sNB

end % ---- end of loop to make two plots
fprintf(fout,'%s\n','<br>');

