#!/usr/bin/env python
"""
XFIXEMPTYMATFILES - find empty X-Pipeline output/*.mat files and rerun the corresponding condor jobs

$Id$
The X-Pipeline local output/ directory is scanned for empty .mat files, which
can occur due to file system problems. The corresponding jobs are rerun.

Currently this script should work for empty on-source and simulation files 
produced by GRB searches (from grb.py). It will not work for empty off-source
files or for empty merged files.

"""
__author__  = 'Patrick Sutton <patrick.sutton@ligo.org>'
__date__    = '$Date$'
__version__ = '$Revision$'

import os, sys, getopt
import configparser


# ---- Function usage.
def usage():
    msg = """\
Usage: 
    xfixemptymatfiles [options]
    -d, --dir <path>        [REQUIRED] Path to directory to be checked (absolute
                            or relative).
    -h, --help              Display this message and exit.

  e.g.,
    xfixemptymatfiles.py -d /home/gareth.jones/S5/GRB070508 
"""
    print(msg, file=sys.stderr)


def xfixemptymatfiles(grb_dir):

    # -----------------------------------------------------------------------------
    #    Move to target directory.
    # -----------------------------------------------------------------------------

    # ---- Record starting directory.
    start_dir = os.getcwd()

    # ---- Move to target directory.
    os.chdir(grb_dir)

    # -----------------------------------------------------------------------------
    #    Check to see if user is also owner of the target files. 
    # -----------------------------------------------------------------------------

    # ---- Determine user running this script.
    user_name = os.popen('whoami').read().rstrip() 
    # ---- Determine owner of target files, from the home directory name.
    target_user = None
    grb_dir_full = os.getcwd() #-- use in case grb_dir is a relative path
    if grb_dir_full.startswith('/home'):
        target_user = grb_dir_full.split('/')[2]
    if user_name == target_user:
        fix_files = True
    else:
        fix_files = False
        print() 
        print("Script user (" + user_name + ") differs from owner of target files (" + target_user + ").")
        print("We will check for empty files but cannot fix them.")


    # -----------------------------------------------------------------------------
    #    Find empty output .mat files.
    # -----------------------------------------------------------------------------

    # ---- Get dump of empty files.
    print() 
    print("Scanning output/ directory tree for empty .mat files. This may take a minute ...")
    files = os.popen('find output/ -size 0').read().splitlines()
    print("Found", len(files), "empty .mat files.")

    # -----------------------------------------------------------------------------
    #    Find the jobs that made these files.
    # -----------------------------------------------------------------------------

    if fix_files:

        # ---- Read the list of empty .mat files.
        #      Procedure:
        #          Verify file path has three parts. If only 2 then it is a merged 
        #              file that is empty; code for this later.
        #          Get directory and file names as [1] and [2] entries of file path.
        #          If directory = simulations*:
        #              extract injection set, injection scale, and injection number from the file name.
        #          Elseif directory = on_source:
        #              extract event number from the file name.
        #          Elseif directory = off_source:
        #              extract event number from the file name.
        # ---- Remove any pre-existing rerun dag.
        os.system("rm -rf rerun_empty.dag*")
        rerun_dag_exists = False

        if len(files):

            # ---- Get list of currently running jobs. We won't "fix" an empty file
            #      if the job that created it is still running.
            print("Getting list of running condor jobs.")
            os.system("condor_q -format \"%d.\" ClusterId -format \"%d\n\" ProcId > .job_list.txt")
            j = open(".job_list.txt","r")
            #jobs = j.readlines()
            jobs = j.read().splitlines()
            j.close()
            print("Found", len(jobs), "running condor jobs.")

            # ---- Loop over empty .mat files.
            for path in files:
                #path = path.rstrip() #-- remove trailing \n
                print("Processing", path)
                pathlist = path.split('/')

                if len(pathlist)==2:
                    print("Empty merged .mat file - this case is not handled yet. Skipping.")

                elif len(pathlist)==3:
                    file_dir = pathlist[1]
                    if file_dir.startswith('simulations'):
                        # ---- Extract injection set, injection scale, and injection number from the file name.
                        inj_set   = pathlist[2].split('_')[1]
                        inj_scale = pathlist[2].split('_')[2]
                        inj_num   = pathlist[2].split('_')[-1][:-4]
                        # ---- Determine the nominal dag for this file.
                        dag_file = 'grb_simulations_' + inj_set + '.dag'
                        # ---- Extract the job command.
                        os.system("grep -B 2 \"macroargument3=\\\"" + inj_num + "\\\"\" " + dag_file + " > .temp.dag")
                    elif file_dir.startswith('on_source'):
                        # ---- Extract event number from the file name.
                        event_num = pathlist[2].split('_')[3]
                        # ---- Determine the nominal dag for this file.
                        dag_file = 'grb_on_source.dag'
                        # ---- Extract the job command.
                        os.system("grep -B 2 \"macroargument1=\\\"" + event_num + "\\\"\" " + dag_file + " > .temp.dag")
                    elif file_dir.startswith('off_source'):
                        # ---- Extract event number from the file name.
                        event_num = pathlist[2].split('_')[3]
                        # ---- Determine the nominal dag for this file.
                        dag_file = 'grb_off_source.dag'
                        # ---- Extract the job command.
                        os.system("grep -B 2 \"macroargument1=\\\"" + event_num + "\\\"\" " + dag_file + " > .temp.dag")
                    else:
                        print("File type not recognised (not on/off/simulation).")
                    # ---- Verify that we have found the job command. Add it to the 
                    #      rerun dag if the job that made it is no longer running.
                    filesize = os.path.getsize(".temp.dag")
                    if filesize == 0:
                        # ---- Unable to find the job. Print a warning and continue.
                        print("WARNING: unable to fix", path)
                    else:
                        # ---- We have the job info. Before adding job to the rerun 
                        #      rerun dag, make sure it is not still running.
                        # ---- First read the job unique name from .temp.dag (first line,
                        #      second column).
                        t = open(".temp.dag","r")
                        job_name = t.readlines()[0].split()[1]
                        t.close()
                        # ---- Next determine if the job that created the .mat file 
                        #      was from the specific simulations/on/off dag or from 
                        #      grb_alljobs.dag.
                        dagman_file = dag_file + '.dag.dagman.out'
                        if os.path.isfile(dagman_file):
                            pass
                        else:
                            dagman_file = 'grb_alljobs.dag.dagman.out'
                        # ---- Next figure out the condor job ID from the dagman.out file.
                        os.system("grep \"Reassigning the id of job " + job_name + "\" " + dagman_file + " | awk \'{print $NF}\' >> .job_number.txt")
                        n = open(".job_number.txt","r")
                        # ---- Use last entry (in case job ran multiple times). Remove trailing \n.
                        job_num = n.readlines()[-1].rstrip() 
                        n.close()
                        # ---- Typical format is (143022461.0.0), we want 143022461.0 
                        if job_num.startswith('('):
                            job_num = job_num[1:]
                        if job_num.endswith(')'):
                            job_num = job_num[:-1]
                        if job_num.endswith('.0.0'):
                            job_num = job_num[:-2]
                        # ---- Compare this job ID to the list of running jobs.
                        running = False
                        for running_job in jobs:
                            if job_num == running_job:
                                running = True
                        # ---- At this point we have the job command and we know if the job has finished.
                        #      If running == False then add job to the rerun dag.
                        if running == False:
                            rerun_dag_exists = True
                            os.system("cat .temp.dag >> rerun_empty.dag")

                else:
                    print("No method to handle this case.")

        # ---- Clean up.
        os.system("rm -f .temp.dag .job_list.txt .job_number.txt")

        # -----------------------------------------------------------------------------
        #    Submit the rerun dag (if non-empty).
        # -----------------------------------------------------------------------------

        if rerun_dag_exists:
            print("Submitting rerun dag to condor to regenerate empty files.")
            os.system("condor_submit_dag rerun_empty.dag")
        else:
            print("No jobs being rerun.")

    # -----------------------------------------------------------------------------
    #    Return to starting directory.
    # -----------------------------------------------------------------------------

    os.chdir(start_dir)


if __name__ == '__main__':
    # ---- This function is being executed directly as a script (as opposed to 
    #      being imported by another code). Parse the command line arguments 
    #      then run the xfixemptymatfiles() function.

    # -------------------------------------------------------------------------
    #      Parse the command line options.
    # -------------------------------------------------------------------------

    # ---- Initialize variables.
    grb_dir = None

    # ---- Syntax of options, as required by the getopt command.
    # ---- Short form.
    shortop = "hd:"
    # ---- Long form.
    longop = [
        "help",
        "dir="
        ]

    # ---- Get command-line arguments.
    try:
        opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
    except getopt.GetoptError:
        usage()
        sys.exit(1)

    # ---- Parse command-line arguments.  Arguments are returned as strings, so 
    #      convert type as necessary.
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit(0)
        elif o in ("-d", "--dir"):
            grb_dir = a      
        else:
            print("Unknown option:", o, file=sys.stderr)
            usage()
            sys.exit(1)

    # ---- Check that all required arguments are specified, else exit.
    if not grb_dir:
        print("No directory specified.", file=sys.stderr)
        print("Use the --dir option to specify one:", file=sys.stderr)
        print() 
        usage()
        sys.exit(1)

    # ---- Call the xfixemptymatfiles() function.
    xfixemptymatfiles(grb_dir)


