function xastroresults(dataFileName, webPageFileName, figures_dirName, ...
                       figfiles_dirName, popDetString, grbZdistribFile)
% XASTRORESULTS - append astrophysical interpretation of results to batch
%                 GRB report
%
% xastroresults(dataFileName, webPageFileName, ...
%               figures_dirName,figfiles_dirName, grbZdistribFile)
%
% dataFileName     String. Name of file containing the results collected
%                  by xbatchwebsummary.m     
% webPageFileName  String. Name of file containing the batch report web
%                  page to which the astrophysical interpretations will
%                  be appended
% figures_dirName  String. Name of directory containing the figures to
%                  which the report links.
% figfiles_dirName String. Name of directory containing the fig files
%                  from which the report figures were made.
% popDetString     String. Tilde delimited string containing the names of 
%                  population detection statistic to use.
% grbZdistribFile  String. Name of file containing a distribution of
%                  observed external trigger redshifts. The text file format
%                  must a single column containing the different
%                  redshifts. [OPTIONAL]

if nargin < 6
  grbZdistribFile = []
end

% load file containing all the results
load(dataFileName);

% open web page where astrophysical results will be put
summarywebPageFileName = [webPageFileName(1:end-6) '_summary.html'];
fout = fopen(summarywebPageFileName,'w');

% ---- Add links to efficiency curve / background pages produced by xbatchwebsummary.m
fprintf(fout,'%s\n',['<h1>  Summary  </h1>']);
fprintf(fout,'%s\n',['For more details see <a href="' webPageFileName ...
                    '"> eff curve zoo page </a>, and <a href="' ...
                    regexprep(webPageFileName,'batch.shtml','bkg_batch.shtml') ...
                    '"> bkg distribution zoo page</a>']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% histograms of 90% efficiencies
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% find the number of waveform sets used
nWave = length(ulRes.nameCell);

% ---- plot histograms of the 90% efficiency
disp('Creating 90% efficiency histograms')
fprintf(fout,'%s\n',['<h2>  90% efficiency </h2>']);
for iWave = 1:nWave
  figure; hold on
  set(gca,'FontSize',20)
  hist(UL90p(:,iWave),ceil(sqrt(nGRBs)));
  X=xlim;
  xlim([0 X(2)]);
  title([ulRes.nameCell{iWave} ulRes.parametersCell{iWave}]);
  xlabel('injection scale')
  ylabel('number of GRBs')
  % ---- Save plot.
  plotName{iWave} = ['/eff_' ulRes.nameCell{iWave} ulRes.parametersCell{iWave}];
  xsavefigure(plotName{iWave},figures_dirName,figfiles_dirName);
end
% ---- Put all plots into a table.
xaddfiguretable(fout,plotName,figures_dirName);

% ---- plot histograms of efficiency difference between on source and the
%      sanitized ul-box
disp('Creating efficiency open/ul-box mismatch histograms')
fprintf(fout,'%s\n',['<h2> Difference in hrss90% between ul-box and open-box</h2>']);
for iWave = 1:nWave
  figure; hold on
  set(gca,'FontSize',20)
  hist(ULdiff(:,iWave),ceil(sqrt(nGRBs)));
  title([ulRes.nameCell{iWave} ulRes.parametersCell{iWave}]);
  xlabel('hrss diff in %')
  ylabel('number of GRBs')
  % ---- Save plot.
  plotName{iWave} = ['/hrssdiff_' ulRes.nameCell{iWave} ulRes.parametersCell{iWave}];
  xsavefigure(plotName{iWave},figures_dirName,figfiles_dirName);
end
% ---- Put all plots into a table.
xaddfiguretable(fout,plotName,figures_dirName);

% ---- plot histograms of efficiency difference between Flare Fit and the
%      sanitized ul-box
disp('Creating efficiency interpolation/FlareFit mismatch histograms')
fprintf(fout,'%s\n',['<h2> Difference in hrss90% between interpolation ' ...
                    'and FlareFit for UL-box </h2>']);
ULdiffFF = 200*(UL90p-UL90pFF)./(UL90p+UL90pFF);
for iWave = 1:nWave
  figure; hold on
  set(gca,'FontSize',20)
  hist(ULdiffFF(:,iWave),ceil(sqrt(nGRBs)));
  title([ulRes.nameCell{iWave} ulRes.parametersCell{iWave}]);
  xlabel('hrss diff in %')
  ylabel('number of GRBs')
  % ---- Save plot.
  plotName{iWave} = ['/hrssdiffFF_' ulRes.nameCell{iWave} ulRes.parametersCell{iWave}];
  xsavefigure(plotName{iWave},figures_dirName,figfiles_dirName);
end
% ---- Put all plots into a table.
xaddfiguretable(fout,plotName,figures_dirName);

% ---- plot histograms of exclusion distances
disp('Creating exclusion distance histograms')
fprintf(fout,'%s\n',['<h2>  Exclusion distance </h2>']);
for iWave = 1:nWave
  % parse injection parameters
  parameters = tildedelimstr2numorcell(ulRes.parametersCell{iWave}); 
  % determine nominal injection distance in Mpc 
  switch ulRes.nameCell{iWave}
   case 'inspiral'
    nominalDist(iWave)  = parameters(4);
   case 'chirplet'
    % physical constants
    Msolar = 1.9891e30;         %-- solar mass in kg
    G = 6.673*1e-11;            %-- the gravitational constant in m^3/(kg*s^2)
    c = 299792458;              %-- the speed of light in m/s
    pc = 3.0856775807*1e16;     %-- a parsec in m
    Mpc = pc*1e6;               %-- a mega-parsec in m
    % convert hrss at ref energy into distance for circular rotator
    refEnergy = 1e-2*Msolar*c^2;
    h_rss = parameters(1);
    f0 = parameters(3);
    nominalDist(iWave) = sqrt(10*G*refEnergy/(c^3*h_rss^2*(2*pi*f0)^2))/Mpc;
   otherwise
    error(['Unrecognized waveform type: ' ulRes.nameCell{iWave} ...
           '. Only inspiral and chirplet are currently supported'])
  end
  % plot the figure
  figure; hold on
  set(gca,'FontSize',20)
  %  use log spaced bins
  distBinEdges=[-inf logspace(log10(0.02),log10(200),30) inf];
  n(iWave,:)=histc(nominalDist(iWave)./UL90p(:,iWave), distBinEdges);
  stairs(distBinEdges,n(iWave,:))
  X=xlim;
  xlim([0.01 X(2)*1.1]);
  set(gca,'xscale','log');grid;
  title([ulRes.nameCell{iWave} ulRes.parametersCell{iWave}]);
  xlabel('exclusion distance (Mpc)')
  ylabel('number of GRBs')
  % ---- Save plot.
  plotName{iWave} = ['/dist_' ulRes.nameCell{iWave} ulRes.parametersCell{iWave}];
  xsavefigure(plotName{iWave},figures_dirName,figfiles_dirName);
end
% ---- Put all plots into a table.
xaddfiguretable(fout,plotName,figures_dirName);

% ---- combine all distance exclusions into one plot
figure; hold on
set(gca,'FontSize',20)
colorSet={'b','r--','g','k--','m'};
for iWave=1:nWave
  stairs(distBinEdges,n(iWave,:),colorSet{iWave},'linewidth',2*(nWave+1-iWave))
end
legend(ulRes.parametersCell)
legend('hide')
X=xlim;
xlim([0.01 X(2)*1.1]);
set(gca,'xscale','log');grid;
xlabel('exclusion distance (Mpc)')
ylabel('number of GRBs')
plotName = '/dist_combined';
xsavefigure(plotName,figures_dirName,figfiles_dirName);
xaddfiguretable(fout,plotName,figures_dirName);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Write latex result table             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Creating latex result table')

% --- open result table
tableName = ['batchSummary_' user_tag '.tex'];
ftable = fopen(tableName,'w');

% --- open grb info
tableName = ['batchSummary_grbinfo_' user_tag '.tex'];
fgrb = fopen(tableName,'w');

% --- open reduced for publication table
tableName = ['batchSummary_paper_' user_tag '.tex'];
fpaper = fopen(tableName,'w');

% --- write out GRBs sorted by name
[tmp IsortedGRB]=sort(grbName);
for iGRB = IsortedGRB
  % convert GPS time to UTC time
  [status clockTime] = system(['lal_tconvert -f "%T" ' ...
                      num2str(analysis{iGRB}.gpsCenterTime)]);
  % compute RA, Dec from earth fixed coordinates and time
  [ra dec] = earthtoradec(analysis{iGRB}.skyPositions(1,2), ...
                          analysis{iGRB}.skyPositions(1,1), ...
                          analysis{iGRB}.skyPositionsTimes(1,1));
  [raStr, decStr] = radec2string(ra, dec);
  % print line of info on given GRB
  fprintf(ftable,'%s & %s & %s & %s & %s & [%d,%d] & %.3g & (%d)', ...
          grbName{iGRB}(4:9), ...
          clockTime(1:end-2), ...
          raStr, decStr, ...
          [analysis{iGRB}.detectorList{:}], ...
          analysis{iGRB}.onSourceBeginOffset,analysis{iGRB}.onSourceEndOffset, ...
          pOnSource(iGRB), numULjobs(iGRB));
  fprintf(ftable,' & %3.1f', ...
          nominalDist./UL90p(iGRB,:));
  fprintf(ftable,' \\\\\n');

  % KLUDGE, hard code the list of possible detectors
  detList={'H1','L1','V1'};
  for iDet = 1:length(detList)
      % compute antenna response of each detector only if used in analysis
    iDetInList = find(strcmp(detList{iDet},analysis{iGRB}.detectorList));
    if not(isempty(iDetInList))
      antennaFactor{iDet} = sprintf('%.2f',...
          sqrt(analysis{iGRB}.FpList(iDetInList).^2+analysis{iGRB}.FcList(iDetInList).^2));
    else
      % otherwise replace antenna response with a dash
      antennaFactor{iDet} = '--';
    end
  end
  % write down to tex formatted table
  fprintf(fgrb,'%s & %s & %s & %s & %s & %s & %s',...
          grbName{iGRB}(4:end), ...
          clockTime(1:end-2), ...
          raStr, decStr, ...
          antennaFactor{1}, antennaFactor{2}, antennaFactor{3});
  % don't add trailing \\ so that the table can merged with additional
  % information not present here (T90, redshift, satellite providing
  % trigger, ...)
  fprintf(fgrb,' \n');
  % ---- paper version of the table
  % anotation to GRB name 
  % * for any GRB which has a -120 second on source window
  % + for any GRB which uses the T90 for the positive value of the onsource
  %   window
  if analysis{iGRB}.onSourceBeginOffset == -120
    annoBegin = '$^*$';
  else
    annoBegin = '';
  end
  if analysis{iGRB}.onSourceEndOffset > 60
    annoEnd = '$^\dagger$';
  else
    annoEnd = '';
  end  
  % print out only CSG150, CSG300, NSNS and NSBH
  waveformList=[2 4 5 3];
  fprintf(fpaper,'%s & %s & %s & %s & %s ',...
          [grbName{iGRB}(4:9)], ...
          clockTime(1:end-2), ...
          raStr, decStr, ...
          [analysis{iGRB}.detectorList{:} annoBegin annoEnd]);
  fprintf(fpaper,' & %3.1f', ...
          nominalDist(waveformList)./UL90p(iGRB,waveformList));
  fprintf(fpaper,' \\\\\n');
          
end

% close the latex table files
fclose(ftable);
fclose(fgrb);
fclose(fpaper);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Perform binomial null test         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% taken from
% cvs_bursts/projects/xpipeline/s5/grb/results/binomialTest/binomial_threshold_S5_cc.m

fprintf(fout,'%s\n',['<h2>  Binomial null test </h2>']);
% MAGIC NUMBERS
Nmc = 1e5;
Ptail = 0.05; %-- test top 5%.

% sort the p-value and take the top Ptail
[p,I] = sort(pOnSource);
Ndraws = length(p);
Ntail = ceil(Ptail*Ndraws);

% KLUDGE
% increase apparent number of bkg trials to avoid low background sampling problems
inflateFactor = 1e4;
if inflateFactor ~= 1
  fprintf(fout,['<strong>WARNING: inflate nb of bkg trials by %f ' ...
                '</strong><br>\n'],inflateFactor);
end
% END KLUDGE

% ---- Perform binomial test on all GRBs.
disp('Perform binomial test on all GRBs...');
[Pbinom, Pmin, index] = grbbinomialtest(p(1:ceil(Ptail*Ndraws)),numULjobs*inflateFactor,Nmc);
fprintf(fout,'%s<br>\n',['Binomial test results for all GRBs:']);
fprintf(fout,'%s<br>\n',['  Pbinom = ' num2str(Pbinom) ]);
fprintf(fout,'%s<br>\n',['  Pmin = ' num2str(Pmin) ]);
fprintf(fout,'%s<br>\n',['  index = ' num2str(index) ]);
if (Pmin>0.1)
  disp(['  Consistency with the null hypothesis is ' num2str(Pmin) '.  Get a real job.']);
else
  disp(['  Consistency with the null hypothesis is ' num2str(Pmin) '.']);
end

% ---- Compute distribution of minimum binomial probability under the null
%      hypothesis.
threshold = 5;
disp(['Find ' num2str(threshold) ' percentile cumulative binomial probability']);
P_1pc = grbbinomialtest_threshold(numULjobs*inflateFactor,Ntail,threshold,Nmc);

% ---- Compute binomial probabilities for 1% consistency with null
%      hypothesis (consistency reported in paper).
localProbMC = logspace(-5,-1,100);
p0d948 = zeros(Ntail,1);
for itail = 1:Ntail
  PMC = log(1 - binocdf(itail-1,Ndraws,localProbMC));
  % approximate to low value by first binomial term in cdf
  PMC(PMC<log(1e-15)) = log(nchoosek(Ndraws,itail)) + itail* ...
      log(localProbMC(PMC<log(1e-15))); 
  p0d948(itail) = exp(interp1(PMC,log(localProbMC),log(P_1pc)));
end

% ---- Plot binomial test
figure; set(gca,'fontsize',16);
loglog([1e-9;1],[1e-9*Ndraws;Ndraws],'k--','linewidth',1)
grid on; hold on
loglog(p,[1:Ndraws]','-o','linewidth',2,'markerfacecolor','b')
% add first to obtain a starting vertical line, and extend last bar up to infinity
prcEdge = [[0:Ntail]']+0.5;
prcEdge(Ntail+1) = nGRBs;
stairs([p0d948(1)*0.99; p0d948],prcEdge,'k-','linewidth',2)
axis([min(min(p),1e-4) 1 3e-1 Ndraws+1])
axis square
xlabel('local probablity p');
ylabel('number of GRBs')
legend('expected','data',['needed for ~' num2str(threshold) '% CL'],'Location','SouthEast')
loglog(p(index),index,'rs','markerfacecolor','r')
% ---- Save plot and put into table
plotName = '/binomialTest';
xsavefigure(plotName,figures_dirName,figfiles_dirName);
xaddfiguretable(fout,plotName,figures_dirName);
clear plotName

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Perform population detection LR test   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Use same method as for ul-box page to sanitize efficiency curves
sizeSigInj = size(sigInj);
sizeSigInj(1:2) = 1;
mask = sigInj < 1.01*repmat(max(sigInj(:,:,:,1),[],3),sizeSigInj);sigInj(mask) = 0;

% ----- Generate a single set of MC realization and use the same one for all
%       test (should reduce stat error of comparision)
pMC = rand([size(detSensCL99) Nmc]);
nInjMC = 1e3;
nInj = size(sigInj,3);
nScale = size(sigInj,4);
configMC = unidrnd(nInj,[nGRBs nWave nScale nInjMC]);
volScaleMC = rand([nGRBs nWave nScale nInjMC]).^(-1/3).* ...
    repmat(injScale(1,1,:),[nGRBs nWave 1 nInjMC]);
refScaleMC = ones(size(volScaleMC));
correction = zeros(size(volScaleMC));
for iScale=1:(nScale-1)
  mask = volScaleMC >= injScale(1,1,iScale) & volScaleMC < injScale(1,1,iScale+1);
  refScaleMC(mask) = iScale;
  correction(mask) = (log(volScaleMC(mask))-log(injScale(1,1,iScale)))./...
      (log(injScale(1,1,iScale+1))-log(injScale(1,1,iScale)));
end

bkgMC = rand([nGRBs nWave nScale nInjMC]);
% rescale detector sensitivity to best one
detSensCL99 = detSensCL99./repmat(min(detSensCL99),[nGRBs 1]);

% loop over method
methodCell = tildedelimstr2numorcell(popDetString);
for iMethod = 1:length(methodCell)
popMethod = methodCell{iMethod};
disp(['Performing population detection test with: ' popMethod])
fprintf(fout,'%s\n',['<h2>  Population detection, method: ' popMethod ' </h2>']);

% compute population statistic for the on-source p-values
popDetStat = popdetstat(...
    repmat(pOnSource',[1 size(detSensCL99,2)]),popMethod,detSensCL99);
% compute population statistic for Nmc realizations of the null hypothesis
popDetStatMC = popdetstat(pMC, popMethod, detSensCL99);

% ---- plot background distribution of pop detection statistic and obtained results
% cover cumulative probabilities from 1e-3 to 1
plotThr = logspace(-3,0,100);
popDetStatBkgPlot = prctile(popDetStatMC,(1-plotThr)*100,2);
figure; hold on
set(gca,'FontSize',20)
plot(popDetStatBkgPlot(1,:),plotThr,'k-','linewidth',2);
% interpolate background distribution to find background prob of
% on-source p-values, put nan in case of error (common in closed-box
% analysis case)
[tmp uniqueXvalues] = unique(popDetStatBkgPlot(1,:));
try
  popBkgProb = interp1(popDetStatBkgPlot(1,uniqueXvalues), ...
                       plotThr(uniqueXvalues)',popDetStat(1));
catch
  warning('Failed to compute on-source bkg prob')
  popBkgProb = nan;
end
plot(popDetStat(1),popBkgProb,'r+','linewidth',2,'MarkerFaceColor','r','MarkerSize',20)
set(gca,'yscale','log');grid;
ylim([1e-3 1])
xlabel('population detection statistic')
ylabel('cumulative probability')
% ---- Save plot.
plotName = ['/popsig_' popMethod '_'  ulRes.nameCell{iWave} ulRes.parametersCell{iWave}];
xsavefigure(plotName,figures_dirName,figfiles_dirName);
xaddfiguretable(fout,plotName,figures_dirName);
clear plotName

% --- write out summary table of distribution at different percentile level
fprintf(fout,'Population detection statistic values: <br>');
fprintf(fout,'%f, ',popDetStat);
fprintf(fout,'<br>');
prcThr = [(1-popBkgProb)*100 95 99 99.7 99.9];
popDetStatBkgPrc = prctile(popDetStatMC,prcThr,2);
fprintf(fout,'<table border=1 cellspacing="1" cellpadding="5"><tr>');
fprintf(fout,'<td> %f </td>',prcThr);
for iWave = 1:nWave
  fprintf(fout,'</tr>\n<tr>');
  fprintf(fout,'<td> %f </td>',popDetStatBkgPrc(iWave,:));
end
  fprintf(fout,'</tr></table><br>');


% ---- estimate sensitivity of this detection test
disp('Estimating sensitivity of population detection test')
% uses the 4th listed percentile level
detStatThr = popDetStatBkgPrc(:,4)
prcDetStatThr = prcThr(4);
% peak randomly one injection for each GRB at given wf/injection scale
for iGRB=1:nGRBs
  for iWave=1:nWave
    for iScale=1:nScale
      % add noise (uniform in [0,1]) to signal, saturate the significance
      % of an injection at 5 sigma (prob > 1e-7), we are not able to go
      % beyond that with any currently available background estimation
      % scheme
      sigBkgProbMC(iGRB,iWave,iScale,:) = ...
          min(squeeze(bkgMC(iGRB,iWave,iScale,:)),...
              max(1e-7,...
                  squeeze(sigInj(iGRB,iWave,configMC(iGRB,iWave,iScale,:),iScale)).^bkgFit(iGRB,1).*...
              exp(bkgFit(iGRB,2))/numULjobs(iGRB)));
      % same but with distance randomly distributed in volume, compute by
      % log interpolating the nearest smaller and larger injection scale.
      % Assumes that detection statistic is growing as powerlaw with amplitude
      % (not true for 3 detectors powerlaw statistic, but should be good
      % approximation)
      %
      % find corresponding injection below and above the intended
      % injection scale
      smallerInjection =  iGRB+nGRBs*(iWave-1)+nGRBs*nWave*(configMC(iGRB,iWave,iScale,:)-1)+...
          nGRBs*nWave*nInj*(refScaleMC(iGRB,iWave,iScale,:)-1);
      largerInjection =  iGRB+nGRBs*(iWave-1)+nGRBs*nWave*(configMC(iGRB,iWave,iScale,:)-1)+...
          nGRBs*nWave*nInj*(min(refScaleMC(iGRB,iWave,iScale,:),nScale-1));
      % interpolate the significance assuming a powerlaw relation with
      % injection scale
      interpSigInj = sigInj(smallerInjection).^correction(smallerInjection).*...
          sigInj(largerInjection).^(1-correction(smallerInjection));
      sigBkgProbMCvol(iGRB,iWave,iScale,:) = ...
          min(squeeze(bkgMC(iGRB,iWave,iScale,:)),...
              max(1e-7,...
              squeeze(interpSigInj).^bkgFit(iGRB,1).*...
              exp(bkgFit(iGRB,2))/numULjobs(iGRB)));
    end
  end
end

% compute detection statistic for the two type of samples containing
% signal (at fixed distance and uniformly distributed in given volume)
sigBkgDetStatMC = popdetstat(sigBkgProbMC,popMethod,detSensCL99);
sigBkgDetStatMCvol = popdetstat(sigBkgProbMCvol,popMethod,detSensCL99);
clear sigBkgProbMC sigBkgProbMCvol
% compute efficiency 
effPopMC = squeeze(sigBkgDetStatMC) > repmat(detStatThr,[1 nScale nInjMC]);
effPopMC = squeeze(sum(effPopMC,3))/nInjMC;
effPopMCvol = squeeze(sigBkgDetStatMCvol) > repmat(detStatThr,[1 nScale nInjMC]);
effPopMCvol = squeeze(sum(effPopMCvol,3))/nInjMC;


fprintf(fout,['<strong> WARNING: these curves use background fitting for ' ...
              'finding injections background probability</strong><br>']);
fprintf(fout,['Computed for ' num2str(1-prcDetStatThr) ' background probability ' ...
                    'detection threshold <br>']);
% --- make detection efficiency plots
for iWave=1:nWave
  % plot at fixed distance
  figure; hold on
  set(gca,'FontSize',20)
  % rescale injection scale to distance in Mpc
  plot(squeeze(nominalDist(iWave)./injScale(1,iWave,:)),...
         squeeze(effPopMC(iWave,:)),'k-o','linewidth',2,'MarkerFaceColor','k');
  set(gca,'xscale','log');grid;
  xlim([0.1 1e5]);ylim([0 1])
  title([ulRes.nameCell{iWave} ulRes.parametersCell{iWave}]);
  xlabel('fixed distance (Mpc)')
  ylabel('Pop detection efficiency')
  % ---- Save plot.
  plotName{iWave} = ['/popdisteff_' popMethod '_'  ulRes.nameCell{iWave} ulRes.parametersCell{iWave}];
  xsavefigure(plotName{iWave},figures_dirName,figfiles_dirName);

  % plot uniformly distributed in volume
  figure; hold on
  set(gca,'FontSize',20)
  % rescale injection scale to distance in Mpc
  plot(squeeze(nominalDist(iWave)./injScale(1,iWave,:)),...
         squeeze(effPopMCvol(iWave,:)),'k-o','linewidth',2,'MarkerFaceColor','k');
  set(gca,'xscale','log');grid;
  xlim([0.1 1e5]);ylim([0 1])
  title([ulRes.nameCell{iWave} ulRes.parametersCell{iWave}]);
  xlabel('volume radius (Mpc)')
  ylabel('Pop detection efficiency')
  % ---- Save plot.
  plotNameVol{iWave} = ['/popdisteffvol_' popMethod '_'  ulRes.nameCell{iWave} ulRes.parametersCell{iWave}];
  xsavefigure(plotNameVol{iWave},figures_dirName,figfiles_dirName);

end
xaddfiguretable(fout,plotName,figures_dirName);
xaddfiguretable(fout,plotNameVol,figures_dirName);

if 0
% --- make detection efficiency for uniform in volume assumption by
%     marginalizing over the fixed detection curve. Skip by default as it
%     fails if fixed distance efficiency curve has problems
for iWave=1:nWave
  volumeRadius = nominalDist(iWave)*10.^[-1:0.1:2];
  for iRadius = 1:length(volumeRadius)
    volEffMC(iWave,iRadius) = ...
        volumeefficiency(squeeze(injScale(1,iWave,:)),...
                         squeeze(effPopMC(iWave,:))',...
                         nominalDist(iWave),volumeRadius(iRadius));
  end

  figure; hold on
  set(gca,'FontSize',20)  
  plot(volumeRadius,volEffMC(iWave,:),'linewidth',2);
  set(gca,'xscale','log');grid;
  xlim([0.1 1e5])
  title([ulRes.nameCell{iWave} ulRes.parametersCell{iWave}]);
  xlabel('post fact volume radius (Mpc)')
  ylabel('Pop detection efficiency')
  % ---- Save plot.
  plotName{iWave} = ['/popvoleff_' popMethod '_' ulRes.nameCell{iWave} ulRes.parametersCell{iWave}];
  xsavefigure(plotName{iWave},figures_dirName,figfiles_dirName);
end
xaddfiguretable(fout,plotName,figures_dirName);
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform population exclusion   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Creating population exclusion plot')

[popEff popEffFF popEffDist popEffBino distribExcl distribExclBino] = ...
    xpopexclusion(fout,figures_dirName,figfiles_dirName,nominalDist, ...
                  ulRes.nameCell,ulRes.parametersCell,...
                  injScale,effCurve,injScaleFF,effCurveFF,grbZdistribFile);

% ---- close result web page
fclose(fout);

% save all results in new file
save([dataFileName(1:end-4) '_astro.mat']);


