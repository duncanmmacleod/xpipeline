function xoffsourceunion_S(dirName,fileListString,outFile)
% XOFFSOURCEUNION_S - generalised clustering of on/off-source triggers
%
% usage:
%
%   xoffsourceunion_S(dirName,fileList,outFile)
%
%   dirName    String. Directory containing all files to be read.
%   fileList   String. Space-separated list of results files to be read, e.g.
%              'off_source_0_26270_0.mat off_source_0_26271_0.mat off_source_0_26272_0.mat' 
%   outFile    Optional string. File to output merged clusters to. If not
%              specified, original input files are over-written. 
%
% $Id$

% ---- Checks.
error(nargchk(2,3,nargin));

% ---- Parse file list string into cell array of separate file names.
fileList = strread(fileListString,'%s');

% ---- Loop over all files to be merged.
for iFile = 1:length(fileList)
    fileName = [dirName '/' fileList{iFile}];
    s = load(fileName);

    % ---- find fake job numbers for each of the slides
    fjn = unique(s.cluster.fakeJobNumber);
    for m=1:length(fjn)

        % ---- Keep all elements with flag injectionProcessedMask == 1.
        index = find(s.cluster.fakeJobNumber==fjn(m));
        % ---- find nearby triggers in time and freq
        bb = s.cluster.boundingBox(index,:);
        Ntrig = size(bb,1);
        % ---- Define coincidence window in box format.
        win = [0 0 5 8];
        % ---- Test all pairs of triggers for overlap.
        olp = rectint(bb+repmat(win,size(bb,1),1),bb+repmat(win,size(bb,1),1));
        olp = triu(olp,1);
        [indI,indJ] = find(olp);
        % ---- Combine pairs into two-column matrix.
        pairs = [indI indJ];
    
        % ------------------------------------------------------------------------------

        % ---- Only proceed if we have triggers that need to be merged.
        if ~isempty(pairs)
        
            % ---- Split "pairs" matrix into two columns for convenience.
            pairsI = pairs(:,1);
            pairsJ = pairs(:,2);
            % ---- Complete list of triggers to be checked for membership of a generalised cluster.
            trigToCheck = 1:Ntrig;
            % ---- Storage for list of triggers in each generalised cluster.
            genClust = cell(Ntrig,1);
            % ---- Counter over generalised clusters.
            igenClust = 0;

            % ---- Loop over all Ntrig triggers, starting from the last one.
            while ~isempty(trigToCheck)    
                % ---- Start new generalsied cluster.
                igenClust = igenClust + 1;
                % ---- Select next trigger to check, from the end of the list.
                iTrig = trigToCheck(end);
                k = find(pairsI==iTrig | pairsJ==iTrig);
                % ---- Add all pairs containing this trigger to our current generalsied
                %      cluster, or just this one trigger if it is not paired with anyone else.
                if ~isempty(k)
                    tmp = pairs(k,:);
                    genClust{igenClust} = union(genClust{igenClust},tmp(:)');
                    % ---- Delete all pairs containing this trigger from our working list.
                    pairs(k,:) = [];
                    pairsI(k,:) = [];
                    pairsJ(k,:) = [];
                else
                    genClust{igenClust} = iTrig;
                end
                % ---- We're finished with this trigger. Delete it from the list of
                %      to-be-checked and add it to the list of triggers we've checked. 
                trigToCheck = setdiff(trigToCheck,iTrig); 
                checkedTrigs = iTrig;
                % ---- By adding pairs to our generalsied cluser, we may have introduced new
                %      triggers. We now have to check each of the new triggers for any
                %      additional triggers they're clustered with that also have to be added ... 
                newtrigs = setdiff(genClust{igenClust},checkedTrigs);
                while ~isempty(newtrigs)
                    for ii = 1:length(newtrigs)
                        k = find(pairsI==newtrigs(ii) | pairsJ==newtrigs(ii));
                        if ~isempty(k)
                            tmp = pairs(k,:);
                            genClust{igenClust} = union(genClust{igenClust},tmp(:)');
                            pairs(k,:) = [];
                            pairsI(k,:) = [];
                            pairsJ(k,:) = [];
                        end
                    end
                    checkedTrigs = union(checkedTrigs,newtrigs);
                    trigToCheck = setdiff(trigToCheck,checkedTrigs); % -- finished with this trigger
                    % --- Are there more new triggers?  If so, we go through the while loop
                    %     again.  If not, we go on to the next generalised cluster.
                    newtrigs = setdiff(genClust{igenClust},checkedTrigs);
                end        
            end

            % ---- Delete empty genClusts.
            genClust = genClust(1:igenClust);

            % ------------------------------------------------------------------------------

            % ---- Merge triggers to be clustered together.
            for igenClust=1:length(genClust)

                % ---- Triggers to be merged for this generalised cluster.
                trigIdx = genClust{igenClust};

                % ---- chop the initial trigger list and reshape the boundingBox
                % ---- apply threshold on number of pixels <20px, second trial ---- %
                newTriggers = xclustersubset(s.cluster,index(trigIdx));
                newTriggers = xclustersubset(s.cluster,index(trigIdx));
                %if length(newTriggers.nPixels)==1.0
                %    lf = 1.0;
                %else
                    % ---- we dont trust a 2-point line
                    %if length(newTriggers.nPixels)==2.0
                    %    lf = 1.0;
                    %else
                    %    xx = newTriggers.peakTime - newTriggers.startTime;
                    %    yy = newTriggers.peakFrequency;
                    %    line = polyfit(xx,yy,1);
                    %    r2=Rsquare(xx,yy,line);
                        % ---- try extract signals from signals+junk
                    %    if r2<0.9 & r2>0.1
                    %        fit1 = fit(xx,yy,'poly1');
                    %        fdata = feval(fit1,xx);
                    %        I = abs(fdata - yy) > 50.0;
                    %        outliers = excludedata(xx,yy,'indices',I);
                    %        if length(find(I==0))>1.99
                    %            fit2 = fit(xx,yy,'poly1','Exclude',outliers);
                    %            nlc = coeffvalues(fit2);
                    %            xx(I) = [];
                    %            yy(I) = [];
                    %            newr2 = Rsquare(xx,yy,[nlc(1),nlc(2)]);
                    %        else
                    %            newr2 = 1.0;
                    %        end
                            % ---- boost well-fitted data
                            %if newr2>0.9 & newr2<0.9999999
                            %    if nlc(1) > -0.6 & nlc(1) < -0.24
                            %        disp('Boosted after cleaning');
                            %        lf = 1.0/(1.0-newr2);
                            %    else
                            %        lf = 1.0;
                            %    end
                            %else
                            %    lf = 1.0;
                            %end
                        %end
                        % ---- boost well-fitted data
                        %if r2>0.9 & r2<1.0001
                        %    if line(1) > -0.6 & line(1) < -0.24
                        %        disp('Boosted');
                        %        lf = 1.0/(1.0-r2);
                        %    else
                        %        lf = 1.0;
                        %    end
                        %else
                        %    lf = 1.0;
                        %end
                    %end
                %end
                %disp(lf);
                for p=1:length(newTriggers.nPixels)
                    if newTriggers.nPixels(p)<30
                        newTriggers.nPixels(p) = 1;
                        newTriggers.likelihood(p,:) = 0.0;%newTriggers.likelihood(p,:).*(newTriggers.nPixels(p)/100.0);
                        newTriggers.significance(p) = 0.0;%newTriggers.significance(p)*newTriggers.nPixels(p)/100.0;
                    end
                end
                % ---- apply weighted average on number of pixels ---- %
                % ---- downgrade triggers in [350,450] Hz frequency band
                if length(trigIdx)>1
                    for g=1:length(trigIdx)
                        % ---- introduce a prefactor to rid off of significance < 2.75
                        % ---- and scale the off/inj by prf1234
                        % ---- demostrated working fine newTriggers.likelihood(g,2) > 1.00001
                        % ---- demonstrated working the SAME for JW without the restrictions apart from signif
                        if newTriggers.likelihood(g,2) > 1.00001 & (1.0 - sqrt(log(newTriggers.likelihood(g,2))/(newTriggers.likelihood(g,8)))) > 0.00000001 & newTriggers.boundingBox(g,3) > 10.01% & newTriggers.boundingBox(g,4) < 125.01
                            % introduce basic coherence checks
                            % --------------------------------
                            % do alphaNullIoverE>1.82
                            %likelihood1 = newTriggers.likelihood(g,8:9);
                            %ratioArray1=zeros(2,2);
                            %ratioArray1(1,2) = 1.82;
                            %coh_prf1 = xapplyalpharatiotest(likelihood1,ratioArray1);
                            % do circEoverI> 1.2
                            %likelihood2 = newTriggers.likelihood(g,10:11);
                            %ratioArray2=zeros(2,2);
                            %ratioArray2(1,2) = 1.27;
                            %coh_prf2 = xapplyratiotest(likelihood2,ratioArray2);
                            % done coherent tests, get prefactors now
                            %prf1 = 1.0 - sqrt(log(newTriggers.likelihood(g,2))/(newTriggers.nPixels(g)));
                            %prf2 = (newTriggers.boundingBox(g,3) + 0.0000001)/(log(newTriggers.likelihood(g,2)));
                            %prf3 = 1.0 - sqrt(log(newTriggers.likelihood(g,2))/(newTriggers.boundingBox(g,4) + 0.0000001));
                            prf1 = (newTriggers.boundingBox(g,3)*newTriggers.boundingBox(g,4))/(newTriggers.nPixels(g)*log(newTriggers.likelihood(g,2)));
                            prf4 = 1.0 - sqrt(log(newTriggers.likelihood(g,2))/(newTriggers.likelihood(g,8)));
                            %prf5 = ((newTriggers.boundingBox(g,3) + 0.0000001)*(newTriggers.boundingBox(g,4) + 0.0000001))*(newTriggers.nPixels(g));
                            % ---- three more prefactors
                            %prf6 = 1.0 - sqrt(log(newTriggers.likelihood(g,2))/(newTriggers.likelihood(g,4)));
                            %prf7 = 1.0 - sqrt(log(newTriggers.likelihood(g,2))/(newTriggers.likelihood(g,6)));
                            %prf8 = 1.0 - sqrt(log(newTriggers.likelihood(g,2))/(newTriggers.likelihood(g,10)));
                            prf9 = abs(newTriggers.likelihood(g,8) - newTriggers.likelihood(g,9))/newTriggers.nPixels(g);
                            %if newTriggers.boundingBox(g,4)/newTriggers.boundingBox(g,3)>15.0
                            %    asp = 0.0;
                            %else
                            %    asp = 1.0;
                            %end
                            prf = abs(prf1*prf1*prf4*prf9*prf9);
                        else
                            prf = 0.0;
                        end
                        % ---- boost/downgrade prefactors that are most likely to be associated with injections/background
                        % ---- boost a lot most likely injections
                        %if prf > 0.3
                        %    prf = 10.0*prf;
                        %end
                        % ---- mildly boost the ones in the 'grey area'
                        %if prf<0.25 & prf>0.05
                        %    prf = 10.0*prf;
                        %end
                        % ---- downgrade the most likely background
                        %if prf < 0.4
                        %    prf = prf^2.0;
                        %end
                        % disp(prf);
                        %if newTriggers.peakFrequency(g)>350.0 & newTriggers.peakFrequency(g)<450.0
                        if newTriggers.peakFrequency(g)>340.0 & newTriggers.peakFrequency(g)<460.0 | newTriggers.peakFrequency(g)>670.0 & newTriggers.peakFrequency(g)<700.0 | newTriggers.peakFrequency(g)>790.0 & newTriggers.peakFrequency(g)<820.0 | newTriggers.peakFrequency(g)>655.0 & newTriggers.peakFrequency(g)<665.0
                        % ---- try the cut on violin modes from below
                            newTriggers.likelihood(g,:) = 0.0;
                            newTriggers.significance(g) = 0.0;
                        else
                            newTriggers.likelihood(g,:) = newTriggers.likelihood(g,:)*newTriggers.nPixels(g)*prf;
                            newTriggers.significance(g) = newTriggers.significance(g)*newTriggers.nPixels(g)*prf;
                        end
                    end
                    % ---- weigh by sum(nPixels)
                    newTriggers.likelihood(1,:) = (sum(newTriggers.likelihood))/(sum(newTriggers.nPixels));
                    newTriggers.significance(1) = (sum(newTriggers.significance))/(sum(newTriggers.nPixels));
                else
                    % ---- plop prf here again
                    % ---- demostrated working fine newTriggers.likelihood(1,2) > 1.00001
                    if newTriggers.likelihood(1,2) > 1.00001 & (1.0 - sqrt(log(newTriggers.likelihood(1,2))/(newTriggers.likelihood(1,8)))) > 0.00000001 & newTriggers.boundingBox(1,3) > 10.01% & newTriggers.boundingBox(1,4) < 125.01
                        % introduce basic coherence checks
                        % --------------------------------
                        % do alphaNullIoverE>1.82
                        %likelihood1 = newTriggers.likelihood(1,8:9);
                        %ratioArray1=zeros(2,2);
                        %ratioArray1(1,2) = 1.82;
                        %coh_prf1 = xapplyalpharatiotest(likelihood1,ratioArray1);
                        % do circEoverI> 1.2
                        %likelihood2 = newTriggers.likelihood(1,10:11);
                        %ratioArray2=zeros(2,2);
                        %ratioArray2(1,2) = 1.27;
                        %coh_prf2 = xapplyratiotest(likelihood2,ratioArray2);
                        % done coherent tests, get prefactors now
                        %prf1 = 1.0 - sqrt(log(newTriggers.likelihood(1,2))/(newTriggers.nPixels(1)));
                        %prf2 = (newTriggers.boundingBox(1,3) + 0.0000001)/(log(newTriggers.likelihood(1,2)));
                        %prf3 = 1.0 - sqrt(log(newTriggers.likelihood(1,2))/(newTriggers.boundingBox(1,4) + 0.0000001));
                        prf1 = (newTriggers.boundingBox(1,3)*newTriggers.boundingBox(1,4))/(newTriggers.nPixels(1)*log(newTriggers.likelihood(1,2)));
                        prf4 = 1.0 - sqrt(log(newTriggers.likelihood(1,2))/(newTriggers.likelihood(1,8)));
                        %prf5 = ((newTriggers.boundingBox(1,3) + 0.0000001)*(newTriggers.boundingBox(1,4) + 0.0000001))*(newTriggers.nPixels(1));
                        % ---- three more prefactors
                        %prf6 = 1.0 - sqrt(log(newTriggers.likelihood(1,2))/(newTriggers.likelihood(1,4)));
                        %prf7 = 1.0 - sqrt(log(newTriggers.likelihood(1,2))/(newTriggers.likelihood(1,6)));
                        %prf8 = 1.0 - sqrt(log(newTriggers.likelihood(1,2))/(newTriggers.likelihood(1,10)));
                        prf9 = abs(newTriggers.likelihood(1,8) - newTriggers.likelihood(1,9))/newTriggers.nPixels(1);
                        %if newTriggers.boundingBox(1,4)/newTriggers.boundingBox(1,3)>15.0
                        %    asp = 0.0;
                        %else
                        %    asp = 1.0;
                        %end
                        prf = abs(prf1*prf1*prf4*prf9*prf9);
                    else
                        prf = 0.0;
                    end
                    % ---- boost/downgrade prefactors that are most likely to be associated with injections/background
                    % ---- boost a lot most likely injections
                    %if prf > 0.25
                    %    prf = 10.0*prf;
                    %end
                    % ---- mildly boost the ones in the 'grey area'
                    %if prf<0.25 & prf>0.05
                    %    prf = 10.0*prf;
                    %end
                    % ---- downgrade the most likely background
                    %if prf < 0.4
                    %    prf = prf^2.0;
                    %end
                    % disp(prf);
                    %if newTriggers.peakFrequency(1)>350.0 & newTriggers.peakFrequency(1)<450.0
                    % ---- do this next time
                    if newTriggers.peakFrequency(1)>340.0 & newTriggers.peakFrequency(1)<460.0 | newTriggers.peakFrequency(1)>670.0 & newTriggers.peakFrequency(1)<700.0 | newTriggers.peakFrequency(1)>790.0 & newTriggers.peakFrequency(1)<820.0 | newTriggers.peakFrequency(1)>655.0 & newTriggers.peakFrequency(1)<665.0
                        newTriggers.likelihood(1,:) = 0.0;
                        newTriggers.significance(1) = 0.0;
                    else
                        newTriggers.likelihood(1,:) = newTriggers.likelihood(1,:)*prf;
                        newTriggers.significance(1) = newTriggers.significance(1)*prf;
                    end
                end
                newTriggers.nPixels(1) = sum(newTriggers.nPixels);
                mint = min(newTriggers.boundingBox(:,1));
                maxt = max(newTriggers.boundingBox(:,1)+newTriggers.boundingBox(:,3));
                minf = min(newTriggers.boundingBox(:,2));
                maxf = max(newTriggers.boundingBox(:,2)+newTriggers.boundingBox(:,4));
                duration = maxt - mint;
                bw = maxf - minf;
                newTriggers.boundingBox(1,:) = [mint minf duration bw];
                newTriggers.peakTime(1,:) = mint + 0.5*duration;
                newTriggers.peakFrequency(1,:) = minf + 0.5*bw;

                % ---- Keep only first (merged) trigger.
                newTriggers = xclustersubset(newTriggers,1);
    
                if igenClust==1
                    clusterInj = newTriggers;
                else
                    clusterInj = xclustermerge(clusterInj,newTriggers);
                end
    
            end

        else 

            % ---- No merging needed for this fakeJobNumber. Use unedited triggers.
            % ---- but apply threshold on pixels ---- %
            clusterInj = xclustersubset(s.cluster,index);
            for r=1:length(clusterInj.nPixels)
                if clusterInj.nPixels(r)<30
                    clusterInj.nPixels(r) = 1;
                    clusterInj.likelihood(r,:) = 0.0;%clusterInj.likelihood(r,:).*(clusterInj.nPixels(r)/100.0);
                    clusterInj.significance(r) = 0.0;%clusterInj.significance(r)*(clusterInj.nPixels(r)/100.0);
                end
            end
            % ---- extra bit of messing with likelihoods ---- %
            % ---- downgrade in frequency band
            for a=1:length(clusterInj.peakFrequency)
                % ---- plop prf here again
                % ---- demostrated working fine clusterInj.likelihood(a,2) > 1.00001
                if clusterInj.likelihood(a,2) > 1.00001 & (1.0 - sqrt(log(clusterInj.likelihood(a,2))/(clusterInj.likelihood(a,8)))) > 0.00000001 & clusterInj.boundingBox(a,3) > 10.01% & clusterInj.boundingBox(a,4) < 125.01
                    % introduce basic coherence checks
                    % --------------------------------
                    % do alphaNullIoverE>1.82
                    %likelihood1 = clusterInj.likelihood(a,8:9);
                    %ratioArray1=zeros(2,2);
                    %ratioArray1(1,2) = 1.82;
                    %coh_prf1 = xapplyalpharatiotest(likelihood1,ratioArray1);
                    % do circEoverI> 1.2
                    %likelihood2 = clusterInj.likelihood(a,10:11);
                    %ratioArray2=zeros(2,2);
                    %ratioArray2(1,2) = 1.27;
                    %coh_prf2 = xapplyratiotest(likelihood2,ratioArray2);
                    % done coherent tests, get prefactors now
                    %prf1 = 1.0 - sqrt(log(clusterInj.likelihood(a,2))/(clusterInj.nPixels(a)));
                    %prf2 = (clusterInj.boundingBox(a,3) + 0.0000001)/(log(clusterInj.likelihood(a,2)));
                    %prf3 = 1.0 - sqrt(log(clusterInj.likelihood(a,2))/(clusterInj.boundingBox(a,4) + 0.0000001));
                    prf1 = (clusterInj.boundingBox(a,3)*clusterInj.boundingBox(a,4))/(clusterInj.nPixels(a)*log(clusterInj.likelihood(a,2)));
                    prf4 = 1.0 - sqrt(log(clusterInj.likelihood(a,2))/(clusterInj.likelihood(a,8)));
                    %prf5 = ((clusterInj.boundingBox(a,3) + 0.0000001)*(clusterInj.boundingBox(a,4) + 0.0000001))*(clusterInj.nPixels(a));
                    % ---- some more prefactors
                    %prf6 = 1.0 - sqrt(log(clusterInj.likelihood(a,2))/(clusterInj.likelihood(a,4)));
                    %prf7 = 1.0 - sqrt(log(clusterInj.likelihood(a,2))/(clusterInj.likelihood(a,6)));
                    %prf8 = 1.0 - sqrt(log(clusterInj.likelihood(a,2))/(clusterInj.likelihood(a,10)));
                    prf9 = abs(clusterInj.likelihood(a,8) - clusterInj.likelihood(a,9))/clusterInj.nPixels(a);
                    %if clusterInj.boundingBox(a,4)/clusterInj.boundingBox(a,3)>15.0
                    %    asp = 0.0;
                    %else
                    %    asp = 1.0;
                    %end
                    prf = abs(prf1*prf1*prf4*prf9*prf9);
                else
                    prf = 0.0;
                end
                % ---- boost/downgrade prefactors that are most likely to be associated with injections/background
                % ---- boost a lot most likely injections
                %if prf > 0.25
                %    prf = 10.0*prf;
                %end
                % ---- mildly boost the ones in the 'grey area'
                %if prf<0.25 & prf>0.05
                %    prf = 10.0*prf;
                %end
                % ---- downgrade the most likely background
                %if prf < 0.4
                %    prf = prf^2.0;
                %end
                % disp(prf);
                %if clusterInj.peakFrequency(a)>350.0 & clusterInj.peakFrequency(a)<450.0
                %if clusterInj.peakFrequency(a)>350.0 & clusterInj.peakFrequency(a)<450.0
                if clusterInj.peakFrequency(a)>340.0 & clusterInj.peakFrequency(a)<460.0 | clusterInj.peakFrequency(a)>670.0 & clusterInj.peakFrequency(a)<700.0 | clusterInj.peakFrequency(a)>790.0 & clusterInj.peakFrequency(a)<820.0 | clusterInj.peakFrequency(a)>655.0 & clusterInj.peakFrequency(a)<665.0
                    clusterInj.likelihood(a,:) = 0.0;
                    clusterInj.significance(a) = 0.0;
                else
                    clusterInj.likelihood(a,:) = clusterInj.likelihood(a,:)*prf;
                    clusterInj.significance(a) = clusterInj.significance(a)*prf;
                end
            end
 
        end
        % ---- Save merged triggers for this fakeJobNumber into output array.
        if m==1
            outCluster = clusterInj;
        else
            outCluster = xclustermerge(outCluster,clusterInj);
        end
    end % ---- for loop on fakeJobNumber (fjn)

    % ---- Re-order field names to match original file.
    outCluster = orderfields(outCluster,s.cluster);
    % ---- Now save clusters back to original file.
    s.cluster = outCluster;

    if nargin<3
        outFile = fileName;
    end
    save(outFile, '-struct', 's');
    
end

% ---- Done.
return
