function xjitterhrss(inFile,outFile,h_rssMin,h_rssMax)
% XJITTERHRSS - Randomise injection parameter hrss for selected waveforms.
%
% usage:
% 
% xjitterhrss(inFile,outFile,h_rssMin,h_rssMax)
%
%  inFile       String.  Name of input injection file to be modified.
%  outFile      String.  Name of output file with modified injections.
%  h_rssMin     String.  Minimum value of hrss range over which to randomise.
%  h_rss0Max    String.  Maximum value of hrss range over which to randomise.
%
% $Id$

% ---- Check input.
error(nargchk(4, 4, nargin, 'struct'))
if ~ischar(h_rssMin)
    error('Input h_rssMin must be a string.');	
else
    h_rssMin = str2num(h_rssMin);
end
if ~ischar(h_rssMax)
    error('Input h_rssMax must be a string.'); 
else
    h_rssMax = str2num(h_rssMax);
end
if ~ischar(inFile) | ~ischar(outFile)
    error('Inputs inFile, outFile must strings.');
end

if isempty(strfind(inFile,'hrssjitter'))
    disp(['Skipping hrss jittering, no "hrssjitter" string in injection ' ...
        'file name: ' inFile]);
    return
end

% ---- Read and parse input injection file.
injection = readinjectionfile(0,1e10,inFile);
[gps_s, gps_ns, phi, theta, psi, temp_name, temp_parameters] = ...
    parseinjectionparameters(injection);
nInjection = length(gps_s);
% ---- Parser code foolishly outputs "name" and "parameters" as cell arrays
%      of 1x1 cell arrays of strings; reformat to remove extra layer of
%      cell arrays.
name = cell(nInjection,1);
parameters = cell(nInjection,1);
for ii = 1:nInjection
    name{ii} = temp_name{ii}{1};
    parameters{ii} = temp_parameters{ii}{1};
end

% ---- Location of amplitude and central frequency parameters depends on
%      the waveform type.
paramArray = cell(nInjection,1);
h_rssIndex = zeros(nInjection,1);
for ii = 1:nInjection
    switch lower(name{ii})
        case {'ds','chirplet'}  
            % ---- Damped Sinusoid.  Parameters are
            %      [h_rss,tau,f0].
            h_rssIndex(ii) = 1;
            paramArray{ii} = tildedelimstr2numorcell(parameters{ii});
            h_rss(ii) = paramArray{ii}(h_rssIndex(ii));
        otherwise
            error(['Current version of the function can only handle ' ... 
                'DS waveform type.  See xmakewaveform.']); 
   end
end

% ---- Set the random number generator seed based on the name of the
%      input file
randn('state',sum(inFile))
rand('twister',sum(inFile))

% % ---- Jitter central frequency
% lograndh_rss = log10(h_rssMin) + rand(nInjection,1)*(log10(h_rssMax) - log10(h_rssMin));
% 
% % ---- Keep h_rss of first injection unchanged so that web page
% %      captions look sensible.
% randh_rss = 10.^lograndh_rss;
% randh_rss(1) = h_rss(1);

% ---- Jitter amplitude.
logamplfactor = log10(h_rssMin) + rand(nInjection,1)*(log10(h_rssMax) - log10(h_rssMin));
amplfactor = 10.^logamplfactor;
randh_rss = h_rss(:) .* amplfactor(:);
randh_rss(1) = h_rss(1);

% ---- Write down the jittered injection parameters
numData = cell(1,1);
nameData = cell(1,1);
paramData = cell(1,1);
numData{1} = [gps_s(:,1), gps_ns(:,1), phi, theta, psi];
nameData{1} = name;
paramData{1} = cell(nInjection,1);
for ii = 1:nInjection
  jitteredParam = paramArray{ii};
  jitteredParam(h_rssIndex(ii)) = randh_rss(ii);  %-- reset decay time
  jitteredParamStr = num2str(jitteredParam,'%.3e~');
  paramData{1}{ii} = jitteredParamStr(1:end-1);  %-- drop last '~'
end

%----- Write jittered injection log file.
fid = fopen(outFile,'w');
for ii = 1:nInjection
  %----- Write signal type and parameters to output file.
  fprintf(fid,'%d %d %e %e %e ',numData{1}(ii,:));
  fprintf(fid,'%s ',nameData{1}{ii});
  fprintf(fid,'%s ',paramData{1}{ii});
  fprintf(fid,'\n');
end
fclose(fid);

% ---- Done.
return

