#!/usr/bin/env python

"""
Launches grb.py for each GRB listed in grb-list in a separate directory

"""


# -------------------------------------------------------------------------
#      Setup.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, subprocess, getopt
import configparser

# ---- Function usage.
def usage():
    msg = """\
Usage: 
  xbatchgrb.py [options]
  -p, --params-file <file>    [REQUIRED] parameters (.ini) file.
  -g, --grb-list <file>       [REQUIRED] text file listing GRBs. This is modelled on the
                              O3 GRB table; see https://wiki.ligo.org/Bursts/O3GRBparams
                              It has the following fields (example value):
                                gracedb ID*     (E191188 or nan)
                                satellite*      (Fermi) 
                                instrument*     (GBM) 
                                designation     (GRB151009949) 
                                classification* (Long) 
                                UTC time*       (22:47:03.449) 
                                GPS time        (1128466040.45) 
                                T90 duration    (18.944)
                                T90 uncertainty (2.429)
                                RA (deg)        (222.0000) 
                                Dec (deg)       (63.7100) 
                                pos error (deg) (13.2800)
                                redshift*       (nan)
                              Fields marked with a (*) are not used in the analysis. 
                              The name should begin with 'GRB', 'E', or 'MOCK' for the
                              post-processing script xbatchgrbwebpage.py to find it.
  -i, --detector <ifo>        [REQUIRED] add detector to the network 
  -c, --catalogdir <path>     path to location of waveform catalogs
  -s, --network-selection     Select the appropriate network of ifos from the ifos 
                              specified by the the --detector option using data quality
                              criteria.
  -t, --grid-type <grid>      String. Determines what shape of sky position grid will be
                              generated.  Recognized values are:
                                'circular' (2-d grids of concentric circles)
                                'healpix' (2-d grids from the healpix algorithm)
			        'line' (1-d arc grid)
                                'file' (user-generated grid, given with the -e option)
                              Default 'circular'. 
  --priority <prio>           Integer specifying the priority of the job. Higher-priority
                              jobs are submitted to the cluster before lower-priority jobs
                              by the same user. Default value is 0.
  --big-mem <memory>          Integer specifying the minimal memory requirement for condor
                              jobs in MB.
  --fermiFlag                 Flag indicating GRBs were detected by Fermi-GBM. Causes the
                              --sky-pos-err and --injdistrib options for grb.py to be set
                              correctly for Fermi-GBM GRBs.
  --swiftFlag                 Flag indicating GRBs were detected by Swift-BAT. Causes the
                              --sky-pos-err and --injdistrib options for grb.py to be set
                              correctly for Swift GRBs.
  --ipn-dir <directory>       Absolute path to directory containing grid files for IPN 
                              GRBs. For each GRB <grbname> to be analysed, the directory 
                              must contain these two files:
                                <grbname>_grid_search.txt : see grb.py --sky-pos-err option
                                <grbname>_grid_sim.txt    : see grb.py --grid-sim-file option
                              Requires use of the -t 'file' option.
  --disable-fast-injections   Disable fast injections; see grb.py.
  --smart-cluster             Smart clustering; see grb.py.
  --long-inj                  Long straddling injections; see grb.py.
  --xtmva                     Multi-variate analysis; see grb.py.
  --off-source-inj            If set perform injections into the off-source instead of the
                              on-source region.
  --user                      User's Albert.Einstein username. If not specified then the 
                              whoami system comand will be used to guess it.
  -h, --help                  display this message and exit

If none of the --fermi, --swift, or --ipn-dir options are used the analysis will default 
to a simple single-point analysis (typically equivalent to using --swift).

e.g.,
xbatchgrb.py \\
--params-file /home/mwas/s6_cvs/inputs/grb_S6a_rerun.ini \\
--grb-list  /home/mwas/s6_cvs/grb_lists/grbListS6A.txt \\
--detector H1 \\
--detector L1 \\
--detector V1 \\
--network-selection
"""
    print(msg, file=sys.stderr)


# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
params_file = None
grb_list = None
detector = []
catalog_path=None
network_selection = False
grid_type = None
condorPriority = 0
minimalMem = False
smart_cluster = None
long_inj = None
x_tmva = None
disable_fast_inj = False
off_source_inj = False
user_Flag = 0
fermi_Flag = 0
swift_Flag = 0
ipn_dir = None
# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hp:g:i:c:st:"
# ---- Long form.
longop = [
   "help",
   "params-file=",
   "grb-list=",
   "detector=",
   "network-selection",
   "grid-type=",
   "priority=",
   "big-mem=",
   "catalogdir=",
   "smart-cluster",
   "long-inj",
   "disable-fast-injections",
   "xtmva",
   "off-source-inj",
   "user=",
   "fermiFlag",
   "swiftFlag",
   "ipn-dir="
   ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    print('Your options are messed up!')
    usage()
    sys.exit(1)

# ---- Parse command-line arguments.  Arguments are returned as strings, so 
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-p", "--params-file"):
        params_file = a      
    elif o in ("-g", "--grb-list"):
        grb_list = a
    elif o in ("-i", "--detector"):
        detector.append(a)
    elif o in ("-c", "--catalogdir"):
        catalog_path = a
    elif o in ("--priority"):
        condorPriority = a
    elif o in ("--big-mem"):
        minimalMem = str(a)
    elif o in ("-t","--grid-type"):
        grid_type = a
    elif o in ("-s", "--network-selection"):
        network_selection = True
    elif o in ("--smart-cluster"):
        smart_cluster = "--smart-cluster"
    elif o in ("--long-inj"):
        long_inj = "--long-inj"
    elif o in ("--disable-fast-injections"):
        disable_fast_inj = "--disable-fast-injections"
    elif o in ("--xtmva"):
        x_tmva = "--xtmva"
    elif o in ("--off-source-inj"):
        off_source_inj = True
    elif o in ("--user"):
        user_Flag = 1
        user_name = a
    elif o in ("--fermiFlag"):
        fermi_Flag = 1
    elif o in ("--swiftFlag"):
        swift_Flag = 1
    elif o in ("--ipn-dir"):
        ipn_dir = a
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not params_file:
    print("No parameter file specified.", file=sys.stderr)
    print("Use --params-file to specify it.", file=sys.stderr)
    sys.exit(1)
if not grb_list:
    print("No GRB list text file specified.", file=sys.stderr)
    print("Use --grb-list to specify it.", file=sys.stderr)
    sys.exit(1)
if not detector:
    print("No detectors specified.", file=sys.stderr)
    print("Use --detector to specify each detector in the network.", file=sys.stderr)
    sys.exit(1)


# ---- Status message.  Report all supplied arguments.
print(file=sys.stdout)
print("####################################################", file=sys.stdout)
print("#       X-Pipeline GRB Search : batch mode         #", file=sys.stdout)
print("####################################################", file=sys.stdout)
print(file=sys.stdout)
print("Parsed input arguments:", file=sys.stdout)
print(file=sys.stdout)
print("     parameters file:", params_file, file=sys.stdout)
print("       GRB list file:", grb_list, file=sys.stdout)
print("    detector network:", detector, file=sys.stdout)
if catalog_path:
    print("        catalog path:", catalog_path, file=sys.stdout)
if network_selection:
    print("   network selection: automatic", file=sys.stdout)
else:
    print("   network selection: manual", file=sys.stdout)
if condorPriority:
    print("            priority:", condorPriority, file=sys.stdout)
if grid_type:
    print("           grid type:", grid_type, file=sys.stdout)
if user_Flag:
    print("           user name:", user_name, file=sys.stdout)
if fermi_Flag:
    print("          GRB source: Fermi", file=sys.stdout)  
if ipn_dir:
    print("          GRB source: IPN", file=sys.stdout)  
    print("  IPN grid files dir:", ipn_dir, file=sys.stdout)
if swift_Flag:
    print("          GRB source: Swift", file=sys.stdout)  
if off_source_inj: 
    print("    injection region: off-source", file=sys.stdout)
else:
    print("    injection region: on-source", file=sys.stdout)
if minimalMem:
    print(" memory request [MB]:", minimalMem, file=sys.stdout)
if disable_fast_inj:
    print("     fast injections: disabled", file=sys.stdout)
if long_inj:
    print("     long injections: on", file=sys.stdout)
if smart_cluster:
    print("    smart clustering: on", file=sys.stdout)
if x_tmva:
    print("      XTMVA analysis: on", file=sys.stdout)

print(file=sys.stdout)

# ---- Check the params_file exists
if not os.path.isfile(params_file):
    print("Error: non existent parameter file: ", \
       params_file, file=sys.stderr)
    sys.exit(1)

# ---- Check the grb_list exists
if not os.path.isfile(grb_list):
    print("Error: non existent GRB list file: ", \
       grb_list, file=sys.stderr)
    sys.exit(1)

# -------------------------------------------------------------------------
#      Write GRB independent part of grb.py command
# -------------------------------------------------------------------------

detectorStr = ''
command_string_main = 'grb.py -p grb_offline.ini'
for ii in range(0,len(detector)) :
    command_string_main = command_string_main + ' -i ' + detector[ii]
    detectorStr = detectorStr + detector[ii]
if network_selection:
    command_string_main = command_string_main + ' -s '
if catalog_path:
    command_string_main = command_string_main + ' -c ' + catalog_path
if grid_type:
    command_string_main = command_string_main + ' -t ' + grid_type
if condorPriority:
    command_string_main = command_string_main +  ' --priority ' + condorPriority
if smart_cluster:
    command_string_main = command_string_main +  ' --smart-cluster '
if long_inj:
    command_string_main = command_string_main +  ' --long-inj '
if disable_fast_inj:
    command_string_main = command_string_main +  ' --disable-fast-injections '
if off_source_inj:
    command_string_main = command_string_main +  ' --off-source-inj '
if user_Flag:
    command_string_main = command_string_main +  ' --user ' + user_name
if x_tmva:
    command_string_main = command_string_main +  ' --xtmva '

# -------------------------------------------------------------------------
#      Prepare file holding command to run all dags.
# -------------------------------------------------------------------------

# ---- we name the dag_submit command file as well as individual dags
#      according to the detector network.
#      This ensures that when we launch multiple sets of dags from one
#      dir, each set corresponding to a different network combination, 
#      that the dag_submit command file and any rescue multi dag files
#      do not over write each other
cfile = open('dag_submit_command_' + detectorStr + '.txt','w')
cfile.write('condor_submit_dag -usedagdir ')

# -------------------------------------------------------------------------
#      Read GRB list file and run grb.py for each GRB.
# -------------------------------------------------------------------------

grbf = open(grb_list,'r')
for grbevent in grbf:

  # ---- Ignore any blank lines in the trigger file.
  if not str.isspace(grbevent):

    # ---- Parse GRB info.
    grbevent = grbevent.split()
    grbname = grbevent[3]
    trigger_time = int(float(grbevent[6]))
    ra = grbevent[9]
    decl = grbevent[10]

    # ---- Initialize other variables.
    skyposerr = None
    injdistrib = None
    endOffset = None

    if fermi_Flag==1:
        # ---- For Fermi we use
        #        error = (stat_sigma^2 + 0.9*3.7^2 + 0.1*14^2)^0.5
        skyposerr = str((float(grbevent[11])**2. + 0.9*(3.7**2.) + 0.1*(14.**2.))**0.5)
        injdistrib = str(grbevent[11]) + '~3.7~0.9~14.0'
    if swift_Flag==1:
        skyposerr = grbevent[11]
        injdistrib = grbevent[11]
    endOffset = str(float(grbevent[7]) + float(grbevent[8]))

    # ---- Status message.
    print("####################################################", file=sys.stdout)
    print("   Processing GRB " + grbname, file=sys.stdout)
    print("####################################################", file=sys.stdout)
    print(file=sys.stdout)

    # ---- Make directory for this GRB.  
    if os.path.isdir(grbname) :
        print('GRB directory' + grbname + ' already exists. Aborting', file=sys.stdout)
        break
    os.mkdir(grbname)

    # ---- Copy ini file to this directory.
    os.system('cp ' + params_file + ' ' + grbname + '/grb_offline.ini') 

    # ---- Copy IPN grid to this directory if IPN GRB.
    if ipn_dir:
        searchGridFile = grbname + '_grid_search.txt'
        simGridFile    = grbname + '_grid_sim.txt'
        if os.path.isfile(ipn_dir + '/' + searchGridFile) and os.path.isfile(ipn_dir + '/' + simGridFile):
            os.system('cp ' + ipn_dir + '/' + searchGridFile + ' ' + grbname + '/')
            os.system('cp ' + ipn_dir + '/' + simGridFile    + ' ' + grbname + '/')
        else:
            print('Cannot locate IPN sky grid files. Aborting!', file=sys.stdout)
            break

    # ---- Run grb.py to make DAG for this GRB.
    os.chdir(grbname)
    command_string = command_string_main + ' -g %d'%(trigger_time) + ' -r ' + ra + ' -d ' + decl + ' -n ' +  grbname
    if skyposerr:
        command_string = command_string + ' -e ' + skyposerr
    if injdistrib:
        command_string = command_string + ' --injdistrib ' + injdistrib
    if endOffset:
        command_string = command_string + ' --end-offset ' + endOffset
    if minimalMem:
        command_string = command_string + ' --big-mem ' + minimalMem
    if ipn_dir:
        command_string = command_string + ' -e ' + searchGridFile
        command_string = command_string + ' -f ' + simGridFile

    print(command_string, file=sys.stdout)
    exitcode = os.system(command_string)
    print("Exitcode: ", exitcode, file=sys.stdout)

    # ---- Write ASCII file holding name of GRB.
    namefile = open('grbname.txt','w')
    namefile.write(grbname + "\n")
    namefile.close()

    if exitcode==0:
        # ---- Add "alljobs" DAG to the list to be run.
        cfile.write(grbname + '/grb_alljobs.dag ')

    cat_command = 'cat grb_summary.txt >> ../grb_summary_all.txt'
    os.system(cat_command)

    # ---- Return to starting directory.
    os.chdir('..')

# ---- Status message and close-out.
cfile.write("\n")
cfile.close()
print(file=sys.stdout)
print(" ... finished.", file=sys.stdout)


