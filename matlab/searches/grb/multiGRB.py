#!/usr/bin/env python


"""
Launches grb.py for each GRB listed in grb-list in a seperate directory
$Id$
"""
# -------------------------------------------------------------------------
#      Setup.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, subprocess, getopt
import configparser

# ---- Function usage.
def usage():
    msg = """\
Usage: 
  grb.sh [options]
  -p, --params-file <file>    parameters (.ini) file [REQUIRED]
  -g, --grb-list <file>       text file listing GRB's [REQUIRED]
  -s, --script <file>         complete path to grb.py script [REQUIRED]
  -h, --help                  display this message and exit
"""
    print(msg, file=sys.stderr)


# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
params_file = None
grb_list = None
grbscript = None

# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hp:g:s:"
# ---- Long form.
longop = [
   "help",
   "params-file=",
   "grb-liste=",
   "script="
   ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- Parse command-line arguments.  Arguments are returned as strings, so 
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-p", "--params-file"):
        params_file = a      
    elif o in ("-g", "--grb-list"):
        grb_list = a
    elif o in ("-s", "--script"):
        grbscript = a       
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not params_file:
    print("No parameter file specified.", file=sys.stderr)
    print("Use --params-file to specify it.", file=sys.stderr)
    sys.exit(1)
if not grb_list:
    print("No GRB list text file specified.", file=sys.stderr)
    print("Use --grb-list to specify it.", file=sys.stderr)
    sys.exit(1)
if not grbscript:
    print("No GRB list text file specified.", file=sys.stderr)
    print("Use --grb-list to specify it.", file=sys.stderr)
    sys.exit(1)


# ---- Status message.  Report all supplied arguments.
print(file=sys.stdout)
print("####################################################", file=sys.stdout)
print("#              X-GRB Search Pipeline               #", file=sys.stdout)
print("####################################################", file=sys.stdout)
print(file=sys.stdout)
print("Parsed input arguments:", file=sys.stdout)
print(file=sys.stdout)
print("     parameters file:", params_file, file=sys.stdout)
print("       GRB list file:", grb_list, file=sys.stdout)
print("      GRB scipt file:", grbscript, file=sys.stdout)
print(file=sys.stdout)

# -------------------------------------------------------------------------
#      Preparatory.
# -------------------------------------------------------------------------

# ---- Record the current working directory in a string.
cwdstr = os.getcwd( )
# print >> sys.stdout, "   current directory:", cwdstr   
# print >> sys.stdout

# -------------------------------------------------------------------------
#      Read configuration file.
# -------------------------------------------------------------------------

# ---- Status message.
print("Parsing parameters (ini) file ...", file=sys.stdout)

# ---- Create configuration-file-parser object
cp = configparser.ConfigParser()
cp.read(params_file)

# ---- Get LSCsegFind server from parameters file.
datafind_server = cp.get('datafind','server')

blockTime = int(cp.get('parameters','blocktime'))
detector = cp.get('input','detectorList')
detector = detector.split(',')

grbf = open(grb_list,'r')
for grbevent in grbf:
    grbevent = grbevent.split()
    trigger_time = int(float(grbevent[3]))
    ra = grbevent[4]
    decl = grbevent[5]
    grbname = grbevent[1]
    start_time = trigger_time - blockTime/2
    end_time = trigger_time + blockTime/2
    detectorOn = ''
    print("####################################################", file=sys.stdout)
    print("   Processing GRB" + grbname, file=sys.stdout)
    print("####################################################", file=sys.stdout)



    for ifo in detector:
        # get data quality flags from parameter file
        dqflags = cp.get(ifo,'dqflags')

        print('calling: LSCsegFind ' +  ' --server=' + datafind_server + ' -i '+ ifo + ' -t ' + dqflags + ' -s %d'%(start_time) + ' -e %d'%(end_time) + ' -o segwizard> .segments_all.txt')
        os.system('LSCsegFind ' +  ' --server=' + datafind_server + ' -i '+ ifo + ' -t ' + dqflags + ' -s %d'%(start_time) + ' -e %d'%(end_time) + ' -o segwizard > .segments_all.txt')
        print('... finished LSCsegFind call.')

        f= open('.segments_all.txt','r')
        for line in f:
            linelist=line.split()
            if (start_time== int(linelist[1])) & (end_time == int(linelist[2])) :
                detectorOn = detectorOn + ' -i ' + ifo
    if os.path.isdir('GRB' + grbname) :
        print('GRB directory GRB' + grbname + ' already exists. Aborting', file=sys.stdout)
        break

    os.mkdir('GRB' + grbname)
    os.chdir('GRB' + grbname)
    os.system('cp ../' + params_file + ' grb.ini') 
    os.system(grbscript + ' -p grb.ini' + ' -g %d'%(trigger_time) + ' -r ' + ra + ' -d ' + decl + detectorOn)
    pfile = open('grb.param','w')
    pfile.write(' -p grb.ini' + ' -g %d'%(trigger_time) + ' -r ' + ra + ' -d ' + decl + detectorOn + "\n")
    pfile.close()
    namefile = open('grbname.txt','w')
    namefile.write('GRB' + grbname + "\n")
    namefile.close()
    os.chdir('..')





