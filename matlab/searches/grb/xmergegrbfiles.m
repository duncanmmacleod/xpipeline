function xmergegrbfiles(dirName,fileListString,forceFlag,cutFile,matFile,preCutFile,threshold,purgeFlag)
% XMERGEGRBFILES - Merge multiple X-Pipeline GRB job files into one file.
%
% XMERGEGRBFILES merges triggers from separate runs of XDETECTION, outputting a
% single file holding all the triggers. XMERGEGRBFILES will optionally apply
% a significance threshold, and pre-defined coherent cuts as specified in the
% files produced in a previous automatic pipeline tuning, and keep only triggers
% passing those cuts and threshold.
% 
% usage:
%
%   xmergegrbfiles(dirName,fileList,forceFlag,cutFile,matFile,preCutFile,threshold)
%
%   dirName    String. Directory containing all files to be merged.
%   fileList   String. List of results files to be merged. Can include path.
%   forceFlag  Optional scalar, default 0. If 0 then any missing trigger .mat
%              files will cause XMERGEGRBFILES to exit with an error.
%              If 1 then missing injection files are ignored (but not on-source
%              or off-source files). If 2 then all types of missing files are 
%              ignored ("force merge").
%   cutFile    Optional string specifying the name (with path) of the file
%              containing the second-stage tuning command-line parameters
%              (coherent consistency cut thesholds) for xmakegrbwebpage.
%              Note: Both this file and the corresponding '.digest' file
%              are used.  These files are produced automatically when the
%              post-processing dag is run. Use this option when you want to
%              apply pre-defined coherent cuts when merging. Use 'None' to
%              skip applying cuts.
%   matFile    Optional string. Name (with path) of the closed-box results
%              file from the original post-pocessing used to produce the
%              tuned cuts. Must be specified if 'cutFile' is used. Information
%              on the type of cuts to be applied is retrieved from this file.
%              Use 'None' to skip applying cuts.
%   preCutFile Optional string.  As for 'cutFile', except specifies
%              the file for the first-stage ("pre") tuning. Use 'None' to skip
%              applying cuts.
%   threshold  Optional scalar. If supplied, then events with
%              significance<threshold are deleted from merged files. Default 0. 
%   purgeFlag  Optional scalar, default 1. If 1 then redundant vetoed triggers
%              from off-source jobs will be deleted to save memory.  
%
% See also XMERGEGRBRESULTS.
%
% $Id: xmergegrbfiles.m 6518 2024-04-24 10:15:38Z patrick.sutton@LIGO.ORG $ 

format compact

% ---- Assign defaults for optional arguments, check to see if cuts are to
%      be applied.
if nargin < 7
    threshold = 0;
end
applyPreCuts = 0;
if nargin >= 6
    applyPreCuts = 1;
end
applyCuts = 0;
if nargin >= 5
    % ---- Files names of 'None' are placeholders to be ignored, so check for
    %      that before turning on the cuts.
    if ~(strcmpi(cutFile,'None') & strcmpi(preCutFile,'None')) & ~strcmpi(matFile,'None')
        applyCuts = 1;
    end
end
if nargin < 3
    forceFlag = 0;
end

% ---- Maximum number of loudest off-source events to record.
maxOffEvent = 1e4;       %-- max retained per input file
maxOffEventTotal = 30e6; %-- max retained in merged file. Enough for 15 days livetime with 5000 background trials, totalling 7.5GB storage.

% ---- Parse file list string into cell array of separate file names.
fileList = strread(fileListString,'%s');

% ---- Name of first file.
fileName = fileList{1};

% ---- Create file name for merged results, based on the input file names.
mergedFileName = '';
tmp = strread(fileName,'%s','delimiter','/');  %-- split off path
tmp1 = strread(tmp{end},'%s','delimiter','_');
for i = 1:length(tmp1)-1
    mergedFileName = [ mergedFileName tmp1{i} '_' ];  %-- root name
end
mergedASDName = [ mergedFileName 'ASD.mat' ];
mergedFileName = [ mergedFileName 'merged.mat' ];

% ---- Check if these results files are from off-source (background) data.
%      Done by seeing if string 'off' is part of file name.
offSourceMat = 0;
if (~isempty(strfind(mergedFileName,'off')))
    offSourceMat = 1;
end

% ---- Check if these results files are from on-source data.
%      Done by seeing if string 'on_source' is part of file name.
onSourceMat = 0;
if (~isempty(strfind(mergedFileName,'on_source')) | ...
    ~isempty(strfind(mergedFileName,'ul_source')))
    onSourceMat = 1;
end

% ---- Read tuning parameter files.
if applyCuts
    % ---- Read veto method.
    fid = fopen(cutFile);
    tmp = textscan(fid,'%s');
    fclose(fid)
    vetoMethod = tmp{1}{4};
    % ---- Read veto thresholds.
    tunedPassCuts=load([cutFile '.digest']);
    load(matFile,'analysis','plusVetoType','crossVetoType','nullVetoType',...
        'medianA','medianC','maxE')
    % ---- Write thresholds in array format expected by cut functions.
    [ratioArray deltaArray] = xsetratioarray(analysis,...
        plusVetoType,crossVetoType,nullVetoType,...
        tunedPassCuts(2),tunedPassCuts(3),tunedPassCuts(1));
    % ---- Check for "pre" veto cuts.
    if applyPreCuts
        % ---- Read "pre" veto thresholds.
        tunedPrePassCuts=load([preCutFile '.digest']);    
        % ---- Write thresholds in array format expected by cut functions.
        [fixedRatioArray fixedDeltaArray] = xsetratioarray(analysis,...
            plusVetoType,crossVetoType,nullVetoType,...
            tunedPrePassCuts(2),tunedPrePassCuts(3), ...
            tunedPrePassCuts(1));
    else
        % ---- Assign dummy (zero) pre cut thresholds.
        fixedRatioArray = zeros(size(ratioArray));
        fixedDeltaArray = zeros(size(deltaArray));
    end
end

if (offSourceMat || onSourceMat)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %      Processing for off-source results.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % % ---- Shut this code off. Associated with lines 142-153.
    % % ---- Count number of triggers in one of the on/off-source jobs.
    % x = load([dirName fileList{1}],'cluster'); %-- read first file
    % numTrigPerJob = size(x.cluster.significance,1);
    % clear x 

    if offSourceMat
        event = load('../input/event_off_source.txt');
        window = load('../input/window_off_source.txt');
        jobsPerWindow = size(event,1)/size(window,1);
        % % ---- Shut this code off. It can lead to >100GB memory allocations ...
        % % ---- Off-source merging often fails for the supernova and all-sky 
        % %      searches due to the number of triggers to be merged exceeding 
        % %      maxOffEventTotal. Compute required number of triggers to be 
        % %      merged and reset maxOffEvent, maxOffEventTotal if needed.
        % if forceFlag %-- forceFlag==1 for SN and all-sky searches
        %     % ---- Count number of off-source jobs.
        %     numOffJobs = size(event,1);
        %     % ---- Reset limits as needed.
        %     maxOffEvent = numTrigPerJob;
        %     maxOffEventTotal = max(maxOffEventTotal,numOffJobs*numTrigPerJob);
        % end
    elseif onSourceMat
        event = load('../input/event_on_source.txt');
        window = load('../input/window_on_source.txt');
        jobsPerWindow = size(event,1)/size(window,1);
    end
    if floor(jobsPerWindow) ~= jobsPerWindow
        error(['Something is wrong - jobsPerWindow is not an integer: ' num2str(jobsPerWindow)])
    end
    % ---- Check that all expected .mat files have been produced.
    %      if forceFlag == 2 then we treat that as a "force merge" even if files are missing.
    if forceFlag == 2 
        ;
    elseif size(event,1) ~= length(fileList)
        disp('Missing job numbers are:')
        presentJobNumbers = [];
        for iFile = 1:length(fileList)
            tmp = textscan(fileList{iFile},'%s','delimiter','_');
            presentJobNumbers = [presentJobNumbers str2num(tmp{1}{end-1})];
        end
        setdiff(0:(size(event,1)-1),presentJobNumbers)
        error(['Some on/off-source jobs failed silently. The number ' ...
             'of files is ' num2str(length(fileList)) '; it should be ' ...
             num2str(size(event,1))])
    end

    % ---- Target variable is 'cluster', a struct that contains an a priori
    %      unknown number of fields. In a given struct, each field contains
    %      a numeric array with the same number of rows.  For each file, we
    %      read this variable, then loop over the fields and append the
    %      contents to a running 'mergedCluster' struct.

    % ---- Loop over all files to be merged.
    for iFile = 1:length(fileList)

        % ---- Initialize 'cluster' variable to an empty array.  This
        %      avoids a compiler error that confuses later use of cluster
        %      with a function of the same name.  Declaring cluster
        %      explicitly as a variable before first use tells the compiler
        %      it is a variable.  
        cluster = [];

        % ---- Load next results file.  Note that this over-writes the
        %      cluster variable just declared.
        fileName = [dirName fileList{iFile}];
        disp(fileName);
        load(fileName)
        % % ---- Shut this code off. Associated with lines 142-153.
        % if size(cluster.significance,1) > numTrigPerJob
        %     disp(['WARNING: ' fileName ' contains more triggers than expected: ' num2str(size(cluster.significance,1))]);
        % end
        % ---- First file processed.  Initialize mergedCluster struct to 
        %      cluster from first file, then null out the data.  Then 
        %      mergedCluster will be an empty struct with the desired set
        %      of fields.
        if (1 == iFile)
            % ---- Allocate temporary oversized matrix of merged cluster
            %      fields.
            clusterFields = fieldnames(cluster);
            for ii = 1:length(clusterFields)
                fieldValue = getfield(cluster,clusterFields{ii});
                mergedClusterField{ii} = zeros(maxOffEventTotal,size(fieldValue,2));
            end
            firstEmptyCellIndex = 1;
            mergedCluster = cluster;

            % ---- Allocate an array to store the ASD estimated for each job

            % ---- Find the detector spectra with the best frequency resolution
            %      (the 'best Analysis Time index' [bATi])
            bATi = 1; dT = 0;
            for i = 1:length(analysisTimesCell)
                if (dT < analysisTimesCell{i})
                    dT = analysisTimesCell{i};
                    bATi = i;
                end
            end

       	    dF = 1/analysisTimesCell{bATi};

            numDet = length(detectorList);
            [rows, columns] = size(amplitudeSpectraCell{bATi});	    
            % ---- For GRB/FRB searches the number of off-source jobs is 
            %      typically a few hundred (ASD file size 10s of MB). For SN 
            %      and all-sky searches it can be much larger (file size GB), 
            %      potentially causing memory problems. So if there are more 
            %      than 1000 job files, only store ASDs for a randomly selected
            %      set of 1000.
            maxNumASD = 1000;  
            if length(fileList) < maxNumASD
                numASD = length(fileList);
                idxASD = [1:numASD];            
            else
                numASD = maxNumASD;
                idxASD = randperm(length(fileList));
                idxASD = sort(idxASD(1:maxNumASD));            
            end

            % ---- Initialize an array to hold the ASDs.
            S = zeros(numDet,rows,numASD);

        end

        % ---- Optionally store the ASD from this job in the array.
        % ---- Check that iFile is in the list to be saved. If so, colIdx holds
        %      the column to store it in.
        [bool,colIdx] = ismember(iFile,idxASD);
        if bool
            for iDet=1:numDet
                S(iDet,:,colIdx) = amplitudeSpectraCell{bATi}(:,iDet);
            end
        end

        % ---- Only proceed if the cluster is not empty.
        if length(cluster.significance) > 0 

            % ---- Remove clusters that do not pass fixed coherent cuts or
            %      significance threshold, but keep at least one cluster per job
            %      for bookkeeping purposes.
            if applyCuts | threshold
                % ---- Initialise pass flag.
                passFlag = ones(size(cluster.significance));
                % ---- Apply cuts, if requested.
                if applyCuts
                    [passRatio passFixedRatio] = ...
                        xapplycoherentcuts(cluster,analysis,vetoMethod,ratioArray,deltaArray,...
                            medianA,medianC,maxE,fixedRatioArray,fixedDeltaArray);
                    passFlag = passFlag & passRatio & passFixedRatio;
                end
                if threshold
                    passThreshold = cluster.significance > threshold ;
                    passFlag = passFlag & passThreshold;
                end
                passIdx = find(passFlag);
                failIdx = find(not(passFlag));
                %
                % ---- We want to keep at least one trigger from each job, even
                %      if they don't pass the cuts.  Get the list of the first
                %      trigger in each job - we'll keep those. 
                [~, firstTriggerIndex] = unique(cluster.fakeJobNumber);
                % ---- Set to 0 the likelihoods and significance for any kept cluster
                %      that has failed the coherent cuts or thresholding. A significance 
                %      of zero is more useful than NaN because zero is the 
                %      effective value used for events that fail a veto test 
                %      (significance*pass=0), whereas NaN*pass=NaN does not match 
                %      the normal value for an event that has failed a veto test.
                zeroIdx = intersect(firstTriggerIndex,failIdx);
                cluster.likelihood(zeroIdx,:) = 0;
                cluster.significance(zeroIdx) = 0;
                % ---- Keep the union of all triggers that pass the cuts and
                %      the first trigger for each job.
                keepIdx = union(passIdx,firstTriggerIndex);
                cluster = xclustersubset(cluster,keepIdx);
            end
        
            % ---- Sort current batch of clusters.  First, find ordering
            %      of clusters by significance in decending order (i.e.,
            %      loudest first).
            [junk Ic] = sort(cluster.significance(:,1),'descend');
            % ---- Keep at most the maxOffEvent loudest clusters.
            if (length(Ic)>maxOffEvent)
                Ic = Ic(1:maxOffEvent);
            end

            % ---- Loop over fields and keep selected events. Note that extracting
            %      data by field names is robust even if the field ordering in the 
            %      struct changes from one file to the next.
            for ii = 1:length(clusterFields)
                % ---- Get the data for this field.
                fieldValue = getfield(cluster,clusterFields{ii});
                mergedClusterField{ii}(firstEmptyCellIndex: ...
                    firstEmptyCellIndex+length(Ic)-1,:) = fieldValue(Ic,:);
            end
        
            % ---- Update index of first empty cell.
            firstEmptyCellIndex = firstEmptyCellIndex+length(Ic);
            if (firstEmptyCellIndex+maxOffEvent>maxOffEventTotal)
                error(['Run out of space to add new off/on source events; ' ...
                'change total limit (' num2str(maxOffEventTotal) ...
                ') or throttle the output trigger rate.'])
            end
            
        end %-- if length(cluster.significance) > 0

        % ---- Record skyPosition information.
        if onSourceMat
            if (1 == iFile)
                mergedSkyPositions = skyPositions;
                mergedSkyPositionsTimes = repmat(unique(cluster.centerTime),size(skyPositions,1),1);
            else
                mergedSkyPositions = [mergedSkyPositions; skyPositions];
                mergedSkyPositionsTimes = [mergedSkyPositionsTimes; ...
                    repmat(unique(cluster.centerTime),size(skyPositions,1),1)];
            end
        end 

    end  %-- loop over files

    % ---- Drop any empty rows from the end of our merged field arrays and
    %      write them into the merged cluster struct. 
    for ii = 1:length(clusterFields)
        mergedClusterField{ii} = mergedClusterField{ii}(1:firstEmptyCellIndex-1,:);
        mergedCluster = setfield(mergedCluster,clusterFields{ii},mergedClusterField{ii});
    end        

    % --- Hijack jobNumber with analysis window number (effective lag
    %     number). Reshuffle jobs for different circular slides of same
    %     data so that they don't fall into the same window
    nCircSlides = size(unique(mergedCluster.circTimeSlides,'rows'),1);
    nRealJobs = length(unique(mergedCluster.jobNumber));
    mergedCluster.realJobNumber = mergedCluster.jobNumber;
    mergedCluster.jobNumber = floor(mergedCluster.jobNumber/jobsPerWindow) ...
        + nRealJobs/jobsPerWindow*mod(mergedCluster.fakeJobNumber,nCircSlides);
    mergedCluster.centerTime = window(1+floor(mergedCluster.realJobNumber/jobsPerWindow),1);

    % ---- Copy final lists from temporary storage into desired variable names.
    cluster = mergedCluster;

    % ---- If we have applied coherent consistency tests or a significance
    %      threshold then one trigger is kept from each circcular time slide for
    %      each data block even if all triggers have failed the cuts. This is
    %      excessive: we need only one trigger for each jobNumber (defined
    %      above). Loop over jobNumber and keep only significance>0 triggers for
    %      jobs with surviving triggers or 1 failed trigger in the case that
    %      none survive. This is important in SN and allsky searches, where each
    %      jobNumber may include 10^5 or 10^6 blocks.
    if (applyCuts | threshold) & purgeFlag
        disp(['Purging trigger list of redundant triggers. This may take several minutes.'])
        % ---- Find unique jobNumber values.
        ujn = unique(cluster.jobNumber);
        % ---- Initialise list of triggers to keep.
        keepIdx = [];
        % ---- Loop over jobNumber values.
        for ii = 1:length(ujn)
            % ---- Find all surviving triggers for this job.
            nonzeroIdx = find(cluster.jobNumber==ujn(ii) & cluster.significance>0);
            if length(nonzeroIdx)
                % ---- Keep only surviving triggers.
                keepIdx = [keepIdx; nonzeroIdx];
            else
                % ---- No surviving triggers. Keep only first failed trigger.
                zeroIdx = find(cluster.jobNumber==ujn(ii));
                keepIdx = [keepIdx; zeroIdx(1)];
            end
        end
        % ---- Purge trigger list.
        cluster = xclustersubset(cluster,keepIdx);
    end
    
    % ---- Save merged triggers and associated information to a single
    %      output file.
    if (offSourceMat)
        % ---- Save merged file for background results.
        save(mergedFileName,'analysisTimesCell','blockTime','cluster', ...
            'detectorList','injectionScale','likelihoodType', ...
            'maximumFrequency','minimumFrequency','sampleFrequency', ...
            'skyPositions','transientTime','-v7.3');

	% ---- Save file containing ASD estimation for each offsource job
	save(mergedASDName,'S','detectorList','dF','minimumFrequency','sampleFrequency');

    end
    if (onSourceMat)
        % ---- Save merged on source results.
        skyPositions      = mergedSkyPositions;
        skyPositionsTimes = mergedSkyPositionsTimes;
        save(mergedFileName,'analysisTimesCell','blockTime','cluster', ...
            'detectorList','injectionScale','likelihoodType', ...
            'maximumFrequency','minimumFrequency','sampleFrequency', ...
            'skyPositions','skyPositionsTimes','transientTime', ...
            'amplitudeSpectraCell','gaussianityCell','svnversion_xdetection','-v7.3');

        % ---- Save file containing ASD estimation for each onsource job
        save(mergedASDName,'S','detectorList','dF','minimumFrequency','sampleFrequency');

        % ---- Save some information to text file for glitch studies.
        if exist('outputType') && strcmp(outputType,'glitch')
            fid = fopen([mergedFileName '.txt'],'w');
            fprintf(fid, '# peakTime peakFrequency timeOffset1 timeOffset2');
            outputFormatting = '%.6f\t%.5g\t%.6f\t%.6f';
            fprintf(fid, ' %s', likelihoodType{:});
            for iLike = 1:length(likelihoodType);
                outputFormatting = [outputFormatting '\t%.5g'];
            end
            fprintf(fid,'\n');
            outputFormatting = [outputFormatting '\n'];
            fprintf(fid, outputFormatting, [cluster.peakTime cluster.peakFrequency ...
            cluster.timeOffsets cluster.likelihood]');
            fclose(fid);
        end
    end

else 

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %      Processing for injection results.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- Target variable is 'clusterInj', a struct ARRAY.  Each element
    %      of the array contains the same fields, the number of which is
    %      not known a priori.  In a given element, each of the fields is a
    %      numeric array with the same number of rows.  Some of these
    %      elements may contain empty arrays.  The number of elements in
    %      the struct array is different for each file.
    %
    %      For each file, we wish to retain only the struct array elements
    %      specified by nonzero values of the variable
    %      injectionProcessedMask.  The output is a struct array in which
    %      each element is copied from the clusterInj for which the
    %      matching injectionProcessedMask is nonzero (this should be
    %      unique). For each file, we read both injectionProcessedMask and
    %      clusterInj, extract the desired elements of clusterInj, and
    %      write them to a running 'mergedCluster' struct.

    event = load('../input/event_inj_source.txt');
    window = load('../input/window_inj_source.txt');
    jobsPerWindow = size(event,1)/size(window,1);

    % ---- Check that all expected .mat files have been produced. Skip this
    %      check for the supernova and all-sky cases where we do not expect to
    %      have data over the full on-source window. 
    if not(forceFlag)
        tmp = textscan(dirName,'%s','delimiter','_');
        [o injectionNumberString] = system(['wc -l < ../input/injection_' tmp{1}{end-1}  '.txt']);
        nInjections = str2num(injectionNumberString);
        presentJobNumbers = [];
        for iFile = 1:length(fileList)
            tmp  = textscan(fileList{iFile},'%s','delimiter','_');
            tmp2 = textscan(tmp{1}{end},'%s','delimiter','.');
            presentJobNumbers = [presentJobNumbers jobstr2num(tmp2{1}{1})];
        end
        if length(presentJobNumbers) ~= nInjections
            disp('The following injections are missing:')
            setdiff(1:nInjections,presentJobNumbers)
            error(['Some of the injection jobs failed silently. The number of processed injections ' ...
                'is ' num2str(length(presentJobNumbers)) ' it should be ' injectionNumberString])
        end
    end

    % ---- Loop over all files to be merged.
    for iFile = 1:length(fileList)

        % ---- Load results file.
        fileName = [dirName fileList{iFile}];
        disp(fileName);
        load(fileName)
          
        % ---- Decode the fileName.
        underIdx = strfind(fileName,'_');
        % ---- Find segment number; this is normally 0 for zero-lag injections.
        segNum = str2num(fileName([underIdx(length(underIdx)-1)+1 : underIdx(length(underIdx))-1 ]));

        % ---- Force column vectors here, if this is not done then 
        %      mergedPeakTimesAllSegs will be incorrect.
        peakTime = peakTime(:);
        injectionProcessedMask = injectionProcessedMask(:);

        % ---- Keep all elements with flag injectionProcessedMask == 1.
        index = find(1==injectionProcessedMask);          
        mergedCluster(segNum+1,index,:) = clusterInj(index,:);
        mergedPeakTime(segNum+1,index)  = peakTime(index);
        mergedInjectionProcessedMask(segNum+1,index) = injectionProcessedMask(index);  

    end  %-- loop over files

    % ---- cntIdx denotes the array element of clusterInj we will write a given
    %      injection to.
    cntIdx = 1;

    % ---- Loop over segments.
    for segIdx = 1:size(mergedCluster,1)

        % ---- If this is the first segment to be processed then initialise merged arrays.
        if (1 == segIdx)
            mergedPeakTimeAllSegs = [];
            mergedInjectionProcessedMaskAllSegs = [];
            mergedClusterAllSegs = [];
 
            % ---- Create empty fields for mergedClusterAllSegs.
            mergedClusterFields = fieldnames(mergedCluster(segIdx,1,:));
            for ii = 1:length(mergedClusterFields)
                mergedClusterAllSegs = setfield(mergedClusterAllSegs,{1,1},mergedClusterFields{ii},[]);
            end
        end

        % ---- Loop over injections.
        for injIdx = 1:size(mergedCluster,2)     

            % ---- Only go through the following steps if the given injection 
            %      has actually been processed.
            if mergedInjectionProcessedMask(segIdx,injIdx)==1

                % ---- Update job number and center time from block-related
                %      values to window-related values. Reshuffle jobs for
                %      different circular slides of the same data so that they
                %      don't fall into the same window.
                nCircSlides = size(unique(mergedCluster(segIdx,injIdx).circTimeSlides,'rows'),1);
                nRealJobs = length(unique(mergedCluster(segIdx,injIdx).jobNumber));
                mergedCluster(segIdx,injIdx).realJobNumber = mergedCluster(segIdx,injIdx).jobNumber;
                mergedCluster(segIdx,injIdx).jobNumber = floor(mergedCluster(segIdx,injIdx).jobNumber/jobsPerWindow) ...
                    + nRealJobs/jobsPerWindow*mod(mergedCluster(segIdx,injIdx).fakeJobNumber,nCircSlides);
                mergedCluster(segIdx,injIdx).centerTime = window(1+floor(mergedCluster(segIdx,injIdx).realJobNumber/jobsPerWindow),1);

                % ---- Find clusters that do not pass coherent cuts and/or
                %      significance threshold (if applied) and delete them.
                if applyCuts | threshold
                    % ---- Initialise pass flag.
                    passFlag = ones(size(mergedCluster(segIdx,injIdx).significance));
                    % ---- Apply cuts, if requested.
                    if applyCuts
                        [passRatio passFixedRatio] = xapplycoherentcuts(mergedCluster(segIdx,injIdx),analysis, ...
                            vetoMethod,ratioArray,deltaArray,medianA,medianC,maxE,fixedRatioArray,fixedDeltaArray);
                        passFlag = passFlag & passRatio & passFixedRatio;
                    end
                    if threshold
                        passThreshold = mergedCluster(segIdx,injIdx).significance > threshold ;
                        passFlag = passFlag & passThreshold;
                    end
                    passIdx = find(passFlag);
                    mergedCluster(segIdx,injIdx) = xclustersubset(mergedCluster(segIdx,injIdx),passIdx);
                end

                % ---- Collect fields from each segment into AllSegs arrays.
                mergedClusterFields = fieldnames(mergedCluster(segIdx,injIdx,:));            
                for ii = 1:length(mergedClusterFields)
                    % ---- Get the data for this field.
                    mergedFieldValue = getfield(mergedCluster(segIdx,injIdx),mergedClusterFields{ii});
                    % ---- Append the clusters with injectionProcessedMask==1 to the
                    %      merged list and write results back to mergedCluster.
                    mergedClusterAllSegs = setfield(mergedClusterAllSegs,{cntIdx,1},mergedClusterFields{ii}, mergedFieldValue);
                end %-- loop over clusterInj fields

            end %-- if injection processed
            
            % ---- Now that we have finished with this injection, increment cntIdx.
            cntIdx = cntIdx + 1;

        end %-- loop over inj

       % ---- Merge segment-dependent (injection-independent) variables.
       %      Use ' to force column vectors.
       mergedPeakTimeAllSegs               = [mergedPeakTimeAllSegs; mergedPeakTime(segIdx,:)'];
       mergedInjectionProcessedMaskAllSegs = [mergedInjectionProcessedMaskAllSegs; mergedInjectionProcessedMask(segIdx,:)'];

    end %-- loop over segments

    % ---- Copy final lists from temporary storage into desired variable names.
    %      Again, force column vectors.
    clusterInj             = mergedClusterAllSegs;
    peakTime               = mergedPeakTimeAllSegs(:);
    injectionProcessedMask = mergedInjectionProcessedMaskAllSegs(:);

    % ---- Save merged file for injection results.
    save(mergedFileName,'analysisTimesCell','blockTime','clusterInj', ...
        'detectorList','injectionProcessedMask','injectionScale','likelihoodType', ...
        'maximumFrequency','minimumFrequency','peakTime','sampleFrequency', ...
        'skyPositions','transientTime','onSourceTimeOffset','-v7.3');

end

% ---- Done.
return

