function [UL90p,SNRlist,percentNans] = xmakegrbwebpage(grb_name, user_tag,...
    ~, vetoMethod, ...
    nullVetoType, vetoNullRangeInput, ...
    plusVetoType, vetoPlusRangeInput, ...
    crossVetoType, vetoCrossRangeInput, ...
    analysisType, closedBoxMethod, ~, ...
    H1vetoSegList, K1vetoSegList, L1vetoSegList, ...
    G1vetoSegList, V1vetoSegList, ...
    tuningNameStr, resultType, detectionStat, ...
    percentile_tuning, percentile_ulCalc, freqVetoList)
% XMAKEGRBWEBPAGE - Create a report web page for one GRB analysis.
%
% usage:
%
% [UL90p,SNRlist,percentNans] = xmakegrbwebpage(grb_name, user_tag, ...
%     autoFileName, vetoMethod, nullVetoType, vetoNullRangeInput, ...
%     plusVetoType, vetoPlusRangeInput, crossVetoType, vetoCrossRangeInput, ...
%     analysisType, closedBoxMethod, injectionType, H1vetoSegList, ...
%     K1vetoSegList, L1vetoSegList, G1vetoSegList, V1vetoSegList, ...
%     tuningNameStr, resultType, detectionStat)
%
% or 
%
% [UL90p,SNRlist,percentNans] = xmakegrbwebpage(file_name)
%
% grb_name            String. Name of trigger; e.g. 'GRB070201'.
%
% user_tag            String. Arbitrary tag with which to label analysis; e.g. 'test'. 
% 
% autoFileName        String. OBSOLETE - the value of this string is no longer used. This
%                     argument will be removed in a future version of this function.
%
% vetoMethod          String. Determines what type of coherent veto test we will perform.
%                     Must be of the form
%                     '(medianAlpha|alphaLin|medianLin|linear|median|alpha)
%                     (CutScalar|CutCirc|CutAmp|Cut)' or 'None'.
%
% nullVetoType        String. Determines which coherent veto test to perform on null 
%                     likelihoods. Must be either 'nullIoverE' or 'nullEoverI'. 
%
% vetoNullRangeInput  String. List of thresholds to be used in the nullVetoType test.
%
% plusVetoType        String. Determines which coherent veto test to perform on plus-type 
%                     likelihoods. Must be one of 'plusIoverE', 'plusEoverI', 'ampEoverI',
%                     'circEoverI', or 'scalarEoverI'.
%
% vetoPlusRangeInput  String. List of thresholds to be used in the plusVetoType test.
%
% crossVetoType       String. Determines which coherent veto test to perform on cross-type 
%                     likelihoods. Must be one of 'crossIoverE', 'crossEoverI',
%                     'ampnullIoverE', 'circnullIoverE', or 'scalarnullIoverE.'
%
% vetoCrossRangeInput String. List of thresholds to be used in the crossVetoType test.
%
% analysisType        String. Optional. Possible values are:
%        
%                     'closedbox': The on-source events are not used.  One of the
%                     background trials is selected as the "dummy" on-source trial as
%                     determined by the percentile_ulCalc argument; see below.
%
%                     'snrulclosedbox': Same as closedbox, however upper limits are not
%                     done on the injection scale but on the total SNR desposited in the
%                     network by the injections (see ulSNREsitmation for more details).
%                     This analysis type is EXPERIMENTAL.  If used, please note that plots
%                     displaying scatter plots with injections are wrong. In the table at
%                     the bottom values are all wrong. The only meaningful columns are the
%                     50%, and 90% UL on the injections scale, they contain the 50% and
%                     90% UL on the total SNR deposited into the network.
%
%                     'openbox': A standard open box statistical analysis is performed
%                     (i.e., the upper limits are computed using the loudest surviving
%                     on-source event).
%
%                     'ulbox': Same as 'openbox' but uses the "ul-source" triggers which
%                     are not decimated. This is done to ensure that the selected loudest
%                     event is really the loudest.
%
% closedBoxMethod     String. Used to determined how we choose which off-source job is
%                     used as the dummy on-source when doing a closed-box analysis:
%
%                       'median' - use the off-source job with the median loudest loudest event.
%                       'job_<number>' - use the off-source job with the specified job number. 
%
% injectionType       String. OBSOLETE - the value of this string is no longer used. This
%                     argument will be removed in a future version of this function.
%
% H1vetoSegList ... V1vetoSegList
%                     Strings. Contain paths to veto segment lists, these must be ordered 
%                     H1,K1,L1,G1,V1. We assume segment lists are in segwizard format
%                     (columns: segment number, start GPS, end GPS, duration) and contain
%                     lists of times KILLED by the appropriate veto flags. Use 'None' if
%                     you do not want to apply veto segments for a particular detector.
%
% tuningNameStr       String. We will tune our veto ratio cuts in order to minimize the 
%                     expected upper limit for these waveforms. Should be 'all' or a 
%                     tilde-delimited string of injectin set names such as
%                     'SGC150Q9~SGC235Q9~SGC500Q9'; see "--tuning-waveforms" option in 
%                     xgrbwebpage.py.
%
% resultType          String. Determines type of output will be produced:
%
%                     'vetoTestOnly' - xmakegrbwebpage will estimate upper limits for each
%                     input coherent veto threshold combination and write the results to
%                     .mat files. 
%                     
%                     'tunedUpperLimit' - this option requires that only a single coherent
%                     veto threshold combination is input (i.e., vetoNullRangeInput,
%                     vetoPlusRangeInput, and vetoCrossRangeInput must all be single
%                     values). In this case xmakegrbwebpage will generate a webpage
%                     showing the analysis results. 
%
%                     'tunedUpperLimitExtended' same as 'tunedUpperLimit' but reads
%                     off-source jobs for which the coherent cuts have already been
%                     partially applied. Read the median fitting parameters from file.
%
% detectionStat       String. Determines which statistic is used for the significance
%                     (event ranking). Use 'default' to obtain the usual significance
%                     based on the first listed statistic in the .ini file.
%
% percentile_tuning   String. Percentile level to use when selecting the off-source job to
%                     use as the dummy on-source when tuning the coherent veto thresholds.
%
% percentile_ulCalc   String. Percentile level to use when selecting the off-source job to
%                     use as the dummy on-source for upper limit estimation in a closed-box
%                     analysis.
%
% freqVetoList        [OPTIONAL] String. Contains the path to a file with vetoed frequency
%                     bands. The file should be ASCII with two columns: start frequency
%                     and stop frequency, both in Hz.
%
% UL90p               Vector. One element per injection set tested. Value is injectionScale   
%                     at which the detection efficiency for the corresponding injection
%                     set is 90%. If multiple coherent veto threshold combinations are
%                     tested, the return value of UL90p is for the last combination.
% SNRlist             MxN array where M=number of injection sets and N=number of
%                     detectors. Element (i,j) is the estimated SNR of an injectionScale=1
%                     injection from the injection set i made into detector j, neglecting
%                     antenna response factors. For diagnostic use.
% percentNans         Vector. Each element is the percent of tuning tests that give UL=NaN
%                     for the correspoinding injection set. For diagnostic use.
%
% For the alternate usage:
%
%
% file_name           String. Name of text file containing the above input
%                     arguments.
%
% For analysisType=='closedbox', the distribution of sifgnificances of the loudest
% surviving event from each off-source trial used for upper limits is computed. The
% off-source trial giving the loudest surviving event closest to the percentile_ulCalc
% percentile of this distribution is selected as the dummy on-source. 
%
% Comments will appear when the web page file name has a shtml extension and viewed
% through a web server like Apache; local opening of this web page won't display
% comments.html
%
% $Id$

% NOTE TO DEVELOPERS: The function XMAKEHTMLREPORT relies on variables
% computed in this function. The required variables are listed in the 
% "save outputMatFileName" command starting at line 2476. If you make any
% changes to XMAKEGRBWEBPAGE please ensure that they do not break
% XMAKEHTMLREPORT.  


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Formatting of stdout messages from this function.
format short
format compact

% -----------------------------------------------------------------------------
%    Check number of input arguments. Assign default values where needed.
% -----------------------------------------------------------------------------

if (nargin==1)
    disp(['Attempting to read arguments from the file ' grb_name '.']);
    fileID = fopen(grb_name,'r');
    fileContents = textscan(fileID,'%s');
    % ---- The deal() command below requires fileContents to have exactly 24
    %      elements (to match the number of outputs). Add extra empty elements
    %      with default value if necessary.
    if length(fileContents{1})<24
        fileContents{1}{24} = 'None'; %-- default value for freqVetoList
    end
    [grb_name, user_tag, ~, vetoMethod, nullVetoType, ... 
        vetoNullRangeInput, plusVetoType, vetoPlusRangeInput, ...
        crossVetoType, vetoCrossRangeInput, analysisType, closedBoxMethod, ... 
        ~, H1vetoSegList, K1vetoSegList, L1vetoSegList, ...
        G1vetoSegList, V1vetoSegList, tuningNameStr, resultType, ....
        detectionStat, percentile_tuning, percentile_ulCalc, freqVetoList] ...
        = deal(fileContents{1}{:});
    fclose(fileID);
elseif (nargin==23)
    disp('Reading arguments from the command line.');
    % ---- No frequency veto file provided; set to none.
    freqVetoList = 'None';
elseif (nargin==24)
    disp('Reading arguments from the command line.');
else
    error('Too many or too few input arguments')
end

% -----------------------------------------------------------------------------
%    Validate input arguments.
% -----------------------------------------------------------------------------
fprintf(1,'Checking input arguments are valid ...');

% ---- Check that analysis type is a recognized type.
if ~( strcmp(analysisType,'closedbox') || ...
      strcmp(analysisType,'snrulclosedbox') || ...
      strcmp(analysisType,'ulbox') || ...
      strcmp(analysisType,'openbox') )
    error('analysisType must be either closedbox, snrulclosedbox, ulbox or openbox')
end

% ---- Check that resultType is correctly defined.
if ~( strcmp(resultType,'vetoTestOnly') || ...
      strcmp(resultType,'tunedUpperLimit') || ...
      strcmp(resultType,'tunedUpperLimitExtended'))
    error('resultType must be either vetoTestOnly or tunedUpperLimit or tunedUpperLimitExtended')
end

% ---- Convert vetoXXXXRangeInput strings to vectors.
vetoNullRangeInput  = str2num(vetoNullRangeInput);
vetoPlusRangeInput  = str2num(vetoPlusRangeInput);
vetoCrossRangeInput = str2num(vetoCrossRangeInput);

% ---- Set vetosTuned flag based on value of resultType and do checks if needed.
if (strcmp(resultType,'vetoTestOnly'))
    % ---- Coherent veto tests have not yet been tuned.
    vetosTuned = 0;
else
    % ---- Coherent veto tests have been tuned.
    vetosTuned = 1;
    % ---- In this case we should have a single set of veto thresholds.
    if length(vetoNullRangeInput)==1 && length(vetoPlusRangeInput)==1 && length(vetoCrossRangeInput)==1
        bestULIdx = 1;
        vetoNullRangePlots  = vetoNullRangeInput;
        vetoPlusRangePlots  = vetoPlusRangeInput;
        vetoCrossRangePlots = vetoCrossRangeInput;
    else
        error(['For resultType == ' resultType ' we should have a single set of veto thresholds.'])
    end
end

% ---- Check that vetoMethod is correctly defined.
if isempty(regexp(vetoMethod,['(medianAlpha|alphaLin|medianLin|' ...
                      'linear|median|alpha)(CutScalar|CutCirc|CutAmp|Cut)'])) && ...
        not(strcmp(vetoMethod,'None'))
   error(['vetoMethod must be either (medianAlpha|alphaLin|medianLin|' ...
          'linear|median|alpha)(CutScalar|CutCirc|CutAmp|Cut) or None'])
end

% ---- Check that nullVetoType is nullEoverI or nullIoverE.
if and(~strcmp(nullVetoType,'nullEoverI'),~strcmp(nullVetoType,'nullIoverE'))
    error('Invalid string for nullVetoType, must be nullEoverI or nullIoverE');
end

% ---- Check that plusVetoType is plusEoverI or plusIoverE.
if isempty(regexp(plusVetoType,'(plus|circ|amp|scalar)EoverI')) && ~strcmp(plusVetoType,'plusIoverE')
    error('Invalid string for plusVetoType, must be plusEoverI or plusIoverE');
end

% ---- Check that crossVetoType is crossEoverI or crossIoverE.
if ~strcmp(crossVetoType,'crossEoverI') && ...
      isempty(strcmp(crossVetoType,'(cross|circnullreg|circnull|scalarnull|ampnull)IoverE'))
   error(['Invalid string for crossVetoType, must be crossEoverI, crossIoverE, ' ...
         'circnullIoverE or circnullregIoverE or scalarnullIoverE']);
end

% ---- Cast name of detection statistic to lowercase for robustness.
detectionStat = lower(detectionStat);

% ---- Pack veto files into a single convenient cell array.
vetoSegList = {H1vetoSegList K1vetoSegList L1vetoSegList G1vetoSegList ...
    V1vetoSegList};

fprintf(1,'done! \n')


% -----------------------------------------------------------------------------
%    Read the "grb.param", "*.ini", and "xgrbwebpage.param" files.
% -----------------------------------------------------------------------------

% KLUDGE: We should make the xgrbwebpage.param an input argument to this function. From
% the contents we can find the grb.param file (in the -d directory) and then the .ini file
% (in the -d dir or from absolute path).  These changes would make this function much more
% robust, particularly for interactive use.

% ---- Read the "grb.param", "*.ini", and "xgrbwebpage.param" files that record how this
%      analysis was set up. (These files are copied to the auto_web directory by
%      xgrbwebpage.py.) 
% ---- Read grb.param.
[~, grb_py_args] = parseparamfile('grb.param');
% ---- Read the .ini file.
idx = find(strcmp('-p',grb_py_args(:,1)) | strcmp('--params-file',grb_py_args(:,1)));
[inidata, ininames] = parseinifile(grb_py_args{idx,2});
% ---- Read grb.param.
[~, xgrbwebpage_py_args] = parseparamfile('xgrbwebpage.param');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 Read on- and off-source trigger files.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Determine the location of the trigger-generation directory.
idx = find(strcmp('-d',xgrbwebpage_py_args(:,1)) | strcmp('--grb-dir',xgrbwebpage_py_args(:,1)));
grb_dir = xgrbwebpage_py_args{idx,2};
% ---- On- and off-source results files.
offSourceMatFile = [grb_dir '/output/off_source_0_0_merged.mat'];
onSourceMatFile  = [grb_dir '/output/on_source_0_0_merged.mat'];
% ---- Change the on-source to undecimated on-source if using the ulbox analysis type.
if strcmp(analysisType,'ulbox')
    onSourceMatFile = regexprep(onSourceMatFile,'on_source','ul_source');
end
% ---- Retrieve injection sets. Note that matlab does not allow hyphen in struct field
%      names, while the .ini file might use hyphens in keys. Hence the extra copy below.
injectionSetKeys = fieldnames(inidata.waveforms);
nInjectionSets = length(injectionSetKeys);
for i=1:nInjectionSets
    % ---- Name of injection set, including any hyphens (!).
    injectionSetNames{i} = getfield(ininames.waveforms,injectionSetKeys{i});
    % ---- Injection log file.
    injectionFilesCell{i} = [grb_dir '/input/injection_' injectionSetNames{i} '.txt'];
end
% ---- Other needed info.
backgroundPeriod = str2num(inidata.background.backgroundperiod);
catalogDir = [grb_dir '/input/'];
% ---- KLUDGE: old variable name used in code and helper functions. Need to update code
%      reading veto test .mat files. 
setNameCell = injectionSetNames; 

% ---- Load analysis information from the on-source file into a convenient structure.
analysis = load(onSourceMatFile,'amplitudeSpectraCell','analysisTimesCell','blockTime', ...
    'detectorList','gaussianityCell','likelihoodType','maximumFrequency','minimumFrequency', ...
    'sampleFrequency','skyPositions','skyPositionsTimes','transientTime');
% ---- Augment analysis struct with aditional useful information.
analysis.analysisTimes        = cell2mat(analysis.analysisTimesCell); 
analysis.clusterType          = 'connected';
analysis.type                 = analysisType;
analysis.grb_name             = grb_name;
analysis.offSourceMatFile     = offSourceMatFile;
analysis.onSourceMatFile      = onSourceMatFile;
% ---- Information about the on-source window. 
%      We use a (typically asymmetric) on-source time "window" around the GRB trigger
%      time. We discard on-source events which do not overlap this window. To treat
%      off-source and injection events consistently we will also discard off-source and
%      injection events that do not overlap an identical window centred on a given block's
%      centerTime.
analysis.onSourceBeginOffset  = str2num(inidata.parameters.onsourcebeginoffset);
analysis.onSourceEndOffset    = str2num(inidata.parameters.onsourceendoffset);
% ---- Duration of the on-source window.
analysis.onSourceTimeLength   = analysis.onSourceEndOffset - analysis.onSourceBeginOffset;
analysis.jobsPerWindow        = ceil(analysis.onSourceTimeLength/(analysis.blockTime-2*analysis.transientTime));
% ---- Actual amount of data needed to be read to process the whole on-source window. 
analysis.onSourceWindowLength = 2*analysis.transientTime+analysis.jobsPerWindow*(analysis.blockTime-2*analysis.transientTime);

% ---- Redundant copies of on-source window properties, in format expected by later code. 
window.offset = -analysis.onSourceBeginOffset;
window.duration = analysis.onSourceTimeLength;

% ---- Load the on-source events.
fprintf(1,['Loading on-source events from ' onSourceMatFile  ' ...']);
onSource = getfield(load(onSourceMatFile,'cluster'),'cluster');
% ---- Initialise the pass flag. Each event will have a pass flag, pass = 1 (0) indicates
%      that the event has survived all (failed at least one) background rejection test.
onSource.pass = ones(size(onSource.significance));
fprintf(1,'done! \n')

% ---- Load the off-source events.
fprintf(1,['Loading off source events from ' offSourceMatFile  ' ...']);
offSource = getfield(load(offSourceMatFile,'cluster'),'cluster');
% ---- Initialise the pass flag. 
offSource.pass = ones(size(offSource.significance));
% ---- Info needed later for likelihood rate histograms.
uniqueOffJobNumber = unique(offSource.jobNumber);
nJobFilesProcessed = length(uniqueOffJobNumber);
% ---- Significance bins for histrograms.  Set here so that the range
%      covers up to the loudest background event before any cuts.
binArray = exp([ 1:0.01:log(max(offSource.significance)) inf]);
fprintf(1,'done! \n')

% ---- Update significance of events to use a new ranking statistic, if requested.
detectionIdx = []; %-- if non-empty later then injections will also be updated
if not(strcmp(detectionStat,'default'))
    detectionIdx = find(strcmp(detectionStat,lower(analysis.likelihoodType)),1);
    if isempty(detectionIdx)
        error(['Detection statistic ' detectionStat ' is not in the list of likelihood types.'])
    else
        disp(['Changing the significance to the detection statistic: ' detectionStat])
        onSource.significance  = onSource.likelihood(:,detectionIdx);
        offSource.significance = offSource.likelihood(:,detectionIdx);
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             Magic numbers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Setting magic numbers...');

% ---- When choosing dummy on-source events we choose the off-source job with
%      loudest event which is the Nth percentile loudest event of all
%      the off-source events, where N is defined below 
% ---- Convert provided strings to doubles.
percentile_tuning = str2num(percentile_tuning);
percentile_ulCalc = str2num(percentile_ulCalc);

% ---- If we are performing medianCut vetoes we find it is also useful
%      to apply some fixed linearCuts in order to throw away 
%      significant glitches lying close to the I=E line.
% ---- Load fixed ratio cuts from file if in pre pass mode.
% ---- Define fixed ratio cut values as trivial (no cut applied) regardless
%      of whether a fixed ratio cut is to be applied. We do this because
%      the variable has to be initialised for passing to xmakehtmlreport.m.   
tunedPrePassCuts = [0 0 0];
if not(isempty(regexp(vetoMethod,'median.+Cut.*')) && ...
       isempty(regexp(vetoMethod,'alpha.+Cut.*')))
    if vetosTuned==0
        % ---- Strip the extra job number in the user_tag string that is added for
        %      tuning jobs.
        tmp = textscan(user_tag,'%s','delimiter','_');
        tmp = tmp{1};
        shortUserTag = tmp{1};
        for iCell=2:(length(tmp)-1)
            shortUserTag = [shortUserTag '_' tmp{iCell}];
        end
        clear tmp
    else
        shortUserTag = user_tag;
    end
    tunedPrePassCuts = load(['xmakegrbwebpage_' grb_name  '_' ...
                           shortUserTag '_xmake_args_tuned_pre.txt.digest'])
else
    fixedRatioCut = 0;
    disp(['Setting fixedRatioCut to: ' num2str(fixedRatioCut)]);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Find time GRB and do some sanity checking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- GRB on source center time is set to the trigger time by xmerge. (We could also get
%      this from the grb.param file).
analysis.gpsCenterTime = unique(onSource.centerTime);

% ---- Check that there is only one on source center time
if (length(analysis.gpsCenterTime) ~= 1)
  analysis.gpsCenterTime
  error('There is not only one on source center time, aborting')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  Find svnversions of codes used
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Try and get svnversion of processing from on-source file.
try 
    load(onSourceMatFile,'svnversion_xdetection');
    analysis.svnversion_xdetection = svnversion_xdetection;
    clear svnversion_xdetection    
catch
    warning('svnversion of xdetection was not stored by xdetection: ');
    warning('setting analysis.svnversion_xdetection to Unknown');
    analysis.svnversion_xdetection = 'Unknown';
end

% ---- Try and get svnversion of postprocessing using 
%      report-svnversion.sh, this bash script is created
%      when running make install-grbwebpage and is copied to the INSTDIR
%      which should be in the users path
[status,result] = ...
    system('sh $XPIPE_INSTALL_BIN/report-svnversion.sh');
if status==0
    analysis.svnversion_grbwebpage = result;
else
    warning('report-svnversion.sh has failed');
    disp(status);
    disp(result);
    warning('setting analysis.svnversion_grbwebpage to Unknown');
    analysis.svnversion_grbwebpage = 'Unknown';
end

disp(['For processing we used svnversion      : ' analysis.svnversion_xdetection ]);
disp(['For post processing we used svnversion : ' analysis.svnversion_grbwebpage ]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Find time of off-source triggers after time-shifts have been undone
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(1,'Finding off-source trigger times after time-shifts have been removed ... '); 
% ---- Add unslidTime and unslidCenterTime fields to trigger structs.
%      The on source should not be slid but various functions require the 
%      unslidTime and unslidCenterTime fields so we set them here.
offSource = xunslidtime(offSource,analysis.blockTime,analysis.transientTime);
onSource  = xunslidtime(onSource,analysis.blockTime,analysis.transientTime);
fprintf(1,'done! \n') 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Apply asymmetric window to on and off source data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp(['Keeping only clusters that lay in window' ...
      ' beginning ' num2str(-analysis.onSourceBeginOffset) 's before center time of block' ...
      ' with duration ' num2str(window.duration) ]);

fprintf(1,'Checking on source times for vetoed clusters ...\n');
onSource.passWindow = xapplywindowcut(window,onSource,true);
onSource.pass = min(onSource.pass,onSource.passWindow);

fprintf(1,'Checking off source times for vetoed clusters ... \n');
offSource.passWindow = xapplywindowcut(window,offSource,true);
offSource.pass = min(offSource.pass,offSource.passWindow);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         Read in veto segment data from files to vetoSegs struct
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Assume segment lists correspond to times KILLED by cat >=2 veto
%      cuts.  vetoSegsLists must be in order H1, K1, L1, G1, V1.  In the 
%      future these will be provided by the .ini file.
vetoSegs = [];

% ---- list of ifos in the same order as the veto seg files are
%      given on the command line, eventually this will be given
%      directly from the ini file
ifos = {'H1','K1','L1','G1','V1'};

% ---- We discard veto segments which lay beyond the offSource region
%      of the particular GRB we are analysing.
%      Reducing the number of veto segments we use saves us considerable 
%      time.
% ---- Number of seconds before centerTime that offSource region begins,
%      should always be +ve
offSourceRegion.offset = 0.5 * backgroundPeriod;
% ---- Duration of window in seconds
offSourceRegion.duration = backgroundPeriod;

disp(' ');
disp('Reading in segments from veto segment files');

% ---- Loop over ifos we have analysed
for thisIfo = 1:length(analysis.detectorList)

    % ---- Find vetoSegList corresponding to thisIfo
    thisVetoSegList = vetoSegList{strcmp(ifos,analysis.detectorList(thisIfo))};  

    % ---- Read in veto seg files
    if ~strcmp(thisVetoSegList,'None') 
        % ---- Read in veto seg files

        % ---- Write output to stdout
        fprintf(1,'Reading segments from %s for %s ... ', thisVetoSegList, ...
            analysis.detectorList{thisIfo});
        tempVetoSegs = [];
        fsegin = fopen(thisVetoSegList,'r');
        tempVetoSegs = textscan(fsegin, '%f %f %f %f','CommentStyle','#');
        fclose(fsegin); 
        fprintf(1,'done! \n') 
    
        vetoSegsTemp(thisIfo).gpsStart = tempVetoSegs{2};
        vetoSegsTemp(thisIfo).duration = tempVetoSegs{4};

        disp(['Keeping only veto segments that lay in window' ...
            ' beginning ' num2str(offSourceRegion.offset) ...
            's before GRB time' ...
            ' with duration ' num2str(offSourceRegion.duration) ]);
        disp(['    GRB times : ' num2str(analysis.gpsCenterTime) ]);
        disp(['    offSourceRegion spans from ' ...
             num2str(analysis.gpsCenterTime - offSourceRegion.offset) ...
             ' to ' ...
             num2str(analysis.gpsCenterTime - offSourceRegion.offset ...
             + offSourceRegion.duration)]);
        % ---- Keep only segments which overlap with our offSourceRegion
        coincOut=Coincidence2(analysis.gpsCenterTime - offSourceRegion.offset,...
                              offSourceRegion.duration,...
                              vetoSegsTemp(thisIfo).gpsStart,...
                              vetoSegsTemp(thisIfo).duration);

        if ~isempty(coincOut)
            % ---- Sixth column of coincOut contains indices of tempVetoSegs
            %      overlapping with our offSource region
            fprintf(1,'Keeping %d of %d of veto segments for %s \n', ...
                length(coincOut(:,6)),...
                length(vetoSegsTemp(thisIfo).gpsStart),...
                analysis.detectorList{thisIfo} );

            vetoSegs(thisIfo).gpsStart = ...
                vetoSegsTemp(thisIfo).gpsStart(coincOut(:,6));
            vetoSegs(thisIfo).duration = ...
                vetoSegsTemp(thisIfo).duration(coincOut(:,6));

        % ---- If none of the veto segments overlap with our offSource region
        else
            disp(['No veto segs for ' analysis.detectorList{thisIfo} ...
                 ' overlap with our off source region ']); 
            vetoSegs(thisIfo).gpsStart = [];
            vetoSegs(thisIfo).duration = [];
        end

    % ---- If we did not supply veto segments for this ifo
    else
        fprintf(1,'No veto segs for %s\n', analysis.detectorList{thisIfo});
        vetoSegs(thisIfo).gpsStart = [];
        vetoSegs(thisIfo).duration = [];
    end  

end % -- End of for loop over ifos

% ---- Load file with frequency bands to veto.
%      KLUDGE: We re-use the time veto function xapplyvetosegments to apply the
%      frequency veto, so the notch bands are called with gpsStart and duration
%      instead of frequencyStart and bandwidth. Also, all notched bands are 
%      treated as vetos for the first detector and the other detectors are 
%      treated as having an empty veto list.
freqVetoBands = [];
if ~strcmp(freqVetoList,'None') 
    % ---- read file
    ffreqin = fopen(freqVetoList, 'r');
    tmpFreqVeto = textscan(ffreqin, '%f %f', 'CommentStyle', '#');
    fclose(ffreqin);
    freqVetoBands(1).gpsStart = tmpFreqVeto{1};
    freqVetoBands(1).duration = tmpFreqVeto{2} - tmpFreqVeto{1};  
else
    freqVetoBands(1).gpsStart = [];
    freqVetoBands(1).duration = [];  
end
for iIFO = 2:length(analysis.detectorList)
    freqVetoBands(iIFO).gpsStart = [];
    freqVetoBands(iIFO).duration = [];    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Check on source segments satisfy our DQ criteria.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Note that unlike the other pass flags, onJobsPass has one element 
%      per onSource job, not for each onSource trigger.
disp('Checking data quality of on source segments');
[onJobsPass, onJobsDeadtime] = ...
    xoffsourcedataqualitycheck(analysis,onSource,vetoSegs,'quiet');
if onJobsPass
    fprintf(1,'On source segment passes data quality checks.\n')
else
    error('On source segment fails data quality checks!');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Check off source segments satisfy our DQ criteria.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Note that unlike the other pass flags, offJobsPass has one element 
%      per offSource job, not for each offSource trigger.
disp('Checking data quality of off source segments');
[offJobsPass, offJobsDeadtime] = ...  % ,uniqueJobNumbers
    xoffsourcedataqualitycheck(analysis,offSource,vetoSegs,'quiet');
fprintf(1,'%d of the %d off source segments pass data quality checks \n',...
    sum(offJobsPass),length(offJobsPass));

% ---- Number of jobs passing our DQ criteria
nOffJobsPass = sum(offJobsPass);

% ---- Find jobNumbers of jobs that pass DQ criteria
uniqueOffJobNumberPass = uniqueOffJobNumber(logical(offJobsPass));

% ---- Find jobNumbers of jobs that fail DQ criteria
uniqueOffJobNumberFail = uniqueOffJobNumber(not(logical(offJobsPass)));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Apply veto seg lists to on- and off-source events
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Applying veto segments to on source clusters');
% ---- Note that xapplyvetosegments expects a column of trigger times for
%      each ifo analysed. Therefore we must multiply the column vector 
%      onSource.boundingBox(:,1) by a row vector of ones with length numIfos
onSource.passTimeVetoSegs = ...
    xapplyvetosegments(onSource,onSource.unslidTime,...
    vetoSegs,analysis,'onSource','quiet',zeros(1,length(analysis.detectorList)));
onSource.passFrequencyVetoSegs = ...
    xapplyvetosegments(onSource,...
                       onSource.peakFrequency*ones(1,length(analysis.detectorList)),...
                       freqVetoBands, analysis,'onSource','quiet');
onSource.passVetoSegs = min(onSource.passTimeVetoSegs,onSource.passFrequencyVetoSegs);
onSource.pass = min(onSource.pass,onSource.passVetoSegs);

disp('Applying veto segments to off source clusters');
offSource.passTimeVetoSegs = ...
    xapplyvetosegments(offSource,offSource.unslidTime,...
    vetoSegs,analysis,'offSource','quiet');
offSource.passFrequencyVetoSegs = ...
    xapplyvetosegments(offSource,...
                       offSource.peakFrequency*ones(1,length(analysis.detectorList)),...
                       freqVetoBands, analysis,'offSource','quiet');
offSource.passVetoSegs = min(offSource.passTimeVetoSegs,offSource.passFrequencyVetoSegs);
offSource.pass = min(offSource.pass,offSource.passVetoSegs);
offSourceBeforeDQ = offSource;
offSource = xclustersubset(offSource,find(offSource.passVetoSegs));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            Identify likelihoods used for coherent vetoes.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

analysis.likelihoodPairs = [];
pairIdx = 1;

% ---- First case, when there are None vetoes e.g. MVA or 1-IFO analysis
% ---- the case for 2- or 3-IFO GRBs analyzed with the MVA
% ---- this bit of code will allow for E/I scatter plots even when the vetoMethod is None
% ---- it is ugly, but it works
if strcmp(vetoMethod,'None')
  disp('Veto Method is None, we will NOT apply coherent cuts but we will build scatter plots using available E/I likelihoods, if any...');
  lNames = {'eplus','iplus','ecross','icross','enull','inull','icirc','ecirc','icircnull','ecircnull'};
  lIndex = [];
  lNameIndex = [];

  for i=1:length(lNames)
    [indexPos,indexRange] = xlikelihoodindex(lNames{i},analysis.likelihoodType,onSource.likelihood,vetoPlusRangeInput);
    if indexPos>0
      lIndex = [lIndex;indexPos];
      lNameIndex = [lNameIndex; i];
    end
  end

  if length(lIndex) < 2
    disp('No E-I likelihoods available...');
  end
  if length(lIndex) == 2
    analysis.likelihoodPairs(pairIdx).name   = lNames{lNameIndex(1)};
    analysis.likelihoodPairs(pairIdx).energy = lIndex(2);
    analysis.likelihoodPairs(pairIdx).inc    = lIndex(1);
  end
  if length(lIndex) == 4
    analysis.likelihoodPairs(pairIdx).name   = lNames{lNameIndex(1)};
    analysis.likelihoodPairs(pairIdx).energy = lIndex(2);
    analysis.likelihoodPairs(pairIdx).inc    = lIndex(1);
    pairIdx = pairIdx + 1;
    analysis.likelihoodPairs(pairIdx).name   = lNames{lNameIndex(3)};
    analysis.likelihoodPairs(pairIdx).energy = lIndex(4);
    analysis.likelihoodPairs(pairIdx).inc    = lIndex(3);
  end
  if length(lIndex) == 6
    analysis.likelihoodPairs(pairIdx).name   = lNames{lNameIndex(1)};
    analysis.likelihoodPairs(pairIdx).energy = lIndex(2);
    analysis.likelihoodPairs(pairIdx).inc    = lIndex(1);
    pairIdx = pairIdx + 2;
    analysis.likelihoodPairs(pairIdx).name   = lNames{lNameIndex(3)};
    analysis.likelihoodPairs(pairIdx).energy = lIndex(4);
    analysis.likelihoodPairs(pairIdx).inc    = lIndex(3);
    analysis.likelihoodPairs(pairIdx).name   = lNames{lNameIndex(5)};
    analysis.likelihoodPairs(pairIdx).energy = lIndex(6);
    analysis.likelihoodPairs(pairIdx).inc    = lIndex(5);
  end
end

% ---- Figure our which column of onSource.likelihood contains each
%      likelihoodType
if or(strcmp(plusVetoType,'plusEoverI'),strcmp(plusVetoType,'plusIoverE'))
  [analysis.ePlusIndex,vetoPlusRangeInput] = ...
      xlikelihoodindex('eplus',analysis.likelihoodType,...
                       onSource.likelihood,vetoPlusRangeInput);
  [analysis.iPlusIndex,vetoPlusRangeInput] = ...
      xlikelihoodindex('iplus',analysis.likelihoodType,...
                       onSource.likelihood,vetoPlusRangeInput);
elseif strcmp(plusVetoType,'circEoverI')
  [analysis.ePlusIndex,vetoPlusRangeInput] = ...
      xlikelihoodindex('ecirc',analysis.likelihoodType,...
                       onSource.likelihood,vetoPlusRangeInput);
  [analysis.iPlusIndex,vetoPlusRangeInput] = ...
      xlikelihoodindex('icirc',analysis.likelihoodType,...
                       onSource.likelihood,vetoPlusRangeInput);
elseif strcmp(plusVetoType,'scalarEoverI')
  [analysis.ePlusIndex,vetoPlusRangeInput] = ...
      xlikelihoodindex('escalar',analysis.likelihoodType,...
                       onSource.likelihood,vetoPlusRangeInput);
  [analysis.iPlusIndex,vetoPlusRangeInput] = ...
      xlikelihoodindex('iscalar',analysis.likelihoodType,...
                       onSource.likelihood,vetoPlusRangeInput);
elseif strcmp(plusVetoType,'ampEoverI')
  [analysis.ePlusIndex,vetoPlusRangeInput] = ...
      xlikelihoodindex('eamp',analysis.likelihoodType,...
                       onSource.likelihood,vetoPlusRangeInput);
  [analysis.iPlusIndex,vetoPlusRangeInput] = ...
      xlikelihoodindex('iamp',analysis.likelihoodType,...
                       onSource.likelihood,vetoPlusRangeInput);
end
if or(strcmp(crossVetoType,'crossEoverI'),strcmp(crossVetoType,'crossIoverE'))
  [analysis.eCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('ecross',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
  [analysis.iCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('icross',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
elseif strcmp(crossVetoType,'circnullIoverE')
  [analysis.eCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('ecircnull',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
  [analysis.iCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('icircnull',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
elseif strcmp(crossVetoType,'scalarnullIoverE')
  [analysis.eCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('escalarnull',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
  [analysis.iCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('iscalarnull',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
elseif strcmp(crossVetoType,'ampnullIoverE')
  [analysis.eCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('eampnull',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
  [analysis.iCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('iampnull',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
elseif strcmp(crossVetoType,'circnullregIoverE')
  % saving the supplied cross cut value in case the regularized
  % circnull/inc are not found so that the escape below works
  suppliedCrossRangeInput = vetoCrossRangeInput;
  [analysis.eCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('ecircnullreg',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
  [analysis.iCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('icircnullreg',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
  % -- if regulated circnull/inc not found try to use normal ones
  if not(and(analysis.eCrossIndex,analysis.iCrossIndex))
    [analysis.eCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('ecircnull',analysis.likelihoodType,...
                       onSource.likelihood,suppliedCrossRangeInput);
    [analysis.iCrossIndex,vetoCrossRangeInput] = ...
        xlikelihoodindex('icircnull',analysis.likelihoodType,...
                         onSource.likelihood,suppliedCrossRangeInput);
  end
end
[analysis.eNullIndex,vetoNullRangeInput] = ...
   xlikelihoodindex('enull',analysis.likelihoodType,...
   onSource.likelihood,vetoNullRangeInput);
[analysis.iNullIndex,vetoNullRangeInput] = ...
   xlikelihoodindex('inull',analysis.likelihoodType,...
   onSource.likelihood,vetoNullRangeInput);

% ---- Populate analysis.likelihoodPairs structure with the indices of
%      the E,I pairs we wish to make plots of later
% ---- Only add likelihoods to this structure if both E and I part exist

% ---- plus
if and(analysis.ePlusIndex,analysis.iPlusIndex)
  if not(isempty(regexp(plusVetoType,'(scalar|circ|amp)EoverI') ))
   analysis.likelihoodPairs(pairIdx).name   = 'plus';
  else
   analysis.likelihoodPairs(pairIdx).name   = 'plus';
  end
   analysis.likelihoodPairs(pairIdx).energy = analysis.ePlusIndex;
   analysis.likelihoodPairs(pairIdx).inc    = analysis.iPlusIndex;
   pairIdx = pairIdx + 1;
end

% ---- cross
if and(analysis.eCrossIndex,analysis.iCrossIndex)
   analysis.likelihoodPairs(pairIdx).name   = 'cross';
   analysis.likelihoodPairs(pairIdx).energy = analysis.eCrossIndex;
   analysis.likelihoodPairs(pairIdx).inc    = analysis.iCrossIndex;
   pairIdx = pairIdx + 1;
end

% ---- null
if and(analysis.eNullIndex,analysis.iNullIndex)
   analysis.likelihoodPairs(pairIdx).name   = 'null';
   analysis.likelihoodPairs(pairIdx).energy = analysis.eNullIndex;
   analysis.likelihoodPairs(pairIdx).inc    = analysis.iNullIndex;
end
pairIdx = [];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Write time and sky position of GRB.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- set rand seed used by randperm to matlab default
%      seed will be different for each GRB but will be the same for
%      each GRB when we run it again and again
rand('seed',931316785+analysis.gpsCenterTime);
analysis.initialSeed = rand('seed');
disp(' ');
disp(['initial random seed: ' num2str(analysis.initialSeed)]);

% ---- Call helper function which provides sensitivity information.
[analysis.FpList,analysis.FcList,analysis.dF,S,Snull] = xwritenetworkinfo(analysis);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Preparation for veto cuts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Array of ratio thresholds.
numberOfLikelihoods = size(onSource.likelihood,2);
ratioArray = zeros(numberOfLikelihoods,numberOfLikelihoods);
% ---- Default should be not to use deltas.
deltaArray = zeros(numberOfLikelihoods,numberOfLikelihoods);

% ---- Get all distinct combinations of veto thresholds, pack into vectors.
[X,Y,Z] = meshgrid(vetoNullRangeInput,vetoPlusRangeInput,vetoCrossRangeInput);
vetoNullRange  = X(:);
vetoPlusRange  = Y(:);
vetoCrossRange = Z(:);

% ---- We should only ever run open box analysis with a single tuned
%      set of vetoes.
if (strcmp(analysis.type,'openbox') | strcmp(analysis.type,'ulbox'))& length(vetoNullRange)>1
    error('We should only run openbox analysis with a single tuned set of vetoes')
end

% ---- If we are only analysing a single ifo we should not have consistency cuts,
%      all veto RangeInputs should be zero
if length(analysis.detectorList) == 1
    if vetoNullRange | vetoPlusRange | vetoCrossRange
        error(['When running with a single ifo we cannot perform ' ...
               'consistency cuts. vetoNullRangeInput, vetoPlusRangeInput '...
               'and vetoCrossRangeInput should each be zero'])
    end
end

% ---- If we running with no consistency cuts we can usefully reset 
%      vetoMethod to None, unless compound veto method are used for which
%      the fixed cut can be non null
if vetoNullRange==0 & vetoPlusRange==0 & vetoCrossRange==0 & ...
      isempty(regexp(vetoMethod,'median.+Cut.*')) & ...
      isempty(regexp(vetoMethod,'alpha.+Cut.*'))
    vetoMethod = 'None';
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Select off source jobs to be used for veto tuning and those
%                   to be used for upper limit calc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dummyOnSourceJobNumber = [];

% ---- Same selection should be made in cases of open or closed boxes.
if strcmp(closedBoxMethod,'median')

    % ---- If we are using the median method to choose which offSource job
    %      to use as our fake onSource we want to use:
    %      * half of the offSource jobs for veto tuning and
    %      * the other half of the offSource jobs for UL calc.  

    % ---- Get random order of indices between 1 and 
    %      nOffJobsPass.
    rand('seed',analysis.initialSeed + 11);
    offJobSeed = rand('seed');
    randOffIdx = randperm(nOffJobsPass);
    nOffJobsPassBy2 = ceil(nOffJobsPass/2);

    % ---- Use the first half of randomly ordered injections for tuning.
    offIdx_tuning  = randOffIdx(1:nOffJobsPassBy2);
    nOffJob_tuning = length(offIdx_tuning);   
    offJob_tuning  = uniqueOffJobNumberPass(offIdx_tuning);    
    offJob_tuning  = sort(offJob_tuning);   
 
    % ---- Use the second half of randomly ordered injections for ul calc.
    offIdx_ulCalc  = randOffIdx(nOffJobsPassBy2+1:nOffJobsPass);
    nOffJob_ulCalc = length(offIdx_ulCalc);   
    offJob_ulCalc  = uniqueOffJobNumberPass(offIdx_ulCalc); 
    offJob_ulCalc  = sort(offJob_ulCalc); 

elseif strcmp(closedBoxMethod(1:4),'job_')

    % ---- If we want to use a specific offSource job as our fake onSource
    %      we want to 
    %      * use the median loudest of all of the other jobs for veto tuning 
    %        and
    %      * our chosen offSource job for UL calc.

    % ---- Get chosen offSource jobNumber from closedBoxMethod.
    dummyOnSourceJobNumber = str2num(closedBoxMethod(5:end));
    if isempty(find(dummyOnSourceJobNumber == uniqueOffJobNumberPass))
        error(['Chosen dummy on source job (number ' ...
            num2str(dummyOnSourceJobNumber) ') fails DQ cuts' ]);
    end
    offIdx_ulCalc  = find(dummyOnSourceJobNumber == uniqueOffJobNumberPass);
    nOffJob_ulCalc = 1; 
    offJob_ulCalc  = dummyOnSourceJobNumber;

    % ---- Get random order of indices between 1 and nJobFilesProcessed-1.
    rand('seed',analysis.initialSeed + 11);
    offJobSeed = rand('seed');
    randOffIdx = randperm(nOffJobsPass-1);
    nOffJobsPassBy2 = ceil(nOffJobsPass/2);

    % ---- Remove offSource job we will use for ul calc. 
    removeIdx = find(uniqueOffJobNumberPass == offJob_ulCalc);
    uniqueOffJobNumberPass(removeIdx) = [];

    % ---- Use the first half of randomly ordered injections for tuning.
    offIdx_tuning  = randOffIdx(1:nOffJobsPassBy2);
    nOffJob_tuning = length(offIdx_tuning);   
    offJob_tuning  = uniqueOffJobNumberPass(offIdx_tuning);
    offJob_tuning  = sort(offJob_tuning);   

else

    error('Bad value for analysisType or closedBoxMethod')

end

% ---- Initialise storage for upper limits.
ulVector90 = [];
ulVector50 = [];

% ---- Output to screen/log.
if (vetosTuned == 0)
   fprintf(1,'\n%s\n','Tuning our vetoes');
else
   fprintf(1,'\n%s\n','Calculating upper limits using tuned vetoes');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Select offSource for either tuning or ULCalc 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (vetosTuned == 0)
   % ---- Find the set of job numbers analysed.
   jobNumbers = offJob_tuning;
elseif (vetosTuned == 1)
   % ---- Find the set of job numbers analysed.
   jobNumbers = offJob_ulCalc;
else
   error('Bad value for vetosTuned')   
end 

% ---- Pull from the offSource structure every trigger whose
%      job number is listed in the jobNumbers vector.
% ---- First we identify which elements of offSource we want
%      in clusterIndex. 
% ---- Reuse function of coincidence between segments and events.
try
    coincIndex = fastcoincidence2([jobNumbers 0.1*ones(size(jobNumbers))],...
                     [offSource.jobNumber 0.1*ones(size(offSource.jobNumber))]);
catch
    coincIndex = Coincidence2(jobNumbers, 0.1*ones(size(jobNumbers)),...
                     offSource.jobNumber, 0.1*ones(size(offSource.jobNumber)));
    coincIndex = coincIndex(:,5:6);
end
clusterIndex = coincIndex(:,2);

% ---- Use xclustersubset to pull out the jobs whose indices are
%      listed in clusterIndex and write these to a new structure
%      offSourceSelected.
offSourceSelected = [];
offSourceSelected = xclustersubset(offSource,clusterIndex); 
% only median method might still need the full off-source clusters
if not(strfind(vetoMethod,'median') )
    clear offSource
end

% ---- Pull from the offSource structure every trigger whose
%      job number is listed in the jobNumbers vector.
% ---- First we identify which elements of offSource we want
%      in clusterIndex. 
% ---- Reuse function of coincidence between segments and events
try
    coincIndex = fastcoincidence2([jobNumbers 0.1*ones(size(jobNumbers))],...
                     [offSourceBeforeDQ.jobNumber 0.1*ones(size(offSourceBeforeDQ.jobNumber))]);
catch
    coincIndex = Coincidence2(jobNumbers, 0.1*ones(size(jobNumbers)),...
                     offSourceBeforeDQ.jobNumber, 0.1*ones(size(offSourceBeforeDQ.jobNumber)));
    coincIndex = coincIndex(:,5:6);
end
clusterIndex = coincIndex(:,2);

% ---- Use xclustersubset to pull out the jobs whose indices are
%      listed in clusterIndex and write these to a new structure
%      offSourceSelected.
offSourceSelectedBeforeDQ = [];
offSourceSelectedBeforeDQ = xclustersubset(offSourceBeforeDQ,clusterIndex); 
clear offSourceBeforeDQ


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Calculate median of offSource events
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Here we will measure the median of the offSource triggers in the
%      I and E likelihood planes. We use the median when applying our 
%      glitch rejection cuts and only calculate it when we are
%      applying the medianCut and when we have at least a single pair
%      of likelihoods.

% ---- Initialise structs, these will be used for plotting the
%      fit to the median of the offSource on the E-I plane
medianE = [];
medianI = [];  
medianA = [];
medianC = [];
maxE = [];

if strfind(vetoMethod,'median') 
    % ---- We should always calc median using offSource tuning 
    %      (rather than ulCalc) jobs.
    if (vetosTuned == 0)
        % ---- If we are doing a tuning run, we use the off source 
        %      jobs we have just selected
        offSourceForMedian = offSourceSelected;
        jobNumbersForMedian = offJob_tuning;
    elseif (vetosTuned == 1)
        % ---- If we are doing a ulCalc job, we must create the 
        %      offSourceForMedian structure
        jobNumbersForMedian = offJob_tuning;
        clusterIndex = [];
        for jobIdx = 1:length(unique(jobNumbersForMedian))
            clusterIndex = [clusterIndex; ...
                find(offSource.jobNumber == jobNumbersForMedian(jobIdx))];
        end
        offSourceForMedian = [];
        offSourceForMedian = xclustersubset(offSource,clusterIndex); 
    else
        error('Bad value for vetosTuned')   
    end 

    % clear up memory 
    clear offSource

    % ---- Estimate median of I values vs. E for this offSource data.
    % ---- Range of E values over which to do fit to data.
    minE = 5;
    if strcmp(resultType,'tunedUpperLimitExtended')
      % -- Read the median parameters from file
      inputMatFileName = [analysis.grb_name '_' user_tag '_' ...
                      analysis.type '.mat'];
      disp(['Reading median fit parameters from ' inputMatFileName])
      load(inputMatFileName,'medianA','medianC','maxE')
    else
      % -- or compute them using offSourceData
    maxE = 100;
    [medianA,medianC] = ...
        xgetrunningmedian(analysis,offSourceForMedian,minE,maxE);
    end

    % ---- Create structures medianE and medianI which contain vectors
    %      of points which track the median distribution of the offSource
    %      for each of the likelihood pairs investigated. These vectors
    %      will be used for plotting. 
    medE = logspace(0,5);
    for pairIdx=1:length(analysis.likelihoodPairs)
        % ---- Status report, get name of likelihood (e.g., 'null') from
        %      name field of analysis.likelihoodPairs
        fprintf(1,['Calculating fit to median distribution of '... 
            analysis.likelihoodPairs(pairIdx).name ' likelihoods... ']);

        % ---- Set temporary variables, e.g., medA = medianA.null
        medA = getfield(medianA,analysis.likelihoodPairs(pairIdx).name);
        medC = getfield(medianC,analysis.likelihoodPairs(pairIdx).name);

        % ---- Calculate medI corresponding to each medE 
        [nDummy,medI] = xdeviation(medE,medE,medA,medC,2*maxE);

        % ---- Set appropriate field of medianE and medianI structures,
        %      equivalent to medianE.null = medE etc.
        medianE = setfield(medianE,analysis.likelihoodPairs(pairIdx).name,medE);
        medianI = setfield(medianI,analysis.likelihoodPairs(pairIdx).name,medI);

        % ---- Status report.
        fprintf(1,'done! \n');
    end
end % -- if vetoMethod = medianCut


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Begin loop over different veto cuts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- length(vetoNullRange) is the total number of veto cuts we will
%      perform
for thisVeto = 1:length(vetoNullRange)

    % ---- Some output to screen 
    fprintf(1,'\nApplying veto combination %d of %d at %s\n', ...
        thisVeto,length(vetoNullRange),datestr(now));

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                      Set up ratioArray 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % ---- ratioArray is a square array with the same number of columns 
    %      as the likelihood variable. Each nonzero element is a threshold
    %      to be applied to a pair of likehoods;

    [ratioArray deltaArray]=...
        xsetratioarray(analysis,...
                       plusVetoType,crossVetoType,nullVetoType,...
                       vetoPlusRange(thisVeto),vetoCrossRange(thisVeto),vetoNullRange(thisVeto));

                   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                      Set up fixedRatioArray.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % ---- If we are performing medianCut vetoes we find it is also useful
    %      to apply some fixed linearCuts in order to throw away 
    %      significant glitches lying close to the I=E line.
    % ---- fixedRatioCut set in "Magic numbers" section above.
    if not(isempty(regexp(vetoMethod,'medianCut')))
        fixedRatioArray = fixedRatioCut * sign(ratioArray); 
        fixedDeltaArray = zeros(size(ratioArray));
        disp(['fixedRatioCut = ' num2str(fixedRatioCut) ]);
    elseif not(isempty(regexp(vetoMethod,'median.+Cut.*')) && ...
               isempty(regexp(vetoMethod,'alpha.+Cut.*')))
      % Read fixed fixed ratio cut from tuned file
      disp('Using the following fixed/alpha ratio cuts:')
      [fixedRatioArray fixedDeltaArray]=...
          xsetratioarray(analysis,...
                         plusVetoType,crossVetoType,nullVetoType,...
                         tunedPrePassCuts(2),tunedPrePassCuts(3),tunedPrePassCuts(1));
    else
        fixedRatioArray = zeros(size(ratioArray));
        fixedDeltaArray = zeros(size(ratioArray));
    end

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %            Apply coherent cuts to on and off-source events.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if not(isempty(regexp(vetoMethod,'None')))
        warning('Veto method is None, skipping coherent veto cuts')
    else
        [onSource.passRatio onSource.passFixedRatio] = ...
            xapplycoherentcuts(onSource,analysis,vetoMethod,ratioArray,deltaArray,...
                               medianA,medianC,maxE,fixedRatioArray,fixedDeltaArray);
        onSource.pass = min(onSource.pass,onSource.passRatio);
        onSource.pass = min(onSource.pass,onSource.passFixedRatio);

        [offSourceSelected.passRatio offSourceSelected.passFixedRatio] = ...
            xapplycoherentcuts(offSourceSelected,analysis,vetoMethod,ratioArray,deltaArray,...
                           medianA,medianC,maxE,fixedRatioArray,fixedDeltaArray);
        offSourceSelected.pass = min(offSourceSelected.pass, ...
            offSourceSelected.passRatio);
        offSourceSelected.pass = min(offSourceSelected.pass, ...
            offSourceSelected.passFixedRatio);
    end

    if sum(offSourceSelected.pass) == 0
        warning('We have vetoed all of the offSourceSelected events.')
    end

    % ---- Apply superclustering to combine nearby-in-time-frequency events,
    %      if time (dT) or frequency (dF) windows were specified in the ini file.
    dT = 0;  %-- default: no superclustering
    dF = 0;
    if isfield(inidata,'postprocessing') && isfield(inidata.postprocessing,'superclustering_dt')
        dT = str2num(inidata.postprocessing.superclustering_dt);
    end
    if isfield(inidata,'postprocessing') && isfield(inidata.postprocessing,'superclustering_df')
        dF = str2num(inidata.postprocessing.superclustering_df);
    end
    if dT | dF
        onSource          = xsuperclustercombineoffsource(onSource,analysis.likelihoodType,dT,dF); 
        offSourceSelected = xsuperclustercombineoffsource(offSourceSelected,analysis.likelihoodType,dT,dF); 
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %      Find loudest event for each off-source job.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- loudestBackgroundEvent will store just the significance and
    %      the jobNumber of the loudest events in each
    %      offSourceSelected job.
    loudestBackgroundEvent = [];

    offLoudestEvent.significance          = zeros(length(jobNumbers),1);
    offLoudestEventPreVeto.significance   = zeros(length(jobNumbers),1);
    offLoudestEventWindowCut.significance = zeros(length(jobNumbers),1);
    offLoudestEventVetoSegs.significance  = zeros(length(jobNumbers),1);

    % ---- Flag indicating whether events came from time-shifted or zero
    %      lag (non time-shifted) segments
    offLoudestEvent.zeroLag               = ones(length(jobNumbers),1);

    % ---- Loop over job numbers and for each job find the loudest event
    %      that survives the inc/null cut.
    for iJob = 1:length(jobNumbers)
         % ---- All clusters from this job.
         clusterIndex = find(offSourceSelected.jobNumber == jobNumbers(iJob));
         clusterIndexBeforeDQ = find(offSourceSelectedBeforeDQ.jobNumber == jobNumbers(iJob));

         % ---- Make sure trigger list is not empty.
         if ~isempty(clusterIndex)
             % ---- If we have timeOffsets then set zeroLag flag to zero
             if sum(abs(offSourceSelected.timeOffsets(clusterIndex(1),:)) + abs(offSourceSelected.circTimeSlides(clusterIndex(1),:)))
                 offLoudestEvent.zeroLag(iJob) = 0;
             end

             % ---- Loudest event and its row number in clusterIndex.
             %      Assess loudness using significance
             % ---- Here, keep only events that pass our vetos
             [sigMax,index] = ... 
                 max(offSourceSelected.significance(clusterIndex).* ...
                 offSourceSelected.pass(clusterIndex));
             offLoudestEvent.significance(iJob)  = sigMax;

             % ---- Record loudest event significance and this job number.
             loudestBackgroundEvent = [loudestBackgroundEvent; sigMax, jobNumbers(iJob)];

             % ---- Record loudest event for each off source job AFTER veto 
             %      segs have been applied but BEFORE any veto cuts
             [sigMaxVetoSegs,indexVetoSegs] = ...
                 max(offSourceSelected.significance(clusterIndex).* ... 
                     offSourceSelected.passWindow(clusterIndex).* ... 
                     offSourceSelected.passVetoSegs(clusterIndex));
             offLoudestEventVetoSegs.significance(iJob) = sigMaxVetoSegs;

         else

             loudestBackgroundEvent = [loudestBackgroundEvent; 0, jobNumbers(iJob)];

         end

         % ---- Make sure trigger list is not empty.
         if ~isempty(clusterIndexBeforeDQ)
             % ---- Record loudest event for each off source job BEFORE any 
             %      veto cuts
             [sigMaxPreVeto,indexPreVeto] = ...
                 max(offSourceSelectedBeforeDQ.significance(clusterIndexBeforeDQ));
             offLoudestEventPreVeto.significance(iJob) = sigMaxPreVeto; 

             % ---- Record loudest event for each off source job AFTER window 
             %      cut has been applied but BEFORE any veto segments or cuts
             [sigMaxWindowCut,indexWindowCuts] = ...
                 max(offSourceSelectedBeforeDQ.significance(clusterIndexBeforeDQ).* ... 
                     offSourceSelectedBeforeDQ.passWindow(clusterIndexBeforeDQ));
             offLoudestEventWindowCut.significance(iJob) = sigMaxWindowCut;
          end

    end

    % ---- Sort list of loudest event from each job in ascending order of significance.
    loudestBackgroundEvent = sortrows(loudestBackgroundEvent);

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %      Find "loudest event" for a closed-box analysis.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- For a closed-box analysis, find the "loudest event" that will be 
    %      used to estimate the "upper limit".
    if (strcmp(analysis.type,'closedbox') || strcmp(analysis.type,'snrulclosedbox'))

        % ---- In a closed box analysis we choose one of the off source 
        %      segments to be our dummy on source segment. 
        %      We choose the off source segment based upon how loud its
        %      loudest event (rated by significance)is compared to the 
        %      loudest events in the off source jobs. 
        %      Setting percentile to 95 means we take the off source job
        %      which is louder than 95% of the other   
        if vetosTuned
            percentile = percentile_ulCalc;
        else
            percentile = percentile_tuning;
        end 

        % ---- Select a "dummy" loudest on source event.  
        index = ceil(1 + percentile/100*(size(loudestBackgroundEvent,1)-1));
        dummyOnSourceJobNumber = loudestBackgroundEvent(index,end);
        disp(['Closed-box analysis: we will use ' num2str(percentile) ...
             'th percentile loudest off-source event as our dummy on-source.']);
        disp(['Events taken from off source job number: '...
             num2str(dummyOnSourceJobNumber) ' as our dummy on source']);

        % ---- Replace onSource struct with offSourceSelected events with 
        %      jobNumber equal to dummyOnSourceJobNumber.
        dummyOnSourceIdx = find(offSourceSelected.jobNumber == ...
            dummyOnSourceJobNumber);
        onSource = xclustersubset(offSourceSelected,dummyOnSourceIdx);

    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %      Compute "probability" of each surviving on-source event.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- Probability used here is simply the fraction of off-source
    %      jobs producing an event as loud as or louder than the given 
    %      on-source event.  "Loudness" = significance.  Vetoed events
    %      are treated as significance = 0, so probability = 1.
    % ---- Default probability = 1.
    onSource.probability = ones(size(onSource.significance,1),1);  
    onSource.probabilityZeroLag = ones(size(onSource.significance,1),1);
    % ---- Loop over all on-source events.
    for ii = 1:size(onSource.likelihood,1)
        if onSource.pass(ii)
            % ---- Fraction of background jobs giving equal or larger event.
            onSource.probability(ii) = ...
                sum(offLoudestEvent.significance(:) >= onSource.significance(ii)) /...
                    size(offLoudestEvent.significance,1);

            if sum(offLoudestEvent.zeroLag)
                % ---- Fraction of zerolag background jobs giving equal or 
                %      larger event.
                onSource.probabilityZeroLag(ii) = ...
                    sum((offLoudestEvent.zeroLag(:).* ...
                    offLoudestEvent.significance(:)) >= ...
                    onSource.significance(ii)) /...
                    sum(offLoudestEvent.zeroLag);
            end
        end  
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %     Extract trigger subsets if vetos have already been tuned.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if vetosTuned
        % ---- Select triggers that pass various levels of tests.
        offSourceSelectedAfterDQ = ...
            xclustersubset(offSourceSelected,...
                           find(offSourceSelected.passVetoSegs));
        onSourceAfterDQ= ...
            xclustersubset(onSource,...
                           find(onSource.passVetoSegs));
        
        offSourceSelectedAfterDQandWindow = ...
            xclustersubset(offSourceSelected,...
                           find(offSourceSelected.passVetoSegs& ...
                           offSourceSelected.passWindow));
        onSourceAfterDQandWindow = ...
            xclustersubset(onSource,...
                           find(onSource.passVetoSegs& ...
                                onSource.passWindow));
        offSourceSelectedAfterAllCuts = ...
            xclustersubset(offSourceSelected,...
                           find(offSourceSelected.pass));
        onSourceAfterAllCuts = ...
            xclustersubset(onSource,...
                           find(onSource.pass));

    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                 Compute ULs for Simulated GWBs.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % -----------------------------------------------------------------
    %                 Assign storage for injection data.
    % -----------------------------------------------------------------

    % ---- Assign storage only if this is the first tried veto threshold set,
    if thisVeto == 1

        % ---- Waveform name.
        name = cell(1,nInjectionSets);
        % ---- Waveform parameters (as understood by xmakewaveform).
        parameters = cell(1,nInjectionSets);
        % ---- Default h_{rss} amplitude of waveform (i.e., at
        %      injectionScale==1).
        hrss = zeros(1,nInjectionSets);
        % ---- Central frequency of waveform.
        cFreq = zeros(1,nInjectionSets);
        % ---- Upper limit: injectionScale for 90% detection efficiency.
        UL = zeros(nInjectionSets,1);
        % ---- Upper limit: injectionScale for 50% detection efficiency.
        UL50 =  zeros(nInjectionSets,1);
        % ---- Diagnostic info: percent of tuning tests that give UL=NaN, 
        %      approximate SNRs in coherent and incoherent-null streams at
        %      UL amplitude.
        percentNans = zeros(nInjectionSets,1);
        SNRlist = zeros(nInjectionSets,length(analysis.detectorList));
        nullSNRlist = zeros(nInjectionSets,length(analysis.detectorList));

        % ---- Injection scales (assumed common to all injection sets).
        injectionScales = str2num(inidata.injection.injectionscales);
        nInjectionScales = length(injectionScales);

        % ---- Number of injections at each scale (assumed common to all injection sets).
        nInjections = -str2num(inidata.injection.injectioninterval);
	% ---- Verify that nInjections > 0 (nInjections<0 may imply different numbers of 
	%      injections for different waveforms).
	if nInjections<0
		error('Analysis .ini file must have injectionInterval = {negative integer}.')
        end
	% ---- KLUDGE: for common numbers of injections, injectionProcessedMask and injPeakTime
	%      can be simplified to numeric arrays, and injectionScale to a single vector.
        injectionProcessedMask = cell(nInjectionSets);
        injectionScale         = zeros(nInjectionSets,nInjectionScales);
        onSourceTimeOffset     = zeros(nInjectionSets,1); 
        injPeakTime            = cell(nInjectionSets);

    end

    % -------------------------------------------------------------
    %                  Loop over injection sets
    % -------------------------------------------------------------

    % ---- Loop over injection sets tested.
    for iWave = 1:nInjectionSets
        
        % -------------------------------------------------------------
        %                Determine waveform properties.
        % -------------------------------------------------------------

        % ---- Read and parse injection parameters.
        disp(['Loading file ' injectionFilesCell{iWave}]);
        % ---- If we have performed internally generated injections.
        [~, gps_s, gps_ns, phi, theta, psi, name, parameters] = ... 
            parseinjectionparameters(readinjectionfile(char(injectionFilesCell{iWave})));

        % ---- Record name and xmakewaveform parameters for the first 
        %      injection.
        nameCell{iWave} = cell2mat(name{1});
        parametersCell{iWave} = cell2mat(parameters{1});

        % ---- keep only last part of parametersCell if '/' is
        %      present, mostly usefull for ninja waveforms.
        tmpStr = textscan(parametersCell{iWave},'%s','Delimiter','/');
        parametersCellClean{iWave} = tmpStr{1}{end};

        % ---- Status report.
        disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        disp(['Working on waveform: ' injectionSetNames{iWave}])

        % ---- Injections are made using X-Pipeline's built-in simulation engine.  Use 
        %      this engine to compute various waveform properties (snr, hrss, etc.) and 
        %      write them to the web page.
        % ---- Make the time-series waveform.
        snippetPad = 32;
        snippetDuration = 2*snippetPad + 1;
        [t,hp,hc,hb] = xmakewaveform(nameCell{iWave}, parametersCell{iWave}, snippetDuration, ...
            snippetPad, analysis.sampleFrequency, 'catalogDirectory',catalogDir);
        % ---- Compute central frequency and hrss (ignoring noise).
        [SNR, h_rss, h_peak, Fchar, bw, Tchar, dur] =  ...
            xoptimalsnr([hp hc hb],0,analysis.sampleFrequency,[],[],[],...
            analysis.minimumFrequency,analysis.maximumFrequency);
        cFreq(iWave) =  Fchar;  %-- central frequency
        % hrss(iWave) = h_rss;  %-- hrss
        % ---- The population hrss is most accurately determined by reading directly from
        %      the .ini file rather than estimating from a single injection.
        %      Get the <type>!<parameters> string from the ini file.
        type_params_str = getfield(inidata.waveforms,injectionSetKeys{iWave});
        % ---- Split string on the '!' and get the amplitude/distance value with units.
        idx = strfind(type_params_str,'!');
        [hrss(iWave),hrss_unit{iWave}] = getinjamplordist(type_params_str(1:idx-1),type_params_str(idx+1:end));
        % ---- Loop over detectors.
        for iDetector =1:length(analysis.detectorList)
            % ---- Compute SNR for this detector, ignoring antenna response
            %      factors.  Use both actual noise spectrum and "effective
            %      null spectrum".  
            SNR =  xoptimalsnr([hp hc hb],0,analysis.sampleFrequency,S(:,iDetector),0,analysis.dF, ...
                               analysis.minimumFrequency,...
                               min(analysis.maximumFrequency,0.48*analysis.sampleFrequency));
            SNRlist(iWave,iDetector) = SNR ;
            % ---- Only calculate "nullSNR" if we have analysis.eNullIndex.
            if (analysis.eNullIndex)
                nullSNR = xoptimalsnr([hp hc hb],0,analysis.sampleFrequency,Snull(:,iDetector), ...
                                      0,analysis.dF,analysis.minimumFrequency,...
                                      min(analysis.maximumFrequency,0.48*analysis.sampleFrequency));
                if isnan(nullSNR)
                    % ---- KLUDGE: most probably the Snull for this detector is not well defined 
                    %      and the third detector does not contribute to the null energy.
                    nullSNR = 0;
                end
                nullSNRlist(iWave,iDetector) = nullSNR ;
            end
        end % -- loop over ifo

        % -------------------------------------------------------------
        %                Load injection triggers.
        % -------------------------------------------------------------

        % ---- Load injections only if this is the first tried veto threshold
        %      set, otherwise the clusters in memory can be reused.
        if thisVeto == 1
            % ---- Loop over injection scales.
            for iInjScale = 1:nInjectionScales
                % ---- Name of matlab file containing injection triggers for this scale.
                %      Remember that injection scales are numbered from 0, hence the "-1".
                simFileName = [grb_dir '/output/simulation_' injectionSetNames{iWave} '_' num2str(iInjScale-1) '_0_0_merged.mat'];
                % ---- Load injection triggers from this file.
                disp(['Loading injection file ' simFileName])
                temp = load(simFileName,'clusterInj', ...
                    'injectionProcessedMask','injectionScale','peakTime','onSourceTimeOffset');
                % ---- Copy clusterInj array for this injection scale into higher-dimensional 
                %      struct array: number of injections x number of scales.
                nInjections(iWave) = length(temp.clusterInj);
                % KLUDGE: can we speed this up by pre-allocating memory?
                for iN = 1:nInjections(iWave)
                    clusterInj(iWave,iN,iInjScale) = temp.clusterInj(iN);                 
                    % ---- Update significance (ranking staistic) of events if demanded.
                    if not(isempty(detectionIdx)) && not(isempty(clusterInj(iWave,iN,iInjScale).likelihood))
                        clusterInj(iWave,iN,iInjScale).significance = ...
                            clusterInj(iWave,iN,iInjScale).likelihood(:,detectionIdx);
                    end
                end
                injectionProcessedMask{iWave}(:,iInjScale) = temp.injectionProcessedMask;
                injectionScale(iWave,iInjScale) = temp.injectionScale;
                onSourceTimeOffset(iWave) = temp.onSourceTimeOffset;
                injPeakTime{iWave} = temp.peakTime;
                clear temp
            end %-- end loop over injection scales
        end %-- end if first-set-of-thresholds

        % ---- In here we choose which injections to be used for tuning
        %      and which to be used for ul calculation. 
        % ---- In the open box case we should pick the same injections that were
        %      used in the closed box ul calc.
        
        % ---- Get random order of indices between 1 and nInjections(iWave). 
        rand('seed',analysis.initialSeed + 22);
        injSeed = rand('seed');
        randInjIdx = randperm(nInjections(iWave));
        nInjectionsBy2(iWave) = ceil(nInjections(iWave)/2);
        
        % ---- Use the first half of randomly ordered injections for tuning.
        injIdx_tuning{iWave} = randInjIdx(1:nInjectionsBy2(iWave));
        injIdx_tuning{iWave} = sort(injIdx_tuning{iWave});
        % ---- Use the second half of randomly ordered injections for ul calc.
        injIdx_ulCalc{iWave} = randInjIdx(nInjectionsBy2(iWave)+1:nInjections(iWave));
        injIdx_ulCalc{iWave} = sort(injIdx_ulCalc{iWave});

        % ---- Create a mask to pass injections for tuning.
        injMask_tuning{iWave} = zeros(nInjections(iWave),1);
        injMask_tuning{iWave}(injIdx_tuning{iWave}) = 1;
        % ---- Create a mask to pass injections for ul calc.
        injMask_ulCalc{iWave} = zeros(nInjections(iWave),1);
        injMask_ulCalc{iWave}(injIdx_ulCalc{iWave}) = 1;
        
    end % Loop over injection sets tested
    % cut needed otherwise clusters with and without pass(*) fields
    % initialized don't combine
    % ---- Loop over injection sets tested.
    for iWave = 1:nInjectionSets
        

        % ---- Initilize storage for veto pass flags.
        %      Do as a loop over injection scales and injection number.
        %      Do SEPARATELY from 
        %      "load" loop because assignment of storage to first
        %      element of array will initialize these fields for all
        %      other elements.
        for iInjScale = 1:nInjectionScales
            for iN = 1:nInjections(iWave)
                nTriggers = length(clusterInj(iWave,iN,iInjScale).significance);
                if(nTriggers)
                  clusterInj(iWave,iN,iInjScale).pass         = ones(nTriggers,1);
                  clusterInj(iWave,iN,iInjScale).passVetoSegs = ones(nTriggers,1);
                  clusterInj(iWave,iN,iInjScale).passInjCoinc = ones(nTriggers,1);
                else
                  clusterInj(iWave,iN,iInjScale).pass         = [];
                  clusterInj(iWave,iN,iInjScale).passVetoSegs = [];
                  clusterInj(iWave,iN,iInjScale).passInjCoinc = [];
                end
            end
        end
            
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %       Use appropriate injections for tuning, ul calc .
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- If we are currently doing the tuning.
        if (vetosTuned == 0)
            fprintf(1,'Selecting injections to use for tuning... \n')
            % ---- Loop over all injections, injectionScales.
            for iInjScale = 1:nInjectionScales
                for iN = 1:nInjections(iWave)
                    % ---- Use injMask_tuning{iWave}.
                    injectionProcessedMask{iWave}(iN,iInjScale) = ...
                        min(injectionProcessedMask{iWave}(iN,iInjScale),injMask_tuning{iWave}(iN));
                end
            end 
        % ---- If we are currently doing the ul calc.
        elseif (vetosTuned == 1)
            fprintf(1,'Selecting injections to use for UL calc... \n')
            % ---- Loop over all injections, injectionScales.
            for iInjScale = 1:nInjectionScales
                for iN = 1:nInjections(iWave)
                     % ---- Use injMask_ulCalc{iWave}.
                     injectionProcessedMask{iWave}(iN,iInjScale) = ...
                         min(injectionProcessedMask{iWave}(iN,iInjScale),injMask_ulCalc{iWave}(iN));
                end
            end 
        end %-- if vetosTuned statement.

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %            Apply coherent tests to injection triggers.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        fprintf(1,'Applying vetos to %s waveform injections \n', injectionSetNames{iWave});

        % ---- Precompute coherent consistency test results for speed (vectorization)
        clusterPrecompute.significance  = vertcat(clusterInj(iWave,:,:).significance);
        clusterPrecompute.likelihood    = vertcat(clusterInj(iWave,:,:).likelihood);
        clusterPrecompute.peakTime      = vertcat(clusterInj(iWave,:,:).peakTime);
        clusterPrecompute.peakFrequency = vertcat(clusterInj(iWave,:,:).peakFrequency);
        clusterPrecompute.boundingBox   = vertcat(clusterInj(iWave,:,:).boundingBox);
        [passRatioPrecompute passFixedRatioPrecompute ] = ...
            xapplycoherentcuts(clusterPrecompute,analysis,vetoMethod,ratioArray,deltaArray,...
                               medianA,medianC,maxE,fixedRatioArray,fixedDeltaArray);
        % ---- Note that xapplyvetosegments expects a column of 
        %      trigger times for each ifo analysed. Therefore we 
        %      must multiply the column vector peakTime 
        %      by a row vector of ones with length numIfos   
        passTimeVetoPrecompute = ...
            xapplyvetosegments(clusterPrecompute,...
                               clusterPrecompute.peakTime*...
                               ones(1,length(analysis.detectorList)),...
                               vetoSegs,analysis,...
                               ['inj_' num2str(iWave) ],'quiet');
        passFrequencyVetoPrecompute = ...
            xapplyvetosegments(clusterPrecompute,...
                               clusterPrecompute.peakFrequency*...
                               ones(1,length(analysis.detectorList)),...
                               freqVetoBands,analysis,...
                               ['inj_' num2str(iWave) ],'quiet');
        passVetoSegsPrecompute = min(passTimeVetoPrecompute, passFrequencyVetoPrecompute);

        % ---- Iterator on precomputed outputs.
        iCluster = 0;

        % ---- Loop over injection scales and injection number.
        for iInjScale = 1:nInjectionScales
            for iN = 1:nInjections(iWave)
                % ---- Find range of precomputed values
                precomputeRange = iCluster+1:iCluster+length(clusterInj(iWave,iN,iInjScale).pass);
                iCluster = iCluster + length(clusterInj(iWave,iN,iInjScale).pass);
                % -- work around 0x1 and 0x0 empty matrices not being equal
                if length(precomputeRange) == 0
                  precomputeRange = [];
                end

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %        Apply veto segments to injection triggers.
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                %disp(['Applying veto segments to ' ...
                %    ' injections with injScale number ' ...
                %    num2str(iInjScale) ...
                %    '(' num2str(iN) '/' num2str(nInjections(iWave)) ')' ]);
                                  
                clusterInj(iWave,iN,iInjScale).passVetoSegs = ...
                    passVetoSegsPrecompute(precomputeRange);
                
                clusterInj(iWave,iN,iInjScale).pass = ...
                    min(clusterInj(iWave,iN,iInjScale).passVetoSegs,...
                    clusterInj(iWave,iN,iInjScale).pass);

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %   Apply coherent glitch test to injection triggers.
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                clusterInj(iWave,iN,iInjScale).passRatio = ...
                    passRatioPrecompute(precomputeRange);
                clusterInj(iWave,iN,iInjScale).passFixedRatio = ...
                    passFixedRatioPrecompute(precomputeRange);
                
                clusterInj(iWave,iN,iInjScale).pass = min( ...                        
                    clusterInj(iWave,iN,iInjScale).pass, ...
                    clusterInj(iWave,iN,iInjScale).passRatio);
                clusterInj(iWave,iN,iInjScale).pass = min( ...
                    clusterInj(iWave,iN,iInjScale).pass, ...
                    clusterInj(iWave,iN,iInjScale).passFixedRatio);

                % ---- Apply superclustering to combine nearby-in-time-frequency events.
                if dT | dF
                    clusterInj(iWave,iN,iInjScale) = xsuperclustercombineoffsource( ...
                        clusterInj(iWave,iN,iInjScale),analysis.likelihoodType,dT,dF); 
                end

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %  Keep only triggers which lie within 100ms of injection.
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                % skip if there are no triggers
                if(not(isempty(clusterInj(iWave,iN,iInjScale).boundingBox)))
                % ---- Event must peak within interval 
                %      [-injCoincTimeWindow(1), +injCoincTimeWindow(2)] of 
                %      the injection.
                if strcmp(lower(nameCell{iWave}),'inspiral') || ...
                      strcmp(lower(nameCell{iWave}),'inspiralsmooth') || ...
                      strcmp(lower(nameCell{iWave}),'ninja') || ...
                      strcmp(lower(nameCell{iWave}),'lalinspiral') 
                    % ---- Larger coincidence window for inspirals.
                    injCoincTimeWindow = [10.0 0.1]; 
                else
                    % ---- Default coincidence window.
                    injCoincTimeWindow = [0.1 0.1]; 
                end

                % ---- Bounding box of injection in the time-frequency plane.
                injectionBox = [(injPeakTime{iWave}(iN)-injCoincTimeWindow(1)-onSourceTimeOffset(iWave)), ...
                    analysis.minimumFrequency, sum(injCoincTimeWindow), ...
                    analysis.maximumFrequency-analysis.minimumFrequency];   

                % ---- Find all clusters whose bounding box does NOT overlap that 
                %      of the injection and set passInjCoinc to zero for them
                mask = find(not(rectintersect(injectionBox,clusterInj(iWave,iN,iInjScale).boundingBox)));
                clusterInj(iWave,iN,iInjScale).passInjCoinc(mask) = 0;
                clusterInj(iWave,iN,iInjScale).pass = min( ...
                   clusterInj(iWave,iN,iInjScale).pass, ...
                   clusterInj(iWave,iN,iInjScale).passInjCoinc);
                end %-- trigger list not empty

            end  %-- loop over injection number
        end  %-- loop over injection scales

    end % Loop over injection sets tested
    % cutting the loop into two parts is needed by the 'ulbox'
    % sanitized loudest event threshold
    % ---- Loop over injection sets tested.
    for iWave = 1:nInjectionSets

        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %                   Compute upper limit.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Compute upper limit.
        if (exist('pathToInjections','var') && ~isempty(pathToInjections) && ...
            strcmp(analysis.type,'snrulclosedbox'))

            % ---- "snrulclosed" box analysis.  Calculate UL in terms of SNR.
            %      Experimental!  NOT TO BE USED FOR PUBLISHED ULs!
            warning('snrulclosed analysis is experimental!  Caveat emptor!')
            [ul90p,allFractionOnSource,effFactor,ul50p,injectionValueArray] = ...
            snrul(boxChoice.likelihood,boxChoice.jobNumber,...
               boxChoice.jobFilesProcessed,injection.likelihoodCell,...
               injection.maskCell,injection.scaleArray,bestRatio,...
               analysis.iNullIndex,analysis.eNullIndex,energyIndex,pathToInjections,...
               analysis.amplitudeSpectraCell,analysis.analysisTimes,...
               analysis.sampleFrequency,analysis.detectorList);
            injection.scaleArray = injectionValueArray;

        else

            % ---- Compute upper limit on injection scale for 90% and
            %      50% detection efficiency. 
            threshold = max(onSource.significance .* onSource.pass);
            % ---- Up the threshold by the loudest event coming from
            %      the weakest injection to catch any spurious loud
            %      events coming from fast processing of injections
            if strcmp(analysis.type,'ulbox')
              [m iMinInjScale] = min(injectionScale(1,:));
              threshold = max(max(max(vertcat(clusterInj(:,:,iMinInjScale).significance).*...
                                      vertcat(clusterInj(:,:,iMinInjScale).pass))),...
                              threshold);
              threshold=threshold*1.01;
              disp(['The sanity checked loudest event threshold is ' num2str(threshold)]);
            end
            [UL90p(iWave),UL50p(iWave),efficiency(iWave,:),...
            flareInjScale,flareDetection,UL95p(iWave),analysisEff(iWave,:)] = ... 
               loudesteventul(threshold, squeeze(clusterInj(iWave,:,:)),...
               squeeze(injectionProcessedMask{iWave}(:,:)),injectionScale(iWave,:));

            % ---- KLUDGE
            %      Replace 90% UL with 95% UL when tuning linearCut, this
            %      should limit the cases where the cut goes below
            %      90% efficiency for very loud signals
            if vetosTuned==0
              [percentageLvl UL90p(iWave)] = ...
                  xoverrideullevel(vetoMethod,UL50p(iWave),UL90p(iWave),UL95p(iWave));
            end
            % ---- END KLUDGE

            fprintf(1,['For %s we have 50%% UL at injScale = %8.2e, '...
                       '90%% UL at injScale = %8.2e \n'],...
               injectionSetNames{iWave}, UL50p(iWave), UL90p(iWave) ) 

            [injectionScaleSorted(iWave,:) sortIdx] = sort(injectionScale(iWave,:));
            efficiencySorted(iWave,:)               = efficiency(iWave,sortIdx);
            disp('injectionScale  efficiency');
            disp('--------------  ----------');
            [injectionScaleSorted(iWave,:)', efficiencySorted(iWave,:)']
       
        end % -- Choice of upper limit calc.

        % ---- Keep track of how many NaNs were encountered in loudesteventul
        %      call.
        nNans = sum(isnan(UL90p(iWave)),1);
        percentNans(iWave,1) = nNans / length(jobNumbers)*100;


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %                 Web page plots for this waveform.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Only make plots, tables if vetos have already been tuned.
        if vetosTuned

            % ---- Overlay scatter plot of injection clusters at the scale 
            %      closest to the 90% UL, or loudest injection scale if UL
            %      does not exist.
            % ---- Find the desired injection scale.
            if(~isnan(UL90p(iWave)))
                [~, iS1] = nanmin(abs(UL90p(iWave)-injectionScale(iWave,:)));
            else
                [~, iS1] = max(injectionScale(iWave,:));
            end

            % ---- Report number of injections used for UL calc at this
            %      injectionScale
            nInjectionsUL(iWave) = sum(injectionProcessedMask{iWave}(:,iS1));
            fprintf(1,'%s\n',[num2str(nInjectionsUL(iWave)) ...
                ' injections used for UL calc'])
  

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %      Find loudest event associated with each injection
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            loudestInjEvents = [];
            loudestInjEventsPassCuts = [];

            firstLoop = 1;
            firstPass = 1;
            % ---- Loop over all injections of this waveform at this 
            %      injection scale.
            for iN = 1:nInjections(iWave)
                if (injectionProcessedMask{iWave}(iN,iS1)==1)
                    % ---- Find index of loudest trigger associated with 
                    %      this injection.
                    [injSigMax,injSigMaxIdx] = max(clusterInj(iWave,iN,iS1).significance);

                    % ---- Pull out loudest trigger.
                    thisLoudestInjEvent = xclustersubset(clusterInj(iWave,iN,iS1),injSigMaxIdx);

                    % ---- Create a list of the loudest triggers.
                    if firstLoop == 1
                        loudestInjEvents = thisLoudestInjEvent;
                        firstLoop = 0;
                    else
                        loudestInjEvents = ...
                            xclustermerge(loudestInjEvents,thisLoudestInjEvent);
                    end

                    % ---- Create list of the loudest triggers which pass
                    %      our veto cuts.
                    if thisLoudestInjEvent.pass == 1
                        if firstPass == 1
                            loudestInjEventsPassCuts = thisLoudestInjEvent;
                            firstPass = 0;
                        else
                            loudestInjEventsPassCuts = ...
                                xclustermerge(loudestInjEventsPassCuts,thisLoudestInjEvent);
                        end
                    end
                end % -- if injectionProcessedMask.
            end % -- Loop over all inj and given inj scale.

            if ~(length(loudestInjEvents.significance) == nInjectionsUL(iWave))
                disp(['Something wrong with loudestInjEvents, some ' ...
                      'injections do not have a loudest event'])
                disp(['Number of injections:     ' num2str(nInjectionsUL(iWave))]);
                disp(['Number of loudest events: ' num2str(length(loudestInjEvents.significance))]);                    
            end

            if ~isfield(loudestInjEventsPassCuts,'significance')
                warning('The loudest event for each injection was vetoed');
                numLoudestInjEventsPassCuts = 0;
                triggersForHist{iWave} = {loudestInjEvents};
                triggerNamesForHist{iWave} = {'loudest inj'};
            else 
                numLoudestInjEventsPassCuts = length(loudestInjEventsPassCuts.significance); 
                triggersForHist{iWave} = {loudestInjEvents,loudestInjEventsPassCuts};
                triggerNamesForHist{iWave} = {'loudest inj','loudest inj, passed cuts'};
            end  

            disp(['For ' ...
                  num2str(numLoudestInjEventsPassCuts) ...
                  ' of our ' ...
                  num2str(size(loudestInjEvents.likelihood,1)) ...
                  ' injections, the event with largest significance'... 
                  ' survives our veto cuts' ]);

            % ---- save best matching trigger for each injection
            for iInjScale = 1:nInjectionScales
                for iN = 1:nInjections(iWave)
                    % ---- Find index of loudest trigger associated with 
                    %      this injection that passes all cuts
                    [injSigMax,injSigMaxIdx] = max(clusterInj(iWave,iN,iInjScale).significance.*clusterInj(iWave,iN,iInjScale).pass);
                    % ---- If no passing trigger found, associate the loudest one
                    if not(clusterInj(iWave,iN,iInjScale).pass(injSigMaxIdx))
                        [injSigMax,injSigMaxIdx] = max(clusterInj(iWave,iN,iInjScale).significance);
                    end
                    % ---- pull out this trigger
                    injAssociatedTrigger(iN,iInjScale) = ...
                        xclustersubset(clusterInj(iWave,iN,iInjScale),injSigMaxIdx);
                end % -- loop over injection number
            end % -- loop over injection scale
            save([analysis.grb_name '_' user_tag '_' analysis.type ...
                 'associatedTriggers_' injectionSetNames{iWave}  '.mat'],'-mat',...
                 'injAssociatedTrigger','injectionScale','injectionProcessedMask','iWave');

        end % -- if (vetosTuned)
    end  %-- loop over injection sets

    % ---- Store ULs for all frequencies.
    ulVector90(thisVeto,:) = UL90p;
    ulVector50(thisVeto,:) = UL50p;

end % ---- loop over different ratios


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Store UL results for all veto combos investigated
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Store these results.
%      KLUDGE: Why are we renaming the variables?
vetoNullRangePlots  = vetoNullRange;
vetoPlusRangePlots  = vetoPlusRange;
vetoCrossRangePlots = vetoCrossRange;

disp(['Completed loop over veto cut combinations!']);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   Save UL results if doing vetoTestOnly
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if vetosTuned==0
    disp(['Writing matFile summary of veto tuning investigation.'])
    vetoTestFileName = [analysis.grb_name '_' user_tag ...
        '_' analysis.type '_' detectionStat '_vetoTest' vetoMethod '.mat'];
    % ---- Save UL results to a mat file.
    save(vetoTestFileName, 'ulVector90', 'ulVector50', ...
        'nameCell', 'parametersCell','parametersCellClean', 'setNameCell', ...
        'injectionSetNames', 'injectionSetKeys', ...
        'vetoNullRangePlots', 'vetoPlusRangePlots', 'vetoCrossRangePlots', ...
        'tuningNameStr', 'vetoMethod', 'nullVetoType', 'plusVetoType', ...
        'crossVetoType', 'cFreq', 'detectionStat');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Produce text file containing tuned veto values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if vetosTuned
    tunedVetoFileName = [analysis.grb_name '_' user_tag '_' analysis.type '_tunedVetos.txt']
    ftunedVeto = fopen(tunedVetoFileName,'w');
    fprintf(ftunedVeto,'%s\n',[...
        nullVetoType ' ' num2str(vetoNullRange) ' '...
        plusVetoType ' ' num2str(vetoPlusRange) ' '...
        crossVetoType ' ' num2str(vetoCrossRange) ' '...
        ]);
    fclose(ftunedVeto);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Save variables.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- If the vetoes have been tuned, then save an extended list of
%      variables, including all variables needed by xmakehtmlreport.
if vetosTuned

    outputMatFileName = [analysis.grb_name '_' user_tag '_' analysis.type '.mat'];
    save(outputMatFileName, ...
        'analysis', ...                 % required by: xmakehtmlreport
        'analysisEff', ...              % [xmakehtmlreport]
        'binArray', ...                 % [xmakehtmlreport]
        'catalogDir', ...
        'cFreq', ...                    % [xmakehtmlreport]
        'clusterInj',...
        'crossVetoType', ...            % [xmakehtmlreport]
        'detectionStat', ...            % [xmakehtmlreport]
        'dummyOnSourceJobNumber', ...   % [xmakehtmlreport]
        'efficiency', ...               % [xmakehtmlreport]
        'efficiencySorted', ...
        'fixedRatioArray', ...          % [xmakehtmlreport]
        'fixedDeltaArray', ...          % [xmakehtmlreport]
        'hrss', ...                     % [xmakehtmlreport]
        'hrss_unit', ...                % [xmakehtmlreport]
        'ifos', ...
        'injectionFilesCell', ...       % [xmakehtmlreport]
        'injectionScale', ...           % [xmakehtmlreport]
        'injectionScaleSorted', ...
        'injIdx_tuning', ...            % [xmakehtmlreport]
        'injIdx_ulCalc', ...            % [xmakehtmlreport]
        'injSeed', ...                  % [xmakehtmlreport]
        'maxE', ...
        'medianA', ...
        'medianC', ...
        'medianE', ...                  % [xmakehtmlreport]
        'medianI', ...                  % [xmakehtmlreport]
        'nameCell', ...                 % [xmakehtmlreport]
        'nInjections', ...              % [xmakehtmlreport]
        'nInjectionsBy2', ...           % [xmakehtmlreport]
        'nInjectionsUL', ...
        'nJobFilesProcessed', ...       % [xmakehtmlreport]
        'nOffJob_tuning', ...           % [xmakehtmlreport]
        'nOffJob_ulCalc', ...           % [xmakehtmlreport]
        'nullSNRlist', ...
        'nullVetoType', ...             % [xmakehtmlreport]
        'offIdx_tuning', ...            % [xmakehtmlreport]
        'offIdx_ulCalc', ...            % [xmakehtmlreport]
        'offJob_tuning', ...            % [xmakehtmlreport]
        'offJob_ulCalc', ...            % [xmakehtmlreport]
        'offJobsDeadtime', ...          % [xmakehtmlreport]
        'offJobSeed', ...               % [xmakehtmlreport]
        'offLoudestEvent', ...                      % [xmakehtmlreport]
        'offLoudestEventPreVeto', ...               % [xmakehtmlreport]
        'offLoudestEventVetoSegs', ...              % [xmakehtmlreport]
        'offLoudestEventWindowCut', ...             % [xmakehtmlreport]
        'offSourceSelected', ...                    % [xmakehtmlreport]
        'offSourceSelectedAfterAllCuts', ...        % [xmakehtmlreport]
        'offSourceSelectedAfterDQ', ...             % [xmakehtmlreport]
        'offSourceSelectedAfterDQandWindow', ...    % [xmakehtmlreport]
        'offSourceSelectedBeforeDQ', ...            % [xmakehtmlreport]
        'onJobsDeadtime', ...           % [xmakehtmlreport]
        'onSource', ...                 % [xmakehtmlreport]
        'onSourceAfterAllCuts', ...     % [xmakehtmlreport]
        'onSourceAfterDQ', ...          % [xmakehtmlreport]
        'onSourceAfterDQandWindow', ... % [xmakehtmlreport]
        'parametersCell', ...
        'parametersCellClean', ...      % [xmakehtmlreport]
        'percentile_tuning', ...        % [xmakehtmlreport]
        'percentile_ulCalc', ...        % [xmakehtmlreport]
        'percentNans', ...
        'plusVetoType', ...             % [xmakehtmlreport]
        'ratioArray', ...               % [xmakehtmlreport]
        'SNRlist', ...
        'triggerNamesForHist', ...      % [xmakehtmlreport]
        'triggersForHist', ...          % [xmakehtmlreport]
        'tunedPrePassCuts', ...         % [xmakehtmlreport]
        'tuningNameStr', ...            % [xmakehtmlreport]
        'UL50p', ...                    % [xmakehtmlreport] 50% ULs on injection scale for each injection set for last veto combination tested
        'UL90p', ...                    % [xmakehtmlreport] 90% ULs on injection scale for each injection set for last veto combination tested
        'ulVector50', ...               % [xtunegrbwebpage] 50% ULs on injection scale for all thresholds x sets
        'ulVector90', ...               % [xtunegrbwebpage] 90% ULs on injection scale for all thresholds x sets
        'uniqueOffJobNumber', ... 
        'uniqueOffJobNumberFail', ...   % [xmakehtmlreport]
        'user_tag', ...                 % [xmakehtmlreport]
        'vetoCrossRange', ...           % [xmakehtmlreport]
        'vetoCrossRangePlots', ...      %          KLUDGE: identical to 'vetoCrossRange'
        'vetoMethod', ...               % [xmakehtmlreport]
        'vetoNullRange', ...            % [xmakehtmlreport]
        'vetoNullRangePlots', ...       %          KLUDGE: identical to 'vetoNullRange'
        'vetoPlusRange', ...            % [xmakehtmlreport]
        'vetoPlusRangePlots', ...       %          KLUDGE: identical to 'vetoPlusRange'
        'vetoSegList', ...              % [xmakehtmlreport]
        'injectionSetKeys', ...         
        'injectionSetNames', ...        
        'window', ...                   % [xmakehtmlreport]
        '-v7.3' ...                     % sets .mat file version; required for storing large variables
    );

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Write web page report.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Write web page if desired.
if vetosTuned
    disp('Writing web page report ...');
    xmakehtmlreport(outputMatFileName);
    disp(' ... finished.');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              Done!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(['xmakegrbwebpage completed successfully!']);

% ---- Done.
return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         Helper functions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function mask = rectintersect(A, B)
% RECTINTERSECT Rectangle intersection
%
%   Returns bool table of intersection, for speed A is assumed to contain
%   only on rectangle
%
%  mask = rectintersect(A, B)
%
%  A      1-by-4 matrix giving rectangle in [X,Y,WIDTH,HEIGHT] format
%  B      N-by-4 matrix giving rectangles in [X,Y,WIDTH,HEIGHT] format
%
%  mask   bool vector of intersection between A and rectangles in B

if size(A,1) ~= 1
  error(['Number of rectangles in A is ' num2str(size(A,1)) ...
         ', it should be 1.']);
end

horMask = (A(1) <= B(:,1) & B(:,1) < A(1) + A(3)) | ...
          (B(:,1) <= A(1) & A(1) < B(:,1) + B(:,3));
verMask = (A(2) <= B(:,2) & B(:,2) < A(2) + A(4)) | ...
          (B(:,2) <= A(2) & A(2) < B(:,2) + B(:,4));
mask = horMask & verMask;
