function [FpList,FcList,dF,S,Snull,wFpDPmean,wFcDPmean,wFbDPmean] = xwritenetworkinfo(analysis,varargin)
% XWRITENETWORKINFO - Write information about the detector network to a webpage.
% xwritenetworkinfo is a helper function for xmakegrbwebpage.
%
% Usage:
%
%  [FpList,FcList,dF,S,Snull] = xwritenetworkinfo(analysis,varargin)
%
%    analysis           Structure created in xmakegrbwebpage.
%    varargin           Optional.  If specfied, must consist of the following 
%                       three inputs:
%                         fout: Pointer to webpage html file, must be open and 
%                           writable.
%                         figures_dirName: String. Name of directory where 
%                           image files (.png) are stored.
%                         figfiles_dirName: String. Name of directory where 
%                           matlab figure files (.fig) are stored.
%
%    FpList             Array, plus polarization antenna response value 
%                       for each detector in our network 
%    FcList             Array, cross polarization antenna response value 
%                       for each detector in our network 
%    dF                 Double, best frequency resolution 
%    S                  Array, sensitivity curve at best frequency
%                       resolution
%    Snull              Array, sensitivity in incoherent null streams at
%                       best frequency resolution
%
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
if (nargin>1)
    % ---- If we have optional args then we will produce a webpage
    produceWebpageFlag = 1;    
    
    % ---- We should have three optional args 
    if length(varargin)==3
        fout             = varargin{1};
        figures_dirName  = varargin{2}; 
        figfiles_dirName = varargin{3};
    else 
        error(['fout, figures_dirName and figfiles_dirName '...
            'must all be provided if webpage is to be produced']); 
    end
else
    % ---- If we only have one optional arg then don't make webpage
    produceWebpageFlag = 0;    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Write GPS,UTC times, RA and dec to web page.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if produceWebpageFlag

    fprintf(fout,'%s\n',['<h2> ' analysis.grb_name ' </h2>']);

    % ---- find out if the file tmpUTCtime_53ab6e3fe.txt already exists
    tmpTimeFile = dir('tmpUTCtime_53ab6e3fe.txt');

    % ---- if this file doesnt exist we will try and create it
    if isempty(tmpTimeFile)
       % ---- Call lal_tconvert to convert GRB time to UTC; dump to temporary file.
       % ---- eval commands don't work when we compile matlab
       %      xbatchgrbwebpage now writes the file containing the UTC time
       eval(['!lal_tconvert ' num2str(analysis.gpsCenterTime) ...
             ' > tmpUTCtime_53ab6e3fe.txt']);
    end

    % ---- Retrieve UTC time from file.
    timeFile = fopen('tmpUTCtime_53ab6e3fe.txt');
    timeStrCell = textscan(timeFile,'%s','delimiter','\n');
    fclose(timeFile);

    % ---- Convert time cell array into a single string.
    timeStr = cell2mat(timeStrCell{:});

    % ---- Find peak probability sky positions
    maskTime = analysis.skyPositionsTimes(1) == analysis.skyPositionsTimes;
    if size(analysis.skyPositions,2) >= 3
      maskProb = analysis.skyPositions(1,3)*0.99 < analysis.skyPositions(:,3);
    else
      maskProb = zeros(size(maskTime));
      maskProb(1) = 1;
    end
    maskPeak = maskTime & maskProb;

    % ---- Compute RA,DEC 
    [ra,dec] = earthtoradec(analysis.skyPositions(maskPeak,2),analysis.skyPositions(maskPeak,1), ...
        analysis.skyPositionsTimes(maskPeak));

    % ---- read sky position error from grb.param
    paramFile = fopen('grb.param');
    paramStrCell = textscan(paramFile,'%s','delimiter',' ');
    paramStrCell = paramStrCell{1};
    iErr = find(strcmp(paramStrCell,'-e') | strcmp(paramStrCell,'--sky-pos-err'));
    if iErr
      skyPosError = paramStrCell{iErr+1};
    else
      skyPosError = '0';
    end

    % ---- Write GPS,UTC times, RA and dec to web page.
    fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
    fprintf(fout,'%s\n',['<tr>']);
    fprintf(fout,'%s\n',['<td><b> External trigger time </b></td>']);
    fprintf(fout,'%s\n',['<td> ' num2str(analysis.gpsCenterTime) ' ' ...
        timeStr ' </td>']);
    fprintf(fout,'%s\n',['</tr>']);
    fprintf(fout,'%s\n',['<tr>']);
    fprintf(fout,'%s\n',['<td><b> Right ascension </b></td>']);
    fprintf(fout,'%s\n',['<td> ' num2str(ra') ' </td>']);
    fprintf(fout,'%s\n',['</tr>']);
    fprintf(fout,'%s\n',['<tr>']);
    fprintf(fout,'%s\n',['<td><b> Declination </b></td>']);
    fprintf(fout,'%s\n',['<td> ' num2str(dec') ' </td>']);
    fprintf(fout,'%s\n',['</tr>']);
    fprintf(fout,'%s\n',['<tr>']);
    fprintf(fout,'%s\n',['<td><b> Sky position error (degree) </b></td>']);
    fprintf(fout,'%s\n',['<td> ' skyPosError ' </td>']);
    fprintf(fout,'%s\n',['</tr>']);
    fprintf(fout,'%s\n','</table><br>');


end % -- end of produceWebpageFlag

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Write antenna responses for each detector.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Find antenna responses for each detector in Earth-based
%      polarization frame (the default for X-Pipeline).
for iDetector = 1:length(analysis.detectorList)
    [Fp, Fc, Fb] = ComputeAntennaResponse(analysis.skyPositions(1,2), ...
        analysis.skyPositions(1,1),0,analysis.detectorList{iDetector});
    FpList(1,iDetector) = Fp;
    FcList(1,iDetector) = Fc;
    FbList(1,iDetector) = Fb;
end
clear Fp Fc Fb

if produceWebpageFlag

    fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
    fprintf(fout,'%s\n',['<tr>']);
    fprintf(fout,'%s\n',['<td><b> Detector </b></td>']);
    fprintf(fout,'%s\n',['<td><b> F_+  </b></td>']);
    fprintf(fout,'%s\n',['<td><b> F_x </b></td>']);
    fprintf(fout,'%s\n',['<td><b> F_b </b></td>']);
    fprintf(fout,'%s\n',['<td><b> F_+^2 + F_x^2 </b></td>']);
    fprintf(fout,'%s\n',['</tr>']);

    % ---- Write antenna responses for each detector in Earth-based
    %      polarization frame (the default for X-Pipeline).
    for iDetector = 1:length(analysis.detectorList)
        fprintf(fout,'%s\n',['<tr>']);
        fprintf(fout,'%s\n',['<td> ' analysis.detectorList{iDetector} ' </td>']);
        fprintf(fout,'%s\n',['<td> ' num2str(FpList(1,iDetector)) ' </td>']);
        fprintf(fout,'%s\n',['<td> ' num2str(FcList(1,iDetector)) ' </td>']);
        fprintf(fout,'%s\n',['<td> ' num2str(FbList(1,iDetector)) ' </td>']);        
        fprintf(fout,'%s\n',['<td> ' num2str(FpList(1,iDetector)^2 + ...
            FcList(1,iDetector)^2) ' </td>']);
        fprintf(fout,'%s\n',['</tr>']);
    end
    fprintf(fout,'%s\n','</table><br>');

end % -- end if produceWebpageFlag

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Write list of sky positions searched.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if produceWebpageFlag

    % ---- If we are running with a sky positions array, write a table.
    % ---- Check if array has 4 columns [theta phi pOmega dOmega].
    if  size(analysis.skyPositions,2) == 4

        [ra_list, dec_list] = earthtoradec(analysis.skyPositions(:,2),...
                                           analysis.skyPositions(:,1),...
                                           analysis.skyPositionsTimes);

        fprintf(fout,'%s\n',' <input id="skyPos" type="checkbox" unchecked ');
        fprintf(fout,'%s\n',' onclick="toggleVisible(''skyPos'');" /> ');
        fprintf(fout,'%s\n',[' Table of sky positions searched (' ...
                            num2str(length(ra_list)/analysis.jobsPerWindow) ' sky positions)']);
        fprintf(fout,'%s\n',' <div id="div_skyPos" style="display: none;"> ');
        fprintf(fout,'%s\n',['<br>Note that a total of ' num2str(analysis.jobsPerWindow) ...
                            ' data blocks are required to cover the full on-source window. ' ...
                            'The search grid is updated for each block to track the GRB across the sky. ' ...
                            'Therefore the number of rows in the table below is a factor of ' ...
                            num2str(analysis.jobsPerWindow) ' larger than the ' ...
                            'number of physical sky positions searched.']);
        fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
        fprintf(fout,'%s\n',['<tr>']);
        fprintf(fout,'%s\n',['<td><b> Block centre time </b></td>']);
        fprintf(fout,'%s\n',['<td><b> RA (deg)  </b></td>']);
        fprintf(fout,'%s\n',['<td><b> Dec (deg) </b></td>']);
        fprintf(fout,'%s\n',['<td><b> theta (rad) </b></td>']);
        fprintf(fout,'%s\n',['<td><b> phi (rad) </b></td>']);
        fprintf(fout,'%s\n',['<td><b> pOmega </b></td>']);
        fprintf(fout,'%s\n',['<td><b> dOmega </b></td>']);
        fprintf(fout,'%s\n',['</tr>']);

        for iSky = 1:length(ra_list)
            fprintf(fout,'%s\n',['<tr>']);
            fprintf(fout,'%s\n',['<td> ' num2str( analysis.skyPositionsTimes(iSky)) ' </td>']);
            fprintf(fout,'%s\n',['<td> ' num2str( ra_list(iSky)) ' </td>']);
            fprintf(fout,'%s\n',['<td> ' num2str( dec_list(iSky)) ' </td>']);
            fprintf(fout,'%s\n',['<td> ' num2str( analysis.skyPositions(iSky,1) ) ' </td>']);
            fprintf(fout,'%s\n',['<td> ' num2str( analysis.skyPositions(iSky,2) ) ' </td>']);
            fprintf(fout,'%s\n',['<td> ' num2str( analysis.skyPositions(iSky,3) ) ' </td>']);
            fprintf(fout,'%s\n',['<td> ' num2str( analysis.skyPositions(iSky,4) ) ' </td>']);
            fprintf(fout,'%s\n',['</tr>']);
        end

        fprintf(fout,'%s\n','</table><br>');
        fprintf(fout,'%s\n','</div>'); %-- end of div_skyPos.
        fprintf(fout,'%s\n','');

    end

end % -- end if produceWebpageFlag

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Make noise spectra & coherent projection plots for web page.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ------------------------------------------------------------------------
%    Preparatory
% ------------------------------------------------------------------------

% ---- Find the detector spectra with the best frequency resolution (for
%      plots and for estimating SNRs).
bATi = 1; maxPrec = 0;
for i = 1:length(analysis.amplitudeSpectraCell)
    if (maxPrec < length(analysis.amplitudeSpectraCell{i}))
        maxPrec = length(analysis.amplitudeSpectraCell{i});
        bATi = i;
    end
end
dF = 1/analysis.analysisTimes(bATi);

% ---- Record spectra and Gaussianity / Rayleigh "spectra" for best
%      frequency resolution.
if isfield(analysis,'gaussianityCell') && ~isempty(analysis.gaussianityCell)
    gaussianity = squeeze(analysis.gaussianityCell{bATi});
end
S = analysis.amplitudeSpectraCell{bATi}.^2;

% ---- Convert antenna responses in the dominant-polarization frame, after
%      weighting by the noise spectra.
%      Array entries are (frequency bin) x (detector).
wFp = repmat(FpList(1,:),[size(S,1) 1]) ./ sqrt(S);
wFc = repmat(FcList(1,:),[size(S,1) 1]) ./ sqrt(S);
wFb = repmat(FbList(1,:),[size(S,1) 1]) ./ sqrt(S);
[wFpDP wFcDP] = convertToDominantPolarizationFrame(wFp,wFc);
wFbDP = wFb;
clear wFp wFc wFb
% ---- Normalized antenna response vectors ('Hat' := unit vector).
wFpDPHat = wFpDP./repmat(sum(wFpDP.^2,2).^0.5,[1,length(analysis.detectorList)]);
wFcDPHat = wFcDP./repmat(sum(wFcDP.^2,2).^0.5,[1,length(analysis.detectorList)]);
wFbDPHat = wFbDP./repmat(sum(wFbDP.^2,2).^0.5,[1,length(analysis.detectorList)]);
% ---- Compute mean antenna response in DP frame
wFpDPmean = mean(wFpDP,1);
wFcDPmean = mean(wFcDP,1);
wFbDPmean = mean(wFbDP,1);

if produceWebpageFlag

    % ---- Write section header to web page.
    fprintf(fout,'%s\n','<h2>Network Sensitivity Plots</h2>');

    % ---- First assign a fixed color with each detector.
    listOfColors = [1 0 0 ; 0 1 0 ; 0 0 1 ; 1 0 1 ; 0 1 1];
    newListOfColors = [];
    detectorColorsOrder = {'H1','L1','K1','V1','G1'};
    for jj = 1:length(analysis.detectorList)
         newListOfColors(jj,:) = listOfColors(strcmp(analysis.detectorList{jj}, ...
             detectorColorsOrder),:);
    end
    set(0,'DefaultAxesColorOrder',newListOfColors)

    % ------------------------------------------------------------------------
    %    Make noise spectrum plots.
    % ------------------------------------------------------------------------

    % ---- Plot amplitude noise spectra.
    xspectrumplot(0:dF:dF*(size(S,1)-1),S.^0.5,analysis.detectorList, ...
        analysis,'detectors_spectra',figures_dirName,figfiles_dirName)

    % ---- Add SRD curve to amplitude noise spectra
    open([figfiles_dirName 'detectors_spectra.fig']); hold on;

    % ---- retrieve legend from figure
    %      note that the location needs to be set again 
    legendStr = get(legend,'String');

    if max( strcmp('G1',analysis.detectorList) )  
        % ---- We have GEO in our network
        f = 40:ceil(dF*(size(S,1)-1));
        [Sh, fstop] = SRD('GEO-HF',f);
        plot(f,Sh.^0.5,'k--','linewidth',2);
        % ---- update legend
        legendStr{length(legendStr)+1} = ...
            ['GEO-HF design '];
    end

    if (max( strcmp('H1',analysis.detectorList) ) | ...
        max( strcmp('H2',analysis.detectorList) ) | ...
        max( strcmp('L1',analysis.detectorList) ) )
        % ---- We have a LIGO detector in our network
        f = 40:ceil(dF*(size(S,1)-1));
        [Sh, fstop] = SRD('aLIGO',f);
        plot(f,Sh.^0.5,'k-','linewidth',2);
        % ---- update legend
        legendStr{length(legendStr)+1} = ...
            ['aLIGO design '];
    end

    if max( strcmp('K1',analysis.detectorList) )  
        % ---- We have KAGRA in our network
        f = 40:ceil(dF*(size(S,1)-1));
        [Sh, fstop] = SRD('KAGRA',f);
        plot(f,Sh.^0.5,'k:','linewidth',2);
        % ---- update legend
        legendStr{length(legendStr)+1} = ...
            ['KAGRA design '];
    end

    if max( strcmp('V1',analysis.detectorList) ) 
        % ---- We have Virgo in our network
        f = 20:ceil(dF*(size(S,1)-1));
        [Sh, fstop] = SRD('aVIRGO',f);
        plot(f,Sh.^0.5,'k-.','linewidth',2);
        % ---- update legend
        legendStr{length(legendStr)+1} = ...
            ['aVIRGO design '];
    end

    hold off
    % ---- write legend
    legend(legendStr,'Location','SouthEast');
    % ---- Save figure.
    xsavefigure('detectors_spectra',figures_dirName,figfiles_dirName);

    % ---- Plot amplitude noise spectra weighted by antenna response.
    xspectrumplot(0:dF:dF*(size(S,1)-1), ...
        repmat(sqrt(FpList(:)'.^2+FcList(:)'.^2),[size(S,1),1]).^(-1).*S.^0.5, ...
        analysis.detectorList,analysis)
    % ---- Reset y label before saving.
    ylabel('amplitude spectra/sqrt(F_+^2+F_x^2)  (Hz^{-1/2})')
    xsavefigure('aweighted_detectors_spectra',figures_dirName,figfiles_dirName);

    % ---- Add these figures to the web page.
    xaddfiguretable(fout,{'detectors_spectra','aweighted_detectors_spectra'},...
                    figures_dirName,...
                    {analysis.captions.detectors_spectra,...
                     analysis.captions.aweighted_detectors_spectra});

    % ---- Add the Gaussianity measure plot, if that variable is defined.
    if (exist('gaussianity','var'))

        % ---- Make the figure.
        xspectrumplot(...
            analysis.minimumFrequency:dF:...
            (analysis.minimumFrequency+dF*(size(gaussianity,1)-1)), ...
            gaussianity,analysis.detectorList,analysis)
        % ---- Manually add extra lines and edit formatting before saving.
        hold on;
        X = [analysis.minimumFrequency analysis.maximumFrequency];    
        Yp = (2+3*sqrt(20/(analysis.blockTime-2*analysis.transientTime)...
            *analysis.analysisTimes(bATi)))*[1 1];    
        Ym = (2-3*sqrt(20/(analysis.blockTime-2*analysis.transientTime)...
            *analysis.analysisTimes(bATi)))*[1 1];
        semilogx(X,Yp,'--k','linewidth',2);
        semilogx(X,Ym,'--k','linewidth',2);
        ylabel(' Gaussianity measure')
        set(gca,'YScale','linear')
        v = axis;
        axis auto
        axis([v(1:2) get(gca,'YLim')])
        xsavefigure('detectors_gauss',figures_dirName,figfiles_dirName);

        % ---- Add the figure to the web page.
        xaddfiguretable(fout,{'detectors_gauss'},figures_dirName,...
            analysis.captions.detectors_gauss);
    end

    % ---- Add plots comparing the off-source noise spectra to the on-source noise spectra
    if ~isfield(analysis,'offSourceMatFile') || ~exist(strrep(analysis.offSourceMatFile,'merged','ASD'));
    else
        xplotasdcomparison(analysis.offSourceMatFile,analysis.onSourceMatFile,figures_dirName,figfiles_dirName);
    end

    [status,result] = system(['python $XPIPE_INSTALL_BIN/plotSearchGrid.py -d ',figures_dirName]);
    if status==0
        dummy = result;
    else
	warning('plotSearchGrid.py has failed');
	disp(status);
	disp(result);
    end

    plotAntennaNames{1} = ['FullSkyAntennaSensitivityDP'];
    Gridcaptions{1} = analysis.captions.searchgrid;
    
    % ---- Add the ASD figures to the web page.
    for iDet=1:length(analysis.detectorList)    
    	det = analysis.detectorList(iDet);
        plotASDNames{iDet} = char(strcat(det,'_ASD'));
        ASDcaptions{iDet} = analysis.captions.asd_compare;
        plotNormNames{iDet} = char(strcat(det,'_ASD_norm'));
        Normcaptions{iDet} = analysis.captions.asd_norm;
        plotAntennaNames{1+iDet} = char(strcat('FullSkyAntennaSensitivity',det));
        Gridcaptions{1+iDet} = analysis.captions.searchgrid;
    end
    xaddfiguretable(fout,plotASDNames,figures_dirName,ASDcaptions);
    xaddfiguretable(fout,plotNormNames,figures_dirName,Normcaptions);


    % ------------------------------------------------------------------------
    %    Plots of F+ and Fx projection components vs frequency.
    % ------------------------------------------------------------------------

    % ---- \hat{F}_+.^2
    xantennaplot(0:dF:dF*(size(S,1)-1),wFpDPHat.^2,analysis.detectorList,analysis)
    ylabel('(F_i)^2')
    title('normalized contribution to F+ in DP frame')
    xsavefigure('Fspectra',figures_dirName,figfiles_dirName);

    % ---- Plots that get made only if we have analysis.eCrossIndex.
    if (analysis.eCrossIndex)

        % ---- Cross projection components \hat{F}_x.^2.
        xantennaplot(0:dF:dF*(size(S,1)-1),wFcDPHat.^2,...
            analysis.detectorList,analysis)
        ylabel('(F_i)^2')
        title('normalized contribution to Fx in DP frame')
        xsavefigure('FCspectra',figures_dirName,figfiles_dirName);

        % ---- Plus and cross projection components (\hat{F}_+.^2 + \hat{F}_x.^2).
        A = (wFcDP.^2)+(wFpDP.^2);
        B = repmat(sum(A,2),1,length(analysis.detectorList));
        xantennaplot(0:dF:dF*(size(S,1)-1),A./B,analysis.detectorList,analysis)
        ylabel('(F_+^2+F_x^2)_i')
        title('normalized contribution to (F_+^2 + F_x^2) in DP frame')
        xsavefigure('FTOTspectra',figures_dirName,figfiles_dirName);

        % ---- Antenna alignment factor |wFcDP|^2/|wFpDP|^2.
        A = sum(wFcDP.^2,2);
        B = sum(wFpDP.^2,2);
        xantennaplot(0:dF:dF*(size(S,1)-1),A./B,'|Fx|^2/|F+|^2',analysis)
        ylabel('(wFx/wF+)^2')
        title('whitened |Fx|^2/|F+|^2 in DP frame')
        axis 'auto y'
        xsavefigure('Fratio',figures_dirName,figfiles_dirName);
        % -- add summary information of alignment factor to caption,
        % usefull for script parsing of the html page
        maskFreq100_200Hz = ...
            0:dF:dF*(size(S,1)-1)>100 & 0:dF:dF*(size(S,1)-1)<200;
        meanEllipticity100_200Hz = ...
            mean(A(maskFreq100_200Hz)./B(maskFreq100_200Hz));
        maskFreq1_2kHz = ...
            0:dF:dF*(size(S,1)-1)>1000 & 0:dF:dF*(size(S,1)-1)<2000;
        meanEllipticity1_2kHz = ...
            mean(A(maskFreq1_2kHz)./B(maskFreq1_2kHz));
        ellipticityCaption = ...
            [ 'Mean ellipticity in 100 - 200 Hz range !@' ...
              num2str(meanEllipticity100_200Hz) ...
              '!@, Mean ellipticity in 1 - 2 kHz range !@' ...
              num2str(meanEllipticity1_2kHz) '!@'];
    end

    % ---- Add these figures to web page.
    if (analysis.eCrossIndex)
        xaddfiguretable(fout,{'Fspectra','FCspectra','FTOTspectra','Fratio'},...
        figures_dirName,{analysis.captions.Fspectra,...
        analysis.captions.FCspectra,'',ellipticityCaption});
    else
        xaddfiguretable(fout,{'Fspectra'},figures_dirName,{analysis.captions.Fspectra});
    end

end %-- end if produceWebpageFlag

% ------------------------------------------------------------------------
%      Plots of null stream projection components vs frequency.
% ------------------------------------------------------------------------

% ---- Only make these plots if we have analysis.eNullIndex.
if (analysis.eNullIndex)

    % ---- Null stream.  Only make this plot if we have analysis.eNullIndex
    A = abs(1 - wFpDPHat.^2 - wFcDPHat.^2);

    % ---- Sensitivity in incoherent null streams
    Snull = S./A;

    if produceWebpageFlag 

        xantennaplot(0:dF:dF*(size(S,1)-1),A,analysis.detectorList,analysis)
        ylabel('(K_i)^2')
        title('normalized contribution to null streams')
        xsavefigure('Nullspectra',figures_dirName,figfiles_dirName);

        % ---- Plot sensitivity in incoherent null streams
        xspectrumplot(0:dF:dF*(size(S,1)-1),Snull.^0.5,analysis.detectorList, ...
            analysis)
        ylabel('S^{1/2}/Abs(K_i) (Hz^{-1/2})')
        title('sensitivity in incoherent null streams')
        xsavefigure('Incspectra',figures_dirName,figfiles_dirName);

        % ---- Add these null stream-related plots to web page.
        xaddfiguretable(fout,{'Nullspectra','Incspectra'},figures_dirName,...
            {analysis.captions.Nullspectra,analysis.captions.Incspectra});

    end % -- end if produceWebpageFlag
% ---- Pass out empty Snull array if we cannot construct null stream
else
    Snull = [];
end


if produceWebpageFlag 
  % ------------------------------------------------------------------------
  %      Plots of antenna pattern sky dependance
  % ------------------------------------------------------------------------
  captionsAntenna = {};
  % Compute antenna pattern factors
  trigPropertyName1 = 'relative phi';
  trigPropertyName2 = 'relative theta';
  relPhi = mod(analysis.skyPositions(:,2)...
               -analysis.skyPositions(1,2)+pi,2*pi)-pi; 
  relTheta = mod(analysis.skyPositions(:,1)...
                   -analysis.skyPositions(1,1)+pi/2,pi)-pi/2;
  for iDetector = 1:length(analysis.detectorList)
    [FpSky, FcSky, FbSky] = ComputeAntennaResponse(analysis.skyPositions(:,2), ...
                                            analysis.skyPositions(:,1),0, ...
                                            analysis.detectorList{iDetector});
    FpSkyList(:,iDetector) = FpSky;
    FcSkyList(:,iDetector) = FcSky;
    FbSkyList(:,iDetector) = FbSky;
  end
  [FpSkyDP FcSkyDP] = convertToDominantPolarizationFrame(FpSkyList,FcSkyList);
  %FbSkyDP = FbSkyList;
  FsensSky = sum(FpSkyDP.^2+FcSkyDP.^2,2);
  FellSky = sum(FcSkyDP.^2,2)./sum(FpSkyDP.^2,2);
  %FsensSkyb = sum(FbSkyDP.^2,2);
  %FellSky = sum(FcSkyDP.^2,2)./sum(FpSkyDP.^2,2);

  % --- compute plot edges
  phiMin = min(relPhi)-0.1*abs(min(relPhi))-0.02;
  phiMax = max(relPhi)+0.1*abs(max(relPhi))+0.02;
  thetaMin = min(relTheta)-0.1*abs(min(relTheta))-0.02;
  thetaMax = max(relTheta)+0.1*abs(max(relTheta))+0.02;
  X = [phiMin phiMax];
  Y = [thetaMin thetaMax];

  % Compute iso time delay lines
  [isoLines baselineNames]=xisotimedelay(analysis.skyPositions(1,:), ...
                                         analysis.detectorList);
  relIsoLinePhi = mod(squeeze(isoLines(:,:,2))'...
                      -analysis.skyPositions(1,2)+pi,2*pi)-pi;
  relIsoLineTheta =  mod(squeeze(isoLines(:,:,1))'...
                         -analysis.skyPositions(1,1)+pi/2,pi)-pi/2;
  
  % Plot antenna patteren sky dependance and iso time delay lines
  figure; set(gca,'FontSize',20);
  scatter(relPhi,relTheta,30,FsensSky,'+');colorbar;
  hold on
  plot(relIsoLinePhi,relIsoLineTheta,'x');
  xlim(X);ylim(Y)
  legend('grid',baselineNames{:})
  grid on
  title('antenna sensitivity (F_+^2 + F_x^2) in DP frame');
  xlabel(trigPropertyName1);
  ylabel(trigPropertyName2);
  plotName{1} = ['/SkyAntennaSensitivity'];
  xsavefigure(plotName{1},figures_dirName,figfiles_dirName);
  captionsAntenna = {captionsAntenna{:}, analysis.captions.antennaNetwork};

  figure; set(gca,'FontSize',20);
  scatter(relPhi,relTheta,30,FellSky,'+');colorbar;
  hold on
  plot(relIsoLinePhi,relIsoLineTheta,'x');
  xlim(X);ylim(Y)
  legend('grid',baselineNames{:})
  grid on
  title('antenna ellipticity (F_x^2/F_+^2) in DP frame');
  xlabel(trigPropertyName1);
  ylabel(trigPropertyName2);
  plotName{2} = ['/SkyAntennaEllipticity'];
  xsavefigure(plotName{2},figures_dirName,figfiles_dirName);
  captionsAntenna = {captionsAntenna{:}, analysis.captions.antennaEll};

  for iDetector = 1:length(analysis.detectorList)
    figure; set(gca,'FontSize',20);
    scatter(relPhi,relTheta,30,...
            FpSkyList(:,iDetector).^2+FcSkyList(:,iDetector).^2,'+');
    colorbar;
    hold on
    plot(relIsoLinePhi,relIsoLineTheta,'x');
    xlim(X);ylim(Y)
    legend('grid',baselineNames{:}) 
    grid on
    title(['antenna sensitivity (F_x^2+F_+^2) for ' ...
           analysis.detectorList{iDetector}]);

    xlabel(trigPropertyName1);
    ylabel(trigPropertyName2);

    plotName{2+iDetector} = ['/SkyAntennaSensitivity' ...
                        analysis.detectorList{iDetector}];
    xsavefigure(plotName{2+iDetector},figures_dirName,figfiles_dirName);
    captionsAntenna = {captionsAntenna{:}, analysis.captions.antenna};
  end
  
  % ---- Put all plots into a table.
  xaddfiguretable(fout,plotName,figures_dirName,captionsAntenna);

  % ---- Add the pylal plots of the antenna sensitivity
  xaddfiguretable(fout,plotAntennaNames,figures_dirName,Gridcaptions);
  
end % -- end if produceWebpageFlag
