function xwritedqtriggers(fout,user_tag,analysis,sigThreshold,...
                          offSourceSelectedAfterDQandWindow,...
                          offSourceSelectedAfterAllCuts,onSource,figures_dirName, ...
                          figfiles_dirName)
% XWRITEDQTRIGGERS calls XSELECTDQTRIGGERS to define the various types of
% "annoying" triggers, then makes plots of the triggers and adds those to the
% web page.
%
% $Id$

% ---- Write header info to the web page.
fprintf(fout,'%s\n','<h2>Triggers for DQ followup</h2>');
fprintf(fout,'<br>%s\n',['The threshold in significance for annoying events ' ...
    'is: ' num2str(sigThreshold)])

% ---- Determine which triggers are "annoying".
[triggersItfAnnoying] = xselectdqtriggers(fout,analysis,user_tag,sigThreshold, ...
    offSourceSelectedAfterDQandWindow,offSourceSelectedAfterAllCuts);
levelDescription = {'all triggers','annoying triggers','very annoying triggers'};

% ---- Plotting sometimes fails, but this section of the web page is not used 
%      for detections or upper limits, so encase in a try-catch block so the
%      analysis can complete even if the annoying triggers analysis fails.
try

    % ---- Convert GPS time to UTC and determine the day.
    [o dateStr] = system(['lal_tconvert -f "%Y/%m/%d" ' num2str(analysis.gpsCenterTime) ]);
    [o dayName] = system(['lal_tconvert -f "%d %b %Y" ' num2str(analysis.gpsCenterTime)]);
    [o dayStartGPSstr]=system(['lal_tconvert ' dayName]);
    dayStartGPS = str2num(dayStartGPSstr);
    dayStopGPSstr = num2str(dayStartGPS+86400);

    % ---- Loop over levels of annoying triggers and plot them.
    for iLevel=1:length(triggersItfAnnoying)
        fprintf(fout,'%s\n',['<h3> Plots of ' levelDescription{iLevel}  ' </h3>']);
        for iITF=1:length(triggersItfAnnoying{iLevel})
            iEnergy=find(strcmp(analysis.likelihoodType,['energyitf' num2str(iITF)]));
            if isempty(iEnergy)
               fprintf(fout,['<br> Skipping annoying triggers for ' ...
                   analysis.detectorList{iITF} '. Single detector energy column  ' ...
                   'not found\n'])
               continue
            end
            % ---- Scatter plot of duration vs frequency.
            figure;set(gca,'FontSize',20);
            if not(isempty(triggersItfAnnoying{iLevel}{iITF}.peakFrequency))
                scatter(triggersItfAnnoying{iLevel}{iITF}.peakFrequency, ...
                    triggersItfAnnoying{iLevel}{iITF}.boundingBox(:,3),40,...
                    sqrt(triggersItfAnnoying{iLevel}{iITF}.likelihood(:,iEnergy)),'+');grid;
            end
            set(gca,'xscale','log');set(gca,'yscale','log');
            caxis([0 50]);colorbar;
            xlim([analysis.minimumFrequency analysis.maximumFrequency])
            xlabel('Frequency (Hz)'); ylabel('Duration (sec)')
            title(['Distribution of ' levelDescription{iLevel} ' in ' ...
                analysis.detectorList{iITF}])
            plotNameHist{iITF} = ['annoyingTriggers_DvsF_Level' num2str(iLevel) '_' ...
                analysis.detectorList{iITF}];
            xsavefigure(plotNameHist{iITF},figures_dirName,figfiles_dirName);
            % ---- Scatter plot of frequency vs peak time.
            figure;set(gca,'FontSize',20);
            if not(isempty(triggersItfAnnoying{iLevel}{iITF}.peakFrequency))
                scatter((triggersItfAnnoying{iLevel}{iITF}.unslidTime(:,iITF)-...
                    dayStartGPS)/3600, ...
                    triggersItfAnnoying{iLevel}{iITF}.peakFrequency,40,...
                    sqrt(triggersItfAnnoying{iLevel}{iITF}.likelihood(:,iEnergy)),'+');grid;
            end
            set(gca,'yscale','log');
            caxis([0 50]);colorbar;
            xlabel(['Time from start of ' dayName ' UTC (hours)']);ylabel('Frequency (Hz)')
            ylim([analysis.minimumFrequency analysis.maximumFrequency])
            xlim([floor((analysis.gpsCenterTime-dayStartGPS)/3600)-2 floor((analysis.gpsCenterTime-dayStartGPS)/3600)+3])
            title(['Distribution of ' levelDescription{iLevel} ' in ' analysis.detectorList{iITF}])
            plotNameHist{iITF+length(analysis.detectorList)} = ['annoyingTriggers_FvsT_Level' num2str(iLevel) '_' ...
                analysis.detectorList{iITF}];
            xsavefigure(plotNameHist{iITF+length(analysis.detectorList)},figures_dirName,figfiles_dirName);
        end  %-- loop over detectors
        if exist('plotNameHist')
            % ---- Put all plots into a table.
            xaddfiguretable(fout,plotNameHist,figures_dirName);
            clear plotNameHist
        end 
    end %-- loop over annoying trigger level

catch

    % ---- Retrieve last error message and dump some of its contents. 
    err = lasterror;
    fprintf(fout,'%s\n</div>\n',...
            ['Collecting information about annoying triggers failed: ' err.message]);
    % ---- Print a sensible error message to the log file.
    [a,b] = err.stack.file;                          
    [c,d] = err.stack.line;
    disp(['Error in ' a ' at line ' num2str(c) '...'])
    disp(['... called in ' b ' at line ' num2str(d) '.'])

end

% ---- Done.
return


