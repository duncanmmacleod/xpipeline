function [] = xnetworkselection(detectorList, centerTimesFile, timeOffsetsFile, ...
    H1_live_file, K1_live_file, L1_live_file, G1_live_file, V1_live_file, ...
    H1_veto_file, K1_veto_file, L1_veto_file, G1_veto_file, V1_veto_file, ...
    output_file, windowBeginOffset, windowEndOffset, transientTime, windowLength)
% XNETWORKSELECTION - wrapper function for calling XAPPLYNETWORKTESTS.
%
%  usage:
%
%  [] = xnetworkselection(detectorList, centerTimesFile, timeOffsetsFile, ...
%           H1_live_file, K1_live_file, L1_live_file, G1_live_file, V1_live_file, ...
%           H1_veto_file, K1_veto_file, L1_veto_file, G1_veto_file, V1_veto_file, ...
%           output_file, windowBeginOffset, windowEndOffset, transientTime, windowLength)
%
%  detectorList             String. List of detectors we will consider for the 
%                           network. E.g.: H1L1V1. 
%  centerTimesFile          String. Name of file containing list of job center
%                           times. Size = nJobs x 1.
%  timeOffsetsFile          String. Name of file containing list of job time
%                           offsets [sec]. Size = nJobs x nIfos.
%  XX_live_file             String. Name of file containing livetime segments
%                           (typically science-cat1 times). Use 'None' if you do
%                           not want to use science segs for a particular ifo.
%                           Five of these must be supplied, in the order
%                           H1,K1,L1,G1,V1. IMPORTANT: each file must contain
%                           the four columns idx, startTime, endTime, duration
%                           for each segment.
%  XX_veto_file             String. Name of file containing veto deadtimes
%                           (typically cat2+cat4 times). Use 'None' if you do
%                           not want to apply veto segs for a particular ifo.
%                           IMPORTANT: number, order, and format must be the 
%                           same as for XX_live_file.
%  output_file              String. Name of file to which we will write the
%                           detector networks. 
%  windowBeginOffset        String. Offset [sec] between start of on-source
%                           window and trigger time, e.g., -600. 
%  windowEndOffset          String. Offset [sec] between end of on-source 
%                           window and trigger time, e.g., 60.
%  transientTime            String. Length of time [sec] discarded at each end
%                           of each analysis block, e.g., 16.
%  windowLength             String. Total amount of data (including start and
%                           end transients) that must be read to cover the
%                           on-source window with a whole number of analysis
%                           jobs:
%                             2*transientTime+jobsPerWindow*(blockTime-2*transientTime)    
%
% XNETWORKSELECTION is a wrapper function for calling XAPPLYNETWORKTESTS.
% It is intended to be compiled, allowing it to be called from the command
% line or from python scripts such as grb.py.
%
% XNETWORKSELECTION takes in lists of event times and on-source windows,
% plus lists of science mode livetime and veto deadtime segment files. It  
% uses XAPPLYNETWORKTESTS to determine which detectors satisfy the data
% quality criteria (defined in XAPPLYNETWORKTESTS) to be used for the
% analysis of each event. The list of acceptable detectors for each event
% is written to the file specified by the input argument output_file, with
% one line per event, of the form 'H1L1V1', or 'H1K1', etc. 
%
% The following additional files are also produced:
%  [output_file '.passonly'] - A copy of output_file containing only the
%            events for which all detectors in detectorList satisfy the
%            data quality criteria.
%  [centerTimesFile '.passonly'] - Copy of centerTimesFile containing only 
%            the center times for the events for which all detectors in
%            detectorList satisfy the data quality criteria.
%  [timeOffsetsFile '.passonly'] - Copy of timeOffsetsFile containing only 
%            the timeoffsets for the events for which all detectors in
%            detectorList satisfy the data quality criteria.
%
% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             Preliminaries.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(18, 18, nargin));

% ---- Convert strings into floats.
windowBeginOffset = str2num(windowBeginOffset);
windowEndOffset = str2num(windowEndOffset);
transientTime = str2num(transientTime);
windowLength = str2num(windowLength);

% ---- Cell array of all detectors we know about.
all_detectors = {'H1' 'K1' 'L1' 'G1' 'V1'};

% ---- Convert detectorList into a cell array.
%      detectorList will have format e.g., 'H1L1V1'.
detectors_considered = {};
for ifoIdx = 1:length(all_detectors)
    % ---- If an ifo is contained within detectorList add it to the 
    %      detectors_considered cell array.
    if strfind(detectorList,all_detectors{ifoIdx})
        detectors_considered = [detectors_considered all_detectors{ifoIdx}];
    end
end

% ---- Pack livetime segment files into a single convenient cell array.
live_file_list = {H1_live_file ...
                  K1_live_file ...
                  L1_live_file ...
                  G1_live_file ...
                  V1_live_file ...
                 };

% ---- Pack veto segment files into a single convenient cell array.
veto_file_list = {H1_veto_file ...
                  K1_veto_file ...
                  L1_veto_file ...
                  G1_veto_file ...
                  V1_veto_file ...
                 };

             
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          Read data from files.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Open centerTimesFile.
disp(['Reading ' centerTimesFile]);
centerTimes = load(centerTimesFile);

% ---- Open timeOffsetsFile.
disp(['Reading ' timeOffsetsFile]);
timeOffsets = load(timeOffsetsFile);

% ---- Some sanity checking.
if size(centerTimes,1)~=size(timeOffsets,1)
    error(['centerTimes and timeOffsets files should have the '...
    'same number of rows.'])
end
if length(detectors_considered)~=size(timeOffsets,2)
    error(['timeOffsets files should have a column for each detector to be '...
    'considered.'])
end

% ---- Read in segment lists for detectors_considered.
for ifoIdx = 1:length(detectors_considered)

    disp(['Reading in segments for ' detectors_considered{ifoIdx} ]);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                              Livetimes.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- Find live segment file corresponding to current ifo.
    this_live_file = live_file_list{strcmp(all_detectors,detectors_considered(ifoIdx))};

    % ---- Call function to read in segment list. 
    %      This seglist contains the columns SEGNO, START, END, DURATION.
    [this_live_seglist] = xreadseglist(this_live_file);

    % ---- Read gpsStart and duration into structure.
    live_seglist(ifoIdx).gpsStart = this_live_seglist(:,2);
    live_seglist(ifoIdx).duration = this_live_seglist(:,4);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                              Vetoes.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- Find veto segment file corresponding to current ifo.
    this_veto_file = veto_file_list{strcmp(all_detectors,detectors_considered(ifoIdx))};

    % ---- Call function to read in segment list.
    %      This seglist contains the columns SEGNO, START, END, DURATION.
    [this_veto_seglist] = xreadseglist(this_veto_file);

    % ---- Read gpsStart and duration into structure.
    veto_seglist(ifoIdx).gpsStart = this_veto_seglist(:,2);
    veto_seglist(ifoIdx).duration = this_veto_seglist(:,4);

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Apply network selection test.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

verbosity = 'quiet';

[networkCell,pass] = xapplynetworktests(detectors_considered, centerTimes, ...
                        timeOffsets, live_seglist, veto_seglist, verbosity, ...
                        windowBeginOffset, windowEndOffset, transientTime, ...
                        windowLength);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Write output files.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fid = fopen(output_file,'w');
for iJob = 1:length(networkCell)
    % ---- Construct strings from networkCell
    networkStr{iJob} = '';
    for iStr = 1:length(networkCell{iJob})
        networkStr{iJob} = [networkStr{iJob} networkCell{iJob}{iStr}];
    end
    if strcmp(networkStr{iJob},'')
        networkStr{iJob} = 'None';
    end
    fprintf(fid,'%s\n',networkStr{iJob});
end
fclose(fid);

% ---- Copies of output_file, centerTimesFile, and timeOffsetsFile
%      containing only events for which all detectors pass the data quality
%      requirements. 
fid1 = fopen([output_file '.passonly'],'w');
fid2 = fopen([centerTimesFile '.passonly'],'w');
fid3 = fopen([timeOffsetsFile '.passonly'],'w');
for iJob = 1:length(networkCell)
    % ---- Only proceed if all detectors pass.
    if prod(pass(iJob,:))
        fprintf(fid1,'%s\n',detectorList);
        fprintf(fid2,'%d\n',centerTimes(iJob));
        for iDet = 1:size(timeOffsets,2)
            fprintf(fid3,'%d ',timeOffsets(iJob,iDet));
        end
        fprintf(fid3,'\n');
    end
end
fclose(fid1);
fclose(fid2);
fclose(fid3);


% ---- Done!
return
