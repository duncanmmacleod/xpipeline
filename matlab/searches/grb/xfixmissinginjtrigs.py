#!/usr/bin/env python

"""
XFIXMISSINGINJTRIGS - check injection job output files for evidence of missing triggers
                      and rerun jobs as needed
$Id$
"""


# -------------------------------------------------------------------------
#      Setup.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, subprocess, getopt
import configparser
import h5py
import numpy


# ---- Function usage.
def usage():
    msg = """\
Usage: 
    xfixmissinginjtrigs.py [options]
    -d, --grb-dir<path>    [REQUIRED] Path to directory to be checked (absolute
                           or relative).
    --dry-run              Do not attempt to rerun jobs for missing triggers.
    --exhaustive-check     Scan injection files for all injection scales instead of only
                           one. If used then --dry-run will be enforced as the current 
                           version of the script won't write the rerun dag correctly 
                           when the --exhaustive-check option is used.
    -v, --verbose          Extra verbosity.
    -h, --help             Display this message and exit.

  e.g.,
    xfixmissinginjtrigs.py -d /home/gareth.jones/S5/GRB070508 -v
"""
    print(msg, file=sys.stderr)


# ---- Output file checking function. 
def xfixmissinginjtrigs(grb_dir,verbose=False,dry_run=False,exhaustive_check=False):

  # ---- Enforce the --dry-run option if --exhaustive-check has been requested.
  if exhaustive_check :
      dry_run = True

  if verbose:
      # ---- Status message.  Report all supplied arguments.
      print(file=sys.stdout)
      print("###########################################################################", file=sys.stdout)
      print("#    xfixmissinginjtrigs.py - checking for missing injection triggers     #", file=sys.stdout)
      print("###########################################################################", file=sys.stdout)
      print(file=sys.stdout)
      print("Parsed input arguments:", file=sys.stdout)
      print(file=sys.stdout)
      print("             GRB dir:", grb_dir, file=sys.stdout)
      print("             dry run:", dry_run, file=sys.stdout)
      print("    exhaustive check:", exhaustive_check, file=sys.stdout)
      print("        verbose flag:", verbose, file=sys.stdout)
      print(file=sys.stdout)

  # ---- Ensure directory names end in '/'.
  if not(grb_dir.endswith('/')):
      grb_dir = grb_dir + '/'

  # ---- Read grb.param file to verify whether we are using the --long-inj flag and to find the .ini file.
  long_inj_flag = False  #-- default for grb.py
  if verbose:
      print("Parsing grb.param file.", file=sys.stdout)
  try:
      param_file = open(grb_dir + 'grb.param','r')
  except:
      print("ERROR: File", grb_dir + "grb.param Cannot be opened. Exiting.", file=sys.stderr)
      sys.exit(2)
  word = param_file.readline().split()
  for ii in range(len(word)):
      if word[ii] == '--params-file' or word[ii] == '-p':
          ini_file_name = word[ii+1]
      elif word[ii] == '--long-inj':
          long_inj_flag = True

  # ---- Only continue if long_inj_flag == True, otherwise each injection is done in a single segment only
  #      and there is no need for this check.
  if long_inj_flag :

    # ---- Determine if ini_file_name contains just the file name or also the path. If 
    #      just the name then pre-pend the grb_dir to it.
    if not '/' in ini_file_name:
        ini_file_name = grb_dir + ini_file_name
    # ---- Verify that ini_file_name exists.
    if not os.path.isfile(ini_file_name):
        print("ERROR: ini file", ini_file_name, "does not exist. Exiting.", file=sys.stderr)
        sys.exit(3)
    if verbose:
        print("Found ini file:", ini_file_name, file=sys.stdout)

    # ---- Parse .ini file to determine injection sets, number of scales, and number of injections. 
    cp = configparser.ConfigParser()
    cp.read(ini_file_name)
    if cp.has_section('waveforms') :
        waveform_set = cp.options('waveforms')
    if not waveform_set:
        print("Waveform sets could not be determined. Exiting.", file=sys.stderr)
        sys.exit(8)
    if cp.has_option('injection','injectionScales'):
        injection_scales = cp.get('injection','injectionScales').split(',')
        number_of_scales = len(injection_scales)
    if not number_of_scales: 
        print("Number of injection scales could not be determined. Exiting.", file=sys.stderr)
        sys.exit(7)
    if cp.has_option('injection','injectionInterval'):
        number_of_injections = int(cp.get('injection','injectionInterval'))
        if number_of_injections < 0 :
            number_of_injections = -1 * number_of_injections
        else:
            print("This script can only handle the case of a fixed number of injections. Exiting.", file=sys.stderr)
            sys.exit(4)
    if not number_of_injections:
        print("Number of injections could not be determined. Exiting.", file=sys.stderr)
        sys.exit(5)
    if verbose: 
        print("Waveform sets:", waveform_set, file=sys.stdout) 
        print("Number of injection scales per set        :", number_of_scales, file=sys.stdout)
        print("Number of injections per set and amplitude:", number_of_injections, "\n", file=sys.stdout)

    # ---- Check that all simulation output files are present and that each contains 
    #      triggers from the correct number of data segment blocks. 
    # ---- Initialise some flags.
    trigs_missing  = False
    rerun_dag_open = False

    # ---- Remove any pre-existing rerun dag.
    if not dry_run:
        os.system('rm -rf ' + grb_dir + 'rerun_missing.dag*')

    if exhaustive_check:
        # ---- Check files for all injection scales.
        check_range = number_of_scales
        if verbose:
            print("Performing exhaustive scan of all injection files. This may take several minutes.", file=sys.stdout)
    else:
        # ---- Check files only for first injection scale.
        check_range = 1
        if verbose:
            print("Performing fast scan of all injection files for the lowest injection scale only.", file=sys.stdout)

    for inj_set in waveform_set:

        # ---- Initialise some flags.
        trigs_missing_this_set  = False

        # ---- Check the simulation output files.
        min_num_seg = numpy.zeros((number_of_injections,1), dtype=int)
        for inj_scale in range(check_range):
            # ---- Read sim file. The name will be of the form simulations_<inj_set>_<inj_scale>_0_0_merged.mat .
            sim_file  = grb_dir + 'output/' + 'simulation_' + inj_set + '_' + str(inj_scale) + '_0_0_merged.mat'
            if not os.path.isfile(sim_file):
                print("ERROR: missing injection trigger file:", sim_file, ". Exiting.", file=sys.stdout)
                sys.exit(6)
            # ---- Read file and count number of segment blocks analysed for each injection.
            if verbose:
                print("Processing", sim_file, file=sys.stdout)
            f = h5py.File(sim_file)
            clusterInj = f['clusterInj']
            for ii in range(number_of_injections):
                num_seg = len(set((f[clusterInj['realJobNumber'][0, ii]].value)[0]))
                if inj_scale == 0:  
                    min_num_seg[ii] = num_seg 
                else:
                    min_num_seg[ii] = min(min_num_seg[ii],num_seg)

        # ---- Scan dag file to count number of segments expected for each injection.
        sim_dag = grb_dir + 'grb_simulations_' + inj_set + '.dag' 
        if verbose:
            print("Scanning dag file", sim_dag, file=sys.stdout) 
        dag = open(sim_dag,'r')
        working_inj   = 1
        working_count = 0
        working_job   = []
        parent_jobs   = []
        merge_jobs    = []
        line = 'dummy' 
        while line:
            line = dag.readline()
            if line.split()[0] == 'JOB' and line.split()[2] == 'xsearch.sub' :
                # ---- Save this line and the next two (one complete job) in temporary storage.
                current_job = []
                current_job.append(line)
                current_job.append(dag.readline())
                current_job.append(dag.readline())
                inj_num = int(current_job[-1].split()[5].replace('macroargument3="','').replace('"',''))
                if inj_num == working_inj:
                    working_count = working_count + 1
                    working_job.append(current_job[0])
                    working_job.append(current_job[1])
                    working_job.append(current_job[2])
                else:
                    # ---- Found all jobs for working_inj. Check number and act if needed.
                    if min_num_seg[working_inj-1] != working_count :
                        print("     Missing injection triggers - waveform:", inj_set, " scale:", inj_scale, "   injection number:", working_inj, "    number of segments:", min_num_seg[working_inj-1], "    expected:", working_count, file=sys.stdout)
                        trigs_missing_this_set = True
                        trigs_missing          = True
                        # ---- Delete faulty .mat files from affected jobs.
                        if not dry_run :
                            os.system('rm -rf ' + grb_dir + 'output/simulations_' + inj_set + '_*/simulation_' + inj_set + '_*_0_0_' + str(working_inj) + '.mat')
                        # ---- Add these jobs to the rerun dag.
                        if not rerun_dag_open and not dry_run:
                            try:
                                rerun_dag = open(grb_dir + 'rerun_missing.dag','w')
                                rerun_dag_open = True
                            except IOError:
                                rerun_dag_open = False
                        if rerun_dag_open :
                            # ---- Write these jobs to the rerun dag.
                            for line in working_job :
                                rerun_dag.write(line)
                            # ---- Also record the parent-child relationships.
                            for ii in range(3,len(working_job),3):
                                rerun_dag.write('PARENT ' + working_job[ii-3].split()[1] + ' CHILD ' + working_job[ii].split()[1] + '\n')
                            # ---- The last job is also a parent to all the merge jobs. Record its name.
                            parent_jobs.append(working_job[len(working_job)-1].split()[1])
                    # ---- Update to current_job.
                    working_job   = current_job
                    working_inj   = inj_num
                    working_count = 1
            elif line.split()[0] == 'JOB' and line.split()[2] == 'xmerge.sub' :
                # ---- We've moved past the search jobs into the merge jobs section of the dag file.
                # ---- If this is the first merge job then we still need to check the segment count for the last search job.
                if working_job != [] :
                    if min_num_seg[working_inj-1] != working_count :
                        print("     Missing injection triggers - waveform:", inj_set, " scale:", inj_scale, "   injection number:", working_inj, "    number of segments:", min_num_seg[working_inj-1], "    expected:", working_count, file=sys.stdout)
                        trigs_missing_this_set = True
                        trigs_missing          = True
                        # ---- Delete faulty .mat files from affected jobs.
                        if not dry_run :
                            os.system('rm -rf ' + grb_dir + 'output/simulations_' + inj_set + '_*/simulation_' + inj_set + '_*_0_0_' + str(working_inj) + '.mat')
                        # ---- Add these jobs to the rerun dag.
                        if not rerun_dag_open and not dry_run:
                            try:
                                rerun_dag = open(grb_dir + 'rerun_missing.dag','w')
                                rerun_dag_open = True
                            except IOError:
                                rerun_dag_open = False
                        if rerun_dag_open :
                            # ---- Write these jobs to the rerun dag.
                            for line in working_job :
                                rerun_dag.write(line)
                            # ---- Also record the parent-child relationships.
                            for ii in range(3,len(working_job),3):
                                rerun_dag.write('PARENT ' + working_job[ii-3].split()[1] + ' CHILD ' + working_job[ii].split()[1] + '\n')
                            # ---- The last job is also a parent to all the merge jobs. Record its name.
                            parent_jobs.append(working_job[len(working_job)-1].split()[1])
                    working_job = []
                # ---- Any single injection job being rerun will require all merge jobs to
                #      be rerun. So simply copy all merge jobs to the dag. 
                if rerun_dag_open and trigs_missing_this_set :
                    rerun_dag.write(line)
                    rerun_dag.write(dag.readline())
                    rerun_dag.write(dag.readline())
                # ---- Record the merge job name for the parent-child relationships.
                merge_jobs.append(line.split()[1])
            elif line.split()[0] == 'PARENT' :
                # ---- No need to read any further - we already know all the parent-child 
                #      relationships. Short-circuit the loop.
                line = []
        # ---- Write the final parent-child relationships: all merge jobs are children of each of the parent_jobs.
        if rerun_dag_open and trigs_missing_this_set :
            rerun_dag.write('PARENT ' + ' '.join(parent_jobs) + ' CHILD ' + ' '.join(merge_jobs) + '\n')

    # ---- Close the rerun dag if is open.
    if rerun_dag_open :
        rerun_dag.close()
        # ---- Launch the dag unless the user has specified otherwise with the --dry-run option.
        if not dry_run :
            # ---- Record starting directory.
            start_dir = os.getcwd()
            # ---- Move to target directory.
            os.chdir(grb_dir)
            # ---- Launch dag.
            print("Submitting rerun dag to condor to regenerate missing files.", file=sys.stdout)
            os.system('condor_submit_dag rerun_missing.dag')
            # ---- Return to starting directory.
            os.chdir(start_dir)
    elif trigs_missing and not dry_run:
        print('\n' + "Warning: triggers missing but unable to write rerun dag. May be a permissions error?" + '\n', file=sys.stdout)

    # ---- If verbose == False and no triggers are missing then the rest of the script generates no output. 
    #      Let the user know everything is okay.
    if not trigs_missing :
        print("No injections missing.", file=sys.stdout)
        print("No jobs being rerun.", file=sys.stdout)

    if verbose:
        print(" ... finished.", file=sys.stdout)

    print(" ", file=sys.stdout) 

  else:

    print("The --long-inj option was not used in this analysis. Skipping check.", file=sys.stdout)


if __name__ == '__main__':
    # ---- This function is being executed directly as a script (as opposed to 
    #      being imported by another code). Parse the command line arguments 
    #      then run the xfixmissinginjtrigs() function.

    # -------------------------------------------------------------------------
    #      Parse the command line options.
    # -------------------------------------------------------------------------

    # ---- Assign defaults to command-line arguments.
    dry_run = False
    exhaustive_check = False
    verbose = False

    # ---- Syntax of options, as required by the getopt command.
    # ---- Short form.
    shortop = "hvd:"
    # ---- Long form.
    longop = [
        "help",
        "dry-run",
        "verbose",
        "exhaustive-check",
        "grb-dir=",
        ]

    # ---- Get command-line arguments.
    try:
        opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
    except getopt.GetoptError:
        usage()
        sys.exit(1)

    # ---- Parse command-line arguments.  Arguments are returned as strings, so 
    #      convert type as necessary.
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit(0)
        elif o in ("-d", "--grb-dir"):
            grb_dir = a      
        elif o in ("--dry-run"):
            dry_run = True   
        elif o in ("--exhaustive-check"):
            exhaustive_check = True   
        elif o in ("-v", "--verbose"):
            verbose = True   
        else:
            print("Unknown option:", o, file=sys.stderr)
            usage()
            sys.exit(1)

    # ---- Check that all required arguments are specified, else exit.
    if not grb_dir:
        print("No grb dir specified.", file=sys.stderr)
        print("Use --grb-dir to specify it.", file=sys.stderr)
        sys.exit(1)

    # ---- Call the xcheckoutputfiles() function.
    xfixmissinginjtrigs(grb_dir,verbose,dry_run,exhaustive_check) 


