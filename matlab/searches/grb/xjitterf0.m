function xjitterf0(inFile,outFile,f0Min,f0Max)
% XJITTERF0 - Randomise injection parameter f0 for DS waveforms.
%
%  inFile     String.  Name of input injection file to be modified.
%  outFile    String.  Name of output file with modified injections.
%  f0Min      String.  Minimum value of f0 range over which to randomise.
%  f0Max      String.  Maximum value of f0 range over which to randomise.
%
% $Id$

% ---- Check input.
error(nargchk(4, 4, nargin, 'struct'))
if ~ischar(f0Min)
    error('Input f0Min must be a string.');	
else
    f0Min = str2num(f0Min);
end
if ~ischar(f0Max)
    error('Input f0Max must be a string.'); 
else
    f0Max = str2num(f0Max);
end
if ~ischar(inFile) | ~ischar(outFile)
    error('Inputs inFile, outFile must strings.');
end

if isempty(strfind(inFile,'f0jitter'))
  disp(['Skipping f0 jittering, no "f0jitter" string in injection ' ...
        'file name: ' inFile]);
  return
end

% ---- Read and parse input injection file.
injection = readinjectionfile(0,1e10,inFile);
[gps_s, gps_ns, phi, theta, psi, temp_name, temp_parameters] = ...
    parseinjectionparameters(injection);
nInjection = length(gps_s);
% ---- Parser code foolishly outputs "name" and "parameters" as cell arrays
%      of 1x1 cell arrays of strings; reformat to remove extra layer of
%      cell arrays.
name = cell(nInjection,1);
parameters = cell(nInjection,1);
for ii = 1:nInjection
    name{ii} = temp_name{ii}{1};
    parameters{ii} = temp_parameters{ii}{1};
end

% ---- Location of amplitude and central frequency parameters depends on
%      the waveform type.
paramArray = cell(nInjection,1);
f0Index = zeros(nInjection,1);
for ii = 1:nInjection
    switch lower(name{ii})
        case {'ds','chirplet'}  
            % ---- Damped Sinusoid.  Parameters are
            %      [h_peak,tau,f0].
            f0Index(ii) = 3;
            paramArray{ii} = tildedelimstr2numorcell(parameters{ii});
            f0(ii) = paramArray{ii}(f0Index(ii));
        otherwise
            error(['Current version of the function can only handle ' ... 
                'DS waveform type.  See xmakewaveform.']); 
   end
end

% ---- Set the random number generator seed based on the name of the
%      input file
randn('state',sum(inFile))
rand('twister',sum(inFile))

% ---- Jitter central frequency
randf0 = f0Min + rand(nInjection,1)*(f0Max - f0Min);

% ---- Keep f0 of first injection unchanged so that web page
%      captions look sensible.
randf0(1) = f0(1);

% ---- Write down the jittered injection parameters
numData = cell(1,1);
nameData = cell(1,1);
paramData = cell(1,1);
numData{1} = [gps_s(:,1), gps_ns(:,1), phi, theta, psi];
nameData{1} = name;
paramData{1} = cell(nInjection,1);
for ii = 1:nInjection
  jitteredParam = paramArray{ii};
  jitteredParam(f0Index(ii)) = randf0(ii);  %-- reset decay time
  jitteredParamStr = num2str(jitteredParam,'%.3e~');
  paramData{1}{ii} = jitteredParamStr(1:end-1);  %-- drop last '~'
end

%----- Write jittered injection log file.
fid = fopen(outFile,'w');
for ii = 1:nInjection
  %----- Write signal type and parameters to output file.
  fprintf(fid,'%d %d %e %e %e ',numData{1}(ii,:));
  fprintf(fid,'%s ',nameData{1}{ii});
  fprintf(fid,'%s ',paramData{1}{ii});
  fprintf(fid,'\n');
end
fclose(fid);

% ---- Done.
return
