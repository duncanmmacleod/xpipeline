function xjitterspin(inFile,outFile,sstring)
% XJITTERSPIN - Generated a population spins instead of fixed values
%
% XJITTERMASS modifies an X-Pipeline formatted injection
% log file to include a distribution of spins for binary systems
%
% usage:
%
%  xjitterspin(inFile,outFile,sstring)
%
%  inFile           String.  Name of input injection file to be modified.
%  outFile          String.  Name of output file with modified injections.
%  sstring          String.  Tilde delimited string giving the 
%                   minimal and maximal spin magnitude, and the minimal
%                   value of cos(tilt angle wrt orbit axis) of
%                   the first compact object at inspiral start, then the
%                   same parameters for the second compact object. Spins
%                   are given by the adimensional spin parameter.
%
% $Id$

% ---- Check input.
error(nargchk(3, 3, nargin, 'struct'))

if ~ischar(inFile) | ~ischar(outFile)
    error('Inputs inFile, outFile must strings.');
end


% ---- convert tilde strings into parameters vectors
sParams = tildedelimstr2numorcell(sstring);
% ---- check spin parameters length
if length(sParams) ~= 6
  error(['The spin parameters tilde string is wrong. 6 parameters are ' ...
         'expected found ' num2str(length(sParams))]);
end
% --- check that parameters are sensible
if sParams(1) > sParams(2) || sParams(4) > sParams(5) || ...
      any(sParams>1)  || any(sParams([1 2 4 5])<0) || any(sParams([3 6])<-1)
  error(['Spin distribution parameters ' num2str(sParams) ' do not make sense']);
end


% ---- Read and parse input injection file.
injection = readinjectionfile(0,1e10,inFile);
[gps_s, gps_ns, phi, theta, psi, temp_name, temp_parameters] = ...
    parseinjectionparameters(injection);
nInjection = length(gps_s);
% ---- Parser code foolishly outputs "name" and "parameters" as cell arrays
%      of 1x1 cell arrays of strings; reformat to remove extra layer of
%      cell arrays.
name = cell(nInjection,1);
parameters = cell(nInjection,1);
for ii = 1:nInjection
    name{ii} = temp_name{ii}{1};
    parameters{ii} = temp_parameters{ii}{1};
end

paramArray = cell(nInjection,1);
amplIndex = zeros(nInjection,1);
for ii = 1:nInjection
    switch lower(name{ii})
        case {'lalinspiral'}  
            % ---- LAL post-Newtonian inspiral.  Parameters are
            %      [mass1,mass2,iota,dist,generator,f_lower,spin1x,spin1y,spin1z,spin2x,spin2y,spin2z,coa_phase]
            paramArray{ii} = tildedelimstr2numorcell(parameters{ii});
        otherwise
            error(['Current version of the function can only handle ' ... 
                   'lalinspiral.  See xmakewaveform.']); 
    end
end

% ---- Set the random number generator seed based on the name of the
% input file
rand('twister',sum(inFile))

% KLUDGE
% Set the first injection to no spin and Taylor waveform, better as a
% refence and SpinTaylor does not work for zero inclination
paramArray{1}{5} = 'TaylorT2threePN';
% end kludge
% don't jitter first injection, needed for post-processing reference
% jitter all other injection
for iInj = 2:nInjection
  % ---- Generating random spin parameters
  magS1 = sParams(1)+(sParams(2)-sParams(1))*rand;
  thetaS1 = acos((1-sParams(3))*rand+sParams(3));
  phiS1 = 2*pi*rand;
  s1x = magS1*sin(thetaS1)*cos(phiS1);
  s1y = magS1*sin(thetaS1)*sin(phiS1);
  s1z = magS1*cos(thetaS1);

  magS2 = sParams(1)+(sParams(2)-sParams(1))*rand;
  thetaS2 = acos((1-sParams(3))*rand+sParams(3));
  phiS2 = 2*pi*rand;
  s2x = magS2*sin(thetaS2)*cos(phiS2);
  s2y = magS2*sin(thetaS2)*sin(phiS2);
  s2z = magS2*cos(thetaS2);

  paramArray{iInj}{7} = s1x;
  paramArray{iInj}{8} = s1y;
  paramArray{iInj}{9} = s1z;
  paramArray{iInj}{10} = s2x;
  paramArray{iInj}{11} = s2y;
  paramArray{iInj}{12} = s2z;

end



% ---- Write down the jittered injection parameters
numData = cell(1,1);
nameData = cell(1,1);
paramData = cell(1,1);
numData{1} = [gps_s(:,1), gps_ns(:,1), phi, theta, psi];
nameData{1} = name;
paramData{1} = cell(nInjection,1);
for ii = 1:nInjection
   paramData{1}{ii} = numorcell2tildedelimstr(paramArray{ii});
end

%----- Write jittered injection log file.
fid = fopen(outFile,'w');
for ii = 1:nInjection
  %----- Write signal type and parameters to output file.
  fprintf(fid,'%d %d %e %e %e ',numData{1}(ii,:));
  fprintf(fid,'%s ',nameData{1}{ii});
  fprintf(fid,'%s ',paramData{1}{ii});
  fprintf(fid,'\n');
end
fclose(fid);

% ---- Done.
return
