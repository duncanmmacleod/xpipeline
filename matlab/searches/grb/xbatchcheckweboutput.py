#!/usr/bin/env python

"""
Checks existence of output files of many xmakegrbwebpage jobs
$Id$
"""


# -------------------------------------------------------------------------
#      Setup.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, subprocess, getopt, glob
import configparser

# ---- Function usage.
def usage():
    msg = """\
Usage: 
  xbatchcheckweboutput.py [options]
  -p, --parent-dir <path>     path of parent dir [REQUIRED] 
  -h, --help                  display this message and exit

e.g.,
  xbatchcheckweboutput.py -p /archive/home/gjones/S5/v3/H1H2L1/

This function checks for the existence of *closedbox.mat and *openbox.mat
as indicators of the status of the post processing of each GRB in the
parent-dir

"""
    print(msg, file=sys.stderr)


# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
params_file = None
grb_list = None
detector = []

# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hp:"
# ---- Long form.
longop = [
   "help",
   "parent-dir=",
   ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- Parse command-line arguments.  Arguments are returned as strings, so 
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-p", "--parent-dir"):
        parent_dir = a      
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not parent_dir:
    print("No parent dir specified.", file=sys.stderr)
    print("Use --parent-dir to specify it.", file=sys.stderr)
    sys.exit(1)

# ---- Status message.  Report all supplied arguments.
print(file=sys.stdout)
print("####################################################", file=sys.stdout)
print("#    Checking results of X-Pipeline GRB search     #", file=sys.stdout)
print("####################################################", file=sys.stdout)
print(file=sys.stdout)
print("Parsed input arguments:", file=sys.stdout)
print(file=sys.stdout)
print("             parent dir:", parent_dir, file=sys.stdout)
print(file=sys.stdout)

dirContents = os.listdir(parent_dir)

grbDirs = []

# ---- check dir names end in '/'
if not(parent_dir.endswith('/')):
    parent_dir = parent_dir + '/'

# ---- loop through contents of parent_dir
#      if contents name begins with GRB and is a dir add to our list
#      of grbDirs
print('We will process the following dirs:')
for idx in range(len(dirContents)):
    # look for GRB* dirs 
    if dirContents[idx].startswith('GRB') and os.path.isdir(parent_dir + dirContents[idx]):
        grbDirs.append(dirContents[idx])
        print(parent_dir + dirContents[idx])

    # look for MOCK* dirs 
    if dirContents[idx].startswith('MOCK') and os.path.isdir(parent_dir + dirContents[idx]):
        grbDirs.append(dirContents[idx])
        print(parent_dir + dirContents[idx])

print(file=sys.stdout)

closedboxResults   = []
noClosedboxResults = []
openboxResults     = []
noOpenboxResults   = []
ulboxResults       = []
noULboxResults     = []

for grbIdx in range(len(grbDirs)):

    auto_dir = parent_dir + grbDirs[grbIdx] +  '/auto_web/'
    #autoContents = os.listdir(auto_dir)

    if glob.glob(auto_dir + '*closedbox.mat'):
        closedboxResults.append(grbDirs[grbIdx])
    else: 
        noClosedboxResults.append(grbDirs[grbIdx])

    if glob.glob(auto_dir + '*openbox.mat'):
        openboxResults.append(grbDirs[grbIdx])
    else: 
        noOpenboxResults.append(grbDirs[grbIdx])

    if glob.glob(auto_dir + '*ulbox.mat'):
        ulboxResults.append(grbDirs[grbIdx])
    else: 
        noULboxResults.append(grbDirs[grbIdx])


print("Closed box results (" + str(len(closedboxResults)) + "): ", file=sys.stdout) 
for idx in range(len(closedboxResults)):
    print(closedboxResults[idx], file=sys.stdout)
print(file=sys.stdout)

print("Open box results (" + str(len(openboxResults)) + "): ", file=sys.stdout) 
for idx in range(len(openboxResults)):
    print(openboxResults[idx], file=sys.stdout)
print(file=sys.stdout)

print("UL box results (" + str(len(ulboxResults)) + "): ", file=sys.stdout) 
for idx in range(len(ulboxResults)):
    print(ulboxResults[idx], file=sys.stdout)
print(file=sys.stdout)

print("Missing closed box results (" + str(len(noClosedboxResults)) + "): ", file=sys.stdout) 
for idx in range(len(noClosedboxResults)):
    print(noClosedboxResults[idx], file=sys.stdout)
print(file=sys.stdout)

print("Missing open box results (" + str(len(noOpenboxResults)) + "): ", file=sys.stdout) 
for idx in range(len(noOpenboxResults)):
    print(noOpenboxResults[idx], file=sys.stdout)
print(file=sys.stdout)

print("Missing UL box results (" + str(len(noULboxResults)) + "): ", file=sys.stdout) 
for idx in range(len(noULboxResults)):
    print(noULboxResults[idx], file=sys.stdout)
print(file=sys.stdout)
print(" ... finished.", file=sys.stdout)


