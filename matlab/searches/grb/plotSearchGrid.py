#!/usr/bin/env python
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# This function is used to plot the antenna pattern and search grid on a matplotlib basemap projection
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import matplotlib
matplotlib.use('Agg')
#from matplotlib.font_manager import FontProperties
#from mpl_toolkits.basemap import Basemap
#Usually fails on previous line ...import Basemap
#On caltech, at least, the following seems to work.

import mpl_toolkits
mpl_toolkits.__path__.append('/usr/lib64/python3.6/site-packages/mpl_toolkits/')
from mpl_toolkits.basemap import Basemap

import os
from numpy import *
import pylab
from pylal import antenna
from optparse import OptionParser

def GPStoGMST(gps):

    # Function to convert a time in GPS seconds to Greenwich Mean Sidereal Time
    # Necessary for converting RA to longitude

    gps0 = 630763213   # Start of J2000 epoch, Jan 1 2000 at 12:00 UTC

    D = (gps-gps0)/86400.0   # Days since J2000

    d_u = floor(D)+0.5  # Days between J2000 and last 0h at Greenwich (always integer + 1/2)

    df_u = D-d_u

    T_u = d_u/36525.0
    gmst0h = 24110.54841 + 8640184.812866*T_u + 0.093104*T_u**2 - 6.2e-6*T_u**3

    gmst = gmst0h + 1.00273790935*86400*df_u

    if (gmst>=86400):
        gmst = gmst - floor(gmst/86400)*86400;

    return gmst

def radectoearth(ra,dec,gps):

    # Script to convert Right Ascension, Declination to earth-centered coordinates
    # 
    # phi is azimuthal angle (longitude), -pi <= phi < pi
    # theta is polar angle (latitude),   0 <= theta <= pi
    #
    # Inputs (RA,dec) are in degrees, RA from zero (GMT) to 360, dec from 90 (north pole) to -90 (south pole)

    gmst = GPStoGMST(gps)  # get time in sidereal day

    gmst_deg = gmst / 86400.0 * 360.0   # convert time in sidereal day to 

    phi_deg = ra - gmst_deg   # longitude is right ascension minus location of midnight relative to greenwich
    theta_deg  = 90 - dec     # latitude is the same as declination, but theta (earth-centered coordinate) is zero
    # at the north pole and pi at the south pole

    # convert to radians
    phi = phi_deg * 2 * pi / 360
    phi = mod(phi+pi,2*pi)-pi    # make sure phi is -pi to pi, not zero to 2*pi, which would make sense but whatever
    theta = theta_deg * 2 * pi / 360

    return phi, theta


usage = """usage: %prog [options]

This script will plot the antenna responses for each detector and the summed 
response for the network with the search grid overlaid.

Options:

  -a, --auto_web         [Optional] Directory containing the xgrbwebpage.param 
                         file. This is used to determine the network and sky 
                         grid. Default '.' (current directory). 
  -d, --figures_dirName  Directory to which the plots will be written. 

plotSearchGrid -a GRB200109A/auto_web/ -d GRB200109A/auto_web/GRB200109A_autogating-r5929_closedbox_figures

"""

parser = OptionParser(usage=usage)

parser.add_option("-d", "--figures_dirName", action="store", type="str",\
                      help="path to directory to save plots (relative or absolute)")

parser.add_option("-a", "--auto_web", action="store", type="str", default=".",\
                      help="path to auto_web directory (defaults to current directory)")

(options, args) = parser.parse_args()

figures_dirName = options.figures_dirName
auto_web        = options.auto_web

detectors = []


# open the post-processing parameters file to get the analysis directory, web directory, grb name, and user tag
webparams_handle = open(auto_web + '/xgrbwebpage.param')
webparams = webparams_handle.readlines()
webparams_handle.close()
webparams_list = webparams[0].split(' ')

grb_name_idx = webparams_list.index('-g')
grb_name = webparams_list[grb_name_idx + 1]

grb_dir_idx = webparams_list.index('-d')
grb_dir = webparams_list[grb_dir_idx + 1]

user_tag_idx = webparams_list.index('-u')
user_tag = webparams_list[user_tag_idx + 1]

# now that we know where the analysis directory is, we can read the analysis parameters file to get the GRB time and location

params_file = grb_dir + '/grb.param'
summary_file = grb_dir + '/grb_summary.txt'

summary_handle = open(summary_file)
summary = summary_handle.readlines()
summary_handle.close()

name, event_gps, ra, de, network, numSky, numBG, analyse = summary[-1].split('\t',8)

for i in range(len(network)/2):
    detectors.append(network[2*i:2*i+2])


gps = float(event_gps)
RA = float(ra)
dec = float(de)

# convert to earth-fixed coordinates
grb_phi, grb_theta = radectoearth(RA,dec,gps)
 
params_handle = open(params_file)
params = params_handle.readlines()
params_handle.close()
params_list = params[0].split(' ')

if '-e' in params_list:
    error_idx = params_list.index('-e')
    try:
        # ---- Normally the argument to -e is the error radius angle; convert that to a float.
        error = float(params_list[error_idx + 1])
    except:
        # ---- For IPN-style searches with externally supplied search grids, the argument will 
        #      be a file name which cannot be converted to a float. Catch this case and define
        #      a small dummy error radius.
        error = 0.01 
else:
    error = 0.01  # assume a Swift-type analysis if error is not specified to grb.py

# we only need to read the ini file if we need to get the segment lists
#ini_idx = params_list.index('-p')
#ini_file = params_list[error_idx + 1])

# read in the search grid sky locations
skypos_file = grb_dir + '/input/sky_positions_trigger_time.txt'

if os.path.isfile(skypos_file):
    search_grid = genfromtxt(skypos_file)
    if search_grid.ndim == 1:
        grid_theta = search_grid[0]
        grid_phi = search_grid[1]
    else:
        grid_theta = search_grid[:,0]
        grid_phi = search_grid[:,1]
else:
    grid_theta = grb_theta
    grid_phi = grb_phi



dRA = 2*pi/120
ddec = pi/60
RAs,decs = mgrid[0:2*pi+dRA:dRA, -pi/2:pi/2+ddec:ddec]

rows, columns = shape(RAs)

# get 95% containment radius in radians
err = 1.65*error*pi/180.0

# convert earth-based coordinates to latitude and longitude (in degrees)
grb_longitude = grb_phi*180/pi
grb_latitude = (pi/2-grb_theta)*180/pi

grid_longitude = grid_phi*180/pi
grid_latitude = (pi/2-grid_theta)*180/pi

num_points = 80

lat_error = empty(num_points)
lon_error = empty(num_points)

lat_rad = grb_latitude*pi/180
lon_rad = grb_longitude*pi/180

bearing = arange(0,2*pi,2*pi/num_points)

ifos = len(detectors)

for i in range(len(bearing)):
    lat_error[i] = arcsin( sin(lat_rad)*cos(err) + cos(lat_rad)*sin(err)*cos(bearing[i]))
    lon_error[i] = lon_rad + arctan2( sin(bearing[i])*sin(err)*cos(lat_rad), cos(err)-sin(lat_rad)*sin(lat_error[i]) )


Fp = empty(ifos)
Fc = empty(ifos)
Fa = empty(ifos)
q = empty(ifos)


F_plus = empty((rows,columns,ifos))
F_cross = empty((rows,columns,ifos))
F_avg = empty((rows,columns,ifos))

Fp_DP = empty((rows,columns,ifos))
Fc_DP = empty((rows,columns,ifos))
F_avg_DP = empty((rows,columns))
F_ellip_DP = empty((rows,columns))

psi = empty((rows,columns))
phi = empty((rows,columns))
theta = empty((rows,columns))

# Get the antenna factors for each detector across the globe, and calculate the DP frame sensitivity
for k in range(ifos):

    Fp[k], Fc[k], Fa[k], q = antenna.response(gps,RA*pi/180,dec*pi/180,0,0,'radians',detectors[k])

    # for each sky position, get the detector sensitivities to plus- and cross-polarized signals
    for i in range(rows):
        for j in range(columns):
            F_plus[i,j,k], F_cross[i,j,k], F_avg[i,j,k], q = antenna.response(gps,RAs[i,j],decs[i,j],0,0,'radians',detectors[k])



# sanity check - calculate DP quantities at GRB location
Psi = 0.25*arctan(2*(sum(Fp*Fc))/(sum(Fp**2)-sum(Fc**2)))
FpDP = cos(2*Psi)*Fp + sin(2*Psi)*Fc
FcDP = -sin(2*Psi)*Fp + cos(2*Psi)*Fc
FaDP = sum(FpDP**2 + FcDP**2)
ellip = sum(FcDP**2)/sum(FpDP**2)


# for each sky position, calculate DP rotation
# note that this does not additionally rotate psi by pi/4 if |Fp|<|Fc|
for i in range(rows):
    for j in range(columns):

        psi[i,j] = 0.25*arctan(2*(sum(F_plus[i,j,:]*F_cross[i,j,:]))/(sum(F_plus[i,j,:]**2)-sum(F_cross[i,j,:]**2)))

        Fp_DP[i,j,:] = cos(2*psi[i,j])*F_plus[i,j,:] + sin(2*psi[i,j])*F_cross[i,j,:]
        Fc_DP[i,j,:] = -sin(2*psi[i,j])*F_plus[i,j,:] + cos(2*psi[i,j])*F_cross[i,j,:]

        F_avg_DP[i,j] = sum(Fp_DP[i,j,:]**2 + Fc_DP[i,j,:]**2)
        F_ellip_DP[i,j] = sum(Fc_DP[i,j,:]**2)/sum(Fp_DP[i,j,:]**2)

        # convert RAs, decs to earth-fixed coordinates
        phi[i,j], theta[i,j] = radectoearth(RAs[i,j]*180/pi,decs[i,j]*180/pi,gps)


# generate the basemap
# resolution: c - crude, l - low, i - intermediate, h - high, f - full. 80% drop per step.
map = Basemap(projection='ortho', lat_0 = grb_latitude, lon_0 = grb_longitude,resolution = 'l', area_thresh = 1000.)

# convert antenna theta, phi to lat, lon in degrees
# longitude is from 0 to 2pi (?), latitude is from -pi/2 to pi/2
antenna_lon = phi*180/pi
antenna_lat = (pi/2-theta)*180/pi
x, y = list(map(antenna_lon,antenna_lat))
x_err, y_err = list(map(lon_error*180/pi,lat_error*180/pi))
x_grid, y_grid = list(map(grid_longitude,grid_latitude))

fignum=0

#fontP = FontProperties()
#fontP.set_size('small')

# Plot the antenna sensitivity in the DP frame
fignum=fignum+1
pylab.figure(fignum)

map.drawmapboundary()
map.drawmeridians(arange(0, 360, 30))
map.drawparallels(arange(-90, 90, 30))
map.drawcoastlines()
map.drawcountries()

CS = map.contourf(x,y,F_avg_DP,300)
map.plot(x_err,y_err,'k-',label="Error Box")
map.plot(x_grid,y_grid,'go',markersize=5,label='Search grid')
#map.colorbar(cs,location='bottom',pad="5%")

pylab.title('Network Sensitivity (F+^2 + Fx^2) in DP frame',fontsize=12)
pylab.savefig(figures_dirName + '/FullSkyAntennaSensitivityDP_thumb.png')
#pylab.legend(loc=1,prop=fontP,bbox_to_anchor=(1.1, 1.0),fancybox=True)
pylab.legend(loc=1,bbox_to_anchor=(1.1, 1.0),fancybox=True,prop={'size':12})
pylab.savefig(figures_dirName + '/FullSkyAntennaSensitivityDP.png')
pylab.close()


"""
# Plot the antenna ellipticity in the DP frame
fignum=fignum+1
pylab.figure(fignum)
    
map.drawmapboundary()
map.drawmeridians(arange(0, 360, 30))
map.drawparallels(arange(-90, 90, 30))
map.drawcoastlines()
map.drawcountries()

CS = map.contourf(x,y,F_ellip_DP,100)
map.plot(x_err,y_err,'k-',label="Error Box")
map.plot(x_grid,y_grid,'go',markersize=5,label='Search grid')
#map.colorbar(cs,location='bottom',pad="5%")
    
pylab.title('Network Ellipticity (Fx^2/Fx^2) in DP frame')
pylab.savefig(figures_dirName + '/FullSkyAntennaEllipticityDP_thumb.png')
pylab.legend(loc=1,prop=fontP,bbox_to_anchor=(1.1, 1.0),fancybox=True)
pylab.savefig(figures_dirName + '/FullSkyAntennaEllipticityDP.png')
pylab.close()
"""


for k in range(len(detectors)):

    # Plot the average sensitivity for each detector
    fignum=fignum+1
    pylab.figure(fignum)

    map.drawmapboundary()
    map.drawmeridians(arange(0, 360, 30))
    map.drawparallels(arange(-90, 90, 30))
    map.drawcoastlines()
    map.drawcountries()

    CS = map.contourf(x,y,F_avg[:,:,k],100)
    map.plot(x_err,y_err,'k-',label="Error Box")
    map.plot(x_grid,y_grid,'go',markersize=5,label='Search grid')
    #map.colorbar(cs,location='bottom',pad="5%")

    pylab.title('Antenna Sensitivity (F+^2 + Fx^2) for ' + detectors[k])
    pylab.savefig(figures_dirName + '/FullSkyAntennaSensitivity' + detectors[k] + '_thumb.png')
    #pylab.legend(loc=1,prop=fontP,bbox_to_anchor=(1.1, 1.0),fancybox=True)
    pylab.legend(loc=1,bbox_to_anchor=(1.1, 1.0),fancybox=True,prop={'size':12})
    pylab.savefig(figures_dirName + '/FullSkyAntennaSensitivity' + detectors[k] + '.png')
    pylab.close()

