trunk/matlab/searches/glitch/   

This directory holds a single script, glitch.py, which was intended to run
x-pipeline as a glitch follow-up tool.  Any desirable content should be absorbed 
into grb.py. 

Note: glitch.py is not functional; for example, the *.sub requirements for 
LDG accounting and request_disk are not implemented. 

