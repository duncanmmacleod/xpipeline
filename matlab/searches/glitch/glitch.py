#!/usr/bin/env python

"""
Script for setting up X-Pipeline glitch study analysis
Adapted from time_slides.py by X. Siemens.
$Id$
"""

# -------------------------------------------------------------------------
#      Setup.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, shutil, math, random, copy, getopt, re, string, time
import configparser, glob, operator
import numpy
from glue import segments
from glue import segmentsUtils
from glue import pipeline
from glue.lal import CacheEntry
#from pylal.date import LIGOTimeGPS

__author__ = "Patrick Sutton <psutton@ligo.caltech.edu>, Michal Was <michal.was@ens.fr>"
__date__ = "$Date: 2012-02-07 01:58:28 -0800 (Tue, 07 Feb 2012) $"
__version__ = "$Revision: 3821 $"

# ---- Function usage.
def usage():
    msg = """\
Usage: 
  grb.py [options]
  -p, --params-file <file>    Parameters (.ini) file [REQUIRED]
  -n, --grb-name <name>       Name of glitch study [REQUIRED]

  --glitch-time1 <gps>        Trigger time in first detector (GPS seconds) [REQUIRED] 
  --glitch-time2 <gps>        Trigger time in second detector (GPS seconds) [REQUIRED]
  --glitch-time-file <file>   Alternatively provide file with two
                              columns of GPS pairs to analyze [REQUIRED]
  -t, --grid-type <grid>      String. Determines what shape of sky position
                              grid will be generated.  Recognized values are 
                              'circular' (2-d grids constructed from concentric
			      circles), 'healpix' (2-d grids constructed using
			      the healpix algorithm), 'line' (1-d arc 
                              grid), and 'file' (user generated grid of point, given
                              with -e option). Default 'circular'. [OPTIONAL]
  -e, --sky-pos-err <skyerr>  1-sigma uncertainty in sky position of GRB 
                              (degrees) or file name of sky position grid to be used
                              with "-t file" option. [OPTIONAL]
  -i, --detector <ifo>        Add detector to the network [REQUIRED]
  --priority <prio>           Integer specifying the priority of condor jobs.
                              Default value is 0, higher priority jobs are
                              submitted first to the cluster. [OPTIONAL]
  --big-mem <memory>          Integer specifying the minimal memory requirement 
                              for condor jobs in MB [OPTIONAL].
  -h, --help                  Display this message and exit
"""
    print(msg, file=sys.stderr)


# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
params_file       = None
trigger_time      = None
trigger_time2     = None
trigger_time_file = None
ra                = None
decl              = None
ra2               = None
decl2             = None
detector          = []
grb_name          = None
grid_type         = None
grid_sim_file     = ''
mdc_path          = None
network_selection = False
sky_pos_err       = None
sky_pos_err2      = None
injdistrib        = None
injdistrib2       = None
condorPriority    = "0"
disableFastInjections = False
catalog_dir       = None
minimalMem        = False
mergingCutsPath   = False
mergingCutsString = ""
reUseInj          = False

# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hp:i:n:t:e:"
# ---- Long form.
longop = [
   "help",
   "params-file=",
   "detector=",
   "grb-name=",
   "grid-type=",
   "sky-pos-error",
   "priority=",
   "glitch-time1=",
   "glitch-time2=",
   "glitch-time-file=",
   "big-mem="
   ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- We will record the command line arguments to grb.py in a file called 
#      grb.param.
#      This file is used by xgrbwebpage.py which expects the short form of 
#      the options to have been used 
command_string = 'glitch.py '

# ---- Parse command-line arguments.  Arguments are returned as strings, so 
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-p", "--params-file"):
        params_file = a      
        command_string = command_string + ' -p ' + a
    elif o in ("-i", "--detector"):
        detector.append(a) 
        command_string = command_string + ' -i ' + a
    elif o in ("-n", "--grb-name"):
        grb_name = a       
        command_string = command_string + ' -n ' + a
    elif o in ("-t", "--grid-type"):
        grid_type = a       
        command_string = command_string + ' -t ' + a
    elif o in ("-e", "--sky-pos-err"):
        if grid_type == 'file':
            sky_pos_err = a
        else:
            sky_pos_err = float(a)
        command_string = command_string + ' -e ' + a
    elif o in ("--glitch-time1"):
        trigger_time = round(float(a)*512)/512
        command_string = command_string + ' --glitch-time1 ' + a
    elif o in ("--glitch-time2"):
        trigger_time2 = round(float(a)*512)/512
        command_string = command_string + ' --glitch-time2 ' + a
    elif o in ("--glitch-time-file"):
        trigger_time_file = a
        command_string = command_string + ' --glitch-time-file ' + a
    elif o in ("--big-mem"):
        minimalMem = a
        command_string = command_string + ' --big-mem ' + a
    elif o in ("--priority"):
        condorPriority = a
        command_string = command_string + ' --priority ' + a
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not params_file:
    print("No parameter file specified.", file=sys.stderr)
    print("Use --params-file to specify it.", file=sys.stderr)
    sys.exit(1)
if not trigger_time and not trigger_time_file:
    print("No trigger time specified.", file=sys.stderr)
    print("Use --glitch-time1 or --glitch-time-file to specify it.", file=sys.stderr)
    sys.exit(1)
if not trigger_time2:
    print("No second trigger time specified.", file=sys.stderr)
    print("Use --glitch-time2 to specify it.", file=sys.stderr)
    print("Assuming second trigger time equal to first one.", file=sys.stderr)
    trigger_time2 = trigger_time
if trigger_time and trigger_time_file:
    print("Options -glitch-time1 and --glitch-time-file are mutually exclusive.", file=sys.stderr)
    print("Use only one of them.", file=sys.stderr)
    sys.exit(1)
if not detector:
    print("No detectors specified.", file=sys.stderr)
    print("Use --detector to specify each detector in the network.", file=sys.stderr)
    sys.exit(1)
if not grb_name:
    print("No GRB name specified.", file=sys.stderr)
    print("Use --grb-name to specify name of GRB.", file=sys.stderr)
    sys.exit(1)
if not grid_type:
    print("No grid type specified.", file=sys.stderr)
    print("Use .--grid-type to specify it", file=sys.stderr)


# ---- grb_name should have format e.g., "GRB070201"
#      we append GRB prefix if missing unless we are analysing a 
#      MOCK GRB.
if not(grb_name.startswith('MOCK')) and not(grb_name.startswith('GRB')):
    grb_name = 'GRB' + grb_name

#------------------------------------------------------------------------------
#               Test to see if we are running on Atlas.
#------------------------------------------------------------------------------

# ---- Get partial hostname.
os.system('hostname > host.txt')
f = open('host.txt','r')
hostname=f.read()
f.close()

# ---- Get full hostame.
os.system('hostname -f > host.txt')
f = open('host.txt','r')
fullhostname=f.read()
f.close()

os.system('rm host.txt')
# ---- Check hostname and set flag if necessary.
if 'atlas' in fullhostname:
    atlasFlag = 1
elif 'h2' in hostname:
    atlasFlag = 1
elif 'coma' in fullhostname:
    atlasFlag = 1
else:
    atlasFlag = 0


#------------------------------------------------------------------------------
#             Status message.  Report all supplied arguments.
#------------------------------------------------------------------------------

print(file=sys.stdout)
print("####################################################", file=sys.stdout)
print("#              X-GRB Search Pipeline               #", file=sys.stdout)
print("####################################################", file=sys.stdout)
print(file=sys.stdout)
print("Parsed input arguments:", file=sys.stdout)
print(file=sys.stdout)
print("     parameters file:", params_file, file=sys.stdout)
if trigger_time:
    print("        trigger time:", trigger_time, file=sys.stdout)   
elif trigger_time_file:
    print("        trigger time:", trigger_time_file, file=sys.stdout)   
print("         sky pos err:", sky_pos_err, file=sys.stdout) 
print("        trigger name:", grb_name, file=sys.stdout)   
print("           grid type:", grid_type, file=sys.stdout) 
if grid_type == 'file':
    print("       grid sim file:", grid_sim_file, file=sys.stdout)
print("     condor priority:", condorPriority, file=sys.stdout)
if atlasFlag:
    print("     running on Atlas: yes", file=sys.stdout)
print(file=sys.stdout)

# ---- Write ASCII file holding grb.py command.
pfile = open('glitch.param','w')
pfile.write(command_string + "\n")
pfile.close()

# -----------------------------------------------------------------------------
#                            Preparatory.
# -----------------------------------------------------------------------------

# ---- Generate unique id tag.
os.system('uuidgen > uuidtag.txt')
f = open('uuidtag.txt','r')
uuidtag=f.read()
f.close()
os.system('rm uuidtag.txt')

# ---- Record the current working directory in a string.
cwdstr = "."

# ---- Make directory to store text files (segment lists, parameter 
#      files, etc.) that will be input to X-Pipeline.  This is done 
#      lsto minimize clutter in the working directory.
try: os.mkdir( 'input' )
except: pass  # -- Kludge: should probably fail with error message.

# ------------------------------------------------------------------------------
#                        Read configuration file.
# -----------------------------------------------------------------------------

# ---- Status message.
print("Parsing parameters (ini) file ...", file=sys.stdout)   

# ---- Check the params_file exists
if not os.path.isfile(params_file):
    print("Error: non existent parameter file: ", \
       params_file, file=sys.stderr)
    sys.exit(1)

# ---- Create configuration-file-parser object and read parameters file.
cp = configparser.ConfigParser()
cp.read(params_file)

if cp.has_option('tags','version') : 
    ini_version = cp.get('tags','version')
    print("Parameter file CVS tag:", ini_version, file=sys.stdout)

# ---- NOTE: The following reading of variables can be split up and 
#      moved into the relevant sections of the script where the 
#      variables are actually used.

# ---- Read needed variables from [parameters] and [background] sections.
background_period = int(cp.get('background','backgroundPeriod'))
blockTime         =  int(cp.get('parameters','blockTime'))
whiteningTime     =  float(cp.get('parameters','whiteningTime'))
transientTime     =  4 * whiteningTime
onSourceEndOffset   = blockTime/2-transientTime
onSourceBeginOffset = -onSourceEndOffset
onSourceTimeLength = onSourceEndOffset - onSourceBeginOffset
jobsPerWindow=int(math.ceil(float(onSourceTimeLength)/float(blockTime-2*transientTime)))
onSourceWindowLength=2*transientTime+jobsPerWindow*(blockTime-2*transientTime)
minimumFrequency = int(cp.get('parameters','minimumFrequency'));
maximumFrequency = int(cp.get('parameters','maximumFrequency'));

# ---- Interval of data to be analysed.
trigger_time_list = []
if trigger_time:
    trigger_time_list.append([trigger_time, trigger_time2])
else:
    trigger_time_list = numpy.loadtxt(trigger_time_file)
    # round to nearest sample edge which fits into float (16384Hz sampling)
    trigger_time_list = numpy.round(trigger_time_list*512)/512
start_time = [[],[]]
end_time = [[],[]]
for iTime in range(len(trigger_time_list)):
    start_time[0].append(int(trigger_time_list[iTime][0] - background_period / 2))
    end_time[0].append(int(trigger_time_list[iTime][0] + background_period / 2))
    start_time[1].append(int(trigger_time_list[iTime][1] - background_period / 2))
    end_time[1].append(int(trigger_time_list[iTime][1] + background_period / 2))
duration = int(end_time[0][0] - start_time[0][0])

# ---- We will copy all ifo data and mdc frame caches to the location
#      stored in frameCacheAll.
frameCacheAll = 'input/framecache.txt'

# ---- Read [input] channel parameters. 
detectorListLine  = cp.get('input','detectorList')
detectorList      = detectorListLine.split(',')
channelListLine   = cp.get('input','channelList')
channelList       = channelListLine.split(',')
frameTypeListLine = cp.get('input','frameTypeList')
frameTypeList     = frameTypeListLine.split(',')

# ---- Variables that may or may not be defined in .ini file:

# ---- Check for frame cache for real ifo data
try:
    dataFrameCache = cp.get('input','frameCacheFile')
except:
    print("Warning: No frameCacheFile file specified in " \
        "[input] section of configuration file.", file=sys.stdout)
    print("        A frameCache for the ifo data file will be " \
        "generated automatically.", file=sys.stdout)
    dataFrameCache = None

# ---- Check for a seed value for matlab's random number generator.
try:
    seed = int(cp.get('parameters','seed'))
except:
    seed = 931316785
    print("Warning: No seed specified in configuration file.", file=sys.stdout)
    print("         seed will be set to: ", seed, file=sys.stdout)

# ---- Matlab seed can take values between 0 and 2^31-2.
if (seed > pow(2,31)-2) or (seed < 0):
    print("Error: seed must have value between 0 and 2^31-2", file=sys.stderr)
    sys.exit(1)

# ---- Get datafind server.
datafind_server = cp.get('datafind','datafind_server')
# ---- Get datafind executable e.g., ligo_data_find.
datafind_exec = cp.get("datafind", 'datafind_exec')
# ---- Get segfind executable e.g., ligolw_segment_query.
segfind_exec = cp.get("datafind", "segfind_exec")
# ---- Get segs_from_cats executable e.g., ligolw_ligolw_segments_from_cats.
segs_from_cats_exec = cp.get("datafind", "segs_from_cats_exec")
# ---- Get ligolw_print executable e.g., ligolw_print.
ligolw_print_exec = cp.get("datafind", "ligolw_print_exec")

# ---- Status message.
print("... finished parsing parameters (ini) file.", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Validate list of detectors given.
# -------------------------------------------------------------------------

# ---- KLUDGE:TODO: Move this below automatic network selection so that we 
#      only perform this once???

# ---- Status message.
print("Comparing requested network to list of known detectors ...", file=sys.stdout)

# ---- For each detector specified with the --detector option, compare to 
#      the list of known detectors from the .ini file.  Keep only the requested
#      detectors and corresponding channel name and frame type.
#      KLUDGE:TODO: Should exit with error message if any of the detectors 
#      is not recognized.
# ---- Indices of the detectors requested for this analysis.
keepIndex = [] 
for i in range(0,len(detector)) :
    for ii in range(0,len(detectorList)) :
        if detector[i] == detectorList[ii] :
            keepIndex.append(ii)
# ---- Sort indices so that order matches that used in ini file.
keepIndex.sort()
# ---- We now have a list of the indices of the detectors requested for the 
#      analysis.  Keep only these.  Note that we over-write the 'detector'
#      list because we want to make sure the order matches the channel and 
#      frameType lists.
detector  = []
channel   = []
frameType = []
for jj in range(0,len(keepIndex)):
    detector.append(detectorList[keepIndex[jj]])
    channel.append(channelList[keepIndex[jj]])
    frameType.append(frameTypeList[keepIndex[jj]])

# ---- glitch studies only for a pair of streams is currently availble
if len(detector) != 2:
    print("Error: only 2 data stream case is cattered for", file=sys.stderr)
    sys.exit(1)



# ---- Status message.
print("... finished validating network.          ", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Retrieve single-IFO segment lists for analysis period.
# -------------------------------------------------------------------------

# ---- Write time range to a temporary segment file named gps_range.txt.
#      We'll then read this file into a ScienceData object.  It's a hack, but 
#      it seems that the only way to populate ScienceData objects is to read 
#      segments from a file.
f=open('input/gps_range.txt', 'w')
for i in range(len(detector)):
    for iTime in range(len(trigger_time_list)):
        time_range_string = '1 ' + str(start_time[i][iTime]) + ' ' + str(end_time[i][iTime]) + ' ' + str(duration) + '\n'
        f.write(time_range_string)
f.close()
# ---- Read full analysis time range back in to a ScienceData object for easy manipulation.
analysis_segment = pipeline.ScienceData()
analysis_segment.read( 'input/gps_range.txt', blockTime )
analysis_segment.coalesce()

# ---- Prepare storage for full segment lists.
full_segment_list = []
# ---- Prepare storage for lists of analysis and veto segment filenames.
analysis_seg_files = []
veto_seg_files     = []

# ---- If generateSegs ==0 then user should have supplied segment-list and
#      veto-list for each ifo used.
if int(cp.get('segfind','generateSegs'))==0:

    # ---- Loop over ifos we are considering.
    for ifoIdx in range(0,len(detector)):
        ifo = detector[ifoIdx]

        print('Retrieving analysis segment list for ', ifo)
        # ---- Get name of segment file and check that it exists.
        analysis_seg_files.append(cp.get(ifo,'segment-list'))
        if not os.path.isfile(analysis_seg_files[ifoIdx]):
            print("Error: non existent segment list file: ", \
                analysis_seg_files[ifoIdx], file=sys.stderr)
            sys.exit(1)

        print('Retrieving veto segment list for ', ifo)
        # ---- Get name of veto file and check that it exists.
        if cp.has_option(ifo, "veto-list"):
            veto_seg_files.append(cp.get(ifo, "veto-list"))
            if not os.path.isfile(veto_seg_files[ifoIdx]):
                print("Error: non existent veto list file: ", \
                    veto_seg_files[ifoIdx], file=sys.stderr)
                sys.exit(1)
        else:
            veto_seg_files.append("None")

# ---- If generateSegs ~= 0 then we will generate segment-list and veto-list 
#      for each ifo.
else:

    print('Generating veto segment lists for all ifos ')
    # ---- Set output dir for cat 1,2,3,4,5 veto files
    segs_from_cats_output_dir = "input/"
    if not(segs_from_cats_output_dir.endswith('/')):
        segs_from_cats_output_dir = segs_from_cats_output_dir + "/"

    # ---- Find global start and stop time
    start_time_global = min(min(start_time[0]),min(start_time[1]))
    end_time_global = max(max(end_time[0]),max(end_time[1]))

    # ---- Run segs_from_cats_exec to determine cat 1,2,3,4,5 vetos
    segs_from_cats_call = ' '.join([ segs_from_cats_exec,
        "--segment-url", cp.get("segs_from_cats", "segment-url"),
        "--veto-file", cp.get("segs_from_cats", "veto-file"),
        "--gps-start-time", str(start_time_global),
        "--gps-end-time", str(end_time_global),
        "--output-dir", segs_from_cats_output_dir,
        "--separate-categories"])
    if cp.has_option("segs_from_cats","dmt-file"):
        segs_from_cats_call = ' '.join([segs_from_cats_call, "--dmt-file"])
    print(segs_from_cats_call, file=sys.stdout)
    os.system(segs_from_cats_call)

    for ifoIdx in range(0,len(detector)):
        ifo = detector[ifoIdx]

        for catIdx in range(1,5):

            # ---- Construct name of veto files.
            vetocat_filename = ''.join([segs_from_cats_output_dir,ifo,
                "-VETOTIME_CAT",str(catIdx),"-",str(start_time_global),"-",str(end_time_global-start_time_global)])
            vetocat_filename_XML = vetocat_filename + ".xml"
            vetocat_filename_TXT = ''.join(["input/",ifo,"-veto-cat",str(catIdx),".txt"])

            # ---- Check file exists.
            if not(os.path.isfile(vetocat_filename_XML)):
                print("Error: Problem creating veto file:", \
                    vetocat_filename_XML, file=sys.stderr)
                sys.exit(1)

            # ---- Convert XML file to TXT format.
            segsToTxtCall = ' '.join([ ligolw_print_exec,
                "--table segment --column start_time --column end_time",
                "--delimiter ' '",
                vetocat_filename_XML,
                "| awk '{print NR  \" \" $1 \" \" $2 \" \" $2-$1}' >",
                vetocat_filename_TXT ])
            os.system(segsToTxtCall)

        print('Generating science segment list for ', ifo)
        seg_filename     = ''.join(["input/",ifo,"-seg"])
        seg_filename_XML = ''.join([seg_filename,".xml"])
        seg_filename_TXT = ''.join([seg_filename,".txt"])

        segFindCommand = ' '.join([segfind_exec,
            "--query-segments",
            "--segment-url", cp.get(ifo, "segment-url"),
            "--gps-start-time", str(start_time_global),
            "--gps-end-time", str(max(end_time)),
            "--include-segments", cp.get(ifo, "include-segments"),
            "--output-file", seg_filename_XML ])
        if cp.has_option("segfind","dmt-file"):
            segfind_call = ' '.join([segfind_call, "--dmt-file"])
        print(segFindCommand, file=sys.stdout)
        os.system(segFindCommand);
        print('... finished generating segment list.')

        print('Converting segment list from XML to TXT file: ')
        segsToTxtCall = ' '.join([ ligolw_print_exec,
            "--table segment",
            "--column start_time",
            "--column end_time",
            "--delimiter",
            "' '",
            seg_filename_XML,
            "| awk '{print NR  \" \" $1 \" \" $2 \" \" $2-$1}' >",
            seg_filename_TXT ])
        os.system(segsToTxtCall)
        print('... finished converting segment list to TXT file.')

        # ---- Read in science and veto segments. 
        sciseg   = segmentsUtils.fromsegwizard(open(seg_filename_TXT)); sciseg.coalesce()
        cat1veto = segmentsUtils.fromsegwizard(open("input/"+ifo+"-veto-cat1.txt")); cat1veto.coalesce()
        cat2veto = segmentsUtils.fromsegwizard(open("input/"+ifo+"-veto-cat2.txt")); cat2veto.coalesce()
        cat4veto = segmentsUtils.fromsegwizard(open("input/"+ifo+"-veto-cat4.txt")); cat4veto.coalesce()

        # ---- Subtract cat1veto flags from the science segments.
        sciseg_cat1 = sciseg - cat1veto; sciseg_cat1.coalesce()

        # ---- Write out newly constructed segment list.
        filename = ''.join(["input/",ifo,"_science_cat1.txt"])
        print('Writing file: ', filename)   
        f = open(filename,"w"); segmentsUtils.tosegwizard(f,sciseg_cat1); f.flush(); f.close()
        analysis_seg_files.append(filename)

        # ---- Add cat2veto and cat4veto segments for each ifo.
        cat24veto = cat2veto + cat4veto; cat24veto.coalesce()
        # ---- Write out newly constructed veto list.
        filename = ''.join(["input/",ifo,"_cat24veto.txt"])
        print('Writing file: ', filename)   
        f = open(filename,"w"); segmentsUtils.tosegwizard(f,cat24veto); f.flush(); f.close()
        veto_seg_files.append(filename)

# ---- Read in segment_lists to ScienceData object. 
for ifoIdx in range(0,len(detector)):
    ifo = detector[ifoIdx]

    # ---- Read full segment list into a ScienceData object for easy 
    #      manipulation.  Throw away science segment shorter than the 
    #      blockTime value read from the configuration file.
    print('Reading segment list from file ', analysis_seg_files[ifoIdx])
    full_segment = pipeline.ScienceData()
    full_segment.read( analysis_seg_files[ifoIdx], blockTime )
    print('... finished reading segment list.')

    # ---- Now restrict to desired analysis time range around trigger_time.
    full_segment.intersection(analysis_segment)
    # ---- Finally, append segment for this detector to the full list.
    full_segment_list.append(full_segment)

# -------------------------------------------------------------------------
#    Choose detector network based on on-source data quality.
# -------------------------------------------------------------------------

# ---- Pulled the segment lists stuff outside of the "if network_selection"
#      statement since we need these to test the off-source segments.
# ---- Get cat1 and cat2 segment files for all 5 ifos.
#      Use 'None' when no file exists.
all_detectors = ['H1', 'H2', 'L1', 'G1', 'V1']
cat1_segment_file_list = []
cat2_segment_file_list = []
for ifo in all_detectors:
    try:
        # ----- Did we consider current ifo.
        idx = detector.index(ifo)
        cat1_segment_file_list.append(analysis_seg_files[idx])
        cat2_segment_file_list.append(veto_seg_files[idx])
    except (ValueError):
        cat1_segment_file_list.append("None")
        cat2_segment_file_list.append("None")

# ---- Write string listing detectors
detectorStr = ''
detectorStrTilde = ''
for ifo in detector:
    detectorStr = detectorStr + ifo 
    detectorStrTilde = detectorStrTilde + ifo + '~'
detectorStrTilde = detectorStrTilde[0:len(detectorStrTilde)-1]

if network_selection and blockTime < 256:
    print("Warning: network selection does not currently ", \
       "work for blockTimes < 256s \n", file=sys.stderr)

if network_selection :
    print("\nSelecting ifo network from ", detector, file=sys.stdout)

    # ---- Write file containing GRB trigger time.
    ftriggertime=open('input/trigger_time.txt', 'w')
    ftriggertime.write("%f\n"%trigger_time)
    ftriggertime.close()

    # ---- Write file containing time offsets for on-source.
    ftimeoffsets=open('input/time_offsets.txt', 'w')
    for ifo in detector:
        ftimeoffsets.write("0 ")
    ftimeoffsets.close()

    network_outputFile = 'input/xnetworkselection_onsource.dat'

    xnetworkselectionCall = ' '.join([ "xnetworkselection",
        detectorStr,
        "input/trigger_time.txt",        
        "input/time_offsets.txt",
        cat1_segment_file_list[0],       
        cat1_segment_file_list[1],       
        cat1_segment_file_list[2],       
        cat1_segment_file_list[3],       
        cat1_segment_file_list[4],       
        cat2_segment_file_list[0],       
        cat2_segment_file_list[1],       
        cat2_segment_file_list[2],       
        cat2_segment_file_list[3],       
        cat2_segment_file_list[4],
        network_outputFile,
        str(onSourceBeginOffset),
        str(onSourceEndOffset),
        str(transientTime),
        str(onSourceWindowLength)
        ])

    print(xnetworkselectionCall, file=sys.stdout)
    os.system(xnetworkselectionCall)

    fnet = open(network_outputFile)
    network_selection_on = fnet.read()
    fnet.close()

    if network_selection_on.endswith("\n"):
        network_selection_on = network_selection_on[0:len(network_selection_on)-1]
    detectorStr = network_selection_on

    # ---- Figure out which if our ifos made it into the network.
    network_selection_on_list = []
    retained_ifo_indices      = []
    full_segment_list_updated = []
    channel_updated           = []
    frameType_updated         = []
    # ---- skip GRB if the network is not well determined
    if network_selection_on[0] != 'X':
        # ---- Loop over the ifos we originally considered.
        for ifoIdx in range(0,len(detector)):
            ifo = detector[ifoIdx]
            # ---- If this ifo is in our new network.
            if network_selection_on.count(ifo):
                network_selection_on_list.append(ifo) 
                full_segment_list_updated.append(full_segment_list[ifoIdx])
                retained_ifo_indices.append(ifoIdx)
                channel_updated.append(channel[ifoIdx])
                frameType_updated.append(frameType[ifoIdx])

    # ---- Update channel list.
    channel = channel_updated;
    # ---- Update frameType list.
    frameType = frameType_updated;

    # ---- Update list of segments.
    full_segment_list = full_segment_list_updated

    # ---- Update list of ifos we are using.
    detector = network_selection_on_list

# -------------------------------------------------------------------------
#          Choose appropriate likelihoods and lags for this network.
# -------------------------------------------------------------------------


print(" ", file=sys.stdout)

if len(detector) ==0:
    print("Error:No ifos in network!!!", file=sys.stderr)
    sys.exit(1)

elif len(detector) ==1:
    print("One ifo in network: ", detector, file=sys.stdout)
    lagType = []
    likelihoodType = "likelihoodType_1det1site"

elif len(detector) ==2:
    if detector.count('H1') and detector.count('H2'):
        print("Two aligned ifos in network: ", detector, file=sys.stdout)
        lagType = "lags_2det1site"
        likelihoodType = "likelihoodType_2det1site"
    else:
        print("Two misaligned ifos in network: ", detector, file=sys.stdout)
        lagType = "lags_2det2site"
        likelihoodType = "likelihoodType_2det2site"

elif len(detector) ==3:
    if detector.count('H1') and detector.count('H2'):
        print("Three ifos at two sites: ", detector, file=sys.stdout)
        lagType = "lags_3det2site"
        likelihoodType = "likelihoodType_3det2site"
    else:
        print("Three ifos at three sites: ", detector, file=sys.stdout)
        lagType = "lags_3det3site"
        likelihoodType = "likelihoodType_3det3site"

elif len(detector) ==4:
    if detector.count('H1') and detector.count('H2'):
        lagType = "lags_4det3site"
        likelihoodType = "likelihoodType_4det3site"
    else:
        print("Four ifos at four sites: ", detector, file=sys.stdout)
        lagType = "lags_4det4site"
        likelihoodType = "likelihoodType_4det4site"

elif len(detector) ==5:
    if detector.count('H1') and detector.count('H2'):
        print("Five ifos at four sites: ", detector, file=sys.stdout)
        lagType = "lags_5det4site"
        likelihoodType = "likelihoodType_5det4site"
    else:
        print("Five ifos at five sites: ", detector, file=sys.stdout)
        lagType = "lags_5det5site"
        likelihoodType = "likelihoodType_5det5site"

# -----------------------------------------------------------------------------
#               Construct tilde-separated list of sites 
# -----------------------------------------------------------------------------

# ---- Initialise tilde-separated list of sites.
siteStrTilde = ''
for iDet in range(0,len(detector)):
    # ---- Get name of current site from current detector. 
    siteTemp = detector[iDet][0]
    # ---- Add current site to list only if does not already appear in it. 
    if siteStrTilde.count(siteTemp)==0:
        siteStrTilde = '~'.join([siteStrTilde,siteTemp])

# ---- Remove preceding tilde.
siteStrTilde = siteStrTilde[1:len(siteStrTilde)] 

# -----------------------------------------------------------------------------
#               Construct tilde-separated list of detectors 
# -----------------------------------------------------------------------------

detectorStr = ''
detectorStrTilde = ''
for ifo in detector:
    detectorStr = detectorStr + ifo 
    detectorStrTilde = detectorStrTilde + ifo + '~' 
detectorStrTilde = detectorStrTilde[0:len(detectorStrTilde)-1]

frameTypeStrTilde = ''
for frameTypeName in frameType:
    frameTypeStrTilde = frameTypeStrTilde + frameTypeName + '~'
frameTypeStrTilde = frameTypeStrTilde[0:len(frameTypeStrTilde)-1]

# -----------------------------------------------------------------------------
#         Having figured out network read in appropriate lag file.
# -----------------------------------------------------------------------------

# ---- We only need to read in a lag file if our network has 2 or more ifos.
if lagType:
    try:
        lagFile =  cp.get('background',lagType)
    except:
        print("Warning: No lagFile specified in configuration file.", file=sys.stdout)
        print("         No time lag jobs will be made.", file=sys.stdout)
        lagFile =  None

    if lagFile:
        if not os.path.isfile(lagFile):
            print("Error: non existant lag file: ",lagFile, file=sys.stderr)
            sys.exit(1)
else:
    lagFile = None

print("Using lag file: ", lagFile, file=sys.stdout)


# -----------------------------------------------------------------------------
#         Having figured out network read in appropriate likelihood types.
# -----------------------------------------------------------------------------

try:
    likelihoodTypeStr =  cp.get('parameters',likelihoodType)
except:
    print("Error: required likelihoodType not specified in " \
       "configuration file.", file=sys.stderr)
    sys.exit(1)

print("Using likelihoods: ", likelihoodTypeStr, file=sys.stdout)

# -----------------------------------------------------------------------------
#      Write Matlab-formatted parameters files.
# -----------------------------------------------------------------------------

# ---- We will write three sets of parameters files:  one on-source file,
#      one off-source file, and (for each waveform set and injection scale)
#      one injections file.

# ---- Status message.
print("Writing Matlab-formatted parameter files ...", file=sys.stdout)

# ---- Write all options available in the parameters section to a file.
parameters = cp.options('parameters')

# ---- Status message.
print("... finished writing parameter files.     ", file=sys.stdout)
print(file=sys.stdout)

# -------------------------------------------------------------------------
#      Write channel file.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing Matlab-formatted channel file ...      ", file=sys.stdout)

# ---- KLUDGE: ToDo: Add channelVirtualNames as an optional parameter 
#      read from the parameter file and copied into the Matlab channel 
#      file.  This will help with analysis of simulated data.
# ---- For each detector, write the 
#      corresponding channel name and frame type to a file.
f=open('input/channels.txt', 'w')
for i in range(0,len(detector)) :
    f.write(detector[i] + ':' + channel[i] + ' ' + frameType[i] + '\n')
f.close()

# ---- Status message.
print("... finished writing channel file.        ", file=sys.stdout)
print(file=sys.stdout)

# -------------------------------------------------------------------------
#    Make coincidence segment list for on-source, zero-lag segment.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing on-source event file ...          ", file=sys.stdout)

fwin = open('input/window_on_source.txt', 'w')
for iTime in range(len(trigger_time_list)):
    trigger_time = trigger_time_list[iTime][0]
    trigger_time2 = trigger_time_list[iTime][1]
    # ---- Now make segment lists for coincidence operation.  First
    #      determine segment list for zero lag, and verify that the
    #      on-source time is contained by a segment, else quit with error.
    #      ---- On source period: +/- minimumSegmentLength / 2 around
    #      trigger time, pad by 1 second to avoid issues with non integer
    #      trigger times
    on_source_start_time = int(trigger_time + onSourceBeginOffset - transientTime-1)
    on_source_end_time = int(trigger_time + onSourceEndOffset + transientTime+1)

    # ---- Write time range to a temporary segment file named on_source_interval.txt.
    #      We'll then read this file into a ScienceData object.  It's a hack, but 
    #      it seems that the only way to populate ScienceData objects is to read 
    #      segments from a file.
    f=open('input/on_source_interval.txt', 'w')
    time_range_string = '1 ' + str(on_source_start_time) + ' ' \
        + str(int(on_source_start_time + onSourceWindowLength + 2)) + ' ' + str(blockTime+2) + '\n'
    f.write(time_range_string)
    f.close()

    # ---- Read on-source time range into a "ScienceData" object. Add 2 seconds to block time, counterpart to kludge in non integer trigger times
    on_source_segment = pipeline.ScienceData()
    on_source_segment.read( 'input/on_source_interval.txt', (blockTime+2))

    # ---- Make a time-lagged copy of the segment lists.
    lag_full_segment_list = copy.deepcopy(full_segment_list)
    # ---- Time shift segment list of second detector.
    for seg in lag_full_segment_list[1]:
        seg.set_start(seg.start()-int(trigger_time2-trigger_time))  # -- SUBTRACT lag
        seg.set_end(seg.end()-int(trigger_time2-trigger_time))  # -- SUBTRACT lag

    # ---- Now get the intersection of all of the detector segment lists with 
    #      this on-source list.
    coincidence_segment = copy.deepcopy(on_source_segment)
    for det in lag_full_segment_list:
        coincidence_segment.intersection(det)

    # ---- If on source interval is a coincidence segment, then the intersection of
    #      this interval with the "not" coincidence should be empty.
    not_coincidence_segment = copy.deepcopy(coincidence_segment)
    not_coincidence_segment.invert()
    overlap = not_coincidence_segment.intersection(on_source_segment)
    if overlap != 0:
        print("Error: on-source period is not a coincidence segment of the specified network.")
        for seg in on_source_segment:
            print("on source period:", seg.start(), " ", seg.end())
        for seg in coincidence_segment:
            print("coincidence period:", seg.start(), " ", seg.end())
        sys.exit(1)
    # write current trigger time to file
    fwin.write(str(trigger_time) + '\n')

fwin.close()


# ---- At this point, the ScienceData object on_source_segment contains the 
#      on-source zero-lag segment.  Write this to the on-source event file.
f=open('input/event_on_source.txt', 'w')


# ---- Generate sky positions for patch about ra, dec at trigger_time.
if int(cp.get('input','usexchooseskylocations')) and sky_pos_err:
    skyPosFilename_trigger = 'input/sky_positions_trigger_time.txt'

    if grid_type == 'file':
        print("Generating ", skyPosFilename_trigger, file=sys.stdout)
        xconvertfilegridCall = ' '.join(['cp',sky_pos_err,skyPosFilename_trigger])
        print(xconvertfilegridCall, file=sys.stdout)
        os.system(xconvertfilegridCall)

    elif grid_type == 'ipn':
        print("Error: IPN grids are currently unsupported. Please use circular or line instead.", file=sys.stdout)
        sys.exit(1)

    elif grid_type == 'opt':
        print("Error: Optimized position grids are currently unsupported. Please use circular or line instead.", file=sys.stdout)
        sys.exit(1)

    else:
        print("Error: Choice of sky position grid is unrecognized. Please use file.", file=sys.stdout)
        sys.exit(1)



for iTime in range(len(trigger_time_list)):
    trigger_time = trigger_time_list[iTime][0]
    trigger_time2 = trigger_time_list[iTime][1]
    # ---- Write event to event file.
    time_range_string = repr(trigger_time) + ' 0 ' + repr(trigger_time2-trigger_time) +  '\n'
    f.write(time_range_string)

    # ---- If user has set usexchooseskylocations flag to 1 in params file we now 
    #      compute a list of sky positions we need to search and write these to a 
    #      file.
    if sky_pos_err:
        skyPositionList = skyPosFilename_trigger
        # ---- Count number of sky positions.
        numSky = len(open(skyPosFilename_trigger).readlines())

    # ---- If user has not specified a sky position error we will search only a
    #      single sky position.
    else:
        skyPositionList = '[0,0]'
        # ---- Set number of sky positions.
        numSky = 1 

    # ---- Parameters file for on-source analysis.
    fParam=open('input/parameters_on_source.txt', 'w')
    # ---- First write framecache file, channel file, event file, and sky position. 
    fParam.write('channelFileName:input/channels.txt' + '\n')
    fParam.write('frameCacheFile:' + frameCacheAll + '\n')
    fParam.write('eventFileName:input/event_on_source.txt' + '\n')
    fParam.write('skyPositionList:' + skyPositionList + '\n')
    fParam.write('skyCoordinateSystem:earthfixed' + '\n')
    fParam.write('likelihoodtype:' + likelihoodTypeStr + '\n')
    # ---- Now write all of the other parameters from the parameters section.
    #      We ignore the likelihoodType_* lines since this is handled above.
    for i in range(0,len(parameters)) :
        if not(parameters[i].startswith("likelihoodtype")):
            value = cp.get('parameters',parameters[i])
            if parameters[i] == "onsourceendoffset":
                fParam.write('onsourceendoffset:' + str(onSourceEndOffset) + '\n')
            else:
                fParam.write(parameters[i] + ':' + value + '\n')
    fParam.close()

    # ---- Parameter files for on-the-fly simulated waveform analyses, if requested.
    if cp.has_section('waveforms') :
        # ---- Read [injection] parameters.  If waveform_set is empty then no 
        #      files will be written.
        waveform_set = cp.options('waveforms')
        # ---- Write one parameters file for each (waveform set, injection scale) pair.
        for set in waveform_set :
            if cp.has_section(set) & cp.has_option(set,'injectionScales') :
                # ---- This check lets you specify different injection scales and
                #      spacing for each waveform set.  
                injectionScalesList = cp.get(set,'injectionScales')
                injectionScales = injectionScalesList.split(',')
            else :
                # ---- Otherwise, use the injection scales and spacing specified in 
                #      the [injection] section.
                injectionScalesList = cp.get('injection','injectionScales')
                injectionScales = injectionScalesList.split(',')

            # ---- Write a separate parameter file for each injection scale. 
            scale_counter = 0
            for injectionScale in injectionScales :
                fParam=open("input/parameters_simulation_" + set + "_" + str(scale_counter) + "_" + str(iSeg) + ".txt", 'w')
                # ---- First write framecache file, channel file, event file, and sky position. 
                fParam.write('channelFileName:input/channels.txt' + '\n')
                fParam.write('frameCacheFile:' + frameCacheAll + '\n')
                fParam.write('eventFileName:input/event_on_source.txt' + '\n')
                fParam.write('catalogdirectory:input/' + '\n')
                fParam.write('skyPositionList:' + skyPositionList + '\n')
                fParam.write('skyCoordinateSystem:earthfixed' + '\n')
                fParam.write('likelihoodtype:' + likelihoodTypeStr + '\n')
                # ---- Now write all of the other parameters from the parameters section.
                #      We ignore the likelihoodType_* lines since this is handled above.
                for i in range(0,len(parameters)) :
                    if not(parameters[i].startswith("likelihoodtype")) :
                        value = cp.get('parameters',parameters[i])
                        if parameters[i] == "outputtype"  and value == "clusters" and not(disableFastInjections):
                            fParam.write('outputtype:injectionclusters\n')
                        elif parameters[i] == "onsourceendoffset":
                            fParam.write('onsourceendoffset:' + str(onSourceEndOffset) + '\n')
                        else:
                            fParam.write(parameters[i] + ':' + value + '\n')

                # ---- Write simulations info.  The specified injection file 
                #      will be written later.
                fParam.write('injectionFileName:input/injection_' + set + '.txt' + '\n') 
                fParam.write('injectionScale:' + str(injectionScale) + '\n')
                # fParam.write('catalogdirectory:' + XPIPELINE_ROOT  + '/waveforms\n') 
                fParam.close()
                scale_counter = scale_counter + 1

f.close()

# ---- Status message.
print("... finished writing on-source event file.", file=sys.stdout)
print(file=sys.stdout)



# -------------------------------------------------------------------------
#    Copy waveform catalogs (if specified) to input/.
# -------------------------------------------------------------------------

if catalog_dir:
    print("Copying waveform catalogs to input/ ...", file=sys.stdout)
    cpCommand = ' '.join(['cp ' + catalog_dir + '/*.mat input/'])
    os.system(cpCommand)
    print("... finished copying waveform catalogs.", file=sys.stdout)
    print(file=sys.stdout)




# -----------------------------------------------------------------------------
#                   Find data frames, if necessary.
# -----------------------------------------------------------------------------

# ---- If dataFrameCache is specified and exists we will add it to frameCacheAll
if dataFrameCache:
    # ---- check the frameCache specified actually exists 
    if not os.path.isfile(dataFrameCache): 
        print("Error: non existant framecache: ",dataFrameCache, file=sys.stderr)
        sys.exit(1)

    # ---- if the specified mdc frame cache exists then concat it other frame caches
    command = 'cat ' + dataFrameCache + '  >> ' + frameCacheAll
    os.system(command)

# ---- If dataFrameCache is not specified in the config file, then call dataFind 
#      for each detector, and convert to readframedata-formatted framecache file.
else:
    # ---- Status message.
    print("Writing framecache file for ifo data...", file=sys.stdout)
    os.system('rm -f framecache_temp.txt')
    # ---- Loop over detectors.
    for i in range(0,len(detector)):
        # ---- Construct dataFind command.
        dataFindCommand = ' '.join([datafind_exec, \
        "--server", datafind_server, \
        "--observatory", detector[i][0], \
        "--type", frameType[i], \
        "--gps-start-time", str(min(start_time[i])),  \
        "--gps-end-time", str(max(end_time[i])),    \
        "--url-type file", "--gaps", "--lal-cache > lalcache.txt 2> ligo_data_find.err"])
        # ---- Issue dataFind command.
        print("calling dataFind:", dataFindCommand)
        os.system(dataFindCommand)
        os.system('cat ligo_data_find.err') 
        print("... finished call to dataFind.")
        # ---- Convert lalframecache file to readframedata format.
        print("calling convertlalcache:")
        os.system('convertlalcache.pl lalcache.txt framecache_temp.txt')
        os.system('cat framecache_temp.txt >> ' + frameCacheAll)
        print("... finished call to convertlalcache.")

        # ---- Check that none of the missing frames overlap with
        #      our analysis segment lists.
        # ---- Read stderr log of ligo_data_find
        f = open('ligo_data_find.err','r')
        gaps_str=f.read()
        f.close()
        os.system('rm ligo_data_find.err')

        # ---- If there are any missing frames see if they overlap
        #      with our analysis segments.
        if gaps_str:
            gaps_str = gaps_str.replace('segment','segments.segment')
            # ---- Strip out segment list from the strerr log of ligo_data_find
            gaps_segments_str = 'segments.segmentlist(' + gaps_str[ gaps_str.index('[') : gaps_str.index(']')+1 ] + ')'
            # ---- Read gap segments into segment list
            gaps_segments = eval(gaps_segments_str)
            gaps_segments.coalesce()
            # ---- Need to write out these gap segments so we can read them
            #      in again as ScienceData object, we really should choose
            #      one type of segment object and use it everywhere.
            f = open("gaps.txt","w"); segmentsUtils.tosegwizard(f,gaps_segments); f.flush(); f.close()
            gaps_segments = pipeline.ScienceData()
            gaps_segments.read( 'gaps.txt', 0 )
            os.system('rm gaps.txt')
            # ---- Identify any overlap between missing frames and full_segment_list
            gaps_segments.intersection(full_segment_list[i])
            if gaps_segments: 
                print("Error: missing frames in ", detector[i], file=sys.stderr)
                for jj in range(gaps_segments.__len__()):   
                    time_range_string = str(jj) + ' ' + \
                      str(gaps_segments.__getitem__(jj).start()) \
                      + ' ' + str(gaps_segments.__getitem__(jj).end())  \
                      + ' ' + str(gaps_segments.__getitem__(jj).end() \
                      -gaps_segments.__getitem__(jj).start())
                    print(time_range_string)
                sys.exit(1)

    # ---- Clean up.
    os.system('rm -f framecache_temp.txt lalcache.txt')
    # ---- Set dataFrameCache variable to point to our new file.
    dataFrameCache = frameCacheAll
    # ---- Status message.
    print("... finished writing framecache file for ifo data", file=sys.stdout)
    print(file=sys.stdout)

# -------------------------------------------------------------------------
#      Write injection files for on-the-fly simulations, if needed.
# -------------------------------------------------------------------------

if cp.has_section('waveforms') :
    # ---- Read [injection] parameters. 
    waveform_set = cp.options('waveforms')
    injectionInterval = cp.get('injection','injectionInterval')
    # ---- If we have second error circle, then adjust injectionInterval by a 
    #      factor of 2 to put half the injections in each circle.
    if ra2 and decl2: 
        injectionInterval_num = float(injectionInterval)
        if injectionInterval_num > 0: 
            injectionInterval_num = 2 * injectionInterval_num 
        elif injectionInterval_num < 0: 
            injectionInterval_num = int(injectionInterval_num/2)
        else:
            print("Error: injectionInterval == 0", file=sys.stderr)
            sys.exit(1)
        injectionInterval = str(injectionInterval_num)
    print("Making injection files ...", file=sys.stdout)
    for set in waveform_set :
        waveforms = cp.get('waveforms',set)
        injfilename = "input/injection_" + set + ".txt"
        # ---- Construct command to make injection file for this waveform set.
        if ra2 and decl2: 
            # ---- Call injection code twice, once for each error circle.
            # ---- Command to make injection file for first error circle.
            make_injection_file_command = ' '.join(["xmakegrbinjectionfile",
            "circle1.txt", waveforms,
            str(on_source_start_time + transientTime),
            str(on_source_end_time - transientTime),
            injectionInterval, str(ra), str(decl), str(seed), grid_type, grid_sim_file ])
            # ---- Issue command to make injection file for this waveform set.
            print("make_injection_file_command:", make_injection_file_command, file=sys.stdout)
            os.system(make_injection_file_command)
            # ---- Make and issue command to jitter injection file.
            #      Note that for the two-circle case we assume the sky position
            #      to be lognormal distributed.  If injdistrib, injdistrib2 are
            #      not specified then no jittering is done.
            if int(cp.get('injection','jitterInjections'))==1 and injdistrib:
                jitter_injection_command = ' '.join(["xjitterinjectionskypositions",
                "circle1.txt",
                "circle1.txt",
                injdistrib ])
                print("    Jittering: ", "circle1.txt", file=sys.stdout)
                print(jitter_injection_command, file=sys.stdout)
                os.system(jitter_injection_command)
            # ---- Command to make injection file for second error circle.
            make_injection_file_command = ' '.join(["xmakegrbinjectionfile",
            "circle2.txt", waveforms,
            str(on_source_start_time + transientTime),
            str(on_source_end_time - transientTime),
            injectionInterval, str(ra2), str(decl2), str(seed+1), grid_type, grid_sim_file ])
            # ---- Issue command to make injection file for this waveform set.
            print("make_injection_file_command:", make_injection_file_command, file=sys.stdout)
            os.system(make_injection_file_command)
            # ---- Make and issue command to jitter injection file.
            #      Note that for the two-circle case we assume the sky position
            #      to be lognormal distributed.  If injdistrib, injdistrib2 are
            #      not specified then no jittering is done.
            if int(cp.get('injection','jitterInjections'))==1 and injdistrib2:
                jitter_injection_command = ' '.join(["xjitterinjectionskypositions",
                "circle2.txt",
                "circle2.txt",
                injdistrib2 ])
                print("    Jittering: ", "circle2.txt", file=sys.stdout)
                print(jitter_injection_command, file=sys.stdout)
                os.system(jitter_injection_command)
            # ---- Combine into a single file.
            combine_command = ' '.join(["sort -m circle1.txt circle2.txt >", injfilename ])
            os.system(combine_command)
            os.system("rm circle1.txt circle2.txt")
        else :
            make_injection_file_command = ' '.join(["xmakegrbinjectionfile",
            injfilename, waveforms,
            str(on_source_start_time + transientTime),
            str(on_source_end_time - transientTime),
            injectionInterval, str(ra), str(decl), str(seed), grid_type, grid_sim_file ])
            # ---- Issue command to make injection file for this waveform set.
            # ---- We'll overwrite this file later with miscalibrated and/or 
            #      jittered versions, if desired. 
            print("    Writing :", injfilename, file=sys.stdout)
            os.system(make_injection_file_command)
            if int(cp.get('injection','jitterInjections'))==1 and injdistrib:
                jitter_injection_command = ' '.join(["xjitterinjectionskypositions",
                injfilename,
                injfilename,
                injdistrib ])
                print("    Jittering: ", injfilename, file=sys.stdout)
                print(jitter_injection_command, file=sys.stdout)
                os.system(jitter_injection_command)

        if cp.has_option('injection','jitterInclination') :
            if int(cp.get('injection','jitterInclination'))==1:
                # ---- Read inclination angle from file name. If none found fall 
                #      back to hardcoded 5 degrees.
                inclStartPos = injfilename.find("incljitter")
                if inclStartPos >= 0:
                    inclEndPos = inclStartPos + len("incljitter")
                    if inclEndPos+4 < len(injfilename) :
                        sigmaIncl_jitter = str(float(injfilename[inclEndPos:-4])/180*math.pi)
                    else :
                        sigmaIncl_jitter = str(0.0873) # hard coded 5 degree error
                else :
                    sigmaIncl_jitter = "0"
                print(sigmaIncl_jitter, file=sys.stdout)
                jitter_inclination_command = ' '.join(["xjitterinclinationgrbinjectionfile",
                                                       injfilename,
                                                       injfilename,
                                                       sigmaIncl_jitter ])                   
                print("    Jittering inclination:", injfilename, file=sys.stdout)
                print(jitter_inclination_command, file=sys.stdout)
                os.system(jitter_inclination_command)

        if cp.has_option('injection','jitterh_rss') :
            if int(cp.get('injection','jitterh_rss'))==1:
                h_rssmin = str(cp.get('injection','h_rssMin'))
                h_rssmax = str(cp.get('injection','h_rssMax'))
                jitter_h_rss_command = ' '.join(["xjitterh_rss",
                                                       injfilename,
                                                       injfilename,
                                                       h_rssmin, h_rssmax ])
                print("    Jittering h_rss:", injfilename, file=sys.stdout)
                print(jitter_h_rss_command, file=sys.stdout)
                os.system(jitter_h_rss_command)


        if cp.has_option('injection','jitterTau') :
            if int(cp.get('injection','jitterTau'))==1:
                taumin = str(cp.get('injection','tauMin'))
                taumax = str(cp.get('injection','tauMax'))
                jitter_tau_command = ' '.join(["xjittertau",
                                                       injfilename,
                                                       injfilename,
                                                       taumin, taumax ])                   
                print("    Jittering tau:", injfilename, file=sys.stdout)
                print(jitter_tau_command, file=sys.stdout)
                os.system(jitter_tau_command)

        if cp.has_option('injection','jitterf0') :
            if int(cp.get('injection','jitterf0'))==1:
                f0min = str(cp.get('injection','f0Min'))
                f0max = str(cp.get('injection','f0Max'))
                jitter_f0_command = ' '.join(["xjitterf0",
                                                       injfilename,
                                                       injfilename,
                                                       f0min, f0max ])
                print("    Jittering f0:", injfilename, file=sys.stdout)
                print(jitter_f0_command, file=sys.stdout)
                os.system(jitter_f0_command)

        if cp.has_option('injection','jitterMass') :
            if int(cp.get('injection','jitterMass'))==1:
                if cp.has_option('injection','mass'+set):
                    mstring = cp.get('injection','mass'+set)
                    jitter_mass_command = ' '.join(["xjittermass",
                                                  injfilename,
                                                  injfilename,
                                                  mstring ])
                    print(" Jittering mass:", injfilename, file=sys.stdout)
                    print(jitter_mass_command, file=sys.stdout)
                    os.system(jitter_mass_command)

        if cp.has_option('injection','jitterSpin') :
            if int(cp.get('injection','jitterSpin'))==1:
                if cp.has_option('injection','spin'+set):
                    sstring = cp.get('injection','spin'+set)
                    jitter_spin_command = ' '.join(["xjitterspin",
                                                  injfilename,
                                                  injfilename,
                                                  sstring ])
                    print(" Jittering spin:", injfilename, file=sys.stdout)
                    print(jitter_spin_command, file=sys.stdout)
                    os.system(jitter_spin_command)

        # ---- Apply calib uncertainties to injections if required. 
        if int(cp.get('injection','miscalibrateInjections'))==1:
            miscalib_injection_command = ' '.join(["xmiscalibrategrbinjectionfile", 
            injfilename, 
            injfilename, 
            detectorStrTilde,
            frameTypeStrTilde,
            '0' ]) 
            print("    Miscalibrating :", injfilename, file=sys.stdout)
            print(miscalib_injection_command, file=sys.stdout)
            os.system(miscalib_injection_command)

    print("... finished making injection files.", file=sys.stdout)
    print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Define special job classes.
# -------------------------------------------------------------------------

class XsearchJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An x search job
    """
    def __init__(self,cp):
        """
        cp = ConfigParser object from which options are read.
        """
        # ---- Get path to executable from parameters file.
        # self.__executable = cp.get('condor','xsearch')
        # ---- Get path to executable.
        os.system('which xdetection > path_file.txt')
        f = open('path_file.txt','r')
        xdetectionstr = f.read()
        f.close()
        os.system('rm path_file.txt')
        self.__executable = xdetectionstr 
        # ---- Get condor universe from parameters file.
        self.__universe = cp.get('condor','universe')
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)
        self.__param_file = None

        # ---- Add required environment variables.
        self.add_condor_cmd('environment',"USER=$ENV(USER);HOME=$ENV(HOME);" \
            "LD_LIBRARY_PATH=$ENV(LD_LIBRARY_PATH);" \
            "XPIPE_INSTALL_BIN=$ENV(XPIPE_INSTALL_BIN)")

        # ---- Add priority specification
        self.add_condor_cmd('priority',condorPriority)

        # --- add minimal memory needed 
        if minimalMem :
            self.add_condor_cmd('request_memory',minimalMem)
        else :
            minimalMemSearch = "1000"
            self.add_condor_cmd('request_memory',minimalMemSearch)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xsearch-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xsearch-$(cluster)-$(process).err')

        # If on Atlas, use getenv=true to pass variables
        if atlasFlag:
            self.add_condor_cmd('getenv',"true")

        # ---- Name of condor job submission file to be written.
        self.set_sub_file('xsearch.sub')

class XsearchNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    xsearch node
    """
    def __init__(self,job):
        """
        job = A CondorDAGJob.
        """
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)
        self.__x_jobnum = None
        self.__x_injnum = None

    # ---- Set parameters file.
    def set_param_file(self,path):
        self.add_var_arg(path)
        self.__param_file = path

    def get_param_file(self):
        return self.__param_file

    def set_x_jobnum(self,n):
        self.add_var_arg(str(n))
        self.__x_jobnum = n

    def get_x_jobnum(self):
        return self.__x_jobnum

    def set_output_dir(self,path):
        self.add_var_arg(path)
        self.__output_dir = path

    def get_output_dir(self,path):
        return self.__output_dir

    def set_x_injnum(self,n):
        self.add_var_arg(n)
        self.__x_injnum = n

    def get_x_injnum(self):
        return self.__x_injnum

class XmergeJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An x merge job
    """
    def __init__(self,cp):
        """
        cp = ConfigParser object from which options are read.
        """
        # ---- Get path to executable.
        os.system('which xmergegrbresults > path_file.txt')
        f = open('path_file.txt','r')
        xmergestr = f.read()
        f.close()
        os.system('rm path_file.txt')
        self.__executable = xmergestr 
        # ---- Get condor universe from parameters file.
        self.__universe = cp.get('condor','universe')
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)
        # ---- Add name of 'output' directory as first argument.
        self.add_arg('output')
        # ---- Add required environment variables.
        self.add_condor_cmd('environment',"USER=$ENV(USER);HOME=$ENV(HOME);" \
            "LD_LIBRARY_PATH=$ENV(LD_LIBRARY_PATH)")

        # ---- Add priority specification
        self.add_condor_cmd('priority',condorPriority)

        # --- add minimal memory needed 
        if minimalMem :
            self.add_condor_cmd('request_memory',minimalMem)
        else :
            minimalMemMerge = "1500"
            self.add_condor_cmd('request_memory',minimalMemMerge)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xmerge-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xmerge-$(cluster)-$(process).err')

        # If on Atlas
        if atlasFlag:
            self.add_condor_cmd('getenv',"true")

        # ---- Name of condor job submission file to be written.
        self.set_sub_file('xmerge.sub')

class XmergeNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    merge results that were cut into blocks of less than maxInjNum jobs
    """
    def __init__(self,job):
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)

    def set_dir_prefix(self,path):
        self.add_var_arg(path)
        self.__dir_prefix = path

    def get_dir_prefix(self):
        return self.__dir_prefix

    def set_sn_flag(self,path):
        self.add_var_arg(path)
        self.__sn_flag = path

    def get_sn_flag(self):
        return self.__sn_flag


# -------------------------------------------------------------------------
#    Preparations for writing dags.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing job submission files ... ", file=sys.stdout)

# ---- Retrieve job retry number from parameters file.
if cp.has_option('condor','retryNumber'):
    retryNumber = int(cp.get('condor','retryNumber'))
else :
    retryNumber = 0

# ---- DAGman log file.
#      The path to the log file for condor log messages. DAGman reads this
#      file to find the state of the condor jobs that it is watching. It
#      must be on a local file system (not in your home directory) as file
#      locking does not work on a network file system.
log_file_on_source = cp.get('condor','dagman_log_on_source')
if cp.has_section('waveforms') :
    log_file_simulations = cp.get('condor','dagman_log_simulations')
if cp.has_option('mdc','mdc_sets') :
    log_file_mdcs = cp.get('condor','dagman_log_mdcs')

# ---- Make directories to store the log files and error messages 
#      from the nodes in the DAG
try: os.mkdir( 'logs' )
except: pass
# ---- Make directory to store the output of our test job.
#      NOT TO BE DONE FOR PRODUCTION RUNS!
try: os.mkdir( 'output' )
except: pass
try: os.mkdir( 'output/on_source' )
except: pass
# ---- Make directories to hold results of injection runs - one for each 
#      waveform and injection scale.
if cp.has_section('waveforms') :
    waveform_set = cp.options('waveforms')
    for set in waveform_set :
        # ---- The user can specify different injection scales for each 
        #      waveform set by including a section with that name in the ini 
        #      file.  Look for one.  If no special section for this waveform, 
        #      then use the default injection scales from [injection].  
        if cp.has_section(set) & cp.has_option(set,'injectionScales') :
            injectionScalesList = cp.get(set,'injectionScales')
            injectionScales = injectionScalesList.split(',')
        else :
            injectionScalesList = cp.get('injection','injectionScales')
            injectionScales = injectionScalesList.split(',')
        scale_counter = 0
        for injectionScale in injectionScales :
            try: os.mkdir( 'output/simulations_' + set + '_' + str(scale_counter) )
            except: pass
            scale_counter = scale_counter + 1


# -------------------------------------------------------------------------
#      Write on-source dag.
# -------------------------------------------------------------------------

# ---- Create a dag to which we can add jobs.
dag = pipeline.CondorDAG(log_file_on_source + uuidtag)

# ---- Set the name of the file that will contain the DAG.
dag.set_dag_file( 'grb_on_source' )

# ---- Make instance of XsearchJob.
job = XsearchJob(cp)

# ---- Make instance of XmergeJob. 
mergejob = XmergeJob(cp)
# ---- Put a single merge job node in mergejob.  This job will combine 
#      into a single file the output of all of the off-source analysis jobs.
mergenode = XmergeNode(mergejob)

# ---- Read the number of chunks from the event list
event_on_file = open('input/event_on_source.txt')
event_on_list = event_on_file.readlines()
nOnEvents = len(event_on_list)
event_on_file.close()

# ---- Check how many segments are to be bundled into each job, and create 
#      job nodes accordingly.
maxInjNum = int(cp.get('output','maxInjNum'))
if cp.has_option('output','maxOffNum'):
    maxOffNum = int(cp.get('output','maxOffNum'))
else :
    maxOffNum = maxInjNum

if maxOffNum == 0 :
    # ---- Each segment is to be analysed as a separate job.
    # ---- Add one node to the job for each segment to be analysed.
    for i in range(nOnEvents):
        node = XsearchNode(job)
        # ---- Parameters file:
        matlab_param_file = cwdstr + "/input/parameters_on_source.txt"
        node.set_param_file(matlab_param_file)
        node.set_x_jobnum(i)
        # ---- On-source results files are always written to the local 
        #      output/on_source directory - there are very few such jobs.
        node.set_output_dir( os.path.join( cwdstr + "/output/on_source" ) )
        node.set_x_injnum('0')
        mergenode.add_parent(node)
        node.set_retry(retryNumber)
        # ---- Prepend human readable description to node name.
        node.set_name("xdetection_on_source_seg" + str(i) + "_" + node.get_name())
        dag.add_node(node)
else:
    # ---- This option bundles together segments so that maxOffNum segments 
    #      are analysed by each condor job node.
    nSets = int(math.ceil(float(nOnEvents)/float(maxOffNum)))
    for i in range(nSets) :
        node = XsearchNode(job)
        # ---- Parameters file:
        matlab_param_file = cwdstr + "/input/parameters_on_source.txt"
        node.set_param_file(matlab_param_file)
        node.set_x_jobnum("%d-"%(i*maxOffNum) + "%d"%(min((i+1)*maxOffNum-1,nOnEvents-1)))
        # ---- On-source results files are always written to the local 
        #      output/on_source directory - there are very few such jobs.
        node.set_output_dir( os.path.join( cwdstr + "/output/on_source" ) )
        node.set_x_injnum('0')
        mergenode.add_parent(node)
        node.set_retry(retryNumber)
        # ---- Prepend human readable description to node name.
        node.set_name("xdetection_on_source_seg" + str(i) + "_" + node.get_name())
        dag.add_node(node)

# ---- Supply remaining XmergeJob node parameters, add job to the dag.
mergenode.set_dir_prefix("on_source/")
mergenode.set_sn_flag("0 " + mergingCutsString)
mergenode.set_retry(retryNumber)
# ---- Prepend human readable description to node name.
mergenode.set_name("xmerge_on_source_" + mergenode.get_name())
dag.add_node(mergenode)


# ---- Write out the submit files needed by condor.
dag.write_sub_files()
# ---- Write out the DAG itself.
dag.write_dag()
# ---- Delete used dag job
del dag
del job
del mergejob



# -------------------------------------------------------------------------
#      Write on-the-fly simulations dags - one for each waveform set.
# -------------------------------------------------------------------------

# ---- All injection scales for a given waveform set will be handled by a 
#      single dag.
if cp.has_section('waveforms') :

    # ---- Read [injection] parameters. 
    waveform_set = cp.options('waveforms')

    # ---- Read how distribute results on node and write to file
    distributeSimulation = int(cp.get('output','distributeSimulation'))

    if distributeSimulation == 1 :
        nodePath = cp.get('output','nodePath')
        onNodeSimulationPath = cp.get('output','onNodeSimulationPath')
        nNodes = int(cp.get('output','nNodes'))
        jobNodeFileSimulationPrefix = cp.get('output','jobNodeFileSimulationPrefix')
        nodeOffset = int(cp.get('output','numberOfFirstNode'));
        jobNumber = nodeOffset-1

    # ---- Write one dag for each waveform set.
    for set in waveform_set :
        # ---- The user can specify different injection scales for each 
        #      waveform set by including a section with that name in the ini 
        #      file.  Look for one.  If no special section for this waveform, 
        #      then use the default injection scales from [injection].  
        if cp.has_section(set) & cp.has_option(set,'injectionScales') :
            injectionScalesList = cp.get(set,'injectionScales')
            injectionScales = injectionScalesList.split(',')
        else :
            injectionScalesList = cp.get('injection','injectionScales')
            injectionScales = injectionScalesList.split(',')


        # ---- Create a dag to which we can add jobs.
        dag = pipeline.CondorDAG( log_file_simulations + "_" + set + uuidtag )

        # ---- Set the name of the file that will contain the DAG.
        dag.set_dag_file( "grb_simulations_" + set )

        # ---- Make instance of XsearchJob.
        job = XsearchJob(cp)
        mergejob = XmergeJob(cp)

        # ---- Make analysis jobs for all segments in the on-source segment list.
        #      Read segment list from file.
        segmentList = pipeline.ScienceData()
        segmentList.read( 'input/segment_on_source.txt' , blockTime )

        # ---- Read injection file to determine number of injections 
        #      for this set.
        #      Use system call to wc to figure out how many lines are 
        #      in the injection file.
        os.system('wc input/injection_' + set + \
            '.txt | awk \'{print $1}\' > input/' + set + '.txt' ) 
        f = open('input/' + set + '.txt')
        numberOfInjections = int(f.readline())
        f.close()
        os.system('cat input/injection_' + set + \
                  '.txt | awk \'{printf \"%.9f\\n\",$1+$2*1e-9}\'' + \
                  ' > input/gps' + set + '.txt' )
        f = open('input/gps' + set + '.txt')
        injection_list_time = f.readlines()
        f.close()


        # ---- Loop over injection scales.
        scale_counter = 0
        for injectionScale in injectionScales :
            mergenode = XmergeNode(mergejob)
            # Skip injection trigger production if path to already available triggers provided
            if not(reUseInj):
                # ---- Loop over segments.
                for i in range(len(segmentList)):
                    if  0 == maxInjNum:
                        # ---- Create job node.
                        node = XsearchNode(job)
                        # ---- Assign first argument: parameters file
                        matlab_param_file = cwdstr + "/input/parameters_simulation_" \
                            + set + "_" + str(scale_counter) + '_' + str(i) + ".txt"
                        node.set_param_file(matlab_param_file)
                        # ---- Assign second argument: job (segment) number 
                        node.set_x_jobnum(i)
                        # ---- Assign third argument: output directory
                        # ---- Check to see if distributing results files over nodes.
                        if 1 == distributeSimulation & 0 == maxInjNum:
                            # ---- Yes: determine output directories and make sure they exist.
                            #      Record directories in a file.
                            jobNumber = (jobNumber+1-nodeOffset)%nNodes + nodeOffset
                            nodeList = open( jobNodeFileSimulationPrefix + '_' + set \
                                + "_seg%d"%(i) + "_injScale" + injectionScale + '.txt'  ,'w')
                            nodeList.write("Number_of_detectors %d\n"%(len(detector)))
                            for j in range(0,len(detector)) :
                                nodeList.write("%s\t"%(detector[j]))
                            nodeList.write("\n")
                            nodeList.write('event_simulation_file ')
                            nodeList.write(cwdstr + "/input/event_on_source.txt \n")
                            nodeList.write('Injection_file ')
                            nodeList.write(cwdstr + "/input/injection_" + set + ".txt \n")
                            nodeList.write('Number_of_jobs ')
                            nodeList.write("%d \n"%(numberOfInjections))
                            #nJobsPerNode = int(numberOfInjections/nNodes) + 1
                            nJobsPerNode = numberOfInjections
                            nodeList.write('Number_of_jobs_per_node ')
                            nodeList.write("%d \n"%(nJobsPerNode))
                            # write path for each result file, on each node write results from
                            # nJobsPerNode jobs
                            #jobNumber = int((injectionNumber-1)/nJobsPerNode) + nodeOffset
                            while ~(os.path.isdir (nodePath + "%d/"%(jobNumber))) &1:
                                print("Waiting for automount to unmount ...\n", file=sys.stdout)
                                time.sleep(10)
                            # Write name of node before changing nodes
                            #if 0 == (injectionNumber-1) % nJobsPerNode:
                            print("Node number %d"%(jobNumber), file=sys.stdout)
                            nodeList.write(nodePath + "%d/ \n"%(jobNumber))
                            # Create directory for results files
                            fullPath = nodePath + "%d/"%(jobNumber) + onNodeSimulationPath \
                                + "simulations_" + set + '_' + str(scale_counter)
                            if ~(os.path.isdir (fullPath))& 1 :
                                os.makedirs(fullPath)
                            else:
                                print("**WARNING** path: " + fullPath \
                                + " already exists, previous results may be overwritten\n", file=sys.stdout)
                            node.set_output_dir(os.path.join( fullPath))
                            for injectionNumber in range(1,numberOfInjections+1) :
                                nodeList.write(fullPath + "/results_%d"%(i) \
                                    +  "_%d.mat \n"%(injectionNumber) )
                            nodeList.close()
                        else :
                            # ---- No: Set output directory to local.
                            node.set_output_dir( os.path.join( cwdstr + \
                                '/output/simulations_' + set + '_' + str(scale_counter) ) )
                        # ---- Assign fourth argument: injection number 
                        #      KLUDGE: This will screw up injections if more than 
                        #      one segment; injection number iterates only over 
                        #      injections that fall within analysis interval.
                        node.set_x_injnum("1-%d"%(numberOfInjections))
                        mergenode.add_parent(node)
                        node.set_retry(retryNumber)
                        # ---- Prepend human readable description to node name.
                        node.set_name("xdetection_simulation_" + set + "_seg" \
                            + str(i) + "_injScale" + str(scale_counter) + "_" + node.get_name())
                        dag.add_node(node)
                    else :
                        thisSegmentInjStart = 1e10
                        thisSegmentInjNumber = 0
                        for iInj in range(numberOfInjections) :
                            if abs(float(event_on_list[i])-float(injection_list_time[iInj]))<=blockTime/2-transientTime :
                                thisSegmentInjNumber += 1
                                thisSegmentInjStart = min(thisSegmentInjStart,iInj)
                        for iInjRange in range(int(math.ceil(float(thisSegmentInjNumber)/float(maxInjNum)))) :
                            node = XsearchNode(job)
                            matlab_param_file = cwdstr + "/input/parameters_simulation_" \
                                + set + "_" + str(scale_counter) + "_" + str(i) + ".txt"
                            node.set_param_file(matlab_param_file)
                            node.set_x_jobnum(i)
                            node.set_output_dir( os.path.join( cwdstr \
                                + '/output/simulations_' + set + '_' + str(scale_counter) ) )
                            node.set_x_injnum("%d"%(thisSegmentInjStart+iInjRange*maxInjNum+1) + "-" \
                                + "%d"%(thisSegmentInjStart+min((iInjRange+1)*maxInjNum,thisSegmentInjNumber)))
                            mergenode.add_parent(node)
                            node.set_retry(retryNumber)
                            # ---- Prepend human readable description to node name.
                            node.set_name("xdetection_simulation_" + set +  "_seg" + \
                                str(i) + "_injScale" + str(scale_counter) \
                                + "_injRange" + str(iInjRange) + "_" + node.get_name())
                            dag.add_node(node)
                    # ---- Add job node to the dag.

            # ---- Continue on to the next injection scale.
            # ---- Continue on to the next injection scale.
            # point to already produced injection results if provided
            if reUseInj:
                mergenode.set_dir_prefix(mergingCutsPath + '/../output/simulations_' + set + '_' + str(scale_counter) + '/')
            else :
                mergenode.set_dir_prefix('simulations_' + set + '_' + str(scale_counter) + '/')
            mergenode.set_sn_flag("0 " + mergingCutsString)
            mergenode.set_retry(retryNumber)
            # ---- Prepend human readable description to node name.
            mergenode.set_name("xmerge_simulation_" + set + "_injScale" \
                               + str(scale_counter) + "_" + mergenode.get_name())
            dag.add_node(mergenode)
            scale_counter = scale_counter + 1

        # ---- Write out the submit files needed by condor.
        dag.write_sub_files()
        # ---- Write out the DAG itself.
        dag.write_dag()

        # ---- Delete used dag job
        del dag
        del job
        del mergejob



# -------------------------------------------------------------------------
#      Write single dag containing all jobs.
# -------------------------------------------------------------------------

# ---- Use grep to combine all dag files into a single dag, with the 
#      PARENT-CHILD relationships listed at the end.
print("Combining all jobs into a single dag ...")
os.system('echo "DOT xpipeline_triggerGen.dot" > .dag_temp')
os.system('grep -h -v PARENT *.dag >> .dag_temp')
os.system('grep -h PARENT *.dag >> .dag_temp')
os.system('mv .dag_temp grb_alljobs.dag')


# -------------------------------------------------------------------------
#      Finished.
# -------------------------------------------------------------------------

# ---- Status message.
print("... finished writing job submission files. ", file=sys.stdout)
print(file=sys.stdout)

print("############################################", file=sys.stdout)
print("#           Completed.                     #", file=sys.stdout)
print("############################################", file=sys.stdout)


# ---- Exit cleanly
sys.exit( 0 )


