trunk/matlab/searches/combined/ 

This directory holds a single script, setUpJobs.py, which was intended as a 
replacement for grb.py, sn.py, etc.  Any desirable content should be absorbed 
into grb.py. See also the x-pypeline project by Scott Coughlin.

Note: setUpJobs.py is not functional; for example, the *.sub requirements for 
LDG accounting and request_disk are not implemented. 

