function [fout,figures_dirName,figfiles_dirName] = ...
    xproducewebpage(user_tag,analysis,vetoSegList,clusterInj)
% XPRODUCEWEBPAGE - open webpage and write webpage
% This requires xproducecuts to have been run first
%
% Usage:
%
%  [fout,figures_dirName,figfiles_dirName]=...
%         xwritewebpageheader(user_tag,analysis)
%
%  user_tag            String.
%  analysis            Structure created by xmakegrbwebpage
%
%  fout                Pointer to webpage file, opened in this function
%  figures_dirName     Name of output dir for figures to be created
%  figfiles_dirName    Name of output dir for figfiles to be created
%
% $Id: xwritewebpageheader.m 3272 2010-09-01 13:44:56Z mwas $

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(4, 4, nargin));
% Load data form xproducecuts
load(['EfficiencyData' analysis.vetoMethod '.mat'])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Assign Captions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

captions.detectors_spectra = ['Noise amplitude spectra sqrt{S(f)} of ' ...
'detectors in on-source interval (--- are design spectra).'];

captions.aweighted_detectors_spectra = ...
['Noise amplitude spectra divided by RSS antenna response: ' ... 
'sqrt{S(f)/(F+^2 +Fx^2)}. ' ...
'This gives a good idea of the relative sensitivity of the detectors.'];

captions.detectors_gauss = ...
['Glitchiness measure of on-source power spectrum: variance(S)/mean(S). '...  
'Gaussian noise will stay between --- lines. '...
'Big excursions are often seen at low frequency or at violin modes '...
'(~340 and ~680 Hz).'];

captions.asd_compare = ...
['Comparison of estimated noise amplitude spectra for off-source (blue) and on-source (black) segments. ' ...
'Biased results are possible if off-source and on-source noise spectra are significantly different.'];

captions.asd_norm = ...
['Estimated noise amplitude spectra for off-source jobs, normalized to on-source median.'];

captions.searchgrid = ...
['Antenna sensitivity and search grid.'];

captions.Fspectra = ...
['Weighting given to each detector in forming h+ stream; '...
'[e_i^+]^2 in the notation of the technical document. '...
'Large weightings for 2+ detectors are good.'];

captions.FCspectra = ...
['Weighting given to each detector in forming hx stream; '...
'[e_i^x]^2 in the notation of the technical document. '...
'Large weightings for 2+ detectors are good.'];

captions.Nullspectra = ...
['Weighting given to each detector in forming null stream; '...
'(1-[e_i^+]^2 - [e_i^x]^2) in the notation of the technical document. '...
'Near-equal weightings mean a stronger null stream test.'];

captions.Incspectra = ...
['Noise amplitude spectra divided by null stream weighting: '...
'sqrt{S(f)/K_i^2}. This gives an idea of the amplitude a '...
'GW or glitch needs for a good null stream test.'];

captions.onsource_scatter = ...
['Scatter plots of (dummy) on-source triggers: '...
'I_+ vs E_+, I_x vs E_x, I_null vs E_null (all available pairs). '...
'Magenta lines show coherent consistency cut thresholds. '...
'Star shows loudest surviving event (if any).'...
'Color gives the value of log10(significance)'];

captions.offsource_scatter = ...
['Scatter plots of all background events used in upper limit calculation. '];

captions.efficiency = ...
['Fraction of injections recovered with significance greater than loudest '...
'event in (dummy) on-source. Black dots are sampled values, red and yellow dot '...
'is respectivetly the 90% and 50% efficiecy obtained from interpolation. ' ...
'Green dots mark sampled valued with 0<efficiency<5%. ' ...
'Blue curve show the efficiency when DQ flags are not applied to injections. '...
'The cyan curve shows the sigmoid fit from FlareFit. '...
'We obtain the 90% upper limit from fit'];

captions.injection_scatter = ...
['Scatter plots of on- or off-source events with injections. '...
'(squares: injections at amplitude closest to 90% UL)'];

captions.ratevssig = ...
['Histogram of trigger rate vs significance threshold. '...
'All triggers from all UL background jobs, before and after all vetoes. '...
'All triggers from (dummy) on-source job, before and after all vetoes.'];

captions.hist_offLoudest = ...
['Histogram of significance of loudest surviving event from ' ...
'each UL background job, before and after each cut (--- after all cuts).'];

captions.FspectraWithInj = ...
['Noise spectrum plots with hrss 50% upper limits super-imposed.'];

captions.ratevsenergy_lowfreq = ...
    ['Histogram of trigger rate vs energy for each detector.' ...
     'Low frequency (peakFrequency<=200Hz) triggers from all UL background ' ...
     'jobs after applying cuts stated in heading.'];

captions.ratevsenergy_highfreq = ...
    ['Histogram of trigger rate vs energy for each detector.' ...
     'High frequency (peakFrequency>200Hz) triggers from all UL background ' ...
     'jobs after applying cuts stated in heading.'];

captions.energy1vs2= ...
    ['2D histogram of number of triggers in a bin of given energy in detector 1 and 2. ' ...
     'The color denotes the number of triggers on a log10 scale.'...
     'All triggers from all UL background jobs, after applying cuts stated in ' ...
      'heading.'];

captions.antennaNetwork = ...
    ['Pluses: color gives value of F_+^2+F_x^2 summed over all detectors in the network, ' ...
     'for each sky position used in the analysis. Note: for longer on-source windows ' ...
     'the sky positions come in bunches as the external trigger moves across the ' ...
     'sky with Earths rotation. Crosses: iso-time-delay line for the baseline in ' ...
     'the legend. Detector noise floors are not included in these computations.'];

captions.antennaEll = ...
    ['Pluses: color gives value of F_x^2/F_+^2 in DP frame, ' ...
     'the plus and cross component are summed over all detectors in the network; ' ...
     'for each sky position used in the analysis. Note: for longer on-source windows ' ...
     'the sky positions come in bunches as the external trigger moves across the ' ...
     'sky with Earths rotation. Crosses: iso-time-delay line for the baseline in ' ...
     'the legend. Detector noise floors are not included in these computations.'];

captions.antenna = ...
    ['Pluses: color gives value of F_+^2+F_x^2 for the detector in the title, ' ...
     'for each sky position used in the analysis. Note: for longer on-source windows ' ...
     'the sky positions come in bunches as the external trigger moves across the ' ...
     'sky with Earths rotation. Crosses: iso-time-delay line for the baseline in ' ...
     'the legend. Detector noise floors are not included in these computations.'];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Create comments file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Record input args in comments file
commentsFileName = [analysis.grb_name '_' user_tag '_comments.html'];
if(~exist(['./' commentsFileName ],'file'))
    comments=fopen(['./' commentsFileName],'w');
    fprintf(comments,'%s\n',['Any text/html added to ' commentsFileName  ...
        ' will appear in the "Human added comments" section of the '...
        'results webpage']);
    fclose(comments);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Create fig directories
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Create 'figures' and 'figfiles' directories, if they don't already
%      exist. These will hold the web page .png image files and the matlab 
%      .fig files used to generate them. 
figures_dirName = [analysis.grb_name '_' user_tag '_' ...
    analysis.type '_figures/'];
if(size(dir(figures_dirName), 1) == 0)
    mkdir(figures_dirName)
    mkdir([figures_dirName '/events'])
end

figfiles_dirName = [analysis.grb_name '_' user_tag '_' ...
    analysis.type '_figfiles/'];
if(size(dir(figfiles_dirName), 1) == 0)
    mkdir(figfiles_dirName)
    mkdir([figfiles_dirName '/events'])
end

events_dirName = './events/';
if(size(dir(events_dirName), 1) == 0)
    mkdir(events_dirName)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  Open web page and write header info.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Name of output web page.
webPageFileName = [analysis.grb_name '_' user_tag '_' ...
    analysis.type '.shtml'];

% ---- Open web page file.
fout = fopen(webPageFileName,'w');

% ---- Write web page headers and style info.
fprintf(fout,'%s\n',['<!DOCTYPE HTML PUBLIC "-//W3C//DTD '...
                     'HTML 4.01 Transitional//EN">']);
fprintf(fout,'%s\n','<html>');
fprintf(fout,'%s\n','<head>');
s = pwd;
fprintf(fout,'%s\n',['  <title> ' s ' </title>']);
fprintf(fout,'%s\n','  <style type="text/css">');
fprintf(fout,'%s\n','body {');
fprintf(fout,'%s\n','font-family: Tahoma,Geneva,sans-serif;');
fprintf(fout,'%s\n','color: black;');
fprintf(fout,'%s\n','background-color: white;');
fprintf(fout,'%s\n','}');
fprintf(fout,'%s\n','h1 {');
fprintf(fout,'%s\n','color: #ffffff;');
fprintf(fout,'%s\n','background-color: #000088;');
fprintf(fout,'%s\n','padding: 0.25em;');
fprintf(fout,'%s\n','border: 1px solid black;');
fprintf(fout,'%s\n','}');
fprintf(fout,'%s\n','h2 {');
fprintf(fout,'%s\n','color: #000000;');
fprintf(fout,'%s\n','background-color: #cccccc;');
fprintf(fout,'%s\n','padding: 0.25em;');
fprintf(fout,'%s\n','border: 1px solid black;');
fprintf(fout,'%s\n','}');
fprintf(fout,'%s\n','  </style>');

fprintf(fout,'%s\n',' <script type="text/javascript">');
fprintf(fout,'%s\n',' function toggleVisible(division) {');
fprintf(fout,'%s\n',[' if (document.getElementById("div_" + '...
                    'division).style.display == "none") {']);
fprintf(fout,'%s\n',[' document.getElementById("div_" + '...
                     'division).style.display = "block";']);
fprintf(fout,'%s\n',[' document.getElementById("input_" + '...
                     'division).checked = true;']);
fprintf(fout,'%s\n',' } else {');
fprintf(fout,'%s\n',[' document.getElementById("div_" + '...
                     'division).style.display = "none";']);
fprintf(fout,'%s\n',[' document.getElementById("input_" + '...
                     'division).checked = false;']);
fprintf(fout,'%s\n',' }');
fprintf(fout,'%s\n',' }');
fprintf(fout,'%s\n',' </script>');

fprintf(fout,'%s\n','</head>');
fprintf(fout,'%s\n','<body>');

if strcmp(analysis.type,'closedbox')
    
    fprintf(fout,'%s\n',['<h1>CLOSED-BOX Analysis of ' analysis.grb_name ...
                         ' with X-Pipeline</h1>']);
    fprintf(fout,'%s\n',['This page provides preliminary results '...
                         'from the analysis of ' analysis.grb_name]);
    fprintf(fout,'%s\n','using X-Pipeline.');
    
elseif strcmp(analysis.type,'openbox')
    
    fprintf(fout,'%s\n',['<h1>OPEN-BOX Analysis of ' analysis.grb_name ...
                         ' with X-Pipeline</h1>']);
    fprintf(fout,'%s\n',['This page provides results from the '...
                         'analysis of ' analysis.grb_name]);
    fprintf(fout,'%s\n','using X-Pipeline.');
    
else
    
  fprintf(fout,'%s\n',['<h1>UL-BOX Analysis of ' analysis.grb_name ...
                         ' with X-Pipeline</h1>']);
    fprintf(fout,'%s\n',['This page provides sanity check ULs from the '...
                         'analysis of ' analysis.grb_name]);
    fprintf(fout,'%s\n','using X-Pipeline.');  
    
end

fprintf(fout,'%s\n','<h2>Human added comments</h2>');
fprintf(fout,'%s\n',['<!--#include file="' commentsFileName '" -->']);

fprintf(fout,'%s\n','<h2>Code versions</h2>');

fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td><b> Codes </b></td>']);
fprintf(fout,'%s\n',['<td><b> svnversion </b></td>']);
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> Processing (i.e., xdetection) </td>']);
fprintf(fout,'%s\n',['<td> ' analysis.svnversion_xdetection ' </td>']);
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> Post-processing (i.e., xmakegrbwebpage) </td>']);
fprintf(fout,'%s\n',['<td> '  ' </td>']); % analysis.svnversion_grbwebpage
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n','</table><br>');

fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td><b> Detector </b></td>']);
fprintf(fout,'%s\n',['<td><b> Veto segment list</b></td>']);
fprintf(fout,'%s\n',['</tr>']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  Write veto segs used
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ifos = {'H1','L1','V1'};

for iDet = 1:length(analysis.detectorList)
    thisVetoSegList = vetoSegList{strcmp(ifos,analysis.detectorList(iDet))};
    fprintf(fout,'%s\n',['<tr>']);
    fprintf(fout,'%s\n',['<td> ' analysis.detectorList{iDet} ...
        ' </td>']);
    fprintf(fout,'%s\n',['<td> ' thisVetoSegList ' </td>']);
    fprintf(fout,'%s\n',['</tr>']);
end

fprintf(fout,'%s\n','</table><br>');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  Link to ini file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n','<br>Ini file used:<br>');
fileList=dir('*.ini');
for iFile=1:length(fileList)
fprintf(fout,'<A HREF="%s">%s</A><br>\n',fileList(iFile).name,fileList(iFile).name);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Write Network Info and other things! 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
analysis.captions = captions;
analysis.eCrossIndex = eCrossIndex;
analysis.eNullIndex  = eNullIndex;
xwritenetworkinfo(analysis,fout,figures_dirName,figfiles_dirName)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               summary html table showing optimal vetoes 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n','<h2>Veto Tuning</h2>');

fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td><b>'                '</b></td>']);
fprintf(fout,'%s\n',['<td><b>  nullinc over nullenergy      </b></td>']);
if strcmp(analysis.typeOfCutPlus,'twosided')
    fprintf(fout,'%s\n',['<td><b>' analysis.likelihoodType{ePlusIndex}  ' over ' ...
        analysis.likelihoodType{iPlusIndex} '</b></td>']);
elseif strcmp(analysis.typeOfCutPlus,'EoverI')
    fprintf(fout,'%s\n',['<td><b>' analysis.likelihoodType{ePlusIndex}  ' over ' ...
        analysis.likelihoodType{iPlusIndex} '</b></td>']);
else strcmp(analysis.typeOfCutPlus,'IoverE')
    fprintf(fout,'%s\n',['<td><b>' analysis.likelihoodType{iPlusIndex}  ' over ' ...
        analysis.likelihoodType{ePlusIndex} '</b></td>']);
end
if strcmp(analysis.typeOfCutCross,'twosided')
    fprintf(fout,'%s\n',['<td><b>' analysis.likelihoodType{eCrossIndex}  ' over ' ...
        analysis.likelihoodType{iCrossIndex} '</b></td>']);
elseif strcmp(analysis.typeOfCutCross,'EoverI')
    fprintf(fout,'%s\n',['<td><b>' analysis.likelihoodType{eCrossIndex}  ' over ' ...
        analysis.likelihoodType{iCrossIndex} '</b></td>']);
else strcmp(analysis.typeOfCutCross,'IoverE')
    fprintf(fout,'%s\n',['<td><b>' analysis.likelihoodType{iCrossIndex}  ' over ' ...
        analysis.likelihoodType{eCrossIndex} '</b></td>']);
end
fprintf(fout,'%s\n',['<td><b> detection statistic  </b></td>']);
fprintf(fout,'%s\n',['</tr>']);

fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td>   Optimal ' analysis.vetoMethod ' threshold </td>']);
fprintf(fout,'%s\n',['<td>   ' num2str(alphaCutNullTuned) ' </td>']);
fprintf(fout,'%s\n',['<td>   ' num2str(alphaCutPlusTuned) ' </td>']);
fprintf(fout,'%s\n',['<td>   ' num2str(alphaCutCrossTuned) ' </td>']);
fprintf(fout,'%s\n',['<td>   ' detectionStat{1} ' </td>']);
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n','</table><br>');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              Write table of alpha cuts cuts versus 50 percent Injection Scale for
%              each WaveForm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n',[' <input id="50PercentAlpha" type="checkbox" unchecked ']);
fprintf(fout,'%s\n',[' onclick="toggleVisible(''50PercentAlpha'');" />      ']);
fprintf(fout, '%s\n', ['Injection scale for which 50 percent of the tuning injections that pass a given alpha cut '...
    'and a pre-determined linear cut shown below and were louder than the '...
    num2str(100-FAR_Tuning) 'th percentile most significant '...
    'off-source event using the ranking (or loudness) statistic of '...
     detectionStat{1} ': <br>']);
fprintf(fout,'%s\n',[' <div id="div_50PercentAlpha" style="display: none;"> ']);

fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> Detection Stat </td>']);
fprintf(fout,'%s\n',['<td>null</td>']);
fprintf(fout,'%s\n',['<td>plus</td>']);
fprintf(fout,'%s\n',['<td>cross</td>']);
for iWave=1:length(injectionFilesCell)
    fprintf(fout,'%s\n',['<td>' injectionFilesCell{iWave} ' </td>']);
end
fprintf(fout,'%s\n',['</tr>']);
for iDet =1:length(detIndex);
    for iCut=1:length(analysisAlphaEff50percent{detIndex(iDet)})
        fprintf(fout,'%s\n',['<tr>']);
        fprintf(fout,'%s\n',['<td> ' analysis.likelihoodType{detIndex(iDet)}  ' </td>']);
        fprintf(fout,'%s\n',['<td> ' num2str(alphaCutNullRange(iCut))  ' </td>']);
        fprintf(fout,'%s\n',['<td> ' num2str(alphaCutPlusRange(iCut))  ' </td>']);
        fprintf(fout,'%s\n',['<td> ' num2str(alphaCutCrossRange(iCut)) ' </td>']);
        for iWave=1:length(injectionFilesCell)
            fprintf(fout,'%s\n',['<td>' num2str(analysisAlphaEff50percent{detIndex(iDet)}(iCut,iWave)) ' </td>']);
        end
        fprintf(fout,'%s\n',['</tr>']);
    end
end
fprintf(fout,'%s\n','</table><br>');    
fprintf(fout,'%s\n','</div>'); %-- end of div_50percentAlpha
fprintf(fout, '\n');
fprintf(fout, '<br> \n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              Write optimal linear cuts to table
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td><b>'                '</b></td>']);
fprintf(fout,'%s\n',['<td><b>  nullinc over nullenergy      </b></td>']);
fprintf(fout,'%s\n',['<td><b>' analysis.likelihoodType{ePlusIndex}  ' over ' ...
    analysis.likelihoodType{iPlusIndex} '</b></td>']);
fprintf(fout,'%s\n',['<td><b>' analysis.likelihoodType{eCrossIndex} ' over ' ...
    analysis.likelihoodType{iCrossIndex} '</b></td>']);
fprintf(fout,'%s\n',['</tr>']);

fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td>  Optimal linear cut threshold  </td>']);
fprintf(fout,'%s\n',['<td>   ' num2str(linearCutNullTuned) ' </td>']);
fprintf(fout,'%s\n',['<td>   ' num2str(linearCutPlusTuned) ' </td>']);
fprintf(fout,'%s\n',['<td>   ' num2str(linearCutCrossTuned) ' </td>']);
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n','</table><br>');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              Write table of linear cuts versus 95 percent Injection Scale for
%              each WaveForm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n',[' <input id="95PercentLinear" type="checkbox" unchecked ']);
fprintf(fout,'%s\n',[' onclick="toggleVisible(''95PercentLinear'');" />      ']);
fprintf(fout, ['Injection scale for which 95 percent of the tuning injections passed '...
    'a given linear cut below and were louder than the '...
    num2str(100-FAR_Tuning) 'th percentile most significant '...
    'off-source event using the ranking (or "loudness") statistic of '...
     detectionStat{1} ': \n']);
fprintf(fout,'%s\n',[' <div id="div_95PercentLinear" style="display: none;"> ']);
 
fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> detection stat </td>']);
fprintf(fout,'%s\n',['<td> null </td>']);
fprintf(fout,'%s\n',['<td> plus </td>']);
fprintf(fout,'%s\n',['<td> cross </td>']);
for iWave=1:length(injectionFilesCell)
    fprintf(fout,'%s\n',['<td>' injectionFilesCell{iWave} ' </td>']);
end
fprintf(fout,'%s\n',['</tr>']);
for iDet =1:length(detIndex);
    for iCut=1:length(analysisAlphaEff50percent{detIndex(iDet)})
        fprintf(fout,'%s\n',['<tr>']);
        fprintf(fout,'%s\n',['<td> ' analysis.likelihoodType{detIndex(iDet)}  ' </td>']);
        fprintf(fout,'%s\n',['<td> ' num2str(linearCutNullRange(iCut))  ' </td>']);
        fprintf(fout,'%s\n',['<td> ' num2str(linearCutPlusRange(iCut))  ' </td>']);
        fprintf(fout,'%s\n',['<td> ' num2str(linearCutCrossRange(iCut)) ' </td>']);
        for iWave=1:length(injectionFilesCell)
            fprintf(fout,'%s\n',['<td>' num2str(analysisRatioEff95percent{detIndex(iDet)}(iCut,iWave)) ' </td>']);
        end
        fprintf(fout,'%s\n',['</tr>']);
    end
end
fprintf(fout,'%s\n','</table><br>');   
fprintf(fout,'%s\n','</div>'); %-- end of div_50percentAlpha
fprintf(fout, '\n');
fprintf(fout, '<br> \n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               On Source and Off Source Section
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Print section header.
fprintf(fout,'%s\n','<h2>On-Source and Off-Source Events (no injections)</h2>');

fprintf(fout, '%s\n', ['We use random number generator rand with seed = '...
                       num2str(offJobSeed) ' to choose off Source jobs ' ...
                       'for veto tuning and upper limit calculation']);
fprintf(fout, '<br> \n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              Write list of jobNumbers discarded due to bad DQ
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n',[' <input id="offJobsDiscarded" type="checkbox" unchecked ']);
fprintf(fout,'%s\n',[' onclick="toggleVisible(''offJobsDiscarded'');" />      ']);
fprintf(fout, ['The ' num2str(length(uniqueOffJobNumberFail)) ... 
               ' off source segments with the following job numbers '...
               'were discarded since they failed our DQ criteria \n']);
fprintf(fout,'%s\n',[' <div id="div_offJobsDiscarded" style="display: none;"> ']);

for iJob = 1:length(uniqueOffJobNumberFail)
   fprintf(fout,'%s,\n',num2str( uniqueOffJobNumberFail(iJob) ));
end
fprintf(fout,'%s\n','</div>'); %-- end of div_offJobsDiscarded
fprintf(fout, '<br> \n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Write list of jobNumbers used for tuning
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n',[' <input id="offJobsForTuning" type="checkbox" unchecked ']);
fprintf(fout,'%s\n',[' onclick="toggleVisible(''offJobsForTuning'');" />      ']);
fprintf(fout, ['For tuning we use the ' num2str(length(offJob_tuning)) ...
               ' off source segments with the following job numbers: \n']);
fprintf(fout,'%s\n',[' <div id="div_offJobsForTuning" style="display: none;"> ']);
for iJob = 1:length(offJob_tuning)
   fprintf(fout,'%s,\n',num2str(offJob_tuning(iJob)));
end
fprintf(fout,'%s\n','</div>'); %-- end of div_offJobsForTuning
fprintf(fout, '<br> \n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Write list of jobNumbers used for ul calc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n',[' <input id="offJobsForULCalc" type="checkbox" unchecked ']);
fprintf(fout,'%s\n',[' onclick="toggleVisible(''offJobsForULCalc'');" />      ']);
fprintf(fout, ['For UL calc we use the ' num2str(length(offJob_ulCalc)) ... 
               ' off source segments with the following job numbers: \n']);
fprintf(fout,'%s\n',[' <div id="div_offJobsForULCalc" style="display: none;"> ']);
for iJob = 1:length(offJob_ulCalc)
   fprintf(fout,'%s,\n ',num2str(offJob_ulCalc(iJob)));
end
fprintf(fout,'%s\n','</div>'); %-- end of div_offJobsForULCalc
fprintf(fout, '<br> \n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  Write event properties to a table.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n','<br>');
fprintf(fout,'%s\n','AnalysisTimes used are :');
for iAT = 1:length(analysis.analysisTimes)
   fprintf(fout,'1/%g s, ',1/analysis.analysisTimes(iAT));
end
fprintf(fout,'%s\n','<br>');

if strcmp(analysis.type, 'closedbox')

    fprintf(fout,'%s\n',['For veto tuning, '...
    num2str(100-FAR_Tuning) 'th percentile loudest '...
    'off-source event taken as dummy on-source. <br>']); 
    fprintf(fout,'%s\n',['For upper limit calculation, '...
    num2str(100-FAR_UL) 'th percentile loudest '...
    'off-source event taken as dummy on-source, '... 
    'events taken ' ...
    'from background job number ' num2str(offSourceSelected.jobNumber(dummyOnSourceIndex)) ...
    ', center time = '  num2str(offSourceSelected.centerTime(dummyOnSourceIndex)) ...
    ', time offsets = ' num2str(offSourceSelected.timeOffsets(dummyOnSourceIndex,:)) ...
    ', circ time slides = ' num2str(offSourceSelected.circTimeSlides(dummyOnSourceIndex,:)) ...
    ' <br>']);
    % --- safety check, we want the variance on the
    % percentile level p: p(1-p)/N to be smaller than some
    % small factor squared: 0.1^2 (the allowed relative error, here
    % 0.1) times the expected value squared: (1-p)^2
    maxPercentile=max((100-FAR_Tuning),(100-FAR_Tuning));
    minNjobs=min(length(offJob_tuning),length(offJob_ulCalc));
    if(1-maxPercentile/100)*0.1^2 < 1/minNjobs
      fprintf(fout,'%s\n',...
             ['<font color="red">***WARNING***</font> not enough background' ...
              ' jobs, the percentile levels' ...
              ' have an absolut error in their estimation ~'...
               num2str(100*sqrt((1-maxPercentile/100)*maxPercentile/100/minNjobs)) '% <br>'] )
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Report deadtimes to webpage.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td><b> Data type </b></td>']);
fprintf(fout,'%s\n',['<td><b> Deadtime (s) </b></td>']);
fprintf(fout,'%s\n',['<td><b> Percentage of ' ...
    num2str(window.duration) 's analysis window </b></td>']);
fprintf(fout,'%s\n',['</tr>']);

% ---- Deadtime in real on-source.
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> On-source </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(onJobsDeadtime) ' </td>']);
fprintf(fout,'%s\n',['<td> ' ...
    num2str(onJobsDeadtime*100/window.duration) ' </td>']);
fprintf(fout,'%s\n',['</tr>']);

% ---- Deadtime in dummy on-source (if we have closedbox).
if strcmp(analysis.type,'closedbox') 
    % ---- Find idx of jobNumber corresponding to dummy onSource.
    dummyIdx = find(uniqueOffJobNumber == ...
        offSourceSelected.jobNumber(dummyOnSourceIndex));

    fprintf(fout,'%s\n',['<tr>']);
    fprintf(fout,'%s\n',['<td> Dummy on-source </td>']);
    fprintf(fout,'%s\n',['<td> ' num2str(offJobsDeadtime(dummyIdx)) ' </td>']);
    fprintf(fout,'%s\n',['<td> ' ...
        num2str(offJobsDeadtime(dummyIdx)*100/window.duration) ' </td>']);
    fprintf(fout,'%s\n',['</tr>']);
end

% ---- Deadtime in all off-source.
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> Off-source ('...
    num2str(nJobFilesProcessed) ' jobs) </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(sum(offJobsDeadtime)) ' </td>']);
fprintf(fout,'%s\n',['<td> ' ...
    num2str(sum(offJobsDeadtime)*100/...
    (window.duration * nJobFilesProcessed)) ' </td>']);
fprintf(fout,'%s\n',['</tr>']);

% ---- Deadtime in off-source for tuning.
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> Off-source (' ...
    num2str(nOffJob_tuning) ' tuning jobs) </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(sum(offJobsDeadtime(offIdx_tuning))) ' </td>']);
fprintf(fout,'%s\n',['<td> ' ...
    num2str(sum(offJobsDeadtime(offIdx_tuning))*100/...
    (window.duration * nOffJob_tuning)) ' </td>']);
fprintf(fout,'%s\n',['</tr>']);

% ---- Deadtime in off-source for ulCalc.
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> Off-source ('...
    num2str(nOffJob_ulCalc) ' UL calc jobs) </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(sum(offJobsDeadtime(offIdx_ulCalc))) ' </td>']);
fprintf(fout,'%s\n',['<td> ' ... 
    num2str(sum(offJobsDeadtime(offIdx_ulCalc))*100/...
    (window.duration * nOffJob_ulCalc)) ' </td>']);
fprintf(fout,'%s\n',['</tr>']);

fprintf(fout,'%s\n','</table><br>');
            
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Make scatter plot of on-source and off-source events.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- (Eplus,IPlus) (Ecross,Icross) etc.
tic 
[plotName,onSource] = xwriteonsourcescatter(ePlusIndex,iPlusIndex,alphaCutPlusTuned,linearCutPlusTuned,...
    onSource,analysis,analysis.typeOfCutPlus);
xsavefigure(plotName,figures_dirName,figfiles_dirName);
fprintf(fout,'%s\n',['<TD ALIGN="left"><A HREF="' figures_dirName ...
plotName '.png"><IMG SRC="' figures_dirName plotName ...
'_thumb.png" WIDTH=300 '...
'ALT="' captions.onsource_scatter '" TITLE="' captions.onsource_scatter '"></A></TD>']);
toc
[plotName] = xwriteoffsourcescatter(ePlusIndex,iPlusIndex,alphaCutPlusTuned,linearCutPlusTuned,...
    offSourceSelected,onSource,analysis,analysis.typeOfCutPlus);
xsavefigure(plotName,figures_dirName,figfiles_dirName);
fprintf(fout,'%s\n',['<TD ALIGN="left"><A HREF="' figures_dirName ...
plotName '.png"><IMG SRC="' figures_dirName plotName ...
'_thumb.png" WIDTH=300 '...
'ALT="' captions.offsource_scatter '" TITLE="' captions.offsource_scatter '"></A></TD>']);
toc
[plotName,onSource] = xwriteonsourcescatter(eCrossIndex,iCrossIndex,alphaCutCrossTuned,linearCutCrossTuned,...
    onSource,analysis,analysis.typeOfCutCross);
xsavefigure(plotName,figures_dirName,figfiles_dirName);
fprintf(fout,'%s\n',['<TD ALIGN="left"><A HREF="' figures_dirName ...
plotName '.png"><IMG SRC="' figures_dirName plotName ...
'_thumb.png" WIDTH=300 '...
'ALT="' captions.onsource_scatter '" TITLE="' captions.onsource_scatter '"></A></TD>']);
toc
[plotName] = xwriteoffsourcescatter(eCrossIndex,iCrossIndex,alphaCutCrossTuned,linearCutCrossTuned,...
    offSourceSelected,onSource,analysis,analysis.typeOfCutCross);
xsavefigure(plotName,figures_dirName,figfiles_dirName);
fprintf(fout,'%s\n',['<TD ALIGN="left"><A HREF="' figures_dirName ...
plotName '.png"><IMG SRC="' figures_dirName plotName ...
'_thumb.png" WIDTH=300 '...
'ALT="' captions.offsource_scatter '" TITLE="' captions.offsource_scatter '"></A></TD>']);
toc
if eNullIndex ==0
else
    [plotName,onSource] = xwriteonsourcescatter(eNullIndex,iNullIndex,alphaCutNullTuned,linearCutNullTuned,onSource,analysis,analysis.typeOfCutNull);
    xsavefigure(plotName,figures_dirName,figfiles_dirName);
    fprintf(fout,'%s\n',['<TD ALIGN="left"><A HREF="' figures_dirName ...
    plotName '.png"><IMG SRC="' figures_dirName plotName ...
    '_thumb.png" WIDTH=300 '...
    'ALT="' captions.onsource_scatter '" TITLE="' captions.onsource_scatter '"></A></TD>']);
    toc
    [plotName] = xwriteoffsourcescatter(eNullIndex,iNullIndex,alphaCutNullTuned,linearCutNullTuned,...
    offSourceSelected,onSource,analysis,analysis.typeOfCutNull);
    xsavefigure(plotName,figures_dirName,figfiles_dirName);
    fprintf(fout,'%s\n',['<TD ALIGN="left"><A HREF="' figures_dirName ...
    plotName '.png"><IMG SRC="' figures_dirName plotName ...
    '_thumb.png" WIDTH=300 '...
    'ALT="' captions.offsource_scatter '" TITLE="' captions.offsource_scatter '"></A></TD>']);
toc

end

fprintf(fout, '<br> \n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Make two tables, one of loudest events and one of loudest events 
%                           passing vetoes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for thisTable = 1:2
    
    onSource.probability = ones(size(onSource.significance,1),1);  
    onSource.probabilityZeroLag = ones(size(onSource.significance,1),1);
    % ---- Loop over all on-source events.
    for ii = 1:length(onSource.significance)
        if onSource.pass(ii)
            % ---- Fraction of background jobs giving equal or larger event.
            numLargerThanBack = ...
                sum( loudestBackgroundAlphaAll>= onSource.significance(ii));
            onSource.probability(ii) = numLargerThanBack / nOffJob_ulCalc;
% 
%             if sum(offLoudestEvent.zeroLag)
%                 % ---- Fraction of zerolag background jobs giving equal or 
%                 %      larger event.
%                 onSource.probabilityZeroLag(ii) = ...
%                     sum((offLoudestEvent.zeroLag(:).* ...
%                     offLoudestEvent.significance(:)) >= ...
%                     onSource.significance(ii)) /...
%                     sum(offLoudestEvent.zeroLag);
%             end
        end  
    end % -- Loop over all on-source events.
        
    % ---- Sort events using significance
    if (thisTable == 1)
        [s I] = sort(onSource.significance,'descend');
    elseif (thisTable == 2)
        [s I] = sort(onSource.significance.*onSource.pass,'descend');
    end

    % ---- write lists of quantities sorted by significance
    sLL  = onSource.likelihood(I,:);
    sBB  = onSource.boundingBox(I,:);
    sAT  = onSource.analysisTime(I);
    sNB  = onSource.nPixels(I);

    if (thisTable == 1)
        % ---- top ten of all onSource events
        if strcmp(analysis.type,'closedbox')
            fprintf(fout,'%s\n',['<h3> Table of loudest dummy on source events chosen' ....
                'from the background</h3>']);

        else
            fprintf(fout,'%s\n',['<h3> Table of loudest on-source events <h3>']);
        end
        fprintf(fout,'%s\n',[' Rows in <font color="red">red</font> indicate ' ...
            'events that failed one or more of the consistency or DQ vetoes.']);
        nLoudestClusters = min(10,length(onSource.analysisTime));
        
    elseif (thisTable == 2)
         % ---- top ten of all onSource events surviving cuts
        if strcmp(analysis.type,'closedbox')
            fprintf(fout,'%s\n',['<h3> Table of loudest dummy on-source events'...
                'choosen from the background</h3>']);
        else
            fprintf(fout,'%s\n',['<h3> Table of loudest on-source events <h3>']);
        end
         fprintf(fout,'%s\n',['All events in this table have ' ...
              'passed all consistency and DQ vetoes'])
         nLoudestClusters = min(10,sum(onSource.pass));
    end

    fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
    fprintf(fout,'%s\n','<br>');

    % ---- Write html table headers.
    fprintf(fout,'%s\n','<tr>');
    fprintf(fout,'%s','<td><b>significance</b></td>');
    fprintf(fout,'%s','<td><b> probability </b></td>');
    fprintf(fout,'%s','<td><b> probability (zero lag only)</b></td>');
    for likelihoodIdx=1:length(analysis.likelihoodType)
        fprintf(fout,'%s', [ '<td><b>' ...
            analysis.likelihoodType{likelihoodIdx} '</b></td>']);
    end
    fprintf(fout,'%s', [...
        '<td><b> peak time <br> GPS </b></td>' ...
        '<td><b> peak frequency<br>(Hz)</b></td>' ...
        '<td><b> start time <br> GPS </b></td>' ...
        '<td><b>duration<br>(ms)</b></td>' ...
        '</td><td><b>lowest frequency<br>(Hz)</b></td>' ...
        '<td><b> highest frequency <br>(Hz)</b></td>' ...
        '<td><b>1/analysis time</b></td>' ...
        '<td><b>number of <br>TF pixels</b></td>']);
    fprintf(fout,'%s\n','</tr>');

    % ---- Write event data.
    for iC=1:nLoudestClusters
        % ---- Color an event red if it fails a veto test.
        if onSource.pass(I(iC))
            rowcolor = 'white';
        else
            rowcolor = 'red';
        end

        event_prefix = [analysis.grb_name '_' analysis.type '_' user_tag];
        if (thisTable == 1)
            event_webpage =  ['./events/' event_prefix '_' num2str(iC) '.html'];
            %xplotonsourcevent(event_webpage,onSource,I(iC),detectionStat,analysis,figfiles_dirName,figures_dirName)
        elseif (thisTable == 2)
            event_webpage =  ['./events/' event_prefix '_pass' num2str(iC) '.html'];
            %xplotonsourcevent(event_webpage,onSource,I(iC),detectionStat,analysis,figfiles_dirName,figures_dirName)
        end

        fprintf(fout,['<tr bgcolor="' rowcolor '">']);
        % ---- Significance.
        fprintf(fout,'<td><A HREF="%s">%g</A></td>',event_webpage,onSource.significance(I(iC)));
        % ---- Probability.
        fprintf(fout,'<td>%g</td>',onSource.probability(I(iC)));
        % ---- Probability using zero lag background only.
        fprintf(fout,'<td>%g</td>',onSource.probabilityZeroLag(I(iC)));
        % ---- All likelihoods.
        for likelihoodIdx=1:length(analysis.likelihoodType)
            fprintf(fout,'<td>%g</td>',sLL(iC,likelihoodIdx));
        end
        % ---- Peak time-frequency.
        fprintf(fout,'<td>%9.5f</td>',onSource.peakTime(I(iC)));
        fprintf(fout,'<td>%g</td>',onSource.peakFrequency(I(iC)));
        % ---- Time-frequency info.
        fprintf(fout,[...
            '<td>%9.5f</td>'...
            '<td>%3.1f</td>'...
            '<td>%g</td>'...
            '<td>%g</td>'...
            '<td>%g</td>' ...
            '<td>%g</td></tr>\n'], ...
            sBB(iC,1),...
            sBB(iC,3)*1000, ...
            sBB(iC,2),...
            sBB(iC,2)+sBB(iC,4),...
            1/sAT(iC),...
            sNB(iC));
    end % ---- end loop over loudesnt events

    % ---- Close table and clean up.
    fprintf(fout,'%s\n','</table><br>');
    fprintf(fout,'%s\n','<br>');
    clear sLL sBB sAT sNB

end % ---- end of loop to make two plots
fprintf(fout,'%s\n','<br>');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Injection Section
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[~,numWaves] = size(analysisULEff);

for iWave = 1:numWaves;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %               Write Some Injection Info 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [injectionParameters] = ...
                 readinjectionfile(char(injectionFilesCell{iWave}));
    [~, gps_s, gps_ns, phi, theta, psi, name, parameters] = ...
                 parseinjectionparameters(injectionParameters);
    waveformName{1}           = cell2mat(name{1});
    waveformparametersCell{1} = cell2mat(parameters{1});
    catalogDir = '/home/scoughlin/CardiffMphil/xpipeline/SNEWS_Official/utilities';
    snippetPad = 32;
    snippetDuration = 2*snippetPad + 1;
    [t,hp,hc,hb] = xmakewaveform(waveformName{1}, ...
                    waveformparametersCell{1}, snippetDuration, ...
                    snippetPad, analysis.sampleFrequency, ...
                    'catalogDirectory',catalogDir);
                % ---- Compute central frequency and hrss (ignoring noise).
    [SNR, h_rss, h_peak, Fchar, bw, Tchar, dur] =  ...
          xoptimalsnr([hp hc hb],0,analysis.sampleFrequency,[],[],[],...
    analysis.minimumFrequency,analysis.maximumFrequency);
    cFreq(iWave) =  Fchar;  %-- central frequency

    fprintf(fout,'%s<br>\n',['<h2 id="'  injectionFilesCell{iWave} ...
                    '" > Injected Waveform : ' injectionFilesCell{iWave}  ...
                    ' from injection file: ' injectionFilesCell{iWave} '</h2>']); 
    fprintf(fout, '<br> \n');
    fprintf(fout, '%s\n', ['We use random number generator rand with seed = '...
        num2str(injSeed) ' to choose injections ' ...
        'for veto tuning and upper limit calculation']);
    fprintf(fout, '<br> \n');

    % ---- injections for tuning...
    fprintf(fout,'%s\n',[' <input id="injForTuning_' iWave '" type="checkbox" unchecked ']);
    fprintf(fout,'%s\n',[' onclick="toggleVisible(''injForTuning_' iWave ''');" />']);
    fprintf(fout, ['For tuning we use ' num2str(length(injIdx_tuning{iWave})) ' injections: \n']);
    fprintf(fout,'%s\n',[' <div id="div_injForTuning_' iWave '" style="display: none;"> ']);
    for injIdx = 1:nInjectionsBy2(iWave)
        fprintf(fout,'%s\n, ',num2str(injIdx_tuning{iWave}(injIdx)));
    end
    fprintf(fout,'%s\n','</div>'); %-- end of div_injForTuning
    fprintf(fout, '<br> \n');

    % ---- injections for ul calc...
    fprintf(fout,'%s\n',[' <input id="injForULCalc_' iWave '" type="checkbox" unchecked ']);
    fprintf(fout,'%s\n',[' onclick="toggleVisible(''injForULCalc_ ' iWave ''');" />']);
    fprintf(fout, ['For UL calculation we use ' num2str(length(injIdx_ulCalc{iWave})) ' injections: \n']);
    fprintf(fout,'%s\n',[' <div id="div_injForULCalc_ ' iWave '" style="display: none;">   ']);
    for injIdx = 1:(nInjections(iWave) - nInjectionsBy2(iWave))
        fprintf(fout,'%s\n, ',num2str(injIdx_ulCalc{iWave}(injIdx)));
    end
    fprintf(fout,'%s\n','</div>'); %-- end of div_injForULCalc
    fprintf(fout, '\n');
    fprintf(fout, '<br> \n');
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %               Efficiency Curves
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    plotName = ['/eff_' waveformName{1} '_' num2str(iWave)];
    figure; hold on;
    set(gca,'FontSize',20)
    X = injectionScale(iWave,:)';
    Y = analysisULEff(:,iWave);
    semilogx(X,Y,'k-o','linewidth',2,'MarkerFaceColor','k')
    plot(analysisULEff50and90percent(2,iWave),0.90,'rs','MarkerFaceColor','r');
    plot(analysisULEff50and90percent(1,iWave),0.5,'rs','MarkerFaceColor','y');

    % ---- color green points that are not zero efficiency but are below 5
    % percent, should draw the eye to false associations
    mask = Y>0 & Y<=0.05;
    semilogx(X(mask),Y(mask),'go','linewidth',2,'MarkerFaceColor','g');

    set(gca,'xscale','log')
    legend('on source','90% upper limit','50% upper limit','analysis eff','Location','SouthEast')
    title('detection efficiency');
    xlabel('Injection Scale')
    ylabel('fraction above loudest event')
    axis([min(X) max(X) 0 1.0]);
    grid;
    hold off;
    
    % ---- Save plot.
    xsavefigure(plotName,figures_dirName,figfiles_dirName);
    
    fprintf(fout,'%s\n',['<TD ALIGN="left"><A HREF="' figures_dirName ...
    plotName '.png"><IMG SRC="' figures_dirName plotName ...
    '_thumb.png" WIDTH=300 '...
    'ALT="' captions.efficiency '" TITLE="' captions.efficiency '"></A></TD>']);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %               ScatterPlots Injections over On and Off Source
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    inj90Percent{1} = zeros(size(nInjections(iWave)));
    inj90Percent{2} = zeros(nInjections(iWave),length(analysis.likelihoodType));
    % Obtain likelihood and significance for the loudest event associated
    % with an injection at the 90 percent injection scale
    for iN = 1:nInjections(iWave)
        [maxValue,maxIndex] = max(clusterInj(iWave,iN,analysisULEff90percentindex(1,iWave)).significance);
        if isempty(maxValue)
        else
            inj90Percent{1}(iN,:) = maxValue;
            inj90Percent{2}(iN,:) = clusterInj(iWave,iN,analysisULEff90percentindex(1,iWave)).likelihood(maxIndex,:);
        end
    end
    
    % Plot the TF map of the waveform in the data at the 90 percent
    % efficiency ampltiude.
    % Work in progress but think it would be nice

    % on source
    toc
    
    [plotName] = xwriteinjectionsscatter(ePlusIndex,iPlusIndex,analysis,...
        inj90Percent,'on',iWave,figfiles_dirName,analysisULEff50and90percent(2,iWave));
    xsavefigure(plotName,figures_dirName,figfiles_dirName);
    fprintf(fout,'%s\n',['<TD ALIGN="left"><A HREF="' figures_dirName ...
    plotName '.png"><IMG SRC="' figures_dirName plotName ...
    '_thumb.png" WIDTH=300 '...
    'ALT="' captions.efficiency '" TITLE="' captions.efficiency '"></A></TD>']);
    
    [plotName] = xwriteinjectionsscatter(ePlusIndex,iPlusIndex,analysis,...
        inj90Percent,'off',iWave,figfiles_dirName,analysisULEff50and90percent(2,iWave));
    xsavefigure(plotName,figures_dirName,figfiles_dirName);
    fprintf(fout,'%s\n',['<TD ALIGN="left"><A HREF="' figures_dirName ...
    plotName '.png"><IMG SRC="' figures_dirName plotName ...
    '_thumb.png" WIDTH=300 '...
    'ALT="' captions.efficiency '" TITLE="' captions.efficiency '"></A></TD>']);
    
    [plotName] = xwriteinjectionsscatter(eCrossIndex,iCrossIndex,analysis,...
        inj90Percent,'on',iWave,figfiles_dirName,analysisULEff50and90percent(2,iWave));
    xsavefigure(plotName,figures_dirName,figfiles_dirName);
    fprintf(fout,'%s\n',['<TD ALIGN="left"><A HREF="' figures_dirName ...
    plotName '.png"><IMG SRC="' figures_dirName plotName ...
    '_thumb.png" WIDTH=300 '...
    'ALT="' captions.efficiency '" TITLE="' captions.efficiency '"></A></TD>']);
    
    [plotName] = xwriteinjectionsscatter(eCrossIndex,iCrossIndex,analysis,...
        inj90Percent,'off',iWave,figfiles_dirName,analysisULEff50and90percent(2,iWave));
    xsavefigure(plotName,figures_dirName,figfiles_dirName);
    fprintf(fout,'%s\n',['<TD ALIGN="left"><A HREF="' figures_dirName ...
    plotName '.png"><IMG SRC="' figures_dirName plotName ...
    '_thumb.png" WIDTH=300 '...
    'ALT="' captions.efficiency '" TITLE="' captions.efficiency '"></A></TD>']);
    toc
    
    if eNullIndex ==0
    else
        [plotName] = xwriteinjectionsscatter(eNullIndex,iNullIndex,analysis,...
            inj90Percent,'on',iWave,figfiles_dirName,analysisULEff50and90percent(2,iWave));
        xsavefigure(plotName,figures_dirName,figfiles_dirName);
        fprintf(fout,'%s\n',['<TD ALIGN="left"><A HREF="' figures_dirName ...
        plotName '.png"><IMG SRC="' figures_dirName plotName ...
        '_thumb.png" WIDTH=300 '...
        'ALT="' captions.efficiency '" TITLE="' captions.efficiency '"></A></TD>']);

        [plotName] = xwriteinjectionsscatter(eNullIndex,iNullIndex,analysis,...
            inj90Percent,'off',iWave,figfiles_dirName,analysisULEff50and90percent(2,iWave));
        xsavefigure(plotName,figures_dirName,figfiles_dirName);
        fprintf(fout,'%s\n',['<TD ALIGN="left"><A HREF="' figures_dirName ...
        plotName '.png"><IMG SRC="' figures_dirName plotName ...
        '_thumb.png" WIDTH=300 '...
        'ALT="' captions.efficiency '" TITLE="' captions.efficiency '"></A></TD>']);
    end
    fprintf(fout, '<br> \n');
    clear inj90Percent
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         Histogram rate vs.significance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ---- Significance bins for histrograms.  Set here so that the range
%      covers up to the loudest background event before any cuts.
binArray = exp([ 1:0.01:log(max(offSourceSelected.significance)) inf]);
% WOULD LIKE TO GET THIS WORKING
xwriteratevssig(fout,analysis,window,onSource,offSourceSelected,...
  binArray,figures_dirName,figfiles_dirName);

fprintf(fout,'%s\n','<h2>Loudest Off-source Events</h2>');

% ---- Initialize histograms.
histOffLoudestPreVeto   = [];
histOffLoudest          = [];

% ---- Find loudest off-source per each job before vetos
% Determine unique jobNumbers
uniqueJobNumber = unique(offSourceSelected.jobNumber);
maxBackgroundJob = zeros(length(uniqueJobNumber),1);
for iJob = 1:length(uniqueJobNumber)
    % find indicies associated with jobNumber
    ijobNumber          = uniqueJobNumber(iJob);
    iUniqueJobNumber    = find(offSourceSelected.jobNumber == ijobNumber);
    maxBackgroundJob(iJob)    = max(offSourceSelected.significance(iUniqueJobNumber,:));

end
loudestBackgroundPreCuts = sort(maxBackgroundJob,'descend');

% ---- Loudest off-source clusters before vetos.
histOffLoudestPreVeto = histc(loudestBackgroundPreCuts, binArray);
% ---- Off-source clusters after vetos.
histOffLoudest = histc(loudestBackgroundAlphaAll,binArray);

% ---- Make plot.
figure; set(gca,'FontSize',20);
loglog(binArray,flipud(cumsum(flipud(histOffLoudestPreVeto))),'b','LineWidth',2);
hold on; grid;
loglog(binArray,flipud(cumsum(flipud(histOffLoudest))),'k','LineWidth',2);
xlabel('significance');
ylabel('#');
%title([ 'cluster type: ' analysis.clusterType]);
legend('off source: all events','off source: after window cut',...
       'off source: after veto segs','off source: after vetoing');

% ---- Save plot
plotNameHistOffLoudest = ['/hist_offLoudest_' analysis.clusterType];
xsavefigure(plotNameHistOffLoudest,figures_dirName,figfiles_dirName);

% ---- Put all plots into a table.
xaddfiguretable(fout,plotNameHistOffLoudest,figures_dirName,...
    analysis.captions.hist_offLoudest);
clear plotNameHistOffLoudest

    % ---- Write header to web page.
fprintf(fout,'%s\n','<h2>Summary</h2>');

% ---- Plot detector spectra with hrss ULs superimposed.
open([figfiles_dirName 'detectors_spectra.fig']); hold on;

% ---- retrieve legend from figure
%      note that the location needs to be set again 
legendStr = get(legend,'String');

plot(cFreq(:),analysisULEff50and90percent(1,:)*1e-22,'ko','markerfacecolor','k');
title('Upper Limits vs. Frequency')

legendStr{length(legendStr)+1} = 'upper limit';

legend(legendStr ,'Location','SouthEast')
hold off
% ---- Save figure.
xsavefigure('FspectraWithInj',figures_dirName,figfiles_dirName);
xaddfiguretable(fout,{'FspectraWithInj'},figures_dirName,...
        analysis.captions.FspectraWithInj);

% ---- End of web page.
fprintf(fout,'%s\n','</body>');
fprintf(fout,'%s\n','</html>');
fclose(fout);
% 
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %           Testing Stuff                   %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% % -------------------------------------------------------------------------
% %    External trigger.
% % -------------------------------------------------------------------------
% 
% % ---- Triggers values for fake SNEWS. See
% % https://wiki.ligo.org/Bursts/SNEWS
% [ra,dec] = earthtoradec(analysis.skyPositions(2),analysis.skyPositions(1),analysis.gpsCenterTime);
% trigger.ra  = ra;
% trigger.dec = dec;
% trigger.err = 0;
% trigger.gps = analysis.gpsCenterTime; 
% 
% % -------------------------------------------------------------------------
% %    Network and cuts.
% % -------------------------------------------------------------------------
% 
% 
% cutTypes = {'alpha','alpha','alpha', ...
%          'ratio','ratio','ratio'};
%     
% if exist('loudestBackgroundAlpha','var')
%     cutThresholds = [alphaCutNullTuned,alphaCutPlusTuned,alphaCutCrossTuned,...
%         linearCutNullTuned, linearCutPlusTuned,linearCutCrossTuned,loudestBackgroundAlpha];
% else
%     cutThresholds = [alphaCutNullTuned,alphaCutPlusTuned,alphaCutCrossTuned,...
%         linearCutNullTuned, linearCutPlusTuned,linearCutCrossTuned,loudestBackgroundRatio];
% end
% detectionStatsim = detectionStat;
% 
% %analysis.pixelselection = 'bpp';
% analysis.pixelselection = 'matchbw';
% %analysis.pixelselection = 'maxsnr';
% %analysis.pixelselection = 'minNpix';
% % ---- Choose one of these.
% analysis.mode = 'spectrogram';
% % analysis.mode = 'emulatefft'; analysis.pixelselection = 'foo';
% % analysis.mode = 'fft';
% % ---- Set each of these as desired.
% if ~isempty(regexp(vetoMethod,'.*Cut$', 'once'))
%     analysis.type = 'GR';
%     
% elseif ~isempty(regexp(vetoMethod,'.*CutCirc', 'once'))
%     
%     analysis.type = 'Circ';
%     
% elseif ~isempty(regexp(vetoMethod,'.*CutScalar', 'once'))
%     
%     analysis.type = 'scalar';
%     
% elseif ~isempty(regexp(vetoMethod,'.*CutLinear', 'once'))
%        
%     analysis.type = 'linear';
%     
% else
%     error('Cut Type cannot assign E and I values')
% end
% 
% analysis.usenoise = true;
% analysis.verbose = false;
% analysis.catalogDirectory = '~/Documents/MATLAB/xpipeline/utilities/waveforms/';
% analysis.spectrumfile = 'averaged_spectra_SNEWS.mat';
% fs = 4096;
% N_initial = 16*fs;
% 
% 
% % -------------------------------------------------------------------------
% %    Select waveforms to process.
% % -------------------------------------------------------------------------
% % Load ini file
% inifile = ini2struct('SNEWS_SN.ini');
% 
% iset = 0;
% 
% for iWave = 1:length(autoFilesCell)
%    waveformNamehold = strsplit(autoFilesCell{iWave},{'_','.'});
%    parametersHold{iWave} =  strsplit(inifile.waveforms.([waveformNamehold{2}]),',');
% end
% 
% for iWave = 1:length(autoFilesCell)
%     iset = iset + 1;
%     waveformPolarisation{iset} = 'elliptical';
%     waveformParametersHold = '';
%     for iNumWaveForms = 1: length(parametersHold{iWave})
%         parametersSplit = strsplit(parametersHold{iWave}{iNumWaveForms},'!');
%         waveformType{iset} = parametersSplit{1};
%         waveformParametersHold = [waveformParametersHold,parametersSplit{2},','];
%     end
%     waveformParameters{iset} = strsplit(waveformParametersHold,',');
%     waveformParameters{iset} = waveformParameters{iset}(1:end-1);
% end
% 
% 
% % -------------------------------------------------------------------------
% %    Perform analysis.
% % -------------------------------------------------------------------------
% 
% % ---- Loop over waveform sets.
% for iset = 1:length(autoFilesCell)
%     
%     [eff,scale,effAll] = xsimulateanalysis(trigger,analysis.detectorList, ...
%         cutTypes, cutThresholds, detectionStatsim{1},injectionScale(1,:), ...
%         waveformType{iset}, waveformPolarisation{iset}, ...
%         waveformParameters{iset},  N_initial, fs, analysis);     
% end
% 


