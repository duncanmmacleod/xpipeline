function [cluster]= ...
                xapplyratiocutsinjections(cluster,...
                    ePlusIndex,eCrossIndex,eNullIndex,...
                    iPlusIndex,iCrossIndex,iNullIndex,...
                    vetoPlusRange,vetoCrossRange,vetoNullRange,...
                       loudestBackgroundRatio,typeOfCutPlus,typeOfCutCross)

% Calculate all Ratio Values

plusRatioEoverI = log(cluster.likelihood(:,ePlusIndex)./cluster.likelihood(:,iPlusIndex));

crossRatioEoverI = log(cluster.likelihood(:,eCrossIndex)./cluster.likelihood(:,iCrossIndex));

plusRatioIoverE = log(cluster.likelihood(:,iPlusIndex)./cluster.likelihood(:,ePlusIndex));

crossRatioIoverE = log(cluster.likelihood(:,iCrossIndex)./cluster.likelihood(:,eCrossIndex));


if iNullIndex == 0 
    nullRatioIoverE = ones(size(plusRatioEoverI));          
else
    nullRatioIoverE = log(cluster.likelihood(:,iNullIndex)./cluster.likelihood(:,eNullIndex));
end


% Depending on wether you are requesting a two sided or one sided cut. 
% construct specific ratio and cut arrays.

if strcmp(typeOfCutPlus,'twosided')
    
    ratioCutsPlus   = log(vetoPlusRange);
    ratioCutsCross  = log(vetoCrossRange);
    ratioCutsNull   = log(vetoNullRange);
    ratioArrayPlus  = [plusRatioEoverI,plusRatioIoverE];
    ratioArrayCross = [crossRatioEoverI,crossRatioIoverE];
    ratioArrayNull  = nullRatioIoverE;
    sidedCut        = 2;
    
else
    
    ratioCutsPlus   = log(abs(vetoPlusRange));
    ratioCutsCross  = log(abs(vetoCrossRange));
    ratioCutsNull   = log(abs(vetoNullRange));
    if strcmp(typeOfCutPlus,'EoverI')
        ratioArrayPlus  = plusRatioEoverI;
    else
        ratioArrayPlus  = plusRatioIoverE;
    end   
    if strcmp(typeOfCutCross,'EoverI')
        ratioArrayCross = crossRatioEoverI;
    else
        ratioArrayCross = crossRatioIoverE;
    end   
    ratioArrayNull  = nullRatioIoverE;
    sidedCut        = 1;
    

end

% Preassign Ratio pass flags
cluster.passRatioCuts = true(length(ratioArrayPlus),length(ratioCutsPlus));
% We do a for loop to lighten the memory load.

for iN = 1:2500

    % Reshape Calculated Ratios and Cut Values such that they are the same size
    % Do this by repating the ratio array # of Cuts times and by
    % repeating each cut combination # of ratioarray times.

    ratioArrayTempPlus = repmat(ratioArrayPlus(iN:2500:length(ratioArrayPlus),:),length(ratioCutsPlus),1);
    ratioArrayTempCross= repmat(ratioArrayCross(iN:2500:length(ratioArrayPlus),:),length(ratioCutsCross),1);
    ratioArrayTempNull= repmat(ratioArrayNull(iN:2500:length(ratioArrayPlus)),length(ratioCutsNull),1);

    vetoPlusRangeRep = kron(ratioCutsPlus,ones(length(ratioArrayPlus(iN:2500:length(ratioArrayPlus))),sidedCut));
    vetoPlusCrossRep = kron(ratioCutsCross,ones(length(ratioArrayCross(iN:2500:length(ratioArrayPlus))),sidedCut));
    vetoPlusNullRep =  kron(ratioCutsNull,ones(length(ratioArrayNull(iN:2500:length(ratioArrayPlus))),1));

    % Reshape cluster Signifiance and LoudestBackGround per cut to same
    % size

    loudestBackGroundRatiorep = repmat(loudestBackgroundRatio,length(ratioArrayPlus(iN:2500:length(ratioArrayPlus))),1);
    significanceRepeated = repmat(cluster.significance(iN:2500:length(ratioArrayPlus)),1,length(ratioCutsPlus));

    % Determine what clusters passed all the Ratio cuts

    ratioPassCut = min(...
        min(...
        (sum(ratioArrayTempPlus > vetoPlusRangeRep,2)>0),...
        (sum(ratioArrayTempCross > vetoPlusCrossRep,2)>0)...
            ),...
        (sum(ratioArrayTempNull > vetoPlusNullRep,2)>0)...
        );

    % Reshape the pass cuts into matrix of 1s and 0s with dimensions
    % # of clusters X # of cuts
    ratioPassCutReshape = reshape(ratioPassCut,...
        length(ratioArrayPlus(iN:2500:length(ratioArrayPlus))),length(ratioCutsPlus));

    cluster.passRatioCuts(iN:2500:length(ratioArrayPlus),:) = min(...
        ratioPassCutReshape,...
        (significanceRepeated> loudestBackGroundRatiorep));

end
                         



