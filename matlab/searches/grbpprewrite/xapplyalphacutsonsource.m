function [cluster]= ...
                xapplyalphacutsonsource(cluster,...
                    ePlusIndex,eCrossIndex,eNullIndex,...
                    iPlusIndex,iCrossIndex,iNullIndex,...
                    vetoPlusRange,vetoCrossRange,vetoNullRange,...
                    vetoPlusRange2,vetoCrossRange2,vetoNullRange2,...
                    typeOfCutPlus,typeOfCutCross)

% Calculate all ratio values

% Calculate all ratio values

plusAlphaEoverI  = (2*(cluster.likelihood(:,ePlusIndex) - cluster.likelihood(:,iPlusIndex))...
    ./((cluster.likelihood(:,ePlusIndex)+cluster.likelihood(:,iPlusIndex)).^0.8));
crossAlphaEoverI = (2*(cluster.likelihood(:,eCrossIndex) - cluster.likelihood(:,iCrossIndex))...
    ./((cluster.likelihood(:,eCrossIndex)+cluster.likelihood(:,iCrossIndex)).^0.8));
plusAlphaIoverE  = (2*(cluster.likelihood(:,iPlusIndex) - cluster.likelihood(:,ePlusIndex))...
    ./((cluster.likelihood(:,ePlusIndex)+cluster.likelihood(:,iPlusIndex)).^0.8));
crossAlphaIoverE = (2*(cluster.likelihood(:,iCrossIndex) - cluster.likelihood(:,eCrossIndex))...
    ./((cluster.likelihood(:,eCrossIndex)+cluster.likelihood(:,iCrossIndex)).^0.8));

plusRatioEoverI = log(cluster.likelihood(:,ePlusIndex)./cluster.likelihood(:,iPlusIndex));
crossRatioEoverI = log(cluster.likelihood(:,eCrossIndex)./cluster.likelihood(:,iCrossIndex));
plusRatioIoverE = log(cluster.likelihood(:,iPlusIndex)./cluster.likelihood(:,ePlusIndex));
crossRatioIoverE = log(cluster.likelihood(:,iCrossIndex)./cluster.likelihood(:,eCrossIndex));


if iNullIndex == 0    
    nullRatioIoverE = ones(size(plusRatioEoverI));  
    nullAlphaIoverE = ones(size(plusRatioEoverI));    
else
    nullAlphaIoverE = 2*(cluster.likelihood(:,iNullIndex) - cluster.likelihood(:,eNullIndex)) ...
                 ./((cluster.likelihood(:,eNullIndex)+cluster.likelihood(:,iNullIndex)).^0.8);
    nullRatioIoverE = log(cluster.likelihood(:,iNullIndex)./cluster.likelihood(:,eNullIndex)); 
end

% Depending on wether you are requesting a two sided or one sided cut. 
% construct specific ratio and cut arrays.

if strcmp(typeOfCutPlus,'twosided')
    
    ratioCutsPlus   = log(vetoPlusRange);
    ratioCutsCross  = log(vetoCrossRange);
    ratioCutsNull   = log(vetoNullRange);
    ratioArrayPlus  = [plusRatioEoverI,plusRatioIoverE];
    ratioArrayCross = [crossRatioEoverI,crossRatioIoverE];
    ratioArrayNull  = nullRatioIoverE;
    
    alphaCutsPlus   = vetoPlusRange2;
    alphaCutsCross  = vetoCrossRange2;
    alphaCutsNull   = abs(vetoNullRange2);
    alphaArrayPlus  = abs(plusAlphaEoverI)+1;
    alphaArrayCross = abs(crossAlphaEoverI)+1;
    alphaArrayNull  = nullAlphaIoverE+1;
    sidedCut        = 2;
    
else
    
    ratioCutsPlus   = log(abs(vetoPlusRange));
    ratioCutsCross  = log(abs(vetoCrossRange));
    ratioCutsNull   = log(abs(vetoNullRange));
    if strcmp(typeOfCutPlus,'EoverI')
        ratioArrayPlus  = plusRatioEoverI;
    else
        ratioArrayPlus  = plusRatioIoverE;
    end   
    if strcmp(typeOfCutCross,'EoverI')
        ratioArrayCross = crossRatioEoverI;
    else
        ratioArrayCross = crossRatioIoverE;
    end   
    ratioArrayNull  = nullRatioIoverE;
    
    alphaCutsPlus   = abs(vetoPlusRange2);
    alphaCutsCross  = abs(vetoCrossRange2);
    alphaCutsNull   = abs(vetoNullRange2);
    if strcmp(typeOfCutPlus,'EoverI')
        alphaArrayPlus  = plusAlphaEoverI+1;
    else
        alphaArrayPlus  = plusAlphaIoverE+1;
    end   
    if strcmp(typeOfCutCross,'EoverI')
        alphaArrayCross = crossAlphaEoverI+1;
    else
        alphaArrayCross = crossAlphaIoverE+1;
    end         
    alphaArrayNull  = nullAlphaIoverE+1;
    sidedCut        = 1;

end

    
% Reshape Calculated Ratios and Cut Values such that they are the same size
% Do this by repating the ratio array # of Cuts times and by
% repeating each cut combination # of ratioarray times.

ratioArrayTempPlus = repmat(ratioArrayPlus,length(ratioCutsPlus),1);
ratioArrayTempCross= repmat(ratioArrayCross,length(ratioCutsCross),1);
ratioArrayTempNull= repmat(ratioArrayNull,length(ratioCutsNull),1);

alphaArrayTempPlus = repmat(alphaArrayPlus,length(alphaCutsPlus),1);
alphaArrayTempCross= repmat(alphaArrayCross,length(alphaCutsCross),1);
alphaArrayTempNull= repmat(alphaArrayNull,length(alphaCutsNull),1);

vetoPlusRangeRep = kron(ratioCutsPlus,ones(length(ratioArrayPlus),sidedCut));
vetoCrossRangeRep = kron(ratioCutsCross,ones(length(ratioArrayCross),sidedCut));
vetoNullRangeRep = kron(ratioCutsNull,ones(length(ratioArrayNull),1));

vetoPlusRange2Rep = kron(alphaCutsPlus,ones(length(alphaArrayPlus),1));
vetoCrossRange2Rep = kron(alphaCutsCross,ones(length(alphaArrayCross),1));
vetoNullRange2Rep = kron(alphaCutsNull,ones(length(alphaArrayNull),1));

% Take absolute value of vetoRange2Rep.
vetoPlusRange2Rep = abs(vetoPlusRange2Rep);
vetoCrossRange2Rep = abs(vetoCrossRange2Rep);
vetoNullRange2Rep = abs(vetoNullRange2Rep);
vetoPlusRange2Rep(vetoPlusRange2Rep==0) = -Inf;
vetoCrossRange2Rep(vetoCrossRange2Rep==0) = -Inf;
vetoNullRange2Rep(vetoNullRange2Rep==0) = -Inf;

% Determine what clusters passed all the Ratio and Alpha cuts

ratioPassCut = min(...
    min(...
    (sum(ratioArrayTempPlus > vetoPlusRangeRep,2)>0),...
    (sum(ratioArrayTempCross > vetoCrossRangeRep,2)>0)...
        ),...
    (sum(ratioArrayTempNull > vetoNullRangeRep,2)>0)...
    );


alphaPassCut = min(...
    min(...
    (sum((alphaArrayTempPlus  > vetoPlusRange2Rep),2)>0),...
    (sum((alphaArrayTempCross > vetoCrossRange2Rep),2)>0)...
    ),...
    (sum((alphaArrayTempNull  > vetoNullRange2Rep ),2)>0)...
        );

alphaPassCut = ...
       min(...
       repmat(ratioPassCut,length(alphaCutsPlus),1),alphaPassCut);

% Reshape the pass cuts into matrix of 1s and 0s with dimensions
% # of clusters X # of cuts

cluster.passAlphaCuts = reshape(alphaPassCut,...
    length(alphaArrayPlus),length(alphaCutsPlus));

                        
