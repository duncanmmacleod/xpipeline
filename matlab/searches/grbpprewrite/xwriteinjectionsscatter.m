function [plotName] = xwriteinjectionsscatter(eIndex,iIndex,analysis,cluster,...
    source,iWave,figfiles_dirName,UL90InjectionScale)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Make scatter plot of injections on top of on or off-source events.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

open([figfiles_dirName source '_' ...
  analysis.likelihoodType{eIndex} '_' ...
  analysis.likelihoodType{iIndex} '_' ...
  analysis.clusterType '.fig']);
hold on
legendStr = get(legend,'String');

if length(cluster{2}) == length(cluster{1})
scatter(cluster{2}(:,eIndex),...
    cluster{2}(:,iIndex),...
    40,log10(cluster{1}),...
    's','filled','MarkerEdgeColor','k');
else
end
% ---- Update legend
legendStr{length(legendStr)+1} = ...
    ['injections with injectionScale ' num2str(UL90InjectionScale)];

maxInjX = max(cluster{2}(:,eIndex));
maxInjY = max(cluster{2}(:,iIndex));

% ---- Format plot.
X=xlim;
Y=ylim;
axis([ X(1) max([X(2); maxInjX ]) ...
Y(1) max([Y(2); maxInjY ]) ]);

hold off;

% ---- Write legend
legend(legendStr,'Location','SouthEast');

% ---- Save plot.
plotName = ['Wave' num2str(iWave) '_' ...
  source                          '_' ...
  analysis.likelihoodType{eIndex} '_' ...
  analysis.likelihoodType{iIndex} '_' ...
  analysis.clusterType ];
