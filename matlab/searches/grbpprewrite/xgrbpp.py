#!/usr/bin/env python


"""
Script that sets up postprocessing (xmakegrbwebpage) jobs for a single GRB.
$Id: xgrbwebpage.py 4912 2015-05-30 13:27:12Z daniel.hoak@LIGO.ORG $
"""

# ---- Import standard modules to the python path.
import sys, os, subprocess, getopt, string, re
import configparser
from glue import pipeline


# ---- Function usage.
def usage():
    msg = """\
Usage:
  xgrbwebpage.py [options]
  -g, --grb-name <string>     Name of GRB, used to name the output files
                              [REQUIRED].
  -d, --grb-dir <path>        Path to grb dir [REQUIRED].
  -a, --auto-dir <path>       Path where to put auto_web dir; can be same as or
                              different from --grb-dir [REQUIRED].
  -w, --web-dir <path>        Path to output web page.  We will create a dir
                              <web-dir>/<grb-name>_<user-tag> to which we will
                              copy the output html and figure files [REQUIRED].
  -l, --log-dir <path>        Path to write condor log files to; must be on a
                              local file system (not in your home directory)
                              [REQUIRED].
  -u, --user-tag <string>     Descriptive tag used in output file names
                              [REQUIRED].
  -m, --mdc                   Set this flag if you are running MDC jobs.
  -c, --veto-method <type>    Type of veto cut to perform.  Must be one of
                              '(linear|median|medianLin|alpha|medianAlpha|alphaLin)
                               (Cut|CutCirc|CutAmp|CutScalar)', e.g. 'medianAlphaCutCirc'
                              [REQUIRED].
  -p, --percentile <string>   String in "XX-YY" format, where XX is percentile
                              level of the background distribution to use for
                              tuning and YY is the percentile level used for UL
                              estimation [REQUIRED].
  --dq-dir <path>             Path to dir holding DQ segments [OPTIONAL].
  -s, --add-stat <string>     Name of an additional detection statistic for
                              which the tuning will be done [OPTIONAL].
  --priority <prio>           Integer specifying the priority of the job.
                              Default value is 0, higher priority jobs are
                              submitted first to the cluster [OPTIONAL].
  --big-mem <memory>          Integer specifying the minimal memory requirement
                              for condor jobs in MB [OPTIONAL].
  --tuning-waveforms <string> Tilde-delimited string of waveform names to be used
                              for tuning.  Waveforms must be subset of those used
                              for injections.  Names must be in *lower case*, with
                              the same format as in the [waveforms] section of
                              .ini file, e.g. "csg500Q9incljitter5~csg1000q9incljitter5".
                              Default is to tune using all waveforms. [OPTIONAL]
  -h, --help                  Display this message and exit.

Example:

xgrbwebpage.py \\
  --grb-name GRB090709B \\
  --grb-dir /home/mwas/GRBrerun/S6A/analyzeGRBs/GRB090709B \\
  --auto-dir /home/mwas/GRBrerun/S6A/alphaLin/GRB090709B \\
  --web-dir /home/mwas/WWW/LSC/GRBrerun/S6A/alphaLin/ \\
  --log-dir /local/user/mwas/log/ \\
  --user-tag michal-alphalin \\
  --veto-method alphaLinCutCirc \\
  --percentile 99-99 \\
  --add-stat powerlaw

Notes:
- All paths need to be global.  Use `pwd` instead of ./ to obtain a localpath
  that is expanded to a global path by your shell.
- The GRB name can differ from the name supplied to grb.py.  However, it is
  recommended to use the user-tag option below to create unique names for the
  output and to keep the grb-name something sensible, e.g., GRB051211.
- Making auto-dir different from grb-dir is useful when post-processing
  triggers generated in a different run or by another user for whose dirs you
  lack write permission.

This script sets up the directory structure and creates the condor submit files
required to run postprocessing (xmakegrbwebpage) jobs on a cluster. Once you
have run this script you will find that a shell script has been created:

   run_xmakegrbwebpageDAGJobs_<user-tag>_<grb-name>.sh :
      run this script to launch the post processing condor jobs

"""
    print(msg, file=sys.stderr)


# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
params_file = None
grb_list = None
grbscript = None
detectors = []
condorPriority = "0"
minimalMem = False


# ---- assume we are not looking at MDCs as standard
mdcFlag = 0
batch_mode = False
veto_method = []
prePassFlag = False

# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hd:g:a:w:l:u:mc:bs:p:"
# ---- Long form.
longop = [
   "help",
   "grb-dir=",
   "grb-name=",
   "auto-dir=",
   "web-dir=",
   "log-dir=",
   "user-tag=",
   "mdc",
   "veto-method=",
   "batch-mode",
   "dq-dir=",
   "add-stat=",
   "percentile=",
   "priority=",
   "big-mem=",
   "tuning-waveforms="
   ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- We will record the command line arguments to grb.py in a file called
#      xgrbwebpage.param
command_string = 'xgrbwebpage.py '

# ---- Set default value for directory of DQ segments and tuning waveforms file
dq_dir = ''
tuning_waveforms = ''

# ---- Parse command-line arguments.  Arguments are returned as strings, so
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-d", "--grb-dir"):
        grb_dir = str(a)
        command_string = command_string + ' -d ' + a
    elif o in ("-g", "--grb-name"):
        grb_name = str(a)
        command_string = command_string + ' -g ' + a
    elif o in ("-a", "--auto-dir"):
        auto_dir = str(a)
        command_string = command_string + ' -a ' + a
    elif o in ("-w", "--web-dir"):
        web_dir = str(a)
        command_string = command_string + ' -w ' + a
    elif o in ("-l", "--log-dir"):
        log_dir = str(a)
        command_string = command_string + ' -l ' + a
    elif o in ("-u", "--user-tag"):
        user_tag = str(a)
        command_string = command_string + ' -u ' + a
    elif o in ("--dq-dir"):
        dq_dir = str(a)
        command_string = command_string + ' --dq-dir ' + a
    elif o in ("--add-stat", "-s"):
        stat_list = str(a)
        command_string = command_string + ' -p ' + a
        # Parse percentile string in order to pass it over to xproducecuts
        stat_list = stat_list.replace("-","~")
    elif o in ("-m", "--mdc"):
        mdcFlag = 1
        command_string = command_string + ' -m '
    elif o in ("-c", "--veto-method"):
        veto_method = str(a)
        command_string = command_string + ' -c ' + a
    elif o in ("-b", "--batch-mode"):
        batch_mode = True
        command_string = command_string + ' -b '
    elif o in ("-p", "--percentile"):
        percentile_str = str(a)
        command_string = command_string + ' -p ' + a
        # Parse percentile string in order to pass it over to xmakegrbwebpage
        percentile_str = percentile_str.replace("-"," ")
    elif o in ("--priority"):
        condorPriority = a
        command_string = command_string + ' --priority ' + a
    elif o in ("--tuning-waveforms"):
        tuning_waveforms = a
        command_string = command_string + ' --tuning-waveforms ' + a
    elif o in ("--big-mem"):
        minimalMem = a
        command_string = command_string + ' --big-mem ' + a
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not grb_dir:
    print("No grb-dir specified.", file=sys.stderr)
    print("Use --grb-dir to specify it.", file=sys.stderr)
    sys.exit(1)
if not grb_name:
    print("No grb-name specified.", file=sys.stderr)
    print("Use --grb-name to specify it.", file=sys.stderr)
    sys.exit(1)
if not auto_dir:
    print("No auto-dir specified.", file=sys.stderr)
    print("Use --auto-dir to specify it.", file=sys.stderr)
    sys.exit(1)
if not web_dir:
    print("No web-dir specified.", file=sys.stderr)
    print("Use --web-dir to specify it.", file=sys.stderr)
    sys.exit(1)
if not log_dir:
    print("No log-dir specified.", file=sys.stderr)
    print("Use --log-dir to specify it.", file=sys.stderr)
    sys.exit(1)
if not user_tag:
    print("No user-tag specified.", file=sys.stderr)
    print("Use --user-tag to specify it.", file=sys.stderr)
    sys.exit(1)
if not percentile_str:
    print("No percentile levels specified.", file=sys.stderr)
    print("Use --percentile to specify it.", file=sys.stderr)
    sys.exit(1)
if not veto_method:
    print("No veto-method specified.", file=sys.stderr)
    print("Use --veto-method to specify it.", file=sys.stderr)
    sys.exit(1)
if not tuning_waveforms:
    print("No waveforms specified for tuning coherent cuts.", file=sys.stdout)
    print("Will use all injected waveforms for tuning.", file=sys.stdout)
# -- BEWARE: Regular expression documenation: A|B - When one pattern
#    completely matches, that branch is accepted. This means that once
#    A matches, B will not be tested further, even if it would produce
#    a longer overall match.
if not(re.match('(alphaLin|linear|alpha)(CutScalar|CutCirc|CutAmp|Cut)',veto_method) or veto_method == 'None'):
    print("veto-method is currently set to " + veto_method, file=sys.stderr)
    print("veto-method must be of the form '(alphaLin|linear|alpha)(CutScalar|CutCirc|CutAmp|Cut) or None'", file=sys.stderr)
    sys.exit(1)

# ---- Check to see if we are running on Atlas
# ---- When running on Atlas we must add env flags to our .sub files

# ---- Get partial hostname
os.system('hostname > host.txt')
f = open('host.txt','r')
hostname=f.read()
f.close()

# ---- Get full hostame
os.system('hostname -f > host.txt')
f = open('host.txt','r')
fullhostname=f.read()
f.close()

os.system('rm host.txt')
# ---- Check hostname and set flag if necessary
if 'atlas' in fullhostname:
    atlasFlag = 1
elif 'h2' in hostname:
    atlasFlag = 1
elif 'coma' in fullhostname:
    atlasFlag = 1
else:
    atlasFlag = 0

# ---- Status message.  Report all supplied arguments.
print(file=sys.stdout)
print("####################################################", file=sys.stdout)
print("#                    xgrbwebpage                   #", file=sys.stdout)
print("####################################################", file=sys.stdout)
print(file=sys.stdout)
print("Parsed input arguments:", file=sys.stdout)
print(file=sys.stdout)
print("          grb dir          :", grb_dir, file=sys.stdout)
print("          grb name         :", grb_name, file=sys.stdout)
print("          auto dir         :", auto_dir, file=sys.stdout)
print("          web dir          :", web_dir, file=sys.stdout)
print("          log dir          :", log_dir, file=sys.stdout)
print("          user tag         :", user_tag, file=sys.stdout)
print("          veto method      :", veto_method, file=sys.stdout)
print("          mdc flag         :", mdcFlag, file=sys.stdout)
print("          dq dir           :", dq_dir, file=sys.stdout)
print("          stat list        :", stat_list, file=sys.stdout)
print("          percentile       :", percentile_str, file=sys.stdout)
if tuning_waveforms:
    print("          Cuts tuned using :", tuning_waveforms, file=sys.stdout)
if atlasFlag:
    print("          Running on Atlas : Yes", file=sys.stdout)
else:
    print("          Running on Atlas : No", file=sys.stdout)
print(file=sys.stdout)

# -------------------------------------------------------------------------
#      Preparatory.
# -------------------------------------------------------------------------

# ---- Generate unique id tag, this is used when naming dagman log file
os.system('uuidgen > uuidtag.txt')
f = open('uuidtag.txt','r')
uuidtag=f.read()
f.close()
os.system('rm uuidtag.txt')

# ---- Check dir names end in '/'
if not(auto_dir.endswith('/')):
    auto_dir = auto_dir + '/'
if not(web_dir.endswith('/')):
    web_dir = web_dir + '/'
if not(log_dir.endswith('/')):
    log_dir = log_dir + '/'
if not(grb_dir.endswith('/')):
    grb_dir = grb_dir + '/'

# ---- Set injectionType depending on whether --mdc flag was specified
if mdcFlag:
    injectionType = 'mdcInj'
else:
    injectionType = 'internalInj'

#------------------------------------------------------------------------------
#                             create dir structure
#------------------------------------------------------------------------------

# ---- Record cwd so we can come back later
initial_dir = os.getcwd();
if not(initial_dir.endswith('/')):
    initial_dir = initial_dir + '/'

if not(os.path.exists(log_dir)):
    print('Making log dir:' + log_dir)
    os.makedirs(log_dir)

# ---- Make web directory for this GRB, this is where we will put the
#      results webpage etc
web_dir_full =  web_dir + grb_name + '_' + user_tag
print('Making web dir: ' + web_dir_full)
try:
    os.makedirs(web_dir_full)
except OSError:
    print('WARNING: web dir already exists, continuing...')

print('Making auto_web dir: ' + auto_dir + 'auto_web' + '_' + user_tag)
try:
    os.makedirs(auto_dir + 'auto_web' + '_' + user_tag)
except OSError:
    print('WARNING: auto_dir already exists or permission denied? Try to continue...')

os.chdir(auto_dir + 'auto_web' + '_' + user_tag)

# ---- Write ASCII file holding xgrbwebpage.py command.
pfile = open('xgrbwebpage.param','w')
pfile.write(command_string + "\n")
pfile.close()

# ---- Making log dirs for condor jobs
print('making auto_web/logs dir')
try:
    os.mkdir('logs')
except OSError:
    print('WARNING: logs dir already exists or permission denied? Try to continue...')
#------------------------------------------------------------------------------
#                          read grb.params file
#------------------------------------------------------------------------------

# ---- copy over grb.param file for future reference
os.system('cp ' + grb_dir + 'grb.param' + ' grb.param')

# ---- read in args from grb.param file
grbParams = []
fin = open(grb_dir + 'grb.param')
grbParams = fin.read()
fin.close

# ---- remove pesky newline token
#      NB, it only counts as one char
if grbParams.endswith('\n'):
    grbParams = grbParams[0:len(grbParams)-1]

# ---- extract grb gps time from grb.params and use lalapps_tconvert to
#      write UTC time to a file
grbParamsList=grbParams.split(' ')
gpsStr = grbParamsList[grbParamsList.index('-g')+1]
print('Writing UTC time to tmpUTCtime_53ab6e3fe.txt')
timeCommand = 'lalapps_tconvert ' + gpsStr + ' > tmpUTCtime_53ab6e3fe.txt'
os.system(timeCommand)

iniFile = grbParamsList[grbParamsList.index('-p')+1]

# ---- Create configuration-file-parser object and read parameters file.
if iniFile.find('/') == 0 : # check if the ini file found is a global path
    params_file = iniFile
    iniFile = iniFile[iniFile.rfind('/')+1:]
else :
    params_file = grb_dir + '/' + iniFile
    iniFile = iniFile[iniFile.rfind('/')+1:]
print('Param file used: ' + params_file)
print('Ini file name: ' + params_file)
cp = configparser.ConfigParser()
cp.read(params_file)
possibleDetectors= ['H1','L1','V1']
veto_file_names = ''
if int(cp.get('segfind','generateSegs'))==0:
    for ifoIdx in range(0,len(possibleDetectors)):
        ifo = possibleDetectors[ifoIdx]

        # ---- If veto_list_cat2 is provided for this ifo in the
        #      params-file we will use it.
        if cp.has_option(ifo,"veto-list"):
            veto_file_names = veto_file_names + ' ' + cp.get(ifo, "veto-list")
        else:
            veto_file_names = veto_file_names + ' None'
# ---- If generateSegs ~= 0 then the segment-list and veto-list where
# ---- auto-generated and are available in a known location
else:
    for ifoIdx in range(0,len(possibleDetectors)):
        ifo = possibleDetectors[ifoIdx]
        thisIfoVetoFileName = grb_dir + '/input/' + ifo + '_cat24veto.txt'
        if os.path.isfile(thisIfoVetoFileName):
            veto_file_names = veto_file_names + ' ' + thisIfoVetoFileName
        else :
            veto_file_names = veto_file_names + ' None'
# ---- if a directory of veto files is provided by the user override
# ---- the veto files listed in the ini file
if dq_dir:
    veto_file_names =   ' ' + dq_dir + '/H1-cat2DQveto_all_KILLED_GJ.txt ' + \
        ' ' + dq_dir + '/H2-cat2DQveto_all_KILLED_GJ.txt ' + \
        ' ' + dq_dir + '/L1-cat2DQveto_all_KILLED_GJ.txt ' + \
        ' None ' + \
        ' ' + dq_dir + '/V1-cat2DQveto_all_KILLED_GJ.txt '

print(" Using DQ flags listed below to kill triggers", file=sys.stdout)
print(veto_file_names, file=sys.stdout)


#------------------------------------------------------------------------------
#               read channels.txt to find which ifos were analysed
#------------------------------------------------------------------------------

# ---- Figure out what ifos we are using, this is used to determine which
#      veto cuts we should use.
# possibleDetectors = ['H1', 'L1', 'V1']

channels_file = grb_dir + 'input/channels.txt'
if os.path.exists(channels_file):
    f = open(channels_file,'r')
    lines = f.readlines()
    for line in lines:
        if possibleDetectors.count(line[0:2]):
            detectors.append(line[0:2])
else:
    print("Required channels file does not exist:  " + \
    channels_file, file=sys.stderr)
    sys.exit(1)

if not(detectors):
    print("Error getting ifo names from channels file:  " + \
    channels_file, file=sys.stderr)
    sys.exit(1)

print('We will analyse the following detectors: ',  detectors)

#------------------------------------------------------------------------------
#           symlink files and dirs required by xmakegrbwebpage from the
#                              grb_dir to auto_dir
#------------------------------------------------------------------------------

if grb_dir != auto_dir:
    # ---- sym link input files to auto_dir + 'input'
    #      If this dir already exists ln -s will not do anything
    print('sym linking /input from ' + grb_dir)
    os.system('ln -s ' + grb_dir + 'input ' + auto_dir + 'input')

    # ---- sym link output files to auto_dir + 'output'
    #      If this dir already exists ln -s will not do anything
    print('sym linking /output from ' + grb_dir)
    os.system('ln -s ' + grb_dir + 'output ' + auto_dir + 'output')

print('sym linking ' + iniFile  + ' from ' + grb_dir)
os.system('ln -s ' + params_file + ' ' + iniFile)

#------------------------------------------------------------------------------
#              create auto.txt file required by xmakegrbwebpage
#------------------------------------------------------------------------------

makeCommand = 'makeAutoFiles2.py -p ' +  iniFile
print('Running makeAutoFiles2.py:')
print(makeCommand)
os.system(makeCommand)

# ---- If minimalMem is not provided try to come up with some sensible value

if not(minimalMem):
    # use 1000MB plus 4 times the size of the off-source mat file
    offSourceFileSize = os.path.getsize(auto_dir + 'output/off_source_0_0_merged.mat')/1e6
    minimalMem = str(int(round(1500 + 6*offSourceFileSize)))

# ---- Add the appropriate job tag to you post processing script!
grouptag = 'ligo.' + \
                cp.get('condor','ProdDevSim') + '.' +\
                cp.get('condor','Era') + '.' +\
                cp.get('condor','Group') + '.' +\
                cp.get('condor','SearchType')

UserName = cp.get('condor','UserName')

# -------------------------------------------------------------------------
#      Define condor job classes.
# -------------------------------------------------------------------------

class XwebJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An xmakegrbwebpage job
    """
    def __init__(self,submitFileName):
        """
        """
        # ---- Get path to executable.
        os.system('which xproducecuts > path_file.txt')
        f = open('path_file.txt','r')
        xdetectionstr = f.read()
        f.close()
        os.system('rm path_file.txt')
        self.__executable = xdetectionstr

        # ---- Get condor universe from parameters file.
        self.__universe = 'vanilla'
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)

        # ---- Add required environment variables.
        self.add_condor_cmd('environment',"USER=$ENV(USER);HOME=$ENV(HOME);LD_LIBRARY_PATH=$ENV(LD_LIBRARY_PATH);XPIPE_INSTALL_BIN=$ENV(XPIPE_INSTALL_BIN);PATH=$ENV(PATH);LIGOTOOLS=$ENV(LIGOTOOLS);X509_USER_PROXY=$ENV(HOME)/x509up.file" + uuidtag)

        # ----Add Accounting Group Flag
        self.add_condor_cmd('accounting_group',grouptag)

        # ----Add UserName Flag
        self.add_condor_cmd('accounting_group_user',UserName)

        # ---- Add priority specification
        self.add_condor_cmd('priority',condorPriority)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xmakegrbwebpage-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xmakegrbwebpage-$(cluster)-$(process).err')

        if minimalMem:
            self.add_condor_cmd('request_memory',minimalMem)

        # ---- If running on Atlas, add 'getenv = true' to sub file
        #    if atlasFlag:
        # ---- Set 'getenv = true' for all cluster, otherwise ligolw_dq_query fails at multiple stages
        self.add_condor_cmd('getenv',"true")

        # ---- Name of condor job submission file to be written.
        self.set_sub_file(submitFileName)

class XwebNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    An xmakegrbwebpage node
    """
    def __init__(self,job):
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)
        self.__args = None

    def set_dir_prefix(self,path):
        self.add_var_arg(path)
        self.__dir_prefix = path

    def get_dir_prefix(self):
        return self.__dir_prefix

    def set_args(self,args):
        self.add_var_arg(args)
        self.__args = args

    def get_args(self):
        return self.__args

# -----------------------------------------------------------------------------
#      Write dag which will run post post processing
# -----------------------------------------------------------------------------
# ---- The structure of our dag is as follows:

#
# ---- Create a dag to which we will add our jobs
dagman_log = log_dir
dag = pipeline.CondorDAG(dagman_log + uuidtag)

# ---- Set the name of the file that will contain the dag.
dag_name = 'grb_web'
dag.set_dag_file(dag_name)

#------------------------------------------------------------------------------
#                Write jobs for final xmakegrbwebpage job
#------------------------------------------------------------------------------

# ---- Write pre script which tries to set up the grid proxy for cluster nodes
pre_script = grb_name + '_' + user_tag + '_pre.sh'
fpre = open(pre_script,'w')
fpre.write('#!/bin/bash \n')
fpre.write('if [[ -e $X509_USER_PROXY ]]\nthen\n')
fpre.write('cp $X509_USER_PROXY $HOME/x509up.file' + uuidtag + ' \n')
fpre.write('fi\n')
fpre.close()
command = 'chmod +x ' + pre_script
os.system(command)

# ---- Write post script which will copy output of post processing
#      to web_dir_fill
post_script = grb_name  + '_' + user_tag + '_post.sh'
fpost = open(post_script,'w')
fpost.write('#!/bin/bash \n')
fpost.write('rm -f $HOME/x509up.file' + uuidtag + ' \n')
fpost.write('if (( $1 )) ; then exit $1 ; fi \n')
# query files to wake up file system
command = 'ls ' + grb_name  + '_' + user_tag + '* | wc \n'
fpost.write(command)
fpost.write('sleep 30 \n\n')
# copy files
command = 'cp -r ' + grb_name  + '_' + user_tag + '* ' + web_dir_full + '\n'
fpost.write(command)
command = 'cp -r ' + grb_name  + '_' + user_tag + '* ' + web_dir_full + '\n'
fpost.write(command)
command = 'cp -r events/ ' + web_dir_full + '\n'
fpost.write(command)
command = 'cp ' + params_file  + ' ' + web_dir_full + '\n'
fpost.write(command)
fpost.close()
command = 'chmod +x '+ post_script
os.system(command)

# ---- Make instance of XwebJob
# ---- This will be the final xmakegrbwebpage job
xproduce_args = 'xproducecuts_' + grb_name + '_' + user_tag + \
'.txt'
farg = open(xproduce_args,'w')
prod_args ='auto.txt ' + veto_method + ' ' + stat_list + \
veto_file_names + ' ' +\
percentile_str + ' closedbox ' + user_tag + ' ' + grb_name
farg.write(prod_args)
farg.close()

finalwebjob = XwebJob('xproducecuts.sub')
finalwebnode = XwebNode(finalwebjob)
finalwebnode.set_args(auto_dir + '/auto_web' + '_' + user_tag + '/' + xproduce_args)
retryNumber = 1
finalwebnode.set_retry(retryNumber)
finalwebnode.set_post_script(post_script)
finalwebnode.add_post_script_arg('$RETURN')
finalwebnode.set_pre_script(pre_script)
# ---- Prepend human readable description to node name.
finalwebnode.set_name("xmakegrbwebpage_tunedUL_" + finalwebnode.get_name())
dag.add_node(finalwebnode)

# ---- Write out the submit files needed by condor.
dag.write_sub_files()
# ---- Write out the dag itself.
dag.write_dag()
# ---- Delete used dag webjob tunejob
del dag
del finalwebjob

# ---- Add line to .dag file to produce dot file
os.system('mv ' + dag_name + '.dag .temp_dag');
os.system('echo "DOT xpipeline_postProc.dot" > ' + dag_name + '.dag')
os.system('cat .temp_dag >> ' + dag_name + '.dag')
os.system('rm -f .temp_dag')

# ---- Write open and ul post script that copy over the result to web page area
open_post_script = grb_name  + '_' + user_tag + '_open_post.sh'
fpost = open(open_post_script,'w')
fpost.write('#!/bin/bash \n')
fpost.write('rm -f $HOME/x509up.file' + uuidtag + ' \n\n')
# run event display code
fpost.write('xgrbeventdisplay.py -a ' + auto_dir + 'auto_web -t openbox\n')
fpost.write('cd ' + auto_dir + 'auto_web' + '_' + user_tag + '/event\n')
fpost.write('condor_submit_dag grb_event_openbox.dag\n')
fpost.write('cd ' + auto_dir + 'auto_web' + '_' + user_tag + '/\n\n')
command = 'cp -r ' + grb_name  + '_' + user_tag + '_open* ' + web_dir_full + '\n'
fpost.write(command)
fpost.close()
command = 'chmod +x '+ open_post_script
os.system(command)
ul_post_script = grb_name  + '_' + user_tag + '_ul_post.sh'
command = 'cat ' + open_post_script + " | sed 's|_open\*|_ul\*|' > " + ul_post_script
os.system(command)
command = 'chmod +x '+ ul_post_script
os.system(command)

#------------------------------------------------------------------------------
#             write scripts to allow user to launch dag job
#------------------------------------------------------------------------------

# ---- Return to initial working dir.
os.chdir(initial_dir)

if (batch_mode):
    # ---- Open shell script that can be used to launch condor jobs.
    #      If we are running xgrbwebpage.py from xbatchgrbwebpage.py
    #      this script may already exist (i.e., if the current GRB is
    #      not the first GRB to be processed) and we should append the
    #      new commands to the existing script.
    frun_name = 'run_xmakegrbwebpageDAGJob_' + user_tag + '_ALL.sh'
    print('Writing file: ', frun_name)
    if not os.path.isfile(frun_name):
        frun = open(frun_name,'w')
        os.system('chmod +x ' + frun_name)
        command = 'condor_submit_dag -usedagdir ' + auto_dir + 'auto_web' + '_' + user_tag + '/' + dag_name + '.dag '
        frun.write(command)
        frun.close()
    else:
        frun = open(frun_name,'a')
        command = auto_dir + 'auto_web' + '_' + user_tag + '/' + dag_name + '.dag '
        frun.write(command)
        frun.close()

else:
    # ---- Open shell script that can be used to launch condor jobs.
    frun_name = 'run_xmakegrbwebpageDAGJob_' + user_tag + '_' + grb_name + '.sh'
    frun = open(frun_name,'w')
    os.system('chmod +x ' + frun_name)

    print('Writing file: ', frun_name)
    # ---- Write lines to run this GRB's condor dag to our script file
    command = 'cd ' + auto_dir + 'auto_web' + '_' + user_tag + '/; condor_submit_dag ' + \
        dag_name + '.dag;\n'
    frun.write(command)
    frun.close()

    print('Now run ' + frun_name + \
    ' to launch the xmakegrbwebpage DAG jobs', file=sys.stdout)

print(file=sys.stdout)
print(" ... finished.", file=sys.stdout)
