function [plotName,onSource] = xwriteonsourcescatter(eIndex,iIndex,alphaCut,linearCut,...
    onSource,analysis,typeOfCut)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Make scatter plot of on-source events.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Plot Plus Energy Versus Plus Inc

% ---- Plot only 100000 points with the highest significance.
maxPoints = 100000;
[sigSorted, sortIdx] = sort(onSource.significance,'descend');
if length(sigSorted) > maxPoints
    warning(['We are using only the ' num2str(maxPoints) ...
        ' most significant triggers when making scatter plots.']);
end

numPoints = min([maxPoints length(onSource.significance)]);
plotIdx = sortIdx(1:numPoints);   

figure; 
scatter(onSource.likelihood(plotIdx,eIndex),...
    onSource.likelihood(plotIdx,iIndex),...
    min(20+5*sqrt(onSource.significance(plotIdx)),175),log10(onSource.significance(plotIdx)),'+')

colorbar
set(gca,'FontSize',20)
set(gca,'xscale','log')
set(gca,'yscale','log')
xlabel(analysis.likelihoodType{eIndex});
ylabel(analysis.likelihoodType{iIndex});
grid;
hold on;

% Find max on-source
% If there is nothing to plot then the max on source surviing is 0 so we
% must note this.

[~, maxIdx] = max((onSource.significance).*(onSource.pass));

if maxIdx == 0
   onSource.maxSignificance  = 0;
   onSource.maxSigLikelihood = -1 * ones(1,length(analysis.likelihoodType));
else
   onSource.maxSignificance  = onSource.significance(maxIdx);
   onSource.maxSigLikelihood = onSource.likelihood(maxIdx,:);
   scatter(onSource.likelihood(maxIdx,eIndex),...
        onSource.likelihood(maxIdx,iIndex),...
       400, log10(onSource.maxSignificance), ...
       'p','filled','MarkerEdgeColor','r','LineWidth',1)
end

% ---- Initialise legend string for events scatter plot.
legendStr = {'onSource'};
legendStr{length(legendStr)+1} = 'loudest on source event surviving vetoes';

% ---- Format plot.
X=xlim;
Y=ylim;
axis([max(X(1),0.1) max(X(2),10) max(Y(1),0.1) max(Y(2),10)]);
title('onSource');

% ---- Plot a diagonal line over a large range.  The axes limits might
%      increase when we add in injections.
Xvec = 1:10^5;
plot(Xvec,Xvec,':k','LineWidth',0.5);  %-- diagonal line
legendStr{length(legendStr)+1} = 'diagonal';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         Plot alpha and linear cut to webpage            
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Determine what sided cut you are performing
alpha = 0.8;
if strcmp(typeOfCut,'twosided')
   if alphaCut == 0
   else
   Ypos = (alphaCut-1)/2*Xvec.^alpha;
   IposAlpha = (Xvec+Ypos)/2;
   EposAlpha = (Xvec-Ypos)/2;

   Yneg = - (alphaCut-1)/2*Xvec.^alpha;
   InegAlpha = (Xvec+Yneg)/2;
   EnegAlpha = (Xvec-Yneg)/2;

   plot(EposAlpha,IposAlpha,'--r','LineWidth',2);
   plot(EnegAlpha,InegAlpha,'--r','LineWidth',2);
   legendStr{length(legendStr)+1} = 'alphacut';
   end
elseif strcmp(typeOfCut,'IoverE')
   if alphaCut == 0
   else
    Ypos = (abs(alphaCut)-1)/2*Xvec.^alpha;
    IposAlpha = (Xvec+Ypos)/2;
    EposAlpha = (Xvec-Ypos)/2;
    plot(EposAlpha,IposAlpha,'--r','LineWidth',2);
    legendStr{length(legendStr)+1} = 'alphacut';
   end
elseif strcmp(typeOfCut,'EoverI')
   if alphaCut == 0
   else
    Yneg = - (abs(alphaCut)-1)/2*Xvec.^alpha;
    InegAlpha = (Xvec+Yneg)/2;
    EnegAlpha = (Xvec-Yneg)/2;
    plot(EnegAlpha,InegAlpha,'--r','LineWidth',2);   
    legendStr{length(legendStr)+1} = 'alphacut';
   end 
else
    
end

if strcmp(typeOfCut,'twosided')
   if linearCut == 0
   else
   plot(Xvec*abs(linearCut),Xvec,'--g','LineWidth',2);
   plot(Xvec,Xvec*abs(linearCut),'--g','LineWidth',2);
   legendStr{length(legendStr)+1} = 'linearcut';
   end
elseif strcmp(typeOfCut,'IoverE')
   if linearCut == 0
   else
   plot(Xvec,Xvec*abs(linearCut),'--g','LineWidth',2);
   legendStr{length(legendStr)+1} = 'linearcut'; 
   end
elseif strcmp(typeOfCut,'EoverI')
   if linearCut == 0
   else
   plot(Xvec*abs(linearCut),Xvec,'--g','LineWidth',2);
   legendStr{length(legendStr)+1} = 'linearcut'; 
   end
else
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Reset axis save figure and add to webpage.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Reset axes.
axis([max(X(1),0.1) max(X(2),10) max(Y(1),0.1) max(Y(2),10)]);

hold off;
legend(legendStr,'Location','SouthEast');

% ---- Save plot.
plotName = ['on_' ...
  analysis.likelihoodType{eIndex} '_' ...
  analysis.likelihoodType{iIndex} '_' ...
  analysis.clusterType ];
