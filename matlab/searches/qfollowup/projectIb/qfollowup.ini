; LIGO-Virgo Project Ib X-Pipeline coherent follow-up configuration script.
; 
; $Id$
;
; This ini file gives the configuration information for running coherent 
; follow-ups on triggers from the LIGO-Virgo Project Ib analysis.


; ---- This section specifies information on this version of the configuration.
[pipeline]
version = $Id$
user-tag = 'LIGO-Virgo Project Ib follow-up.'


; ---- This section specifies how the coherent likelihoods are to be calculated.
[parameters]
analysisTimes = 0.03125,0.015625,0.0078125
blockTime = 16
likelihoodType = hardconstraint,standard,nullenergy,incoherentenergy
minimumFrequency = 64
maximumFrequency = 1024
offsetFraction = 0.25
outputType = followup
sampleFrequency = 4096
verboseFlag = 1
whiteningTime = 1


; ---- This section specifies all of the "known" detectors and information 
;      on the frame types and channel names to be used.
[input]

; ---- Detector and channel info for simulated data from LIGO-Virgo project.
detectorList = H1,L1,V1
channelList = STRAIN,STRAIN,noise
frameTypeList = SIM,SIM,SIM
frameCacheFile = framecache.txt

; ; ---- Detector and channel info for real S5 data (version 3 calibration).
; detectorList = H1,H2,L1,G1
; channelList = LSC-STRAIN,LSC-STRAIN,LSC-STRAIN,DER_DATA_H
; frameTypeList = H1_RDS_C03_L2,H2_RDS_C03_L2,L1_RDS_C03_L2,RDS_C01_L3

; ; ---- Detector and channel info for real time-shifted data from the LIGO-Virgo project IIb.
; detectorList = H1,H2,L1,V1,G1
; channelList = LSC-STRAIN,LSC-STRAIN,LSC-STRAIN,h_16384HzNo50,DER_DATA_H
; frameTypeList = H1_LIGOVIRGO_2B,H2_LIGOVIRGO_2B,L1_LIGOVIRGO_2B,V1_LIGOVIRGO_2B,G1_LIGOVIRGO_2B
; frameCacheFile = /archive/home/shourov/qscan/framecaches/S5.txt


; ---- The optional sections [H1], [H2], etc. can be used to specify 
;      single-IFO segment lists or data-quality flags to be used by 
;      LSCsegFind to generate segment lists.
[H1]
segmentListFile = segments.txt
[L1]
segmentListFile = segments.txt
[V1]
segmentListFile = segments.txt


; ---- Info on MDC sets: LIGO-Virgo project Ib MDCs.
;      ORDER MUST MATCH LIST OF KNOWN DETECTORS IN [input]!  Fill missing entries with 'None'.
[mdc_a1b2g1]
channelList = DFM_A1B2G1,DFM_A1B2G1,DFM_A1B2G1
frameTypeList = BURSTINJ,BURSTINJ,BURSTINJ
[mdc_a2b4g1]
channelList = DFM_A2B4G1,DFM_A2B4G1,DFM_A2B4G1
frameTypeList = BURSTINJ,BURSTINJ,BURSTINJ
[mdc_gauss1]
channelList = GAUSS1,GAUSS1,GAUSS1
frameTypeList = BURSTINJ,BURSTINJ,BURSTINJ
[mdc_gauss4]
channelList = GAUSS4,GAUSS4,GAUSS4
frameTypeList = BURSTINJ,BURSTINJ,BURSTINJ
[mdc_sg235]
channelList = SG235,SG235,SG235
frameTypeList = BURSTINJ,BURSTINJ,BURSTINJ
[mdc_sg820]
channelList = SG820,SG820,SG820
frameTypeList = BURSTINJ,BURSTINJ,BURSTINJ

; ; ---- Info on MDC set: LIGO-Virgo project IIb SG MDCs.
; [mdc_sg1]
; channelList = GW-H,GW-H,GW-H,GW-H,GW-H
; frameTypeList = SG1_LIGOVIRGO_2B,SG1_LIGOVIRGO_2B,SG1_LIGOVIRGO_2B,SG1_LIGOVIRGO_2B,SG1_LIGOVIRGO_2B


; ---- This section specifies the location of the log files for the DAGs, and 
;      some other condor information.
[condor]
universe = vanilla
dagman_log_on_source = /usr1/psutton/log/grb_jobs_on_source.logs
dagman_log_mdcs = /usr1/psutton/log/grb_jobs_simulations.logs
retryNumber = 0

; ---- This section specifies what server to use when looking for frames or 
;      segment lists.
[datafind]
server = ldas-cit.ligo.caltech.edu 

