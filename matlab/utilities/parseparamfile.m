function [xcommand, args] = parseparamfile(fileName)
% PARSEPARAMFILE - parse grb.param, xgrb.param, or other x-pipeline .param file
%
% usage:
%
%   [xcommand, args] = parseparamfile(fileName)
%
% fileName  String. Name of .param file to be parsed.
%
% xcommand  String. Name command/script that made the .param file.
% args      Nx2 cell array of strings. Column 1 holds the argss, e.g.
%           '--params-file', and column 2 holds the corresponding value, e.g.
%           'grb_offline.ini'. If the args takes no arguments, e.g.
%           '--network-selection', then the corresponding second column element
%           will be []. 
%
% $Id$

try
    % ---- Read file.
    [status,result] = system(['cat ' fileName]);
    % ---- Split contents into cell array by whitespace, discarding the whitespace
    %      characters.
    result = strsplit(result); 
catch
    error(['Unable to open specified file ' fileName])
end

% ---- Parse entries. The first is the x-pipeline script name, argss start
%      with one or two hyphens. Arguments start without hyphens, but could be
%      negative numbers, so we have to check by testing for conversion to a
%      number using str2num. Note that the arguments '-i' and '-j' have
%      sensible numeric interpretations, so we need to make an exception for
%      those.
xcommand = result{1};
numArgs  = 0;
for ii=2:length(result)
    if ~isempty(result{ii})
        word = result{ii};
        if strcmp(word,'-i') || strcmp(word,'-j')
            % ---- Special argss that will confuse str2num() test.
            numArgs = numArgs + 1;
            args{numArgs,1} = word;
        elseif strcmp(word(1),'-') && isempty(str2num(word))
            % ---- General argss: of the form -* but not a negative number.
            numArgs = numArgs + 1;
            args{numArgs,1} = word;
        else
            args{numArgs,2} = word;
        end
    end
end

% ---- Done.
return

