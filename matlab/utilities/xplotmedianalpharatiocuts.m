function [numCuts] = xplotmedianalpharatiocuts(ratioArray,medianE,medianI, ...
    fixedRatioArray,fixedDeltaArray,analysis,likelihood,pairIdx,fout,writeText)
% XPLOTMEDIANRATIOCUTS - plot thresholds for median-racking test.
%
% usage:
%  
% [numCuts] = xplotmedianratiocuts(ratioArray,medianE,medianI, ...
%     analysis,likelihood,pairIdx,fout,writeText)
%  
% ratioArray    Square array with the same number of columns as the 
%               likelihood variable. Each nonzero element is a threshold
%               to be applied to the ratio of two likehoods; see below.
% medianys      Vector. Median value of each of the three incoherent likelihoods
%               for different values of the energy (coherent) likelihood.
% analysis      Structure. Contains parameters of data analysis, see 
%               xmakegrbwebpage
% likelihood
% pairIdx       Integer. Identifies which likelihoods in 
%               analysis.likelihoodType we will plot
% fout          File identifier of file to which we will direct text output
% writeText     Integer. Set to 1 to write text on veto ratios to fout 
%  
% numCuts       Integer. Number of ratio cut lines plotted, useful when
%               creating legends
%
% $Id$

% ---- Checks.
error(nargchk(10,10,nargin));

% ---- initialise number of cuts plotted to zero
numCuts = 0;

% ---- Status report, get name of likelihood (e.g., 'null') from
%      name field of analysis.likelihoodPairs
fprintf(1,['Plotting fit to median distribution of '...
    analysis.likelihoodPairs(pairIdx).name ' likelihoods... ']);

medE = getfield(medianE,analysis.likelihoodPairs(pairIdx).name);
medI = getfield(medianI,analysis.likelihoodPairs(pairIdx).name);
X = medE + medI;

eLikeIdx = analysis.likelihoodPairs(pairIdx).energy;
iLikeIdx = analysis.likelihoodPairs(pairIdx).inc;

alpha = 0.8;
% ---- Do the relevant plotting.
if (ratioArray(eLikeIdx,iLikeIdx) > 0) || ...
      (fixedRatioArray(eLikeIdx,iLikeIdx) > 0)
   % E/I +ve 
   % ---- ratioArray is +ve we do a two sided test
   numCuts = 2; 

   % plot line    : 
   ra = ratioArray(eLikeIdx,iLikeIdx);
   fra = abs(fixedRatioArray(eLikeIdx,iLikeIdx))-1;
   IposMed = 0.5 * (2*medI + ra^2 + ( 4*medI*ra^2 + ra^4 ).^0.5); 
   Ypos = + fra/2*X.^alpha;
   IposFixed = (X+Ypos)/2;
   Epos = (X-Ypos)/2;
   IposMed = interp1(medE,IposMed,Epos);
   if abs(ra) > 0
     Ipos = max(IposMed,IposFixed);
   else
     Ipos = IposFixed;
   end
   InegMed = 0.5 * (2*medI + ra^2 - ( 4*medI*ra^2 + ra^4 ).^0.5); 
   Yneg = - fra/2*X.^alpha;
   InegFixed = (X+Yneg)/2;
   Eneg = (X-Yneg)/2;
   InegMed = interp1(medE,InegMed,Eneg);
   if abs(ra) > 0
     Ineg = min(InegMed,InegFixed);
   else
     Ineg = InegFixed;
   end

   plot(medE,medI,'--r','LineWidth',2);
   plot(Epos,Ipos,'--m','LineWidth',2);
   plot(Eneg,Ineg,'--m','LineWidth',2);

elseif (ratioArray(iLikeIdx,eLikeIdx) > 0) || ...
      (fixedRatioArray(iLikeIdx,eLikeIdx) > 0)
   % I/E +ve  
   % ---- ratioArray is +ve we do a two sided test
   numCuts = 2;

   % plot line    : 
   ra = ratioArray(iLikeIdx,eLikeIdx);
   fra = abs(fixedRatioArray(iLikeIdx,eLikeIdx))-1;
   IposMed = 0.5 * (2*medI + ra^2 + ( 4*medI*ra^2 + ra^4 ).^0.5); 
   Ypos = + fra/2*X.^alpha;
   IposFixed = (X+Ypos)/2;
   Epos = (X-Ypos)/2;
   IposMed = interp1(medE,IposMed,Epos);
   if abs(ra) > 0
     Ipos = max(IposMed,IposFixed);
   else
     Ipos = IposFixed;
   end
   InegMed = 0.5 * (2*medI + ra^2 - ( 4*medI*ra^2 + ra^4 ).^0.5); 
   Yneg = - fra/2*X.^alpha;
   InegFixed = (X+Yneg)/2;
   Eneg = (X-Yneg)/2;
   InegMed = interp1(medE,InegMed,Eneg);
   if abs(ra) > 0
     Ineg = min(InegMed,InegFixed);
   else
     Ineg = InegFixed;
   end
   plot(medE,medI,'--r','LineWidth',2);
   plot(Epos,Ipos,'--m','LineWidth',2);
   plot(Eneg,Ineg,'--m','LineWidth',2);

elseif (ratioArray(eLikeIdx,iLikeIdx) < 0) || ...
      (fixedRatioArray(eLikeIdx,iLikeIdx) < 0)
   % E/I -ve 
   % ---- ratioArray is -ve we do a one sided test 
   numCuts = 1;

   % plot line    : 
   ra = abs(ratioArray(eLikeIdx,iLikeIdx));
   fra = abs(fixedRatioArray(eLikeIdx,iLikeIdx))-1;
   InegMed = 0.5 * (2*medI + ra^2 - ( 4*medI*ra^2 + ra^4 ).^0.5); 
   Yneg = - fra/2*X.^alpha;
   InegFixed = (X+Yneg)/2;
   Eneg = (X-Yneg)/2;
   InegMed = interp1(medE,InegMed,Eneg);
   if abs(ra) > 0
     Ineg = min(InegMed,InegFixed);
   else
     Ineg = InegFixed;
   end

   plot(medE,medI,'--r','LineWidth',2);
   plot(Eneg,Ineg,'--m','LineWidth',2);

elseif (ratioArray(iLikeIdx,eLikeIdx) < 0) || ...
      (fixedRatioArray(iLikeIdx,eLikeIdx) < 0)
   % I/E -ve  
   % ---- ratioArray is -ve we do a one sided test  
   numCuts = 1;

   % plot line    : 
   ra = abs(ratioArray(iLikeIdx,eLikeIdx));
   fra = abs(fixedRatioArray(iLikeIdx,eLikeIdx))-1;
   IposMed = 0.5 * (2*medI + ra^2 + ( 4*medI*ra^2 + ra^4 ).^0.5); 
   Ypos = + fra/2*X.^alpha;
   IposFixed = (X+Ypos)/2;
   Epos = (X-Ypos)/2;
   IposMed = interp1(medE,IposMed,Epos);
   if abs(ra) > 0
     Ipos = max(IposMed,IposFixed);
   else
     Ipos = IposFixed;
   end

   plot(medE,medI,'--r','LineWidth',2);
   plot(Epos,Ipos,'--m','LineWidth',2);

end

% ---- Status report.
fprintf(1,'done! \n');

return;
