function V = CartesianPointingVector(phi,theta)
% CARTESIANPOINTINGVECTOR - convert spherical coordinates to unit 3-vectors.
%
% CartesianPointingVector converts lists of spherical coordinates to 
% Cartesian unit pointing vectors. 
%
%  V = CartesianPointingVector(phi,theta)
%
%  phi      Vector of azimuthal angles (radians).
%  theta    Vector of polar angles (radians).
%
%  V        Array.  Each row is a unit 3-vector pointing in the direction 
%           specified by the corresponding (phi,theta).
%
% The angle vectors phi and theta must have the same size.
%
% $Id$

% ---- Make sure we are using column vectors.
phi = phi(:);
theta = theta(:);

% ---- Compute unit vectors.
V = [sin(theta).*cos(phi), sin(theta).*sin(phi), cos(theta)];

% ---- Done.
return;
