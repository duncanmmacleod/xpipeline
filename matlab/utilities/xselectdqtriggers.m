function [triggersItfAnnoying]=xselectdqtriggers(fout,analysis,user_tag,sigThr,offSourceSelectedAfterDQandWindow,...
                           offSourceSelectedAfterAllCuts)
energyNpixelsRatio = 2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- all triggers above a small threshold
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- select triggers above some significance threshold
triggers = xclustersubset(...
    offSourceSelectedAfterDQandWindow,...
    find(offSourceSelectedAfterDQandWindow.significance > sigThr/2));
% --- reorder according to significance
[tmp I] = sort(triggers.significance,'descend');
triggers = xclustersubset(triggers,I);

for iItf = 1:length(analysis.detectorList)
  % --- select triggers that have significant contribution from the given
  % itf
  iEnergy=find(strcmp(analysis.likelihoodType,['energyitf' num2str(iItf)]));
  if isempty(iEnergy)
    fprintf(fout,['<br> Skipping annoying triggers for ' ...
                  analysis.detectorList{iItf} '. Single detector energy column  ' ...
                  'not found\n'])
    triggersItf{iItf} = [];
    continue
  end
  if isempty(triggers.likelihood)
    triggersItf{iItf} = triggers;
    continue
  else
    triggersItf{iItf} = ...
      xclustersubset(triggers,...
                     find(triggers.likelihood(:,iEnergy)>energyNpixelsRatio* ...
                          triggers.nPixels));
  end
  
  % --- count a trigger found multiple times only once by taking the most
  % significant in a given second 
  % NB: unique time sorts the triggers
  if(not(isempty(triggersItf{iItf}.unslidTime)))
    % --- reorder according to energy
    [tmp I] = sort(triggers.likelihood(:,iEnergy),'descend');
    triggers = xclustersubset(triggers,I);
    % --- keep the most significant triggers in given second
    [tmp I] = unique(floor(triggersItf{iItf}.unslidTime(:,iItf)),'first');
    triggersItf{iItf}=xclustersubset(triggersItf{iItf},I);
    triggersInfo=[triggersItf{iItf}.unslidTime(:,iItf)...
                  triggersItf{iItf}.significance ...
                  triggersItf{iItf}.peakFrequency ...
                  triggersItf{iItf}.boundingBox(:,3) ...
                  sqrt(triggersItf{iItf}.likelihood(:,iEnergy))];
  end
end % --- loop over ITFs
triggersItfAnnoying{1} = triggersItf;
clear triggers triggersInfo triggersItf

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- all triggers above threshold that appear more than once in coincidence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- select triggers above some significance threshold
triggers = xclustersubset(...
    offSourceSelectedAfterDQandWindow,...
    find(offSourceSelectedAfterDQandWindow.significance > sigThr));
% --- reorder according to significance
[tmp I] = sort(triggers.significance,'descend');
triggers = xclustersubset(triggers,I);

for iItf = 1:length(analysis.detectorList)
  % --- select triggers that have significant contribution from the given
  % itf
  iEnergy=find(strcmp(analysis.likelihoodType,['energyitf' num2str(iItf)]));
  if isempty(iEnergy)
    fprintf(fout,['<br> Skipping annoying triggers for ' ...
                  analysis.detectorList{iItf} '. Single detector energy column  ' ...
                  'not found\n'])
    triggersItf{iItf} = [];
    continue
  end
  if isempty(triggers.likelihood)
    triggersItf{iItf} = triggers;
    continue
  else
    triggersItf{iItf} = ...
      xclustersubset(triggers,...
                     find(triggers.likelihood(:,iEnergy)>energyNpixelsRatio* ...
                          triggers.nPixels));
  end
  
  % --- count a trigger found multiple times only once by taking the most
  % significant in a given second 
  % NB: unique time sorts the triggers
  if(not(isempty(triggersItf{iItf}.unslidTime)))
    % --- remove triggers that are seen only once
    [tmp I] = unique(floor(triggersItf{iItf}.unslidTime(:,iItf)),'last');
    triggersItf{iItf}=xclustersubset(triggersItf{iItf},setdiff(1:length(triggersItf{iItf}.peakTime),I));
    if isempty(triggersItf{iItf}.peakTime)
      break
    end
    % --- reorder according to energy
    [tmp I] = sort(triggers.likelihood(:,iEnergy),'descend');
    triggers = xclustersubset(triggers,I);
    % --- keep the most significant triggers in given second
    [tmp I] = unique(floor(triggersItf{iItf}.unslidTime(:,iItf)),'first');
    triggersItf{iItf}=xclustersubset(triggersItf{iItf},I);
    triggersInfo=[triggersItf{iItf}.unslidTime(:,iItf)...
                  triggersItf{iItf}.significance ...
                  triggersItf{iItf}.peakFrequency ...
                  triggersItf{iItf}.boundingBox(:,3) ...
                  sqrt(triggersItf{iItf}.likelihood(:,iEnergy))];
    f = fopen([analysis.grb_name '_' user_tag '_' analysis.type '_annoyingTriggers' analysis.detectorList{iItf} '.txt'],'w') ;
    fprintf(f,'#%s\t\t%s\t%s\t%s\t%s\n','GPS time','Significance',...
            'Frequency (Hz)','Duration (sec)','SNR');
    fprintf(f,'%.6f\t%.1f\t\t%.0f\t\t%.3f\t\t%.1f\n',triggersInfo');
    fclose(f);
    fprintf(fout,'<br><A HREF="%s">%s</a>',...
            [analysis.grb_name '_' user_tag '_' analysis.type '_annoyingTriggers' ...
             analysis.detectorList{iItf} '.txt'],...
            [analysis.grb_name '_' user_tag '_' analysis.type '_annoyingTriggers' ...
             analysis.detectorList{iItf} '.txt']);
  end
end % --- loop over ITFs
triggersItfAnnoying{2} = triggersItf;
clear triggers triggersInfo triggersItf

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- triggers passing cuts and above threshold that appear more than
%     once in coincidence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- select triggers above some significance threshold
triggers = xclustersubset(...
    offSourceSelectedAfterAllCuts,...
    find(offSourceSelectedAfterAllCuts.significance > sigThr));
% --- reorder according to significance
[tmp I] = sort(triggers.significance,'descend');
triggers = xclustersubset(triggers,I);

for iItf = 1:length(analysis.detectorList)
  % --- select triggers that have significant contribution from the given
  % itf
  iEnergy=find(strcmp(analysis.likelihoodType,['energyitf' num2str(iItf)]));
  if isempty(iEnergy)
    fprintf(fout,['<br> Skipping annoying triggers for ' ...
                  analysis.detectorList{iItf} '. Single detector energy column  ' ...
                  'not found\n'])
    triggersItf{iItf} = [];
    continue
  end
  if isempty(triggers.likelihood)
    triggersItf{iItf} = triggers;
    continue
  else
  triggersItf{iItf} = ...
      xclustersubset(triggers,...
                     find(triggers.likelihood(:,iEnergy)>energyNpixelsRatio* ...
                          triggers.nPixels));
  end
  % --- count a trigger found multiple times only once by taking the most
  % significant in a given second 
  % NB: unique time sorts the triggers
  if(not(isempty(triggersItf{iItf}.unslidTime)))
    % --- remove triggers that are seen only once
    [tmp I] = unique(floor(triggersItf{iItf}.unslidTime(:,iItf)),'last');
    triggersItf{iItf}=xclustersubset(triggersItf{iItf},setdiff(1:length(triggersItf{iItf}.peakTime),I));
    if isempty(triggersItf{iItf}.unslidTime)
      break
    end
    % --- reorder according to energy
    [tmp I] = sort(triggers.likelihood(:,iEnergy),'descend');
    triggers = xclustersubset(triggers,I);
    % --- keep the most significant triggers in given second
    [tmp I] = unique(floor(triggersItf{iItf}.unslidTime(:,iItf)),'first');
    triggersItf{iItf}=xclustersubset(triggersItf{iItf},I);
    triggersInfo=[triggersItf{iItf}.unslidTime(:,iItf)...
                  triggersItf{iItf}.significance ...
                  triggersItf{iItf}.peakFrequency ...
                  triggersItf{iItf}.boundingBox(:,3) ...
                  sqrt(triggersItf{iItf}.likelihood(:,iEnergy))];
    f = fopen([analysis.grb_name '_' user_tag '_' analysis.type '_veryAnnoyingTriggers' analysis.detectorList{iItf} '.txt'],'w') ;
    fprintf(f,'#%s\t\t%s\t%s\t%s\t%s\n','GPS time','Significance',...
            'Frequency (Hz)','Duration (sec)','SNR');
    fprintf(f,'%.6f\t%.1f\t\t%.0f\t\t%.3f\t\t%.1f\n',triggersInfo');
    fclose(f);
    fprintf(fout,'<br><A HREF="%s">%s</a>',...
            [analysis.grb_name '_' user_tag '_' analysis.type '_veryAnnoyingTriggers' ...
             analysis.detectorList{iItf} '.txt'],...
            [analysis.grb_name '_' user_tag '_' analysis.type '_veryAnnoyingTriggers' ...
             analysis.detectorList{iItf} '.txt']);
  end
end % --- loop over ITFs

triggersItfAnnoying{3} = triggersItf;
