function xspectrumplot(X,Y,legendText,analysis,plotName,...
         figures_dirName,figfiles_dirName)
% XSPECTRUMPLOT - Make and save a plot formatted for xmakegrbwebpage output.
%
% usage:
%
%  xspectrumplot(X,Y,legendText,analysis,plotName,...
%     figures_dirName,figfiles_dirName)
%
% X                 Vector of x axis values (Hz).
% Y                 Array of y axis values (Hz^{-1/2}).
% legendText        Cell array of strings.  Legend text.
% analysis          Struct containing analysis parameters.
% plotName          String to use in plot name when saving.  If not specified,
%                   then plot is not saved (hardly useful for writing a web
%                   page!)
% figures_dirName   String. Name of figures dir
% figfiles_dirName  String. Name of figfiles dir
%
% $Id$

% ---- Checks.
error(nargchk(4,7,nargin));

% ---- Create figure.
figure; set(gca,'FontSize',20);
loglog(X,Y,'linewidth',2);
axis([0.75*analysis.minimumFrequency 0.5*analysis.sampleFrequency 1e-24 1.01e-21])
grid;
legend(legendText,'Location','SouthEast')
xlabel('Frequency (Hz)')
ylabel('Amplitude (Hz^{-1/2})')

% ---- Save figure, if desired.
if (exist('plotName','var'))
    xsavefigure(plotName,figures_dirName,figfiles_dirName);
end

% ---- Done.
return
