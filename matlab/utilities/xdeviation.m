function [nstd, Imed] = xdeviation(E,I,A,C,maxE)
% XDEVIATION - compute distance of event from running median I(E).
%
% usage:
%
%   [nstd, Imed] = xdeviation(E,I,A,C,maxE)
%
%   E     Vector.
%   I     Vector of same size as E.
%   A     Scalar.
%   C     Scalar.
%   maxE  Scalar.
%
%   nstd  Vector of same size as E.
%   Imed  Vector of same size as E.
%
% xdeviation uses the fit parameters A, C supplied by xmedianfit to
% estimate the median value of Imed of I at each of the input E values.  It
% forces the estimated median, Imed, to go smoothly to the diagonal (I=E)
% at E=maxE. This fitting is done in log space.
%
% xdeviation then computes the "distance" nstd of each input (E,I) pair
% above the median, where nstd is defined as
%
%   nstd := (I - Imed) ./ I.^0.5;
%
% See xmedianfit.
%
% $Id$

% ---- Checks.
error(nargchk(5, 5, nargin));

% ---- Switch input from linear to log space.
X = log10(E);
Y = log10(I);
maxX = log10(maxE);

% ---- Quadratic fit to median.
Yquad = A*X.^2+C;

% ---- Construct "window" function that forces a smooth transition from the 
%      quadratic fit Y=A*X^2+C to the diagonal Y=X at X=maxX.
W = cos((X/maxX) * (pi/2)).^2;

% ---- Force smooth transition from quadratic to diagonal, in linear space.
%      (This seems to give nicer looking curves than the transition in log
%      space.)
Iquad = 10.^Yquad;
Imed = W.*(Iquad) + (1-W).*(E);
Ymed = log10(Imed);

% ---- Force median to be the diagonal above maxX.
k = find(X>=maxX);
Ymed(k) = X(k);

% ---- Force constant median for X<=0.
k = find(X<=0);
Ymed(k) = C;

% ---- Convert back to linear space.
Imed = 10.^Ymed;

% ---- Number of "standard deviations" of each point above the median.
nstd = (I - Imed)./I.^0.5;

% ---- Done.
return
