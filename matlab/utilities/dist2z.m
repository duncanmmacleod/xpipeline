function [z Jv]= dist2z(r,mode)
% DIST2Z - convert luminosity distance to redshift
%
% usage:
%     z = dist2z(r)
%
%   r    Vector. Luminosity distance in Mpc
%   mode String. What kind of distance-redshift conversion to use see
%        below. 
%
%   z    Vector. Coresponding redshift.
%   Jv   Vector. Comoving time-volume element jacobian Jv = dV/dr/(1+z) where dr is the luminosity
%        distance in Mpc, and 1/(1+z) takes into account the redshift of
%        the rate.
%
%  mode = 'linear'
%  convert distance to redshift using the linear relation of Hubble
%
%  mode = 'flat'
% convert luminosity distance to redshift in flat cosmology
% see equation (41) in Copeland et al. arxiv:hep-th/0603057.
% Assumes a 0.3 fraction of matter and 0.7 fraction of dark energy
% Use equations in astro-ph/9905116 to compute Jacobian


% base for volume element
Jv = 4*pi*r.^2;

switch mode
 case 'flat'
  % compute luminosity distance for a grid of redshifts and interpolate
  % back to get redshifts corresponding to requeseted luminosity distances
  zGrid = [0 logspace(-3,1,1000)];
  [dLumin DH] = z2dist(zGrid);
  z = interp1(dLumin,zGrid,r);
  % an additional 1/(1+z) comes from the comoving constant rate part
  Jv = Jv./(cosmoE(z).*(1+z).*r/DH + (1+z).^3)./(1+z);
 case 'linear'
  [tmp DH]= z2dist(1);
  z = r/DH;
 otherwise
  error(['Unrecognized mode: ' mode]);
end

% sanity check Jacobian
if any(Jv<0)
  error(['Jacobian gets negative, its minimum is at ' num2str(min(Jv(:)))]);
end

