function [seglist]=xreadseglist(segfile)

%  xmakeseglist - read in segment file, check for reading errors and
%  writes out array of segments (start, end, duration).
%  ADD FUNCTIONALITY TO DISCARD SEGMENTS NOT OVERLAPPING WITH USER
%  DEFINED REGION.
%
%  Usage:
%  [seglist]=xreadseglist(segfile)
%
%  segfile        String. Name of segment file. If 'None' we
%                 just return four arrays of 0 (segno start end dur).
%
%  seglist        Array of size (NumSegs,4) listing segno, start, end
%                 times and duration of segments  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(1, 1, nargin));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Reading segfile
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(['Reading from ' segfile]);

% ---- If we have a valid segfile name.
if ~strcmp(segfile,'None')

   fid = fopen(segfile);
   % ---- CHECK FORMATTING STRING, VARIES FILE TO FILE
   segdata = ...
      textscan(fid,'%f%f%f%f','commentstyle','#');
   fclose(fid);

   % ---- Checking that we have read some content
   if isempty(segdata{1})
      warning([segfile ' was empty!']);
      % ---- If segfile contained no segments we just return empty arrays.
      segments_segno    = 0; 
      segments_start = 0;
      segments_end   = 0;
      segments_dur   = 0;

   else

      % ---- textscan returns NaN for empty %f fields.  Reset these to zero.
      if any(isnan(segdata{3}))
         segdata{3} = zeros(size(segdata{3}));
      end

      if any(isnan(segdata{4}))
         segdata{4} = zeros(size(segdata{4}));
      end

      % ---- Check for 2 vs 4 columns.
      if (0==max(abs(segdata{3})) & 0==max(abs(segdata{3})))
         % ---- Only two columns supplied.  These must be start and stop.
         segments_start = segdata{1};
         segments_end   = segdata{2};
         segments_dur   = segments_end - segments_start;
         segments_segno = zeros(size(segments_start));
      elseif (max(abs(segdata{3}))>0 & max(abs(segdata{3}))>0 )
         % ---- All four columns supplied.
         segments_segno = segdata{1};
         segments_start = segdata{2};
         segments_end   = segdata{3};
         segments_dur   = segdata{4};
         if any(abs(segments_start+segments_dur-segments_end)>1e-3)
           mask = segments_start+segments_dur~=segments_end;
           format long
           [segments_start(mask)+segments_dur(mask) segments_end(mask)]
            error('Format of input file not recognized.');
         end
      else
         error('Format of input file not recognized.');
      end

   end

% ---- If segfile name was 'None' we just return empty arrays.
else
   segments_segno = 0;
   segments_start = 0;
   segments_end   = 0;
   segments_dur   = 0;
end

seglist = [ segments_segno segments_start segments_end segments_dur ];

% -- Done!
