function [ra_search_deg,dec_search_deg,probabilities,gridarea,coverage]=...
   xmakeskygrid(ra_ctr_deg,dec_ctr_deg,gps,...
   sigma_deg,nSigma,sites,delay_tol,outputfile,gridtype,verbose,opt_reduction)
% XMAKESKYGRID - tile the sky to cover circular error boxes
%
% XMAKESKYGRID - Given a set of estimated sky locations and sky location
% uncertainties, generate a list of sky locations which we should search
% over to keep time-delay errors below a given threshold for the specified
% network.
%
% NOTE: Note that the grid returned in the ra_search_deg, dec_search_deg
% vectors uses right ascension and declination coordinates in degrees,
% while the grid written to the outputfile (optional) is in Earth-fixed
% coordinates (theta,phi) in radians.
%
% usage:
%
% [ra_search_deg,dec_search_deg,probabilities,gridarea] = ...
%     xmakeskygrid(ra_ctr_deg,dec_ctr_deg,gps,sigma_deg, ...
%     nSigma,sites,delay_tol,outputfile,gridtype,verbose,opt_reduction)
%
%  ra_ctr_deg        Tilde-delimited string. Right ascension of centre of  
%                    error circles [deg], e.g., '178.1~172.3'.
%  dec_ctr_deg       Tilde-delimited string. Declination of centre of 
%                    error circles [deg], e.g., '-12.0~-22.2'.
%  gps               String. GPS time of trigger.
%  sigma_deg         Tilde-delimited string. 1-sigma size of error circles
%                    [deg]. This is 
%                    multiplied by nSigma (see below) to define a radius
%                    around each central point to be covered by the grid.
%                    A single value may be supplied, in which case this
%                    common value is used for all error circles. 
%  nSigma            String. Determines region about each central position
%                    to be covered by grid.
%  sites             String. Tilde-separated list of detector sites, 
%                    e.g., 'H~L~V'.
%  delay_tol         String. Maximum error in time delay [sec] allowed
%                    between any sky position in an error box and the
%                    closest grid point.  
%  outputfile        String, optional.  Name of output file to which to 
%                    write grid. The file will contain four columns:
%                    1. theta   Polar angle [rad] in the range [0, pi] with 
%                       0 at the North Pole/ z axis.
%                    2. phi     Azimuthal angle [rad]in the range [-pi, pi)  
%                       with 0 at Greenwich / x axis.
%                    3. pOmega  A priori probability associated with each 
%                       grid point.
%                    4. dOmega  Solid angle associated with each grid point.
%                    Note that these are Earth-fixed coordinates. The
%                    format follows that of the skyPositions array as
%                    described in sinusoidalMap.m. Specify 'None' for no
%                    file to be produced.
%  gridtype          String, optional.  Type of grid.  Recognized values:
%                      'circular'	Grid made of concentric rings centered
%                        on each input sky position, with redundant points
%                        discarded.  Made using SINUSOIDALMAP. 
%                      'healpix'    Single uniform all-sky grid, with
%                        points outside all error circles discarded.  Made
%                        using HEALPIX.
%                      'line'   Linear grid along the line of greatest time
%                        delay change spanning the error circle. Only valid
%                        for the case of a single input (ra,dec).  
%  verbose           String, optional.  Use '1' for verbose output.
%                    Default '0'. 
%  opt_reduction     String, optional.  Recognised values are 
%                      '0' - redundant points from overlapping grids are not
%                            eliminated. 
%                      '1' - redundant points from overlapping grids are 
%                            eliminated in order from most dense grid to least
%                            dense. 
%                      '2' - all possible orderings of grids are tested to find
%                            the one that gives the smallest combined grid -
%                            this may be slow!  
%                    Default '0'.  
%
%  ra_search_deg     Vector. Right ascensions [deg] of search grid points.    
%  dec_search_deg    Vector. Declinations [deg] of search grid points.
%  probabilities     Vector. Fisher probability of each sky position. For
%                    1-sigma errors above 360 degrees uniform probability
%                    is used.
%  gridarea          Vector. Area [steradians] of each grid point.
%  coverage          Vector. Maximum angle [deg] from the center of each of the
%                    corresponding error circles that is covered by the output
%                    grid. If the opt_reduction input is used, the value of
%                    coverage will be zero if the corresponding circular grid
%                    has been eliminated by the grid reduction procedure.
%
%  The probability for each grid point is the sum of the Fisher
%  probabilities for that grid point with each of the input central sky
%  positions.  
%
% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              Preliminaries.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check number of input args
error(nargchk(7, 11, nargin));

% ---- Assign default inputs.
if (nargin<11)
    opt_reduction = 0;
else
    opt_reduction = str2num(opt_reduction);
end

% ---- Convert input variables to appropriate type.
ra_ctr_deg      = tildedelimstr2numorcell(ra_ctr_deg);
ra_ctr_deg      = ra_ctr_deg(:);
dec_ctr_deg     = tildedelimstr2numorcell(dec_ctr_deg);
dec_ctr_deg     = dec_ctr_deg(:);
gps             = str2num(gps);
sigma_deg       = tildedelimstr2numorcell(sigma_deg);
sigma_deg       = sigma_deg(:);
nSigma          = str2num(nSigma);
sites           = tildedelimstr2numorcell(sites); 
delay_tol       = str2num(delay_tol);

% ---- Checks on input.
if ~isnumeric(ra_ctr_deg) | ~isnumeric(dec_ctr_deg) | ~isnumeric(sigma_deg)
    error('Unable to convert input right ascension, declination, and sigma to numbers.');
end

% ---- Length checks.
if length(ra_ctr_deg) ~= length(dec_ctr_deg) 
    error('Right ascension and declination must have same length.');
end
if length(sigma_deg) == 1
    sigma_deg = sigma_deg * ones(size(ra_ctr_deg));
elseif length(sigma_deg) ~= length(ra_ctr_deg)
    error('Sigma must be scalar or same length as right ascension, declination.');
end

% ---- Assign default arguments.
if (nargin<10)
    verbose = 0;
else
    verbose = str2num(verbose);
end
if (nargin<9)
   gridtype = 'circular';
end
if (nargin<8)
   outputfile = 'None';
end

% ---- Speed of light (in m/s).
speedOfLight = 299792458;

% ---- Calc radius of sky area we will search for GRBs.
skyPosError_deg = sigma_deg * nSigma;

% ---- Radius of sky area in radians.
skyPosError_rad = skyPosError_deg * pi/180; 

% ---- Ensure optional output is defined.
coverage = [];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          Get cartesian Earth-based coordinates (m) of detector 
%                       vertex for each site.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nSites = length(sites);
for iSite = 1:nSites
    det = LoadDetectorData(sites{iSite}(1));
    siteVertex(iSite,:) = det.V';
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    If only one site is present, use only one point for the error box.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nSites == 1
    coverage = 180*ones(size(ra_ctr_deg)); %-- deg
    % ---- Use first input sky location.
    ra_ctr_deg = ra_ctr_deg(1);
    dec_ctr_deg = dec_ctr_deg(1);
    % ---- Command-line output.
    ra_search_deg = ra_ctr_deg;
    dec_search_deg = dec_ctr_deg;
    probabilities = 1;
    gridarea = []; %-- dummy value that can't be mistaken for physical number
    % ---- File output.
    % ---- Convert trigger ra,dec to Earth-based phi,theta.
    [phi_ctr_rad, theta_ctr_rad] = radectoearth(ra_ctr_deg,dec_ctr_deg,gps);
    skyPositions = [theta_ctr_rad phi_ctr_rad 1 4*pi];
    dlmwrite(outputfile,skyPositions,'delimiter',' ','precision','%7.5f');      
    if verbose
        disp(['Single-site network: using only one grid point.']);
    end
    return
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Construct vector joining each pair of sites.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

iPair = 1; 
iSite1 = 1;
for iSite1 = 1:nSites-1
    for iSite2 = iSite1+1:nSites
       % ---- Construct structure listing names and displacements between
       %      pairs of sites.
       pairNames{iPair} = [sites{iSite1} sites{iSite2}];
       site1ToSite2(iPair,:) = siteVertex(iSite2,:) -  siteVertex(iSite1,:);
       % ---- Baseline in units of seconds.
       site1ToSite2_sec(iPair,:) = site1ToSite2(iPair,:) ./ speedOfLight;  
       iPair = iPair + 1;
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Construct vector representing direction to error box centres 
%                          from Earth's centre.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Convert trigger ra,dec to Earth-based phi,theta.
[phi_ctr_rad, theta_ctr_rad] = radectoearth(ra_ctr_deg,dec_ctr_deg,gps);

% ---- Construct the sky direction assumed from skyPosition_central.
earthToGRB = [sin(theta_ctr_rad).*cos(phi_ctr_rad)... % x
              sin(theta_ctr_rad).*sin(phi_ctr_rad)... % y
              cos(theta_ctr_rad)];                    % z


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      For each pair of sites calculate angle with GRB line of sight.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- For sites separated by distance delay_max [sec] have 
%        delay = delay_max cos(lambda)
%      Differentiating yields
%        ddelay = (-) alpha dlambda
%      where 
%        alpha := delay_max sin(lambda).
%      Here we find the baseline giving the largest value of alpha.
%      This will produce the most conservative spacing of sky positions
%      (dlambda) for a given tolerance in delay time error (ddelay).
Npair = length(pairNames);
for iPair = 1:Npair 

    % ---- Define some shorthand.
    v1 = site1ToSite2_sec(iPair,:).';
    v2 = earthToGRB;
    
    % ---- Check for 2 detectors at the same site (i.e., H1-H2).  In this
    %      case set alpha = 0; otherwise compute alpha.
    if (norm(v1)<1e-6)  %-- time delay of 1 microsec = 1000 foot baseline ...
        
        alpha(iPair) = 0;
        % ---- Dummy values for vebose output case.
        lambda_extrema = 0;
        lambda_opt = 0;
        
    else

        % ---- Invert dot product to calculate opening angle. 
        lambda_ctr_rad = acos(v2*v1/norm(v1));

        % ---- Lambda should be between 0 and pi.
        if max(lambda_ctr_rad) > pi | min(lambda_ctr_rad) < 0
            disp(['lambda_ctr_rad = ' num2str(lambda_ctr_rad(iPair))])
            error('lambda_ctr_rad should be between 0 and pi.')
        end

        % ---- Add (subtract) skyPosError to find extreme angles.
        lambda_extrema  = [lambda_ctr_rad - skyPosError_rad, ...
            lambda_ctr_rad + skyPosError_rad];

        % ---- We are looking to maximise sin(abs(lambda)).
        % ---- If our range of lambda excludes pi/2 then choose the 
        %      extrema closest to pi/2.
        [dummy,idx]=min(abs(pi/2-lambda_extrema),[],2);
        for ii = 1:length(idx)
            lambda_opt(ii) = lambda_extrema(ii,idx(ii));
        end
        % ---- If our range of lambda includes pi/2 this will maximise
        %      sin(abs(lambda)).
        ind = find(lambda_extrema(:,1) < pi/2 & lambda_extrema(:,2) > pi/2);
        if ~isempty(ind)
            lambda_opt(ind) = pi/2;
        end

        % ---- Compute alpha for this baseline.
        alpha(:,iPair) = norm(v1) * sin(lambda_opt(:));
        
    end
   
    % ---- Some output, if desired.
    if verbose
        fprintf(1,'For %s : \n', pairNames{iPair});
        fprintf(1,'Baseline time delay : %2.5f \n', norm(v1));
        fprintf(1,'lambda_opt : %2.5f \n', lambda_opt);
        fprintf(1,'alpha      : %2.5f \n', alpha(:,iPair));
    end

end % -- Loop over pairs.
clear v1 v2

% ---- Find largest alpha value (highest resolution) for each circle.
[alpha_max,maxPair] = max(alpha,[],2);
% ---- Use alpha_max to calculate most conservative angularResolution which
%      will be used to place out skyPositions.
angularResolution = 2 * delay_tol ./ alpha_max;
if verbose
    disp(['Desired angularResolution = ' num2str(angularResolution(:).') '.']);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Lay down grid of desired type.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch lower(gridtype)
  
    % ---- Construct grid of requested type. In each case the outputs should
    %      be the four-column array skyPositions (theta,phi,prob,area) and 
    %      the Nsky x Ncircle array overlap containing cos(lambda) where
    %      lambda is the angle between each grid point and all circle
    %      centers (needed later for the probability calculation). The
    %      values of skyPositions(:,3) may be dummy values; the real
    %      probabilities will be calculated later. 

    case 'circular'

        % ---- Use separate circular grids for each error circle, removing
        %      overlap as needed. 
        % ---- Number of error circles.
        Ncircle = size(earthToGRB,1);
        % ---- Number of points in each circular grid.
        Ngrid = zeros(Ncircle,1);
        % ---- Prepare storage.
        skyPositions = [];  

        % ---- Lay down grid for each error circle.
        for ii = 1:Ncircle

            % ---- Place skyPositions about North Pole in sky patch with
            %      radius = skyPosError_rad. 
            %      3rd arg means we will place point at North Pole.
            %      4th arg means we will use 'optimal' spacing in theta.
            tempPositions = sinusoidalMap(angularResolution(ii),skyPosError_rad(ii),1,1);
            theta         = tempPositions(:,1);
            phi           = tempPositions(:,2);
            pOmega        = tempPositions(:,3);
            dOmega        = tempPositions(:,4);
            Ngrid(ii)     = length(theta);  
            % ---- Record the largest angular distance from the centre actually covered by the grid. 
            tmp = [0:angularResolution(ii):(skyPosError_rad(ii)+angularResolution(ii)/2)];
            coverage_rad(ii,1) = (tmp(end)+angularResolution(ii)/2);

            % ---- Rotate skyPositions to center them on each error circle.
            % ---- Convert from spherical coords to cartesian for convenience.
            V = CartesianPointingVector(phi,theta);

            % ---- The first vector in V should point to the North Pole.
            %      Figure out opening angle between this and the GRB.
            v1 = V(1,:);
            if dot(v1,[0,0,1])~=1
                error('First point in non-rotated skyPositions map should be North Pole');
            end
            
            v2 = earthToGRB(ii,:);

            % ---- Construct vector which we will rotate skyPositions around.
            v3 = cross(v1,v2);

            % ---- Angle which we will rotate skyPositions about v3 by.
            psi = acos(dot(v1,v2)/(norm(v1)*norm(v2)));

            % ---- Loop over all skyPositions rotating them by psi about v3.
            Vrot = zeros(Ngrid(ii),3);
            for iVec = 1:size(V,1)
                Vrot(iVec,:) = RotateVector(V(iVec,:),v3,psi);   
            end

            % ---- Convert rotated skyPositions back to spherical coordinates
            %      and replace original skyPositions with rotated ones.
            skyPositions = [skyPositions; ...
                acos(Vrot(:,3)) atan2(Vrot(:,2),Vrot(:,1)) pOmega dOmega];

        end
        
        % ---- Compute angle between each grid point and all circle centers
        %      (also needed for probability calculation).
        V = CartesianPointingVector(skyPositions(:,2),skyPositions(:,1));
        % ---- Location of error circle centers.
        C = CartesianPointingVector(phi_ctr_rad,theta_ctr_rad);
        % ---- cos(Angle between grid points and error circle centers)
        overlap = V*C';
        overlap_angle = acos(overlap);
                
        % ---- Check for and eliminate redundant grid points from overlapping
        %      circles, if requested. Redundant points are defined as a grid
        %      point from one circle whose area of coverage is fully covered by
        %      another grid. 
        if opt_reduction
            if verbose
                disp(['Calling optimalgridreduction for sky grid with ' num2str(Ncircle) ...
                    ' circles and ' num2str(size(skyPositions,1)) ' points...'])
            end
            [skyPositions, overlap, overlap_angle, I, coverage_rad] = optimalgridreduction(Ngrid, ...
                angularResolution,skyPosError_rad,coverage_rad,overlap_angle,skyPositions,opt_reduction);
            if verbose
                disp(['   ... retaining ' num2str(sum(coverage_rad>0)) ' circles and ' ...
                    num2str(size(skyPositions,1)) ' points.'])
            end
        end
        % ---- Record grid coverage (in original circle order) in deg for output.
        coverage = coverage_rad*180/pi;

    case 'healpix'
        
        % ---- Use healpix to generate all-sky grid with desired
        %      angularResolution, then discard points outside error
        %      circles.  Gives a nice regular grid with meaningful area for
        %      each point, but density may be up to 4 x higher than needed
        %      due to the limited possible resolutions of the healpix grid. 
        % ---- Number of points to cover sky at desired resolution -- based
        %      on finest angular resolution needed for all error boxes. 
        Ntotal = 4*pi/min(angularResolution)^2;
        % ---- Make smallest healpix grid with this many points. The number
        %      of points in a healpix grid is 12*2.^(2*n) where n = 0,1,2,
        %      ... . So the smallest n with the required number of grid
        %      points is
        n = ceil(1/2*log2(Ntotal/12));
        Nhealpix = 12*2.^(2*n);
        disp(['Using healpix grid with n = ' num2str(n) ' (' ...
            num2str(Nhealpix) ' sky positions, angular resolution = ' ...
            num2str((4*pi/Nhealpix).^0.5) ').']);
        [coordinates, solidAngles, probabilities] = healpix(n);
        skyPositions = [coordinates, probabilities, solidAngles];

        % ---- Unit three-vectors pointing to each grid point.
        V = CartesianPointingVector(skyPositions(:,2),skyPositions(:,1));
        % ---- Location of error circle centers.
        C = CartesianPointingVector(phi_ctr_rad,theta_ctr_rad);
        % ---- Conpute overlap for all error circles.
        overlap = V*C';
        
        % ---- Throw away grid points outside all error circles.
        % pass = max(overlap >= cos(skyPosError_rad),[],2);
        minOverlap = repmat(cos(skyPosError_rad+angularResolution).',size(overlap,1),1);
        index = find(max(overlap >= minOverlap,[],2));
        skyPositions = skyPositions(index,:);
        overlap = overlap(index,:); %-- keep for probability calculations

    case 'line'
        
        % ---- Note: the 'line' option is only intended for the case of a
        %      single central (ra,dec).
        if length(ra_ctr_deg) > 1
            error('gridtype ''line'' requires a single input ra,dec.');
        end

        % ---- We will tile a line centered on the GRB position. There will
        %      be enough grid points on either side to just exceed the
        %      specified containment radius.
        %      Determine the number of grid points to be laid on each side
        %      of the central GRB position.
        numGridPoints = ceil(skyPosError_rad / angularResolution);

        % ---- Lay the grid parallel to the baseline of the detector pair
        %      that requires ther most conservative spacing of sky positions.
        %      We lay the grid by defining a rotation axis orthogonal to
        %      both this baseline and the GRB direction, then rotating in
        %      steps of +/- angularResolution about this axis away from the
        %      GRB central position.

        % ---- Construct axis unit vector "A" orthogonal to baseline and
        %      GRB direction.
        v1 = site1ToSite2_sec(maxPair,:) / norm(site1ToSite2_sec(maxPair,:));
        v2 = earthToGRB;
        A = cross(v2,v1);

        % ---- Start at the GRB sky position.
        skyPositions(1,:) = v2;
        % ---- Add new sky positions by rotating the GRB vector around A.
        if numGridPoints > 1
            % ---- First go in the positive direction, starting from the GRB.
            for i=2:numGridPoints+1
                skyPositions(i,:) = RotateVector(skyPositions(i-1,:), A, angularResolution);
            end
            % ---- Now go in the negative direction, again starting from the GRB.
            skyPositions(numGridPoints+2,:) = RotateVector(skyPositions(1,:), A, -1*angularResolution);
            for i=numGridPoints+3:2*numGridPoints+1
                skyPositions(i,:) = RotateVector(skyPositions(i-1,:), A, -1*angularResolution);
            end
        end

        % ---- Save Cartesian coordinates for later.
        skyPositions_Cartesian = skyPositions;

        % ---- Convert skyPositions from Cartesian coordinates to spherical coordinates.
        theta = acos(skyPositions(:,3));
        phi   = atan2(skyPositions(:,2),skyPositions(:,1));
        skyPositions(:,1) = theta;
        skyPositions(:,2) = phi;
        
        % ---- Assign storage for grid point probabilities; the values will
        %      be assigned later. 
        skyPositions(:,3) = 0;

        % ---- Assign effective grid area as square with side equal to the
        %      angularResolution.
        %      Note: The historical area calculation used in
        %      xchooseskylocations1 was incorrect. But it was also not
        %      output by the function!
        skyPositions(:,4) = angularResolution^2 * ones(size(skyPositions(:,1)));
        
        % ---- cos(Angle between grid points and error circle center)
        overlap = skyPositions_Cartesian*v2';

    otherwise
        
        error(['gridtype ' gridtype ' not recognised.'])
        
end

% ---- Some output, if desired.
if verbose
    fprintf(1,'We place %d grid points.\n', size(skyPositions,1));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   Calculate probability of sky positions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- If provided error is absurdly large use a uniform distribution.
if any(sigma_deg > 360)

    warning(['The provided 1-sigma error is greater than 360 degrees, using ' ...
             'a uniform penalty instead of a Fisher penalty']);
    skyPositions(:,3) = 1;

% elseif strcmpi(gridtype,'line')
%     
%     % ---- Historically we have treated the probability for line grid
%     %      points using a Gaussian distribution (rather than a Fisher
%     %      distribution). The following code is copied from the
%     %      now-deprecated function xchooseskylocations1. NOTE THAT THIS
%     %      HISTORICAL CALCULATION IS INCORRECT: it computes the angular
%     %      distance of each grid point from the centre using only the theta
%     %      coordinate, not the full angular distance on the sphere.  
%     warning('Using incorrect historical probability calculation.')
%     
%     % ---- We assume that the probabilities of sky positions follow a   
%     %      normal distribution and fall of as the opening angle with the
%     %      central GRB positions increases.
%     % ---- We will rotate so that the North Pole on the skyPositions map
%     %      will point at the GRB. In the non-rotated skyPositions map
%     %      this enables us to use the polar angle theta as the opening
%     %      angle between a given skyPosition and the GRB.
%     % ---- The skyPosSigma is provided as the 68% containment radius. There is a
%     %      0.66 factor to convert it into the parameter sigma of the 2D gaussian
%     %      distribution of sky locations.
%     skyPosSigma_rad = sigma_deg * (pi/180) * 0.66;
%     theta_GRB = acos(earthToGRB(1,3));
%     probabilities = normpdf(theta,theta_GRB,skyPosSigma_rad);
%     % ---- Normalise probabilities to 1 at best sky position
%     probabilities = probabilities / normpdf(theta_GRB,theta_GRB,skyPosSigma_rad);
%     % ---- Replace the prob column in skyPositions array.
%     skyPositions(:,3) = probabilities;
    
else
    
    % ---- Otherwise, assume that the probabilities of sky positions follow a Fisher 
    %      distribution in the angle from the center of each error circle. Note
    %      that for each grid point we add the Fisher probabilities for each
    %      error circle.

    % ---- Compute the approximate kappa to get the 68% containment for the 
    %      Fisher distribution.
    sigma = sigma_deg(:).' * (pi/180);
    kappa = (0.66 * sigma).^(-2);
    % ---- Verify that the approximation is not too bad.
    p1sigma = 0.68;
    containmentRadius = acos((kappa+log(1-p1sigma+p1sigma*exp(-2*kappa)))./kappa);
    if any(abs(containmentRadius-sigma) > 0.3 * sigma)
        error(['Discrepency between desired and generated containment ' ...
            'radius is greater than 30%'])
    end
    if any(abs(containmentRadius-sigma) > 0.1 * sigma)
        warning(['Discrepency between desired and generated containment ' ...
            'radius is greater than 10%'])
    end
    % ---- Compute probabilities, summing over all error circles.  Note that
    %      fisherpdf returns pdf for "polar" angle out from source, so to get
    %      2D probability distribution we need to divide by
    %      2*pi*sin(polar_angle).  Finally, we rescale so that the peak value
    %      is unity.
    kappa = repmat(kappa,size(skyPositions,1),1);
    skyPositions(:,3) = sum(exp(kappa.*(overlap-1)),2);
    skyPositions(:,3) = skyPositions(:,3) / max(skyPositions(:,3));
    % skyPositions(:,3) =  sum(kappa./(1-exp(-2*kappa)).*exp(kappa.*(overlap-1)),2)/...
    %     (kappa./(1-exp(-2*kappa)));

end % -- choice between Fisher and uniform distribution


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   Compute ra,dec for output arguments.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Generate output arguments.
[ra_search_deg, dec_search_deg] = earthtoradec(skyPositions(:,2),skyPositions(:,1),gps);
probabilities = skyPositions(:,3);
gridarea = skyPositions(:,4);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              Write output file in Earth-fixed coordinates.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~strcmp(outputfile,'None')
    dlmwrite(outputfile,skyPositions,'delimiter',' ','precision','%7.5f');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Finished requred calculations. Optionally make plots & run tests.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if verbose
    
    % ---- Plot grid points and error circle centers.
    %
    % ---- Convert to Cartesian coordinates for ease of plotting.
    V = CartesianPointingVector(skyPositions(:,2),skyPositions(:,1));
    C = CartesianPointingVector(phi_ctr_rad,theta_ctr_rad);
    %
    figure(4); set(gca,'fontsize',16)
    plot3(V(:,1),V(:,2),V(:,3),'b.');
    hold on
    plot3(1.03*C(:,1),1.03*C(:,2),1.03*C(:,3),'m.','markersize',20);
    axis([-1.1 1.1 -1.1 1.1 -1.1 1.1])
    axis square
    grid on 
    xlabel('x');
    ylabel('y');
    zlabel('z');
    legend('grid','error circle centre');

    if strcmpi(gridtype,'line')
        % ---- Add "dominant" baseline.
        baseline = site1ToSite2_sec(maxPair,:);
        baseline = baseline/norm(baseline);
        plot3([-1;1]*baseline(:,1),[-1;1]*baseline(:,2),[-1;1]*baseline(:,3),'k-','linewidth',2);
        plot3([0;earthToGRB(:,1)],[0;earthToGRB(:,2)],[0;earthToGRB(:,3)],'m-','LineWidth',2);
        legend('grid points','error circle centre','baseline');
    end
        
end


% ---- Done.
return


% ------------------------------------------------------------------------------

function [skyPositions_min, overlap_min, overlap_angle_min, perm_min, coverage_rad_min] = optimalgridreduction(Ngrid_orig,...
    angularResolution_orig,skyPosError_rad_orig,coverage_rad_orig,overlap_angle_orig,skyPositions_orig,opt_reduction)
% optimalgridreduction - eliminate redundant points from overlapping circular grids

% ---- Assign defaults.
if nargin<7
    % ---- Default to ordering by angular resolution.
    opt_reduction = 1;
end

% ---- Number of circles.
Ncircle = length(Ngrid_orig);

% ---- Repack overlap_angle and skyPositions into cell arrays with one element
%      for each circle, for convenience.
overlap_angle_cell{1} = overlap_angle_orig(1:Ngrid_orig(1),:);
skyPositions_cell{1}  = skyPositions_orig(1:Ngrid_orig(1),:);
index = cumsum(Ngrid_orig);
for ii=2:Ncircle
    index = cumsum(Ngrid_orig);
    index = [index(ii-1)+1:index(ii)];
    overlap_angle_cell{ii,1} = overlap_angle_orig(index,:);
    skyPositions_cell{ii,1}  = skyPositions_orig(index,:);
end

if opt_reduction == 1
    % ---- Order in which to lay down circles: least dense to most dense.  This
    %      tends to give the smallest number of remaining grid points, since we
    %      throw away points from later circles that overlap earlier circles.
    %      However, there is no guarantee this will give the absolute smallest 
    %      grid, particularly for small grid sizes.
    % ---- Sort by angular resolution (default).
    [~,perm] = sort(angularResolution_orig,'descend');
    perm = perm(:).';
elseif opt_reduction == 2
    % ---- Make optimally reducted grid by exhaustively testing all grid
    %      orderings. This may be slow!
    perm = perms(1:Ncircle);
else
    error(['Input value opt_reduction = ' num2str(opt_reduction) ' not recognised.'])
end

% ---- Loop over all permutations and compute final number of grid points for each case.
%      Initialise size of each grid to one more than the original number of points for convenience.
min_grid_size = sum(Ngrid_orig)+1;
grid_size = zeros(size(perm,1),1);
for jperm = 1:size(perm,1)

    % ---- Make a copy of the coverage vector with its original ordering. We
    %      zero out entries in this copy if entire circles are deleted by the
    %      reduction procedure.
    coverage_rad_orig_copy = coverage_rad_orig;

    % ---- Re-order grid data for this permutation.
    Ngrid             = Ngrid_orig(perm(jperm,:));
    angularResolution = angularResolution_orig(perm(jperm,:));
    skyPosError_rad   = skyPosError_rad_orig(perm(jperm,:));
    coverage_rad      = coverage_rad_orig(perm(jperm,:));
    skyPositions      = cell2mat(skyPositions_cell(perm(jperm,:)));
    overlap_angle     = cell2mat(overlap_angle_cell(perm(jperm,:)));
    % ---- Don't forget to re-order the columns over overlap_angle, as these
    %      correspond to the order of the circles.
    overlap_angle     = overlap_angle(:,perm(jperm,:));

    % ---- Loop over circles and eliminate redundant grid points. 
    for ii = Ncircle:-1:2
        index = cumsum(Ngrid);
        index = [index(ii-1)+1:index(ii)];
        if length(index) == 1
            delta = skyPosError_rad(ii);
        else
            delta = angularResolution(ii)/2;
        end
        drop = find(sum(overlap_angle(index,1:ii-1)+delta<repmat(coverage_rad(1:ii-1).',length(index),1),2));
        skyPositions(index(drop),:) = [];
        overlap_angle(index(drop),:) = [];
        % ---- If we have dropped the entire circle (which can happen if it is
        %      fully covered by nearby circles) then we should reset its
        %      coverage to zero. (We don't delete the entry so that each of the
        %      input ra,dec error circles has a corresponding coverage.) 
        if length(drop)==length(index)
            coverage_rad_orig_copy(perm(jperm,ii)) = 0;
        end
    end
    % ---- Record size of the final grid.
    grid_size(jperm,1) = size(overlap_angle,1);
    
    % ---- If this is the smallest grid yet then save the data for output.
    if grid_size(jperm,1) < min_grid_size
        min_grid_size     = grid_size(jperm,1);
        perm_min          = perm(jperm,:);
        skyPositions_min  = skyPositions;
        overlap_angle_min = overlap_angle;
        coverage_rad_min  = coverage_rad_orig_copy;
    end
    
end

% ---- Compute remaining required output for smallest grid.
overlap_min = cos(overlap_angle_min);

