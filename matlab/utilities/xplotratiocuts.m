function [numCuts] = xplotratiocuts(ratioArray,deltaArray,analysis,pairIdx,fout,writeText)
% XPLOTRATIOCUTS - plot ratio cut thresholds.
%
% usage:
%  
% [numCuts] = xplotratiocuts(ratioArray,deltaArray,analysis,pairIdx,fout, ...
%     writeText)
%  
% ratioArray    Square array with the same number of columns as the 
%               likelihood variable. Each nonzero element is a threshold
%               to be applied to the ratio of two likehoods; see below.
% deltaArray    Square array with the same number of columns as the 
%               likelihood variable. Each nonzero element is added to the
%               likelihood in the denominator of the ratio test.
% analysis      Structure. Contains parameters of data analysis, see 
%               xmakegrbwebpage
% pairIdx       Integer. Identifies which likelihoods in 
%               analysis.likelihoodPairs we will plot
% fout          File identifier of file to which we will direct text output
% writeText     Integer. Set to 1 to write text on veto ratios to fout 
%  
% numCuts       Integer. Number of ratio cut lines plotted, useful when
%               creating legends
%
% $Id$

% ---- Checks.
error(nargchk(6,6,nargin));

% ---- plot ratio cut lines over large range
%      axes might increase when we add in injections
Xvec = 1:10^5;

% ---- initialise number of cuts plotted to zero
numCuts = 0;

% ---- do the relevant plotting
if (ratioArray(analysis.likelihoodPairs(pairIdx).energy,...
               analysis.likelihoodPairs(pairIdx).inc ) > 0)     % E/I +ve 
   % ---- ratioArray is +ve we do a two sided test
   numCuts = 2; 

   % test         : E/(I + delta) >= ratio
   if writeText == 1 
      fprintf(fout, '%s\n', [ char(analysis.likelihoodType(analysis.likelihoodPairs(pairIdx).energy)) ' / (' ...
                              char(analysis.likelihoodType(analysis.likelihoodPairs(pairIdx).inc)) '+' ...
                              num2str(deltaArray(analysis.likelihoodPairs(pairIdx).energy,...
                                                 analysis.likelihoodPairs(pairIdx).inc)) ...
                              ') >= ' num2str(ratioArray(analysis.likelihoodPairs(pairIdx).energy,...
                                                         analysis.likelihoodPairs(pairIdx).inc))]);
      fprintf(fout, '%s\n', '<br>');
   end

   % plot line    : I = (E / ratio) - delta
   plot(Xvec,(Xvec / ratioArray(analysis.likelihoodPairs(pairIdx).energy,analysis.likelihoodPairs(pairIdx).inc)) ...
                   - deltaArray(analysis.likelihoodPairs(pairIdx).energy,analysis.likelihoodPairs(pairIdx).inc),'--m','LineWidth',2);

   % test         : I/(E + delta) >= ratio
   if writeText == 1 
      fprintf(fout, '%s\n', [ char(analysis.likelihoodType(analysis.likelihoodPairs(pairIdx).inc)) ' / (' ...
                              char(analysis.likelihoodType(analysis.likelihoodPairs(pairIdx).energy)) '+' ...
                              num2str(deltaArray(analysis.likelihoodPairs(pairIdx).energy,analysis.likelihoodPairs(pairIdx).inc)) ...
                              ') >= ' num2str(ratioArray(analysis.likelihoodPairs(pairIdx).energy,analysis.likelihoodPairs(pairIdx).inc))]);
      fprintf(fout, '%s\n', '<br>');
   end

   % plot line    : I = (E + delta) * ratio
   plot(Xvec,(Xvec + deltaArray(analysis.likelihoodPairs(pairIdx).energy,analysis.likelihoodPairs(pairIdx).inc)) ...
                   * ratioArray(analysis.likelihoodPairs(pairIdx).energy,analysis.likelihoodPairs(pairIdx).inc),    '--m','LineWidth',2);

elseif (ratioArray(analysis.likelihoodPairs(pairIdx).inc,analysis.likelihoodPairs(pairIdx).energy) > 0) % I/E +ve  
   % ---- ratioArray is +ve we do a two sided test
   numCuts = 2;

   % test         : E/(I + delta) >= ratio
   if writeText == 1 
      fprintf(fout, '%s\n', [ char(analysis.likelihoodType(analysis.likelihoodPairs(pairIdx).energy)) ' / (' ...
                              char(analysis.likelihoodType(analysis.likelihoodPairs(pairIdx).inc)) '+' ...
                              num2str(deltaArray(analysis.likelihoodPairs(pairIdx).inc,analysis.likelihoodPairs(pairIdx).energy)) ...
                              ') >= ' num2str(ratioArray(analysis.likelihoodPairs(pairIdx).inc,analysis.likelihoodPairs(pairIdx).energy))]);
      fprintf(fout, '%s\n', '<br>');
   end 
   % plot line    : I = (E / ratio) - delta
   plot(Xvec,(Xvec / ratioArray(analysis.likelihoodPairs(pairIdx).inc,analysis.likelihoodPairs(pairIdx).energy)) ...
                   - deltaArray(analysis.likelihoodPairs(pairIdx).inc,analysis.likelihoodPairs(pairIdx).energy),'--m','LineWidth',2);

   % test         : I/(E + delta) >= ratio
   if writeText == 1 
      fprintf(fout, '%s\n', [ char(analysis.likelihoodType(analysis.likelihoodPairs(pairIdx).inc)) ' / (' ...
                              char(analysis.likelihoodType(analysis.likelihoodPairs(pairIdx).energy)) '+' ...
                              num2str(deltaArray(analysis.likelihoodPairs(pairIdx).inc,analysis.likelihoodPairs(pairIdx).energy)) ...
                              ') >= ' num2str(ratioArray(analysis.likelihoodPairs(pairIdx).inc,analysis.likelihoodPairs(pairIdx).energy))]);
      fprintf(fout, '%s\n', '<br>');
   end
   % plot line    : I = (E + delta) * ratio
   plot(Xvec,(Xvec + deltaArray(analysis.likelihoodPairs(pairIdx).inc,analysis.likelihoodPairs(pairIdx).energy)) ...
                   * ratioArray(analysis.likelihoodPairs(pairIdx).inc,analysis.likelihoodPairs(pairIdx).energy),    '--m','LineWidth',2);

elseif (ratioArray(analysis.likelihoodPairs(pairIdx).energy,analysis.likelihoodPairs(pairIdx).inc) < 0) % E/I -ve 
   % ---- ratioArray is -ve we do a one sided test 
   numCuts = 1;

   % test         : E/(I + delta) >= ratio
   if writeText == 1 
      fprintf(fout, '%s\n', [ char(analysis.likelihoodType(analysis.likelihoodPairs(pairIdx).energy)) ' / (' ...
                              char(analysis.likelihoodType(analysis.likelihoodPairs(pairIdx).inc)) '+' ...
                              num2str(deltaArray(analysis.likelihoodPairs(pairIdx).energy,analysis.likelihoodPairs(pairIdx).inc)) ...
                             ') >= ' num2str(abs(ratioArray(analysis.likelihoodPairs(pairIdx).energy,analysis.likelihoodPairs(pairIdx).inc)))]);
      fprintf(fout, '%s\n', '<br>');
   end
   % plot line    : I = (E / ratio) - delta
   plot(Xvec, (Xvec / abs(ratioArray(analysis.likelihoodPairs(pairIdx).energy,analysis.likelihoodPairs(pairIdx).inc))) ...
                        - deltaArray(analysis.likelihoodPairs(pairIdx).energy,analysis.likelihoodPairs(pairIdx).inc),'--m','LineWidth',2);

elseif (ratioArray(analysis.likelihoodPairs(pairIdx).inc,analysis.likelihoodPairs(pairIdx).energy) < 0) % I/E -ve  
   % ---- ratioArray is -ve we do a one sided test  
   numCuts = 1;

   % test         : I/(E + delta) >= ratio
   if writeText == 1 
      fprintf(fout, '%s\n', [ char(analysis.likelihoodType(analysis.likelihoodPairs(pairIdx).inc)) ' / (' ...
                              char(analysis.likelihoodType(analysis.likelihoodPairs(pairIdx).energy)) '+' ...
                              num2str(deltaArray(analysis.likelihoodPairs(pairIdx).inc,analysis.likelihoodPairs(pairIdx).energy)) ...
                              ') >= ' num2str(abs(ratioArray(analysis.likelihoodPairs(pairIdx).inc,analysis.likelihoodPairs(pairIdx).energy)))]);
      fprintf(fout, '%s\n', '<br>');
   end
   % plot line    : I = (E + delta) * ratio
   plot(Xvec,(Xvec + deltaArray(analysis.likelihoodPairs(pairIdx).inc,analysis.likelihoodPairs(pairIdx).energy)) ...
               * abs(ratioArray(analysis.likelihoodPairs(pairIdx).inc,analysis.likelihoodPairs(pairIdx).energy)),'--m','LineWidth',2);
end

return;
