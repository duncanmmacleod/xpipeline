function [phi,theta] = CartesiantoSpherical(V)
% CARTESIANTOSPHERICAL - convert cartesians 3-vectors to spherical coordinates
%
% usage :
%
% [phi,theta] = CartesiantoSpherical(V)
%
%    V : coordinates map, size must be 3,N (x,y,z)
%
%    phi : Vector of azimuthal angles (radians)
%    theta : Vector of polar angles (radians).

%  N. Leroy (leroy@lal.in2p3.fr)

% ---- compute the radius, if unit vector, must be 1
rho=sqrt(V(:,1).*V(:,1)+V(:,2).*V(:,2)+V(:,3).*V(:,3));

% --- just for debugging purpose
%min(rho)
%max(rho)

% --- compute distance in the x-y plane
Sxy=sqrt(V(:,1).*V(:,1)+V(:,2).*V(:,2));

% --- compute theta 
theta=acos(V(:,3)./rho);

% --- compute phi
phi=[];

for i=1:size(Sxy,1)
if V(i,1) >= 0
phi = [phi,asin(V(i,2)/Sxy(i))];
else
phi = [phi,(pi-asin(V(i,2)/Sxy(i)))];
end

end

% ---- Done
return