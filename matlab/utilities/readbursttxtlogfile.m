function [inj, ifo] = readbursttxtlogfile(fileName,Nifo,startTime,duration)
% readbursttxtlogfile - read a text formatted burst injection log file 
%
% usage: 
%
% [inj, ifo] = readbursttxtlogfile(fileName,Nifo,startTime,duration)
%
% fileName   String. Name of the text log file to be read.
% Nifo       Whole number. Number of detectors the log file describes. 
% startTime  Optional scalar. GPS start time from which to begin reading.
% duration   Optional scalar. Time interval [sec] over which to read log.
%
% inj        Struct. Each field corresponds to one of the columns in the log
%            file.
% ifo        Cell array of strings. Names of the detectors in the log file.
%
% If startTime and duration are supplied, then only injections occuring in the
% interval [startTime, startTime + duration] are returned.
%
% WARNING: The header information in the burst text log files is unreliable. You
% are advised to inspect the files manually and compare to the structure assumed
% in the code below before relying on the output of this function.
% 
% WARNING: O2 logs seem to record ra [rad] instead of phi. This function assumes 
% this and converts from (ra,dec) to Eath-fixed coordinates.
%
% See READBURSTXMLOGFILE for a reader capable of parsing header information.
%
% $Id$

% ---- Read file.
fid = fopen(fileName,'r');
formatStr = '%s %f %f %f %f %f %f %f %f %f %f %s %f %f %f ';
formatStr = [formatStr repmat('%s %f %f %f ',1,Nifo)];
C = textscan(fid, formatStr, 'CommentStyle', '#');
fclose(fid);

% ---- Optionally return only events in a specified time interval.
%      C{11} is the time at LLO. Use this to select injections. 
if nargin>=4
    idx = find(C{11}>=startTime & C{11}<=(startTime+duration));
    for ii=1:length(C)
        C{ii} = C{ii}(idx,:);
    end
end

% ---- Parse contents.
inj.SimID       = C{1};
inj.SimHrss     = C{2};  %-- always 5e-23
inj.SimEgwR2    = C{3};  %-- always zero
inj.Ampl        = C{4};  %-- always 5e-23
inj.ciota       = C{5};  %-- cos(inclination); i.e., cos(polar angle of detector in source frame)
inj.iota        = acos(C{5});
inj.internal_x  = C{6};  %-- always zero (apparently azimuthal angle of detector in source frame)
inj.ctheta      = C{7};
inj.theta       = acos(C{7});
inj.phi         = C{8};
inj.psi         = C{9};
inj.frameGPS    = C{10}; %-- always integer
inj.gps         = C{11}; %-- time at LLO , always >= frameGPS
inj.name        = C{12};
inj.hphp        = C{13};
inj.hchc        = C{14};
inj.hphc        = C{15};
% ---- Extract IFO names and IFO-specific data.
ifo = cell(1,Nifo);
for ii=1:Nifo
    tmp = C{16+(ii-1)*4};
    ifo{ii} = tmp{1};
    switch ifo{ii}
        case 'G1'
            inj.gpsG = C{17+(ii-1)*4};
            inj.FpG  = C{18+(ii-1)*4};
            inj.FcG  = C{19+(ii-1)*4};
        case 'H1'
            inj.gpsH = C{17+(ii-1)*4};
            inj.FpH  = C{18+(ii-1)*4};
            inj.FcH  = C{19+(ii-1)*4};
        case 'K1'
            inj.gpsK = C{17+(ii-1)*4};
            inj.FpK  = C{18+(ii-1)*4};
            inj.FcK  = C{19+(ii-1)*4};
        case 'L1'
            inj.gpsL = C{17+(ii-1)*4};
            inj.FpL  = C{18+(ii-1)*4};
            inj.FcL  = C{19+(ii-1)*4};
        case 'V1'
            inj.gpsV = C{17+(ii-1)*4};
            inj.FpV  = C{18+(ii-1)*4};
            inj.FcV  = C{19+(ii-1)*4};
    end
end

% ---- O2 logs seem to record ra [rad] instead of phi. Check the GPS time of the
%      first injection and convert if required.
%         -bash-4.2$ lalapps_tconvert 1 Jan 2019 15:00 UTC
%         1230390018
%         -bash-4.2$ lalapps_tconvert 1 Jan 2019 00:00 UTC
%         1230336018
%         -bash-4.2$ lalapps_tconvert 1 Jan 2016 00:00 UTC
%         1135641617
if inj.frameGPS(1) < 1230390018
    % ---- O2 logs recorded ra,sin(dec) instead of phi,cos(theta). Fix. Note that
    %      sin(dec) = cos(theta) so we only need to fix the phi data.
    disp('Pre-O3 log: assuming phi is actually ra [rad], converting to Earth-fixed phi [rad].')
    % ---- Enforce 0<=phi<2*pi.
    idx = find(inj.phi<0);
    inj.phi(idx) = inj.phi(idx) + 2*pi;
    % ---- Compute ra, dec.
    ra = inj.phi * 180/pi;
    dec = (pi/2-inj.theta)*180/pi;
    % ---- Compute phi in Earth-fixed coordinates.
    inj.phi = radectoearth(ra,dec,inj.gpsL);
end

% ---- Done.
return

