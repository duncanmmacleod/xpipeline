function [idx,ps] = randsamp(N,p)
% RANDSAMP - select probability-weighted samples from a set.
%
% usage:
%
%   [idx,ps] = randsamp(N,p)
%
% N     Positive scalar. Number of samples to draw.
% p     Vector of positive numbers. p(i) is the relative probability of 
%       drawing the i'th sample. 
%
% idx   Vector of positive integers. Indices of the vector p selected.
% ps    Vector of p(idx) values.
%
% $Id$

% % ---- Ensure p values are non-negative.
% if any(p<0)
%     error('Input probabilities must be non-negative');
% elseif any(p==0)
%     warning('Some input probabilities are zero.');
% end

% ---- Ensure p values are positive. (We could deal with 0 values but the
%      tracking of indices would be more complicated and we don't need it.)
if any(p<=0)
    error('Input probabilities must be positive');
end

% ---- Force column vector.
p = p(:);

% ---- Number of p values.
Np = length(p);

% ---- Construct normalised cumulative distribution.
c = [0; cumsum(p)/sum(p)];

% ---- Select N p-weighted indices.
x = rand(N,1);
idx = ceil(interp1(c,[0:Np],x));
ps = p(idx);

% ---- Done.
return
