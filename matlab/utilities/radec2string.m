function [raStr decStr]=radec2string(ra,dec)
% radec2earth - Convert from right ascension and declination in decimal degrees
% to latex string in hms and dms
%
% [raStr decStr]=radec2string(ra,dec)

raSplit = datevec(datestr(ra /360,13));
raStr = ['$' num2str(raSplit(4)) '^{\mathrm{h}}' num2str(raSplit(5)) '^{\mathrm{m}}' ...
         num2str(round(raSplit(6))) '^{\mathrm{s}}$' ];
raStr = sprintf('$ %2d^{\\mathrm{h}} %02d^{\\mathrm{m}} %02d^{\\mathrm{s}}$',...
                raSplit(4), raSplit(5), round(raSplit(6)));

decStr = ['$' angl2str(dec,'none','degrees2dm',0) '$'];
