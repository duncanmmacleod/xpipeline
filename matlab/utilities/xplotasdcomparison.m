function [] = xplotasdcomparison(offSourceMatFile, onSourceMatFile, figures_dirName, figfiles_dirName)
% XPLOTASDCOMPARISON - plot the estimated on-source and off-source noise spectra
%
% Usage:
%
% [] = xplotasdcomparison(offSourceMatFile,onSourceMatFile,figures_dirName,figfiles_dirName)
%
% offSourceMatFile   String. Off-source ASD results file (typically 
%                    output/off_source_0_0_ASD.mat).
% onSourceMatFile    String. On-source ASD results file (typically 
%                    output/on_source_0_0_ASD.mat).
% figures_dirName    String. Directory where .png figures will be saved.
% figfiles_dirName   String. Directory where .fig files will be saved.
%
% For each detector, two plots are made. The first shows the off-source spectra
% for up to 1000 off-source jobs, with the median on-source spectrum (median 
% over all on-source jobs) superimposed. The second shows the ratio of each 
% off-source spectrum to the on-source median spectrum.
% 
% If the on-source noise spectrum is significantly different from the spectra
% used for background estimation the estimated false-alarm probability for 
% on-source events may be biased.
%
% $Id$

% ---- Load the detector spectra from the on-source ASD file.
disp(['Loading on-source ASD file...'])
load(strrep(onSourceMatFile,'merged','ASD'),'S');
S_on = S;
disp('   ... finished.')

% ---- Load the detector spectra from the off-source ASD file, as well as the frequency parameters and detectors.
disp(['Loading off-source ASD file...']);
load(strrep(offSourceMatFile,'merged','ASD'),'S','dF','detectorList','minimumFrequency','sampleFrequency');
S_off = S;
disp('   ... finished.')
[numDet, rows, nFiles] = size(S_off);

% ---- For all-sky type searches, the number of off-source spectra can be very large. Limit the plot to 1000 instances.
dn = ceil(nFiles/1000);

% ---- Loop over detectors and generate a plot for each.
for iDet=1:length(detectorList)

    det = detectorList(iDet);

    % ---- Compute the median on-source ASD.
    ASD_on = median(S_on(iDet,:,:),3);

    % ---- Generate a frequency vector.
    freq = 0:dF:dF*(size(ASD_on,2)-1);

    % ---- Plot every dn'th off-source ASD, with the on-source median on top.
    figure; set(gca,'FontSize',20);
    for j=1:dn:nFiles
      loglog(freq,S_off(iDet,:,j),'-b','LineWidth',0.5);
      hold on;
    end
    loglog(freq,ASD_on,'-k','LineWidth',2);
    grid on;
    xlabel('Frequency (Hz)');
    ylabel('Amplitude (Hz^{1/2})');
    title(char(strcat('Estimated noise spectra for ',det)))
    axis([0.75*minimumFrequency 0.5*sampleFrequency 0.5*min(ASD_on) 300*min(ASD_on)])

    % ---- Save the figure.
    plotName = char(strcat(det,'_ASD'));
    xsavefigure(plotName,figures_dirName,figfiles_dirName)

    % ---- Plot the ratio of the off-source to on-source median ASD, for every dn'th off-source ASD.
    figure; set(gca,'FontSize',20);
    for j=1:dn:nFiles
      loglog(freq,S_off(iDet,:,j)./ASD_on,'-b','LineWidth',0.5);
      hold on;
    end
    grid on;
    xlabel('Frequency (Hz)');
    ylabel('Normalized Amplitude');
    title(char(strcat('Estimated normalised noise spectra for ',det)))
    axis([0.75*minimumFrequency 0.5*sampleFrequency 0.3 5.0])

    % ---- Save the figure.
    plotName = char(strcat(det,'_ASD_norm'));
    xsavefigure(plotName,figures_dirName,figfiles_dirName)

end

% ---- Done.
return

