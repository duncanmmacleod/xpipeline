function pass = xapplyratiotest(likelihood,ratioArray,deltaArray)
% XAPPLYRATIOTEST - threshold on the ratios of pairs of likelihoods.
% 
% usage: 
% 
%   pass = xapplyratiotest(likelihood,ratioArray,deltaArray)
% 
% likelihood   Array of events, one per row.  Each column represents
%              one likelihood measure.
% ratioArray   Square array with the same number of columns as the 
%              likelihood variable. Each nonzero element is a threshold
%              to be applied to the ratio of two likehoods; see below.
% deltaArray   Optional.  Square array with the same number of columns as  
%              the likelihood variable. Each nonzero element is added to
%              the likelihood in the denominator of the ratio test.
%              Default is all zeros.
%
% pass         Vector of 0s and 1s, with same number of rows as likelihood.
%              A 1 (0) indicates that the corresponding event passed (did
%              not pass) all of the likelihood ratio thresholds.
%
% This function applies logical tests to each event (row) in the array  
% likelihood.  A nonzero value of ratioArray(i,j) is treated 
% as a test on the ratio likelihood(i)/likelihood(j) for each event.
% The form of the test depends on the sign of ratioArray(i,j), as 
% follows:
%
%
% ratioArray > 0:  A positive value of ratioArray(i,j) is treated 
% as a two-sided test.  The requirement to pass is
%
%   abs(log10(likelihood(i)/likelihood(j))) >= abs(log10(ratioArray(i,j)))
% 
% That is, events are passed if they lie OUTSIDE of a band of fixed
% (logspace) width of the diagonal.  Note that ratioArray values of 
% R and 1/R give the same test when R>0.  So does swapping (i,j) -> (j,i).
%
% If deltaArray(i,j) is nonzero the requirement to pass is modified to
%
%   log10(likelihood(i)/(likelihood(j)+deltaArray(i,j))) 
%       >= abs(log10(ratioArray(i,j)))
%  OR
%   log10(likelihood(j)/(likelihood(i)+deltaArray(i,j))) 
%       >= abs(log10(ratioArray(i,j)))
%
% Note the asymmetry in the use of deltaArray(i,j).
%
%
% ratioArray < 0:  A negative value of ratioArray(i,j) is treated 
% as a one-sided test.  The requirement to pass is 
%
%   likelihood(i)/(likelihood(j)+deltaArray(i,j)) >= abs(ratioArray(i,j))
%
% That is, events are passed if they lie ABOVE a band of fixed (logspace)
% width of the diagonal for delatArray=0.  For deltaArray~=0 the veto line
% is curved.
%
% Notes:
% 1) Diagonal elements and zero elements of ratioArray are ignored.
% 2) Be careful!  Setting both ratioArray(i,j) and ratioArray(j,i) 
%    to values <-1 for given i,j will result in a test that cannot 
%    be passed!  This function does not check for redundant or 
%    inconsistent tests.
%
% Example:  Suppose the likelihoods are Eplus, Iplus, Enull, Inull (in 
% order) and we wish to impose the requirements 
%
%   Inull/Enull >= 1.5
%   Eplus/Iplus >= 1.25 or <= 0.8
%
% The ratioArray is then
%
%   [0 1.25 0    0;
%    0 0    0    0;
%    0 0    0    0;
%    0 0   -1.5  0]

% ---- Checks.
error(nargchk(2, 3, nargin));
if (nargin < 3)
    % ---- Assign default value to deltaArray - square matrix of zeroes.
    deltaArray = zeros(size(likelihood,2));
end

% ---- Number of likelihoods.
N = size(likelihood,2);
% escape if there are now events to apply test to
if N == 0
  pass=[];
  return
end

if ((size(likelihood,2)~=size(ratioArray,2)) | ...
    (size(ratioArray,1)~=size(ratioArray,2)) | ...
    (size(deltaArray)~=size(ratioArray)))
    error(['ratioArray and deltaArray must be square with the same ' ...
        'number of columns as likelihood.']);
end


% ---- Zero out diagonal elements of ratioArray.
for irow = 1:N
    ratioArray(irow,irow) = 0;
end

% ---- Note: 0/0 ratios give test (NaN>=number) which always evaluates 
%      to zero.  Turn off divideByZero warning.
warning('Off','MATLAB:divideByZero');

% ---- Apply consistency tests.
pass = ones(size(likelihood,1),1);
for irow = 1:N
    for icol = 1:N
        if (ratioArray(irow,icol) > 0)
            % ---- Keep events that lie OUTSIDE of a band centered on the y=x line.
            ratio1 = log10( likelihood(:,irow) ./ (likelihood(:,icol)+deltaArray(irow,icol)) ); 
            ratio2 = log10( likelihood(:,icol) ./ (likelihood(:,irow)+deltaArray(irow,icol)) ); 
            threshold = abs(log10(ratioArray(irow,icol)));
            pass = pass .* (( ratio1 >= threshold ) | ( ratio2 >= threshold ));
        elseif (ratioArray(irow,icol) < 0)
            % ---- If ratioArray is negative then apply a one-sided test.
            %      Keep events ABOVE the line.
            ratio = likelihood(:,irow)./(likelihood(:,icol)+deltaArray(irow,icol));
            threshold = abs(ratioArray(irow,icol));
            pass = pass .* (ratio>=threshold);
        else 
            % ---- Do nothing for ratioArray == 0.
        end  
    end
end

% ---- Turn divideByZero warning back on.
warning('On','MATLAB:divideByZero');

% ---- Done
return

