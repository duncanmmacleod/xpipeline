function parameters=alignTotalSpin(parameters, verboseFlag)
% ALIGNTOTALSPIN - rigidly rotates binary so that total angular momentum
% has given inclination instead of orbital angular momentum

if nargin < 2
  verboseFlag = 0;
end

if length(parameters)>=13
  % read spins and masses
  [L S1 S2 m1 m2 iota] = readspins(parameters);
  if verboseFlag
    disp('Read parameters:');
    parameters
  end
  % Rigidly rotate the spin triplet (L,S1,S2) around L so that J is in
  % the xz plane (because L can be only specified in the xz
  % plane)
  Lhat = L/norm(L);
  e1 = cross(Lhat,[0 1 0]');
  e2 = cross(Lhat,e1);
  S = S1 + S2;
  phiOutPlane = atan2(dot(S,e2),dot(S,e1));
  newS1 = RotateVector(S1,Lhat,-phiOutPlane);
  newS2 = RotateVector(S2,Lhat,-phiOutPlane);
  % compute total angular momentum and its inclination
  J = L + newS1 + newS2;
  iotaJ = atan2(J(1),J(3));
  % Rigidly rotate the spin triplet (L,S1,S2) around the y-axis
  % so that J has an inclination angle iota
  newIota = 2*iota - iotaJ;
  newS1 = RotateVector(newS1,[0 1 0]',newIota-iota);
  newS2 = RotateVector(newS2,[0 1 0]',newIota-iota);
  % modify waveforms parameters accordingly
  parameters{4} = newIota;
  parameters{8} = newS1(1)/m1^2;
  parameters{9} = newS1(2)/m1^2;
  parameters{10} = newS1(3)/m1^2;
  parameters{11} = newS2(1)/m2^2;
  parameters{12} = newS2(2)/m2^2;
  parameters{13} = newS2(3)/m2^2;
  % check that the rotations are correct
  [cL cS1 cS2 m1 m2] = readspins(parameters);
  cJ = cL + cS1 + cS2;
  iotaJ = atan2(sqrt(cJ(1)^2+cJ(2)^2),cJ(3));
  if verboseFlag
    disp(['New parameters ']);
    parameters
  end
  if( abs(iotaJ - iota)>1e-4 || abs(norm(S1)-norm(cS1))>1e-4*m1^2 || ...
      abs(norm(S2)-norm(cS2))>1e-4*m2^2 || abs(norm(L)-norm(cL))>1e-4*min(m1,m2))
    error('Total spin reallignment did not work correctly');
  end
else
  warning(['Skipping the reallignment of total angular momentum. ' ...
           'Spins of individual objects are not fully specified'])
end

% --- Done
return 

% -------------------------------------------------------------------------
function [L S1 S2 m1 m2 iota] = readspins(parameters)
% read parameters
m1 = parameters{2};
m2 = parameters{3};
iota = parameters{4};
fLower = parameters{7};
S1 = [parameters{8:10}]'*m1^2;
S2 = [parameters{11:13}]'*m2^2;
% compute orbital angular momentum
mTot = m1 + m2;
mu = m1*m2/mTot;
mSol =  4.925e-6; % (conversion of Msol to seconds)
r = (mTot/(fLower*pi*mSol)^2)^(1.0/3);
L = mu*sqrt(r*mTot)*[sin(iota) 0 cos(iota)]';
return

