function [P, P2, dP2] = grbbinomialtest_threshold(Ndraws,Ntail,threshold,Nmc) 
% GRBBINOMIALTEST_THRESHOLD - Compute percentiles of minimum binomial
% probability under the null hypothesis. 
% 
% usage:
%
%  P = grbbinomialtest_threshold(Ndraws,Ntail,threshold,Nmc) 
%
%  Ndraws       Scalar or vector.  If scalar, number of draws in the larger
%               set, of which localProb is the tail (lowest elements).  If
%               vector, then number of draws is the length of the vecotr
%               and the individual elements are taken as the number of
%               background trials for each draw (i.e., the possible
%               probabilities are restricted to multiples of 1/Ndraws(i)).
%  Ntail        Scalar.  Number of trials in the tail to be tested.
%  threshold    Vector.  Desired percentiles of binomial probability
%               distribution. 
%  Nmc          Optional scalar.  Number of Monte Carlo trials to use in
%               computing Pmin.  Default 1e4.
%
%  P            Vector of same size as 'threshold'.  Percentiles of
%               distribution of minimum binomial probability under the null
%               hypothesis. 
%
% $Id$

% ---- Check for optional arguments
nargchk(3,4,nargin);
if nargin<4
    Nmc = 1e4;
end

% ---- Check to see if we should draw from a uniform distribution or
%      discrete approximations to it.
uniformDist = 1;
if length(Ndraws)>1
    Nbckgrd = Ndraws(:).';  %-- force row vector.
    Ndraws = length(Ndraws);
    uniformDist = 0;
end

if uniformDist
    
    % ---- Background Monte Carlo to handle trials factor for N>1.
    % ---- Generate uniformly distributed local probabilities.
    localProbMC = sort(rand(Nmc,Ndraws),2);

else
    
    % ---- Each of the Ndraws background draws should be taken from the
    %      discrete distribution [0:1:Nbckgrd(ii)]/Nbckgrd(ii) rather than
    %      uniformly on [0,1]. 
    Narray = repmat(Nbckgrd,Nmc,1);
    localProbMC = (unidrnd(Narray+1)-1)./Narray;  %-- +1/-1 accounts for starting from 0.
    localProbMC = sort(localProbMC,2);

end

% ---- Keep N most significant values.
localProbMC = localProbMC(:,1:Ntail);
% ---- Cumulative binomial probability for each trial.
PMC = 1 - binocdf(repmat([0:Ntail-1],Nmc,1),Ndraws,localProbMC);
PminMC = sort(min(PMC,[],2));
% ---- Percentiles of distribution of PminMC.
P = prctile(PminMC,threshold);
% ---- Percentiles-by-counting.
index = round(threshold/100*Nmc);
index_lower = round(threshold/100*Nmc - (threshold/100*Nmc).^0.5);
index_upper = round(threshold/100*Nmc + (threshold/100*Nmc).^0.5);
% ---- Check for indices out of range [1,Nmc].
bad = find(index<=0);
if ~isempty(bad)
    index(bad) = 1;
end
bad = find(index_lower<=0);
if ~isempty(bad)
    index_lower(bad) = 1;
end
bad = find(index_upper<=0);
if ~isempty(bad)
    index_upper(bad) = 1;
end
bad = find(index>Nmc);
if ~isempty(bad)
    index(bad) = Nmc;
end
bad = find(index_lower>Nmc);
if ~isempty(bad)
    index_lower(bad) = Nmc;
end
bad = find(index_upper>Nmc);
if ~isempty(bad)
    index_upper(bad) = Nmc;
end
P2 = PminMC(index);
dP2 = (PminMC(index_upper)-PminMC(index_lower))/2;

% ---- Done.
return
