function p = standardhalomodelpdf(v,sigma_v,v_esc)
% STANDARDHALOMODELPDF - velocity distribution of the Galactic dark matter halo
%
% usage:
%
%   p = standardhalomodelpdf(v,sigma_v,v_esc)
%
%  v         Array. Speeds [km/s] at which PDF is to be sampled.
%  sigma_v   Optional scalar. RMS velocity dispersion [km/s]. Default 290.
%  v_esc     Optional scalar. Galactic escape velocity [km/s]. Default 550.
%
%  p         Array. Standard model PDF at the corresponding values of v.
%
% STANDARDHALOMODELPDF returns the three-dimensional velocity PDF, p(\vec{v}).
% To get the one-dimensional speed PDF, p(v), you need to integrate over the
% angular variables and remember the volume element factor v^2:  
%
%   p(v) = 4*pi*v.^2.*p(\vec{v}),  sum(p(v)*dv) = 1
%
% The PDF is calculated using the smoothed-cutoff version, eqns (16)--(18), of
% Freese, Lisanti, and Savage, 1209.3339v3.
%
% $Id$

% ---- Check number of input arguments.
narginchk(1,3);

% ---- Assign default arguments.
if nargin < 3
    v_esc = 550;
end
if nargin < 2
    sigma_v = 290;
end

% ---- PDF. Use smoothed-cutoff version eqns (16)--(18) of Freese, Lisanti, and
%      Savage, 1209.3339v3. 
v_0 = (2/3)^0.5 * sigma_v;
z = v_esc / v_0;
N_esc = erf(z) - 2*pi^(-0.5)*z*(1+2*z^2/3)*exp(-z^2);
p = zeros(size(v));
idx = find(v<v_esc);
p(idx) = 1/N_esc * (3./(2*pi*sigma_v.^2)).^1.5 * ( ... 
             exp(-3*v(idx).^2/(2*sigma_v^2)) ...
           - exp(-3*v_esc^2/(2*sigma_v^2)) ...
         );

% ---- Done.
return

