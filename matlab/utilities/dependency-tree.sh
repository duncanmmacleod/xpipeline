#!/bin/bash
# determine matlab .m dependency tree for X Pipeline SVN dir

DIR=.
OUTDIR=$(mktemp -d)
FILE_LIST="$OUTDIR/files"
MAT_LIST="$OUTDIR/list"

#REJECTS="! -wholename */trash/* ! -wholename */test/* ! -wholename */xdetection_mcr/*"
#REJECTS='"! -wholename */trash/*"'
REJECTS="! -wholename */trash/* ! -wholename */newshare/*"

usage() {
    cat <<EOF
Usage: $(basename $0) option file 

-r      list required files
-r1     list required files, but each only once
-d      list dependent files
EOF
}

cleanup() {
    rm -rf "$OUTDIR"
}

meat() {
    grep -v -e '^[[:space:]]*%' -e '^$' "$@"
}

required_files() {
    local testfile=$1
    local level=$2
    local hi HEAD base REQS

    for (( hi=0; $hi < $level; hi=$hi+1 )) ; do
	HEAD="|$HEAD"
    done

    testbase=$(basename "$testfile" .m)
    # cut the testfile permanently out of the list
    if [ "$SINGLE" = 1 ] ; then
	grep -v "$testbase" "$MAT_LIST" > "$OUTDIR/foo"
	mv "$OUTDIR/foo" "$MAT_LIST"
	REQS=$(meat "$testfile" | grep -f "$MAT_LIST" -h -o | sort -u)
    else
	REQS=$(meat "$testfile" | grep -f "$MAT_LIST" -h -o | sort -u | grep -v "$testbase")
    fi
    # get full list of files from svn list that are referenced in testfile

    if [ "$REQS" ] ; then
	echo "$HEAD+" "$testfile"
	level=$(( $level + 1 ))
	for mat in $REQS ; do
	    file=$(grep "/${mat}.m" "$FILE_LIST")
	    required_files $file $level
	done
    else
	echo "$HEAD-" "$testfile"
    fi
}

dependent_files() {
    local file=$(basename "$1" .m)
    for mat in $(cat "$FILE_LIST" | grep -v "$file") ; do
	if meat "$mat" | grep -q "$file" ; then
	    echo "$mat"
	fi
    done
}

trap cleanup EXIT

find "$DIR" $REJECTS -iname "*.m" > "$FILE_LIST"
find "$DIR" $REJECTS -iname "*.m" -exec basename '{}' .m \; | sort -u > "$MAT_LIST"
#find $DIR $REJECTS -iname "*.m" -exec grep -f $OUTDIR/list -H -o '{}' \+ > $OUTDIR/tree

CMD=$1
shift 1

case "$CMD" in
    '-r')
	level=0
	required_files $1 $level
	;;
    '-r1')
	level=0
	SINGLE=1
	required_files $1 $level
	;;
    '-d')
	dependent_files $1
	;;
    *)
	exit 1
	;;
esac

# for file in $(required_files $testfile) ; do
#     echo "|+" $file
#     required_files $file | xargs -I '{}' echo "||-" '{}'
# done




# for file in $(cat $OUTDIR/files) ; do
#     base=$(basename $file .m)
#     echo '-----'
#     echo '+' "$file"
# done
