function [Y I J] = numstructarray2struct(X)
% NUMSTRUCTARRAY2STRUCT - Convert numerical struct array to struct.
%
% usage:
%
%   Y = numstructarray2struct(X)
%
%   X     Struct array.  Each field must contain only numerical data. For a
%         given field, all elements of X must have the same number of
%         columns, or else be empty.  If not, the function will exit with
%         error. For a given element of X, all field values must have the
%         same number of rows.  THIS IS NOT CHECKED.
%
%   Y     Struct (1x1 aray).  Y is constructed by reshaping X=X(:), 
%         followed by vertical concatenation of the values in each field.
%   I,J   Vectors.  For each field "a" in Y, row "i" is obtained as 
%         Y.a(i,:) = X(I).a(J).
%
% Tip: For large input struct arrays, the computation of I,J is the slowest
% part of the function. Do not request I,J if you do not require them. 
%
% $Id$

% ---- Checks.
error(nargchk(1,1,nargin));
if ~isstruct(X)
    error('Input argument X must be a struct array.')
end

% ---- Get field names (needed for counting number of rows to compute I,J).
fields = fieldnames(X);

% ---- Reshape X to column vector.
X = X(:);

% ---- Loop over fields of X and record number of columns for each.
col = zeros(1,length(fields));
% ---- Define a counter for elements of X.  Start from last element since
%      for X-Pipeline these are more likely to be filled. 
ii = length(X);  
while min(col)==0
    % ---- Have to do 'while' because first element of X may be empty.
    for jj = 1:length(fields)
        col(jj) = size(X(ii).(fields{jj}),2);
    end
    ii = ii - 1;
end

% ---- Compute the indices I,J mapping X to Y.  This is the slowest part of
%      the function by far, so we only do this if the user asks for I,J.
if nargout > 1
    % ---- Loop over all elements of X and record number of rows.
    row = zeros(length(X),1);
    for ii = 1:length(X)
        % ---- Number of rows in first field (assumed same for all fields).
        row(ii) = size(X(ii).(fields{1}),1);
    end

    % ---- Figure out indices mapping X to Y.
    I = zeros(sum(row),1);
    J = zeros(sum(row),1);
    cumrow = [0; cumsum(row)];
    for ii = 1:length(X)
        if row(ii)>0
            I(cumrow(ii)+(1:row(ii))) = ii;
            J(cumrow(ii)+(1:row(ii))) = (1:row(ii))';
        end
    end
end

% ---- Initialize output as empty struct with correct fields.
Y = xclustersubset(X(1),[]);

% ---- Loop over all fields, using comma-separated output and merging.
for jj = 1:length(fields)
    fieldValueY = [X.(fields{jj})];
    % ---- Write the merged data field back into mergedSet.
    Y.(fields{jj}) = reshape(fieldValueY,col(jj),[]).';
end

% ---- Done.
return
