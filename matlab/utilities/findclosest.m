function idx = findclosest(xAll,yAll,x,y)
% findidx - find vector elements closest to requested inputs
%
% use:
%
%   idx = findclosest(xAll,yAll,x,y)
%
% xAll, yAll    Vectors of "x" and "y" values to be matched against. May have
%               repeated values. Must be the same length.
% x, y          Scalars. Input values to be matced against xAll, yAll.
%
%
% idx           Scalar. Index into xAll, yAll that minimises both
%               abs(xAll(idx)-x) and abs(yAll(idx)-y). If no value
%               simultaneously minimises both then idx=[];
%
% $Id$

% ---- Checks.
if length(xAll)~=length(yAll)
    error('Inputs xAll, yAll must be the same length.')
end

% ---- Find unique values of xAll, yAll.
ux = unique(xAll);
uy = unique(yAll);

% ---- Find closest match to unique values.
[val,ind1] = min(abs(ux-x));
[val,ind2] = min(abs(uy-y));

% ---- Find index that simultaneously satifies both criteria.
idx = find(xAll==ux(ind1) & yAll==uy(ind2));

% ---- Done.
return
    