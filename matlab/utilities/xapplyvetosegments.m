function pass = xapplyvetosegments(triggers,trigger_times,vetoSegs,...
    analysis,userTag,verbosity,deltaT)
% XAPPLYVETOSEGMENTS - veto events overlapping veto segment lists.
%
% xapplyvetosegments vetoes triggers occurring within, or overlapping with
% times listed in the veto segment files provided.
%
% usage:
%
%  pass = xapplyvetosegments(triggers,trigger_times,vetoSegs,analysis, ...
%      userTag,verbosity)
%
%  triggers         Structure containing triggers, may be onSource,
%                   offSource or injections.
%  trigger_times    Array containing the times at which the triggers
%                   occurred. These times must have had time-shifts
%                   removed (i.e., they must be unslid).
%                   This  array should have size Ntrig*Nifo
%  vetoSegs         Structure containing list of veto segments for
%                   each ifo:
%                   vetoSegs(thisIfo).gpsStart Array of start times of
%                       veto segments
%                   vetoSegs(thisIfo).duration Array listing the duration
%                       in second of the veto segments
%  analysis         Structure created by xmakegrbwebpage. Only required field is
%                   'detectorList'.
%  userTag          String used to name output files containing list of
%                   vetoed event. These files are used to testing
%                   purposes and will eventually be removed
%  verbosity        String, either 'verbose' or 'quiet'
%  deltaT           [OPTIONAL] Array, intrinsic error on parameter
%                   estimated by pipeline for each detector. Default to
%                   30ms for all but the first detector, as
%                   time-of-flight is not corrected, and the peakTime is
%                   referenced to the first detector
%
%  pass             Vector.  Value of 0 (1) means corresponding event was
%                   (was not) vetoed.
%
%  $Id$

% ---- Checks.
error(nargchk(6, 7, nargin));

% ---- Set default deltaT, time of flight between detectors for all but
%      the first detector
if nargin < 7
    deltaT = 30e-3*ones(1,length(analysis.detectorList));
    deltaT(1) = 0;
end

% ---- Check that verbosity is correctly defined.
if ~( strcmp(verbosity,'verbose') | ...
      strcmp(verbosity,'quiet') )
   error('verbosity must be either verbose or quiet')
end

if strcmp(verbosity,'verbose')
    verboseFlag = 1;
else
    verboseFlag = 0;
end

% ---- Assume all clusters pass veto segment cuts
%      If a cluster does overlap with a veto segment it has failed
%      the veto cut and we set its pass value to zero
pass = ones(size(triggers.boundingBox(:,3)));

% ---- Loop over ifos we have analysed
for thisIfo = 1:length(analysis.detectorList)

    % ---- If vetoSegs struct is not empty apply segs
    if ~isempty(vetoSegs(thisIfo).gpsStart)
        if (verboseFlag == 1)
             fprintf(1,'Applying %s veto segs ...', ...
                 analysis.detectorList{thisIfo})
        end

        % ---- Find intersections between vetos and our clusters
        % ---- Assume we have more triggers than vetoSegs,
        %      passing the shorter list to Coincidence2 first
        %      speeds it up
        % ---- Take into account that the time refers to time in the
        %      first detector, so the peak glitch time at other detectors
        %      may be shifted by the time of flight
        thisDeltaT = deltaT(thisIfo);
        % ---- Use fastcoincidence2 if available, otherwise use slower
        %      Coincidence2 function. 
        try
            coincOut = fastcoincidence2(...
                [vetoSegs(thisIfo).gpsStart vetoSegs(thisIfo).duration],...
                [trigger_times(:,thisIfo)-thisDeltaT 2*thisDeltaT*ones(size(trigger_times(:,thisIfo)))]);
        catch
            coincOut = Coincidence2(...
                vetoSegs(thisIfo).gpsStart, vetoSegs(thisIfo).duration,...
                trigger_times(:,thisIfo)-thisDeltaT, 2*thisDeltaT*ones(size(trigger_times(:,thisIfo))) );
            coincOut = coincOut(:,5:6);
        end
        if (verboseFlag == 1)
            fprintf(1,' done! \n');
        end

        if ~isempty(coincOut)
            % ---- Second column of out contains indices of clusters
            %      overlapping with vetoed segs 
            if (verboseFlag == 1)
                fprintf(1,'Vetoing %d clusters in %s data \n', ...
                    length(coincOut(:,2)),userTag);
            end

            pass(coincOut(:,2)) = 0;

            if (verboseFlag == 1)
                % ---- Testing, will delete later
                vetoedTimes        = triggers.boundingBox(coincOut(:,2),1);
                vetoedTimesUnslid  = trigger_times(coincOut(:,2),thisIfo);
                vetoedDuration     = triggers.boundingBox(coincOut(:,2),3);
                fveto = fopen([userTag '_' analysis.detectorList{thisIfo} ...
                               '_vetoseg_vetoed.txt'],'w');
                for idx=1:length(coincOut(:,2))
                   fprintf(fveto,'%9.5f %9.5f %9.5f \n',vetoedTimes(idx), ...
                       vetoedTimesUnslid(idx), vetoedDuration(idx));
                end
                fclose(fveto);
                % ---- End of testing
            end
        end
    end % -- Check that vetoSegs(thisIfo) is not empty
end % -- Loop over ifos

% -- Done
