function [alpha,delta] = ecliptictoequatorial(lambda,beta)
% ECLIPTICTOEQUATORIAL: Convert from ecliptic to equatorial sky coordinates.
%  
%   [alpha,delta] = ecliptictoequatorial(lambda,beta)
%
% lambda    Vector. Ecliptic longitude (rad).
% beta      Vector. Ecliptic latitude (rad).
%
% alpha     Right ascension in equatorial (sky-based) coordinates (rad).
% delta     Declination in equatorial coordinates (rad).
%
% For definitions and discussion of the equatorial and ecliptic 
% cooridnate systems, see the LAL Software Document, Section 16.7, 
% "SkyCoordinates.h"
%
% -- first version: Patrick J. Sutton 2010.04.23

% ---- Constants defining eclipctic coordinate system.
%      See LAL Software Document, Section 16.7, "SkyCoordinates.h"
epsilon = 23.4392911*pi/180;

% ---- Transformation from ecliptic to equatorial coordinates.
% ---- declination
delta = asin( cos(beta).*sin(lambda)*sin(epsilon) + sin(beta)*cos(epsilon) );
% ---- right ascension, wrapped to [0,2*pi)
alpha = atan2( cos(beta).*sin(lambda)*cos(epsilon) - sin(beta)*sin(epsilon) , cos(beta).*cos(lambda) );
alpha = mod(alpha,2*pi);

% ---- Done
return
