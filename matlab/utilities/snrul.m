function [ul90p,allFraction,effFactor,ul50p,injectionValueArray] = ...
    snrul(likelihoodValueFA, jobNumberFA, jobFilesProcessed, ...
    likelihoodValueEFFCell, injectionProcessedMaskCell, injectionValueArray, ...
    bestRatio, incIndex, nullIndex, energyIndex, injectionFileName,...
    amplitudeSpectraCell,analysisTimesCell,sampleFrequency,detectorList)
% SNRUL - Compute a loudest event upper limit in terms of SNR.
%
% WARNING: Experimental, probably does not work in current form.  Also, 
% exact only for LINEARLY polarized waveforms.  May work well for
% circularly polarized waveforms.  Does not account for scalar mode GWs.
% Caveat emptor!
%
% ul90p = ulEstimation(likelihoodValueFA, jobNumberFA, jobFilesProcessed, ...
%     likelihoodValueEFFCell, injectionProcessedMaskCell, ...
%     injectionValueArray, bestRatio, incIndex, nullIndex, energyIndex)
%
% likelihoodValueFA
% jobNumberFA
% jobFilesProcessed
% likelihoodValueEFFCell
% injectionProcessedMaskCell 
% injectionValueArray
% bestRatio
% incIndex
% nullIndex
% energyIndex 
% injectionFileName
%
% ul90p
% allFraction
% effFactor
% ul50p
% 
% $Id$

% ---- Check input arguments.
error(nargchk(7, 15, nargin));
if (nargin==7)
    % ---- Use default ordering of likelihoods.
    incIndex=4;
    nullIndex=3;
    energyIndex=1;
end
% ---- Prepare storage.
nInjScales = length(likelihoodValueEFFCell);
fraction = zeros(nInjScales,1);
effFactor =  ones(nInjScales,1);
ul90p = zeros(jobFilesProcessed,1);
ul50p = zeros(jobFilesProcessed,1);



i=1;maxPrec=0;
for i =1:length(amplitudeSpectraCell)
    if( maxPrec < length(amplitudeSpectraCell{i}))
        maxPrec = length(amplitudeSpectraCell{i});
        bATi =i;
    end
end
S = amplitudeSpectraCell{bATi}.^2;
dF = 1/analysisTimesCell{bATi};
fs= sampleFrequency;

% ---- Load injection parameters
[injectionParameters] = readinjectionfile(injectionFileName);
[~, gps_s, gps_ns, phi, theta, psi, name, parameters] = parseinjectionparameters(injectionParameters);
minimumFrequency=64;
maximumFrequency=1792;



% ---- Compute Antenna Patterns
for iDet = 1:length(detectorList)
    % ---- The detector is matched by the first character of the channel name.
    detData = LoadDetectorData(detectorList{iDet}(1));
    % ---- Extract the position vector
    rDet(iDet,:) = detData.V';
    % ---- Extract the detector response tensors
    dDet{iDet}   = detData.d;
end
[Fp, Fc] = antennaPatterns(dDet, [theta(1) phi(1)]);

% ---- Compute weighting for detection statistics
FpDominantArray= zeros(length(detectorList),1);
FcDominantArray= zeros(length(detectorList),1);
wFp = repmat(Fp(1,:),[size(S,1) 1]) ./ sqrt(S);
wFc = repmat(Fc(1,:),[size(S,1) 1]) ./ sqrt(S);
[FpDominantArray FcDominantArray]= convertToDominantPolarizationFrame(wFp,wFc);
FpHatDP=FpDominantArray./repmat(sqrt(sum(FpDominantArray.^2,2)),[1 length(detectorList)]);

% ---- Compute detection estimater (e.g. total SNR root-sum-squared)
[t,hp,hc] = xmakewaveform(cell2mat(name{1}),cell2mat(parameters{1}),1/dF,1/dF*0.5,fs);
for iDetector =1:length(detectorList)
    [Fp, Fc] = ComputeAntennaResponse(phi,theta,psi,detectorList{iDetector});
            SNRp =  xoptimalsnr(hp,0,fs,S(:,iDetector)./abs(FpHatDP(:,iDetector)),0,dF,minimumFrequency,maximumFrequency);
            SNRc =  xoptimalsnr(hc,0,fs,S(:,iDetector)./abs(FpHatDP(:,iDetector)),0,dF,minimumFrequency,maximumFrequency);
    %SNRp =  xoptimalsnr(hp,0,fs,S(:,iDetector),0,dF,minimumFrequency,maximumFrequency);
    %SNRc =  xoptimalsnr(hc,0,fs,S(:,iDetector),0,dF,minimumFrequency,maximumFrequency);
    SNRlist(:,iDetector) = sqrt((SNRp*Fp(:)).^2+(SNRc*Fc(:)).^2) ;
end
totSNRlist=sqrt(sum(SNRlist.^2,2));
%    SNRlist=sort(SNRlist,2,'descend');
%    totSNRlist=sqrt(sum(SNRlist(:,2:end).^2,2)+ SNRlist(:,2).^2);

% ----- find bins for estimator
minLog=log(0.99*max(min(totSNRlist),1)*min(injectionValueArray));
maxLog=log(1.01*max(totSNRlist)*max(injectionValueArray));
binArray=[exp(minLog:((maxLog-minLog)/12):maxLog)];
binNumber=length(binArray)-1;


% ---- copy only processed injections
for iInjScale=1:length( likelihoodValueEFFCell)
    tmpEFFCell{iInjScale} =  likelihoodValueEFFCell{iInjScale}(logical(injectionProcessedMaskCell{iInjScale}),:);
end
likelihoodValueEFFCell=cell(binNumber,1);

sumTotSNRbin=zeros(binNumber,1);
nOfInjBinned=zeros(binNumber,1);
% ---- re-bin injections according to estimator and not injection scale
for iInjScale=1:nInjScales
    [N BIN]=histc(totSNRlist(logical(injectionProcessedMaskCell{iInjScale}))*injectionValueArray(iInjScale),binArray);
    for jj=1:binNumber
        likelihoodValueEFFCell{jj}=[likelihoodValueEFFCell{jj};tmpEFFCell{iInjScale}(jj==BIN,:)];
        % [tmpEFFCell{iInjScale}(jj==BIN,energyIndex) totSNRlist(jj==BIN)*injectionValueArray(iInjScale)]
        tmpTotSNRlist=totSNRlist(logical(injectionProcessedMaskCell{iInjScale}))*injectionValueArray(iInjScale);
        sumTotSNRbin(jj)=sumTotSNRbin(jj)+sum(tmpTotSNRlist(jj==BIN));
        nOfInjBinned(jj)=nOfInjBinned(jj)+sum(jj==BIN);
    end
end

% ---- only processed injection have been re-bined
for jj=1:binNumber
    injectionProcessedMaskCell{jj}=ones(size(likelihoodValueEFFCell{jj},1),1);
end


% compute center bins so that +Inf do not give bin center at infinity
injectionValueArray=sumTotSNRbin./nOfInjBinned;
nInjScales=binNumber;
% ---- Prepare storage.
fraction = zeros(nInjScales,1);
effFactor =  ones(nInjScales,1);
ul90p = zeros(jobFilesProcessed,1);
ul50p = zeros(jobFilesProcessed,1);

% ---- Loop over all job files.  Estimate upper limit for each.
for iJob = 1:jobFilesProcessed
    % ---- Find the largest false-alarm likelihood surviving the
    %      consistency test.
    lMax = computeLmax( ...
        likelihoodValueFA(iJob == jobNumberFA,energyIndex), ...
        likelihoodValueFA(iJob == jobNumberFA,incIndex), ...
        likelihoodValueFA(iJob == jobNumberFA,nullIndex), ...
        bestRatio);
    % lMax =  computeLmax(likelihoodValueFA(iJob == jobNumberFA,:), ...
    %     incIndex, nullIndex, energyIndex, bestRatio, ...
    %     sum(iJob == jobNumberFA) +1);
    if(strcmp(parameters{1},'5.2227e-20~0.0036565~554'))
        save ulDump.mat tmpEFFCell likelihoodValueEFFCell lMax
    end
    for iInjScale=1:nInjScales
        % ---- Compute fraction of injections at this scale which survive
        %      the inc/null consistency test AND are louder than the loudest
        %      surviving noise event.
        % ---- KLUDGE: WARNING - If the fraction is the same at different
        %      injection scales (e.g., saturated at 1) then interpolation
        %      for 90% efficiency may fail.  We therefore add a different
        %      tiny number to each fraction.  This should be fixed.

        fraction(iInjScale) = computeFractionAboveLmax( ...
            likelihoodValueEFFCell{iInjScale}(:,energyIndex), ...
            likelihoodValueEFFCell{iInjScale}(:,incIndex), ...
            likelihoodValueEFFCell{iInjScale}(:,nullIndex), ...
            injectionProcessedMaskCell{iInjScale}, ...
            bestRatio, lMax) + injectionValueArray(iInjScale)*1e-7;
        % ---- correct results for low number of injections
        %if(exist('injectionFileName','var'))
        %    FpProc = Fp(logical(injectionProcessedMaskCell{iInjScale}));
        %    FcProc = Fc(logical(injectionProcessedMaskCell{iInjScale}));
        %    sortedFp = sort(abs(FpProc)./sqrt(FpProc.^2+FcProc.^2),'descend');
        %    if(round((length(sortedFp)*fraction(iInjScale))) && fraction(iInjScale) <1-1e-5)
        %       
        %        effFactor(iInjScale) = sortedFp(1+round((length(sortedFp)*fraction(iInjScale)))) ...
        %            /cos(fraction(iInjScale)*pi/2);
        %    end
        %end

        % fraction(iInjScale) = computeFractionAboveLmax( ...
        %     likelihoodValueEFFCell{iInjScale}, ...
        %     injectionProcessedMaskCell{iInjScale}, ...
        %     incIndex, nullIndex, energyIndex, bestRatio, lMax) ...
        %     + iInjScale*eps;
    end
    fraction(:)
    mask=~isnan(fraction);
    fraction=fraction(mask)
    injectionValueArray=injectionValueArray(mask);
    effFactor=effFactor(mask);

    allFraction(:,:,iJob) = fraction;
    % ---- Display a warning message if the interpolation to fraction=0.9
    %      will fail.
    if(min(fraction)>0.9)
        disp(['For every injection scale more than 90% events are above loudest background, ' ...
            'try with lower injection scales']);
    elseif(max(fraction)<0.9)
        disp(['For every injection scale less than 90% events are above loudest background, ' ...
            'try with higher injection scales'])
    end
    % ---- Interpolate to injection scale such that fraction = 0.9.
    if(sum(~mask))
        disp('WARNING some fraction are NaNs ***********************************************************');
    end
    ul90p(iJob)  = interp1(fraction(:), injectionValueArray.*effFactor, 0.9, 'linear');
    ul50p(iJob) = interp1(fraction(:),injectionValueArray .*effFactor, 0.5, 'linear');
end

% ---- Done.
return
