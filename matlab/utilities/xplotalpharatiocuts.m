function [numCuts] = xplotalpharatiocuts(alphaRatioArray, ...
    linRatioArray,linDeltaArray,analysis,pairIdx,Emax)
% XPLOTALPHARATIOCUTS - plot thresholds for coherent consistency veto test.
%
% usage:
%  
% [numCuts] = xplotalpharatiocuts(alphaRatioArray,linRatioArray, ...
%                 linDeltaArray,analysis,pairIdx,Emax)
%  
% alphaRatioArray Square array with the same number of columns as the 
%                 likelihood variable. Each nonzero element is a threshold
%                 to be applied to the ratio of two likehoods; see 
%                 xapplyratiotest and xapplyalpharatiotest for details.
% linRatioArray   As alphaRatioArray.
% linDeltaArray   As alphaRatioArray.
% analysis        Structure. Needs to include the field likelihoodPairs as  
%                 defined in xmakegrbwebpage.
% pairIdx         Whole number. Identifies which likelihood pair in 
%                 analysis.likelihoodPairs we will plot.
% Emax            Optional scalar. Approximate maximum energy value to which to 
%                 plot cut line. Default 5e4.    
%  
% numCuts         Integer. Number of ratio cut lines plotted, useful when
%                 creating legends
%
% The cut threshold lines are added to the current figure.
%
% $Id$

% ---- Checks.
error(nargchk(5,6,nargin));

% ---- Assign default arguments.
if nargin<6
    Emax = 5e4;
end
X = 10.^[0:0.1:log10(2*Emax)];

% ---- Initialise number of cuts plotted to zero.
numCuts = 0;

% ---- Status report, get name of likelihood (e.g., 'null') from
%      name field of analysis.likelihoodPairs
fprintf(1,['Plotting coherent consistency cut line for '...
    analysis.likelihoodPairs(pairIdx).name ' likelihoods... ']);

% ---- Extract index of the coherent and incoherent energies to be tested.
eLikeIdx = analysis.likelihoodPairs(pairIdx).energy;
iLikeIdx = analysis.likelihoodPairs(pairIdx).inc;

% ---- The "alpha" test always uses the hardcoded value alpha = 0.8.
alpha = 0.8;

% ---- Do the relevant plotting. This depends on whether the test is one-sided 
%      (threshold<0) or two-sided (threshold>0) and on whether it is an E/I type 
%      test or an I/E type test.
if (linRatioArray(eLikeIdx,iLikeIdx) > 0) || ...
      (alphaRatioArray(eLikeIdx,iLikeIdx) > 0)

   % ---- E/I test with positive threshold, so we do a two-sided test.
   numCuts = 2; 

   % ---- Extract the thresholds for the fixed ratio (ra) and alphs ratio (ara) tests. 
   ra = abs(linRatioArray(eLikeIdx,iLikeIdx));
   ara = abs(alphaRatioArray(eLikeIdx,iLikeIdx))-1;

   % ---- Draw the threshold line above the I=E diagonal.
   Ypos = + ara/2*X.^alpha;
   IposAlpha = (X+Ypos)/2;
   Epos = (X-Ypos)/2;
   IposLin = (Epos + linDeltaArray(eLikeIdx,iLikeIdx)).* ra;
   Ipos = max(IposLin,IposAlpha);

   % ---- Draw the threshold line below the I=E diagonal.
   Yneg = - ara/2*X.^alpha;
   InegAlpha = (X+Yneg)/2;
   Eneg = (X-Yneg)/2;
   InegLin  = (Eneg + linDeltaArray(eLikeIdx,iLikeIdx))./ ra;
   Ineg = min(InegLin,InegAlpha);

   % ---- Plot both threshold lines.
   plot(Epos,Ipos,'--m','LineWidth',2);
   plot(Eneg,Ineg,'--m','LineWidth',2);

elseif (linRatioArray(iLikeIdx,eLikeIdx) > 0) || ...
      (alphaRatioArray(iLikeIdx,eLikeIdx) > 0)

   % ---- I/E test with positive threshold, so we do a two-sided test.
   numCuts = 2;

   % ---- Extract the thresholds for the fixed ratio (ra) and alphs ratio (ara) tests. 
   ra = abs(linRatioArray(iLikeIdx,eLikeIdx));
   ara = abs(alphaRatioArray(iLikeIdx,eLikeIdx))-1;

   % ---- Draw the threshold line above the I=E diagonal.
   Ypos = + ara/2*X.^alpha;
   IposAlpha = (X+Ypos)/2;
   Epos = (X-Ypos)/2;
   IposLin = (Epos + linDeltaArray(iLikeIdx,eLikeIdx)).* ra;
   Ipos = max(IposLin,IposAlpha);

   % ---- Draw the threshold line below the I=E diagonal.
   Yneg = - ara/2*X.^alpha;
   InegAlpha = (X+Yneg)/2;
   Eneg = (X-Yneg)/2;
   InegLin  = (Eneg + linDeltaArray(iLikeIdx,eLikeIdx))./ ra;
   Ineg = min(InegLin,InegAlpha);
   
   % ---- Plot both threshold lines.
   plot(Epos,Ipos,'--m','LineWidth',2);
   plot(Eneg,Ineg,'--m','LineWidth',2);

elseif (linRatioArray(eLikeIdx,iLikeIdx) < 0) || ...
      (alphaRatioArray(eLikeIdx,iLikeIdx) < 0)

   % ---- E/I test with negative threshold, so we do a one-sided test.
   numCuts = 1;

   % ---- Extract the thresholds for the fixed ratio (ra) and alphs ratio (ara) tests. 
   ra = abs(linRatioArray(eLikeIdx,iLikeIdx));
   ara = abs(alphaRatioArray(eLikeIdx,iLikeIdx))-1;

   % ---- Draw the threshold line below the I=E diagonal.
   Yneg = - ara/2*X.^alpha;
   InegAlpha = (X+Yneg)/2;
   Eneg = (X-Yneg)/2;
   InegLin  = (Eneg + linDeltaArray(eLikeIdx,iLikeIdx))./ ra;
   Ineg = min(InegLin,InegAlpha);

   % ---- Plot the threshold line.
   plot(Eneg,Ineg,'--m','LineWidth',2);

elseif (linRatioArray(iLikeIdx,eLikeIdx) < 0) || ...
      (alphaRatioArray(iLikeIdx,eLikeIdx) < 0)

   % ---- I/E test with negative threshold, so we do a one-sided test.
   numCuts = 1;

   % ---- Extract the thresholds for the fixed ratio (ra) and alphs ratio (ara) tests. 
   ra = abs(linRatioArray(iLikeIdx,eLikeIdx));
   ara = abs(alphaRatioArray(iLikeIdx,eLikeIdx))-1;

   % ---- Draw the threshold line above the I=E diagonal.
   Ypos = + ara/2*X.^alpha;
   IposAlpha = (X+Ypos)/2;
   Epos = (X-Ypos)/2;
   IposLin = (Epos + linDeltaArray(iLikeIdx,eLikeIdx)).* ra;
   Ipos = max(IposLin,IposAlpha);

   % ---- Plot the threshold line.
   plot(Epos,Ipos,'m--','LineWidth',2);

end

% ---- Status report.
fprintf(1,'done! \n');

return;
