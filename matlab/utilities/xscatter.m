function xscatter(X,Y,C,Xlabel,Ylabel)
% XSCATTER - Make scatter plot with pre-set formatting.
%
% This function is a wrapper to the standard scatter() function that applies 
% specific formatting:
%  1) The X, Y, and C (color) axes are all log10-scale.
%  2) The data points are sorted so that larger C-values are drawn on top of
%     smaller ones.
%  3) The marker size is fixed to min(20+5*sqrt(C),175).
%  4) Larger than standard axes fonts are used and the grid is turned on. 
%  5) If more than 1e5 ppoints are specified then only the 1e5 points with 
%     largest C values are plotted.
%
% usage:
%
%   xscatter(X,Y,C,Xlabel,Ylabel)
%
% X	  Vector of x-axis values.
% Y	  Vector of y-axis values.
% C 	  Vector of c-axis (color) values.
% Xlabel  String.  X-axis label.
% Ylabel  String.  Y-axis label.
%
% $Id$

% ---- Maximum number of points we'll try to plot.
maxPoints = 1e5;

% ---- Sort by color ("height") so large values appear on top.
[C,I] = sort(C);

% ---- Limit number of plotted points.
if (length(I)>maxPoints)
    I = I(end-maxPoints+1:end);
    C = C(end-maxPoints+1:end);
end

% ---- Draw figure with hardwired marker scaling and marker type.
figure; scatter(X(I),Y(I),min(20+5*sqrt(C),175),log10(C),'+')

% ---- Format plot.
colorbar
set(gca,'FontSize',20)
set(gca,'xscale','log')
set(gca,'yscale','log')
xlabel(Xlabel);
ylabel(Ylabel);
grid;

% ---- Done.
return

