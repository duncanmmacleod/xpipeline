function [common, d1, d2] = comp_struct(s1,s2,prt,pse,tol,mode,recursion,n1,n2)
% check two structures for differances - i.e. see if strucutre s1 == structure s2
%
% usage:
%
%   function [common, d1, d2] = comp_struct(s1,s2,prt,pse,tol,mode)
%
% inputs  5 (3 optional)
% s1      structure one                              class structure
% s2      structure two                              class structure
% prt     print test results (0 / 1 / 2 / 3)         class integer - optional
% pse     pause flag (0 / 1 / 2)                     class integer - optional
% tol     tol default tolerance (real numbers)       class integer - optional
% mode    comparison: 'absolute' or 'fractional'     class string - optional
%
% outputs 3 (3 optional)
% common  matching fields                            class struct - optional
% d1      non-matching fields for structure one      class struct - optional
% d2      non-matching fields for structure two      class struct - optional
%
% prt:
%	0 --> no print
%	1 --> print class differences
%	2 --> print all differences
% pse:
%	1 --> pause for class differences
%	2 --> pause for all differences
% If the inputs satisfy pse>prt then pse is reset as pse=prt.
%
% The mode parameter determines whether the comparison is absolute 
% (abs(s1-s2)<tol) or fractional (abs((s1-s2)./s1)<tol). Default 'absolute'.
% The default val of tol is tol=2*eps. The tolerance test is only applied
% to fields with value x that satisfies isa(x,'numeric'), eg numeric
% arrays. For other classes such as cell arrays, no tolerance test is
% applied. So for example cell arrays with elements that are numeric arrays
% are not checked for tolerance.
%
% example:	[same, er1, er2] = comp_struct(data1,data2,1,1,1e-6)
%
% michael arant - may 27, 2013
% updated - aug 22, 2015
%
% Original version taken from 
% michael arant (2020). Compare Structures (https://www.mathworks.com/matlabcentral/fileexchange/22752-compare-structures), MATLAB Central File Exchange. 
%
% Minor edits by Patrick Sutton, 2020.01.21 and mode added 2022.03.08.

% note: recursion, n1 and n2 are not required at launch - used for recursive purposes


%% default argument check
if nargin < 2
	help comp_struct; error('I / O error');
end

if nargin < 3 || isempty(prt); prt = 0; end
if nargin < 4 || isempty(pse); pse = 0; elseif pse ~= 1 && prt == 0; pse = 0; end
if nargin < 5 || isempty(tol); tol = 2*eps; end
if nargin < 6 || isempty(mode); mode = 'absolute'; end
if nargin < 7 || isempty(recursion); recursion = 0; end
if pse > prt, pse = prt; end

if ~exist('n1','var'); n1 = inputname(1); end
if ~exist('n2','var'); n2 = inputname(2); end


%% structure defintion
d1 = s1; d2 = s2; common = s1;


%% begin analysis
flag = [0 0]; 
    % flag = [0 0]  inputs are equal
    % flag = [0 1]  inputs are both structs but not equal - run function
    %               recursively 
    % flag = [1 0]  one input is not a struct, or both are not and they are
    %               not equal within tolerance 

% test entire structure
if ~isequal(s1,s2)
    
	% differences noted - parse
	if isstruct(s1) && isstruct(s2)
		% both structures - once sub structures are tested, do not 
		% modify the parrent
		flag(2) = 1;
		% both structures - get the field names for each structure
		fn1 = fieldnames(s1);
		fn2 = fieldnames(s2);
		% missing fields?  (common was a copy of s1 - so only s1 needs checking)
		temp = find(~ismember(fn1,fn2));
		for ii = 1:numel(temp)
			% drop unique fields
			common = rmfield(common,fn1{temp(ii)});
		end
		% get the common fields
		fn = fieldnames(common);
		% missing fields in set 1
		for ii = 1:numel(fn)
			% common field - recurse and test
			for jj = 1:min([numel(d1) numel(d2)])
				[common(jj).(fn{ii}) d1(jj).(fn{ii}) d2(jj).(fn{ii})] = ...
					comp_struct(s1(jj).(fn{ii}),s2(jj).(fn{ii}), ...
					prt,pse,tol,mode,recursion + 1, ...
					[n1 '(' num2str(jj) ').' fn{ii}], ...
					[n2 '(' num2str(jj) ').' fn{ii}]);
			end
		end
        
	% one or both not structures - evaluate
	elseif isstruct(s1)
		% first variable is structure - second is not
		if prt
			fprintf(['Recursion level ' num2str(recursion) ': %s is a structure and %s is not a structure\n'],n1,n2);
		end
		if pse
			uiwait(msgbox(sprintf(['Recursion level ' num2str(recursion) ': %s is a structure and %s is not a structure\n'],n1,n2)))
		end
		% flag purge
		flag(1) = 1;
		
	elseif isstruct(s2)
		% second variable is structure - first is not
		if prt
			fprintf(['Recursion level ' num2str(recursion) ': %s is not a structure and %s is a structure\n'],n1,n2);
		end
		if pse; pause; end
		% flag purge
		flag(1) = 1;
        
    else
        % Neither variable is a structure, and they are not equal.   
        % Note that since this function is recursive, we will usually get
        % to this point in the code when comparing the "lowest level"
        % elements of the input structures.
        % Our only concern is to check if the not-equal statement is due to
        % numerical differences below the tolerance level, in which case we
        % treat the variables as equal.
        flag(1) = 1;

        % variables are not the same - explore the difference
        % tolerance error? Variable must be numeric and of the same size.
        if length(size(s1)) == length(size(s2)) && ... %-- same number of dimensions
            min(size(s1) == size(s2)) && ...           %-- same size in each dimensions
            (isa(s1,'numeric') & isa(s2,'numeric'))    %-- both numeric so can be compared to tol
            % tolerance match?
            % if numel(find(abs(s1-s2) < tol)) == numel(s1)
            switch lower(mode)
                case 'absolute'
                    pass = (sum(abs(s1-s2) < tol) == numel(s1));
                case 'fractional'
                    % have to be careful to check for cases where s1=0.
                    pass = true;
                    idx = find(s1==0);
                    if ~isempty(idx)
                        pass = not(any(s2(idx)~=0));
                    end 
                    if pass
                        idx = find(s1~=0);
                        if ~isempty(idx)
                            pass = (sum(abs(s1(idx)-s2(idx))./s1(idx)<tol) == numel(s1));
                        end 
                    end 
                otherwise
                    error(['Mode value ' mode ' not recognised'])
            end
            if pass
                % ---- All elements are the same to within tolerance. Reset
                %      flag.
                if prt>1
                    fprintf(['Recursion level ' num2str(recursion) ': %s and %s differ but within tolerance\n'],n1,n2);
                end
                flag(1) = 0;
            else
                if prt>1
                    fprintf(['Recursion level ' num2str(recursion) ': %s and %s differ by more than tolerance\n'],n1,n2);
                end
            end
        else
            % ---- We don't really care about why the variables are
            %      different. But we'll check for difference in class and
            %      optionally dump values to the screen.        
            % class error
            if ~strcmp(class(s1),class(s2))
                % different classes
                if prt
                    fprintf(['Recursion level ' num2str(recursion) ': %s is class %s and %s class %s\n'],n1,class(s1),n2,class(s2));
                end
                if pse; pause; end
            else
                % print and pause
                if prt > 1
                    fprintf(['Recursion level ' num2str(recursion) ': first input %s is \n'],n1); disp(s1);
                    fprintf('\n second input %s is \n',n2); disp(s2);
                end
                if pse > 1; pause; end
            end
        end
    end
    
end

%% keep or delete
if flag(1) && ~flag(2)
	% denote error
	common = [];
elseif ~flag(2)
	% inputs are equal - set difference structures to empty arrays
	d1 = [];
	d2 = [];
end


%% purge empty fields
% test common
if isstruct(common)
	% fieldnames
	fn = fieldnames(common);
	for ii = 1:numel(fn)
		temp = 1;
		% test for empty field
		for jj = 1:numel(common)
			if ~isempty(common(jj).(fn{ii})); temp = 0; end
		end
		% purge if field was empty
		if temp
			common = rmfield(common,fn{ii});
		end
	end
end
% test d1
if isstruct(d1)
	% fieldnames
	fn = fieldnames(d1);
	for ii = 1:numel(fn)
		temp = 1;
		% test for empty field
		for jj = 1:numel(d1)
			if ~isempty(d1(jj).(fn{ii})); temp = 0; end
		end
		% purge if field was empty
		if temp
			d1 = rmfield(d1,fn{ii});
		end
	end
end
% test d2
if isstruct(d2)
	% fieldnames
	fn = fieldnames(d2);
	for ii = 1:numel(fn)
		temp = 1;
		% test for empty field
		for jj = 1:numel(d2)
			if ~isempty(d2(jj).(fn{ii})); temp = 0; end
		end
		% purge if field was empty
		if temp
			d2 = rmfield(d2,fn{ii});
		end
	end
end


%% test for null fields
if isstruct(common)
	% fieldnames
	if isempty(fieldnames(common)); common = []; end
end
if isstruct(d1)
	% fieldnames
	if isempty(fieldnames(d1)); d1 = []; end
end
if isstruct(d2)
	% fieldnames
	if isempty(fieldnames(d2)); d2 = []; end
end
