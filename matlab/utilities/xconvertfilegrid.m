function []=xconvertfilegrid(skypos_infile,trigger_time,skypos_outfile)
% XCONVERTFILEGRID: Converts sky grid description in RA,DEC coordinates
% to earth fixed coordinates at trigger time.
%
% usage: xconvertfilegrid(skypos_infile,trigger_time,skypos_outfile)
%
% skypos_infile               String. Name of input sky positions file.
%                             4 column file: RA, DEC, probability, area
% trigger_time                String. GPS time to which skypos_outfile 
%                             should corresponds, e.g., GRB trigger time.
% skypos_outfile              String. Name of output sky positions file. 
%                             4 column file: theta, phi, probability, area
%
% Currently (r3376) the area of the region closest to a given point in the grid
% is not used in the analysis.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              Preliminaries.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check number of input args
error(nargchk(3, 3, nargin));

% ---- Convert input variables to appropriate type.
trigger_time  = str2num(trigger_time);

data = load(skypos_infile);

if size(data,2) ~=4
   error(['sky positions file: ' skypos_infile ' should contain 4 columns. ']);
end

% ---- Read contents of sky_pos_file.
%      These should be theta, phi corresponding to the time of the trigger.
ra_trigger  = data(:,1);
dec_trigger    = data(:,2);
pOmega_trigger = data(:,3);
dOmega_trigger = data(:,4);

% ---- Convert to earth based coords at trigger_time
[phi_trigger, theta_trigger] = radectoearth(ra_trigger,dec_trigger,trigger_time);

% ---- Write sky positions array at block time.
skyPositions(:,1) = theta_trigger;
skyPositions(:,2) = phi_trigger;
skyPositions(:,3) = pOmega_trigger;
skyPositions(:,4) = dOmega_trigger;

% ---- Write output file.
dlmwrite(skypos_outfile,skyPositions,'delimiter',' ','precision','%7.5f');

% ---- done.
