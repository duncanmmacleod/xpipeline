function r = gwbrange(Egw,hrss,f,S)
% GWBRANGE - Distance relating GW burst energy to RSS amplitude or SNR.
%
% usage:
%
%   r = gwbrange(Egw,hrss,f)
%
% Egw   Scalar. Energy in GWs [solar masses].
% hrss  Scalar or vector. Root-sum-square amplitude of the GWB
%       [Hz^{-1/2}]. 
% f     Scalar or vector. Central frequency of the GWB [Hz]. If both
%       hrss and f are vectors, they must have the same size.
%
% r     Scalar or column vector. Distance to the source [pc] for which the
%       given energy in GWs Egw produces the given RSS amplitude hrss assuming
%       isotropic emission. It is based on the formula
%         Egw = c/(G*Msun)*(pi*r*f*hrss)^2;
%
%   r = gwbrange(Egw,snr,f,S)
%
% Egw   Scalar. Energy in GWs [solar masses].
% snr   Scalar. Matched-filter SNR at which the GWB is detectable. 
% f     Scalar or vector. Central frequency of the GWB [Hz]. 
% S     Scalar or vector. Noise power spectrum of the background noise [Hz^-1]. 
%       Must be the same size as f.
%
% r     Scalar or column vector. Distance to the source [pc] for which the
%       given energy in GWs Egw produces the given SNR in the background noise
%       spectrum S:
%         r = [G*Egw/(2*pi^2*c^3*S*f^2*snr^2)]^0.5;
%       This formula is valid to within a few percent for unpolarized, linearly
%       polarised, and elliptically polarized emission.
%
% Both versions of this function assume a narrowband signal. See 1304.0210 for
% details. 
%
% $Id$

% ---- Needed constants.
speedOfLight = 299792458;  %-- m/s
NewtonG = 6.67e-11;        %-- Newton's G (Nm^2/kg^2=m^3/kg/s^2).
secondsPerYear = 365.25*86400;              %-- s
parsec = 3.26*speedOfLight*secondsPerYear;  %-- m
solarMass = 1.988435e30;   %-- kg

% ---- Check number of input arguments and assign defaults.
error(nargchk(3, 4, nargin));
if nargin==4
    % ---- "hrss" input is really snr.
    snr = hrss;
    snrFlag = true;
else
    snrFlag = false;    
end

% ---- Compute range.
if snrFlag

    % ---- r vs. f using effective range formula.
    
    % ---- Check sizes of input arguments.
    if length(Egw)>1
        error('Input argument Egw must be a scalar.');
    end
    if length(snr)>1
        error('Input argument snr must be a scalar.');
    end
    if size(f)~=size(S)
        error('Input vectors f, S must have the same size.');
    end
    
    % ---- Average range [pc] at which GWB is detectable at the specified SNR.
    r = (NewtonG*Egw*solarMass/(2*pi^2*speedOfLight)./(S.*f.^2*snr^2)).^0.5 / parsec;

else
    
    % ---- r vs. hrss for isotropic emission.
    
    % ---- Check sizes of input arguments.
    if length(Egw)>1
        error('Input argument Egw must be a scalar.');
    end
    if length(hrss)>1 & length(f)>1 & size(hrss)~=size(f)
        error('Input vectors hrss, f must have the same size.');
    elseif length(hrss)>1 & length(f)==1 
        f = f*ones(size(hrss));
    elseif length(hrss)==1 & length(f)>1 
        hrss = hrss*ones(size(f));
    end
    
    % ---- Range [pc] to GWB source which gives desired hrss given Egw].
    r = (NewtonG*Egw*solarMass/speedOfLight)^0.5 / (pi*parsec) ./ (hrss.*f);

end

% ---- Done.
return

