function [] = xwriteonoffasciifiles(matFile, jobs, outputfile,threshold)
% XWRITEONOFFASCIIFILES - convert matlab trigger files to ascii for MVA analysis
%
% XWRITEONOFFASCIIFILES writes selected triggers from an on-source and
% off-source merged.mat file into a space-delimited ascii file with
% root-formatted header info, to be used in a TMVA analysis.
% 
% usage:
% 
% xwriteonoffasciifiles(matFile, jobs, outputfile)
% 
%  matFile          String. Full path to on- or off-source .mat file.
%  jobs             Vector. List of jobNumbers whose triggers are to be
%                   written to the ascii file.
%  outputfile       String. Name of output ascii file.
%  threshold        Triggers with significance below the threshold are not used.
%
% The meaning of each column in the output file is usually clear from the header 
% information. However, the last few columns require explanation:
%
%       injIdx:  0 for all on-/off-cource triggers.
%   triggerIdx:  index (row number) of trigger in the cluster struct in original 
%                merged.mat file 
%      uniqIdx:  1,...,N in the order they appear in the matlab struct and the 
%                output txt file
%
% Remember that typically only half the triggers appear in a given txt file 
% (training or testing), so we expect 1 <= triggerIdx <= M, and 
% 1 <= uniqIdx <~ M/2.
%
% $Id$

% ---- Checks.
error(nargchk(3,4,nargin));
%narginchk(3, 3);

% ---- Load matlab triggers.
offsource = load(matFile);

% ---- Initialise a mask used to select triggers.
mask = false(length(offsource.cluster.jobNumber),1);

% --- Loop over jobs, updating mask as we go.
for iJob = 1:length(jobs)
    % ---- Find triggers corresponding to current jobNumber and add to mask.
    %      (This is slightly faster than the equivalent two-step version below.)
    mask = or(mask,(offsource.cluster.jobNumber == jobs(iJob)));
    % ---- Remove triggers with significance below threshold
    mask = and(mask,offsource.cluster.significance>threshold);
    %mask = or(mask,(offsource.cluster.jobNumber == jobs(iJob)));
    %offsource.cluster.significance(mask)
    % % ---- Find triggers corresponding to current jobNumber.
    % currentMask = (offsource.cluster.jobNumber == jobs(iJob)); 
    % % ---- Update mask.
    % mask = or(mask,currentMask);
end
%clear currentMask 
fprintf(1,'Total number of triggers extracted: %d \n', sum(mask));

% ---- Extract triggers we are interested in.
triggerIdx = find(mask);
data = xclustersubset(offsource.cluster,triggerIdx);
likelihoodType = offsource.likelihoodType;
clear offsource

% ---- Verbosity.
fprintf(1,'Writing file %s ... \n',outputfile);

% ---- Open output file and write header info.
fid = fopen(outputfile,'w');
fprintf(fid,'#significance/F:');
for iLike = 1:length(likelihoodType)
   fprintf(fid,'%s/F:', likelihoodType{iLike});
end
fprintf(fid,'jobNumber/I:');
fprintf(fid,'nPixels/I:');
fprintf(fid,'peakTime/F:');
fprintf(fid,'peakFrequency/F:');
fprintf(fid,'minTime/F:');
fprintf(fid,'minFreq/F:');
fprintf(fid,'deltaTime/F:');
fprintf(fid,'deltaFreq/F:');
fprintf(fid,'injIdx/I:');
fprintf(fid,'triggerIdx/I:');
fprintf(fid,'uniqIdx/I:'); %-- "Unique index": a counter to distinguish triggers
fprintf(fid,'filename/S:\n');

% ---- Now write trigger data.
Ntrig = length(data.significance);
outdata = [data.significance, data.likelihood, data.jobNumber, data.nPixels, ...
    data.peakTime, data.peakFrequency, data.boundingBox, zeros(Ntrig,1), triggerIdx, [1:Ntrig]'];
% ---- Format string for fprintf call. Include (fixed) matlab file name as part
%      of format string. This allows us to call fprintf once in vectorised form
%      for a massive speed increase.
formatStr = '%12.4f ';
for iLike = 1:length(likelihoodType)
    formatStr = [formatStr '%12.4f '];
end
formatStr = [formatStr '%d %d %12.4f %12.4f %12.4f %12.4f %12.4f %12.4f %d %d %d ' matFile ' \n'];
fprintf(fid,formatStr,outdata');
fclose(fid);
fprintf(1,'Done! \n')

% ---- Done.
return
