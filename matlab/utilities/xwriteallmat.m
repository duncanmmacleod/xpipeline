function [] = xwriteallmat(matDir_original, tmvaDir, matDir_new)
% XWRITEALLMAT - merge TMVA classification from ascii files into matlab files
%
% XWRITEALLMAT reads the ascii files produced by TMVA and merges the
% classification result into pre-existing matlab-formatted trigger files. 
% It does this for the on-source and off-source triggers and for the
% injection set used for MVA training, replacing the the original X-Pipeline
% significance with the value of the MVA classifier for each trigger.
% XWRITEALLMAT calls both XWRITEOFFMATFILEFROMMVAASCII and 
% XWRITEINJMATFILEFROMMVAASCII.  
%
% usage:
%
%  xwriteallmat(matDir_original, tmvaDir, matDir_new)
%
%  matDir_original      String. Full path to directory containing the
%                       original X-Pipeline merged.mat files.
%  tmvaDir              String. Full path to the directory containing the
%                       ASCII files produced by TMVA.
%  matDir_new           String. Full path to the directory where the new
%                       merged.mat files are to be written.
%
% To process injection sets that were not used for MVA training, call
% XWRITEINJMATFILEFROMMVAASCII directly.
%
% original write: Duncan Meacher, 24/3/11
% $Id$

% ---- Check inputs.
error(nargchk(3,3,nargin));
%narginchk(3, 3);

% ---- Names of original matlab on/off files.
on_mat_original  = sprintf('%s/on_source_0_0_merged.mat',matDir_original);
off_mat_original = sprintf('%s/off_source_0_0_merged.mat',matDir_original);
% ---- Directory holding original matlab injection files.
inj_mat_original = sprintf('%s',matDir_original);

% ---- Names of output ascii files from TMVA.
tmva_bk_train  = sprintf('%s/Output_background_training.txt',tmvaDir);
tmva_bk_test   = sprintf('%s/Output_background_testing.txt',tmvaDir);
tmva_on        = sprintf('%s/Output_on_source.txt',tmvaDir);
tmva_sig_train = sprintf('%s/Output_signal_training.txt',tmvaDir);
tmva_sig_test  = sprintf('%s/Output_signal_testing.txt',tmvaDir);

% ---- Names of mew matlab on/off files.
on_mat_new  = sprintf('%s/on_source_0_0_merged.mat',matDir_new);
off_mat_new = sprintf('%s/off_source_0_0_merged.mat',matDir_new);
% ---- Directory holding new matlab injection files.
inj_mat_new = sprintf('%s',matDir_new);

% ---- Merge TMVA classification data from ASCII files into matlab files.
% ---- Background data.
xwriteoffmatfilefromMVAascii(off_mat_original, tmva_bk_train, tmva_bk_test,off_mat_new)
% ---- On-source data.
xwriteoffmatfilefromMVAascii(on_mat_original, [], tmva_on, on_mat_new)
% ---- Injection set used for training (convert both training and testing results).
xwriteinjmatfilefromMVAascii(inj_mat_original, tmva_sig_train, tmva_sig_test, inj_mat_new)

% ---- Done.
return


% ---- Done.
return
