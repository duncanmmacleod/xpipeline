function Tfft = optimaltfft(h,fs,S,fmin,df,optMetric,verbose)
% OPTIMALTFFT - determine optimal FFT length for time-frequency analysis
%
% usage:
%
%   Tfft = optimaltfft(h,fs,S,fmin,df,metric)
%
% h         Vector. Injection strain data.
% fs        Scalar. Sample rate of h [Hz].
% S         Vector. Noise power spectral density [Hz^-1].
% fmin      Scalar. Starting frequency of S [Hz].
% df        Scalar. Frequency spacing of S [Hz].
% metric    String. Metric by which to select optimal FFT duration. Recognised
%           values are:
%             'matchbw' - Tfft is power of 2 closed to the bandwidth of the
%                         noise-weighted signal. 
%             'maxsnr'  - Tfft is power of 2 that maximises SNR^2 packed into
%                         Npix=50 pixels. 
%             'minNpix' - Tfft is power of 2 that minimises number of pixels
%                         required to capture 90% of SNR^2. 
% verbose   Optional boolean. If true, increases verbosity. Default false.
%
% Tfft      Scalar. Optimal FFT duration [s].
%
% $Id$


switch optMetric

    case 'matchbw' 

        % ---- Reset Tfft to match bandwidth of signal.  
        % ---- Compute noise-weighted bandwidth of the signal using
        %      network-averaged noise spectrum. 
        [snr , hrss, hpeak, Fchar, bw, Tchar, dur] = xoptimalsnr(h,0,fs,S,fmin,df);
        % ---- Pick FFT resolution matching bandwidth.
        tmp = -round(log2(bw));
        if tmp < -7
            tmp = -7;
        elseif tmp > -2
            tmp = -2;
        end
        Tfft = 2^tmp;
        disp(['Reset Tfft = ' num2str(Tfft) ' to match signal bandwidth.'])

    otherwise

        % ---- Reset Tfft to maximise SNR2 packed into Npix pixels or
        %      minimise Npix for fixed SNR2.
        % ---- Start by setting dummy values.
        maxSNR2    = -Inf;
        mincount90 =  Inf;
        
        % ---- Vector of frequencies of S.
        fS = fmin + [0:length(S)-1]'*df;
        
        % ---- Loop over (hardwired) candidate FFT durations and perform TF analysis.
        for Tfft = 2.^[-8:1];

            % ---- Default spectrogram parameters. The noverlap and wind choices
            %      match what's used in X-Pipeline. The Npix value is arbitrary 
            %      but smallish -- larger than needed for sine-Gaussians but
            %      too small for BNS.  
            wind = modifiedhann(fs*Tfft);
            noverlap = fs*Tfft/2;
            Npix = 15;

            % ---- Select TF pixels to analyse by selecting them from a
            %      noise-weighted TF map. 
            %
            % % ---- Option 1: Make strain spectrogram then weight by noise
            % %      spectrum. Prone to NaNs where estimated/modelled S ~=0 .
            % [fh,F] = spectrogram(h,wind,noverlap,Tfft*fs,fs);
            % map = abs(fh).^2;
            % % ---- Noise-weight map using network-averaged spectrum
            % %      interpolated to frequencies of spectrogram. 
            % map = map ./ repmat(interp1(fS,S,F),1,size(map,2));
            %
            % ---- Option 2: Make noise-weighted spectrogram directly from
            %      noise-weighted injection returned from xoptimalsnr.
            [~, ~, ~, ~, ~, ~, ~, ~, ~, ~, hw] = xoptimalsnr(h,0,fs,S,fmin,df);
            [fh,F] = spectrogram(hw,wind,noverlap,Tfft*fs,fs);
            map = abs(fh).^2;
            %
            if verbose 
                figure; 
                imagesc([0;length(h)/fs],[0;fs/2],map); 
                colorbar; 
                set(gca,'ydir','reverse'); 
                xlabel('time [s]'); 
                ylabel('frequency [Hz]');
                title(['Tfft = ' num2str(Tfft) ' [s]']);
            end
            
            switch optMetric

                case 'minNpix'
                                        
                    % ---- Find minimum number of pixels required to hold
                    %      90% of the SNR^2.
                    smap    = sort(map(:),'descend');
                    csmap   = cumsum(smap);
                    csmap   = csmap/csmap(end);
                    count90 = find(csmap>=0.9);
                    if length(count90)>0
                        count90 = count90(1);
                        if count90<mincount90
                            mincount90 = count90;
                            Tfft_opt   = Tfft;
                            Npix_opt   = count90;
                        end
                    end
                    
                case 'maxsnr'
                    
                    % ---- Find maximum SNR^2 contained in the fixed number
                    %      Npix loudest pixels. 
                    % ---- Set threshold to retain only the Npix loudest pixels.
                    thresh = prctile(map(:),100*(1-Npix/prod(size(map))));
                    idx    = find(map > thresh);
                    % [irow,icol] = ind2sub(size(map),idx);
                    % ---- Fractional SNR2 accumulated.
                    SNR2 = sum(map(idx))/sum(map(:));
                    if SNR2>maxSNR2
                        maxSNR2  = SNR2;
                        Tfft_opt = Tfft;
                        Npix_opt = Npix;
                    end
                    
                otherwise
                    
                    error(['metric ' optMetric ' not recognised.']);
                    
            end
            
        end
                
        % ---- Reset to use optimal Tfft / Npix.
        Tfft = Tfft_opt;
        Npix = Npix_opt;
        if verbose
            switch optMetric
                case 'minNpix'
                    disp(['Reset Tfft = ' num2str(Tfft) ', Npix = ' num2str(Npix) ' to minimise Npix.'])
                case 'maxsnr'
                    disp(['Reset Tfft = ' num2str(Tfft) ' to maximise SNR2.'])
            end
        end
        
end

        
