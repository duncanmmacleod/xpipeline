function plottfboxes(injFile,injScale,logFile,injIdxAll,onSource,gps,catDir,plotName)
% PLOTTFBOXES - plot time-frequency bounding boxes of injections and on-source events
%
% usage:
%
%   plottfboxes(injFile,injScale,logFile,injIdx,onSource,gps,catDir,plotName) 
%
% injFile    String. Name of injection associated trigger file or merged trigger
%            file; see below. Use '' if no injections are to be plotted.
% injScale   Vector. Indices of injection scale values of injections to plot. 
%            Required if plotting merged trigger files from xmerge. Otherwise
%            use [] to plot all injection scales. *** NOTE: this function reads
%            injection scales from 1:N rather than 0:N-1. ***
%            Use [] if no injections are to be plotted.
% logFile    String. Name of injection log file. Used for predicting expected tf 
%            boxes. Use '' if no predictions are to be plotted.
% injIdx     Vector. Indices (rows of injection struct and log file) of 
%            injections to plot. Use [] if no injections or predictions are to
%            be plotted.
% onSource   String or struct. String: Name of on-source merged file. Struct: 
%            x-pipeline struct of on-source triggers (or off-source triggers
%            from a single background trial). Use '' if no triggers are to be 
%            plotted. Note: for correct noise-weighting of predicted tf boxes,
%            the struct must also contain these fields:
%                analysisTimesCell
%                amplitudeSpectraCell
%                sampleFrequency
% gps        Optional scalar or two-element vector. Scalar: GPS time to be used
%            as "time 0" on plots. Vector: [start,end] times for plot range. 
%            Default 0.
% catDir     Optional string. Directory containing waveform catalogs. Default 
%            '/home/xpipeline/xpipeline/branches/waveforms/' (at CIT).
% plotName   Optional string. If specified then these plots are saved in the  
%            current directory for each injIdx value:
%                <plotName>_triggers_inj_<injIdx>.png
%                <plotName>_spectra.png
%            Default false.
%
% PLOTTFBOXES can plot injection triggers from an injection-associated trigger
% file (as produced by xmakegrbwebpage) or from the merged injection trigger
% files produced by xmerge. To plot an injection associated trigger file, supply
% the full file name, e.g.
%
%   plottfboxes('GRB200418_run10_closedboxassociatedTriggers_sgc1000q9.mat',...)
%   
% To plot triggers from the merged files, supply the file base name up to but 
% not including the injection scale index, e.g.
%
%   plottfboxes('output/simulation_adi-c_', ...)
%
% In this case PLOTTFBOXES will supply the rest of the expected file name 
% ([num2str(index) '_0_0_merged.mat']) using the user-specified injScale values.
%
% PLOTTFBOXES will also plot the network-summed noise spectrum (defined as 
% 1/S=1/S_1+1/S_2+...) if the noise spectra are supplied in onSource.
%
% $Id$


% ------------------------------------------------------------------------------
%   Preliminaries.
% ------------------------------------------------------------------------------

% ---- Check inputs and assign defaults.
narginchk(5,8)
savePlots = true;
if nargin<8
    savePlots = false;
end
if nargin<7
    catDir = '/home/xpipeline/xpipeline/branches/waveforms/';
end
if nargin<6
    gps = 0;
end

% ---- Check on time range to be plotted.
if length(gps)==2
    gps_start = gps(1);
    gps_end   = gps(2);
    gps       = gps(1);
end

% ---- Check to see which tf boxes we are plotting.
plotInj = true;
if isempty(injFile)  
    plotInj = false;
end
plotPred = true;
if isempty(logFile)  
    plotPred = false;
end
plotOn = true;
if isempty(onSource)
    plotOn = false;
end

% ---- Set number of tf-box plots to make to 1 if there are no injections or
%      predictions to plot.
if plotInj == false && plotPred == false
    injIdxAll = 1;  %-- dummy value: any scalar will do
end


% ------------------------------------------------------------------------------
%    Load injection triggers, if required.
% ------------------------------------------------------------------------------

if plotInj

    % ---- Load injection triggers.
    if exist(injFile)==2

        % ---- injFile is a single file that exists. We assume this to be an
        %      injection-associated trigger file.  
        [trig, pmask, iscale, fI, mI] = loadinjassoctrigfile(injFile,true,0,Ninj);
        rawTriggers = false;

        % ---- Assign default injection scales, if needed.
        if isempty(injScale)
            injScale = 1:length(iscale);
        end

    else

        % ---- Attempt to load raw triggers from merged files.
        if isempty(injScale)
            error(['Input injScale must be specified and non-empty when ' ...
	           'loading merged file triggers.'])
        end
    
        for iScale = injScale(:).'
            disp(['Loading ' injFile num2str(iScale-1) '_0_0_merged.mat'])
            trig{iScale} = load([injFile num2str(iScale-1) '_0_0_merged.mat']);
        end
        rawTriggers = true;

    end

end


% ------------------------------------------------------------------------------
%    Load injection log file, if required.
% ------------------------------------------------------------------------------

if plotPred
    % ---- Load injection parameters.
    injParam = parseinjectionparameters(readinjectionfile(logFile));
    % ---- Number of injections.
    Ninj = length(injParam.gps_s);
end


% ------------------------------------------------------------------------------
%    Load on-source triggers, if required.
% ------------------------------------------------------------------------------

% ---- Load on-source triggers, if required.
if plotOn 

    if isstr(onSource) 
        on = load(onSource);
        onBB                 = on.cluster.boundingBox;
    elseif isstruct(onSource)
        on                   = onSource;
        onBB                 = onSource.boundingBox;
    end

    % ---- Extract bounding box and reset origin of time coordinate.
    onBB(:,1) = onBB(:,1) - gps;

    % ---- Determine an average noise spectrum. Use spectrum sampled at 1 Hz if
    %      available, otherwise use highest resolution available.
    if isfield(on,'analysisTimesCell') && isfield(on,'amplitudeSpectraCell') ...
        && isfield(on,'sampleFrequency')
        idxFFT = find(cell2mat(on.analysisTimesCell)==1);
        df = 1;
        if isempty(idxFFT)
            [dt,idxFFT] = max(cell2mat(on.analysisTimesCell));
            df = 1/dt;
        end
        asd = on.amplitudeSpectraCell{idxFFT}; %-- 1 Hz resolution
        f = [0:df:on.sampleFrequency/2]';
        S_avg = sum(asd.^(-2),2).^(-1);

        % ---- Plot noise spectra.
        figure; 
        loglog(f,asd)
        grid on; hold on
        loglog(f,S_avg.^0.5,'k-')
        set(gca,'fontsize',16)
        xlabel('frequency [Hz]')
        ylabel('noise amplitude [Hz^{-1/2}]')
        legendText = [];
        if isfield(on,'detectorList')
            legendText = on.detectorList;
        else
            for iDet = 1:size(asd,2)
                legendText{end+1} = ['detector ' num2str(iDet)];
            end
        end
        legendText{end+1} = 'effective sum';
        legend(legendText)
        title(strrep(injFile,'_','\_'));
        specPos = get(gcf,'position');
        specPos(3) = 780;
        set(gcf,'position',specPos);
        axis([10 on.sampleFrequency/2 1e-24 1e-19])
        if savePlots
            saveas(gcf,[plotName '_spectra.png'],'png');
            saveas(gcf,[plotName '_spectra.fig'],'fig');
        end 
    end

end


% ------------------------------------------------------------------------------
%    Make plots.
% ------------------------------------------------------------------------------

% ---- Loop over plots / injections to study.
for iInj=1:length(injIdxAll)

    % ---- Initialise plot.
    figure; 
    grid on; hold on;
    legendText = [];

    % ---- Select current injection to process. 
    injIdx     = injIdxAll(iInj);


    % --------------------------------------------------------------------------
    %   Generate prediction for injection properties based on log file.
    % --------------------------------------------------------------------------

    if plotPred

        % ---- Make injection timeseries for comparison.
        paramStr = [ num2str(injParam.gps_s(injIdx)) ' ' ...
                     num2str(injParam.gps_s(injIdx)) ' ' ...
                     num2str(injParam.phi(injIdx)) ' ' ...
                     num2str(injParam.theta(injIdx)) ' ' ...
                     num2str(injParam.psi(injIdx)) ' ' ...
                     injParam.type{injIdx} ' ' ...
                     injParam.parameters{injIdx} ];

        % ---- Make x-pipeline injection log file.
        [status,result] = system(['echo ' paramStr ' > test.log']);

        % ---- Compute time interval of interest. A +/-256 window is long enough
        %      to handle any injection the current version of x-pipeline will 
        %      inject without clipping. 
        blockTime = 512;
        startTime = floor(injParam.gps_s(injIdx)) - blockTime/2;

        % ---- Make x-pipeline injection timeseries. Only need it for one 
        %      detector; H1 is the most commonly used reference detector in 
        %      x-pipeline analyses so use that. But the choice should be 
        %      irrelevant unless we need the timing to millisecond precision or
        %      we are very unlucky with the injection sky position. 
        channel = {'H1'};
        injFileName = 'test.log';
        noAntResp = 0;
        injNumber = 0;
        fs = 16384;
        % ---- Make timeseries. Suppress warnings that are common with
        %      processing catalog waveforms. 
        warning off
        [injData, gps_s, gps_ns, phi, theta, psi] = xinjectsignal( ...
             startTime,blockTime,channel,fs*ones(1,length(channel)), ...
             injFileName,noAntResp,injNumber,'catalogDirectory',catDir);
        warning on
        injData = injData{1};
        N = size(injData,1);
        t = [0:(N-1)]'/fs + startTime;

        % ---- Clean up (delete) injection log file.
        [status,result] = system(['rm test.log']);

        % ---- Estimate time-frequency properties of the noise-weighted 
        %      injection.
	if exist('S_avg','var')
            [SNR, h_rss, h_peak, Fchar, bw, Tchar, dur, Tchar_w, dur_w, tfbox] = ...
                xoptimalsnr(injData,t(1),fs,S_avg,f(1),f(2)-f(1),[],[],0.5);
        else 
            [SNR, h_rss, h_peak, Fchar, bw, Tchar, dur, Tchar_w, dur_w, tfbox] = ...
                xoptimalsnr(injData,t(1),fs);
        end
        % ---- Extract the bounding box and reset time origin to gps.
        predBB = tfbox;
        predBB(1) = predBB(1) - gps;

        % ---- Plot time-frequency bounding boxes.
        rectangle('position',predBB,'FaceColor','y','EdgeColor','y')

    end


    % --------------------------------------------------------------------------
    %   Plot on-source triggers.
    % --------------------------------------------------------------------------

    if plotOn

        % ---- Plot time-frequency bounding boxes.
        for ii=1:size(onBB,1)
            if ~isfield(on,'pass') || on.pass(ii)==0
                rectangle('position',onBB(ii,:),'EdgeColor','k','linewidth',2);
            else 
                rectangle('position',onBB(ii,:),'EdgeColor','b','linewidth',2);
            end
        end

    end


    % --------------------------------------------------------------------------
    %   Extract the injection triggers of interest.
    % --------------------------------------------------------------------------

    if plotInj

        % ---- Select injection triggers to process. The plotting code to follow
        %      requires two variables: injBB (bounding box array) and injPass 
        %      (pass vector).
        if rawTriggers

            % ---- Extract triggers from merged lists.
            injBB = [];
            for iScale = injScale(:).'
                tmp = trig{iScale};
                injBB = [injBB; tmp.clusterInj(injIdx).boundingBox];
            end
            injBB(:,1) = injBB(:,1) - gps;
            injPass = zeros(size(injBB,1),1);

        else

            % ---- Extract triggers.
            injTrig    = xclustermerge(trig(injIdx,injScale(:)));
            % ---- Extract bounding boxes and reset time origin (first element)
            %      to gps.
            injBB      = injTrig.boundingBox;
            injBB(:,1) = injBB(:,1) - gps;
            injPass    = injTrig.pass;

        end

        % ---- Plot time-frequency bounding boxes.
        for ii=1:size(injBB,1)
            if injPass(ii)==0
                rectangle('position',injBB(ii,:),'EdgeColor','r','linewidth',2);
            elseif injPass(ii)==1
                rectangle('position',injBB(ii,:),'EdgeColor','g','linewidth',2);
            end
        end

    end


    % --------------------------------------------------------------------------
    %    Format and save plot.
    % --------------------------------------------------------------------------

    set(gca,'fontsize',16)
    xlabel(['time since GPS ' num2str(gps) ' [s]']);
    ylabel('frequency [Hz]')
    title(strrep(injFile,'_','\_'));
    pos = get(gcf,'position');
    pos(3:4) = [1400 780];
    set(gcf,'position',pos);
    if exist('gps_start','var') && exist('gps_end','var')
        axis([0 (gps_end-gps_start) get(gca,'ylim')])
    end
    if isfield(on,'sampleFrequency')
        axis([get(gca,'xlim') 0 on.sampleFrequency/2])
    end
    % ---- The rectangle() function does not cause lines to be added to the 
    %      legend. So, we add hidden nonsense lines so that legend will display.
    if plotPred
        plot([0;1],[-1;-1],'y-','linewidth',2)
	legendText{end+1} = ['injection ' num2str(injIdx) ' - predicted'];
    end
    if plotOn  
        plot([0;1],[-1;-1],'k-','linewidth',2)
        plot([0;1],[-1;-1],'b-','linewidth',2)
	legendText{end+1} = ['on-source (before cuts)'];
	legendText{end+1} = ['on-source (after cuts)'];
    end
    if plotInj 
        plot([0;1],[-1;-1],'r-','linewidth',2)
        plot([0;1],[-1;-1],'g-','linewidth',2)
	legendText{end+1} = ['injection ' num2str(injIdx) ' - recovered (before cuts)'];
	legendText{end+1} = ['injection ' num2str(injIdx) ' - recovered (after cuts)'];
    end
    legend(legendText)
    % ---- Add dashed lines showing analysis block boundaries. Number required
    %      to cover on-source region can vary so determine that from x-axis
    %      range. 
    % ---- Hardwire aLIGO-standard analysis duration of 224 sec per block.
%    analDur = 224;
%    xrange = get(gca,'xlim');
%    yrange = get(gca,'ylim');
%    edges = [xrange(1):analDur:xrange(2)];
%    plot([edges;edges],yrange,'m--')
%    if rawTriggers
%        legend(['injection ' num2str(injIdx) ' - theoretical'],['n/a'], ...
%            ['injection ' num2str(injIdx) ' - raw'],'glitches','segment boundaries')
%    else
%        legend(['injection ' num2str(injIdx) ' - theoretical'],['injection ' num2str(injIdx) ' - recovered'], ...
%            ['injection ' num2str(injIdx) ' - vetoed'],'glitches','segment boundaries')
%    end
    if savePlots
        saveas(gcf,[plotName '_triggers_inj_' num2str(injIdx) '.png'],'png');
        saveas(gcf,[plotName '_triggers_inj_' num2str(injIdx) '.fig'],'fig');
    end

end


% ---- Done
return
