function [minBinProbPerc1, minBinProbPerc2, minBinProbPerc2Err, index_lower, index, index_upper] = binomialtestthreshold(Ndraws,Ntail,significance,Nmc,slowAnalysis) 
% BINOMIALTESTTHRESHOLD - Compute p value required for given binomial significance.
%
% BINOMIALTESTTHRESHOLD computes the largest p value that will give a
% specified significance in the binomial test.
% 
% usage:
%
%  [P1, P2, P2Err, IL, I, IU] = binomialtestthreshold(Ndraws, ...
%      Ntail,significance,Nmc,slowAnalysis) 
%
%  Ndraws       Scalar.  Number of p values (draws) in the binomial test. 
%  Ntail        Scalar.  Number of p values in the tail to be tested.
%  significance Scalar.  Desired significance of the binomial test
%               result (false alarm probability under the null hypothesis);
%               must be between 0 and 1.   
%  Nmc          Optional scalar.  Number of Monte Carlo trials to use in
%               computing output.  Default 1e4.
%  slowAnalysis Optional scalar flag.  If nonzero, then force a slower analysis 
%               mode that is more accurate for significance >~ 0.01.  Default 0.
%
%  P1, P2       Vectors of same size as 'significance'.  Value of binomial 
%               probability need to achieve corresponding significance under
%               the null hypothesis.  P1 is computed from the percentiles of 
%               the distribution of sampled binomial probabilites generated  
%               using the null hypothesis.  P2 is computed similarly, but using 
%               the average of percentiles of a number of smaller Monte Carlo
%               experiments.  The P2 procedure may be less accurate but gives i
%               an estimate of the uncertainty in P2.
%  P2Err        Vector. Estimated 1-sigma uncertainty in P2.
%  IL, I, UL    Vectors.  Used for debugging.
%
% $Id$

% ---- Largest number of Monte Carlo trials to perform in one go.
NmcMax = 1e5;

% ---- Checks on arguments.
nargchk(3,5,nargin);
if nargin<5
    slowAnalysis = 0;
end
if nargin<4
    Nmc = NmcMax;
end
if ~(isscalar(Ndraws) & isscalar(Ntail) & isscalar(significance) & isscalar(Nmc))
    error('All input arguments must be scalars.'); 
end

% ---- Maximum value of NmcMax*Ntail that my laptop is happy with before
%      grinding into virtual memory hell.  This will keep the one-loop time
%      at O(10) sec.
NmcMax_laptop = 1e5;
% ---- Reset NmcMax as necessary.
if NmcMax*Ntail > NmcMax_laptop
    NmcMax = floor(NmcMax_laptop / Ntail);
    if nargin<4
        Nmc = NmcMax;
    end    
end

if (Nmc <= NmcMax) || slowAnalysis
    
    % ---- Background Monte Carlo to handle trials factor for N>1.
    %      Generate uniformly distributed local probabilities.
    localProb = sort(rand(Nmc,Ndraws),2);
    % ---- Keep Ntail most significant values.
    localProb = localProb(:,1:Ntail);

    % ---- Cumulative binomial probability for each trial.
    binProb = 1 - binocdf(repmat([0:Ntail-1],Nmc,1),Ndraws,localProb);
    minBinProb = sort(min(binProb,[],2));

else
    
    % ---- Lots of Monte Carlo trials required.  Split into subsets of
    %      NmcMax and do as a for loop. 
    Nrun = ceil(Nmc/NmcMax);
    warning(['Large number of Monte Carlo trials requested.  Using for loop.'])
    % ---- Fraction of trials to keep in each iteration of foor loop.
    frac = 0.01; 
    Nkeep = round(frac*NmcMax);
    % ---- Prepare storage.
    minBinProb = zeros(2*Nkeep,Nrun);
    % ---- Loop over runs.
    for irun = 1:Nrun
        
        % ---- Status report.
        disp(['  Loop ' num2str(irun) ' of ' num2str(Nrun) '.']);
        
        % ---- Background Monte Carlo to handle trials factor for N>1.
        %      Generate uniformly distributed local probabilities.
        %      Note: This next line is the slowest part of the code, using
        %      90% of the run time: 60% for sort(), 30% for rand().
        localProb = sort(rand(NmcMax,Ndraws),2);
        % ---- Keep Ntail most significant values.
        localProb = localProb(:,1:Ntail);

        % ---- Cumulative binomial probability for each trial.
        binProb = 1 - binocdf(repmat([0:Ntail-1],NmcMax,1),Ndraws,localProb);
        minBinProb_tmp = sort(min(binProb,[],2));
        
        % ---- Keep 2*Nkeep smallest values.
        minBinProb(:,irun) = minBinProb_tmp(1:2*Nkeep);

    end

    % ---- Danger check.
    minBinProb_max = max(minBinProb,[],1);
    if min(minBinProb_max) < prctile(minBinProb(:),50)
        warning('Bollocks!');
    end
    % ---- Cut down to half.
    ind = find(minBinProb <= prctile(minBinProb(:),50));
    minBinProb = sort(minBinProb(ind));
    
    % ---- Reset significance threshold to account for keeping only 1%
    %      lowest values.
    significance = significance/frac;
    
    % ---- Reset Nmc to number of values retained.
    Nmc = length(minBinProb);

end

% -------------------------------------------------------------------------
%    Method 1: prctile function.
% -------------------------------------------------------------------------

minBinProbPerc1 = prctile(minBinProb,significance*100);

% -------------------------------------------------------------------------
%    Method 2: Percentiles-by-counting.
% -------------------------------------------------------------------------

index = round(significance*Nmc);
index_lower = round(significance*Nmc - (significance*Nmc).^0.5);
index_upper = round(significance*Nmc + (significance*Nmc).^0.5);

% ---- Check for indices out of range [1,Nmc].
bad = find(index<=0);
if ~isempty(bad)
    index(bad) = 1;
end
bad = find(index_lower<=0);
if ~isempty(bad)
    index_lower(bad) = 1;
end
bad = find(index_upper<=0);
if ~isempty(bad)
    index_upper(bad) = 1;
end
bad = find(index>Nmc);
if ~isempty(bad)
    index(bad) = Nmc;
end
bad = find(index_lower>Nmc);
if ~isempty(bad)
    index_lower(bad) = Nmc;
end
bad = find(index_upper>Nmc);
if ~isempty(bad)
    index_upper(bad) = Nmc;
end
minBinProbPerc2 = minBinProb(index);
minBinProbPerc2Err = (minBinProb(index_upper)-minBinProb(index_lower))/2;
    
% ---- Done.
return
