function c = logsumexp(a, b)
% Computes log(exp(a) + exp(b)) for matrices a and b while guarding against
% overfow if exp(a) and or exp(b) are very large
%
% Copied from Omega r2405
% Authors:
% Antony Searle <antony.searle@anu.edu.au>
%
% $Id$

d = max(a, b);
d(d == -Inf) = 0;

c = log(exp(a - d) + exp(b - d)) + d;
