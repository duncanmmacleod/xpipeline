function [] = xwriteallascii(inputDir,injGlob,threshold,injScale_threshold)
% XWRITEALLASCII - convert matlab trigger files to ascii for MVA analysis
%
% XWRITEALLASCII writes ascii files from the merged.mat files for the on
% source, off source and injection files produced by an X-Pipeline analysis. 
%
%  inputDir       String. Full path to the directory containing all
%                 merged.mat files that are to be processed.
%  injGlob        String. We search inputDir for files with the name
%                 *<injGlob>*merged.mat.
%  threshold      This puts a significance threshold on injections
%                 training/testing.
%
% XWRITEALLASCII produces the following ascii files, in the same directory
% as the original matlab files:
%   BG_train.txt  -> off-source training set
%   BG_test.txt   -> off-source testing set
%   ON_test.txt   -> on-source set
%   SIG_train.txt -> injection training set
%   SIG_test.txt  -> injection testing set
% Triggers are split into training and testing sets using the same random seed
% as in xmakegrbwebpage and xmakesnwebpage, so the same triggers will be used 
% for training/testing in TMVA.
%
% This function calls xwriteonoffasciifiles.m and xwriteinjasciifiles.m.
% Where quoted, line numbers correspond to xmakegrbwebapge.m.
%
% original write: J Clark, 21/02/11
% $Id$

'New training sets'

% ---- Checks.
if ~exist('threshold','var'),threshold='0';end 
if ~exist('injScale_threshold','var'),injScale_threshold='10';end 
threshold = str2num(threshold);
injScale_threshold = str2num(injScale_threshold);
error(nargchk(2, 4, nargin));


% ---- Names of input on/off files.
on_matFile  = sprintf('%s/output/on_source_0_0_merged.mat',inputDir);
off_matFile = sprintf('%s/output/off_source_0_0_merged.mat',inputDir);

% ---- Directory holding input injection files.
injDir  = sprintf('%s/output/',inputDir);

% ---- Names for output ascii files.
BG_training_file  = sprintf('%s/output/BG_train.txt',inputDir);
BG_testing_file   = sprintf('%s/output/BG_test.txt',inputDir);
ON_file           = sprintf('%s/output/ON_test.txt',inputDir);
SIG_training_file = sprintf('%s/output/SIG_train.txt',inputDir);
SIG_testing_file  = sprintf('%s/output/SIG_test.txt',inputDir);

% ---- Load on/off data.
on_data  = load(on_matFile);
off_data = load(off_matFile);

% ---- Select BACKGROUND Jobs For TMVA training & testing.
%      These must be identical to the jobs used for X-Pipeline's internal
%      consistency tests so we re-use the appropriate bits of code.

% --- Get total number of off-source jobs (lines 759, 765).  Note
%     distinction between 'jobs' and 'events'!
uniqueOffJobNumber = unique(off_data.cluster.jobNumber);
nOffJobs    = length(uniqueOffJobNumber);
nOffJobsBy2 = ceil(nOffJobs/2);

% --- Get center time for analysis to generate random seed (line 499).
analysis_centerTime =  unique(on_data.cluster.centerTime);

% --- Set random seed for job selection (line 953).
rand('seed',931316785+analysis_centerTime);
initialSeed = rand('seed');
disp(' ');
disp(['initial random seed: ' num2str(initialSeed)]);

% ---- Get random order of indices between 1 and
%      nOffJobsPass (line 1060-1075).
rand('seed',initialSeed + 11);
offJobSeed = rand('seed');
randOffIdx = [0,randperm(nOffJobs-1)];
% ---- Use the first half of randomly ordered injections for tuning.
BG_training_jobs  = randOffIdx(1:nOffJobsBy2);
% ---- Use the second half of randomly ordered injections for ul calc.
BG_testing_jobs  = randOffIdx(nOffJobsBy2+1:nOffJobs);

% ---- Select SIGNAL (injection) jobs for TMVA training & testing.
%      As above, we must be consistent with X-Pipeline about which jobs are 
%      used for training and testing.  This is taken from around line 1958 of
%      xmakegrbwebpage.m

% ---- Load in first merged injection file to find number of injections (we
%      then assume this is the same for all files)
%injFiles = dir([injDir '/*' injGlob '*merged.mat']);
%first_inj_file = load([injDir '/' injFiles(1).name]);
%nInjections = length(first_inj_file.injectionProcessedMask);

% ---- Get random order of indices between 1 and nInjections.
%rand('seed',initialSeed + 22);
%injSeed = rand('seed');
%randInjIdx = randperm(nInjections);
%nInjectionsBy2 = ceil(nInjections/2);

% ---- Use the first half of randomly ordered injections for tuning.
%SIG_training_jobs = sort(randInjIdx(1:nInjectionsBy2));

% ---- Use the second half of randomly ordered injections for ul calc.
%SIG_testing_jobs = sort(randInjIdx(nInjectionsBy2+1:nInjections));

% ---- Produce ASCII files for TMVA input.

% ---- Background training data.
xwriteonoffasciifiles(off_matFile, BG_training_jobs, BG_training_file,0);

% ---- Background testing data.
xwriteonoffasciifiles(off_matFile, BG_testing_jobs, BG_testing_file,0);

% ---- On-source data.
xwriteonoffasciifiles(on_matFile, 0, ON_file,0);

% ---- Signal training data.
%xwriteinjasciifiles(injDir, injGlob, SIG_training_jobs, SIG_training_file, threshold, 1, injScale_threshold)

% ---- Signal testing data.
%xwriteinjasciifiles(injDir, injGlob, SIG_testing_jobs, injDir, threshold, 0)

