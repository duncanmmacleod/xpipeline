function y = ffttimeshift(x,fs,dt)
% FFTTIMESHIFT - time-shift a timeseries using the FFT method
%
% usage
%
%    y = ffttimeshift(x,fs,dt)
%
% x     Vector. Input timeseries.
% fs    Scalar. Sampling rate of timeseries [Hz].
% dt    Scalar. Time shift to be applied.
%
% FFTTIMESHIFT time-shifts the input vector x using the FFT method. The output 
% timeseries is 
%
%   y = real(ifft(exp(-i*2*pi*f*dt) .* fft(x)));
%
% where f is the vector of frequencies corresponding to fft(x). The time shift 
% is positive in the sense that a peak in x at time t0 will appear as a peak in 
% y at t0+dt.
%
% $Id$

% ---- Check inputs.
narginchk(3,3)

% ---- Compute frequency spacing of fft(x). The FFT resolution is always 1/duration.
N  = length(x);  %-- number of samples
T  = N/fs;       % -- timeseries duration
df = 1/T;        %-- FFT resolution

% ---- Compute the phase correction. The trickiest part is knowing the ordering
%      of frequencies from the fft() method. In matlab it is as follows: [zero, 
%      positive frequencies to Nyquist, negative frequencies from first bin 
%      above -Nyquist to last bin below zero.]
f = [0:df:fs/2,-fs/2+df:df:-df]';
phaseFactor = exp(-i*2*pi*f*dt);

% ---- Compute output timeseries, discarding any small imaginary part from 
%      numerical precision limitations.
y = real(ifft(phaseFactor .* fft(x)));

% ---- Done.
return

