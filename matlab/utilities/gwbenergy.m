function [Egw_Msun, Egw_erg] = gwbenergy(r,hrss,f0,emission)
% GWBENERGY - Energy in solar masses in a GW burst.
%
% There are two usage modes:
%
%   [Egw_Msun, Egw_erg] = gwbenergy(r,hrss,f0,emission)
%
% r         Scalar.  Distance to the source [parsec].
% hrss      Scalar or vector.  Root-sum-square amplitude of the GWB [Hz^{-1/2}].
% f0        Scalar or vector.  Central frequency of the GWB [Hz].  If both
%           hrss and f0 are vectors, they must have the same size.
% emission  Optional string. Emission pattern of the source. Recognised values
%           are 'isotropic' (default), 'elliptical', and 'linear'. For 
%           'elliptical' and 'linear' it is assumed that the hrss supplied is
%           that at the inclination of peak emission.
%
% and 
%
%   [Egw_Msun, Egw_erg] = gwbenergy(r,h,fs,emission)
%
% r         Scalar.  Distance to the source [parsec].
% h         Two-column array. The columns are the timeseries GW polarisations 
%           [hp, hc].
% fs        Scalar.  Sampling rate of the timeseries in h [Hz].
% emission  Optional string. Emission pattern of the source. Recognised values
%           are 'isotropic' (default), 'elliptical', and 'linear'. For 
%           'elliptical' and 'linear' it is assumed that the hrss supplied is
%           that at the inclination of peak emission.
%
% Egw_Msun  Scalar or vector.  Energy in GW [solar masses] for each value of r.
% Egw_erg   Scalar or vector.  Energy in GW [erg] for each value of r.
%
% GWBENERGY computes the energy in the GW burst following 1304.0210v1. The first
% form assumes a narrowband signal:
%
%   Egw = factor * c/(G*Msun)*(pi*r*f0*hrss)^2 
%
% The second form computes the energy by integrating the waveform timeseries:
%
%   Egw = factor * r^2*c^3/(4*G)*[\int dt {(dh_+/dt)^2 + (dh_x/dt)^2}]
%
% On both cases 'factor' is a scalar with the numeric value of
%
%   1    - isotropic emission
%   2/5  - elliptical emission
%   8/15 - linear emission
%
% The direct integration method should be accurate to 1% (10%) provided the
% signal frequency is < 20% (37%) of the Nyquist frequency. For 16384 Hz
% sampling rate this means >=1% acuracy for signal frequencies up to 3.2kHz.
% 
% The narrowband approximation is accurate to better than 1% (6%) for
% sine-Gaussians with Q values >=9 (>=3). 
%
% $Id$

% % ----------------------------------------------------------------------------
% %   Test code
% % ----------------------------------------------------------------------------
%
% % ---- This test code demonstrates that the direct integration method using
% %      five-point derivatives and Simpson's rule is accurate to 1% (10%)
% %      provided the signal frequency is < 20% (37%) of the Nyquist frequency. 
% Q = 100;
% f0 = 100;
% fs = 16384*8;
% [t,hp,hc] = xmakewaveform('sg',['1e-21~' num2str(Q) '~' num2str(f0)],10,5,fs);
% Egw_int_0 = gwbenergy(1000,[hp hc],fs);
% for fs = [16384 8192 4096 2048 1024 512 256]
%     [t,hp,hc] = xmakewaveform('sg',['1e-21~' num2str(Q) '~' num2str(f0)],10,5,fs);
%     Egw_int = gwbenergy(1000,[hp hc],fs);
%     disp([num2str(fs) ' ' num2str(f0/(fs/2))  ' ' num2str(Egw_int/Egw_int_0)])
% end
%
% % ----------------------------------------------------------------------------
% %   Test code
% % ----------------------------------------------------------------------------
%
% % ---- This test code demonstrates that the narrowband approximation is
% %      accurate to better than 1% (6%) for Q values >=9 (>=3).
% Q = 100;
% f0 = 128;
% fs = 16384;
% [t,hp,hc] = xmakewaveform('sg',['1e-21~' num2str(Q) '~' num2str(f0)],10,5,fs);
% Egw_int_0 = gwbenergy(1000,[hp hc],fs);
% for Q = [90:-10:20,9,3]
%     [t,hp,hc] = xmakewaveform('sg',['1e-21~' num2str(Q) '~' num2str(f0)],10,5,fs);
%     Egw_int = gwbenergy(1000,[hp hc],fs);
%     disp([num2str(Q) ' ' num2str(Egw_int/Egw_int_0)])
% end
% 
% % ----------------------------------------------------------------------------


% ---- Needed constants.
speedOfLight = 299792458;  %-- m/s
NewtonG = 6.67e-11;        %-- Newton's G (Nm^2/kg^2=m^3/kg/s^2).
secondsPerYear = 365.25*86400;              %-- s
parsec = 3.26*speedOfLight*secondsPerYear;  %-- m
solarMass = 1.988435e30;   %-- kg

% ---- Check number of input arguments.
error(nargchk(3, 4, nargin));

% ---- Check for emission pattern.
emissionFactor = 1; %-- isotropic
if nargin==4
    switch emission
        case 'isotropic'
            emissionFactor = 1;
        case 'elliptical'
            emissionFactor = 2/5;
        case 'linear'
            emissionFactor = 8/15;
        otherwise
            error(['Emission value ' emission ' not recognised.'])
    end
end
    
% ---- Check sizes of input arguments.
if length(r)>1
    error('Input argmuent r must be a scalar.');
end
if size(hrss,1)>1 & size(hrss,2)==2 & numel(f0)==1
    % ---- Input 'hrss' is two-column array of h_+, h_x values and 'f0' is
    %      sampling rate. Compute GWB energy by integrating over the waveform.
    % ---- Rename inputs for clarity.
    h  = hrss;
    hp = hrss(:,1);
    hc = hrss(:,2);
    fs = f0;
    % ---- Isotropic emission.
    %        Fgw = c^3/(16*pi*G*T)*[\int dt {(dh_+/dt)^2 + (dh_x/dt)^2}]
    %        Egw = 4*pi*r^2*T * Fgw
    %            = r^2*c^3/(4*G)*[\int dt {(dh_+/dt)^2 + (dh_x/dt)^2}]
    %      so
    %        Egw/(Msun*c^2) = r^2*c/(4*G*Msun)*[\int dt {(dh_+/dt)^2 + (dh_x/dt)^2}]
    % % ---- Compute derivatives using centred method for O(dt^2) accuracy.
    % hdot = (hp(3:end,:)-hp(1:end-2,:))/(2/fs);
    % ---- Compute derivatives using a five-point method for O(dt^4) accuracy.
    hdot = ( -1*h(5:end,:)+8*h(4:end-1,:)-8*h(2:end-3,:)+1*h(1:end-4,:) ) / (12/fs);

    % ---- All of these approaches to the integration appear to give the same
    %      result. We'll stick with the most theoretically precise one,
    %      Simpson's rule, just in case...
    % % ---- Compute integral using midpoint rule.
    % Egw_Msun = emissionFactor * speedOfLight/(4*NewtonG*solarMass)*(parsec*r)^2 * sum(sum(hdot.^2,2))/fs
    % % ---- Compute integral using trapezoidal rule.
    % Egw_Msun = emissionFactor * speedOfLight/(4*NewtonG*solarMass)*(parsec*r)^2 * trapz(sum(hdot.^2,2))/fs
    % ---- Compute integral using Simpson's rule.    
    f = sum(hdot.^2,2);
    n = length(f)-1;
    if isodd(n)
        f(end) = [];
        n = length(f)-1;
    end
    coeff = 2*ones(size(f));
    coeff(2:2:end) = 4;
    coeff([1,end]) = 1;
    Egw_Msun = emissionFactor * speedOfLight/(4*NewtonG*solarMass)*(parsec*r)^2 * sum(coeff.*f)/(3*fs);    
else
    % ---- Using narrowband approximation for GWB energy.
    if length(hrss)>1 & length(f0)>1 & size(hrss)~=size(f0)
        error('Input vectors hrss, f0 must have the same size.');
    elseif length(hrss)>1 & length(f0)==1 
        f0 = f0*ones(size(hrss));
    elseif length(hrss)==1 & length(f0)>1 
        hrss = hrss*ones(size(f0));
    end
    % ---- Energy in GW, in units of solar masses.
    Egw_Msun = emissionFactor * speedOfLight/(NewtonG*solarMass)*(parsec*pi*r)^2*(f0.*hrss).^2;
end

% ---- Convert to erg (1 erg = 10^{-7} J).
Egw_erg  = Egw_Msun * solarMass * speedOfLight^2 * 1e7;     

% ---- Done.
return
