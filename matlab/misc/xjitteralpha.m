function xjitteralpha(inFile,outFile,alphaMin,alphaMax)
% XJITTERTAU - Randomise injection parameter alpha for DS waveforms.
%
%  inFile     String.  Name of input injection file to be modified.
%  outFile    String.  Name of output file with modified injections.
%  alphaMin     String.  Minimum value of alpha range over which to randomise.
%  alphaMax     String.  Maximum value of alpha range over which to randomise.
%
% $Id$

% ---- Check input.
error(nargchk(4, 4, nargin, 'struct'))
if ~ischar(alphaMin)
    error('Input alphaMin must be a string.');	
else
    alphaMin = str2num(alphaMin);
end
if ~ischar(alphaMax)
    error('Input alphaMax must be a string.'); 
else
    alphaMax = str2num(alphaMax);
end
if ~ischar(inFile) | ~ischar(outFile)
    error('Inputs inFile, outFile must strings.');
end

if isempty(strfind(inFile,'alphajitter'))
  disp(['Skipping alpha jittering, no "alphajitter" string in injection ' ...
        'file name: ' inFile]);
  return
end

% ---- Read and parse input injection file.
injection = readinjectionfile(0,1e10,inFile);
[gps_s, gps_ns, phi, theta, psi, temp_name, temp_parameters] = ...
    parseinjectionparameters(injection);
nInjection = length(gps_s);
% ---- Parser code foolishly outputs "name" and "parameters" as cell arrays
%      of 1x1 cell arrays of strings; reformat to remove extra layer of
%      cell arrays.
name = cell(nInjection,1);
parameters = cell(nInjection,1);
for ii = 1:nInjection
    name{ii} = temp_name{ii}{1};
    parameters{ii} = temp_parameters{ii}{1};
end

% ---- Location of amplitude and central frequency parameters depends on
%      the waveform type.
paramArray = cell(nInjection,1);
amplIndex = zeros(nInjection,1);
for ii = 1:nInjection
    switch lower(name{ii})
        case {'ds','chirplet'}  
            % ---- Chirplet
            %      [h_peak,tau,f0,alpha].
            alphaIndex(ii) = 4;
            paramArray{ii} = tildedelimstr2numorcell(parameters{ii});
            alpha(ii) = paramArray{ii}(alphaIndex(ii));
        otherwise
            error(['Current version of the function can only handle ' ... 
                'DS waveform type.  See xmakewaveform.']); 
   end
end

% ---- Set the random number generator seed based on the name of the
%      input file
randn('state',sum(inFile))
rand('twister',sum(inFile))

% ---- Jitter injection alpha
randalpha = alphaMin + rand(nInjection,1)*(alphaMax - alphaMin);

% ---- Keep alpha of first injection unchanged so that web page
%      captions look sensible.
randalpha(1) = alpha(1);

% ---- Write down the jittered injection parameters
numData = cell(1,1);
nameData = cell(1,1);
paramData = cell(1,1);
numData{1} = [gps_s(:,1), gps_ns(:,1), phi, theta, psi];
nameData{1} = name;
paramData{1} = cell(nInjection,1);
for ii = 1:nInjection
  jitteredParam = paramArray{ii};
  jitteredParam(alphaIndex(ii)) = randalpha(ii);  %-- reset decay time
  jitteredParamStr = num2str(jitteredParam,'%.3e~');
  paramData{1}{ii} = jitteredParamStr(1:end-1);  %-- drop last '~'
end

%----- Write jittered injection log file.
fid = fopen(outFile,'w');
for ii = 1:nInjection
  %----- Write signal type and parameters to output file.
  fprintf(fid,'%d %d %e %e %e ',numData{1}(ii,:));
  fprintf(fid,'%s ',nameData{1}{ii});
  fprintf(fid,'%s ',paramData{1}{ii});
  fprintf(fid,'\n');
end
fclose(fid);

% ---- Done.
return
