function outCluster = xoffsourceunion(dirName,fileListString,outFile)
% XOFFSOURCEUNION - finds triggers associated to long background events and unions them up into one
%
% usage:
%
%   xinjunion(dirName,fileList)
%
%   dirName    String. Directory containing all files to be read.
%   fileList   String. List of results files to be read. 
%   outFile    Optional string. File to output merged clusters to. If not
%              specified, original input file is over-written. 
%
% $Id$

% ---- Checks.
narginchk(2, 3);

% ---- Parse file list string into cell array of separate file names.
fileList = strread(fileListString,'%s');

% ---- Loop over all files to be merged.
for iFile = 1:length(fileList)
    fileName = [dirName '/' fileList{iFile}];
    s = load(fileName);

    % ---- find fake job numbers for each of the slides
    fjn = unique(s.cluster.fakeJobNumber);
    for m=1:length(fjn)

        % ---- Keep all elements with flag injectionProcessedMask == 1.
        index = find(s.cluster.fakeJobNumber==fjn(m));
        % ---- find nearby triggers in time and freq
        %bb = s.clusterInj(index).boundingBox;
        bb = s.cluster.boundingBox(index,:);
        Ntrig = size(bb,1);
        olp = zeros(Ntrig,Ntrig);
        indI = [];
        indJ = [];
        win = [0 0 5 8]; %-- coincidence window
        for i=1:Ntrig-1
            for j=(i+1):Ntrig
                olp(i,j) = rectint(bb(i,:)+win,bb(j,:)+win);
                if olp(i,j) > 0
                    indI(end+1) = i;
                    indJ(end+1) = j;		
                end
            end
        end
        % ---- Combine pairs into two-column matrix.
        pairs = [indI' indJ'];
        %check pairs
        if isempty(pairs)
            disp('pairs are empty, we dont have overlapping boxes!');
        else
            disp('pairs are not empty!');
            disp(pairs);
        end    
        % ------------------------------------------------------------------------------

        % ---- Only proceed if we have triggers that need to be merged.
        if ~isempty(pairs)
        
            % ---- Split "pairs" matrix into two columns for convenience.
            pairsI = pairs(:,1);
            pairsJ = pairs(:,2);
            % ---- Complete list of triggers to be checked for membership of a generalised cluster.
            trigToCheck = 1:Ntrig;
            % ---- Storage for list of triggers in each generalised cluster.
            genClust = cell(Ntrig,1);
            % ---- Counter over generalised clusters.
            igenClust = 0;

            % ---- Loop over all Ntrig triggers, starting from the last one.
            while ~isempty(trigToCheck)    
                % ---- Start new generalsied cluster.
                igenClust = igenClust + 1;
                % ---- Select next trigger to check, from the end of the list.
                iTrig = trigToCheck(end);
                k = find(pairsI==iTrig | pairsJ==iTrig);
                % ---- Add all pairs contaiing this trigger to our current generalsied
                %      cluster, or just this one trigger if it is not piared with anyone else.
                if ~isempty(k)
                    tmp = pairs(k,:);
                    genClust{igenClust} = union(genClust{igenClust},tmp(:)');
                    % ---- Delete all pairs containing this trigger from our working list.
                    pairs(k,:) = [];
                    pairsI(k,:) = [];
                    pairsJ(k,:) = [];
                else
                    genClust{igenClust} = iTrig;
                end
                % ---- We're finished with this trigger. Delete it from the list of
                %      to-be-checked and add it to the list of triggers we've checked. 
                trigToCheck = setdiff(trigToCheck,iTrig); 
                checkedTrigs = iTrig;
                % ---- By adding pairs to our generalsied cluser, we may have introduced new
                %      triggers. We now have to check each of the new triggers for any
                %      additional triggers they're clustered with that also have to be added ... 
                newtrigs = setdiff(genClust{igenClust},checkedTrigs);
                while ~isempty(newtrigs)
                    for ii = 1:length(newtrigs)
                        k = find(pairsI==newtrigs(ii) | pairsJ==newtrigs(ii));
                        if ~isempty(k)
                            tmp = pairs(k,:);
                            genClust{igenClust} = union(genClust{igenClust},tmp(:)');
                            pairs(k,:) = [];
                            pairsI(k,:) = [];
                            pairsJ(k,:) = [];
                        end
                    end
                    checkedTrigs = union(checkedTrigs,newtrigs);
                    trigToCheck = setdiff(trigToCheck,checkedTrigs); % -- finished with this trigger
                    % --- Are there more new triggers?  If so, we go through the while loop
                    %     again.  If not, we go on to the next generalised cluster.
                    newtrigs = setdiff(genClust{igenClust},checkedTrigs);
                end        
            end

            % ---- Delete empty genClusts.
            genClust = genClust(1:igenClust);

            % ------------------------------------------------------------------------------

            % ---- Merge triggers to be clustered together.
            for igenClust=1:length(genClust)

                % ---- Triggers to be merged for this generalised cluster.
                trigIdx = genClust{igenClust};

                % ---- chop the initial trigger list and reshape the boundingBox
                newTriggers = xclustersubset(s.cluster,index(trigIdx));
                newTriggers.nPixels(1) = sum(newTriggers.nPixels);
                if length(trigIdx)>1
                    newTriggers.likelihood(1,:) = sum(newTriggers.likelihood);
                else
                    newTriggers.likelihood(1,:) = newTriggers.likelihood;
                end
                newTriggers.significance(1) = sum(newTriggers.significance); 
                % disp(newTriggers.significance);
                mint = min(newTriggers.boundingBox(:,1));
                maxt = max(newTriggers.boundingBox(:,1)+newTriggers.boundingBox(:,3));
                minf = min(newTriggers.boundingBox(:,2));
                maxf = max(newTriggers.boundingBox(:,2)+newTriggers.boundingBox(:,4));
                duration = maxt - mint;
                bw = maxf - minf;
                newTriggers.boundingBox(1,:) = [mint minf duration bw];
                newTriggers.peakTime(1,:) = mint + 0.5*duration;
                newTriggers.peakFrequency(1,:) = minf + 0.5*bw;
                % ---- Keep only first (merged) trigger.
                newTriggers = xclustersubset(newTriggers,1);
    
                if igenClust==1
                    clusterInj = newTriggers;
                else
                    clusterInj = xclustermerge(clusterInj,newTriggers);
                end
    
            end

        else 

            % ---- No merging needed for this fakeJobNumber. Use unedited triggers.
            clusterInj = xclustersubset(s.cluster,index);
         
        end 

        % ---- Save merged triggers for this fakeJobNumber into output array.
        if m==1
            outCluster = clusterInj;
        else
            outCluster = xclustermerge(outCluster,clusterInj);
        end

    end % ---- for loop on fakeJobNumber (fjn)

    % ---- Re-order field names to match original file.
    outCluster = orderfields(outCluster,s.cluster);
    % ---- Now save clusters back to original file.
    s.cluster = outCluster;

    if nargin<3
        outFile = fileName;
    end
    save(outFile, '-struct', 's');
    
end

% ---- Done.
return
