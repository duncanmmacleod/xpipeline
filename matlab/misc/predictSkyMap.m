function [Enull,Einc,Etot] = predictSkyMap(sourcePositions,sourcePolarizations, ...
    sourceSNR2,channels,numberOfFreqBins,skyPositions,autocorr,autocorrTime,baselines);
% predictSkyMap: Predict sky maps of selected likelihoods for a specific
% model GWB from a specified direction.
%
%   [Enull,Einc,Etot] = predictSkyMap(sourcePositions,sourcePolarizations, ...
%                    sourceSNR2,channels,numberOfFreqBins,skyPositions);
%
%   sourcePositions      Array of trial sky positions of the source.
%                        Format matches skyPositions, though only the first
%                        two columns are used.
%   sourcePolarizations  Vector of trial polarization values of the GWB.
%   sourceSNR2           Vector of SNR2 values of GWB in each detector, for
%                        optimal orientation.  (This is how one accounts
%                        for different detector sensitivities.)
%   channels             Cell array of strings.  Names of detectors in the 
%                        network.  See xpipeline.
%   numberOfFreqBins     Scalar.  Number of time-frequency pixels summed 
%                        over to make each sky map value.  (Needed to
%                        account for noise statistics.)
%   skyPositions         Array of sky positions at which to predict
%                        likelihoods.  See xpipeline. 
%	autocorr             Array of autocorrelation functions for each detector.
%   autocorrTime         Vector of times corresponding to the the
%                        autocorrelations.
%   baselines            Vector of baseline indices to include in the null energy
%
%   Enull                Array of predicted null energy values for each 
%                        sky position x source position x source polarization.
%   Einc                 Array of predicted incoherent energy values for each 
%                        sky position x source position x source polarization.
%   Etot                 Array of predicted total energy values for each 
%                        sky position x source position x source polarization.
%
% Einc and Etot are incoherent measures; i.e., they involve only data
% auto-correlation terms.  As a result they vary smoothly over the sky and
% can be predicted without detailed knowledge of the GWB waveform.  Enull
% includes cross-correlation temrs, and so depends on the detailed
% waveforms.  predictSkyMap assumes the GWB to be linearly polarized with
% a single narrow peak. This "impulse signal" assumption means that there
% will be only a single ring per baseline. Cross-correlation terms vanish
% except along lines of the correct time delay for each detector pair
% baseline. Enull is only different from Einc along the rings of correct
% constant time delay with respect to the baselines. predictSkyMap 
% estimates Enull by computing pairwise terms in the null energy along
% these rings assuming a delta-like impulse. On each ring Enull differs
% from Einc by the overlap \rho^2_ab of the signal in the two corresponding
% detectors a,b, ignoring the other detectors. The ring width is estimated
% assuming a hardwired fixed pulse duration.
%
% first version: Patrick J. Sutton 2006.03.27
% 
% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Checks.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 9
	baselines = [];
end

if (size(sourcePositions,2)<2)
    error('Array of test source positions must have at least two columns.')
end
if (size(skyPositions,2)<2)
    error('Array of output sky positions must have at least two columns.')
end
if (length(sourceSNR2)~=length(channels))
    error('Vector of signal SNRs must be of same length as channel array.')
end
% ---- Make sure sourceSNR2 is a row vector.
sourceSNR2 = (sourceSNR2(:)).';
if (numberOfFreqBins<=0)
    error('Degrees of freedom must be positive.')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Network information.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Speed of light (m/s).
speedOfLight = 299792458;

% ---- load detector data
numberOfDetectors = length(channels);
for jChannel = 1:numberOfDetectors
    detector{jChannel} = LoadDetectorData(channels{jChannel});
end

% ---- detector baseline vectors
base = [];
% ---- detector baseline unit vectors
bhat = [];
for iChannel = 1:numberOfDetectors-1
    for jChannel = iChannel+1:numberOfDetectors
        b = detector{iChannel}.V-detector{jChannel}.V;
        base = [base, b];
        if (norm(b)>1)
            bhat = [bhat, b/(b'*b)^0.5];
        else
            bhat = [bhat, zeros(size(b))];
        end
    end
end

if isempty(baselines)
	baselines = [1:size(bhat,2)];
end

% ---- Noise amplitudes of the detectors relative to the first detector.
sigma = (sourceSNR2/sourceSNR2(1)).^(-0.5);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Quantities related to output sky positions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Row pointing vector to each sky position in output maps.
skyOmega = CartesianPointingVector(skyPositions(:,2),skyPositions(:,1));
numberOfSkyPositions = size(skyPositions,1);

% ---- Antenna responses for these sky positions.
skyFp = zeros(numberOfSkyPositions,numberOfDetectors);
skyFc = zeros(numberOfSkyPositions,numberOfDetectors);
for iChannel = 1:numberOfDetectors
    [skyFp(:,iChannel), skyFc(:,iChannel)] = ComputeAntennaResponse( ...
        skyPositions(:,2), skyPositions(:,1), ...
        zeros(numberOfSkyPositions,1), detector{iChannel}.d);
end
% ---- Rescale antenna responses for each detector by relative noise level.
skyFp = skyFp * diag(sigma.^(-1));
skyFc = skyFc * diag(sigma.^(-1));

% ---- Convert antenna responses to DP frame.
[skyFpDP skyFcDP psiDP] = convertToDominantPolarizationFrame(skyFp,skyFc);

% ---- Unit vectors in F+, Fx directions in DP frame.
skyFpDPhat = skyFpDP ./ repmat(sum(skyFpDP.^2,2).^0.5,[1,numberOfDetectors]);
skyFcDPhat = skyFcDP ./ repmat(sum(skyFcDP.^2,2).^0.5,[1,numberOfDetectors]);

% ---- Projection operator used to compute null and incoherent energies for
%      each sky position: 
%          Enull(f) = \sum_{a,b} d_a^*(f) Q_ab(f) d_b(f)
%      where in the dominant polarization frame
%          Q = I - \hat{F+}\hat{F+}^T - \hat{Fx}\hat{Fx}^T 
%      For simplicity, we assume the detector noise spectra all have the
%      same shape, in which case \hat{F+}, \hat{Fx}, and Q_ab are all 
%      independent of f.
%
%      Compute Q matrix.  Store diagonal and below-diagonal ("cross") terms
%      separately.
% ---- Diagonal (incoherent) part.  Qdiagonal = [Q11, Q22, ...] 
Qdiagonal = 1 - skyFpDPhat.^2 - skyFcDPhat.^2;
% ---- Indices of off-diagonal terms. careful! must match order of bhat!
crosstermIndices = find(tril(ones(length(channels)),-1));
[Ir,Ic] =  find(tril(ones(length(channels)),-1));
% ---- Off-diagonal (coherent or cross-correlation) part.  
%      Qcrossterms = [Q21, Q31, ..., QD1, Q32, ... , Q{D,D-1}] 
Qcrossterms = zeros(numberOfSkyPositions,numberOfDetectors*(numberOfDetectors-1)/2);
for index = 1:length(crosstermIndices)
    Qcrossterms(:,index) = ...
        - skyFpDPhat(:,Ir(index)).*skyFpDPhat(:,Ic(index)) ...
        - skyFcDPhat(:,Ir(index)).*skyFcDPhat(:,Ic(index));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Quantities related to trial source positions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Row pointing vector to each trial source position.
sourceOmega = CartesianPointingVector(sourcePositions(:,2),sourcePositions(:,1));
numberOfSourcePositions = size(sourcePositions,1);
numberOfSourcePolarizations = length(sourcePolarizations);

% ---- injection angles and responses for trial source positions
sourceFp = zeros(numberOfSourcePositions,numberOfDetectors);
sourceFc = zeros(numberOfSourcePositions,numberOfDetectors);
for iChannel = 1:numberOfDetectors
    [sourceFp(:,iChannel), sourceFc(:,iChannel)] = ComputeAntennaResponse( ...
        sourcePositions(:,2), sourcePositions(:,1), ...
        zeros(numberOfSourcePositions,1), detector{iChannel}.d);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Compute likelihood maps.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Storage for output.
Enull = zeros(numberOfSkyPositions,numberOfSourcePositions,numberOfSourcePolarizations);
Einc = zeros(numberOfSkyPositions,numberOfSourcePositions,numberOfSourcePolarizations);
Etot = zeros(numberOfSkyPositions,numberOfSourcePositions,numberOfSourcePolarizations);

% ---- loop over source polarizations
for jPolarization = 1:numberOfSourcePolarizations

    % ---- Polarization angle of source relative to Earth frame.
    psi_s = sourcePolarizations(jPolarization);

    % ---- loop over source positions
    for jPosition = 1:numberOfSourcePositions

        % ---- Antenna responses for this source position and polarization.
        sourceFpEff = (cos(2*psi_s)*sourceFp(jPosition,:) ...
            + sin(2*psi_s)*sourceFc(jPosition,:)) ;
        
        % ---- Received signal amplitude SNR (signed) in each detector.
        receivedSNR = sourceFpEff .* sourceSNR2.^0.5 ;
        
        % ---- Received SNR^2 in each detector.
        receivedSNR2 = sourceFpEff.^2 .* sourceSNR2 ;

        % ---- Total energy (incoherent statistic).
        Etot(:,jPosition,jPolarization) = 1/2 * sum(receivedSNR2) ;

        % ---- Incoherent energy (incoherent statistic).
        Einc(:,jPosition,jPolarization) = 1/2 * Qdiagonal * transpose(receivedSNR2) ;

        % ---- Null energy, diagonal (autocorrelation) part.
        Enull(:,jPosition,jPolarization) = Einc(:,jPosition,jPolarization);
        
        % ---- Add in off-diagonal (cross-correlation) part, but only for
        %      sky locations and baselines where time delay is correct. 
        %      Remember the factor 2 from symmetry: ab and ba elements,
        %      which cancels the factor 1/2 from using only f>0 bins.
        for nBaseline = 1:length(baselines)
			iBaseline = baselines(nBaseline);
			skyTimeDelays = (skyOmega*base(:,iBaseline) ...
								- sourceOmega(jPosition,:)*base(:,iBaseline)) / speedOfLight;
			% ---- interpolate the autocorrelation at time delays for all sky positions
			R = interp1(autocorrTime,autocorr(:,iBaseline),skyTimeDelays);
			
			% ---- add the null energy terms
			Enull(:,jPosition,jPolarization) = Enull(:,jPosition,jPolarization) ...
				+ R .* Qcrossterms(:,iBaseline) ...
				* receivedSNR(Ir(iBaseline)) * receivedSNR(Ic(iBaseline));
        end

        % ---- Make sure no null energies are < 0.  (Seen sometimes near edges of
        %      ring intersections.)
        k = find(Enull(:,jPosition,jPolarization)<0);
        Enull(k,jPosition,jPolarization) = 0;
        
    end  % -- loop over source positions

end  % -- loop over source polarizations

% ---- Add mean noise contributions to each likelihood.
Etot = Etot + numberOfDetectors * numberOfFreqBins;
Enull = Enull + (numberOfDetectors - 2) * numberOfFreqBins;
Einc = Einc + (numberOfDetectors - 2) * numberOfFreqBins;

% ---- Done.
return;
