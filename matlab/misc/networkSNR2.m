function [sumRho2 sumRho2pDP sumRho2cDP epsilon] = networkSNR2(phi,theta,psi,rho2p,rho2c,detectors,sigma)
%
% Computed the sum-squared SNR of a signal in a specified detector network,
% assuming quasi-identical detectors.
%
%   [sumRho2 sumRho2pDP sumRho2cDP epsilon] = ... 
%       networkSNR2(phi,theta,psi,rho2p,rho2c,detectors,sigma)
%  
%   phi         Column vector of azimuthal sky positions, in Earth-based 
%               coordinates (radians).
%   theta       Column vector of polar sky positions, in Earth-based 
%               coordinates (radians).
%   psi         Column vector of polarization angles, in Earth-based 
%               coordinates (radians).
%   rho2p       Vector of squared SNRs of the plus polarization of each
%               injection in the first detector, for optimal position and
%               orientation.
%   rho2c       Vector of squared SNRs of the cross polarization of each
%               injection in the first detector, for optimal position and
%               orientation.
%   detectors   Row cell array of detector names.
%   sigma       Vector of relative noise amplitudes in each detector in the
%               network.  (Used to determine dominant polarization frame.
%               Note that only relative values are used; i.e., sigma is
%               rescaled so that sigma(1)=1.)
%
%   sumRho2     Column vector of sum-squared SNRs for each sky position.
%   sumRho2pDP  Column vector of sum-squared SNRs for the "plus" 
%               polarization in the DP frame for each sky position.
%   sumRho2cDP  Column vector of sum-squared SNRs for the "cross" 
%               polarization in the DP frame for each sky position.
%   epsilon     Ratio sum(Fc^2) / sum(Fp^2) in the DP frame for each sky
%               position.
%
% See ComputeAntennaResponse for a definition of Earth-based coordinates.
%
% networkSNR2 computes the sum-squared SNR of a signal in a detector
% network, using these simplifying assumptions:
%   (1) The detectors have identical noise spectral shapes; i.e., they
%   differ only by a frequency-independent scale factor.
%   (2) The GWB plus and cross waveforms are orthogonal in the source
%   polarization frame.
% The first output is the squared SNR summed over the network.  The second
% and third outputs are the contributions to the squared SNR due to the
% plus and cross polarizations in the dominant polarization frame (NOT the
% source frame); these are useful for estimating constraint likelihoods. 
%
% WARNINGS / LIMITATIONS OF THIS ALGORITHM:
%
% 1. This function assumes that the plus and cross waveforms are
% orthogonal; i.e., it ignores h_+ h_x cross terms in the SNR.
%
% 2. For computing DP-frame quantities this function assumes that the DP
% frame is the same for all frequencies.  This is true iff the noise
% spectra of the detectors are the same shape across the frequency band of 
% the signal.
%
% Initial write: Patrick J. Sutton 2006.07.11
%
% $Id$

% ---- Preliminaries.
numberOfDetectors = size(detectors,2);
numberOfSignals = size(phi,1);

% ---- Rescale sigma so that sigma(1)=1.
sigma = sigma/sigma(1);

% ---- Compute antenna responses in source frame. 
Fp = zeros(numberOfSignals,numberOfDetectors);
Fc = zeros(numberOfSignals,numberOfDetectors);
for jDetector = 1:numberOfDetectors
    [Fp(:,jDetector) Fc(:,jDetector)] = ComputeAntennaResponse( ...
        phi,theta,psi,detectors{jDetector});
end
% ---- Rescale by relative noise levels in the detectors.  Treat as though
%      spectra have the same shape.
Fp = Fp * diag(sigma.^(-1));
Fc = Fc * diag(sigma.^(-1));

% ---- Compute sum-squared SNR.
sumRho2 = sum(Fp.^2,2) .* rho2p + sum(Fc.^2,2) .* rho2c ;

% ---- Compute extra output arguments if requested.
if (nargout > 1)

    % ---- Convert to dominant polarization frame, if non-standard likelihood
    %      is requested.
    [FpDP FcDP psiDP] = convertToDominantPolarizationFrame(Fp,Fc);

    % ---- Epsilon factor.
    epsilon = sum(FcDP.^2,2)./sum(FpDP.^2,2); 

    % ---- Compute plus and cross contributions to the
    %      SNR2/energy/likelihood in the DP frame.
    sumRho2pDP = sum(FpDP.^2,2) .* ( cos(2*psiDP).^2 .* rho2p + sin(2*psiDP).^2 .* rho2c ) ;
    sumRho2cDP = sum(FcDP.^2,2) .* ( sin(2*psiDP).^2 .* rho2p + cos(2*psiDP).^2 .* rho2c ) ;

end

% ---- Done.
return

