% Script to perform simple estimation of time-frequency
% properties of a signal detected in network data.
%
% Procedure: We FFT the data at a single resolution, extract a fixed fraction 
% of the loudest pixels, cluster the surviving pixels centered on the loudest 
% pixel, and compute the cluster properties.
%
% $Id$


% --------------------------------------------------------------------------
%    User-specified parameters.
% --------------------------------------------------------------------------

% ---- Analysis parameters: these will need to be supplied as input to the PE
%      function.
T = 1;        %-- timeseries duration [s]
fs = 1024;    %-- sample rate [Hz]
Tfft = 1/16;  %-- FFT duration
Ndet = 3;     %-- number of detectors
bpp = 0.05;   %-- fraction of loudest pixels in the map to keep for PE
              %   ("black pixel probability")
searchRadius = 2;  %-- radius in pixels to search for neighbors
verbose = false;   %-- verbosity flag

% ---- Signal parameters: these are for making up some test data. The PE 
%      function will not be aware of these.
h0 = 3;    %-- amplitude 
T0 = 0.7;  %-- central time [s]
tau = 0.1; %-- duration [s]
f0 = 50;   %-- central frequency [Hz]


% --------------------------------------------------------------------------
%    Derived parameters.
% --------------------------------------------------------------------------

% ---- Derived parameters.
N = T*fs;          %-- timeseries length
Nfft = Tfft*fs;    %-- FFT length


% --------------------------------------------------------------------------
%    Prepare sample data to be analysed.
% --------------------------------------------------------------------------

% ---- Example input data: whitened with a sine-Gaussian signal added.
% ---- Time stamps.
t = [0:(N-1)]'/fs;
% ---- Whitened noise in Ndet detectors.
noise = randn(N,Ndet);
% ---- Signal to be added to the three detectors.
h = h0*exp(-(t-T0).^2/(2*tau^2)).*sin(2*pi*f0*t);
% ---- Apply a time shift and rescaling to h for each detector as a rough 
%      approximation of the effect of the sky position.
signal = zeros(N,Ndet);
signal(:,1) = h;
signal(:,2) = 0.9*circshift(h,round(0.010*fs));
signal(:,3) = -0.4*circshift(h,round(-0.015*fs));
% ---- Data to be processed.
data = signal + noise;


% --------------------------------------------------------------------------
%    Create the time-frequency map of the data.
% --------------------------------------------------------------------------

% ---- THE PE FUNCTION WILL START HERE.

% ---- We will FFT using a window with 50% overlap. The FFT duration chosen 
%      above should be large compared to the light travel time between detectors.
%      Then we can ignore time delays and simply add the TF maps from all 
%      detectors together.

% ---- FFT Window: We will NOT use one of the standard window functions.
%      Instead we will use 
%          window = sin(n*pi/Nfft)
%      (i.e., the square-root of the hann window). This unusual choice has
%      the property that sum(wx^2) = sum(x^2) where x is the original data
%      and wx is the windowed overlapping segments. This choice should 
%      therefore keep the total energy or squared SNR in the data the same
%      before and after windowing.
wind = sin([1:Nfft]*pi/Nfft)';

% ---- Number of columns in the segmented overlapping data.
Ncol = 2*N/Nfft-1;
% ---- Prepare temporary storage for the segmented overlapping data (we work 
%      with one detector at a time).
sdata = zeros(Nfft,Ncol);
% ---- Number of non-negative frequency bins ([0,...,Nyquist]).
Nrow = Nfft/2+1;
% ---- Prepare storage for the time-frequency map.
map   = zeros(Nrow,Ncol);
% ---- Create the time-frequency map by looping over detectors and adding the
%      map for each.
for iDet=1:Ndet
    % ---- Segment and window the data.
    for icol=1:Ncol
        shiftIdx = (icol-1)*Nfft/2;
        sdata(:,icol) = data(shiftIdx+1:shiftIdx+Nfft,iDet);
        sdata(:,icol) = sdata(:,icol) .* wind;
    end
    if verbose
        % ---- Sanity check: have we preserved power? Note that the first and last
        %      half bins don't have an overlapping segment so don't include those
        %      in power comparison.
        powerIn = sum(data(Nfft/2+1:end-Nfft/2,iDet).^2);
        powerOut = sdata(:);
        powerOut = sum(powerOut(Nfft/2+1:end-Nfft/2).^2);
        disp(['Power conservation test for detector ' num2str(iDet) ':'])
        disp(['  Power in  = ' num2str(powerIn)])
        disp(['  Power out = ' num2str(powerOut)])
    end
    % ---- FFT and retain non-negative frequencies only ([0,...,Nyquist]).
    sdata = fft(sdata);
    map = map + abs(sdata(1:Nfft/2+1,:)).^2;
end


% --------------------------------------------------------------------------
%    Threshold and cluster the time-frequency map.
% --------------------------------------------------------------------------

% ---- Number of pixels retained.
Nbp = ceil(bpp*numel(map));
% ---- Find the map threshold corresponding to the Nbp'th highest value.
sortedmap = sort(map(:),'descend'); %-- (:) reshapes into a vector in descending order
mapMedian = median(sortedmap);
threshold = sortedmap(Nbp);

% ---- Zero out unwanted pixels.
map(map<threshold) = 0;

% ---- I will define my signal as the loudest pixel, all other pixels that are 
%      above threshold and share an edge or corner with the loudest pixel, and 
%      all other pixels that share an edge or corner with another pixel in the
%      event, working out recursively until there are no remaining pixels above
%      threshold bordering the event. Matlab has the bwlabel function for this;
%      I will avoid using it or any other special matlab commands and just rely 
%      on simple commands.

% ---- Find maximum value of map.
maxMap = 0;
for icol=1:Ncol
    for irow=1:Nrow
        if map(irow,icol) > maxMap
            maxMap = map(irow,icol);
            maxRow = irow;
            maxCol = icol;
        end
    end
end

% ---- Initialise mask marking pixels that are part of our event cluster.
mask = zeros(size(map));
% ---- Add loudest pixel.
mask(maxRow,maxCol) = 1;

% ---- Search outward from loudest to find other loud neighboring pixels.
%      otherPixels* list the pixels to be checked for neighbors. Add to this
%      as we find new neighbors and keep searching until we have checked all
%      of them.
otherPixelsRow = [maxRow];
otherPixelsCol = [maxCol];
while ~isempty(otherPixelsRow)
    % ---- Pixel we are searching around.
    irow = otherPixelsRow(1);
    icol = otherPixelsCol(1);
    for ii=-searchRadius:searchRadius
        for jj=-searchRadius:searchRadius
            if ii~=0 & jj~=0
                % ---- Skip 0,0 - this is the pixel we are searching around.
                ;
	    elseif (irow+ii<1 | irow+ii>Nrow | icol+jj<1 | icol+jj>Ncol)
                % ---- Target "pixel" is off the edge of the map - skip.
                ;
            elseif map(irow+ii,icol+jj)>threshold & mask(irow+ii,icol+jj)==0
                % ---- Add this pixel to the mask and the list of pixels to be checked.
                mask(irow+ii,icol+jj) = 1;
                otherPixelsRow = [otherPixelsRow, irow+ii];
                otherPixelsCol = [otherPixelsCol, icol+jj];
            end
        end
    end
    % ---- Remove this pixel from the list to be checked.
    otherPixelsRow(1) = [];
    otherPixelsCol(1) = [];
end


% --------------------------------------------------------------------------
%    Compute properties of the pixel cluster.
% --------------------------------------------------------------------------

% ---- Initialise values of cluster properties.
% ---- Number of pixels.
nPixels = 0;
% ---- Start time.
startTime = Ncol+1; %-- [bin] dummy value larger than any in map
% ---- End time.
endTime = 0;        %-- [bin] dummy value smaller than any in map
% ---- Weighted peak time.
peakTime = 0;
% ---- Minimum frequency.
startFreq = Nrow+1; %-- [bin] dummy value larger than any in map
% ---- Maximum frequency.
endFreq = 0;        %-- [bin] dummy value smaller than any in map
% ---- Weighted peak frequency.
peakFreq = 0;
% ---- SNR.
snrSquared = 0;

% ---- Loop over all pixels and add to the output properties.
for irow=1:Nrow
    for icol=1:Ncol
        if mask(irow,icol)==1
            nPixels = nPixels + 1;
            snrSquared = snrSquared + map(irow,icol);
            if icol < startTime
                startTime = icol;
            end
            if icol > endTime
                endTime = icol;
            end
            peakTime = peakTime + icol*map(irow,icol);
            if irow < startFreq
                startFreq = irow;
            end
            if irow > endFreq
                endFreq = irow;
            end
            peakFreq = peakFreq + irow*map(irow,icol);
        end
    end
end
% ---- Normalise peakTime, peakFreq.
peakTime = peakTime / snrSquared;
peakFreq = peakFreq / snrSquared;
% ---- Convert all times from bins to seconds.
T0 = Tfft/2; %-- central time of first bin
dT = Tfft/2; %-- hard-coded for 50% overlap
startTime = T0 + (startTime-1)*dT - dT; %-- start of bin (bin width = 2*dT)
endTime   = T0 + (  endTime-1)*dT + dT; %-- end of bin (bin width = 2*dT)
peakTime  = T0 + ( peakTime-1)*dT;
% ---- Convert all frequencues from bins to Hz.
F0 = 0     ; %-- central freq of first bin
dF = 1/Tfft; %-- spacing of frequency bins
startFreq = F0 + (startFreq-1)*dF - 0.5*dF; %-- start of bin
endFreq   = F0 + (  endFreq-1)*dF + 0.5*dF; %-- end of bin
peakFreq  = F0 + ( peakFreq-1)*dF;
% ---- Duration.
duration = endTime - startTime;
% ---- Bandwidth.
bandwidth = endFreq - startFreq;
% ---- Estimate SNR.
SNR = (max(snrSquared-nPixels*mapMedian,0))^0.5;


