function [mdcData]=readGWF(mdcChannelFileName,eventFileName,frameCacheFile,injectionNumber)

verboseFlag=1;
blockTime=256;
numberOfChannels=2;
jobNumber=0;

%----- Read channel data into cell array.
channelLines = dataread('file',mdcChannelFileName,'%s','delimiter','\n');

%----- Number of channels.
nChLines = length(channelLines);

%----- Initialize variables.
mdcFrameTypes = {};
mdcChannelNames = {};
mdcChannelVirtualNames = mdcChannelNames;

%----- Loop over channels.
for iChLine = 1:nChLines,

    %----- Split on whitespace into separate elements of a cell array.
    lineContents = dataread('string',channelLines{iChLine},'%s');

    %----- Number of channel parameters.
    nChParams = length(lineContents);

    %----- Parse lineContents.  Format:
    %      2 params:
    %         channelName frameType
    %      3 params:
    %         channelName frameType channelVirtualName
    %      4 params:
    %         channelName frameType glitchChannelName glitchFrameType
    %      5 params:
    %         channelName frameType glitchChannelName glitchFrameType
    %         channelVirtualName
    mdcChannelNames{iChLine} = lineContents{1};
    mdcChannelVirtualNames{iChLine} = mdcChannelNames{iChLine};
    mdcFrameTypes{iChLine} = lineContents{2};
    if (nChParams == 3)
        mdcChannelVirtualNames{iChLine} = lineContents{3};
    end;

end;

%----- Check and read eventFileName.  Extract the single event
%      specified by the xdetection jobNumber argument.
if (~isequal(eventFileName,''))

    %----- Read each line (for one event) into a cell array.
    eventLines = dataread('file',eventFileName,'%s','delimiter','\n');

    %----- Number of events.
    nEvLines = length(eventLines);

    %----- Check that the requested event is available.
    if (jobNumber +1> nEvLines),
        error(['Event file does not contain number of events implied ' ...
            'by job number.  Pick a smaller job number!']);
    end;

    %----- Extract data for specified event.
    eventData = str2num(eventLines{jobNumber+1});

    %----- Extract center time for specified event.
    centerTime = eventData(1);

    %----- Extract time offsets for specified event.
    if (length(eventData)>1),
        timeOffsets = eventData(2:end);
    else
        timeOffsets = zeros(1,nChLines);
    end;

end;

%----- Start and stop time for this event.
startTime = centerTime - blockTime / 2;
stopTime = centerTime + blockTime / 2;
 

    
disp('reading MDC data ...');
% ---- If injection number is non-zero, then it is to be added
%      as a tag to the end of the channel name.
if (injectionNumber)
    for channelNumber = 1 : numberOfChannels
        mdcChannelNames_tag{channelNumber} = ...
            [mdcChannelNames{channelNumber} '_' ...
            num2str(injectionNumber)];
    end
else
    % ---- Don't modify channel names.
    mdcChannelNames_tag = mdcChannelNames;
end
[mdcData, mdcSampleFrequencies] = xreaddata(mdcChannelNames_tag, ...
    mdcFrameTypes, frameCacheFile, startTime, stopTime, ...
    timeOffsets, verboseFlag);
