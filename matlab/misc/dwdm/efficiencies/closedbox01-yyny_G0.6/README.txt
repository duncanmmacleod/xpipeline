
------------------------------------------------------------------------
 
 Projected Wall Amplitude Limits: 
 
	runName:               closedbox01-yyny_G0.6
	mode:                  training
	applyTOAWeighting:     true
	applyDQVetoes:         true
	applyGravitySpyVetoes: false
	gravitySpyClass.Conf.: 0.95
	applyAmplRatio:        0.6
	amplRatioThreshold:    1
	seedValue:             836465
	FAR threshold:         3.1688e-08 Hz  (1 y^-1)
 
	A50% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	0.08	0.07	0.44	0.19
	1	10	0.07	0.07	0.32	0.22
	10	100	0.07	0.07	0.45	0.18
	100	1000	0.18	0.16	0.78	0.38
	1000	10000	0.96	0.72	4.79	1.78
 
	A90% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	0.41	0.53	2.01	1.19
	1	10	2.35	3.27	0.96	1.86
	10	100	0.36	0.55	2.14	1.17
	100	1000	2.07	1.75	5.33	3.14
	1000	10000	53.56	45.87	104.53	75.61
 
------------------------------------------------------------------------

