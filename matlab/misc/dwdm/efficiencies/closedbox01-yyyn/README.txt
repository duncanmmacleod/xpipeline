
------------------------------------------------------------------------
 
 Projected Wall Amplitude Limits: 
 
	runName:               closedbox01-yyyn
	mode:                  training
	applyTOAWeighting:     true
	applyDQVetoes:         true
	applyGravitySpyVetoes: true
	gravitySpyClass.Conf.: 0.99
	applyAmplRatio:        false
	amplRatioThreshold:    1
	seedValue:             836465
	FAR threshold:         3.1688e-08 Hz  (1 y^-1)
 
	A50% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	0.06	0.06	0.17	0.10
	1	10	0.06	0.05	0.13	0.09
	10	100	0.06	0.06	0.17	0.10
	100	1000	0.12	0.12	0.31	0.19
	1000	10000	0.76	0.54	1.70	0.90
 
	A90% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	0.29	0.27	0.70	0.41
	1	10	0.51	0.48	0.43	0.46
	10	100	0.26	0.26	0.62	0.40
	100	1000	0.63	0.65	1.66	0.96
	1000	10000	26.53	24.57	30.68	28.12
 
------------------------------------------------------------------------


