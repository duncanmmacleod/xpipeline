
------------------------------------------------------------------------
 
 Projected Wall Amplitude Limits: 
 
	runName:               closedbox01-yyyn
	mode:                  training
	applyTOAWeighting:     true
	applyDQVetoes:         true
	applyGravitySpyVetoes: true
	gravitySpyClass.Conf.: 0.99
	applyAmplRatio:        false
	amplRatioThreshold:    1
	seedValue:             836465
	FAR threshold:         3.1688e-08 Hz  (1 y^-1)
 
	A50% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	0.10	0.07	0.46	0.20
	1	10	0.08	0.07	0.37	0.23
	10	100	0.09	0.07	0.48	0.20
	100	1000	0.20	0.16	0.83	0.39
	1000	10000	1.02	0.66	4.61	1.68
 
	A90% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	0.40	0.39	1.65	0.91
	1	10	0.61	0.69	1.09	0.80
	10	100	0.38	0.37	1.62	0.92
	100	1000	0.99	0.89	4.01	2.04
	1000	10000	37.28	33.01	71.37	51.98
 
------------------------------------------------------------------------

