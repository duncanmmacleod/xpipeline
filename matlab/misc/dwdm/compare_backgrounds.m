% $Id$

% ---- Select run and whether or not to save the plots.
% obsRun = 'o1';
savePlots = true;

% ---- Set path for running at CIT.
addpath /home/xpipeline/xpipeline/trunk/matlab/share/
addpath /home/xpipeline/xpipeline/trunk/matlab/searches/grb
addpath /home/xpipeline/xpipeline/trunk/matlab/searches/sn
addpath /home/xpipeline/xpipeline/trunk/matlab/utilities/  
addpath /home/xpipeline/opt/xpipeline/dev/share/fastclusterprop/
addpath /home/edaw/libframe/matlab

% ---- Link analysisTime values to wall number.
analysisTimes = [1   1/2  1/4  1/8  1/16  1/32];
wallNumbers   = [29  100  101  105  1     102];
sampleFrequency = 4096; %-- universal for all runs
analysisLengths = analysisTimes * sampleFrequency;

% ---- Load off-source triggers.
for ii=1:length(analysisTimes)
    % off{ii} = load([obsRun '/' num2str(wallNumbers(ii)) '/output/off_source_0_0_merged.mat']);
    off{ii} = load(['/home/hong.qi/dmdw/' obsRun '/finalof' num2str(wallNumbers(ii)) '/output/off_source_0_0_merged.mat']);
end

% ---- Plot the cumulative distribution of |cc| scores.
figure;
for ii=1:length(analysisTimes)    
    val = sort(off{ii}.cluster.likelihood(:,1));
    N = length(val)
    loglog(val,[N:-1:1]'/N,'linewidth',2)
    hold on
end
grid on
legend('1','1/2','1/4','1/8','1/16','1/32')
set(gca,'fontsize',16)
xlabel('|cc|')
ylabel('cumulative fraction of triggers')
title([obsRun ' - Background'])
if savePlots
    saveas(gcf,[obsRun '_bckgrd_cum_cc_distn.png'],'png')
end

% ---- Sanity check: Plot histogram of likelihood(:,end), which is the
%      intra-analysisLength time shift at which max |cc| occurs.
figure; 
for ii=1:length(analysisTimes)
    histogram(off{ii}.cluster.likelihood(:,end) / analysisLengths(ii))
    hold on
end
legend('1','1/2','1/4','1/8','1/16','1/32')
set(gca,'fontsize',16)
xlabel('cc_{idx}')
ylabel('number of triggers')
title([obsRun ' - Background'])
grid on
if savePlots
    saveas(gcf,[obsRun '_bckgrd_cc_idx_distn.png'],'png')
end


% ---- Sanity check: Plot histogram of likelihood(:,end), which is the
%      intra-analysisLength time shift at which max |cc| occurs.
figure; 
for ii=1:length(analysisTimes)
    subplot(2,3,ii)
    histogram(diff(off{ii}.cluster.likelihood(:,5:6),[],2))
    hold on
    legend(num2str(analysisTimes(ii)),'Location','SouthEast')
    set(gca,'fontsize',14)
    xlabel('t_L - t_H [s]')
    ylabel('number of triggers')
    title([obsRun ' - Background'])
    grid on
end
pos = get(gcf,'position');
set(gcf,'position',[pos(1:2) 2000 1000])
if savePlots
    saveas(gcf,[obsRun '_bckgrd_delta_t_distn.png'],'png')
end

