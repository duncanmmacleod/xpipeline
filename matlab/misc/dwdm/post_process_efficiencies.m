% $Id$

% This script computes the background distribution and efficiencies and saves
% the results to the file post_process_data_<runName>.mat. 


% ------------------------------------------------------------------------------
%    Post-process triggers: backgrounds and efficiencies.
% ------------------------------------------------------------------------------

% ---- Initialise storage for some arrays.
efficiencyAT  = zeros(1100,3);
efficiency    = zeros(1100,NinjScales,3);
efficiencyAvg = zeros(1100,NinjScales);
is50          = zeros(1100,3);
is90          = zeros(1100,3);
is50Avg       = zeros(1100,1);
is90Avg       = zeros(1100,1);
Ttotal        = zeros(1100,1);
sumAPrioriWF  = zeros(1100,3);
NPhysInj = zeros(1100,3);


% ------------------------------------------------------------------------------
%    Main loop over analysis times.
% ------------------------------------------------------------------------------

% ---- Loop over analysis times.
for iat=1:length(analysisTimes)
% disp('KLUDGE: skipping analysis times 1-4!')
% for iat=5:length(analysisTimes)

    % ---- Select analysisTime and wall number containing on- and off-source
    %      data.
    analysisTime = analysisTimes(iat)
    wallNumber = wallNumbers(iat)

    disp(['**********************************************************'])
    disp(['Processing analysisTime ' num2str(analysisTime) ' seconds.'])
    disp(['**********************************************************'])

    % ---- Loop over each observing runs.
    for ior = 1:length(obsRuns)

      %if iat==5 && ior<3
      %  continue;
      %else

        obsRun = obsRuns{ior}
        disp(['----------------------------------------------------------'])
        disp(['  Processing observing run ' obsRun '.'])
        disp(['----------------------------------------------------------'])


        % ------------------------------------------------------------------------------
        %    Retrieve on- and off-source triggers from master array.
        % ------------------------------------------------------------------------------

        on = onsource{iat,ior};
        Non = length(on.cluster.significance); %-- need to check for case Non = 0

        off = offsource{iat,ior};

        
        % ------------------------------------------------------------------------------
        %    Pass/Fail Test: Data-quality vetoes.
        % ------------------------------------------------------------------------------

	% ---- The complicated boolean expression in the next line checks whether DQ 
        %      vetoes were already applied to triggers loaded from a pre-existing file, 
	%      so we can skip this slow step.
        if applyDQVetoes & (~exist('inputparams','var') || (inputparams.applyDQVetoes==false))

            % ---- Read category 2&4 vetoes for each detector.
            disp('Reading data-quality veto files.')
            for ii = 1:length(ifos)
                % ---- The cumbersome construction of the directory name in the next line 
                %      is due to the directory name 'O3a' using a mix of uppercase and lowercase ...
                vetoFileName = [vetoBaseDir 'O' lower(obsRun(2:end)) '/dwdm/' num2str(wallNumber) '/' ifos{ii} '_cat24veto.txt'];
                fid = fopen(vetoFileName);
                tempVetoSegs = textscan(fid, '%f %f %f %f','CommentStyle','#');
                fclose(fid);
                veto(ii).gpsStart = tempVetoSegs{2};
                veto(ii).duration = tempVetoSegs{4};
            end
            clear tempVetoSegs

            % ---- Apply the veto segments to the on-source triggers.
            if Non > 0
                disp('Applying data-quality vetoes to on-source triggers.')
                on.cluster.passDQVetoes = xapplyvetosegments(on.cluster, ...
                    on.cluster.unslidTime,veto,ifoStruct,mode,'quiet');
                on.cluster.passAll = min(on.cluster.passAll,on.cluster.passDQVetoes);
            end

            % ---- Apply the veto segments to the off-source triggers.
            disp('Applying data-quality vetoes to off-source triggers.')
            off.cluster.passDQVetoes = xapplyvetosegments(off.cluster, ...
                off.cluster.unslidTime,veto,ifoStruct,mode,'quiet');
            off.cluster.passAll = min(off.cluster.passAll,off.cluster.passDQVetoes);

        end

        
        % ------------------------------------------------------------------------------
        %    Pass/Fail Test: Gravity Spy glitch coincidence.
        % ------------------------------------------------------------------------------

        if applyGravitySpyVetoes & (~exist('inputparams','var') || (inputparams.applyGravitySpyVetoes==false))

            % ---- For speed, keep only glitches that fall into the range of times
            %      analysed. Add a few minutes padding to each side for safety.
            gpsMin = obsRunGPS(ior,1);
            gpsMax = obsRunGPS(ior,2);
            for ii = 1:length(ifos)
                gravitySpyFile = [gspyBaseDir 'gspy_Epoch-Any_IFO-' ...
                    ifos{ii} '_Pipeline-GravitySpy.csv'];
                glitchTable = readtable(gravitySpyFile);
                disp(['   ... found ' num2str(size(glitchTable,1)) ' glitches for ' ifos{ii} '.'])
                idx = find( ...
                    (glitchTable.confidence >= gravitySpyClassificationConfidence) ...
                    & (glitchTable.GPStime  >= gpsMin) ...
                    & (glitchTable.GPStime  <= gpsMax) ...
                    & (glitchTable.duration >= gravitySpyMinDuration) );
                    % & strcmpi(glitchTable.label,'Blip') ...
                gravitySpy(ii).gpsStart = glitchTable(idx,:).GPStime - 0.5*glitchTable(idx,:).duration;
                gravitySpy(ii).duration = glitchTable(idx,:).duration;
                disp(['   ... retained ' num2str(length(idx)) ' glitches for ' ifos{ii} '.'])
                clear glitchTable idx
            end

            % ---- Apply Gravity Spy to the on-source triggers. The database CSV
            %      files include a peak time and duration but not a start time, so we 
            %      will approximate by looking for peak time +/- duration.
            if Non > 0
                disp('Applying Gravity-Spy vetoes to on-source triggers.')
                on.cluster.passGravitySpyVetoes = xapplyvetosegments(on.cluster, ...
                    on.cluster.unslidTime,gravitySpy,ifoStruct,mode,'quiet');
                on.cluster.passAll = min(on.cluster.passAll,on.cluster.passGravitySpyVetoes);
            end

            % ---- Apply Gravity Spy to the off-source triggers. 
            disp('Applying Gravity-Spy vetoes to off-source triggers.')
            off.cluster.passGravitySpyVetoes = xapplyvetosegments(off.cluster, ...
                off.cluster.unslidTime,gravitySpy,ifoStruct,mode,'quiet');
            off.cluster.passAll = min(off.cluster.passAll,off.cluster.passGravitySpyVetoes);
            
        end


        % ------------------------------------------------------------------------------
        %    Pass/Fail Test: H/L Amplitude Ratio.
        % ------------------------------------------------------------------------------

        if islogical(applyAmplRatio) && (applyAmplRatio & (~exist('inputparams','var') || (inputparams.applyAmplRatio==false)))

            % ---- Apply as a pass/fail test.

            % ---- Apply test to the on-source triggers.
            if Non > 0
                disp('Applying pass/fail amplitude-ratio test to on-source triggers.')
                amplRatio = log10(on.cluster.likelihood(:,3)./on.cluster.likelihood(:,4));
                on.cluster.passAmplRatio = abs(amplRatio) <= amplRatioThreshold;
                on.cluster.passAll = min(on.cluster.passAll,on.cluster.passAmplRatio);
            end

            % ---- Apply test to the off-source triggers.
            disp('Applying pass/fail amplitude-ratio test to off-source triggers.')
            amplRatio = log10(off.cluster.likelihood(:,3)./off.cluster.likelihood(:,4));
            off.cluster.passAmplRatio = abs(amplRatio) <= amplRatioThreshold;
            off.cluster.passAll = min(off.cluster.passAll,off.cluster.passAmplRatio);

        elseif isnumeric(applyAmplRatio) && applyAmplRatio

            error('The current implementation with inputparams.* checks does not check for numeric values of applyAmplRatio.')

            % ---- Apply as an additional weighting: Gaussian in log10(ratio) with standard
            %      deviation equal to the numeric value of applyAmplRatio.

            % ---- Apply to the on-source triggers.
            if Non > 0
                disp('Applying amplitude-ratio weighting to on-source triggers.')
                amplRatio = log10(on.cluster.likelihood(:,3)./on.cluster.likelihood(:,4));
                on.cluster.significance = on.cluster.significance .* exp( -1/2 * (amplRatio).^2 / applyAmplRatio^2);
            end

            % ---- Apply to the off-source triggers.
            disp('Applying amplitude-ratio weighting to off-source triggers.')
            amplRatio = log10(off.cluster.likelihood(:,3)./off.cluster.likelihood(:,4));
            off.cluster.significance = off.cluster.significance .* exp( -1/2 * (amplRatio).^2 / applyAmplRatio^2);
            
        end

        
        % ------------------------------------------------------------------------------
        %    Apply all Pass/Fail tests to on- and off-source triggers.
        % ------------------------------------------------------------------------------

        % ---- Apply all pass/fail tests to the on-source triggers by deleting triggers
        %      that fail any test. 
        if Non > 0
            on.cluster = xclustersubset(on.cluster,find(on.cluster.passAll));
        end
        % ---- Reset trigger count.
        Non = length(on.cluster.significance);
        disp(['Retaining ' num2str(Non) ' on-source triggers after all pass/fail tests.'])

        % ---- Apply all pass/fail tests to the off-source triggers by deleting triggers
        %      that fail any test. 
        off.cluster = xclustersubset(off.cluster,find(off.cluster.passAll));
        % ---- Reset trigger count.
        Noff = length(off.cluster.significance);
        disp(['Retaining ' num2str(Noff) ' off-source triggers after all pass/fail tests.'])


        % ------------------------------------------------------------------------------
        %    Optionally turn off TOA weighting.
        % ------------------------------------------------------------------------------

        % ---- The TOA weighting was computed in post_process_load_data. If we
        %      don't want to use it, over-write it here.
        if ~applyTOAWeighting
            on.cluster.toaWeight  = ones(size(on.cluster.significance));
            off.cluster.toaWeight = ones(size(off.cluster.significance));
        end


        % ------------------------------------------------------------------------------
        %    Compute FAR distribution.
        % ------------------------------------------------------------------------------

        % ---- Rate at which off-source triggers equal or exceed given significance.
        bckgrd{iat,ior}.sig = sort(off.cluster.significance .* off.cluster.toaWeight);
        bckgrd{iat,ior}.far = [Noff:-1:1]' / (NoffJob(iat,ior) * Ton(iat,ior));
                

        % ------------------------------------------------------------------------------
        %   Assign FAR to on-source triggers.
        % ------------------------------------------------------------------------------

        if strcmpi(mode,'training')
            warning(['We are using the training trigger set to assign the FAR to ' ...
                     'on-source triggers. Please update to use the testing trigger set.'])
        end
        if Non > 0
            [on.cluster.far, minFarIdx] = computefar(bckgrd{iat,ior}.sig,bckgrd{iat,ior}.far, ...
                             on.cluster.significance .* on.cluster.toaWeight);
            % ---- Flag any on-source triggers that exceed lowest measurable FAR.
            on.cluster.minFarFlag = zeros(Non,1);
            on.cluster.minFarFlag(minFarIdx) = 1;
        else
            on.cluster.far = [];
            on.cluster.minFarFlag = [];
        end


        % ------------------------------------------------------------------------------
        %   Record final sets of surviving on- and off-source triggers.
        % ------------------------------------------------------------------------------

        % ---- Over-write master array entry for this ior,iat.
        onsource{iat,ior}  = on;
        offsource{iat,ior} = off;

        
        % ------------------------------------------------------------------------------
        %    Process injection triggers.
        % ------------------------------------------------------------------------------

        % ---- Loop over injection sets for this analysisTime
        injSet = injSets{iat};
        for iis = 1:length(injSet)

            disp([num2str(iat) '/' num2str(ior) ': Processing injection set ' num2str(iis) ' of ' num2str(length(injSet)) '.'])

            % ---- If applying TOA weighting then we skip any injection sets for
            %      which all injection delays are unphysical.
            if ~applyTOAWeighting || (applyTOAWeighting && length(validinj{ior}{iat}{iis})>0)             


                % ------------------------------------------------------------------------------
                %  Retrieve injection triggers for this injection set.
                % ------------------------------------------------------------------------------

                injtrig = injtrigAll{injSet(iis),ior};


                % ------------------------------------------------------------------------------
                %    Pass/Fail Test: Data-quality vetoes.
                % ------------------------------------------------------------------------------

                if applyDQVetoes & (~exist('inputparams','var') || (inputparams.applyDQVetoes==false))

                    % ---- Apply the veto segments to the injection triggers.
                    disp('Applying data-quality vetoes to injection triggers.')
                    for ii=1:length(injScales)
                        injtrig{ii}.passDQVetoes = xapplyvetosegments(injtrig{ii}, ...
                            injtrig{ii}.unslidTime,veto,ifoStruct,mode,'quiet');
                        injtrig{ii}.passAll = min(injtrig{ii}.passAll,injtrig{ii}.passDQVetoes);
                    end

                end


                % ------------------------------------------------------------------------------
                %    Pass/Fail Test: Gravity Spy glitch coincidence.
                % ------------------------------------------------------------------------------

                if applyGravitySpyVetoes & (~exist('inputparams','var') || (inputparams.applyGravitySpyVetoes==false))

                    % ---- Apply gravity spy vetoes to the injection triggers.
                    disp('Applying Gravity-Spy vetoes to injection triggers.')
                    for ii=1:length(injScales)
                        injtrig{ii}.passGravitySpyVetoes = xapplyvetosegments(injtrig{ii}, ...
                            injtrig{ii}.unslidTime,gravitySpy,ifoStruct,mode,'quiet');
                        injtrig{ii}.passAll = min(injtrig{ii}.passAll,injtrig{ii}.passGravitySpyVetoes);
                    end

                end


                % ------------------------------------------------------------------------------
                %    Pass/Fail Test: H/L Amplitude Ratio.
                % ------------------------------------------------------------------------------

                if islogical(applyAmplRatio) && (applyAmplRatio & (~exist('inputparams','var') || (inputparams.applyAmplRatio==false)))

                    % ---- Apply as a pass/fail test.
                    disp('Applying pass/fail amplitude-ratio test to injection triggers.')
                    for ii=1:length(injScales)
                        amplRatio = log10(injtrig{ii}.likelihood(:,3)./injtrig{ii}.likelihood(:,4));
                        injtrig{ii}.passAmplRatio = abs(amplRatio) <= amplRatioThreshold;
                        injtrig{ii}.passAll = min(injtrig{ii}.passAll,injtrig{ii}.passAmplRatio);
                    end

                elseif isnumeric(applyAmplRatio) && applyAmplRatio

                    error('The current implementation with inputparams.* checks does not check for numeric values of applyAmplRatio.')
                    % ---- Apply as an additional weighting: Gaussian in log10(ratio) with standard
                    %      deviation equal to the numeric value of applyAmplRatio.
                    disp('Applying amplitude-ratio weighting to injection triggers.')
                    for ii=1:length(injScales)
                        amplRatio = log10(injtrig{ii}.likelihood(:,3)./injtrig{ii}.likelihood(:,4));
                        injtrig{ii}.significance = injtrig{ii}.significance .* exp( -1/2 * (amplRatio).^2 / applyAmplRatio^2);
                    end
                    
                end


                % ------------------------------------------------------------------------------
                %    Apply all Pass/Fail tests.
                % ------------------------------------------------------------------------------

                % ---- Apply the pass/fail tests to the injection triggers by zeroing out abscc,
                %      cc values for triggers that are vetoed. A more precise way to do it is to
                %      apply before selecting the trigger best matching each injection, but this
                %      should be fine ...
                for ii=1:length(injScales)
                    injtrig{ii}.significance    = injtrig{ii}.significance    .* injtrig{ii}.passAll;
                    injtrig{ii}.likelihood(:,1) = injtrig{ii}.likelihood(:,1) .* injtrig{ii}.passAll;
                    injtrig{ii}.likelihood(:,2) = injtrig{ii}.likelihood(:,2) .* injtrig{ii}.passAll;
                end


                % ------------------------------------------------------------------------------
                %    Optionally turn off TOA weighting.
                % ------------------------------------------------------------------------------

                % ---- The TOA weighting was computed in post_process_load_data. If we
                %      don't want to use it, over-write it here.
                for ii=1:length(injScales)
                    if ~applyTOAWeighting
                        injtrig{ii}.toaWeight = ones(size(injtrig{ii}.significance));
                    end
                end


                % ------------------------------------------------------------------------------
                %    Assign FAR to injection triggers.
                % ------------------------------------------------------------------------------

                for ii=1:length(injScales)
                    [injtrig{ii}.far, minFarIdx] = computefar(bckgrd{iat,ior}.sig,bckgrd{iat,ior}.far, ...
                        injtrig{ii}.significance .* injtrig{ii}.toaWeight);
                    % ---- Flag any injection triggers that exceed lowest measurable FAR.
                    injtrig{ii}.minFarFlag = zeros(length(injtrig{ii}.significance),1);
                    injtrig{ii}.minFarFlag(minFarIdx) = 1;
                end


                % ------------------------------------------------------------------------------
                %   Record final set of surviving injection triggers.
                % ------------------------------------------------------------------------------

                % ---- Over-write master array entry for this injection set and run.
                injtrigAll{injSet(iis),ior} = injtrig;


            end %-- if applyTOAWeighting
            
        end %-- injection set
        
        % ---- Save a temporary copy of the data ... this is to handle the common 
        %      problem of matlab being killed for an unknown reason when running 
        %      for multiple hours. 
        if saveTmpFiles
            tmpFileName = ['tmp-' num2str(iat) '-' num2str(ior) '_' outputFileName];
            disp(['Saving temporary file ' tmpFileName ' holding triggers processed to this point ...'])
            save(tmpFileName,'-v7.3'); 
            disp('... finished save call.')
	end

      %end %-- kludge for iat==4 and ior<3
    end %-- obs run
   
end %-- analysis time


% ------------------------------------------------------------------------------
%   Set FAR threshold for open-box analysis.
% ------------------------------------------------------------------------------

% ---- We can do this only after finishing the loop over all data sets (so have all on-source data loaded and processed).
if strcmpi(mode,'testing')
    % ---- Procedure: use min(FAR) over all analysis times and runs. 
    %
    %      A slightly better procedure would be to find min(FAR) over all
    %      Obs Runs for a given analysis time, convert to a p value, then take
    %      min(p) over analysis times. This procedure is better in that it
    %      accounts for the slight (~2%) differences in live times
    %      between analysis times. But it is more complicated in that we need to
    %      assign fap values to the injections in addition to far values, and
    %      the efficiency theshold is then on p=value rather than FAR.
    %
    %      Be careful to check for on-source events    
    %      that are louder than all background (FAR over-estimated).
    onSourceMinFAR = Inf;
    minFarFlag = 0;
    for iat=1:length(analysisTimes)
        for ior = 1:length(obsRuns)
            if ~isempty(onsource{iat,ior}.cluster.far)
                [far,ind] = min(onsource{iat,ior}.cluster.far);
                onSourceMinFAR = min(onSourceMinFAR,far);
                if onsource{iat,ior}.cluster.minFarFlag(ind)
                    warning(['FAR railed for iat=' num2str(iat) ', ior=' num2str(ior) '.']);
                    minFarFlag = 1;
                end
            end
        end
    end
    thresholdFAR = onSourceMinFAR;
end


% ------------------------------------------------------------------------------
%    Compute efficiencies.
% ------------------------------------------------------------------------------

disp(['Evaluating efficiencies at FAR = ' num2str(thresholdFAR) ' Hz.']);

% ---- Loop over analysis times.
for iat=1:length(analysisTimes)

    % ---- Loop over each observing runs.
    for ior = 1:length(obsRuns)

        % ---- Loop over injection sets for this analysisTime
        injSet = injSets{iat};
        for iis = 1:length(injSet)

            % ---- If applying TOA weighting then we skip any injection sets for
            %      which all injection delays are unphysical.
            if ~applyTOAWeighting || (applyTOAWeighting && length(validinj{ior}{iat}{iis})>0)             

                % ---- Copy the current batch of injection triggers into a
                %      convenient variable.                
                injtrig = injtrigAll{injSet(iis),ior};

                % ------------------------------------------------------------------------------
                %    Compute efficiency vs injection scale.
                % ------------------------------------------------------------------------------

                % ---- Compute efficiency at this FAR.
                eff = zeros(1,NinjScales);  %-- temporary storage
                if applyTOAWeighting
                    % ---- Need to weight injections by their a priori
                    %      likelihood from population modelling. Determine that
                    %      weighting (same for all injection scales).
                    params = validinj{ior}{iat}{iis};
                    peakTime = params.gps_s + 1e-9*params.gps_ns;
                    meanTime = mean(peakTime,2);
                    delayTime = peakTime(:,1)-peakTime(:,2);                        
                    aprioriwf = gpstotoaweight(meanTime,delayTime,TOAweights);
                    % ---- Find which of these injections were processed, using
                    %      first injection scale.
                    [C,ia,ib] = intersect(injtrig{1}.idx,params.idx);
                    % ---- Record total a priori weighting of all processed
                    %      injections for this injection set and observing run.
                    %      This will be needed later for normalising the
                    %      efficiencies. Also record number of physical
                    %      injections processed.
                    sumAPrioriWF(injSet(iis),ior) = sum(aprioriwf(ib));
                    NPhysInj(injSet(iis),ior) = length(ib);
                end
                for ii = 1:NinjScales
                    if applyTOAWeighting
                        % ---- Un-normalised efficiency (not physically meaningful).
                        eff(ii) = sum((injtrig{ii}.far(ia) <= thresholdFAR).*aprioriwf(ib));
                    else
                        % ---- Normalised efficiency.
                        eff(ii) = mean(injtrig{ii}.far <= thresholdFAR);
                    end
                end
                % ---- Store full efficiency curve. 
                efficiency(injSet(iis),:,ior) = eff;
                % ---- For convenience, also store analysisTime index associated
                %      with this injection set.  We use this later as a later to
                %      indicate that this injection set has been processed.
                efficiencyAT(injSet(iis),ior) = iat;
                % ---- Compute injection scales for 50% and 90% efficiency.
                is50(injSet(iis),ior) = exp(robustinterp1(eff,log(injScales),0.5));
                is90(injSet(iis),ior) = exp(robustinterp1(eff,log(injScales),0.9));

            end %-- if applyTOAWeighting
            
        end %-- injection set
        
    end %-- obs run
   
end %-- analysis time


% ------------------------------------------------------------------------------
%    Compute run-averaged efficiencies and limits.
% ------------------------------------------------------------------------------

if applyTOAWeighting

    % ---- Compute efficiency to physical population of walls of a given thickness.
    %      For TOA weighting only the total efficiency summed over all injection
    %      sets (for a given narrow thickness range) and weighted by sumAPrioriWF is
    %      relevant. 

    % ---- Load information on walls.
    load injnum_cw_A_d_v.txt
    wall = injnum_cw_A_d_v(:,1);
    A = injnum_cw_A_d_v(:,3);
    d = injnum_cw_A_d_v(:,4);
    v = injnum_cw_A_d_v(:,5);

    % ---- Efficiency vs. injection scale averaged over all runs for all walls
    %      of a given thickness range, weighted by TOA. 
    % ---- Thickness range bin edges.
    d_bins = [0,1,10,100,1000,10000];
    Nbins = length(d_bins)-1;
    
    % ---- Compute efficiency curve for each Observing Run separately, then
    %      averaged weighted by observation time.
    for ior = 1:length(obsRuns)

        % ---- Prepare storage.
        efficiencyD{ior}   = zeros(Nbins,43);
        normAPrioriWF{ior} = zeros(Nbins,1);
        A50D{ior}          = zeros(Nbins,1);
        A90D{ior}          = zeros(Nbins,1);
        ABinValue{ior}     = zeros(Nbins,1);
        
        % ---- Loop over thickness ranges.
        for ibin = 1:Nbins
            d_min = d_bins(ibin);
            d_max = d_bins(ibin+1);
            for ii=1:1100  %-- 1100 is the largest wall number
                if min(efficiencyAT(ii,:)) ~= 0  %-- this is one of the sets processed for all runs
                    if d(wall==ii)>=d_min && d(wall==ii)<d_max  %-- in the desired thickness range
                        % ---- Need to normalise summed efficiencies by sumAPrioriWF.
                        %      For one injection set and observing run:
                        %        eff = sum_{set i}(sum_{inj j}((far_ij<thresh)*w_ij)) / sum_{set k}(sum_{inj l}(w_kl)) 
                        %            = sum_{set i}(------efficiency(iis,:,ior)------) / sum_{set k}(sum_{inj l}(w_kl)) 
                        efficiencyD{ior}(ibin,:) = efficiencyD{ior}(ibin,:) + efficiency(ii,:,ior);
                        normAPrioriWF{ior}(ibin) = normAPrioriWF{ior}(ibin) + sumAPrioriWF(ii,ior);
                        % ---- Record the default wall amplitude parameter. Note
                        %      that A=1e-14 for d>1m, while A=1e-17 for d<=1m. 
                        %      So we can simply use the A value for the last
                        %      wall in each bin. 
                        ABinValue{ior}(ibin) = A(wall==ii);
                    end
                end
            end
            % ---- Normalise by the sum of all APrioriWF for this bin.
            efficiencyD{ior}(ibin,:) = efficiencyD{ior}(ibin,:) / normAPrioriWF{ior}(ibin);
            % ---- Compute is50% and is90% injection scale limits.
            A50D{ior}(ibin) = exp(robustinterp1(efficiencyD{ior}(ibin,:),log(injScales*ABinValue{ior}(ibin)),0.5));
            A90D{ior}(ibin) = exp(robustinterp1(efficiencyD{ior}(ibin,:),log(injScales*ABinValue{ior}(ibin)),0.9));
        end
   
    end  %-- obsRuns
    
    % ---- Finally average efficiency over all observing runs, weighted by observing time. 
    %      Note that the observing time varies by a few percent between analysis
    %      times, so use the average over analysis times.
    % ---- Averaged observing times over analysisTime.
    meanTon =  mean(Ton,1);
    % ---- Prepare storage.
    efficiencyDAvg  = zeros(Nbins,43);
    A50DAvg         = zeros(Nbins,1);
    A90DAvg         = zeros(Nbins,1);
    ABinValueAvg    = zeros(Nbins,1);
    for ibin = 1:Nbins
        for ior = 1:length(obsRuns)
            efficiencyDAvg(ibin,:) = efficiencyDAvg(ibin,:) + efficiencyD{ior}(ibin,:) * meanTon(ior); 
        end
        efficiencyDAvg(ibin,:) = efficiencyDAvg(ibin,:) / sum(meanTon);
        % ---- Compute is50% and is90% limits for run-averaged efficiencies.
        A50DAvg(ibin) = exp(robustinterp1(efficiencyDAvg(ibin,:),log(injScales*ABinValue{1}(ibin)),0.5));
        A90DAvg(ibin) = exp(robustinterp1(efficiencyDAvg(ibin,:),log(injScales*ABinValue{1}(ibin)),0.9));
    end
    
else
    
    % ---- Efficiency vs. injection scale for each wall injection set
    %      individually, averaged over all runs, weighted by observing time. 
    for ii=1:1100  %-- 1100 is the largest wall number
        if min(efficiencyAT(ii,:)) ~= 0  %-- this is one of the sets processed for all runs
            for ior = 1:length(obsRuns)
                efficiencyAvg(ii,:) = efficiencyAvg(ii,:) + efficiency(ii,:,ior) * Ton(efficiencyAT(ii,ior),ior); 
                Ttotal(ii) = Ttotal(ii) + Ton(efficiencyAT(ii,ior),ior);
            end
            efficiencyAvg(ii,:) = efficiencyAvg(ii,:) / Ttotal(ii);
            % ---- Compute is50% and is90% limits for run-averaged efficiencies.
            is50Avg(ii) = exp(robustinterp1(efficiencyAvg(ii,:),log(injScales),0.5));
            is90Avg(ii) = exp(robustinterp1(efficiencyAvg(ii,:),log(injScales),0.9));
        end
    end
    
end


% ------------------------------------------------------------------------------
%    Report efficiency limits to the screen.
% ------------------------------------------------------------------------------


disp('------------------------------------------------------------------------')
disp(' ')
disp(' Projected Wall Amplitude Limits: ')
disp(' ')
disp(['	runName:               ' runName])
disp(['	mode:                  ' mode])
disp(['	applyTOAWeighting:     ' log2str(applyTOAWeighting)])
disp(['	applyDQVetoes:         ' log2str(applyDQVetoes)])
disp(['	applyGravitySpyVetoes: ' log2str(applyGravitySpyVetoes)])
disp(['	gravitySpyClass.Conf.: ' num2str(gravitySpyClassificationConfidence)])
disp(['	gravitySpyMinDuration: ' num2str(gravitySpyMinDuration)])
if islogical(applyAmplRatio)
    disp(['	applyAmplRatio:        ' log2str(applyAmplRatio)])
else
    disp(['	applyAmplRatio:        ' num2str(applyAmplRatio)])
end
disp(['	amplRatioThreshold:    ' num2str(amplRatioThreshold)])
disp(['	seedValue:             ' num2str(seedValue)])
disp(['	FAR threshold:         ' num2str(thresholdFAR) ' Hz  ('  num2str(thresholdFAR*(365.25*86400))  ' y^-1)'])
disp(' ')
if applyTOAWeighting
    d_bins_disp = [d_bins(1:end-1)', d_bins(2:end)'];
    disp('	A50% limits [x10^15]: ')
    disp(['	d_min   d_max   O1	O2	O3a	Combined'])
    fprintf('\t%i\t%i\t%.2f\t%.2f\t%.2f\t%.2f\n', [d_bins_disp [cell2mat(A50D) A50DAvg]*1e15]')
    disp(' ')
    disp('	A90% limits [x10^15]: ')
    disp(['	d_min   d_max   O1	O2	O3a	Combined'])
    fprintf('\t%i\t%i\t%.2f\t%.2f\t%.2f\t%.2f\n', [d_bins_disp [cell2mat(A90D) A90DAvg]*1e15]')
    disp(' ')
else
    A50range = prctile(is50Avg(is50Avg>0),[10 50 90]);
    A90range = prctile(is90Avg(is90Avg>0),[10 50 90]);
    disp('	Amplitude limits [x10^15]: 10th/50th/90th percentiles over all walls tested. ')
    disp(['	    A50:    ' num2str(A50range(1)) '        ' num2str(A50range(2)) '        ' num2str(A50range(3))])
    disp(['	    A90:    ' num2str(A90range(1)) '        ' num2str(A90range(2)) '        ' num2str(A90range(3))])
end
disp('------------------------------------------------------------------------')


% ------------------------------------------------------------------------------
%    Dump info on loudest on-source events to screen.
% ------------------------------------------------------------------------------

if strcmpi(mode,'testing')
    disp(' ')
    disp('      peakTime   unslidTime_H    unslidTime_L     FAR')
    for ior = 1:length(obsRuns)
        disp(['--- ' obsRuns{ior} ' -----------------------------------------------'])
        for iat=1:length(analysisTimes)
            analysisTime_str = ['1/' num2str(1/analysisTimes(iat))];
            if ~isempty(onsource{iat,ior}.cluster.far)
                [far,ind] = min(onsource{iat,ior}.cluster.far);
                trig = xclustersubset(onsource{iat,ior}.cluster,ind);
                peakTime_str = num2str(round(trig.peakTime));
                far_str      = num2str(trig.far);
                unslidTime_str = [num2str(trig.unslidTime(1)) ' ' num2str(trig.unslidTime(2))];
                output_str = [analysisTime_str ' : ' peakTime_str ' ' unslidTime_str ' ' far_str];
                disp(output_str);
            else
                output_str = [analysisTime_str ' : no surviving triggers '];
                disp(output_str);
            end
        end
    end
end


% ------------------------------------------------------------------------------
%    Save workspace.
% ------------------------------------------------------------------------------

% save('post_process_data.mat'); 
% ---- Save data. If an output file of the given name already exists then edit
%      the runName tag until we generate a name  for which no file exists
%      already.
while exist(outputFileName)==2
    outputFileName = ['tmp-' outputFileName]; 
end
save(outputFileName,'-v7.3'); 

