function [search, log, procIdx] = extractinjcctrig(searchFile,logFile,mask,verbose)
% extractinjcctrig - extract triggers best matching injection times
%
% use:
%
%   [search, log, idx] = extractinjcctrig(searchFile,logFile,mask,verbose)
%
% searchFile   String. Name of injection search results file to be loaded, e.g. 
%              'simulation_domainwallq105_40_0_0_merged.mat'.
% logFile      String. Name of injection log file to be loaded, e.g. 
%              'injection_domainwallq105.txt'.
% mask         Optional vector of 1s and 0s. See below.
% verbose      Optional boolean. Controls verbosity.
%
% search       Struct with fields
%                data: [1�1 struct] full raw contents of searchFile.
%                trig: [1�1 struct] x-pipeline trigger struct holding the
%                      best-fit trigger for each processed injection. Here
%                      "best-fit" means 
%                        min(sum(abs(trig_peak_time - log_peak_time)))
%                      where the sum is over detectors.
%                file: copy of searchFile string.
% log          Struct with fields
%                 params: [1�1 struct] containing injection parameters for all
%                      injections with injectionProcessedMask == 1.
%                 file: copy of logFile string.
%                 peakTime: array containing the peak time of each processed
%                      injection for each detector (equal to 
%                      params.gps_s+1e-9*log.params.gps_ns).
% idx          Vector. Injection numbers of extracted injections (row number of
%              log file).
%
% For each injection that has injectionProccessedMask == 1, EXTRACTINJCCTRIG
% returns the trigger that best matches the contents of the log file. If the
% optional input mask is supplied, then a trigger for injection k is returned
% only if injectionProccessedMask(k) == 1 and mask(k) == 1. This option is
% useful for dividing the trigger set into batches, such as for training and
% testing post-processing cuts.
%
% $Id$

% ---- Check number of input arguments.
narginchk(2,4);

% ---- Assign default arguments.
if nargin < 4
    verbose = true;
end

% ---- Load injection trigger file.
search.data = load(searchFile);
search.trig = [];
search.file = searchFile; %-- record for posterity

% ---- Load log file for processed injections (only).
if nargin==2
    % ---- No mask supplied; load all processed injections.
    procIdx = find(search.data.injectionProcessedMask);
else
    % ---- Mask supplied; load only injections that are both processed and in mask.
    Ntrig = length(search.data.injectionProcessedMask);
    if length(mask) < Ntrig
        error(['Input vector mask is shorter than number of triggers in the file (' num2str(Ntrig) ').'])
    end
    procIdx = find(search.data.injectionProcessedMask & mask(1:Ntrig));
end
log.params = parseinjectionparameters(readinjectionfile(logFile,procIdx),verbose);
log.file   = logFile; %-- record for posterity

% ---- Compute expected peak times of each processed injection in each IFO.
log.peakTime = log.params.gps_s + 1e-9*log.params.gps_ns;

% ---- Pull out trigger closest to the expected peak times in each IFO.
for iInj=1:length(procIdx)
    % ---- Extract peak times of all triggers for this injection.
    tmp = search.data.clusterInj(procIdx(iInj));
    peakTime = tmp.likelihood(:,[5,6]) + search.data.analysisTimesCell{1}/2;
    % ---- Pull out trigger closest to the expected peak times in each IFO.
    [~,ind] = min(sum(abs([peakTime(:,1)-log.peakTime(iInj,1),peakTime(:,2)-log.peakTime(iInj,2)]),2));
    if isempty(search.trig)
        search.trig = xclustersubset(tmp,ind);
    else
        search.trig = xclustermerge(search.trig,xclustersubset(tmp,ind));
    end        
end

return

