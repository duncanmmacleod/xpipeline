function [off, ujn_training, NoffJob] = loadoffsourcetriggers(fileName,verbose,seedValue,mode)
% LOADOFFSOURCETRIGGERS - load x-pipeline triggers from an off-source job file
%
% usage:
%
%   [off, ujn, njob] = loadoffsourcetriggers(fileName,verbose,seedValue,mode)
%  
%  fileName 	String. Name of off-source file.
%  verbose 	    Optional Boolean. If true then outputs status messages. Default 
%               true.
%  seedValue 	Optional scalar. Seed value input to rng() to fix state of the 
%               random number generator.
%  mode      	Optional string. If supplied, must be one of 'training' or 
%               'testing'. Default 'training'.
%
%  off 		Struct. Off-source triggers.
%  ujn 		Vector. List of jobNumbers of the returned triggers.
%  njob    	Scalar. Number of unique jobNumber values returned.
%
% LOADOFFSOURCETRIGGERS reads the specified off-source trigger file and divides
% the triggers randomly into two sets by job number. Half of the unique job 
% number values (rounded down if there are an odd number of unique job numbers)
% are assigned as 'training' and the rest as 'testing'. The set specified by 
% mode and the associated job numbers are returned.
%
% The seedValue allows the training/testing split to be reproducible.
% 
% $Id$

% ---- Check number of input arguments.
narginchk(1,4);

% ---- Assign default arguments.
if nargin < 4
    mode = 'training';
end
if nargin >= 3
    % --- Fix the state of the random number generator.
    rng(seedValue);
end
if nargin < 2
    verbose = true;
end

% ---- Load the triggers.
off = load(fileName);

% ---- Select randomly which background jobs will be used for training our cuts.
%      See https://trac.ligo.caltech.edu/xpipeline/wiki/Documentation/Searches/grb/jobnumberdescription
ujn = unique(off.cluster.jobNumber);
Nujn = length(ujn);
ujn_training = ujn(randperm(Nujn));
switch mode
    case 'training'
        ujn_training = ujn_training(1:floor(Nujn/2));
    case 'testing'  
        ujn_training = ujn_training(floor(Nujn/2)+1:end);
end
ujn_training = sort(ujn_training);
NoffJob = length(ujn_training);
if verbose
    disp(['Retaining ' num2str(NoffJob) ' off-source trials for ' mode '.'])
end
% ---- Loop over selected jobs and extract their triggers.
offmask = zeros(size(off.cluster.likelihood,1),1);
for ii=1:NoffJob
    % ---- All triggers belonging to the current jobNumber.
    idx = find(off.cluster.jobNumber==ujn_training(ii));
    % offCell{ii} = xclustersubset(off.cluster,idx);
    % ---- Running list of all triggers belonging to any of the selected jobNumber values.
    offmask(idx) = 1;
end
off.cluster = xclustersubset(off.cluster,find(offmask));
if verbose
    disp(['Retaining ' num2str(length(off.cluster.significance)) ' off-source triggers for ' mode '.'])
end

% ---- To apply data-quality vetoes we will need the true "unslid" time of any
%      time-lagged triggers. The function unslidtriggertime() corrects for 
%      external lags and circular time slides. In addition we need to correct 
%      for the time-of-flight delay between the detectors, since v<<c.
off.cluster.unslidTime = unslidtriggertime(off.cluster,off.blockTime,off.transientTime);
for icol=2:size(off.cluster.unslidTime,2)
    % ---- cluster.likelihood(:,4+N) = start time of event in Nth detector, so correct by difference to N=1.  
    off.cluster.unslidTime(:,icol) = off.cluster.unslidTime(:,icol) + (off.cluster.likelihood(:,4+icol)-off.cluster.likelihood(:,4+1));
end

% ---- Initialise "pass" flag for all retained triggers.
off.cluster.passAll = ones(length(off.cluster.significance),1);

% ---- Done.
return

