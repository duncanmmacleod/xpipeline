function unslidTime = unslidtriggertime(trig,blockTime,transientTime)
% UNSLIDTRIGGERTIME - compute trigger peak times corrected for time slides
%
% usage:
%
%   unslidTime = unslidtriggertime(triggerStruct,blockTime,transientTime)
%
%  triggerStruct  X-Pipeline trigger structure. Required fields: 
%                   peakTime       - weighted central time of trigger [s]
%                   circTimeSlides - circular time slides applied [s] 
%                   timeOffsets    - external time slides applied [s]
%                   startTime      - time of start of analysis block that
%                                    produced this trigger [s] 
%  blockTime      Scalar. Duration [s] of analysis block that produced these
%                 triggers. 
%  transientTime  Scalar. Duration [s] at beginning and end of each analysis
%                 block to be discarded due to filter transients.
%
%  unslidTime     Array. Each element (i,j) is the peak time of the
%                 corresponding trigger "i" corrected for the time slides
%                 applied to each detector "j".  
%
% UNSLIDTRIGGERTIME computes the true trigger peak time in each detector after 
% accounting for time slides, both external and circular. This is useful mainly
% when applying data-qualiy vetoes to off-source (background) triggers or when 
% following up loud off-source triggers with omega scans or similar checks. 
%
% WARNING: this function does not account for time-of-flight delays between 
% detectors. Since the peak time is always defined with respect to the first 
% detector, this produces errors in the peak times for the other detectors.
% This is not a concern for GW searches, as the time-of-flight delays are 
% typically <30 ms and the function that applies data-quality vetoes 
% (utilities/xapplyvetosegments.m) includes a default 30 ms window allowance 
% when comparing veto times to trigger times. However, the error may be 
% significant in searches where the signal speed v=/=c, such as domain wall
% searches. In such cases the unslid trigger times for detectors 2+ need to
% be corrected by the time-of-flight delays.
%
% $Id$

% ---- Determine number of triggers and detectors and prepare storage for output.
nTrigs = size(trig.circTimeSlides,1);
nIfos  = size(trig.circTimeSlides,2);
unslidTime = zeros(nTrigs,nIfos);

% ---- Loop over detectors.
for thisIfo = 1:nIfos 
    % ---- The circular time slides are shifts of data so opposite to time
    %      offsets which are shifts in read data, they also need to be
    %      unwrapped with reference to the start of the time frequency map. Do
    %      this unwrapping first, then add to the start time shifted by the
    %      external slide.
    unslidTime(:,thisIfo) = mod(trig.peakTime - trig.circTimeSlides(:,thisIfo) ...
        - (trig.startTime + transientTime), blockTime - 2*transientTime) ...
        + trig.startTime + transientTime + trig.timeOffsets(:,thisIfo);
end

% ---- Done.
return

