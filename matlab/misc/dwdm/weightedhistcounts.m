function [N, N_under, N_over] = weightedhistcounts(X,W,edges);
% WEIGHTEDHISTCOUNTS - histogram bin counts with weighting
%
% usage:
%
%   [N, N_under, N_over] = weightedhistcounts(X,W,edges);
%
%   X       Vector of data to be binned.
%   W       Vector of same size as X. Weighting to be applied when counting
%           corresponding element of X.
%   edges   Vector of bin edges.
%
%   N       Weighted count of data in the closed interval [edges(1),edges(end)].
%   N_under Weighted count of data in the open interval (-infinity,edges(1)).
%   N_over  Weighted count of data in the open interval (edges(end),infinity).
%
% WEIGHTEDHISTCOUNTS(X,W,edges) performs the same bin counting as
% HISTCOUNTS(X,edges) except that it applies a user-specified weighting W to the
% counts. N(k) = sum(W(i))/Ntot where the sum is over all i such that 
% EDGES(k) <= X(i) < EDGES(k+1). The last bin will also include the right edge
% such that N(end) will count X(i) if EDGES(end-1) <= X(i) <= EDGES(end). The
% normalisation factor Ntot is fixed so that sum(N)+N_under+N_over = length(X).
%       
% $Id$

% ---- Sanity checks.
if any(W<0) 
    warning('Negative histogram weightings found.');
end

% ---- Number of bins.
Nbins = length(edges)-1;

% ---- Assign defaults.
N_under = 0;
N_over  = 0;
N       = zeros(Nbins,1);

% ---- Find "underflow".
idx = find(X<edges(1));
if ~isempty(idx)
    N_under = sum(W(idx));
end

% ---- Find N.
for ibin = 1:(Nbins-1)
    idx = find(X>=edges(ibin) & X<edges(ibin+1));
    if ~isempty(idx)
        N(ibin) = sum(W(idx));
    end
end
% ---- Final bin include right-hand edge.
idx = find(X>=edges(Nbins) & X<=edges(Nbins+1));
if ~isempty(idx)
    N(Nbins) = sum(W(idx));
end

% ---- Find "overflow".
idx = find(X>edges(end));
if ~isempty(idx)
    N_over = sum(W(idx));
end

% ---- Normalise sum of counts to length of X.
Ntot    = N_under + sum(N) + N_over;
N_under = length(X) / Ntot * N_under;
N       = length(X) / Ntot * N;
N_over  = length(X) / Ntot * N_over;

% ---- Done.
return
