load efficiencies/closedbox01-yynn/post_process_data_closedbox01-yynn.mat
% validinj{ior}{iat}{iis};
NPhysInj = zeros(1100,3);
% ---- Loop over analysis times.
for iat=1:length(analysisTimes)
    % ---- Loop over each observing runs.
    for ior = 1:length(obsRuns)
        % ---- Loop over injection sets for this analysisTime
        injSet = injSets{iat};
        for iis = 1:length(injSet)
            if length(validinj{ior}{iat}{iis})>0 
                % ------------------------------------------------------------------------------
                %  Retrieve injection triggers for this injection set.
                % ------------------------------------------------------------------------------
                injtrig = injtrigAll{injSet(iis),ior};
                % ---- Need to weight injections by their a priori
                %      likelihood from population modelling. Determine that
                %      weighting (same for all injection scales).
                params = validinj{ior}{iat}{iis};
                peakTime = params.gps_s + 1e-9*params.gps_ns;
                meanTime = mean(peakTime,2);
                delayTime = peakTime(:,1)-peakTime(:,2);
                aprioriwf = gpstotoaweight(meanTime,delayTime,TOAweights);
                % ---- Find which of these injections were processed, using
                %      first injection scale.
                [C,ia,ib] = intersect(injtrig{1}.idx,params.idx);
                % ---- Record total a priori weighting of all processed
                %      injections for this injection set and observing run.
                %      This will be needed later for normalising the
                %      efficiencies.
                sumAPrioriWF(injSet(iis),ior) = sum(aprioriwf(ib));
                NPhysInj(injSet(iis),ior) = length(ib);
            end
        end
    end
end

MeanPhysWeight  = zeros(length(analysisTimes),1);
TotalPhysWeight = zeros(length(analysisTimes),1);
NumPhysWeight   = zeros(length(analysisTimes),1);
% ---- Loop over analysis times.
for iat=1:length(analysisTimes)
    % ---- Loop over injection sets for this analysisTime
    injSet = injSets{iat};
    % ---- Sum over observing runs and injection sets for this analysys time.
    for iis = 1:length(injSet)
        TotalPhysWeight(iat) = TotalPhysWeight(iat) + sum(sumAPrioriWF(injSet(iis),:));
        NumPhysWeight(iat)   = NumPhysWeight(iat) + sum(NPhysInj(injSet(iis),:));
    end
    MeanPhysWeight(iat) = TotalPhysWeight(iat) / NumPhysWeight(iat);
end

FARweight = MeanPhysWeight / sum(MeanPhysWeight);

