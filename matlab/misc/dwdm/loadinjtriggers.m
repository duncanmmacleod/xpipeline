function [inj, procIdx, injScales] = loadinjtriggers(baseDir,simName,logFile,verbose,seedValue,mode)
% LOADINJTRIGGERS - load x-pipeline triggers from injection job files
%
% usage:
%
%   [inj, idx, injScales] = loadinjtriggers(baseDir,simName,logFile,verbose,seedValue,mode)
%  
%  baseDir      String. Directory holding injection .mat files.
%  simName      String. Name of injection set. Injection files will have names
%               of the form simulation_<simName>_<injScale>_0_0_merged.mat.
%  logFile      String. Name (including path) of the injection log file.
%  verbose      Optional Boolean. If true then outputs status messages. Default 
%               true.
%  seedValue    Optional scalar. Seed value input to rng() to fix state of the 
%               random number generator.
%  mode         Optional string. If supplied, must be one of 'training' or 
%               'testing'. Default 'training'.
%
%  inj          Cell array. Each element is a struct with injection triggers as
%               made by EXTRACTINJCCTRIG. Cell array has one element per
%               injection scale.
%  idx          Vector. Indices of the returned injections (row numbers in the
%               injection log). Same for all injection scales.
%  injScales    Vector of injection scales. 
%
% LOADINJTRIGGERS reads the merged .mat files for the specified injection set
% using EXTRACTINJCCTRIG and divides the triggers randomly into two sets by
% injection number. Half of the injections (rounded down if there are an odd
% number) are assigned as 'training' and the rest as 'testing'. The set
% specified by mode and the total number are returned.
%
% The seedValue allows the training/testing split to be reproducible.
% 
% $Id$

% ---- Check number of input arguments.
narginchk(3,6);

% ---- Assign default arguments.
if nargin < 6
    mode = 'training';
end
if nargin >= 5
    % --- Fix the state of the random number generator.
    rng(seedValue);
end
if nargin < 4
    verbose = true;
end

% ---- Read injection log file.
params = parseinjectionparameters(readinjectionfile(logFile),verbose);

% ---- Determine number of injections.
Ninj = length(params.gps_s);

% ---- Select randomly which injections will be used for training our cuts.
mask = zeros(Ninj,1);
idx = randperm(Ninj);
switch mode
    case 'training'
        idx = sort(idx(1:floor(Ninj/2)));
    case 'testing'  
        idx = sort(idx(floor(Ninj/2)+1:end));
end
mask(idx) = 1;

% ---- Determine injection scale numbers (0,1,2,...,N-1).
startDir = pwd;
cd(baseDir);
[status,result] = system(['ls simulation_' simName '_*_0_0_merged.mat']);
injScaleNums = [];
[TOKEN,REMAIN] = strtok(result);
while ~isempty(TOKEN)
    uidx = strfind(TOKEN,'_');
    injScaleNums(end+1) = str2num(TOKEN(uidx(2)+1:uidx(3)-1));
    [TOKEN,REMAIN] = strtok(REMAIN);
end
injScaleNums = sort(injScaleNums);
NinjScales = length(injScaleNums);
% ---- Check: injScales should have value [0, 1, 2, ..., (Nscales-1)].
if injScaleNums(end) ~= NinjScales-1
    error(['Injection scales do not have expected values: ' num2str(injScaleNums)]);
end
cd(startDir);

% ---- Load injection triggers.
if verbose
    disp('Loading injection triggers for training.')
end
for ii=1:length(injScaleNums)
    injScale = num2str(injScaleNums(ii));
    injFile = [baseDir '/simulation_' simName '_' injScale '_0_0_merged.mat'];
    [inj{ii}, log, procIdx] = extractinjcctrig(injFile,logFile,mask,verbose);
    % ---- To apply data-quality vetoes we will need the true "unslid" time of the
    %      triggers. We don't need to apply unslidtriggertime() since injections 
    %      are performed at zero lag. However, we do need to correct 
    %      for the time-of-flight delay between the detectors, since v<<c.
    % ---- Initialise to peakTime in first detector. We use this instead of 
    %      likelihood(:,4+N) because the likelihood value is the start time, 
    %      while peakTime is the central time.
    inj{ii}.trig.unslidTime = repmat(inj{ii}.trig.peakTime,1,2);
    % ---- Correct for time delay to second detector.
    %      cluster.likelihood(:,4+N) = start time of event in Nth detector, so 
    %      correct by difference to N=1.  
    inj{ii}.trig.unslidTime(:,2) = inj{ii}.trig.unslidTime(:,2) + (inj{ii}.trig.likelihood(:,4+2)-inj{ii}.trig.likelihood(:,4+1));
    % ---- Initialise "pass" flag for all retained triggers.
    inj{ii}.trig.passAll = ones(length(inj{ii}.trig.significance),1);
    % ---- Record value of injection scale.
    injScales(ii) = inj{ii}.data.injectionScale;
end
% if Ninj ~= length(inj{1}.trig.significance) & verbose
%     warning('Some injections have no trigger.');
% end
if verbose
    disp(['Using ' num2str(length(inj{1}.trig.significance)) ' injections for training.'])
end

% ---- Done.
return

