function on = loadonsourcetriggers(fileName,threshold,verbose)
% LOADONSOURCETRIGGERS - load x-pipeline triggers from an on-source job file
%
% usage:
%
%   on = loadonsourcetriggers(fileName,threshold,verbose)
%  
%  fileName 	String. Name of on-source file.
%  threshold 	Optional scalar. If supplied, only clusters with
%               cluster.significance >= threshold are returned. Default 0.
%  verbose 	    Optional Boolean. If true then outputs status messages. Default 
%               true.
%
%  on  		    Struct. On-source triggers.
%
% LOADOFFSOURCETRIGGERS reads the specified on-source trigger file and
% optionally applies the significance threshold. It also initialises the
% .passAll flag to 1 for all triggers, and computes the unslidTime correcting
% for the time-of arrive differences in the two detectors. These are both needed
% for applying data-quality and other vetoes to the triggers.
% 
% $Id$

% ---- Check number of input arguments and assign defaults.
narginchk(1,3);
if nargin < 3
    verbose = true;
end
if nargin < 2
    threshold = 0;
end

% ---- Load all data from the file.
on = load(fileName);
if verbose 
    disp(['Loaded ' num2str(length(on.cluster.significance)) ' on-source triggers.'])
end

% ---- To apply data-quality vetoes we will need the true "unslid" time of the
%      triggers. We don't need to apply unslidtriggertime() since on-source 
%      triggers are generated at zero lag. However, we do need to correct 
%      for the time-of-flight delay between the detectors, since v<<c.
% ---- Initialise to peakTime in first detector. We use this instead of 
%      likelihood(:,4+N) because the likelihood value is the start time, 
%      while peakTime is the central time.
on.cluster.unslidTime = repmat(on.cluster.peakTime,1,2);
% ---- Correct for time delay to second detector.
%      cluster.likelihood(:,4+N) = start time of event in Nth detector, so 
%      correct by difference to N=1.  
on.cluster.unslidTime(:,2) = on.cluster.unslidTime(:,2) + (on.cluster.likelihood(:,4+2)-on.cluster.likelihood(:,4+1));

% ---- Initialise "pass" flag for all retained triggers.
on.cluster.passAll = ones(size(on.cluster.significance));

% ---- Apply significance threshold used for off-source triggers. 
idx = find(on.cluster.significance>=threshold);
on.cluster = xclustersubset(on.cluster,idx);
if verbose 
    disp(['Retaining ' num2str(length(on.cluster.significance)) ' on-source triggers after significance thresholding.'])
end

% ---- Done.
return

