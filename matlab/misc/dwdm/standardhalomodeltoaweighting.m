function w = standardhalomodeltoaweighting(dt,interval,Nmc,Nplot,normw)
% STANDARDHALOMODELTOAWEIGHTING - simulate H-L TOA differences for SHM 
%
% usage:
%
%  w = standardhalomodeltoaweighting(dt,interval,Nmc,Nplot,normw)
%
%  dt        Vector. Bin edges for TOA differences as used by the HISTOGRAM
%            function.
%  interval  Optional two element vector [A,B] where 0<=A,B<=1. Simulated walls
%            are restricted to the sidereal time interval  [A,B]*T, where T is
%            one sidereal day. Default: [0,1].
%  Nmc       Optional positive integer scalar. Number of walls to simulate.
%            Default 1e6. 
%  Nplot   	 Optional non-negative integer scalar. If 0, no plots are made. If
%            nonzero, plots are made using only the first Nplot simulated walls 
%            for scatter plots (to avoid overloading matlab).
%  normw     Optional boolean. If true, weights are normalised so that
%            max(w(:)=1. Default true.
%
%  w         Vector of length length(dt)-1. w(ii) is the relative fraction of
%            simulated walls with TOA differences in the interval 
%            [dt(ii),dt(ii+1)). (The w(end) includes TOA equal to dt(end),
%            following the convention for HISTOGRAM.) The w vector is rescaled
%            so that max(w)=1.
%
% STANDARDHALOMODELTOAWEIGHTING simulates a population of bodies with velocities
% following the standard halo model (see STANDARDHALOMODEL) and computes the
% distribution of expected time-of-arrival (TOA) differences t_H-t_L between the
% LIGO Hanford (H) and Livingston (L) detectors. The distribution is rescaled by
% the Earth-wall relative speed to reflect that the expected rate of collisions
% is proportional to relative speed. 
%
% $Id$

% ---- Procedure:
% 1. Generate a population of domain wall velocities v_dm relative to Galaxy in
%    Galactic coordinates.  
% 2. Compute Sun's velocity v_sun relative to Galaxy in Galactic coordinates 
%    (fixed over observing period).
% 3. Given GPS time of wall:
%    a. Compute Earth's velocity v_earth relative to Sun in Ecliptic coordinates.
%    b. Compute Earth's velocity v_earth relative to Sun to Equatorial coordinates.
%    c. Compute Earth's velocity v_earth relative to Sun to Galactic coordinates.
%    d. Compute Earth-wall relative velocities (v_dm-[v_sun+v_earth]) in Galactic coordinates.
%    e. Convert Earth-wall relative velocities from Galactic to Earth-fixed coordinates.
%    f. Compute H-L delay times for populations of walls.
%    g. Rescale population of delays by the relative speeds |v_dm-[v_sun+v_earth]| 
%       of the walls (to account for rate dependence on speed).

% ------------------------------------------------------------------------------

% % ---- Run this code as a separate script to use this function to generate a
% %      wstruct weighting script as used by GPSTOTOAWEIGHT.
% % close all; 
% clear;
% dt = [-30:30]';
% Nmc = 1e7;
% Nbin = 100;
% for ii=1:Nbin
%     interval = [(ii-1)/Nbin,(ii)/Nbin];
%     w(:,ii) = standardhalomodeltoaweighting(dt,interval,Nmc,0,false);
% end
% % ---- Normalise so that max(w) = 1.
% w = w/max(abs(w(:)));
% % ---- Make plots.
% figure;
% subplot(1,2,1)
% stairs(dt(1:end-1),w)
% axis([ -30 30 0 1.1])
% grid on; hold on
% set(gca,'fontsize',16)
% xlabel('H-L TOA delay [s]')
% ylabel('relative frequency [au]')
% legend(['all ' num2str(Nbin) ' sidereal bins'])
% subplot(1,2,2)
% stairs(dt(1:end-1),max(w,[],2),'linewidth',2)
% grid on; hold on
% stairs(dt(1:end-1),sum(w,2)/Nbin,'linewidth',2)
% stairs(dt(1:end-1),min(w,[],2),'linewidth',2)
% axis([ -30 30 0 1.1])
% stairs(dt(1:end-1),max(abs(diff(w,1,2)),[],2),'linewidth',2)
% stairs(dt(1:end-1),median(abs(diff(w,1,2)),2),'linewidth',2)
% legend('max over sidereal bins','mean over sidereal bins', ...
%     'min over sidereal bins','max bin difference','median bin difference')
% set(gca,'fontsize',16)
% xlabel('H-L TOA delay [s]')
% ylabel('relative frequency [au]')
% save workspace.mat

% ------------------------------------------------------------------------------

% ---- Magic numbers.
% ---- Sidereal year, in seconds. See https://en.wikipedia.org/wiki/Year#Astronomical_years
year = 365.256363004 * 86400;
% ---- Sidereal day, in seconds. See https://en.wikipedia.org/wiki/Sidereal_time#Sidereal_day
sidday  = 86164.0905;
% ---- The ecliptic north pole is at 18 h 0 m 0 s, +66� 33m 38.84s
%      (source: https://en.m.wikipedia.org/wiki/Orbital_pole).
enp.ra = [18 0 0]*[1 1/60 1/3600]'*15;
enp.dec = [66 33 38.84]*[1 1/60 1/3600]';
% ---- Earth's orbital angular velocity, in rad/second.
omega = 2*pi/year; 
% ---- Astronomical unit, in km. See https://en.wikipedia.org/wiki/Astronomical_unit .
au = 1.495978707e8;
% ---- GPS time of a recent vernal equinox.
%         -bash-4.2$ lalapps_tconvert Sat Mar 20 09:37:00 GMT 2021
%         1300268238
%         -bash-4.2$ lalapps_tconvert 1300268238
%         Sat Mar 20 09:37:00 GMT 2021  [seconds not specified in source]
equinox.gps = 1300268238;

% ------------------------------------------------------------------------------

% ---- Check number of input arguments.
narginchk(1,5);

% ---- Assign defaults.
if nargin<5
    normw = true;
end
if nargin<4
    Nplot = 0;
end
if nargin<3
    Nmc = 1e6;
end
if nargin<2
    minSidTime = 0.0;
    maxSidTime = 1.0;
else
    minSidTime = interval(1);
    maxSidTime = interval(2);
    
end

% ------------------------------------------------------------------------------

% ----- Analytic distribution of dark-matter domain-wall speeds.
v_max = 1000;
v = [0:v_max]';
p3D = standardhalomodelpdf(v);
p1D = 4*pi*v.^2.*p3D;
c1D = cumsum(p1D)/sum(p1D);
%
% figure; 
% plot(v,p1D/max(p1D),'linewidth',2)
% grid on; hold on
% plot(v,c1D,'linewidth',2)
% xlabel('speed [km/s]')
% legend('probability distribution','cumulative distribution','location','east')
% title('Analytic distribution of wall speeds relative to Galaxy')
% set(gca,'fontsize',16)

% ---- Generate population of walls. 
R = rand(Nmc,1);
wall.gal.v = robustinterp1(c1D,v,R);
wall.gal.theta = acos(2*rand(Nmc,1)-1);
wall.gal.phi = 2*pi*rand(Nmc,1);
wall.gal.vec = wall.gal.v .* [ sin(wall.gal.theta).*cos(wall.gal.phi) ...
                               sin(wall.gal.theta).*sin(wall.gal.phi) ...
                               cos(wall.gal.theta) ];
wall.gal.norm = sum(wall.gal.vec.^2,2).^0.5;
% ---- Add randomly generated time stamps, distributed over the requested
%      sidereal time interval and scattered over one year starting from a
%      specified reference time. For simplicity, we select a reference time
%      corresponding to GMST = 0h so that our sidereal hour bins will start from
%      GMST = 0h.
gps_ref = 1240048535;  %-- GPSTOGMST(1240048535) = 0.532 sec ~ 0.
wall.gps = gps_ref + (unidrnd(floor(year/sidday),Nmc,1)-1+minSidTime) * sidday ...
                      + rand(Nmc,1) * (maxSidTime-minSidTime) * sidday;
% ---- Scattered uniformly over 1 sidereal year.
% wall.gps = 1240000000 + year * rand(Nmc,1);

% ------------------------------------------------------------------------------

% ---- Sun's speed with respect todelay  the Galactic centre. See 1401.5377; we use
%      their values for [?0 + V?, -U? , W? ] from the abstract and Sect. 5.1.
%      We choose (x,y,z) directions such that x is towards the Galactic centre,
%      y is the direction of the Sun's circular motion around the Galaxy, and z
%      gives a right-handed coordiate system. This differs from the choice in
%      Fig. 1 of 1401.5377. This choice will allow us to use the spherical
%      coordinates of our simulated walls as being in Galactic coordinates,
%      which is convenient. 
%      One-sigma uncertainties of the velocity components are a few km/s. 
v_sun = [9.6,255.2,9.3]; %-- km/s

% ------------------------------------------------------------------------------

% ---- Modelling Earth's motion relative to the Sun at the time of each wall
%      crossing. 
% ---- Earth's orbital velocity vector angles in ecliptic coordinates. Note that
%      at the vernal equinox the Sun is in the direction (ra,dec) = (0,0) by 
%      definition.
earth.ec.lambda = 3*pi/2 + omega*(wall.gps-equinox.gps);
earth.ec.beta   = zeros(size(wall.gps));
% ---- Convert these to equatorial then galactic coordinates:
[earth.eq.alpha,earth.eq.delta] = ecliptictoequatorial(earth.ec.lambda,earth.ec.beta);
[earth.gal.ell,earth.gal.b] = equatorialtogalactic(earth.eq.alpha,earth.eq.delta);
% ---- Convert from angular coordinates to Cartesian vectors.
earth.gal.vec =  omega * au * [ ...
    sin(pi/2-earth.gal.b).*cos(earth.gal.ell) ...
    sin(pi/2-earth.gal.b).*sin(earth.gal.ell) ...
    cos(pi/2-earth.gal.b) ];

% ------------------------------------------------------------------------------

% ---- Wall speeds relative to the Earth.
rel.gal.vec = wall.gal.vec - (earth.gal.vec  + v_sun);
% warning('ignoring earths orbital motion.')
% rel.gal.vec = wall.gal.vec - (v_sun);
norm_v_rel = sum(rel.gal.vec.^2,2).^0.5;

% ------------------------------------------------------------------------------

% ---- Convert wall-Earth relative velocity to Earth-fixed coordinates.
% ---- First convert v_rel in spherical coordinates in Galactic frame.
%      Include a factor (-1) to convert from direction of propagation to
%      direction of incidence. 
rel.gal.n = - (rel.gal.vec) ./ (sum((rel.gal.vec).^2,2)).^0.5;
rel.gal.theta = acos(rel.gal.n(:,3));
rel.gal.phi = atan2(rel.gal.n(:,2),rel.gal.n(:,1));
[alpha,delta] = galactictoequatorial(rel.gal.phi,pi/2-rel.gal.theta);
rel.earth.ra = alpha*180/pi; %-- convert to deg
rel.earth.dec = delta*180/pi;

% ---- Convert to Earth-fixed coordinates.
[rel.earth.phi, rel.earth.theta] = radectoearth(rel.earth.ra,rel.earth.dec,wall.gps);
% ---- Pointing vector to apparent incident direction.
rel.earth.n = [ sin(rel.earth.theta).*cos(rel.earth.phi) ...
                sin(rel.earth.theta).*sin(rel.earth.phi) ...
                cos(rel.earth.theta)];

% ------------------------------------------------------------------------------

% ---- Compute delay times.
L = LoadDetectorData('L');
H = LoadDetectorData('H');
baseline = (L.V-H.V)/1000;  %-- convert to km for convenience
deltaT = rel.earth.n * baseline ./ norm_v_rel;

% ---- Compute weighted bin counts.
edges = dt;
[NdeltaT, NdeltaT_under, NdeltaT_over] = weightedhistcounts(deltaT,ones(size(norm_v_rel)),edges);
[NdeltaT_scaled, NdeltaT_scaled_under, NdeltaT_scaled_over] = weightedhistcounts(deltaT,norm_v_rel,edges);
if normw
    w = NdeltaT_scaled/max(NdeltaT_scaled);
else
    w = NdeltaT_scaled;
end

% ------------------------------------------------------------------------------

% ---- Plots.
if Nplot
    
    % ---- Distribution of wall speeds.
    figure;
    edges = [0:10:v_max];
    Nnorm_v_wall = histcounts(wall.gal.norm,edges);
    Nnorm_v_rel = histcounts(norm_v_rel,edges);
    Ny_rate_scaled = Nnorm_v_rel .* (edges(1:end-1)+5);
    Ny_rate_scaled = Ny_rate_scaled / sum(Ny_rate_scaled) * Nmc;
    stairs(edges(1:end-1),Nnorm_v_wall,'linewidth',2)
    grid on; hold on
    stairs(edges(1:end-1),Nnorm_v_rel,'linewidth',2)
    plot(v,p1D/max(p1D)*max(Nnorm_v_wall),'y--','linewidth',2)
    stairs(edges(1:end-1),Ny_rate_scaled,'linewidth',2)
    xlabel('speed [km/s]')
    legend('relative to Galaxy','relative to Earth','relative to Galaxy (analytical)', ...
        'speed-rescaled','location','northeast')
    title('Distribution of wall speeds')
    set(gca,'fontsize',16)

    % ---- Cumulative distribution of wall speeds.
    figure;
    plot(edges(1:end-1)+5,cumsum(Nnorm_v_wall)/Nmc,'linewidth',2)
    grid on; hold on
    plot(edges(1:end-1)+5,cumsum(Nnorm_v_rel)/Nmc,'linewidth',2)
    plot(edges(1:end-1)+5,cumsum(Ny_rate_scaled)/Nmc,'linewidth',2)
    xlabel('speed [km/s]')
    legend('relative to Galaxy','relative to Earth','speed-rescaled','location','southeast')
    title('Cumulatve distribution of wall speeds')
    set(gca,'fontsize',16)
    axis([0 v_max 0 1])

    % ---- Incident direction of walls - (ra,dec) coordinates.
    figure;
    plot(rel.earth.ra(1:Nplot),rel.earth.dec(1:Nplot),'.')
    axis equal
    axis([0 360 -90 90])
    grid on; hold on
    xlabel('RA [deg]')
    ylabel('Dec [deg]')
    title('Incident direction of walls')
    set(gca,'fontsize',16)
    % ---- Add markers for important directions with respect to the Galaxy.
    [a,d] = galactictoequatorial([pi/2; 0; 0; 0; pi; 3/2*pi],[0; pi/2; -pi/2; 0; 0; 0]);
    hold on
    plot(a(1)*180/pi,d(1)*180/pi,'m*')
    plot(a(2)*180/pi,d(2)*180/pi,'r+')
    plot(a(3)*180/pi,d(3)*180/pi,'rx')
    plot(a(4)*180/pi,d(4)*180/pi,'r>')
    plot(a(5)*180/pi,d(5)*180/pi,'rd')
    plot(a(6)*180/pi,d(6)*180/pi,'mo')
    legend('sample walls','Sun dir','NGP','SGP','GC','GaC','opposite','location','eastoutside')

    % ---- Incident direction of walls - Earth-fixed coordinates.
    figure;
    plot(rel.earth.phi(1:Nplot),rel.earth.theta(1:Nplot),'.')
    axis equal
    axis([-pi pi 0 pi])
    grid on; hold on
    xlabel('phi [rad]')
    ylabel('theta [rad]')
    title('Incident direction of walls')
    set(gca,'fontsize',16)
    set(gca,'ydir','reverse')

    % ---- Raw and speed-rescaled TOA distributions (log scale).
    figure;
    edges = dt;
    stairs(edges(1:end-1),NdeltaT/max(NdeltaT),'linewidth',2)
    grid on; hold on
    stairs(edges(1:end-1),NdeltaT_scaled/max(NdeltaT_scaled),'linewidth',2)
    title('TOA differences of walls - speed-scaled')
    set(gca,'fontsize',16)
    xlabel('time t_H-t_L  [s]')
    legend('raw','speed-rescaled','location','northeast')
    set(gca,'yscale','log')
    axis([ -100 100 1e-6 1])
    
    % ---- Speed-rescaled TOA distribution (linear scale).
    figure;
    stairs(edges(1:end-1),NdeltaT_scaled,'linewidth',2)
    grid on; hold on
    title('TOA differences of walls - speed-scaled')
    set(gca,'fontsize',16)
    xlabel('time t_H-t_L  [s]')
    legend('speed-rescaled','location','northeast')
    axis([ -20 20 get(gca,'ylim')])

    % ---- Tails of TOA distribution.
    figure;
    subplot(1,2,1)
    plot(edges(1:end-1)+0.5,cumsum(NdeltaT)/Nmc,'linewidth',2)
    grid on; hold on
    plot(edges(1:end-1)+5,cumsum(NdeltaT_scaled)/Nmc,'linewidth',2)
    title('TOA differences of walls: t_H<t_L')
    xlabel('time t_H-t_L  [s]')
    ylabel('cumulative distribution')
    legend('raw','speed-rescaled','location','southeast')
    set(gca,'fontsize',16)
    set(gca,'yscale','log')
    axis([-50 0 1e-5 1])
    subplot(1,2,2)
    plot(edges(1:end-1)+0.5,1-cumsum(NdeltaT)/Nmc,'linewidth',2)
    grid on; hold on
    plot(edges(1:end-1)+5,1-cumsum(NdeltaT_scaled)/Nmc,'linewidth',2)
    title('TOA differences of walls: t_H>t_L')
    xlabel('time t_H-t_L  [s]')
    ylabel('1 - cumulative distribution')
    legend('raw','speed-rescaled','location','southeast')
    set(gca,'fontsize',16)
    set(gca,'yscale','log')
    set(gca,'ydir','reverse')
    axis([ 0 50 1e-5 1])

end

% ---- Done.
return
