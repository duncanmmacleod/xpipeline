function estimateInjScale(grbName,autoFileName)
  % estimateInjScale(grbName,autoFileName)
% estimate what injection scales to use based on SNR in on source amplitude spectra


% $Id$
error(nargchk(2, 2, nargin));

% ---- read where results are
fin = fopen(autoFileName,'r');
offSourceMatFile = fscanf(fin,'%s',1);
onSourceMatFile = fscanf(fin,'%s',1);
nWaveforms = fscanf(fin,'%d',1);
for i=1:nWaveforms
    autoFilesCell{i} = fscanf(fin,'%s',1);
end
for i=1:nWaveforms
    injectionFilesCell{i} = fscanf(fin,'%s',1);
end

load(onSourceMatFile,'skyPositions','detectorList','analysisTimesCell',...
     'amplitudeSpectraCell','sampleFrequency','maximumFrequency', ...
    'minimumFrequency')

% ---- Find detector spectrum with best resolution
bATi=1;maxPrec=0;
for i =1:length(amplitudeSpectraCell)
   if( maxPrec < length(amplitudeSpectraCell{i}))
       maxPrec = length(amplitudeSpectraCell{i});
       bATi =i;
   end
end
S = amplitudeSpectraCell{bATi}.^2;
dF = 1/analysisTimesCell{bATi};
fs= sampleFrequency;

% ---- compute antenna responses
for iDetector =1:length(detectorList)
    [Fp, Fc] = ComputeAntennaResponse(skyPositions(2),skyPositions(1),0,...
        detectorList{iDetector});
    Favg{iDetector}=sqrt((Fp^2+Fc^2)/2);
end

% open output file
fout = fopen([grbName 'InjScales.txt'],'w');

% ---- Loop over waveforms
for iWave = 1:length(autoFilesCell)
    % ---- Read injection parameters
    disp(['Loading file ' injectionFilesCell{iWave}])
    [injectionParameters] = readinjectionfile(injectionFilesCell{iWave});
    [~, gps_s, gps_ns, phi, theta, psi, name, parameters] = parseinjectionparameters(injectionParameters);
    nameCell{iWave} = cell2mat(name{1});
    parametersCell{iWave} = cell2mat(parameters{1});
    disp(['Working on waveform: ' nameCell{iWave} ' ' parametersCell{iWave}])
    % ---- Compute  snr, hrss, ...
    [t,hp,hc] = xmakewaveform(nameCell{iWave},parametersCell{iWave},1/dF,1/dF*0.5,fs);
    for iDetector =1:length(detectorList)
        SNR =  xoptimalsnr([hp hc],0,fs,S(:,iDetector),0,dF,minimumFrequency,maximumFrequency);
        SNRlist(iDetector) = SNR*Favg{iDetector} ;
    end
    sumSNR=sum(SNRlist);
    if(strcmp(nameCell{iWave},'SG'))
        injScaleList=30/sumSNR*(exp(-1.5:0.2:1.7));
    else
        injScaleList=20/sumSNR*(exp(-1.5:0.25:1.5));
    end
    tmp=autoFilesCell{iWave};
    tmp1 = strread(tmp ,'%s','delimiter','.');
    tmp2 = strread(tmp1{1},'%s','delimiter','_');
    setname='';
    for i=2:(length(tmp2)-1)
       setname = [setname tmp2{i} '_'];
    end
    setname = [setname tmp2{end}];
    fprintf(fout,'%s\ninjectionScales = ',['[' setname ']']);
    for iInj=1:(length(injScaleList)-1)
        fprintf(fout,'%f,',injScaleList(iInj));
    end
    fprintf(fout,'%f\n',injScaleList(end));
end
