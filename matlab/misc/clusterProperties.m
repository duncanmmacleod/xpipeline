function [clusters] = clusterProperties(map,clusters_Struct);
% clusterProperties - Compute properties of pre-defined clusters in a 
% time-frequency map.
%
% [clusters] = clusterProperties(map,cStruct) 
% 
%   map         Two-dimensional array of time-frequency map values.  
%               Dimension 1 = frequency, dimension 2 = time.
%   cStruct     Struct array containing at least one of the 'PixelList' and
%               'PixelIdxList' cluster properties as given by regionprops.
%
%   clusters    mx8 matrix (m is # of clusters)
%               column 1: minimum time of cluster
%               column 2: weighted center time of cluster
%               column 3: maximum time of cluster
%               column 4: minimum frequency of cluster
%               column 5: weighted center frequency of cluster
%               column 6: maximum frequency of cluster
%               column 7: summed map values over cluster
%               column 8: number of pixels in cluster
%               All times and frequencies are in units of bins.
%
% $Id$

% ---- Check for sufficient command line arguments
error(nargchk(2, 2, nargin));

% ---- Preliminaries.
numberOfClusters = length(clusters_Struct);

% ---- Make sure at least one of PixelIdxList or PixelList is available in 
%      clusters_Struct.
if (isfield(clusters_Struct,'PixelIdxList') & isfield(clusters_Struct,'PixelList'))
    ;
elseif (isfield(clusters_Struct,'PixelIdxList') & ~isfield(clusters_Struct,'PixelList'))
    for clusterNumber = 1:numberOfClusters
        [I,J] = ind2sub(size(map),clusters_Struct(clusterNumber).PixelIdxList);
        clusters_Struct(clusterNumber).PixelList = [J,I];
    end
elseif (~isfield(clusters_Struct,'PixelIdxList') & isfield(clusters_Struct,'PixelList'))
    for clusterNumber = 1:numberOfClusters
        K = sub2ind(size(map),clusters_Struct(clusterNumber).PixelList(:,2), ...
            clusters_Struct(clusterNumber).PixelList(:,1));
        clusters_Struct(clusterNumber).PixelIdxList = K;
    end
else
    error('input cluster struct must contain PixelIdxList or PixelList property.')
end

% ---- Use bounding box, if available, to compute cluster extrema.
if (isfield(clusters_Struct,'BoundingBox'))
    clusters_BoundingBox = reshape([clusters_Struct.BoundingBox],4,[])';
    clusters_tmin = clusters_BoundingBox(:,1);
    clusters_tmax = clusters_BoundingBox(:,1)+clusters_BoundingBox(:,3);
    clusters_fmin = clusters_BoundingBox(:,2);
    clusters_fmax = clusters_BoundingBox(:,2)+clusters_BoundingBox(:,4);
else
    clusters_tmin = zeros(numberOfClusters,1);
    clusters_tmax = zeros(numberOfClusters,1);
    clusters_fmin = zeros(numberOfClusters,1);
    clusters_fmax = zeros(numberOfClusters,1);
end

% ---- Retrieve area, if available.
if (isfield(clusters_Struct,'Area'))
    clusters_area = [clusters_Struct.Area]';
else
    clusters_area = zeros(numberOfClusters,1);
end

% ---- Compute remaining cluster properties in for loop. 
clusters_Likelihood = zeros(numberOfClusters,1);
clusters_tpeak = zeros(numberOfClusters,1);
clusters_fpeak = zeros(numberOfClusters,1);
for clusterNumber = 1:numberOfClusters
    % ---- Retrieve pixel indices and subscripts.
    pixels = clusters_Struct(clusterNumber).PixelIdxList;
    pixelsT = clusters_Struct(clusterNumber).PixelList(:,1);
    pixelsF = clusters_Struct(clusterNumber).PixelList(:,2);
    % ---- Compute cluster extrema, if not done above.
    if (~isfield(clusters_Struct,'BoundingBox'))
        clusters_tmin(clusterNumber) = min(pixelsT) - 0.5;
        clusters_tmax(clusterNumber) = max(pixelsT) + 0.5;
        clusters_fmin(clusterNumber) = min(pixelsF) - 0.5;
        clusters_fmax(clusterNumber) = max(pixelsF) + 0.5;
    end
    % ---- Compute cluster area, if not done above.
    if (~isfield(clusters_Struct,'Area'))
        clusters_area(clusterNumber) = length(pixelsT);
    end
    % ---- Compute cluster peak time, peak freq, summed map value.
    clusters_Likelihood(clusterNumber) = sum(map(pixels));
    clusters_tpeak(clusterNumber) = sum(map(pixels) .* pixelsT) ...
        / clusters_Likelihood(clusterNumber);
    clusters_fpeak(clusterNumber) = sum(map(pixels) .* pixelsF) ...
        / clusters_Likelihood(clusterNumber);
end

% ---- Pack together in output.
clusters = [clusters_tmin, clusters_tpeak, clusters_tmax, ...
    clusters_fmin, clusters_fpeak, clusters_fmax, ...
    clusters_Likelihood, clusters_area];
        
% ---- Done
return

