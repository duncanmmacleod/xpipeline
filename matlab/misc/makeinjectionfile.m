function makeinjectionfile(injectionFileName,signal,nUncorrel,startTime,...
    duration,periodic,interval)
% makeinjectionfile - Make a file containing parameters that specify
% signals to be injected (simulated) in the coherent network search 
% pipeline. 
%
% There are two ways to call this function.  In the first form, the times
% at which injections occur are determined randomly or quasi-randomly,
% according to user-specified parameters:
%
%   makeinjectionfile(injectionFileName,signal,nUncorrel,startTime, ...
%       duration,periodic,interval)
%
% injectionFileName  
%           String.  Name of output file.
% signal    Nx2 cell array of strings.  Each row specifies one type of  
%           signal to be injected: signal{:,1} is a GWB "type" recognized
%           by MakeWaveform; signal{:,2} is a tilde-delimited string with
%           valid "params" for that signal type.
% nUncorrel Scalar.  Number of "uncorrelated" signals per injection event.  
%           Must be [] or a positive integer.  If equal to 1 or [], then each
%           simulation is for a correlated GWB signal and has a single set
%           of injection parameters.  If nUncorrel>1, then make independent
%           injections for each of nUncorrel detectors.  In this case the
%           signal for each detector is determined from separate draws from
%           "signal", with the restriction of different signals for each
%           detector.  Note that we must have size(signal,1)>=nUncorrel.
% startTime Scalar.  Earliest time at which injections can occur.
% duration  Scalar.  Duration during which injections can occur. 
% periodic  Boolean.  If true (or nonzero) then injections occur at fixed
%           time intervals.  If false, injection times are selected
%           independently and randomly with uniform distribution over
%           [startTime, startTime+duration].  The total number of
%           injections in this case is determined randomly from a Poisson
%           distribution with mean=duration/interval.
% interval  Scalar.  Spacing between injections (fixed or mean).
%
% In the second form function call the times at which injections occur are
% specified by the user:
%
%   makeinjectionfile(injectionFileName,signal,nUncorrel,injectionTimes)
%
% injectionTimes 
%           Vector.  List of specific times at which injections are to 
%           occur.
%
% In this form the arguments injectionFileName, signal, nUncorrel are 
% defined as above.
%
% The output file will have one injection per row with columns
%   peak time at center of Earth (sec, GPS)
%   peak time at center of Earth (nanosec, GPS)
%   azimuthal sky position (radians, Earth-centered coordinates)
%   polar sky position (radians, Earth-centered coordinates)
%   polarization angle (radians, Earth-centered coordinates)
%   waveform type (string) as recognized by xmakewaveform
%   waveform parameter (string) as recognized by xmakewaveform
% Repeated columns are used when specifying different waveforms for each
% detector (i.e., for simulating glitches).  Note that an error will occur
% or glitch injections when the number of injections does not match the
% number of detectors analysed.
%
% Currently the sky position and polarization angle are always uniformly
% distributed; e.g., there is no way to specify injections from a
% particular direction.
%
% inital write: Patrick J. Sutton, 2005.07.11
%
% $Id$


%----- Check for a valid number of command line arguments.
if ~((nargin==4) | (nargin==7))
    error('Wrong number of input arguments.');
end


%----- Assign default value to nUncorrel, if not specified.
if (isequal(nUncorrel,[]))
    nUncorrel = 1;
end


%----- Error check on arguments.
if (size(signal,1)<nUncorrel)
    error('Error: Must have size(signal,1)>=nUncorrel.');
end


%----- Select peak times of injections.  
if (nargin==4)
    %-- Use user-specified times.
    t = startTime(:);
else
    %-- Generate random start times.
    if (periodic)
        %-- First injection occurs a random fraction of one interval after
        %   startTime.
        t = [startTime+rand(1)*interval:interval:startTime+duration]';
    else
        %-- Poisson process: pick Poisson-random number of injections,
        %   distribute uniformly and independently.
        t = sort(startTime+rand(poissrnd(duration/interval),1)*duration);
    end
end
%----- Must be a column vector!
t = t(:);
%----- Number of injections.
nInjection = length(t);


%----- Sky position, polarization.
phi = 2*pi*rand(length(t),1);
ctheta = -1+2*rand(length(t),1);
theta = acos(ctheta);
psi = pi*rand(length(t),1);


%----- Collect numeric data into single handy array.
numData = [floor(t), round(1e9*(t-floor(t))), phi, theta, psi];


%----- Write injection log file.
fid = fopen(injectionFileName,'w');


%----- GWB type.
nSignal = size(signal,1);
for jInjection=1:nInjection

    %----- List of signals available for this injection (row numbers in
    %      "signal").
    signalIndexAvail = [1:nSignal]';

    %----- Loop over uncorrelated signals.
    for jDetector = 1:nUncorrel
        
        %----- Pick a signal to inject, and remove it from the list of
        %      remaining choices.
        signalIndex = signalIndexAvail(1+floor(rand(1)*length(signalIndexAvail)));
        k = find(signalIndexAvail==signalIndex);
        signalIndexAvail(k) = [];
        
        %----- Write signal type and parameters to output file.
        GWB_type = signal{signalIndex,1};
        GWB_params = signal{signalIndex,2};
        fprintf(fid,'%d %d %e %e %e ',numData(jInjection,:)');
        fprintf(fid,'%s ',GWB_type);
        fprintf(fid,'%s ',GWB_params);

    end

    fprintf(fid,'\n');

end


%----- Close injection log file.
fclose(fid);


%----- Done.
return;

