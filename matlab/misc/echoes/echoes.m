
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Notes and junk code.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% To do:

% verify that actual injection is being recovered, not an "echo" of the
% injection
% verify time shifts with an injection analysis
% 

% 1. Compute efficiency at fixed FAR (3 sigma?) as function of echo
%    parameters.
% 2. Verify that removing inspiral signal does not affect results.
% 3. Perform  background scan.
% 4. Repeat 1-3 for various different choices of analysis parameters (FFT
%    length, overlap fractiob, number of foldings, whitened vs. overwhitened,
%    antenna response weighting, ... etc.  

% [(Fp-iFc)d] [(Fp+iFc)d*]
% = [Fp.d - iFc.d] [Fp.d* + i Fc.d*]
% = Fp.d Fp.d* - iFc.d Fp.d* + iFp.d Fc.d* + Fc.d Fc.d*
% = FpT.d Fp.dT - iFcT.d Fp.dT + iFpT.d Fc.dT + FcT.d Fc.dT
% = dT [ Fp FpT + Fc FcT - i Fp FcT + i Fc FpT ] d

% tf.trainingMode = 'search';  %-- whiten on full data set.
% Try veto training, avoiding central portion of data set?


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Assign analysis parameters.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Select trigger to analyse.
% trigger = 'o1hwi';
trigger = 'gw170817';
% ---- Make plots? Skip un-necesary plots and analysis steps if false.
makePlots = true;
% ---- Save results? 
saveResults = true;

% -------------------------------------------------------------------------
% ---- Analysis parameters. These are stored in the struct 'analysis'. It
%      has the following fields:
%          triggerName   String. Name of the event.
%          triggerTime   Scalar. GPS time [s] of the event.
%          phi           Scalar. Azimuthal coordinate [rad] of the event in
%                        Earth-fixed coordinates.  
%          theta         Scalar. Polar coordinate [rad] of the event in
%                        Earth-fixed coordinates.  
% ---- Trigger time and sky position in Earth-fixed coordinates. 
switch lower(trigger)
    case 'o1hwi'
        % ---- Time of a BNS HWI in O1. Properties taken from SimInspiral table
        %      at https://gracedb.ligo.org/events/view/H217748
        %      Note that in this table "longitude" is ra in radians, "latitude"
        %      is declination in radians. 
        %      See also https://losc.ligo.org/archive/O1_16KHZ/ and the HWI
        %      logs
        %      https://losc.ligo.org/s/injections/o1/O1_H1_injections.txt,
        %      https://losc.ligo.org/s/injections/o1/O1_L1_injections.txt.    
        analysis.triggerName = 'o1hwi';
        analysis.triggerTime = 1137215767.99;
        longitude = 0.9103; %--ra [rad]
        latitude  = 0.0072; %--dec [rad]
        [analysis.phi, analysis.theta] = radectoearth(longitude*180/pi, ...
            latitude*180/pi,analysis.triggerTime);
        clear longitude latitude
        % polarization = 2.5662;
        % inclination = 0.0204;
        %
        % % ---- Properties in original xml file, where ra is offset by the Earth's
        % %      rotation, between the timestamp in the file and the actual the
        % %      injection time, to keep the same Earth-fixed position. Why CBC
        % %      doesn't just use Earth-fixed coordinates is a mystery for wiser
        % %      minds to unravel.
        % end_time1 = 1135135687
        % longitude1 = 2.478367e-02;
        % latitude1 = 7.155850e-03;
        % [phi, theta] = radectoearth(longitude1*180/pi,latitude1*180/pi,end_time1)
    case 'gw170817'
        % ---- GW170817: These were computed using GPS 1187008882.445709865
        %      (https://gracedb.ligo.org/events/view/G298048) and
        %      (ra,dec) = (197.4503, -23.3815) deg, converted from 
        %      "RA: 13:09:48.09 Dec: -23:22:53.4.6" 
        %      (https://gw-astronomy.org/circs/21531.lvc3).
        analysis.triggerName = 'gw170817';
        analysis.triggerTime = 1187008882.445709865;
        analysis.phi         = 0.7169;
        analysis.theta       = 1.9789;
        % H,L antenna responses: Fp =    0.0796    0.1312
        %                        Fc =   -0.8855    0.7390
    otherwise
        error(['Trigger ' trigger ' not recognised.']);
end

% ---- Analysis type.
% analysis.type = 'background';
% analysis.type = 'simulation'; calibration = true;
analysis.type = 'simulation'; calibration = false;
% analysis.type = 'openbox'; 

% ---- Minimum number of background trials for off-source runs.
analysis.NtrialMin = 100000;
% ---- Number of injections at each hrss amplitude for simulation runs.
analysis.Ninj = 50;
% ---- Set of hrss amplitudes to use for echo injections in simulation runs.
% analysis.hrss_inj = 10.^[-22.5:0.5:-21.0];
% analysis.hrss_inj = 10.^[-22.0:0.5:-20.5];
analysis.hrss_inj = 10.^[-22.0:0.25:-20.0]
% analysis.hrss_inj = 1e-21;

% ---- Minimum and maximum time between echoes - fixed by the CBC component
%      masses.
analysis.dt_max = 0.0159;
analysis.dt_min = 0.0109;
% ---- On-source time-frequency window to search for a signal.
analysis.onsource_t = [0,1];       %-- [s] relative to trigger time
analysis.onsource_f = [1/analysis.dt_max,1/analysis.dt_min];     %-- [Hz]
% ---- Threshold on |cross-correlation| and FAP for signal detection.
analysis.fap_threshold = 0.001;
analysis.cc_threshold  = 1.2018; %-- Gaussian noise - measured from running this script ...
% analysis.cc_threshold  = 1.665; %-- measured from running this script ...
% analysis.fap_threshold = 1.6e-5;
% analysis.cc_threshold  = 2.12654; %-- BNS event, measured from running this script ...

% -------------------------------------------------------------------------
% ---- Fixed injection parameters. These are stored in the struct 'inj'.

% ---- Option to over-ride the parameters below and recycle an existing
%      injection file. This is useful for, eg, debugging & optimisation. In
%      this case must supply a base name for the files <basename>.txt and
%      <basename>.mat (which must already exist).
reUseInjections = false;
% reUseInjections = true; injectionFileBaseName = 'echo_injection';

% ---- In the script, all of the numerical parameters below will be
%      expanded to vectors with one element per injection to be performed.
% ---- Echo parameters. See xmakewaveform for definitions.
if true
    % ---- Echo SGs.
    inj.echoType = 'echo-sg';
    % ---- Amplitude dampening factor.
    % ---- Q factor of echo.
    % ---- Central frequency of echo.
    %inj.A = -0.9;
    %inj.Q = 3;
    %inj.f0 = 1000;
    %
    inj.A = 0.9;
    inj.Q = 3;
    inj.f0 = 1000;
    % 
    %inj.A = -0.1;
    %inj.Q = 3;
    %inj.f0 = 1000;
    %
    %inj.A = -0.9;
    %inj.Q = 3;
    %inj.f0 = 2000;    
else
    % ---- Echo deltas.
    inj.echoType = 'echo-delta';
    % ---- Amplitude dampening factor.
    inj.A = -0.9;
end
%
% % ---- Gaussian limit. Keep the same envelope duration as for the Q=3,
% %      f0=1000Hz case (arbitrary choice).
% %-- hrss50 ~ 10^(-21.5)
% tau_def = 3 / (2^0.5 * pi * 1000);
% inj.f0 = 1;
% inj.Q = tau_def * (2^0.5 * pi * inj.f0);
% clear tau_def
% ---- Inspiral parameters (see 1710.05832).
inj.inspiralType = 'inspiralsmooth';
inj.mass1 = 1.36;
inj.mass2 = 1.36;
inj.distance = 40;

% -------------------------------------------------------------------------
% ---- Time-frequency analysis parameters. These are stored in the struct
%      'tf'.
% ---- Segment duration.
tf.T = 128;%512;
% ---- Analysis sampling rate.
tf.fs = 8192;
% ---- FFT duration [s].
tf.Tfft = 1;
% ---- FFT overlap fraction. Must be of the form (N-1)/N where N is a
%      positive integer.
tf.overlap = 15/16;
% ---- Parameters for xcondition.
tf.overwhitening = true;
tf.whiteningTime = 2;
tf.verboseFlag = 0;
tf.trainingMode = 'search';  %-- whiten on full data set.
% ---- Derived parameter; verify this later.
transientTime = 4*tf.whiteningTime;
% ---- Frequency range of the analysis. Simulated data will follow the
%      desired PSD over this range (only).
tf.fmin = 10;
tf.fmax = 4096;
% ---- Number of frequency bins to combine (fold) in search of echo signal.
tf.Nfold = 10;

% -------------------------------------------------------------------------
% ---- Specify data type and properties.
%
% ---- Time interval to analyse: We analyse real data starting from
%      analysis.startTime in all cases (set below). 
%      - Openbox runs analyse only a single block.
%      - Background runs process additional blocks until the requested 
%        number of background trials have been accumulated.
%      - Simulation runs distribute injections over the interval
%        [analysis.startTime, analysis.startTime+analysis.duration] and
%        analyse all the blocks needed to cover this interval. 
%      The start time must be made late enough after the start of data
%      availability to allow for filter transients to be dropped.
%
network.dataType = 'simulated';
% network.dataType = 'real';
switch network.dataType
    case 'simulated'
        % ---- Detector network.
        network.detector = {'H1','L1'};
        % ---- Design PSDs of each detector in the network. Used to
        %      generate simulated data.
        network.spectrum = {'aligo','aligo'};
        % ---- Data files. Empty for simulated data.
        dataDir = '/Users/psutton/Desktop/';
        network.dataFile = {[],[]};
        % ---- For simulated data these values still need to be defined,
        %      but the values should be irrelevant.
        analysis.startTime = floor(analysis.triggerTime) + 100; 
        analysis.duration = 100;
        % ---- Sampling rates of each detector in the network.
        network.fs = [16384,16384];
    case 'real'
        % ---- Detector network.
        network.detector = {'H1','L1'};
        % ---- Design PSDs of each detector in the network. Used for plots
        %      only.
        network.spectrum = {'aligo','aligo'};
        dataDir = '/Users/psutton/Desktop/URGH/echo_analysis/data/';
        switch analysis.triggerName
            case 'gw170817';
                % ---- Use data files for GW170817. 
                network.dataFile = {[dataDir 'H-H1_LOSC_CLN_16_V1-1187007040-2048.hdf5'],...
                                    [dataDir 'L-L1_LOSC_CLN_16_V1-1187007040-2048.hdf5']};
                % ---- Start time for simulation and background runs. The
                %      event is 1842 sec into the file. So use data from
                %      before the event.                                 
                analysis.startTime = floor(analysis.triggerTime) - 1800; 
                % ---- Only used for injections ... see below.
                analysis.duration = 100;
            case 'o1hwi';
                % ---- Use data files for O1 HWI. 
                network.dataFile = {[dataDir 'H-H1_LOSC_16_V1-1137213440-4096.hdf5'],...
                                    [dataDir 'L-L1_LOSC_16_V1-1137213440-4096.hdf5']};
                % ---- Start time for simulation and background runs. The
                %      injection is 2328 sec into the file. There is also a
                %      gap in the L1 data from roughly 1700-2200 sec. So
                %      use data after the HWI.
                analysis.startTime = floor(analysis.triggerTime) + 100; 
                % ---- Only used for injections ... see below.
                analysis.duration = 100;
            end
        % ---- Sampling rates of each detector in the network. Must match
        %      the data in the HDF5 files above.
        %      FIND OUT HOW TO READ THIS FROM THE HDF5 FILES.
        network.fs = [16384,16384];
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Compute derived parameters.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Number of detectors.
Ndet = length(network.detector);
if Ndet ~= 2
    error('This script works only for the two-detector case.');
end

% ---- FFT length of conditioned data.
Nfft = tf.Tfft * tf.fs;

% ---- Sky position in Earth-fixed coordinates.
phi   = analysis.phi;
theta = analysis.theta;

% ---- Sanity checks.
if strcmp(inj.echoType(end-1:end),'sg') & ((1 + 1/inj.Q) * inj.f0 > 0.75 * (tf.fs/2))
    warning('Echo maximum frequency too high to be sampled properly.');
end

% ---- Reset start time to match trigger time if we're doing an open-box
%      analysis.
if strcmpi(analysis.type,'openbox')
    % ---- Put the trigger 15 sec into the analysis time.
    analysis.startTime   = floor(analysis.triggerTime) - 15; 
end
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Assign storage.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Determine sizes and assign storage based on analysis type.
switch analysis.type
    case 'background'
        % ---- Number of zero-lag background trials we can fit into each
        %      block of data processed. 
        % ---- The on-source window usually won't start on an integer GPS
        %      second boundary, so we mimic this in the background trials.
        %      The first background trial starts in the [0,1) second
        %      interval after the filter transient period ends.
        % ---- Non-integer part of the start time of the true on-source window.
        TstartNonInt = mod(analysis.triggerTime + analysis.onsource_t(1), 1);
        Ttrial = [TstartNonInt:diff(analysis.onsource_t):(tf.T-2*transientTime-diff(analysis.onsource_t))]';
        Ntrial = length(Ttrial);
        % ---- Number of time-slide trials per segment of data processed =
        %      duration of segment / FFT duration for the two-detector
        %      case. This includes the zero-lag background trial.
        warning('Check the next line! Does it account for TstartNonInt?');
        Nslides = (tf.T-2*transientTime) / tf.Tfft;
        % ---- Total number of trials we can extract from a single 4096-sec
        %      block with 16 sec transient periods, only shifting within a
        %      segment, with 1-second shifts (approx):
        %        T = 2.^[6:12]';
        %        N = (T-2*16-1) .* (T-2*16) * 4096 ./ (T-2*16);
        %          = (T-2*16-1) * 4096;
        %      1024 sec segments are enough for 4 million trials (5 sigma).        
        % ---- Number of segments we need to analyse to get requested
        %      background. No check is made that we have enough segments
        %      when analysing real data; in that case an error will occur
        %      when we try to read past the end of the input data files.
        Nblock = ceil(analysis.NtrialMin/(Ntrial*Nslides));
        %
        NbackTot = Ntrial*Nslides*Nblock;
        maxVal            = zeros(NbackTot,1);
        maxVal2           = zeros(NbackTot,1);
        maxT              = zeros(NbackTot,1);
        maxF              = zeros(NbackTot,1);
    case 'simulation'
        NinjTot = length(analysis.hrss_inj)*analysis.Ninj;
        Nblock  = ceil(analysis.duration/(tf.T-2*transientTime));
        %
        maxVal            = zeros(NinjTot,1);
        maxVal2           = zeros(NinjTot,1);
        maxT              = zeros(NinjTot,1);
        maxF              = zeros(NinjTot,1);
        ToffsetArray      = zeros(NinjTot,1);
        dtArray           = zeros(NinjTot,1);
        polarizationArray = zeros(NinjTot,1);
    case 'openbox'
        Nblock = 1;
        %
        maxVal  = zeros(1,1);
        maxVal2 = zeros(1,1);
        maxT    = zeros(1,1);
        maxF    = zeros(1,1);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Construct injection file (if simulation run).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmpi(analysis.type,'simulation')

    if reUseInjections
        
        disp(['Re-using injections from ' injectionFileBaseName '.txt']);

        % ---- Read pre-existing injection data.
        load([injectionFileBaseName '.mat']);
        
        % ---- Assign variables needed later in this script.
        injectionFileName = [injectionFileBaseName '.txt'];
        
    else
        
        % ---- Assign random injection parameters.
        disp('Generating random injection parameters.');

        % ---- Inspiral coalescence times in GPS seconds.
        inj.gps = sort(analysis.startTime + rand(NinjTot,1) * analysis.duration);
        % ---- Time after inspiral coalescence time of first echo [s].
        inj.Toffset = analysis.onsource_t(1) + rand(NinjTot,1) * diff(analysis.onsource_t);
        % ---- Delay between subsequent echoes [s]. 
        inj.dt = analysis.dt_min + rand(NinjTot,1) * (analysis.dt_max - analysis.dt_min);
        % ---- Inclination of binary system. Following GW170817, restrict to
        %      |incl|<28deg (from using the optical counterpart). MAGIC NUMBER. 
        inj.ciota = -1 + rand(NinjTot,1) * (cosd(180-28)+1);
        % ---- Polarisation angle. Value will be relevant for Q<<2.
        inj.polarization = pi*rand(NinjTot,1); 
        % ---- Echo decay factor.
        inj.A = inj.A * ones(NinjTot,1);
        if strcmpi(inj.echoType,'echo-sg')
            % ---- Echo quality factor.
            inj.Q = inj.Q * ones(NinjTot,1);
            % ---- Echo central frequency factor.
            inj.f0 = inj.f0 * ones(NinjTot,1);
        end
        % ---- Inspiral mass parameters.
        inj.mass1 = inj.mass1 * ones(NinjTot,1);
        inj.mass2 = inj.mass2 * ones(NinjTot,1);
        % ---- Source distance [Mpc].
        inj.distance = inj.distance * ones(NinjTot,1);

        % ---- Determine injection amplitude and number.
        tmp = ones(analysis.Ninj,1) * analysis.hrss_inj(:)';
        inj.hrss = tmp(:);

        % ---- Construct injection log file. Note that the file will contain
        %      2*NinjTot injections, as each echo is acccompanied by an
        %      inspiral. For convenience we will list all the echoes first, then
        %      the corresponding inspirals in the same order.
        injectionFileName = 'echo_injection.txt';
        peakTime = [inj.gps+inj.Toffset; inj.gps];
        position = [theta, phi];          
        % ---- Construct signal cell array in a for loop for simplicity.
        for ii=1:NinjTot
            signal{ii,1} = inj.echoType;
            switch inj.echoType
                case 'echo-delta'
                    signal{ii,2} = [num2str(inj.hrss(ii)) '~' num2str(inj.dt(ii)) ...
                                '~' num2str(inj.A(ii))];
                case 'echo-sg'
                    signal{ii,2} = [num2str(inj.hrss(ii)) '~' num2str(inj.Q(ii)) ...
                                '~' num2str(inj.f0(ii))   '~' num2str(inj.dt(ii)) ...
                                '~' num2str(inj.A(ii))    '~' num2str(inj.ciota(ii))];
            end
            signal{NinjTot+ii,1} = inj.inspiralType;
            signal{NinjTot+ii,2} = [num2str(inj.mass1(ii)) '~' num2str(inj.mass2(ii)) ...
                                '~' num2str(inj.ciota(ii)) '~' num2str(inj.distance(ii))]; 
        end
        xmakegwbinjectionfile(injectionFileName, peakTime, 0, position, ...
            inj.polarization, signal, 'injectionMode', 'loop');
        save('echo_injection.mat','inj');
        clear peakTime position signal

    end
    
    % ---- Record the random echo parameters in vectors. These are
    %      redundant with the injection log file, but will be handy later.
    ToffsetArray      = inj.Toffset;
    dtArray           = inj.dt;
    inclinationArray  = inj.ciota;
    polarizationArray = inj.polarization;

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Loop over data segments.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Number of injections done so far.
icount = 0; 

for iblock = 1:Nblock
    
    % ---- Report progress periodically.
    if (Nblock>1) & any(iblock==round(Nblock*[1:10]/10))
        disp(['Processing block ' num2str(iblock) ' of ' num2str(Nblock) ...
            ' (' num2str(round(iblock/Nblock*100)) '% done)']);
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   Read / create data.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- Read or generate detector data ("noise").
    detectorData = cell(Ndet,1);
    % ---- blockStart = time at which we start reading data. 
    blockStart = analysis.startTime - transientTime + (iblock-1)*(tf.T - 2*transientTime);
    switch network.dataType
        case 'simulated'
            % ---- Make a fresh batch of simulated noise data for each detector.
            for ii=1:Ndet
                detectorData{ii} = simulateddetectornoise(network.spectrum{ii},tf.T, ...
                    network.fs(ii),tf.fmin,tf.fmax);
            end
        case 'real'
            for ii=1:Ndet
                % ---- Read the next block of data from files.
                % ---- GPS start time of the HDF5 data.
                fileStart = double(h5read(network.dataFile{ii},'/meta/GPSstart'));
                % ---- Start index and number of samples to read.. 
                startIdx = (blockStart-fileStart)*network.fs(ii) + 1;
                countIdx = tf.T * network.fs(ii);
                detectorData{ii} = h5read(network.dataFile{ii},'/strain/Strain',startIdx,countIdx);
            end
    end

    % ---- Inner loop over injections or time lags for this data segment.
    switch analysis.type
        case 'background'
            Nloop = Nslides;
        case 'simulation'
            % ---- Find all the echoes in this block. If there are none
            %      then the "for iloop = 1:Nloop" loop below is skipped.
            injIdx = find(inj.gps+inj.Toffset>=(blockStart+transientTime) & ...
                          inj.gps+inj.Toffset< (blockStart+tf.T-transientTime));
            Nloop = length(injIdx);
        case 'openbox'
            Nloop = 1;
    end
    
    for iloop = 1:Nloop
        
        % ---- Make injection data, if desired.
        if strcmpi(analysis.type,'simulation')
            
            % ---- Make injection data. Don't forget to add the inspiral
            %      along with the echo.
            [injectionData, inj.gps_s, inj.gps_ns, inj.phi, inj.theta, inj.psi] ...
                = xinjectsignal(blockStart,tf.T,network.detector,network.fs,injectionFileName,0,injIdx(iloop)+[0,NinjTot]);
            % ---- Add injection to background noise.
            for ii=1:Ndet
                data{ii} = detectorData{ii} + injectionData{ii};
            end
            % ---- Optionally plot input timeseries for first detector.
            if (makePlots & iblock*iloop==1) h1 = plotinputtimeseries(data{1},injectionData{1},network.fs(1),network.detector{1},analysis.startTime); end
            % ---- Optionally plot injection and noise spectra for first detector.
            if (makePlots & iblock*iloop==1) h1b = plotnoiseweightedinjection(injectionData{1},inj.gps+inj.Toffset-blockStart,network.fs(1),tf,network.spectrum{1},[network.detector{1} ' noise-weighted injection spectrum']); end

        elseif iloop==1

            % ---- Only need to do this the first time for background,
            %      openbox analyses.
            data = detectorData;

        end

        % ---- Optional bit.
        if makePlots & iblock*iloop==1
            % ---- Compute input spectrograms.
            for ii=1:Ndet
                Nfft_temp = tf.Tfft*network.fs(ii);
                [SpecStrain{ii}, FSpecStrain, TSpecStrain] = spectrogram( ...
                    data{ii},hann(Nfft_temp),Nfft_temp/2,Nfft_temp,network.fs(ii));
            end
            % ---- Plot spectrogram for first detector.
            h2 = plotspectrogram(TSpecStrain,FSpecStrain,SpecStrain{1},[network.detector{1} ' strain data']);
        end


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   Condition & time-shift data.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Only need to condition and time-shift (for sky position) the
        %      data the first time through the inner loop during a
        %      background analysis. 
        if strcmpi(analysis.type,'background') & iloop>1
            
            % ---- No need to redo conditioning.
            ;
            
        else

            gateList = cell(Ndet,1);
            % ---- Condition full data (noise + any injection).
            warning off
            [wdataFull, amplitudeSpectra, analysisLength, transientLength, ...
                  normalizationFactor] = xcondition(data, network.fs, ...
                  tf.fmin, tf.Tfft, tf.whiteningTime, tf.fs, ...
                  tf.verboseFlag, tf.trainingMode, gateList, tf.overwhitening);
            warning on
            ASDfull = amplitudeSpectra;
            FSpecCond = [0:1/tf.Tfft:tf.fs/2];

            % ---- Apply time shifts to correct for the sky location of the
            %      source. 
            % ---- We always hold the injection at a fixed position in
            %      Earth-based coordinates, so we only need to compute the time
            %      shift for the sky position once.
            if iloop==1
                % ---- Compute time shifts for signal sky position.
                timeShifts = computeTimeShifts(network.detector, [theta, phi]);
            end
            % ---- Approximate shifts as an integer number of samples. Note
            %      that large time shift means signal arrives later, so we
            %      apply the shift with a negative sign.
            timeShiftSamples = round(timeShifts * tf.fs);
            for ii=1:Ndet
                wdataFull(:,ii)  = circshift(wdataFull(:,ii),-timeShiftSamples(ii));  
            end
            % ---- Drop start and end of data stream to avoid possible
            %      filter transients.
            wdataFull = wdataFull((transientLength+1):(end-transientLength),:);
            
            % ---- Compute whitened spectrogram for each detector.
            for ii=1:Ndet
                [SpecCond{ii}, FSpecCond, TSpecCond] = spectrogram( wdataFull(:,ii), hann(Nfft), Nfft*tf.overlap, Nfft, tf.fs );
%                 [SpecCond{ii}, FSpecCond, TSpecCond] = spectrogram( wdataFull(:,ii), tukeywin(Nfft,1/6), Nfft*tf.overlap, Nfft, tf.fs );
            end
            

            % ---- Apply sub-sample time shifts as a phase delay in the 
            %      Fourier domain. (These can be important for
            %      high-frequency signals.)
            numberOfTimeBins = size(SpecCond{1},2);
            % ---- Note: verified empirically (and theoretically) that sign
            %      of time shift phase is +i to get large
            %      |cross-correlation| for injections.  
            for ii=1:Ndet
                timeShiftPhases = exp(sqrt(-1) * 2 * pi * FSpecCond * (timeShifts(ii)-timeShiftSamples(ii)/tf.fs));
                SpecCond{ii} = SpecCond{ii} .* repmat(timeShiftPhases,[1,numberOfTimeBins]);  
            end
            
            % ---- Optional bit: condition noise alone (without any
            %      injection). Useful to see if injection has affected the
            %      whitening significantly.
            if makePlots & iblock*iloop==1
                [wdataNoise, amplitudeSpectra, analysisLength, transientLength, ...
                      normalizationFactor] = xcondition(detectorData, network.fs, ...
                      tf.fmin, tf.Tfft, tf.whiteningTime, tf.fs, ...
                      tf.verboseFlag, tf.trainingMode, gateList, tf.overwhitening);
                ASDnoise = amplitudeSpectra;
                wdataNoise = wdataNoise((transientLength+1):(end-transientLength),:);
            end

        end
        
        % ---- Sanity check - we should never trigger this error condition.
        if iblock==1
            if transientTime ~= transientLength/tf.fs
                error('Assumed transientTime does not match output of xcondition.');
            end
        end

        % ---- Optional bits.
        if makePlots & iblock*iloop==1
            % ---- Compute noise spectra of conditioned data for each detector.
            for ii=1:Ndet
                [ASDwdataNoise(:,ii), Fasd] = medianmeanaveragespectrum( ...
                    wdataNoise(:,ii),tf.fs,Nfft);
            end
            % ---- Plot noise spectra of input data & conditioned data for each detector.
            h3 = plotspectra(FSpecCond,ASDnoise{1},network.spectrum,network.detector);
            h4 = plotowspectra(Fasd,ASDwdataNoise,network.spectrum,network.detector);
            % ---- Plot whitened spectrogram for each detector.
            for ii=1:Ndet
                h0 = plotspectrogram(TSpecCond,FSpecCond,SpecCond{ii},[network.detector{ii} ' conditioned data']);
            end
        end

        % % ---- Sample code to plot injections as a sanity check on time shifts, etc.
        % t = [0:1/tf.fs:(size(wdataFull,1)-1)/tf.fs]' + 8;
        % figure; plot(t,wdataFull(:,1))
        % grid on; hold on
        % plot(t,-wdataFull(:,2)) %-- minus sign for easier comparison
        % % ---- overlay injection data from first detector, with time shift to COE
        % t = [0:1/network.fs(1):(length(injectionData{1})-1)/network.fs(1)]';
        % plot(t-timeShifts(1),injectionData{1}*1e19)

        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   Combine data streams.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % % ---- Compute antenna responses for signal sky position.
        % [Fp, Fc, Fb] = antennaPatterns(network.detector, [theta, phi]);
        % 
        % % ---- Compute DPF antenna responses.
        % [FpDP, FcDP, psiDP] = convertToDominantPolarizationFrame(Fp,Fc);

        % ---- Cross correlate: combine spectrograms across detectors.
        if strcmpi(analysis.type,'background')
            % ---- Apply time slide for a background analysis. The
            %      circshift command below shifts the spectrogram by N
            %      columns, where the fractional FFT overlap is (N-1)/N.
            %      This is exactly the shift required so that each
            %      consecutive shift involves entirely new data not used in
            %      the previous shift.
            Scomb = real(SpecCond{1}.*conj(circshift(SpecCond{2},[0,(iloop-1)/(1-tf.overlap)])));
        else
            Scomb = real(SpecCond{1}.*conj(SpecCond{2}));
        end
        
        % ---- Fold over frequencies.
        Nfreq = floor(length(FSpecCond)/tf.Nfold);
        Sfold = zeros(Nfreq,size(Scomb,2));
        Sfold2 = zeros(Nfreq,size(Scomb,2));
        for jj=1:Nfreq
            idx = jj*[1:tf.Nfold];
            Sfold(jj,:)  = sum(Scomb(idx,:),1);
            Sfold2(jj,:) = sum(Scomb(idx,:).^2,1);
        end
        Sfold2 = Sfold2/tf.Nfold - (Sfold/tf.Nfold).^2;
        Sfold  = Sfold/tf.Nfold;
        
        % ---- Optional plots.
        if makePlots & iblock*iloop==1
            h10 = plotfoldspectrogram(TSpecCond,FSpecCond,-Scomb','(-1) x (H)x(L*) conditioned data');
            h11 = plotfoldspectrogram(TSpecCond,FSpecCond([1,Nfreq]),-Sfold','(-1) x folded conditioned data');
        end


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   Analyse folded spectrogram for a signal.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Extract desired cross-correlations.
        switch analysis.type

            case 'openbox'

                % ---- Book-keeping kludge: need to increment counter.
                icount = icount + 1;
        
                % ---- Find TF pixels with start time in on-source window.
                %      Note that with definition of on-source window in [,]
                %      form (instead of eg [,) form) a bin can be
                %      double-counted in background trials as belonging to
                %      two trials. But this will only happen if the trigger
                %      time happens to lie exactly on an FFT bin boundary.   
                warning('Could be double-counting some bins in background trials');
                warning('FFT bins starting before on-source window but overlapping are ignored. Avoids inspiral ...');
                binstart = blockStart + transientTime + TSpecCond - tf.Tfft/2;
                idx_t = find(binstart>=(analysis.triggerTime+analysis.onsource_t(1)) & ...
                             binstart<=(analysis.triggerTime+analysis.onsource_t(2)) );
                idx_f = find(FSpecCond>=analysis.onsource_f(1) & FSpecCond<=analysis.onsource_f(2));

                % ---- Extract largest value of -1 x correlation.
warning('Switch to using positive correlation measure?');
                onsource_map = Sfold(idx_f,idx_t);
                [val_max, idx_max] = min(onsource_map(:));
                [I,J] = ind2sub(size(onsource_map),idx_max); 
warning('Verify with injection that these TF indices are working.')
                maxVal(icount) = val_max;
                maxT(icount)   = binstart(idx_t(J))+tf.Tfft/2;
                maxF(icount)   = FSpecCond(idx_f(I));

                % ---- Extract matching Sfold2.
                onsource_map2 = Sfold2(idx_f,idx_t);
                maxVal2(icount) = onsource_map2(idx_max); 

            case 'simulation'

                % ---- Book-keeping kludge: need to increment counter.
                icount = icount + 1;
                
                % ---- Find TF pixels with start time in on-source window.
                %      Select TF bins overlapping the on-source interval
                %      that we would search based on the coalescence time.
                binstart = blockStart + transientTime + TSpecCond - tf.Tfft/2;
                idx_t = find(binstart>=(inj.gps(injIdx(iloop))+analysis.onsource_t(1)) & ...
                             binstart<=(inj.gps(injIdx(iloop))+analysis.onsource_t(2)) );
                idx_f = find(FSpecCond>=analysis.onsource_f(1) & FSpecCond<=analysis.onsource_f(2));

                if calibration

                    % ---- Forcibly select pixels centered on echo
                    %      injection in time and frequency.
                    t_echo      = inj.gps(injIdx(iloop)) + inj.Toffset(injIdx(iloop));
                    deltat_echo = 1/(1 - inj.A(injIdx(iloop))) *  inj.dt(injIdx(iloop));
                    % ---- Option 1:
                    %      For echo injections, approximately 90% of the signal
                    %      energy is accumulated in the first 1/(1-A) pulses.
                    %      Require overlap of FFT bin with this interval. 
                    %      (This can be wider than the on-ource window!)
%                     tmp = Coincidence2(t_echo,deltat_echo,binstart(:),tf.Tfft*ones(size(binstart(:))));
%                     idx_t_2 = tmp(:,end)
                    % ---- Option 2: 
                    %      Empirically determined values. For a quick run
                    %      of 10 injections these gave the same results as
                    %      the wider energy-based set above for 9 injections
                    %      out of 10. 
                    % idx_t_2 = find(abs(bincentre-t_echo) <= 0.2);
                    % ---- Option 3:
                    %      Pick the FFT bin that starts just at/before the
                    %      echo - this should give the strongest signal.
                    %     Empirically, it doesn't work - INVESTIGATE.
                    idx_t_2 = find(binstart<=t_echo,1,'last')
                    % ---- Frequency coincidence:
                    f_echo    = 1./(inj.dt(injIdx(iloop)));
                    idx_f_2 = find(abs(FSpecCond-f_echo) <= 2.0);

                    % ---- Apply extra coincidence criteria.
                    idx_t = intersect(idx_t,idx_t_2);
                    idx_f = intersect(idx_f,idx_f_2);
                    if isempty(idx_t)
                        idx_t = find(binstart>=(inj.gps(injIdx(iloop))+analysis.onsource_t(1)) & ...
                                     binstart<=(inj.gps(injIdx(iloop))+analysis.onsource_t(2)) );
                        disp(idx_t);
                        disp(idx_t_2);
                        error('Calibration coincidence criteria give empty time interval.');
                    end
                    if isempty(idx_f)
                        idx_f = find(FSpecCond>=analysis.onsource_f(1) & FSpecCond<=analysis.onsource_f(2));
                        disp(idx_f);
                        disp(idx_f_2);
                        error('Calibration coincidence criteria give empty time interval.');
                    end
                    
                end
                
                % ---- Extract largest value of -1 x correlation.
                onsource_map = Sfold(idx_f,idx_t);
                [val_max, idx_max] = min(onsource_map(:));
                [I,J] = ind2sub(size(onsource_map),idx_max); 
                maxVal(icount) = val_max;
                maxT(icount)   = binstart(idx_t(J))+tf.Tfft/2;
                maxF(icount)   = FSpecCond(idx_f(I));
                % ---- Extract matching Sfold2.
                onsource_map2 = Sfold2(idx_f,idx_t);
                maxVal2(icount) = onsource_map2(idx_max); 

            case 'background'

                % ---- This could be vectorised for speed by reshaping the map.
                for jj=1:Ntrial
                    % ---- Find TF pixels in on-source window. Note that
                    %      Ttrial=0 corresponds to the start of the analysed
                    %      data (after trandient filtering).  
                    idx_t = find(TSpecCond>=Ttrial(jj) & ...
                                 TSpecCond<=Ttrial(jj)+diff(analysis.onsource_t));
                    idx_f = find(FSpecCond>=analysis.onsource_f(1) & ...
                                 FSpecCond<=analysis.onsource_f(2));
                    % ---- Extract largest value of -1 x correlation.
                    onsource_map = Sfold(idx_f,idx_t);
                    [val_max, idx_max] = min(onsource_map(:));
                    [I,J] = ind2sub(size(onsource_map),idx_max); 
                    maxVal(icount+jj) = val_max;
                    maxT(icount+jj)   = TSpecCond(idx_t(J))-Ttrial(jj); %-- time in dummy on-source window
                    maxF(icount+jj)   = FSpecCond(idx_f(I));
                    % ---- Extract matching Sfold2.
                    onsource_map2 = Sfold2(idx_f,idx_t);
                    maxVal2(icount+jj) = onsource_map2(idx_max); 
                end
                icount = icount + Ntrial;

        end
        
    end %-- end of inner loop over injections / time slides

end %-- end of big loop over data blocks


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Post processing.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch analysis.type

    case 'openbox'

        disp(['Largest negative correlation in entire map: ' num2str(min(Sfold(:)))]);
        disp([' Largest negative correlation in on-source: ' num2str(val_max)]);
        disp(['                       bin centre GPS time: ' num2str(maxT) ' s']);
        disp(['              bin central time post-merger: ' num2str(maxT-analysis.triggerTime) ' s']);
        disp(['                     bin central frequency: ' num2str(FSpecCond(idx_f(I))) ' Hz']);
        if saveResults
            save onsource.mat
        end

    case 'simulation'

        cc = reshape(maxVal,analysis.Ninj,[])';
        eff = mean(abs(cc)>analysis.cc_threshold,2);
        hrss50 = exp(robustinterp1(eff,log(analysis.hrss_inj),0.50));
        figure; semilogx(analysis.hrss_inj,eff,'-s','markerfacecolor','b','linewidth',2); 
        grid on; hold on
        semilogy(hrss50,0.5,'rs','markerfacecolor','r');
        xlabel('h_{rss} [Hz^{-0.5}]')
        ylabel('efficiency')
        title(['efficiency ( |max statistic|>' num2str(analysis.cc_threshold) ' )'])
        set(gca,'fontsize',16)
        grid on
        if saveResults
            saveas(gcf,'efficiency.fig','fig');
            saveas(gcf,'efficiency.png','png');
            % ---- Clear largest variables to cut down on file size.
            clear Scomb Sfold  SpecCond data noise t timeShiftPhases wdataFull
            clear injectionData
            save efficiency.mat
        end

    case 'background'

        figure;
        smaxVal = sort(abs(maxVal));
        semilogy(smaxVal,[NbackTot:-1:1]'/NbackTot,'b-o', ...
            'markerfacecolor','b','linewidth',2)
        cc_threshold_empirical = interp1([NbackTot:-1:1]'/NbackTot,smaxVal,analysis.fap_threshold);
        hold on
        plot(cc_threshold_empirical,analysis.fap_threshold,'rs','markerfacecolor','r');
        xlabel('|max statistic|')
        ylabel('FAP')
        title('background distribution');
        set(gca,'fontsize',16)
        grid on
        if saveResults
            saveas(gcf,'background.fig','fig');
            saveas(gcf,'background.png','png');
            % ---- Clear largest variables to cut down on file size.
            clear Scomb Sfold  SpecCond data noise t timeShiftPhases wdataFull
            save background.mat
        end
        
end




% -------------------------------------------------------------------------
%    Helper subfunctions.
% -------------------------------------------------------------------------

% ---- These are used to avoid bulking up the main code with many lines of
%      plotting routines.

function hand = plotinputtimeseries(n,h,fs,ifo,start)
    hand = figure;
    t = [0:length(n)-1]'/fs;
    plot(t,[n,h])
    grid on; hold on
    title(['timeseries data for ' ifo])
    xlabel(['time [s] from GPS ' num2str(start) ])
    ylabel('strain')
    legend('detector data + injection data','injection data')
end

function hand = plotspectrogram(T,F,S,text)
    hand = figure;
    imagesc([T(1) T(end)],[F(1) F(end)], abs(S)); colorbar
    set(gca,'ydir','normal')
    xlabel('time [s]')
    ylabel('frequency [Hz]')
    title(text)
end

function hand = plotspectra(F,S,srd,ifo)
    hand = figure;
    loglog(F,S);
    grid on; hold on
    for ii=1:length(srd)
        warning off
        loglog(F(2:end),SRD(srd{ii},F(2:end)).^(0.5),'k--')
        warning on
    end
    xlabel('frequency [Hz]')
    ylabel('amplitude spectral density [Hz^-0.5]')
    title('input data')
    legend(ifo)
end

function hand = plotfoldspectrogram(T,F,S,text)
    hand = figure;
    imagesc([F(1) F(end)],[T(1) T(end)],S); colorbar
    set(gca,'ydir','normal')
    ylabel('time [s]')
    xlabel('frequency [Hz]')
    title(text)
    % ---- Set size and axis to match figure in the paper.
%     axis([0 200 -5 40]);
%     pos = get(gcf,'position');
%     pos(3) = 836;
%     pos(4) = 1024;
%     set(gcf,'position',pos)
    colormap jet
    set(gca,'fontsize',16)
end

function hand = plotowspectra(F,S,srd,ifo)
    hand = figure;
    loglog(F,S);
    grid on; hold on
    for ii=1:length(srd)
        % ---- Apply a rough rescaling to allow spectral shape to
        %      be compared to expectations. 
        x = median(S(:));
        warning off
        z = SRD(srd{ii},F(2:end)).^(-0.5) ;
        warning on
        y = median(z);
        loglog(F(2:end), x/y*z,'k--')
    end
    xlabel('frequency [Hz]')
    ylabel('amplitude spectral density [Hz^-0.5]')
    legend(ifo)
    title('conditioned data')
end

function hand = plotnoiseweightedinjection(d,t_cut,fs,tf,srd,text)
    % ---- Extract 1 FFT length of injection timeseries holding echo.
    Nsamp = floor(t_cut*fs);
    d(1:Nsamp)    = [];
    d(fs*tf.Tfft+1:end) = [];
%     d(1:round(t_cut*fs)) = 0; 
    ftmp = fft(d);
    ftmp = abs(ftmp(2:(floor(end/2)+1)));
    freq = [1/tf.Tfft:1/tf.Tfft:fs/2]';
    S = SRD(srd,freq);
    hand = figure;
    plot(freq,ftmp./S.^0.5)
    grid on; hold on
    xlabel('frequency [Hz]')
    ylabel('amplitude [arb]')
    title(text);    
    tmp = ftmp./S.^0.5;
    imax = floor(length(freq)/tf.Nfold);
    for jj=1:imax
        idx = jj*[1:tf.Nfold];
        tmp2(jj) = sum(tmp(idx));
    end
    plot(freq(1:imax),tmp2);
    legend('original','folded')
end
