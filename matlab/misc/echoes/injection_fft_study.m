
% ---- Investigate FTs of injections ...
fs = 4096;
T = 1;
N = T*fs;
t = [0:N-1]'/fs;
xmakewaveform('echo-sg')
[t,h] = xmakewaveform('echo-sg',[1, 1, 1000, 0.1, 0.99, 1],1,0.5,4096);
[t,h1] = xmakewaveform('echo-sg',[1, 1, 1000, 0.1, 0.99, 1],1,0.5,4096);
[t,hcomb] = xmakewaveform('echo-sg',[1, 1, 1000, 0.1, 0.99, 1],1,0.5,4096);
clear h h1
[t,h1] = xmakewaveform('sg',[1, 1, 1000],1,0.5,4096);
[t,hcomb] = xmakewaveform('echo-sg',[1, 1, 1000, 0.1, 0.99, 1],1,0.05,4096);
figure; plot(t,[hcomb,h1]); grid omn
grid on
fs = 16384;
[t,h1] = xmakewaveform('sg',[1, 1, 1000],1,0.5,fs);
[t,hcomb] = xmakewaveform('echo-sg',[1, 1, 1000, 0.1, 0.99, 1],1,0.05,fs);
plot(t,[hcomb,h1]); grid on
fh1 = fft(h1);
fhcomb = fft(hcomb);
length(fh1)
figure; plot(); grid on
f = [0:fs/2]';
Nf = length(f);
figure; plot(f,abs([fh1(1:Nf),fhcomb(1:Nf)]); grid on
figure; plot(f,abs([fh1(1:Nf),fhcomb(1:Nf)])); grid on
[t,hcomb] = xmakewaveform('echo-sg',[1, 1, 1000, 0.01, 0.99, 1],1,0.05,fs);
fhcomb = fft(hcomb);
figure; plot(f,abs([fh1(1:Nf),fhcomb(1:Nf)])); grid on
figure; plot(f,abs([6.3*fh1(1:Nf),fhcomb(1:Nf)])); grid on
[t,h1] = xmakewaveform('g',[1, 0.001],1,0.5,fs);
%[t,hcomb] = xmakewaveform('echo-delta',[1, 1, 1000, 0.01, 0.99, 1],1,0.05,fs);
xmakewaveform('echo-delta')
[t,hcomb] = xmakewaveform('echo-delta',[1, 0.01, 0.99],1,0.05,fs);
[t,h1] = xmakewaveform('g',[1, 0.0025],1,0.5,fs);
figure; plot([t,hcomb,h1]); grid on
figure; plot(t,[hcomb,h1]); grid on
fh1 = fft(h1);
fhcomb = fft(hcomb);
figure; plot(f,abs([fh1(1:Nf),fhcomb(1:Nf)])); grid on