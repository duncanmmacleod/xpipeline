function [d] = circles(ra_ctr_deg, dec_ctr_deg,sigma_err_deg,lineStyle)
        if nargin<4
            lineStyle = 'm-';
        end
        for jj=1:length(ra_ctr_deg)
            angles = [0:360];
            x = sigma_err_deg(jj) * cosd(angles) / cosd(dec_ctr_deg(jj)) + ra_ctr_deg(jj);
            y = sigma_err_deg(jj) * sind(angles) + dec_ctr_deg(jj);
            d = plot(x,y,lineStyle); hold on
        end
