function [frequencyBandIndices, elementIndices, skyPositionIndices] = ...
    xthreshold(skyPositions, ...
               nullEnergyMap, incoherentEnergyMap, totalEnergyMap, ...
               degreesOfFreedom, numberOfChannels, numberOfFalseEvents)
% XTHRESHOLD Threshold sky maps for statistically significant results
%
% XTHRESHOLD applies thresholds on null, incoherent, and total energies
% to identify statistically significant events.  The thresholds are
% selected to approximately yield the specified number of false events
% in each frequency band and element under test.
%
% If debugging is enables, XTHRESHOLD also displays scatter plots of
% null, incoherent, and total energy.  Thresholds of each quantity are
% displayed as dotted lines while events passing threshold are circled.
%
% usage:
%
%   [frequencyBandIndices, elementIndices, skyPositionIndices] = ...
%       xthreshold(skyPositions, ...
%                  nullEnergyMap, incoherentEnergyMap, totalEnergyMap, ...
%                  degreesOfFreedom, numberOfChannels, whiteNoiseFalseRate);
%
%   skyPositions          matrix of sky positions [radians]
%   nullEnergyMap         null energy sky map returned by XMAPSKY
%   incoherentEnergyMap   incoherent energy sky map returned by XMAPSKY
%   totalEnergyMap        total energy sky map returned by XMAPSKY
%   degreesOfFreedom      number of degrees of freedom for statistical tests
%   numberOfChannels      number of detectors under analysis
%   numberOfFalseEvents   desired number of false events in each element and
%                         frequency band
%
%   frequencyBandIndices  
%   elementIndices        
%   skyPositionIndices    
%
% The desired sky position should be provided as a two column matrix of the form
% [theta phi], where theta is the geocentric colatitude running from 0 at the
% North pole to pi at the South pole and phi is the geocentric longitude in
% Earth fixed coordinates with 0 on the prime meridian.
%
% The null, incoherent, and total energy sky maps are three dimensional arrays
% with the form
%
%   nullEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex)
%   incoherentEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex)
%   totalEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex).
%
% See also XPIPELINE, XREADDATA, XINJECTSIGNAL, XCONDITION, XTILESKY, and
% XMAPSKY.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id$

% check for sufficient command line arguments
error(nargchk(7, 7, nargin));

% number of elements
numberOfElements = size(nullEnergyMap, 2);

% number of frequency bands
numberOfFrequencyBands = size(nullEnergyMap, 1);

% number of sky positions
numberOfSkyPositions = size(skyPositions, 1);

% number of null sums
numberOfNullSums = numberOfChannels - 2;

% account for complex valued measurments
degreesOfFreedom = 2 * degreesOfFreedom;

% initialize result vectors
frequencyBandIndices = [];
elementIndices = [];
skyPositionIndices = [];

% begin loop over frequency bands
for frequencyBandNumber = 1: numberOfFrequencyBands
  
  % significance threshold for desired number of false events
  significanceThreshold = 1 - numberOfFalseEvents / numberOfSkyPositions;
  
  % corresponding null energy threshold
  % ***** this threshold actually determines a false dismissal probability
  % ***** and should probably be specified using a different significance
  % ***** threshold.
  nullEnergyThreshold = ...
      chi2inv(significanceThreshold, ...
              degreesOfFreedom(frequencyBandNumber) * ...
              numberOfNullSums) / 2;

  % corresponding incoherent energy threshold
  % ***** in general, the incoherent energy distribution is sky position
  % ***** dependent distributed.  this is just an rough one-size fits
  % ***** all guess at a threshold.
  incoherentEnergyThreshold = ...
      chi2inv(significanceThreshold, ...
              degreesOfFreedom(frequencyBandNumber) * ...
              (numberOfChannels - 1)) * ...
              ((numberOfChannels - 2) / (numberOfChannels - 1)) / 2;
  
  % corresponding total energy threshold
  totalEnergyThreshold = ...
      chi2inv(significanceThreshold, ...
              degreesOfFreedom(frequencyBandNumber) * ...
              numberOfChannels) / 2;

  % identify statistically significant sky positions in this band
  significantIndices = find(((nullEnergyMap(frequencyBandNumber, :, :) < ...
                              nullEnergyThreshold) & ...
                             (incoherentEnergyMap(frequencyBandNumber, :, :) > ...
                              incoherentEnergyThreshold)) | ...
                            ((nullEnergyMap(frequencyBandNumber, :, :) < ...
                              nullEnergyThreshold) & ...
                             (totalEnergyMap(frequencyBandNumber, :, :) > ...
                              totalEnergyThreshold)));
  significantIndices = significantIndices(:);

  % extract corresponding element, and sky position indices
  [ignore, frequencyBandElementIndices, frequencyBandSkyPositionIndices] = ...
      ind2sub([1 numberOfElements numberOfSkyPositions], significantIndices);

  % append indices of statistically significant events to results
  frequencyBandIndices = [frequencyBandIndices; frequencyBandNumber * ...
                          ones(size(significantIndices));];
  elementIndices = [elementIndices; frequencyBandElementIndices(:);];
  skyPositionIndices = [skyPositionIndices; frequencyBandSkyPositionIndices(:);];

% end loop over frequency bands
end

% return to calling function
return;
