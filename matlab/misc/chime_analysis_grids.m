% ---- Comments on each FRB: search and injection girds from this code vs.
%      Eric's.
%               Search
%                Grid   Comments
% FRB37888771 	 same   HLV, 1 island, no declination error
% FRB39054070    same   V only; ignore
% FRB41358073    same   V only; ignore
% FRB42435604    same   V only; ignore
% FRB47566582    same   HLV, 5 islands, dec error; 1./cosd(DEC) ~1.15 so uncertainties
%                       inflated by 15%. Both codes use same dominant island as the
%                       single grid point, same search grid. Injection distributions 
%                       offset because my code spreads them across all five islands 
%                       while Eric's uses only the dominant one (I think). Coverage 
%                       0.44deg ~ covers injections.
% FRB49473791    same   HL, 1 island, dec error; 1./cosd(DEC) ~1.5 so uncertainties
%                       inflated by ~50%. But these spreads (1 sigma = ~0.25deg vs. 0.17deg) 
%                       are negligible compared to the coverage of the single grid 
%                       point (0.75 deg).          
% FRB49511551    same   HLV, 1 island, dec error; 1./cosd(DEC) ~3.6 so uncertainties
%                       inflated by factor 3.6. Injections spread across ~+/-0.25 deg 
%                       instead of <+/-0.1 deg, but still comparable to the coverage of the 
%                       single grid point (0.27 deg).
% FRB50542754    same   L only; ignore
% FRB54295013    diff   HL, 2 islands, dec error but 1./cosd(DEC) ~ 1.006 so <1% error
%                       in spreads of injections - no problem. Search grids differ:
%                       since coverage ~0.7 deg >> 1 sigma values and separation of
%                       islands, a single grid point is sufficient.
% FRB56503596    diff   HLV, 5 islands, dec error; 1./cosd(DEC) ~ 6 so injections are
%                       spread over a much larger region. The grid is also wonky, probably 
%                       due to the bug in the earlier version of my code for eliminating 
%                       redundant grid points. Search grid reduced from 28->8 points.
% FRB56597950    same   HLV, 1 island, no dec error.

% ---- Hard-wired list of FRBs.
FRBlist = {'37888771','39054070','41358073','42435604','47566582', ...
           '49473791','49511551','50542754','54295013','56503596', ...
           '56597950'};

% ---- Database file of FRBs.
dataDir = '/Users/psutton/Documents/xpipeline/branches/scripts/input/O3-frb/sky_grids/FRB_GRID_CODE_DATA/';
dataFile = [dataDir 'reorganized_chime_data_with_corrected_dms_commas_removed.csv'];

% ---- Loop over desired FRBs and construct grid and injection files.
for ifrb = 1:length(FRBlist)

    % ---- Extract data required from database file.
    %      Crude but effective: grep out relevant line and columns of data file.
    name     = ['FRB' FRBlist{ifrb}];
    [status,result] = system(['grep ' FRBlist{ifrb} ' ' dataFile ' | sed -e ''s/\[//g'' -e ''s/\]//g'' |  awk -F, ''{print $9, $10, $11, $12, $18}'' ']);
    FRBdata  = str2num(result(1:end-1));
    RA       = FRBdata(1:5);
    RA_err   = FRBdata(6:10);
    DEC      = FRBdata(11:15);
    DEC_err  = FRBdata(16:20);
    gps      = num2str(FRBdata(21));    
    [status,result] = system(['grep ' FRBlist{ifrb} ' ' dataFile ' | sed -e ''s/\[//g'' -e ''s/\]//g'' |  awk -F, ''{print $19}'' ']);
    sites    = strrep(result(1:end-2),'1','~');
    % ---- The data file contains dummy "-1" for some angle values if there are
    %      less than 5 islands. Delete these.
    bad_idx = find(RA<0 | RA_err<0 | DEC<0 | DEC_err<0);
    RA(bad_idx)      = [];
    RA_err(bad_idx)  = [];
    DEC(bad_idx)     = [];
    DEC_err(bad_idx) = [];

    % ---- Derive sigma_deg.
    SIGMA_deg = max(RA_err .* cosd(DEC),DEC_err);
    
    % ---- Convert data to strings for input to xmakeskygrid.
    ra        = numorcell2tildedelimstr(RA);
    ra_err    = numorcell2tildedelimstr(RA_err);
    dec       = numorcell2tildedelimstr(DEC);
    dec_err   = numorcell2tildedelimstr(DEC_err);
    sigma_deg = numorcell2tildedelimstr(SIGMA_deg);

    % ---- Parameters common to search and injection grids.
    gridtype = 'circular';
    verbose = '0';

    % ---- Parameters for search grid.
    nSigma_search = '2';
    delay_tol_search = '125e-6';
    outputfile_search = [name '_grid_search.txt'];

    % ---- Parameters for injection "grid".
    N_inj = 1e4;                %-- number of injection locations
    nSigma_inj = '3.0';        %-- higher coverage
    delay_tol_inj = '125e-8';  %-- higher resolution
    outputfile_inj = [name '_grid_sim.txt'];

    % ---- Make search grid.
    [searchgrid.ra,searchgrid.dec,searchgrid.prob,searchgrid.area,coverage] = xmakeskygrid( ...
        ra,dec,gps,sigma_deg,nSigma_search,sites,delay_tol_search,'None',gridtype,verbose,'1');
    % ---- Write search grid file for X-Pipeline. Columns are ra [deg], dec [deg], prob, area.
    dlmwrite(outputfile_search,[searchgrid.ra,searchgrid.dec,searchgrid.prob,searchgrid.area], ...
        'delimiter',' ','precision','%7.5f');

    % ---- Make injection file by random sampling of very high resolution grid.
    [injgrid.ra,injgrid.dec,injgrid.prob] = xmakeskygrid(ra,dec,gps,sigma_deg, ...
        nSigma_inj,sites,delay_tol_inj,'None',gridtype,verbose);
    % ---- Random sampling of grid.
    idx = randsamp(N_inj,injgrid.prob);
    injgrid.ra  = injgrid.ra(idx);
    injgrid.dec = injgrid.dec(idx);
    clear injgrid.prob
    % ---- Write injection location file for X-Pipeline. Columns are ra [deg], dec [deg].
    dlmwrite(outputfile_inj,[injgrid.ra injgrid.dec],'delimiter',' ','precision','%7.5f');

    % ---- Make plot.
    figure; 
    plot(injgrid.ra,injgrid.dec,'g.');
    grid on; hold on;
    plot(searchgrid.ra,searchgrid.dec,'bo','MarkerSize',12,'MarkerFaceColor','b')
    grid on; hold on;
    plot(RA,DEC,'r.','MarkerSize',12)
    circles(RA,DEC,SIGMA_deg,'r-')
    circles(RA,DEC,str2num(nSigma_search)*SIGMA_deg,'b-')
    circles(RA,DEC,coverage,'m-')
    legend('injections','search grid','island centres','1-sigma radius','search radius','coverage')
    set(gca,'xdir','reverse')
    xlabel('RA [deg]')
    ylabel('Dec [deg]')
    title(name);
    saveas(gcf,[name '_inj_search_islands.png'],'png');

end



