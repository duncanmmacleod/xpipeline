
% ---- Directory containing the efficiency eff*.fig files.
% fileDir = '/home/benjaminbenainsley.ainsley/project1/auto_web/SN2023ixf_finalrun_closedbox_figfiles/';
fileDir = '/home/george.smith/project4/auto_web/PSR1809-1943_run_closedbox_figfiles/';

% ---- Select which sets of injections to process. Update the default distance D0 from the .ini file as needed.
if false
    fileNameBase = {'s15a2o05','s15a2o09','s15a3o15','s15-Yakunin','s15-Ott'}; 
    D0 = 1; %-- kpc
    units = 'kpc';
    outputName = 'numerical_s15';
elseif false
    fileNameBase = {'muellerL15-2','muellerL15-3','muellerN20-2','muellerW15-2','muellerW15-4'}; 
    D0 = 1; %-- kpc
    units = 'kpc';
    outputName = 'numerical_mueller';
elseif false
    fileNameBase = {'pirom10d0eta0d3fac0d2','pirom10d0eta0d6fac0d2','pirom5d0eta0d3fac0d2','pirom5d0eta0d6fac0d2'};
    D0 = 1000; %-- kpc
    units = 'kpc';
    outputName = 'piro';
elseif false
    fileNameBase = {'barm0d2l60r10f400t1000','barm0d2l60r10f400t100','barm0d2l60r10f800t100','barm1d0l60r10f400t1000','barm1d0l60r10f400t100','barm1d0l60r10f800t25'};
    D0 = 1000; %-- kpc
    units = 'kpc';
    outputName = 'bar';
elseif true 
    fileNameBase = {'ringdowne1','ringdowne2','sge70q9','sge90q9','sge145q9','sge290q9','sge650q9','sge1100q9','sge1600q9','sge1995q9','wnb100-200-11ms','wnb100-200-100ms','wnb100-1000-11ms','wnb100-1000-100ms'};
    % hrss = 1e-22;
    % units = 's^{0.5}';
    outputName = 'ad_hoc';
end

injectionScales = [0.0100,0.0147,0.0215,0.0316,0.0464,0.0681,0.1000,0.1468, ...
    0.2154,0.3162,0.4642,0.6813,1.0000,1.4678,2.1544,3.1623,4.6416,6.8129, ...
    10.0000]';

% ---- Set linestyle for efficiency plots.
lineStyle = {'b-o','g-s','r-+','c-*','m-o','y-s','k-+','b-*','g-o','r-s'}; 

% -------------------------------------------------------------------------

% ---- Extract efficiency vs injection scale for all waveforms from the .fig files.
eff = zeros(length(injectionScales),length(fileNameBase));
waveformListStr = '';
for ii=1:length(fileNameBase)
    open([fileDir 'eff_' lower(fileNameBase{ii}) '.fig']);
    [X,Y] = datafromplot(gca);
    close all
    eff(:,ii) = Y{end}(:);
    waveformListStr = [waveformListStr, '   ', lower(fileNameBase{ii})];
end
% ---- Some efficiencies may be small and negative (~-1e-5) for technical reasons. Reset those to 0.
eff = max(eff,zeros(size(eff)));

% ---- Write efficiency vs injection scale data to a plain text file.
effFileName = ['eff_vs_scale_' outputName '.txt'];
[fid, msg] = fopen(effFileName, 'w');
assert(fid ~= -1, 'Cannot open file for writing: %s\n%s\n', effFileName, msg);
fprintf(fid, '%s\n', ['# injection_scale', waveformListStr]);
fmt = ['%10.4f ', repmat('%10.3f', 1, length(fileNameBase)), '\n'];
fprintf(fid, fmt, [injectionScales(:) eff].');
fclose(fid);


% ---- If injections have a defined default distance, then compute efficiency vs distance.
if exist('D0')==1

    % ---- Convert injection scale to distance.
    distance = D0./injectionScales;

    % ---- Write efficiency vs distance data to a plain text file.
    effFileName = ['eff_vs_distance_' outputName '.txt'];
    [fid, msg] = fopen(effFileName, 'w');
    assert(fid ~= -1, 'Cannot open file for writing: %s\n%s\n', effFileName, msg);
    fprintf(fid, '%s\n', ['# distance_[' units ']', waveformListStr]);
    fmt = ['%10.4f ', repmat('%10.3f', 1, length(fileNameBase)), '\n'];
    fprintf(fid, fmt, (flipud([distance(:) eff])).');
    fclose(fid);

    % ---- Plot efficiency vs distance.
    h = figure;
    for ii=1:length(fileNameBase)
        semilogx(distance,eff(:,ii),lineStyle{ii},'linewidth',2)
        hold on
    end
    grid on
    xlabel(['distance [' units ']']);
    ylabel('efficiency');
    title(['efficiency vs. distance: ' strrep(outputName,'_','-')]);
    legend(fileNameBase,'Location','EastOutside');
    saveas(gcf,['public_html/' effFileName(1:end-3) 'png'],'png')

end

