function xoutput(triggerFileIdentifier, likelihoodFileIdentifier, ...
                 skyPositions, startTime, analysisTime, frequencyBands, ...
                 frequencyBandIndices, elementIndices, skyPositionIndices, ...
                 nullEnergyMap, incoherentEnergyMap, totalEnergyMap, ...
                 likelihoodNumerator, likelihoodDenominator, ...
                 meanTheta, meanPhi, mostLikelyTheta, mostLikelyPhi)
% XOUTPUT Write significant event and likelihood information to result files
% 
% XOUTPUT writes information on the sky position, time-frequency bounds, and
% energies of statistically significant events to ASCII text files.  Significant
% events are identified by indices returned by XTHRESHOLD for the measured sky
% maps returned by XMAPSKY.  XOUTPUT also writes information the Bayesian
% likelihood that a gravitational-wave burst is present in the data and Bayesian
% estimates of the sky position of such bursts to ASCII text files.  These
% Bayesian quantities are output for each time-frequency region under test.
%
% usage:
%
%   xoutput(triggerFileIdentifier, likelihoodFileIdentifier, ...
%           skyPositions, startTime, analysisTime, frequencyBands, ...
%           frequencyBandIndices, elementIndices, skyPositionIndices, ...
%           nullEnergyMap, incoherentEnergyMap, totalEnergyMap, ...
%           likelihoodNumerator, likelihoodDenominator, ...
%           meanTheta, meanPhi, mostLikelyTheta, mostLikelyPhi)
%
%   triggerFileIdentifier     .
%   likelihoodFileIdentifier  .
%   skyPositions              .
%   startTime                 gps start time of analysis element [gps seconds]
%   analysisTime              duration of analysis element [seconds] 
%   frequencyBands            .
%   frequencyBandIndices      .
%   elementIndices            .
%   skyPositionIndices        .
%   nullEnergyMap             .
%   incoherentEnergyMap       .
%   totalEnergyMap            .
%   likelihoodNumerator       .
%   likelihoodDenominator     .
%   meanTheta                 .
%   meanPhi                   .
%   mostLikelyTheta           .
%   mostLikelyPhi             .
%
% The resulting ASCII trigger file consists of 15 columns of whitepsace
% delimited text.  Individual lines reports the properties of sky positions
% identifed as statistically significant by XTRESHOLD.  The columns have the
% following meanings.
%
%   startTime                 gps start time of analysis element [gps seconds]
%   analysisTime              duration of analysis element [seconds] 
%   minimumFrequency          minimum frequency of analyzed band [Hz]
%   maximumFrequency          maximum frequency of analyzed band [Hz]
%   theta                     geocentric colatitudes [0, pi]
%   phi                       geocentric longitude [-pi, pi)
%   nullEnergy                .
%   incoherentEnergy          .
%   totalEnergy               .
%   logLikelihoodNumerator    .
%   logLikelihoodDenominator  .
%   meanTheta                 .
%   meanPhi                   .
%   mostLikelyTheta           .
%   mostLikelyPhi             .
%
% The resulting ASCII likelihood file consists of 10 columns of whitepsace
% delimited text.  Individual lines reports the likelihoods and parameter
% estimates for each time-frequency region under test.  The columns have the
% following meanings.
%
%   startTime                 gps start time of analysis element [gps seconds]
%   analysisTime              duration of analysis element [seconds]
%   minimumFrequency          minimum frequency of analyzed band [Hz]
%   maximumFrequency          maximum frequency of analyzed band [Hz]
%   loglikelihoodNumerator    .
%   loglikelihoodDenominator  .
%   meanTheta                 .
%   meanPhi                   .
%   mostLikelyTheta           .
%   mostLikelyPhi             .
%
% See also XREADDATA, XINJECTSIGNAL, XCONDITION, XTILESKY, XMAPSKY, and
% XINTERPRET, XTHRESHOLD, and XPIPELINE.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                        process command line arguments                        %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% validate number of command line arguments
error(nargchk(18, 18, nargin));

% force column vectors
skyPositionIndices = skyPositionIndices(:);
elementIndices = elementIndices(:);
frequencyBandIndices = frequencyBandIndices(:);

% determine sky map dimensions
numberOfFrequencyBands = size(nullEnergyMap, 1);
numberOfElements = size(nullEnergyMap, 2);
numberOfSkyPositions = size(nullEnergyMap, 3);

% determine number of triggers
numberOfTriggers = size(skyPositionIndices,1);

% output log likelihoods
likelihoodNumerator = log(likelihoodNumerator);
likelihoodDenominator = log(likelihoodDenominator);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                        output likelihood measurements                        %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% determine start and stop time of likelihood measurement
elementStartTime = startTime + ones(numberOfFrequencyBands, 1) * ...
                   (0 : numberOfElements - 1) * analysisTime / 2;
elementDuration = analysisTime * ones(size(elementStartTime));

% determine frequency range of likelihood measurements
minimumFrequency = frequencyBands(:, 1) * ones(1, numberOfElements);
maximumFrequency = frequencyBands(:, 2) * ones(1, numberOfElements);

% format of likelihood file
formatString = ['%#019.9f %#011.9f ' ...
                '%#09.4f %#09.4f ' ...
                '%#10.4e %#10.4e ' ...
                '%#06.4f %+#07.4f %#06.4f %+#07.4f' ...
                '\n'];

% construct array of likelihood measurements
outputValues = [elementStartTime(:) elementDuration(:) ...
                minimumFrequency(:) maximumFrequency(:) ...
                likelihoodNumerator(:) likelihoodDenominator(:) ...
                meanTheta(:) meanPhi(:) mostLikelyTheta(:) mostLikelyPhi(:)]';

% write triggers to trigger file
fprintf(likelihoodFileIdentifier, formatString, outputValues);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                               output triggers                                %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Nothing to output of numberOfTriggers < 1
if (numberOfTriggers < 1),
    return;
end

% extract sky position of triggers
skyPositions = skyPositions(skyPositionIndices, :);
theta = skyPositions(:, 1);
phi = skyPositions(:, 2);

% determine start and stop time of triggers
elementStartTime = startTime + (elementIndices - 1) * analysisTime / 2;
elementDuration = analysisTime * ones(size(elementStartTime));

% extract frequency range of triggers
frequencyBands = frequencyBands(frequencyBandIndices, :);
minimumFrequency = frequencyBands(:, 1);
maximumFrequency = frequencyBands(:, 2);

% extract null, incoherent, and total energy of triggers
energyMapIndices = ...
    sub2ind([numberOfFrequencyBands numberOfElements numberOfSkyPositions], ...
            frequencyBandIndices, elementIndices, skyPositionIndices);
nullEnergy = nullEnergyMap(energyMapIndices);
incoherentEnergy = incoherentEnergyMap(energyMapIndices);
totalEnergy = totalEnergyMap(energyMapIndices);

% extract likelihood ratio numerator and denomintor of triggers
likelihoodIndices = ...
    sub2ind([numberOfFrequencyBands numberOfElements], ...
            frequencyBandIndices, elementIndices);
likelihoodNumerator = likelihoodNumerator(likelihoodIndices);
likelihoodDenominator = likelihoodDenominator(likelihoodIndices);

% extract sky position estimates of triggers
estimateIndices = ...
    sub2ind([numberOfFrequencyBands numberOfElements], ...
            frequencyBandIndices, elementIndices);
meanTheta = meanTheta(estimateIndices);
meanPhi = meanPhi(estimateIndices);
mostLikelyTheta = mostLikelyTheta(estimateIndices);
mostLikelyPhi = mostLikelyPhi(estimateIndices);

% format of trigger file
formatString = ['%#019.9f %#011.9f ' ...
                '%#09.4f %#09.4f ' ...
                '%#06.4f %+#07.4f ' ...
                '%#10.4e %#10.4e %#10.4e ' ...
                '%#10.4e %#10.4e ' ...
                '%#06.4f %+#07.4f %#06.4f %+#07.4f' ...
                '\n'];

% construct array of trigger values
outputValues = [elementStartTime(:) elementDuration(:) ...
                minimumFrequency(:) maximumFrequency(:) ...
                theta(:) phi(:) ...
                nullEnergy(:) incoherentEnergy(:) totalEnergy(:) ...
                likelihoodNumerator(:) likelihoodDenominator(:) ...
                meanTheta(:) meanPhi(:) mostLikelyTheta(:) mostLikelyPhi(:)]';

% write triggers to trigger file
fprintf(triggerFileIdentifier, formatString, outputValues);

% return to calling function
return
