function [nullEnergyMap, incoherentEnergyMap, totalEnergyMap, degreesOfFreedom] = ...
    xfastmapsky(channelNames, conditionedData, sampleFrequency, ...
            amplitudeSpectra, skyPositions, elementLength, elementOffset, ...
            transientLength, frequencyBands, analysisMode, verboseFlag)
% XFASTMAPSKY Map gravitational-wave burst consistency as a function of sky position
% 
% XFASTMAPSKY generates a sky map describing the consistency of the signals observed
% in a network of three of more non-aligned detectors with the assumption that
% they are due to a gravitational-wave burst from a particular direction on the
% sky.  The sky map is generated for the requested list of sky positions and the
% requested analysis time scale and frequency bands.  To do so, the conditioned
% data is first partitioned into overlapping elements of the specified length.
% For each element, the signals from all detectors are then examined in the
% frequency domain for consistency with a burst coming from the specified sky
% positions.
%
% usage:
% 
%   [nullEnergyMap, incoherentEnergyMap, totalEnergyMap, degreesOfFreedom] = ...
%       xfastmapsky(channelNames, conditionedData, sampleFrequency, ...
%               amplitudeSpectra, skyPositions, elementLength, ...
%               transientLengths, frequencyBands, verboseFlag);
%
%    channelNames         cell array of channel name strings
%    conditionedData      matrix time domain conditioned data
%    sampleFrequency      sampling frequency of conditioned data [Hz]
%    amplitudeSpectra     matrix amplitude spectral densities [1/sqrt(Hz)]
%    skyPositions         matrix of sky positions [radians]
%    elementLength        transform length of fourier analyses [samples]
%    elementOffset        Scalar.  Number of samples between start of 
%                         consecutive elements.
%    transientLength      duration of conditioning transients [samples]
%    frequencyBands       matrix of frequency bands to search over [Hz]
%    analysisMode         string specifying 'search', 'veto', or 'frankenveto' analysis
%    verboseFlag          boolean flag to control status output (default 0)
% 
%    nullEnergyMap        resulting null energy map array (see below)
%    incoherentEnergyMap  resulting incoherent energy map array (see below)
%    totalEnergyMap       resulting total energy map array (see below)
%    degreesOfFreedom     number of degrees of freedom for statistical testing
%
% The resulting sky maps are three dimensional array with the following form.
%
%   nullEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex)
%   incoherentEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex)
%   totalEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex)
%
% The conditioned data should be provided as a matrix of time domain data with
% each channel in a separate column.
%
% The amplitude spectral densities should be provided as a matrix of one-sided
% frequency domain data at a frequency resolution corresponding to the desired
% element length and with each channel in a separate column.
%
% The desired sky position should be provided as a two column matrix of the form
% [theta phi], where theta is the geocentric colatitude running from 0 at the
% North pole to pi at the South pole and phi is the geocentric longitude in
% Earth fixed coordinates with 0 on the prime meridian.
%
% The desired frequency bands should be provided as a two column matrix of
% the form [lowFrequency highFrequency].
%
% See also XPIPELINE, XMAPSKY, XREADDATA, XINJECTSIGNAL, XCONDITION, XTILESKY, and
% XINTERPRET.
%
% See also ComputeAntennaResponse, LoadDetectorData.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        process command line arguments                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check for sufficient command line arguments
error(nargchk(10, 11, nargin));

% default arguments
if nargin == 10,
  verboseFlag = 0;
end

% check that analysisMode is a recognized type
if (~(strcmp(analysisMode,'veto') || strcmp(analysisMode,'search') || strcmp(analysisMode,'frankenveto')))
  error('analysisMode is not a recognized type');
end

% force one dimensional cell array
channelNames = channelNames(:);

% number of detectors
numberOfChannels = length(channelNames);

% validate number of detectors
if numberOfChannels < 3,
  error('data is required from three or more non-aligned detectors');
end

% % number of null sums
% numberOfNullSums = numberOfChannels - 2;

% validate conditioned data
if size(conditionedData, 2) ~= numberOfChannels,
  error('conditioned data is inconsistent with number of detectors');
end

% block length in samples
blockLength = size(conditionedData, 1);

% validate sky positions
if size(skyPositions, 2) < 2,
  error('sky positions must be a at least a two column matrix');
end
if any((skyPositions(:, 1) < 0) | (skyPositions(:, 1) > pi)),
  error('theta outside of [0, pi]');
end
if any((skyPositions(:, 2) < -pi) | (skyPositions(:, 2) >= pi)),
  error('phi outside of [-pi, pi)');
end

% number of sky positions
numberOfSkyPositions = size(skyPositions, 1);

% nyquist frequency
nyquistFrequency = sampleFrequency / 2;

% validate frequency bands
if size(frequencyBands, 2) ~= 2,
  error('frequency bands must be a two column matrix');
end
if max(max(frequencyBands)) > nyquistFrequency,
  error('requested frequency bands exceed nyquist frequency');
end
if any(diff(frequencyBands, 1, 2) <= 0),
  error('frequency bands have negative bandwidth');
end

% number of frequency bands to consider
numberOfFrequencyBands = size(frequencyBands, 1);

% validate integer power of two element length
if bitand(elementLength, elementLength - 1),
  error('element length is not an integer power of two');
end

% determine half element lengths
halfElementLength = elementLength / 2 + 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  partition block into overlapping elements                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% calculate the indices of the endpoints of the elements of length
% elementLength with specified overlap
elementIndices = chunkIndices(blockLength, elementLength, transientLength, elementOffset);

% in 'veto' mode keep only the element overlapping the middle of the data 
% segment
if (strcmp(analysisMode,'veto'))
    elementIndices = elementIndices(floor((size(elementIndices,1)+1)/2),:);
end

% Attack of the FrankenVeto monster!
if (strcmp(analysisMode,'frankenveto'))
    % Make sky maps for a small set of chunks centered on the event time.
    % Later combine over chunks into FrankenMaps!
    %
    % Compute chunkIndices using transient time to eliminate all but an  
    % interval of duration 3*elementLength at the center of the block. 
    elementIndices = chunkIndices(blockLength, elementLength, ...
        floor(0.5*(blockLength-3*elementLength)), elementOffset);
end

% number of elements
numberOfElements = size(elementIndices, 1);

% one-sided frequency vector for elements
elementFrequencies = nyquistFrequency * (0 : halfElementLength - 1) / ...
                     (halfElementLength - 1);

% frequency band indices into one-sided frequency domain element
frequencyIndices = cell(numberOfFrequencyBands, 1);
for frequencyBandNumber = 1 : numberOfFrequencyBands,
  frequencyIndices{frequencyBandNumber} = ...
    [find(elementFrequencies >= frequencyBands(frequencyBandNumber, 1),1) ...
     find(elementFrequencies <= frequencyBands(frequencyBandNumber, 2),1,'last')];
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         determine degrees of freedom                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% number of degrees of freedom in each frequency band
degreesOfFreedom = zeros(numberOfFrequencyBands, 1);
for frequencyBandNumber = 1 : numberOfFrequencyBands,
  degreesOfFreedom(frequencyBandNumber) = ...
      diff(frequencyIndices{frequencyBandNumber});
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         preload detector information                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for iDet = 1:numberOfChannels
    % The detector is matched by the first character of the channel name.
    detData      = LoadDetectorData(channelNames{iDet}(1));
    % the position vectors
    rDet(iDet,:) = detData.V';
    % the cell array of detector response tensors
    dDet{iDet}   = detData.d;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        begin loop over sky positions                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initialized result structure
nullEnergyMap = ...
    zeros(numberOfFrequencyBands, numberOfElements, numberOfSkyPositions);
incoherentEnergyMap = ...
    zeros(numberOfFrequencyBands, numberOfElements, numberOfSkyPositions);
totalEnergyMap = ...
    zeros(numberOfFrequencyBands, numberOfElements, numberOfSkyPositions);

% assign storage for extracted, time-shifted time series
elementTD = zeros(elementLength, numberOfChannels);

% cluster sky positions with similar time delays
% KLUDGE: Move this function outside fastxmapsky.  Also modify clustergrid
% to return cells in decreasing order of size (largest cluster first).
clusterTime = 3e-3;
[clusterNumber skyCells] = clustergrid(skyPositions, clusterTime, channelNames);

% begin loop over sky position clusters
for jSkyCells = 1 : size(skyCells,1)

    % report status
    if verboseFlag && (mod(jSkyCells, size(skyCells,1) / 100) < 1)
    fprintf(1, 'processing sky cell %d of %d (%d%% complete)...\n', ...
            jSkyCells, size(skyCells,1), ...
            round(100 * jSkyCells /  size(skyCells,1)));            
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %               compute detector responses and time delays                     %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Compute detector responses for each detector, sky position.
    [Fp, Fc] = antennaPatterns(dDet, skyCells{jSkyCells,1});
    % Whitened responses for each detector, sky position, frequency. 
    wFp = repmat(Fp',[1 1 halfElementLength]) ./ repmat( ...
        permute(shiftdim(amplitudeSpectra,-1),[3,1,2]), ...
        [1 size(skyCells{jSkyCells,1},1) 1]);
    wFc = repmat(Fc',[1 1 halfElementLength]) ./ repmat( ...
        permute(shiftdim(amplitudeSpectra,-1),[3,1,2]), ...
        [1 size(skyCells{jSkyCells,1},1) 1]);
    % Matrix M_AB (:= \sum_det wFA wFB) components, for each sky position 
    % and frequency.
    Mcc = sum(wFc.*wFc,1);
    Mpc = sum(wFp.*wFc,1);
    Mpp = sum(wFp.*wFp,1);
    % Determinant of M_AB
    detM = Mpp.*Mcc-Mpc.^2;
    % KLUDGE: Optimization choice:  Absorb 1/detM factor into M elements.
    % Needs three uses of detM and then get to clear it, as opposed to 2
    % uses later (Einc and Enull) but need to carry it around.

    % Compute time shifts for each detector and sky position.
    timeShifts = computeTimeShifts(rDet, skyCells{jSkyCells,1});
    % Compute mean integer-sample time shifts for each detector.
    timeShiftLengths = timeShifts * sampleFrequency;
    integerTimeShiftLengths = round(mean(timeShiftLengths,1));
    % Compute residual time shifts that will be applied as phase shift.
    residualTimeShifts = (timeShiftLengths ...
        - repmat(integerTimeShiftLengths,[size(timeShiftLengths,1),1]) ...
        ) / sampleFrequency;
    % Convert residual time shifts to frequency-dependent phase shifts.
    % Requires jumping up to 3D array.
    residualTimeShiftPhases = exp(sqrt(-1) * 2 * pi * ...
        repmat(residualTimeShifts',[1 1 halfElementLength]) ...
        .* repmat(shiftdim(elementFrequencies,-1),[size(residualTimeShifts') 1]) ...
     );
    % Free memory of un-needed variables.
    clear Fp Fc timeShifts timeShiftLengths residualTimeShifts

  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                         begin loop over elements                             %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % begin loop over elements
    for elementNumber = 1 : numberOfElements
  
        % start and stop indices of element
        elementStartIndex = elementIndices(elementNumber, 1);
        elementStopIndex = elementIndices(elementNumber, 2);

        for channelNumber = 1 : numberOfChannels
            elementTD(:, channelNumber) = conditionedData( ...
            (elementStartIndex + integerTimeShiftLengths(channelNumber)) : ...
            (elementStopIndex + integerTimeShiftLengths(channelNumber)), ...
            channelNumber);
        end
    
        % apply fourier transform
        elementFD = fft(elementTD);

        % take one-sided transform
        elementFD = elementFD(1 : halfElementLength, :);

        % apply residual time shift
        % same speed if do in one line or two:
        % element = permute(repmat(elementFD,[1 1 numberOfSkyPositions]),[2 3 1]) .* residualTimeShiftPhases;
        element = permute(repmat(elementFD,[1 1 size(skyCells{jSkyCells,1},1)]),[2 3 1]);
        element = element .* residualTimeShiftPhases;


        % construct energies
        % slightly faster (10%) if can omit conj() operation.  Can do this for
        % ordinary case (not circ pol).
        totalEnergy = sum(real(element).^2 + imag(element).^2, 1);
        elementFp = sum(wFp .* element,1);
        elementFc = sum(wFc .* element,1);
        nullEnergy = totalEnergy - ( ...
            (real(elementFp).^2+imag(elementFp).^2) .* Mcc ...
            - 2*real(elementFp.*conj(elementFc)) .* Mpc ...
            + (real(elementFc).^2+imag(elementFc).^2) .* Mpp ...
        ) ./ detM ;
        incoherentEnergy = totalEnergy - ( ...
            sum(wFp.^2.*(real(element).^2+imag(element).^2),1) .* Mcc ...
            - 2*sum(wFp.*wFc.*(real(element).^2+imag(element).^2),1) .* Mpc ...
            + sum(wFc.^2.*(real(element).^2+imag(element).^2),1) .* Mpp ...
        ) ./ detM ;
        % Complex F case: something like
        % elementFp = sum(conj(wFp) .* element,1);
        % elementFc = sum(conj(wFc) .* element,1);
        % nullEnergy = totalEnergy - ( ...
        %           (real(elementFp).^2+imag(elementFp).^2) .* Mcc ...
        %         - 2*real(elementFp.*conj(elementFc)) .* Mpc ...
        %         + (real(elementFc).^2+imag(elementFc).^2) .* Mpp ...
        %     ) ./ detM ;
        % incoherentEnergy = totalEnergy - ( ...
        %           sum((real(wFp).^2+imag(wFp).^2).*(real(element).^2+imag(element).^2),1) .* Mcc ...
        %         - 2*sum(real(wFp.*conj(wFc)).*(real(element).^2+imag(element).^2),1) .* Mpc ...
        %         + sum((real(wFc).^2+imag(wFc).^2).*(real(element).^2+imag(element).^2),1) .* Mpp ...
        %     ) ./ detM ;

        % construct cumulative sums over frequencies
        % KLUDGE:FIX need to pad with a zero in frequency direction to
        % compensate for diff() operation below, as done in xmapsky:
        % nullEnergy = [0; cumsum(nullEnergy);];
        % incoherentEnergy = [0; cumsum(incoherentEnergy);];
        % totalEnergy = [0; cumsum(totalEnergy);];
        nullEnergy = cumsum(reshape(nullEnergy,[length(skyCells{jSkyCells,2}),halfElementLength]),2)';
        incoherentEnergy = cumsum(reshape(incoherentEnergy,[length(skyCells{jSkyCells,2}),halfElementLength]),2)';
        totalEnergy = cumsum(reshape(totalEnergy,[length(skyCells{jSkyCells,2}),halfElementLength]),2)';
    
        % insert frequency band energies into sky maps
        for frequencyBandNumber = 1 : numberOfFrequencyBands,
            nullEnergyMap(frequencyBandNumber, elementNumber, skyCells{jSkyCells,2}) = ...
                diff(nullEnergy(frequencyIndices{frequencyBandNumber}-1,:));
            incoherentEnergyMap(frequencyBandNumber, elementNumber, skyCells{jSkyCells,2}) = ...
                diff(incoherentEnergy(frequencyIndices{frequencyBandNumber}-1,:));
            totalEnergyMap(frequencyBandNumber, elementNumber, skyCells{jSkyCells,2}) = ...
                diff(totalEnergy(frequencyIndices{frequencyBandNumber}-1,:));
        end
    

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                          end loop over elements                              %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
    % end loop over elements
    end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         end loop over sky positions                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% end loop over sky cells
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         frankenmaps                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (strcmp(analysisMode,'frankenveto'))

    %----- Each type of map: combine over chunks into FrankenMaps!

    %----- Sum over frequencies, and make the singleton dimension last (gone)
    tEM = permute(sum(totalEnergyMap,1),[2 3 1]);

    %----- For each sky pixel, find segment with maximum total energy.
    [valtot, indextot] = max(tEM,[],1);

    %----- Make FrankenMaps based on total energy.
    for frequencyBandNumber = 1:numberOfFrequencyBands,
        sindex = sub2ind(...
            [numberOfFrequencyBands numberOfElements numberOfSkyPositions],...
            frequencyBandNumber*ones(1,numberOfSkyPositions),...
            indextot,1:numberOfSkyPositions);

        frankenNullEnergyMap(frequencyBandNumber,1,:) = nullEnergyMap(sindex);
        frankenIncoherentEnergyMap(frequencyBandNumber,1,:) = incoherentEnergyMap(sindex);
        frankenTotalEnergyMap(frequencyBandNumber,1,:) = totalEnergyMap(sindex);
    end
    nullEnergyMap = frankenNullEnergyMap;
    incoherentEnergyMap = frankenIncoherentEnergyMap;
    totalEnergyMap = frankenTotalEnergyMap;

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          return to calling function                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% return to calling function
return;
