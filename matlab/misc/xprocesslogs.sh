#!/bin/bash

# Script to scan X-Pipeline log files after trigger generation to pull 
# out some summary timing statistics
# The script also deletes empty .err files and moves the .er and .out files 
# for jobs with non-empty .err files to a new directory, logs_err.
#
# usage:
#
#   xprocesslogs.sh <dir>
#
# where <dir> is the run directory to be cleaned.
# The script will produce the file <dir>/log_summary.txt.
#
# $Id$

# ---- Move into the target logs/ subdirectory.
here=`pwd`
cd $1
cd logs

# ---- Move logs for failed jobs to a new directory.
mkdir -p ../logs_err
find  *.err -not -empty | awk -F. '{print "mv", $1 ".err " $1 ".out ../logs_err/"}' > tmp
source tmp
rm tmp 
# ---- All remaining .err files are empty so they can be safely deleted.
rm -rf *.err

# ---- Get a list of all search job logs.
rm -rf ../log_summary.txt
ls | grep out | grep xsearch > search_logs 
while read fileName
do 
    str1=$(head -n 2 $fileName | tail -n 1)
    str2=$(tail -n 4 $fileName | head -n 1)
    echo $str1 " " $str2 >> ../log_summary.txt
done < search_logs
rm search_logs

# ---- Move up to the target directory.
cd ..
# ---- Report summary timing numbers to screen ... not yet distinguishing by job type.
echo "------------------------------------------------------"
echo " xprocesslogs.sh :"
echo "------------------------------------------------------"
echo "Job Duration"
echo "mean [s]     st.dev.[s]"
awk '{ total += $8 ; sq+=$8*$8 } END {print total/NR, (sq/NR-(total/NR)**2)**0.5}' log_summary.txt 

# ---- Return to original directory.
cd $here


