% $Id$

% This script calculates the pdf independently for each of theta,phi,psi.

% script to test the known skymap model version of the bayesian formulation for source direction localization
% loop over sky positions & polarizations
% - calculate predicted skymap along rings of constant time delay
% - calculate probability that source direction matches test direction
% result: plots of the PDF for each variable (theta, phi, psi)

% predictSkyMap - type analysis.  Use some simple signal model (predictSkyMap assumes
%	a linearly polarized delta pulse) to predict the sky map as a function of true source position.
%	Compute the match between the predicted L_pred(omega,omega_test) and the measured L(omega), which will be approximately
% 
%   mismatch(omega_test) = \sum_omega | L_pred(omega,omega_test) - L(omega) |^2
% 
% The source location is estimated as the omega_test with the smallest mismatch value.
% 
% References:
% Your mathematica notebook.
% 
% Advantages:
% - If the signal model is valid, you can use the full sky map, averaging over all the noise
%	and including antenna responses correctly.
% 
% Disadvantages:
% - We're back to assuming a particular signal model.  For example, predictSkyMap may go very wrong
%	 if the signal is oscillatory (it may get the destructive and constructive interference mixed up).

% clear workspace
% clear
format short

savePath = '../singleVariable_101/';

% ---- Path to the skymaps
skymapsPath = '../DFM_A1B2G1inj10/';
% skymapsPath = '../SG235inj1e-21/';
% skymapsPath = '/archive/home/psutton/coherent-network/Porter/DFM_A1B2G1inj10/';
% skymapsPath = '/archive/home/psutton/coherent-network/Porter/SG235inj1e-21/';

% ---- Injections info
injectionFile = '../earthbasedcoordinates.txt';

% ---- Number of time-frequency pixels summed over to make each sky map value
numberOfFreqBins = 61;

% pointing errors for loop over test sky positions
maxPointingErr = 30.0 * pi/180;
pointingErrStep = 0.5 * pi/180;

% errors for polarization for loop
maxPolarizationErr = 30.0 * pi/180;
polarizationErrStep = 1.0 * pi/180;

% ---- Specify network.  Has to match what's specified in parameterFile!
channels = {'H1','L1','V1'};

% ---- Skymap to test
% testSkymapNumbers = 1;
testSkymapNumbers = 500;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(savePath);
mkdir(savePath);

disp('Reading injection file...');
startTime = 0;
duration = 1e10;
injectionParameters = readinjectionfile(startTime,duration,injectionFile);

numberOfInjections = length(injectionParameters);

disp('Parsing injection file...');
[gps_s, gps_ns, phiInjs, thetaInjs, psiInjs, name, parameters] = ...
	parseinjectionparameters(injectionParameters);

% ---- Storage for the pointing errors
thetaErrs = NaN(numberOfInjections,1);
phiErrs = NaN(numberOfInjections,1);
psiErrs = NaN(numberOfInjections,1);
pointingErrs = NaN(numberOfInjections,1);

% ---- Given the test sky positions, what is the minimum pointing error we could get?
% ---- That is, how close is the closest test direction to the true source direction
% ---- This is important because it tells us two things:
% ---- 1. If the pointingErrs are larger than the minPointingErrs, method not optimal
% ---- 2. If the minPointingErrs are large, the method never had a good chance
%			because the match works best the test position is very close
%			to the source position
minPointingErrs = NaN(numberOfInjections,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate autocorrelation functions for each pair of baselines
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sampleFrequency = 16384;
nyquistFrequency = sampleFrequency/2;
F = [1:nyquistFrequency]';
numberOfDetectors = length(channels);

[t,h] = xmakewaveform('DFM','1~signal_A1B2G1_R.dat',1,0.5,sampleFrequency);
% [t,h] = xmakewaveform('SG',[1e-20 5e-3 820],1,0.5,sampleFrequency);
% [t,h] = xmakewaveform('CG',[1e-21 15/(2^0.5*pi*235) 235],1,0.5,sampleFrequency);
% ---- good approximation for any sharp burst < ~2ms
% [t,h] = xmakewaveform('G',[1e-20 1e-4],1,0.5,sampleFrequency);

% ---- storage
S = NaN(nyquistFrequency,numberOfDetectors);
sourceSNR2 = NaN(numberOfDetectors,1);
autocorr = [];
warning off; % -- SRD spits out a warning. Ignore it.

% ---- calculate power spectrums
for iChannel = 1:numberOfDetectors
	if isequal(channels{iChannel}, 'V1')
		S(:,iChannel) = SRD('Virgo-LV',F);
	else
		S(:,iChannel) = SRD('LIGO-LV',F);
	end
	[SNR, h_rss, h_peak, Fchar, bw, Tchar, dur] = ...
		xoptimalsnr(h,0,sampleFrequency,S(:,iChannel),F(1),1,10,nyquistFrequency);
	sourceSNR2(iChannel) = SNR;
end

% ---- calculate autocorrelations
for iChannel = 1:numberOfDetectors-1
    for jChannel = iChannel+1:numberOfDetectors
		iAutocorr = InnerProduct(h,h,sampleFrequency,(S(:,iChannel).*S(:,jChannel)).^0.5,F(1),1,10,nyquistFrequency);
		iAutocorr = fftshift(iAutocorr)/max(iAutocorr);
		autocorr = [autocorr iAutocorr];
	end
end

warning on;
middleSample = length(iAutocorr)/2 + 1;
autocorrTime = ([1:length(iAutocorr)].'-middleSample)/sampleFrequency;

% ---- step function
% signalPeriod = 5e-4;
% autocorr = abs(autocorrTime) < signalPeriod;
% autocorr = repmat(autocorr, 1, 3);

% plot(autocorrTime, autocorr(:,1), autocorrTime, autocorr(:,3));
% legend('LIGO-LIGO','LIGO-Virgo');
% xlim([-0.03 0.03]);
% break;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loop over skymap files
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for nSkymap = 1:length(testSkymapNumbers);
	iSkymap = testSkymapNumbers(nSkymap);
	
	disp(['opening results_' num2str(iSkymap-1) '.mat']);
	
	try
		f = open([skymapsPath 'results_' num2str(iSkymap-1) '.mat']);
	catch
        warning([skymapsPath 'results_' num2str(iSkymap-1) '.mat missing']);
        continue;
    end
	
	% ---- get skyPositions
	thetas = f.skyPositions(:, 1);
	phis = f.skyPositions(:, 2);
	dOmegas = f.skyPositions(:,4); % solid angles
	nSkyPositions = size(f.skyPositions, 1);
	
	% ---- the sourcePositions variable in the skymaps is wrong.
	%	   must read from our injection file.
	theta0 = thetaInjs(iSkymap);
	phi0 = phiInjs(iSkymap);
	psi0 = psiInjs(iSkymap);
	
	% ---- cartesian coordinates
	sourceOmega = [sin(theta0).*cos(phi0),sin(theta0).*sin(phi0),cos(theta0)];
	sourceOmegaTheta = [cos(theta0), sin(theta0)];
	sourceOmegaPhi = [cos(phi0), sin(phi0)];
	
	Enull0 = squeeze(f.nullEnergyMap);
	Einc0 = squeeze(f.incoherentEnergyMap);
	measuredMap = Enull0./Einc0 - 1;
	
	figure;
	subplot(1,2,1);
	xproject(f.skyPositions,measuredMap,'sphere',channels);
	subplot(1,2,2);
	xproject(f.skyPositions,measuredMap,'mollweid',channels);
	set(gca,'XTick',[]);
	set(gca,'YTick',[]);
	% xlabel('\phi');
	% ylabel('\theta','Rotation',0);
	hc = colorbar;
	set(hc,'OuterPosition',[0.02 0.5/2 0.15 0.5]);
	xlabel(hc,'Enull/Einc-1');
	drawnow;

	saveas(gcf, [savePath 'skymap.png']);
	saveas(gcf, [savePath 'skymap.eps'], 'epsc2');

	% storage
	thetas = [];
	phis = [];
	logPDFs = {};
	PDFs = {};

	skyPositionNumber = 0;

	variables = {'theta1'; 'phi1'; 'polarization1'};
	optimalValues = [theta0, phi0, psi0];
	ranges = {...
		[-maxPointingErr+optimalValues(1) : pointingErrStep : maxPointingErr+optimalValues(1)],...
		[-maxPointingErr+optimalValues(2) : pointingErrStep : maxPointingErr+optimalValues(2)],...
		[-maxPolarizationErr+optimalValues(3) : polarizationErrStep : maxPolarizationErr+optimalValues(3)]...
		};

	% loop over test sky positions and parameters
	for iVar = 1:length(variables)
		iVarName = variables{iVar};
		varRange = ranges{iVar};
		logPDFs{iVar} = [];

		% assume loop variables are correct
		for jVar = 1:length(variables)
			eval([variables{jVar} '=' num2str(optimalValues(jVar)) ';']);
		end
	
		disp(['loop over ' iVarName]);
	
		for jValue = 1:length(varRange)
		
			skyPositionNumber = skyPositionNumber + 1;
		
			eval([iVarName '=' num2str(varRange(jValue)) ';']);
		
			% calculate a different skymap from the model
			testSourcePositions = [theta1, phi1];
			[Enull1, Einc1, Etot1] = predictSkyMap(testSourcePositions, ...
				polarization1,sourceSNR2,channels,numberOfFreqBins,f.skyPositions, ...
				autocorr, autocorrTime);

			predictedMap = Enull1./Einc1 - 1;
			
			% figure;
			% xproject(f.skyPositions,predictedMap,'sphere',channels);
			% colorbar;
			% drawnow;
			
			match = zeros(1,size(predictedMap,2),size(predictedMap,3));
			
			measuredUnitMap = transpose(measuredMap) / norm(measuredMap);
			for jPol = 1:size(predictedMap,3)
			    match(1,:,jPol) = (measuredUnitMap * predictedMap(:,:,jPol)) ...
			    					./ sum(predictedMap(:,:,jPol).^2,1).^(0.5);
			end
			
			logPDF = match(1:1:1);
			logPDFs{iVar}(jValue) = logPDF;
	
			disp([theta1, phi1, polarization1, logPDF]);
		end
	
		maxLogPDF = max(logPDFs{iVar});
		PDFs{iVar} = exp(logPDFs{iVar} - maxLogPDF);

		figure;
		plot((varRange-optimalValues(iVar))*180/pi, PDFs{iVar}, 'o-');
		ylabel('Bayesian probability distribution');
		xlabel([iVarName ' error (deg)']);
		drawnow;
	
		saveas(gcf, [savePath iVarName], 'png');
		saveas(gcf, [savePath iVarName], 'fig');
		saveas(gcf, [savePath iVarName], 'eps');
	end

	save([savePath 'data.mat']);
end

