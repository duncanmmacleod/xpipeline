% This script runs xpipeline to get a measured skymap,
% runs predictSkymap, and calculates the matches.
% - Does not assume polarization angle.

% script to test the known skymap model version of the bayesian formulation for source direction localization
% loop over sky positions & polarizations
% - calculate predicted skymap along rings of constant time delay
% - calculate probability that source direction matches test direction
% result: match

% predictSkyMap - type analysis.  Use some simple signal model (predictSkyMap assumes
%	a linearly polarized delta pulse) to predict the sky map as a function of true source position.
%	Compute the match between the predicted L_pred(omega,omega_test) and the measured L(omega), which will be approximately
% 
%   mismatch(omega_test) = \sum_omega | L_pred(omega,omega_test) - L(omega) |^2
% 
% The source location is estimated as the omega_test with the smallest mismatch value.
% 
% References:
% Your mathematica notebook.
% 
% Advantages:
% - If the signal model is valid, you can use the full sky map, averaging over all the noise
%	and including antenna responses correctly.
% 
% Disadvantages:
% - We're back to assuming a particular signal model.  For example, predictSkyMap may go very wrong
%	 if the signal is oscillatory (it may get the destructive and constructive interference mixed up).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% preparations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% run from the xpipeline directory
% path(path, 'sky_direction');

clear
format short
format compact

% ---- Energy measure. whether we use null or null/inc, etc.
% EmeasureType = 'null';
EmeasureType = 'nullOverInc';

% ---- Parameters file for xpipeline.
parameterFile = 'sky_direction/parameters.txt';

% ---- Specify network.  Has to match what's specified in parameterFile!
channels = {'H1','L1','V1'}';

% ---- Injection parameters.  Has to match what's specified in
%      parameterFile!
sourceSNR2 = 30^2 * [ 11.6217^2,  11.6217^2,  8.6470^2];

% sourcePolarizations = 0;

% ---- Pointing errors for loop over test sky positions
maxPointingErr = 5.0 * pi/180;
pointingErrStep = 1.0 * pi/180;
% -- scan all polarization angles from 0-90 deg in this step
polarizationErrStep = 10.0 * pi/180;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% run xpipeline to make a skymap
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[skyPositions, nullEnergyMap, incoherentEnergyMap, totalEnergyMap, ...
	degreesOfFreedom, sourcePositions, sourcenullEnergyMap, ...
	sourceincoherentEnergyMap, sourcetotalEnergyMap] = xpipeline(parameterFile);

Enull0 = squeeze(nullEnergyMap);
Einc0 = squeeze(incoherentEnergyMap);

figure;
switch EmeasureType
	case 'null'
		Emeasure0 = Enull0;
        xproject(skyPositions,Emeasure0/degreesOfFreedom,'sphere',channels);
	case 'nullOverInc'
		Emeasure0 = Enull0./Einc0 - 1;
        xproject(skyPositions,Emeasure0,'sphere',channels);
end
colorbar;
drawnow;

% solid angles
dOmegas = skyPositions(:,4);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% run predictSkyMap
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Test sky positions: Square grid centered on correct position.
[theta,phi] = meshgrid( ...
    sourcePositions(1)+[-maxPointingErr:pointingErrStep:maxPointingErr], ...
    sourcePositions(2)+[-maxPointingErr:pointingErrStep:maxPointingErr]);
testSourcePositions = [theta(:), phi(:)];

% ---- Vector of test source polarizations.  Note that changing the
%      polarization by pi/2 is equivalent to changing the sign of the
%      waveform; therefore need only test [0,pi/2).
testSourcePolarizations = [0:polarizationErrStep:pi/2];

% ---- Noise DOF.
numberOfFreqBins = degreesOfFreedom + 1;

% ---- Run predictSkyMap.
[Enull1, Einc1, Etot1] = predictSkyMap(testSourcePositions, ...
    testSourcePolarizations,sourceSNR2,channels,numberOfFreqBins,skyPositions);
% Enull1 = squeeze(Enull1);
% Einc1 = squeeze(Einc1);
% Etot1 = squeeze(Etot1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Comparisons
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch EmeasureType
	case 'null'
		measuredMap  = Enull0;
		predictedMap = Enull1;
	case 'nullOverInc'
		measuredMap  = Enull0./Einc0 - 1;
		predictedMap = Enull1./Einc1 - 1;
end

% ---- Normalized match
match = zeros(1,size(predictedMap,2),size(predictedMap,3));
measuredUnitMap = transpose(measuredMap) / norm(measuredMap);
for jPol = 1:size(predictedMap,3)
    match(1,:,jPol) = measuredUnitMap * predictedMap(:,:,jPol) ;
    match(1,:,jPol) = match(1,:,jPol) ./ sum(predictedMap(:,:,jPol).^2,1).^(0.5) ;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% figure;
% xproject(skyPositions,predRatio(:,66),'sphere',channel); colorbar; caxis([-1 1])
% figure; 
% plot(sourcePolarization,match,'-o'); 
% grid
% hold on
% plot(min(Enull1,[],1),'g-+')
% figure; imagesc([-5:5],[-5:5],reshape(match,size(theta,1),size(theta,2))); colorbar



% % storage
% thetas = [];
% phis = [];
% logPDFs = [];

% 		% calculate log of the PDF with solid angle weighting
% 		logPDF = -sum(dOmegasOnRing.*(Emeasure0onRing-Emeasure1onRing).^2);
% 		thetas(skyPositionNumber) = iTheta;
% 		phis(skyPositionNumber) = jPhi;
% 		logPDFs(skyPositionNumber) = logPDF;
% 		
% 		disp([iTheta, jPhi, logPDF]);
% 
% 
% maxLogPDF = max(logPDFs);
% PDFs = exp(logPDFs - maxLogPDF);
% PDFmatrix = reshape(PDFs,sqrt(skyPositionNumber),sqrt(skyPositionNumber));
% 
% figure;
% pcolor(PDFmatrix);
% %contour(PDFmatrix,50);
% title('Bayesian probability distribution');
% xlabel('\theta error (deg)');
% ylabel('\phi error (deg)');
% set(gca,'XTickLabel',[-maxPointingErr*180/pi:pointingErrStep*180/pi:maxPointingErr*180/pi]);
% set(gca,'YTickLabel',[-maxPointingErr*180/pi:pointingErrStep*180/pi:maxPointingErr*180/pi]);
% colorbar;


