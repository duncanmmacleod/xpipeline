% This script runs the bayesian or minimization methods on a set of precalculated skymaps
% and generates pointing error statistics.

% Script to test the known skymap model version of the bayesian formulation for source direction localization
% loop over sky positions & polarizations
% - calculate predicted skymap along rings of constant time delay
% - calculate probability that source direction matches test direction
% result: list of pointing errors for each skymap:
%	pointingErrs, thetaErrs, phiErrs

% predictSkyMap - type analysis.  Use some simple signal model (predictSkyMap assumes
%	a linearly polarized delta pulse) to predict the sky map as a function of true source position.
%	Compute the match between the predicted L_pred(omega,omega_test) and the measured L(omega), which will be approximately
% 
%   mismatch(omega_test) = \sum_omega | L_pred(omega,omega_test) - L(omega) |^2
% 
% The source location is estimated as the omega_test with the smallest mismatch value.
% 
% References:
% Your mathematica notebook.
% 
% Advantages:
% - If the signal model is valid, you can use the full sky map, averaging over all the noise
%	and including antenna responses correctly.
% 
% Disadvantages:
% - We're back to assuming a particular signal model.  For example, predictSkyMap may go very wrong
%	 if the signal is oscillatory (it may get the destructive and constructive interference mixed up).

% run from the xpipeline directory
% path(path, 'sky_direction');

% clear
format short
format compact
tic;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Option to save the plots and data
saveData = false;
savePath = '../run_603_2/';

% ---- Option to save html report
saveHTML = true;
HTMLSavePath = [savePath 'html/'];
% HTMLSavePath = ['/archive/home/spoprock/public_html/test/' savePath];
nSkymapsPerPage = 20;

% ---- Path to the skymaps
% skymapsPath = '../DFM_A1B2G1inj10/';
skymapsPath = '../SG235inj1e-21/';
% skymapsPath = '/archive/home/psutton/coherent-network/Porter/DFM_A1B2G1inj10/';
% skymapsPath = '/archive/home/psutton/coherent-network/Porter/SG235inj1e-21/';

% ---- Injections info
injectionFile = '../earthbasedcoordinates.txt';

% ---- Number of time-frequency pixels summed over to make each sky map value
numberOfFreqBins = 61;

% ---- Whether to use the Enull minimization method or bayesian
% method = 'random';
% method = 'minimization';
method = 'bayesian';

% ---- Scan all polarization angles from 0-90 deg in this step
polarizationErrStep = 0; % assume correct polarization
% polarizationErrStep = 10.0 * pi/180;

% ---- Step size for test sky positions
testPositionsStep = 2 * pi/180;

% ---- Specify network.  Has to match what's specified in parameterFile!
channels = {'H1','L1','V1'};

% ---- Skymaps to test
% testSkymapNumbers = [1:1259];
testSkymapNumbers = [1:400:1259];
% testSkymapNumbers = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(savePath);
mkdir(savePath);

if saveHTML
	mkdir(HTMLSavePath);
end

disp('Reading injection file...');
startTime = 0;
duration = 1e10;
injectionParameters = readinjectionfile(startTime,duration,injectionFile);

numberOfInjections = length(injectionParameters);

disp('Parsing injection file...');
[gps_s, gps_ns, phiInjs, thetaInjs, psiInjs, name, parameters] = ...
	parseinjectionparameters(injectionParameters);

% ---- Storage for the pointing errors
thetaErrs = NaN(numberOfInjections,1);
phiErrs = NaN(numberOfInjections,1);
psiErrs = NaN(numberOfInjections,1);
pointingErrs = NaN(numberOfInjections,1);

% ---- Given the test sky positions, what is the minimum pointing error we could get?
% ---- That is, how close is the closest test direction to the true source direction
% ---- This is important because it tells us two things:
% ---- 1. If the pointingErrs are larger than the minPointingErrs, method not optimal
% ---- 2. If the minPointingErrs are large, the method never had a good chance
%			because the match works best the test position is very close
%			to the source position
minPointingErrs = NaN(numberOfInjections,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set up test sky positions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

H = LoadDetectorData('H');
L = LoadDetectorData('L');
baseZ = H.V - L.V;
baseY = cross([0; 0; 1], baseZ);
baseX = cross(baseY, baseZ);
bHLZ = baseZ / norm(baseZ);
bHLY = baseY / norm(baseY);
bHLX = baseX / norm(baseX);

xhat = [1 0 0];
yhat = [0 1 0];
zhat = [0 0 1];
R = [xhat*bHLX, xhat*bHLY, xhat*bHLZ;
	 yhat*bHLX, yhat*bHLY, yhat*bHLZ;
	 zhat*bHLX, zhat*bHLY, zhat*bHLZ];

theta_b = acos(bHLZ(3));
phi_b = atan2(bHLZ(2),bHLZ(1));

% ---- march around theta in HL coordinates
% theta1 = [theta_b : testPositionsStep : pi]';
% theta2 = [pi - testPositionsStep : -testPositionsStep : pi - theta_b ]';
% testThetas = [theta1; theta2];
% phi1 = phi_b*ones(size(theta1));
% phi2 = (phi_b+pi)*ones(size(theta2));
% testPhis = [phi1; phi2];
testThetas1HL = [0 : testPositionsStep : pi]';
testPhis1HL = phi_b*ones(size(testThetas1HL));
V = CartesianPointingVector(testPhis1HL, testThetas1HL);
V = (R * V')';
testThetas1 = acos(V(:,3));
testPhis1 = atan2(V(:,2),V(:,1));

% ---- march around phi in HL coordinates
testPhis2HL = [0 : testPositionsStep : 2*pi]';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate autocorrelation functions for each pair of baselines
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sampleFrequency = 16384;
nyquistFrequency = sampleFrequency/2;
F = [1:nyquistFrequency]';
numberOfDetectors = length(channels);

% [t,h] = xmakewaveform('DFM','1~signal_A1B2G1_R.dat',1,0.5,sampleFrequency);
% [t,h] = xmakewaveform('SG',[1e-20 5e-3 820],1,0.5,sampleFrequency);
[t,h] = xmakewaveform('CG',[1e-21 15/(2^0.5*pi*235) 235],1,0.5,sampleFrequency);
% ---- good approximation for any sharp burst < ~2ms
% [t,h] = xmakewaveform('G',[1e-20 1e-4],1,0.5,sampleFrequency);

% ---- storage
S = NaN(nyquistFrequency,numberOfDetectors);
sourceSNR2 = NaN(numberOfDetectors,1);
autocorr = [];
warning off; % -- SRD spits out a warning. Ignore it.

% ---- calculate power spectrums
for iChannel = 1:numberOfDetectors
	if isequal(channels{iChannel}, 'V1')
		S(:,iChannel) = SRD('VIRGO-LV',F);
	else
		S(:,iChannel) = SRD('LIGO-LV',F);
	end
	[SNR, h_rss, h_peak, Fchar, bw, Tchar, dur] = ...
		xoptimalsnr(h,0,sampleFrequency,S(:,iChannel),F(1),1,10,nyquistFrequency);
	sourceSNR2(iChannel) = SNR;
end

% ---- calculate autocorrelations
for iChannel = 1:numberOfDetectors-1
    for jChannel = iChannel+1:numberOfDetectors
		iAutocorr = InnerProduct(h,h,sampleFrequency,(S(:,iChannel).*S(:,jChannel)).^0.5,F(1),1,10,nyquistFrequency);
		iAutocorr = fftshift(iAutocorr)/max(iAutocorr);
		autocorr = [autocorr iAutocorr];
	end
end

warning on;
middleSample = length(iAutocorr)/2 + 1;
autocorrTime = ([1:length(iAutocorr)].'-middleSample)/sampleFrequency;

% ---- step function
% signalPeriod = 5e-4;
% autocorr = abs(autocorrTime) < signalPeriod;
% autocorr = repmat(autocorr, 1, 3);

% plot(autocorrTime, autocorr);
% xlim([-0.03 0.03]);
% break;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loop over skymap files
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for nSkymap = 1:length(testSkymapNumbers);
	iSkymap = testSkymapNumbers(nSkymap);
	
	disp(['opening results_' num2str(iSkymap-1) '.mat']);
	
	try
		f = open([skymapsPath 'results_' num2str(iSkymap-1) '.mat']);
	catch
        warning([skymapsPath 'results_' num2str(iSkymap-1) '.mat missing']);
        continue;
    end
	
	% ---- get skyPositions
	thetas = f.skyPositions(:, 1);
	phis = f.skyPositions(:, 2);
	dOmegas = f.skyPositions(:,4); % solid angles
	nSkyPositions = size(f.skyPositions, 1);
	
	% ---- the sourcePositions variable in the skymaps is wrong.
	%	   must read from our injection file.
	theta0 = thetaInjs(iSkymap);
	phi0 = phiInjs(iSkymap);
	psi0 = psiInjs(iSkymap);
	
	% ---- cartesian coordinates
	sourceOmega = [sin(theta0).*cos(phi0),sin(theta0).*sin(phi0),cos(theta0)];
	sourceOmegaTheta = [cos(theta0), sin(theta0)];
	sourceOmegaPhi = [cos(phi0), sin(phi0)];
	
	Enull0 = squeeze(f.nullEnergyMap);
	Einc0 = squeeze(f.incoherentEnergyMap);
	measuredMap = Enull0./Einc0 - 1;
	
	if isequal(method, 'random')
		thetaMax = pi*rand();
		phiMax = 2*pi*rand() - pi;
		psiMax = pi*rand();
	elseif isequal(method, 'minimization')
		[minValue, minIndex] = min(Enull0);
		thetaMax = thetas(minIndex);
		phiMax = phis(minIndex);
		psiMax = NaN;
	else
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% March around theta in HL coordinates
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		% ---- Test sky positions:
		testSourcePositions = [testThetas1 testPhis1];
		nTestSkyPositions = length(testSourcePositions);
				
		% ---- cartesian coordinates
		testOmegas = CartesianPointingVector(testPhis1, testThetas1);

		% ---- test sky position pointing errors
		testPointingErrs = real(acos(dot(repmat(sourceOmega, nTestSkyPositions, 1), ...
			testOmegas, 2)));
	
		minPointingErrs(iSkymap) = min(testPointingErrs);
		
		% ---- Vector of test source polarizations.  Note that changing the
		%      polarization by pi/2 is equivalent to changing the sign of the
		%      waveform; therefore need only test [0,pi/2).
		if polarizationErrStep == 0
			testSourcePolarizations = psi0;
		else
			testSourcePolarizations = [0:polarizationErrStep:pi/2];
		end
		
		% ---- calculate predicted skymaps for HL baseline
		[Enull1, Einc1, Etot1] = predictSkyMap(testSourcePositions, ...
			testSourcePolarizations,sourceSNR2,channels,numberOfFreqBins,f.skyPositions, ...
			autocorr, autocorrTime, 1);

		predictedMap = Enull1./Einc1 - 1;
		match = zeros(1,size(predictedMap,2),size(predictedMap,3));
		measuredUnitMap = transpose(measuredMap) / norm(measuredMap);
		for jPol = 1:size(predictedMap,3)
		    match(1,:,jPol) = (measuredUnitMap * predictedMap(:,:,jPol)) ...
		    					./ sum(predictedMap(:,:,jPol).^2,1).^(0.5);
		end
		
		% ---- maximize over polarizations
		[maxOverPsi, maxPsiIndices] = max(match, [], 3);
		figure; plot(maxOverPsi); figure;
		[maxMatch, maxIndex] = max(maxOverPsi);
		thetaMaxHL = testThetas1HL(maxIndex);
		thetaMaxHLMap = predictedMap(:,maxIndex,maxPsiIndices(maxIndex));
		thetaMaxHLUnitMap = thetaMaxHLMap / norm(thetaMaxHLMap);
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% March around phi in HL coordinates
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		testThetas2HL = thetaMaxHL * ones(size(testPhis2HL));
		V = CartesianPointingVector(testPhis2HL, testThetas2HL);
		V = (R * V')';
		
		testThetas2 = acos(V(:,3));
		testPhis2 = atan2(V(:,2),V(:,1));
		testSourcePositions2 = [testThetas2, testPhis2];
		
		% ---- cartesian coordinates
		testOmegas2 = CartesianPointingVector(testPhis2, testThetas2);
		
		% ---- calculate predicted skymaps for non HL baselines
		[Enull2, Einc2, Etot2] = predictSkyMap(testSourcePositions2, ...
			testSourcePolarizations,sourceSNR2,channels,numberOfFreqBins,f.skyPositions, ...
			autocorr, autocorrTime, [2 3]);

		predictedMap = Enull2./Einc2 - 1;
		match = zeros(1,size(predictedMap,2),size(predictedMap,3));
		
		measuredMap2 = measuredUnitMap' - thetaMaxHLUnitMap;
		measuredUnitMap2 = transpose(measuredMap2) / norm(measuredMap2);
		for jPol = 1:size(predictedMap,3)
		    match(1,:,jPol) = (measuredUnitMap2 * predictedMap(:,:,jPol)) ...
		    					./ sum(predictedMap(:,:,jPol).^2,1).^(0.5);
		end
		
		% ---- maximize over polarizations
		[maxOverPsi, maxPsiIndices] = max(match, [], 3);
		[maxMatch, maxIndex] = max(maxOverPsi);
		thetaMax = testSourcePositions2(maxIndex, 1);
		phiMax = testSourcePositions2(maxIndex, 2);
		psiMax = testSourcePolarizations(maxPsiIndices(maxIndex));
	end

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Get values for maximum of PDF, calculate errors
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	maxOmega = [sin(thetaMax).*cos(phiMax),sin(thetaMax).*sin(phiMax),cos(thetaMax)];
	maxOmegaTheta = [cos(thetaMax), sin(thetaMax)];
	maxOmegaPhi = [cos(phiMax), sin(phiMax)];
	
	pointingErrs(iSkymap) = real(acos(dot(sourceOmega, maxOmega)));
	thetaErrs(iSkymap) = real(acos(dot(sourceOmegaTheta, maxOmegaTheta)));
	phiErrs(iSkymap) = real(acos(dot(sourceOmegaPhi, maxOmegaPhi)));
	psiErrs(iSkymap) = abs(psiMax - psi0);
	
	errs = [pointingErrs(iSkymap), thetaErrs(iSkymap), phiErrs(iSkymap), psiErrs(iSkymap)];
	disp(errs);
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Save html for this skymap
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	if saveHTML
		% ---- save images
		xproject(f.skyPositions,measuredMap,'sphere',channels);
		set(gca, 'CameraPosition', sourceOmega.*norm(get(gca, 'CameraPosition')));
		hold on;
		plot3(1.04*sourceOmega(:,1),1.04*sourceOmega(:,2),1.04*sourceOmega(:,3),'ks','markerfacecolor','c');
		plot3(1.04*maxOmega(:,1),1.04*maxOmega(:,2),1.04*maxOmega(:,3),'ks','markerfacecolor','y');
		plot3(1.03*testOmegas(:,1),1.03*testOmegas(:,2),1.03*testOmegas(:,3),'ko','markerfacecolor','m');
		plot3(1.03*testOmegas2(:,1),1.03*testOmegas2(:,2),1.03*testOmegas2(:,3),'ko','markerfacecolor','g');
		imgPath = [HTMLSavePath 'skymap_' num2str(iSkymap) '_testPositions.png'];
		saveas(gcf, imgPath);
		system(['convert -trim ' imgPath ' ' imgPath]);
		
		xproject(f.skyPositions,measuredMap,'mollweid',channels);
		imgPath = [HTMLSavePath 'skymap_' num2str(iSkymap) '.png'];
		saveas(gcf, imgPath);
		system(['convert -trim ' imgPath ' ' imgPath]);
		
		[Enull, Einc, Etot] = predictSkyMap([theta0 phi0], ...
			psi0,sourceSNR2,channels,numberOfFreqBins,f.skyPositions, ...
			autocorr, autocorrTime, [1 2 3]);
		xproject(f.skyPositions,Enull./Einc - 1,'mollweid',channels);
		imgPath = [HTMLSavePath 'skymap_' num2str(iSkymap) '_pred.png'];
		saveas(gcf, imgPath);
		system(['convert -trim ' imgPath ' ' imgPath]);
		
		[Enull, Einc, Etot] = predictSkyMap([thetaMax phiMax], ...
			psiMax,sourceSNR2,channels,numberOfFreqBins,f.skyPositions, ...
			autocorr, autocorrTime, [1 2 3]);
		xproject(f.skyPositions,Enull./Einc - 1,'mollweid',channels);
		imgPath = [HTMLSavePath 'skymap_' num2str(iSkymap) '_best_pred.png'];
		saveas(gcf, imgPath);
		system(['convert -trim ' imgPath ' ' imgPath]);
		
		% ---- save html
		% ---- if this is the 1st on a page, create new file
		if ((mod(nSkymap-1, nSkymapsPerPage)) == 0)
			if nSkymap-1 == 0
				filename = 'index.html';
			else
				fprintf(fid, '</table>\n</body>\n</html>');
				fclose(fid);
				filename = ['index_' num2str(1+(nSkymap-1)/nSkymapsPerPage) '.html'];
			end
			fid = fopen([HTMLSavePath filename], 'w');
			fprintf(fid, ['<html>\n<body>Page: \n']);
			for iPage = 1:ceil(length(testSkymapNumbers)/nSkymapsPerPage)
				if iPage == 1
					filename = 'index.html';
				else
					filename = ['index_' num2str(iPage) '.html'];
				end
				fprintf(fid, ['<a href=\"' filename '\">[' num2str(iPage) ']</a> \n']);
			end
			fprintf(fid, ['<table>\n<tr>\n<th>measured</th>\n' ...
				'<th>predicted</th>\n<th>best predicted</th>\n<th>errors</th>\n</tr>\n']);
		end
		
		if pointingErrs(iSkymap) <= pi/4
			color = 'green';
		elseif pointingErrs(iSkymap) <= 2*pi/4
			color = 'yellow';
		elseif pointingErrs(iSkymap) <= 3*pi/4
			color = 'orange';
		elseif pointingErrs(iSkymap) <= 4*pi/4
			color = 'red';
		end
		
		fprintf(fid, ['<tr bgcolor=\"' color '\">\n<td width=\"25%%\">' ...
			'<a href=\"skymap_' num2str(iSkymap) '.png\">' ...
			'<img src=\"skymap_' num2str(iSkymap) '.png\" width=\"100%%\"></td>\n']);
		fprintf(fid, ['<td width=\"25%%\">' ...
			'<a href=\"skymap_' num2str(iSkymap) '_pred.png\">' ...
			'<img src=\"skymap_' num2str(iSkymap) '_pred.png\" width=\"100%%\"></td>\n']);
		fprintf(fid, ['<td width=\"25%%\">' ...
			'<a href=\"skymap_' num2str(iSkymap) '_best_pred.png\">' ...
			'<img src=\"skymap_' num2str(iSkymap) '_best_pred.png\" width=\"100%%\"></td>\n']);
		fprintf(fid, '<td>pointingErr: %f<br/>thetaErr: %f<br/>phiErr: %f<br />psiErr: %f<br />skymap #: %d</td>\n</tr>\n\n', ...
			[errs'; iSkymap]);
		
		% ---- if this is the last on page or very last, close file
		if ((mod(nSkymap, nSkymapsPerPage)) == 0 || (nSkymap == length(testSkymapNumbers)))
			fprintf(fid, '</table>\n</body>\n</html>');
			fclose(fid);
		end
	end
end

totalTime = toc;
disp(['total time: ' num2str(totalTime)]);

if saveData
	save([savePath 'data.mat'], 'skymapsPath', 'injectionFile', 'numberOfFreqBins', ...
		'sourceSNR2', 'method', 'testFraction', 'polarizationErrStep', ...
		'channels', 'testSkymapNumbers', 'totalTime', ...
		'thetaErrs', 'phiErrs', 'pointingErrs', 'minPointingErrs');
end
