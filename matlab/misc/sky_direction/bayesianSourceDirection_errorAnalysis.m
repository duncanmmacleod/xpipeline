% This script runs the bayesian or minimization methods on a set of precalculated skymaps
% and generates pointing error statistics.

% Script to test the known skymap model version of the bayesian formulation for source direction localization
% loop over sky positions & polarizations
% - calculate predicted skymap along rings of constant time delay
% - calculate probability that source direction matches test direction
% result: list of pointing errors for each skymap:
%	pointingErrs, thetaErrs, phiErrs

% predictSkyMap - type analysis.  Use some simple signal model (predictSkyMap assumes
%	a linearly polarized delta pulse) to predict the sky map as a function of true source position.
%	Compute the match between the predicted L_pred(omega,omega_test) and the measured L(omega), which will be approximately
% 
%   mismatch(omega_test) = \sum_omega | L_pred(omega,omega_test) - L(omega) |^2
% 
% The source location is estimated as the omega_test with the smallest mismatch value.
% 
% References:
% Your mathematica notebook.
% 
% Advantages:
% - If the signal model is valid, you can use the full sky map, averaging over all the noise
%	and including antenna responses correctly.
% 
% Disadvantages:
% - We're back to assuming a particular signal model.  For example, predictSkyMap may go very wrong
%	 if the signal is oscillatory (it may get the destructive and constructive interference mixed up).

% run from the xpipeline directory
% path(path, 'sky_direction');

% clear
format short
format compact
tic;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Option to save the plots and data
saveData = true;
savePath = '../run_1603_2/';

% ---- Option to save html report
saveHTML = true;
HTMLSavePath = [savePath 'html/'];
% HTMLSavePath = ['/archive/home/spoprock/public_html/test/' savePath];
nSkymapsPerPage = 20;

% ---- Path to the skymaps
% skymapsPath = '~/Desktop/';
% skymapsPath = '../DFMA1B2G1/';
skymapsPath = '../SG235/';
% skymapsPath = '../DFM_A1B2G1inj10/';
% skymapsPath = '../SG235inj1e-21/';
% skymapsPath = '../SG820inj1e-20/';
% skymapsPath = '../GAUSS4inj1e-20/';
% skymapsPath = '/archive/home/psutton/coherent-network/Porter/SG820inj1e-20/';
% skymapsPath = '/archive/home/psutton/coherent-network/Porter/SG235inj1e-21/';
% newMapPath = '/archive/home/psutton/coherent-network/qfollowup/projectIb_v3/output/';
% skymapsPath = [newMapPath 'simulations_mdc_a1b2g1_1.76/'];
% skymapsPath = [newMapPath 'simulations_mdc_a2b4g1_2.34/'];
% skymapsPath = [newMapPath 'simulations_mdc_gauss1_7.2e-22/'];
% skymapsPath = [newMapPath 'simulations_mdc_gauss4_2.76e-21/'];
% skymapsPath = [newMapPath 'simulations_mdc_sg235_5e-22/'];
% skymapsPath = [newMapPath 'simulations_mdc_sg820_1.02e-2/'];

% ---- Injections info
% injectionFile = '../earthbasedcoordinates.txt';
injectionFile = [skymapsPath 'injall_gps_delay_skypos_pol.txt'];

% ---- Number of time-frequency pixels summed over to make each sky map value
numberOfFreqBins = 61;

% ---- Whether to use the Enull minimization method or bayesian
% method = 'random';
% method = 'minimization';
method = 'bayesian';

% ---- Use lowest testFraction percentile of the skymap values for determining sky positions
% testFraction = 0.005;
testFraction = 0.0013;

% ---- Scan all polarization angles from 0-90 deg in this step
% polarizationErrStep = 0; % assume correct polarization
polarizationErrStep = 10.0 * pi/180;

% ---- Test a small 3x3 grid around each of the lowest null energy positions.
%	   If gridSpacing is 0, don't use a grid
gridSpacing = 0;
% gridSpacing = 1.0 * pi/180;

% ---- spacing of the interpolated grid
gridInterpSpacing = 0;
% gridInterpSpacing = 0.2 * pi/180;

% ---- Specify network.  Has to match what's specified in parameterFile!
channels = {'H1','L1','V1'};

% ---- Skymaps to test
% testSkymapNumbers = [1:1259];
% testSkymapNumbers = [1:699]; % for GAUSS1 injection set
% testSkymapNumbers = [1:100:1259]; 
% testSkymapNumbers = [1:5];
% testSkymapNumbers = [1:300];

% ---- pick the correct waveform corresponding to the paths specified above
% waveformStr = 'GAUSS1';
% tcIndices = tripleCoincident('../triple_source/VHL:GAUSS1_EGC_source.dat', '../injections.txt');

% waveformStr = 'GAUSS4';
% tcIndices = tripleCoincident('../triple_source/VHL:GAUSS4_EGC_source.dat', '../injections.txt');

% waveformStr = 'SG235';
% tcIndices = tripleCoincident('../triple_source/VHL:Q15F235_EGC_source.dat', '../injections.txt');

% waveformStr = 'SG820';
% tcIndices = tripleCoincident('../triple_source/VHL:Q15F820_EGC_source.dat', '../injections.txt');

% waveformStr = 'DFMA1B2G1';
% tcIndices = tripleCoincident('../triple_source/VHL:A1B2G1_EGC_source.dat', '../injections.txt');

% waveformStr = 'DFMA2B4G1';
% tcIndices = tripleCoincident('../triple_source/VHL:A2B4G1_EGC_source.dat', '../injections.txt');

% testSkymapNumbers = tcIndices;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(savePath);
mkdir(savePath);

if saveHTML
	mkdir(HTMLSavePath);
end

disp('Reading injection file...');
% startTime = 0;
% duration = 1e10;
% injectionParameters = readinjectionfile(startTime,duration,injectionFile);
% 
% numberOfInjections = length(injectionParameters);
% 
% disp('Parsing injection file...');
% [gps_s, gps_ns, phiInjs, thetaInjs, psiInjs, name, parameters] = ...
% 	parseinjectionparameters(injectionParameters);

injectionParameters = dlmread(injectionFile);
numberOfInjections = size(injectionParameters, 1);
testSkymapNumbers = [1:numberOfInjections];

% ---- Storage for the pointing errors
thetaErrs = NaN(numberOfInjections,1);
phiErrs = NaN(numberOfInjections,1);
psiErrs = NaN(numberOfInjections,1);
pointingErrs = NaN(numberOfInjections,1);

% ---- Given the test sky positions, what is the minimum pointing error we could get?
% ---- That is, how close is the closest test direction to the true source direction
% ---- This is important because it tells us two things:
% ---- 1. If the pointingErrs are larger than the minPointingErrs, method not optimal
% ---- 2. If the minPointingErrs are large, the method never had a good chance
%			because the match works best the test position is very close
%			to the source position
minPointingErrs = NaN(numberOfInjections,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate autocorrelation functions for each pair of baselines
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sampleFrequency = 16384;
nyquistFrequency = sampleFrequency/2;
F = [1:nyquistFrequency]';
numberOfDetectors = length(channels);

% ---- KLUDGE: these should probably be options (including step func. below)
% [t,h] = xmakewaveform('G',[1e-20 1e-3],1,0.5,sampleFrequency);
% [t,h] = xmakewaveform('G',[1e-20 4e-3],1,0.5,sampleFrequency);
% [t,h] = xmakewaveform('CG',[1e-21 15/(2^0.5*pi*235) 235],1,0.5,sampleFrequency);
[t,h] = xmakewaveform('CG',[5e-22 15/(2^0.5*pi*235) 235],1,0.5,sampleFrequency);
% [t,h] = xmakewaveform('CG',[1e-20 15/(2^0.5*pi*820) 820],1,0.5,sampleFrequency);
% [t,h] = xmakewaveform('DFM','1~signal_A1B2G1_R.dat',1,0.5,sampleFrequency);
% [t,h] = xmakewaveform('DFM','1~A1B2G1',1,0.5,sampleFrequency);
% [t,h] = xmakewaveform('DFM','1~signal_A2B4G1_R.dat',1,0.5,sampleFrequency);
% ---- good approximation for any sharp burst < ~2ms
% [t,h] = xmakewaveform('G',[1e-20 1e-4],1,0.5,sampleFrequency);

% ---- storage
S = NaN(nyquistFrequency,numberOfDetectors);
sourceSNR2 = NaN(numberOfDetectors,1);
autocorr = [];
warning off; % -- SRD spits out a warning. Ignore it.

% ---- calculate power spectrums
for iChannel = 1:numberOfDetectors
	if isequal(channels{iChannel}, 'V1')
		% S(:,iChannel) = SRD('Virgo-LV',F);
		S(:,iChannel) = SRD('LIGO-LV',F);
	else
		S(:,iChannel) = SRD('LIGO-LV',F);
	end
	[SNR, h_rss, h_peak, Fchar, bw, Tchar, dur] = ...
		xoptimalsnr(h,0,sampleFrequency,S(:,iChannel),F(1),1,10,nyquistFrequency);
	sourceSNR2(iChannel) = SNR;
end

% ---- calculate autocorrelations
for iChannel = 1:numberOfDetectors-1
    for jChannel = iChannel+1:numberOfDetectors
		iAutocorr = InnerProduct(h,h,sampleFrequency,(S(:,iChannel).*S(:,jChannel)).^0.5,F(1),1,10,nyquistFrequency);
		iAutocorr = fftshift(iAutocorr)/max(iAutocorr);
		autocorr = [autocorr iAutocorr];
	end
end

warning on;
middleSample = length(iAutocorr)/2 + 1;
autocorrTime = ([1:length(iAutocorr)].'-middleSample)/sampleFrequency;

% ---- step function
if 0
	signalPeriod = 5e-4;
	autocorr = abs(autocorrTime) < signalPeriod;
	autocorr = repmat(autocorr, 1, 3);
end

% plot(autocorrTime, autocorr(:,1), autocorrTime, autocorr(:,3));
% legend('LIGO-LIGO','LIGO-Virgo');
% xlim([-0.03 0.03]);
% break;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loop over skymap files
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for nSkymap = 1:length(testSkymapNumbers);
	iSkymap = testSkymapNumbers(nSkymap);
	
	disp(['opening results_' num2str(nSkymap-1) '.mat']);
	
	try
		% f = open([skymapsPath 'results_' num2str(iSkymap-1) '.mat']);
		f = open([skymapsPath 'results_' num2str(nSkymap-1) '.mat']);
	catch
        warning([skymapsPath 'results_' num2str(nSkymap-1) '.mat missing']);
        continue;
    end
	
	% ---- get skyPositions
	
	thetas = f.skyPositions(:, 1);
	phis = f.skyPositions(:, 2);
	dOmegas = f.skyPositions(:,4); % solid angles
	nSkyPositions = size(f.skyPositions, 1);
	
	% ---- the sourcePositions variable in the skymaps is wrong.
	%	   must read from our injection file.
	
	if(isfield(f,'centerTime'))
		k = find(injectionParameters(:,1) == f.centerTime);
		phi0 = injectionParameters(k,5);
		theta0 = injectionParameters(k,6);
		psi0 = injectionParameters(k,7);
		% ra = 4.64964 * 180/pi;
    	% dec = -0.505031 * 180/pi;
    	% [phi0, theta0] = radectoearth(ra,dec,f.centerTime);
    else
		theta0 = thetaInjs(iSkymap);
		phi0 = phiInjs(iSkymap);
		psi0 = psiInjs(iSkymap);
	end
	
	% if(isfield(f,'likelihoodMapCell'))
	% 	fMin = f.likelihoodMapCell{1}(:,4);
	% 	fPeak = f.likelihoodMapCell{1}(:,5);
	%     fMax = f.likelihoodMapCell{1}(:,6);
	%     numberOfPixels = length(f.likelihoodMapCell{1});
	% end
	
	% ---- cartesian coordinates
	sourceOmega = [sin(theta0).*cos(phi0),sin(theta0).*sin(phi0),cos(theta0)];
	sourceOmegaTheta = [cos(theta0), sin(theta0)];
	sourceOmegaPhi = [cos(phi0), sin(phi0)];
	
	if(isfield(f,'nullEnergyMap'))
		Enull0 = squeeze(f.nullEnergyMap);
		Einc0 = squeeze(f.incoherentEnergyMap);
	else
		nullEnergyIndex = 7 + find(strcmp(f.likelihoodType(:),'nullenergy'));
	    incEnergyIndex = 7 + find(strcmp(f.likelihoodType(:),'incoherentenergy'));
	    slIndex = 7 + find(strcmp(f.likelihoodType(:),'standard'));
		Enull0 = f.likelihoodMapCell{1}(:,nullEnergyIndex);
		Einc0 = f.likelihoodMapCell{1}(:,incEnergyIndex);
	end
	measuredMap = Enull0./Einc0 - 1;
	
	if isequal(method, 'random')
		thetaMax = pi*rand();
		phiMax = 2*pi*rand() - pi;
		psiMax = pi*rand();
	elseif isequal(method, 'minimization')
		[minValue, minIndex] = min(Enull0);
		thetaMax = thetas(minIndex);
		phiMax = phis(minIndex);
		psiMax = NaN;
	else
		% ---- Test sky positions: 
		testIndices = find(measuredMap <= prctile(measuredMap, testFraction*100));
		% testIndices = find(Enull0 <= prctile(Enull0, testFraction*100));
		nTestSourcePositions = length(testIndices);
		testThetas = thetas(testIndices);
		testPhis = phis(testIndices);
		testSourcePositions = [testThetas testPhis];
		
		% ---- cartesian coordinates
		testOmegas = [sin(testThetas).*cos(testPhis),sin(testThetas).*sin(testPhis),cos(testThetas)];

		% ---- test sky position pointing errors
		testPointingErrs = real(acos(dot(repmat(sourceOmega, nTestSourcePositions, 1), ...
			testOmegas, 2)));
	
		minPointingErrs(iSkymap) = min(testPointingErrs);
		% continue;
		
		% ---- Vector of test source polarizations.  Note that changing the
		%      polarization by pi/2 is equivalent to changing the sign of the
		%      waveform; therefore need only test [0,pi/2).
		if polarizationErrStep == 0
			testSourcePolarizations = psi0;
		else
			testSourcePolarizations = [0:polarizationErrStep:pi/2];
		end
		
		if gridSpacing == 0
			% ---- calculate predicted skymaps
			[Enull1, Einc1, Etot1] = predictSkyMap(testSourcePositions, ...
				testSourcePolarizations,sourceSNR2,channels,numberOfFreqBins,f.skyPositions, ...
				autocorr, autocorrTime);
			% [Enull1, Einc1] = predictSkyMapFD(testSourcePositions,h_rss,fMin,fMax, ...
			%     numberOfPixels,f.skyPositions,channels,S,F,'linearpolarization',testSourcePolarizations);
			
			predictedMap = Enull1./Einc1 - 1;
			match = zeros(1,size(predictedMap,2),size(predictedMap,3));
			
			% measuredUnitMap = transpose(measuredMap) / norm(measuredMap);
			% for jPol = 1:size(predictedMap,3)
			%     match(1,:,jPol) = (measuredUnitMap * predictedMap(:,:,jPol)) ...
			%     					./ sum(predictedMap(:,:,jPol).^2,1).^(0.5);
			% end
			
			measuredUnitMap = measuredMap / norm(measuredMap);
			for jPol = 1:size(predictedMap,3)
				for jSource = 1:size(predictedMap,2)
					predictedUnitMap = predictedMap(:,jSource,jPol) / norm(predictedMap(:,jSource,jPol));
					match(1,jSource,jPol) = -sum(dOmegas.*(measuredUnitMap - predictedUnitMap).^2);
				end
			end
			
            % ---- maximize over polarizations
            [maxOverPsi, maxPsiIndices] = max(match, [], 3);
            % ---- marginalize polarization by adding match for each
            %      polarization
            % maxOverPsi = sum(exp(-match), 3);

            [maxMatch, maxIndex] = max(maxOverPsi);
			thetaMax = testSourcePositions(maxIndex, 1);
			phiMax = testSourcePositions(maxIndex, 2);
			% psiMax = psi0; % KLUDGE
			% psiMax = NaN; % KLUDGE
			psiMax = testSourcePolarizations(maxPsiIndices(maxIndex));
		else
			% ---- use small grids and interpolation
			
			% ---- initial value to be incrementally maximized
			maxMatch = -Inf;
			
			% ---- loop over all test positions and maximize on a small grid around each position
			for iPos = 1:nTestSourcePositions
				% ---- Add a small 3x3 grid around each of the lowest null energy positions.
				if gridSpacing > 0
					[thetaMat, phiMat] = meshgrid( ...
						testThetas(iPos) + [-gridSpacing, 0, gridSpacing], ...
						testPhis(iPos)   + [-gridSpacing, 0, gridSpacing]);
					gridThetas = thetaMat(:);
					gridPhis = phiMat(:);
				end
		
				gridSourcePositions = [gridThetas gridPhis];
				
				% ---- calculate predicted skymaps
				[Enull1, Einc1, Etot1] = predictSkyMap(gridSourcePositions, ...
					testSourcePolarizations,sourceSNR2,channels,numberOfFreqBins,f.skyPositions, ...
					autocorr, autocorrTime);
		
				predictedMap = Enull1./Einc1 - 1;

				match = zeros(1,size(predictedMap,2),size(predictedMap,3));
				measuredUnitMap = transpose(measuredMap) / norm(measuredMap);
				for jPol = 1:size(predictedMap,3)
				    match(1,:,jPol) = (measuredUnitMap * predictedMap(:,:,jPol)) ...
				    					./ sum(predictedMap(:,:,jPol).^2,1).^(0.5);
				end
		
				% ---- maximize over polarizations
				match2D = max(match, [], 3);
				match2D = reshape(match2D, 3, 3);
				% ---- interpolation
				gridThetaRange = testThetas(iPos) + [-gridSpacing, 0, gridSpacing];
				gridPhiRange = testPhis(iPos) + [-gridSpacing, 0, gridSpacing];
				if gridInterpSpacing == 0
					gridInterpThetaRange = gridThetaRange;
					gridInterpPhiRange = gridPhiRange;
				else
					gridInterpThetaRange = testThetas(iPos) + [-gridSpacing:gridInterpSpacing:gridSpacing];
					gridInterpPhiRange = testPhis(iPos) + [-gridSpacing:gridInterpSpacing:gridSpacing];
				end
				[interpThetaMat, interpPhiMat] = meshgrid(gridInterpThetaRange, gridInterpPhiRange);
				gridInterpThetas = interpThetaMat(:);
				gridInterpPhis = interpPhiMat(:);
				matchInterp2D = interp2(gridThetaRange, gridPhiRange, match2D, gridInterpThetaRange, gridInterpPhiRange', 'spline');
			
				[maxGridMatch, maxIndex] = max(matchInterp2D(:));
				gridThetaMax = gridInterpThetas(maxIndex);
				gridPhiMax = gridInterpPhis(maxIndex);
				
				if maxGridMatch > maxMatch
					thetaMax = gridThetaMax;
					phiMax = gridPhiMax;
					maxMatch = maxGridMatch;
					psiMax = NaN; % KLUDGE
				end
			end
		end
	end

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Get values for maximum of PDF, calculate errors
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	maxOmega = [sin(thetaMax).*cos(phiMax),sin(thetaMax).*sin(phiMax),cos(thetaMax)];
	maxOmegaTheta = [cos(thetaMax), sin(thetaMax)];
	maxOmegaPhi = [cos(phiMax), sin(phiMax)];
	
	pointingErrs(iSkymap) = real(acos(dot(sourceOmega, maxOmega)));
	thetaErrs(iSkymap) = real(acos(dot(sourceOmegaTheta, maxOmegaTheta)));
	phiErrs(iSkymap) = real(acos(dot(sourceOmegaPhi, maxOmegaPhi)));
	psiErrs(iSkymap) = abs(psiMax - psi0);
	
	errs = [pointingErrs(iSkymap), thetaErrs(iSkymap), phiErrs(iSkymap), psiErrs(iSkymap)];
	% disp([theta0, phi0]);
	% disp([thetaMax, phiMax]);
	% disp([sourceOmegaTheta, sourceOmegaPhi]);
	% disp([maxOmegaTheta, maxOmegaPhi]);
	disp(errs);
		
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Save html for this skymap
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	if saveHTML
		% ---- set up transform for plots
		mapstruct = defaultm('mollweid');		
		mapstruct.angleunits = 'radians';
		mapstruct.origin = [0 0 0];
		mapstruct.falseeasting = 0;
		mapstruct.falsenorthing = 0;
		mapstruct.scalefactor = 1;
		
		% ---- calculate predicted sky map at true source location
		[Enull, Einc, Etot] = predictSkyMap([theta0 phi0], ...
			psi0,sourceSNR2,channels,numberOfFreqBins,f.skyPositions, ...
			autocorr, autocorrTime);
		% [Enull, Einc] = predictSkyMapFD([theta0 phi0],h_rss,fMin(1),fMax(1), ...
		%     numberOfPixels,f.skyPositions,channels,S,F,'linearpolarization',psi0);
		predictedMap = Enull./Einc - 1;
		predictedUnitMap = transpose(predictedMap) / norm(predictedMap);
		
		cMin = min(min(measuredUnitMap), min(predictedUnitMap));
		cMax = max(max(measuredUnitMap), max(predictedUnitMap));
			  
		% ---- save images
		xproject(f.skyPositions,measuredUnitMap,'mollweid',channels);
		colorbar; set(gca, 'clim', [cMin, cMax]);
		hold on;
		[x0, y0] = mfwdtran(mapstruct, pi / 2 - theta0, phi0);
		plot3(x0, y0, 0.1, 'ko', 'markerfacecolor', 'm', 'markersize', 4)
		
		imgPath = [HTMLSavePath 'skymap_' num2str(iSkymap) '.png'];
		saveas(gcf, imgPath);
		system(['convert -trim ' imgPath ' ' imgPath]);
		
		
		xproject(f.skyPositions,predictedUnitMap,'mollweid',channels);
		colorbar; set(gca, 'clim', [cMin, cMax]);
		hold on;
		plot3(x0, y0, 0.1, 'ko', 'markerfacecolor', 'm', 'markersize', 4)
		
		imgPath = [HTMLSavePath 'skymap_' num2str(iSkymap) '_pred.png'];
		saveas(gcf, imgPath);
		system(['convert -trim ' imgPath ' ' imgPath]);
		
		[Enull, Einc, Etot] = predictSkyMap([thetaMax phiMax], ...
			psiMax,sourceSNR2,channels,numberOfFreqBins,f.skyPositions, ...
			autocorr, autocorrTime);
		% [Enull, Einc] = predictSkyMapFD([thetaMax phiMax],h_rss,fMin(1),fMax(1), ...
		%     numberOfPixels,f.skyPositions,channels,S,F,'linearpolarization',psiMax);
		predictedMap = Enull./Einc - 1;
		predictedUnitMap = transpose(predictedMap) / norm(predictedMap);
		
		xproject(f.skyPositions,predictedUnitMap,'mollweid',channels);
		colorbar; set(gca, 'clim', [cMin, cMax]);
		hold on;
		[xMax, yMax] = mfwdtran(mapstruct, pi / 2 - thetaMax, phiMax);
		plot3(xMax, yMax, 0.1, 'ko', 'markerfacecolor', 'm', 'markersize', 4)
		
		imgPath = [HTMLSavePath 'skymap_' num2str(iSkymap) '_best_pred.png'];
		saveas(gcf, imgPath);
		system(['convert -trim ' imgPath ' ' imgPath]);
		
		xproject(f.skyPositions,Enull0,'mollweid',channels);
		% title('measured E\_null');
		colorbar; set(gca, 'clim', [min(Enull0), max(Enull0)]);
		hold on;
		plot3(x0, y0, 0.04, 'ws', 'markerfacecolor', 'm', 'markersize', 3)
		plot3(xMax, yMax, 0.04, 'wo', 'markerfacecolor', 'g', 'markersize', 3)
		for i=1:nTestSourcePositions
			[x, y] = mfwdtran(mapstruct, pi / 2 - testSourcePositions(i,1), testSourcePositions(i,2));
			plot3(x, y, 0.05, 'ko', 'markerfacecolor', 'g', 'markersize', 2)
		end
		
		imgPath = [HTMLSavePath 'skymap_' num2str(iSkymap) '_testPositions.png'];
		saveas(gcf, imgPath);
		system(['convert -trim ' imgPath ' ' imgPath]);
		
		% ---- save html
		% ---- if this is the 1st on a page, create new file
		if ((mod(nSkymap-1, nSkymapsPerPage)) == 0)
			if nSkymap-1 == 0
				filename = 'index.html';
			else
				fprintf(fid, '</table>\n</body>\n</html>');
				fclose(fid);
				filename = ['index_' num2str(1+(nSkymap-1)/nSkymapsPerPage) '.html'];
			end
			fid = fopen([HTMLSavePath filename], 'w');
			fprintf(fid, ['<html>\n<body>Page: \n']);
			for iPage = 1:ceil(length(testSkymapNumbers)/nSkymapsPerPage)
				if iPage == 1
					filename = 'index.html';
				else
					filename = ['index_' num2str(iPage) '.html'];
				end
				fprintf(fid, ['<a href=\"' filename '\">[' num2str(iPage) ']</a> \n']);
			end
			fprintf(fid, ['<table>\n<tr>\n<th>measured</th>\n' ...
				'<th>predicted</th>\n<th>best predicted</th>\n<th>test positions</th>\n<th>errors</th>\n</tr>\n']);
		end
		
		if pointingErrs(iSkymap) <= pi/4
			color = 'green';
		elseif pointingErrs(iSkymap) <= 2*pi/4
			color = 'yellow';
		elseif pointingErrs(iSkymap) <= 3*pi/4
			color = 'orange';
		elseif pointingErrs(iSkymap) <= 4*pi/4
			color = 'red';
		end
		
		fprintf(fid, ['<tr bgcolor=\"' color '\">\n<td width=\"20%%\">' ...
			'<a href=\"skymap_' num2str(iSkymap) '.png\">' ...
			'<img src=\"skymap_' num2str(iSkymap) '.png\" width=\"100%%\"></td>\n']);
		fprintf(fid, ['<td width=\"20%%\">' ...
			'<a href=\"skymap_' num2str(iSkymap) '_pred.png\">' ...
			'<img src=\"skymap_' num2str(iSkymap) '_pred.png\" width=\"100%%\"></td>\n']);
		fprintf(fid, ['<td width=\"20%%\">' ...
			'<a href=\"skymap_' num2str(iSkymap) '_best_pred.png\">' ...
			'<img src=\"skymap_' num2str(iSkymap) '_best_pred.png\" width=\"100%%\"></td>\n']);
		fprintf(fid, ['<td width=\"20%%\">' ...
			'<a href=\"skymap_' num2str(iSkymap) '_testPositions.png\">' ...
			'<img src=\"skymap_' num2str(iSkymap) '_testPositions.png\" width=\"100%%\"></td>\n']);
		fprintf(fid, '<td>pointingErr: %f<br/>thetaErr: %f<br/>phiErr: %f<br />psiErr: %f<br />skymap #: %d</td>\n</tr>\n\n', ...
			[errs'; iSkymap]);
		
		% ---- if this is the last on page or very last, close file
		if ((mod(nSkymap, nSkymapsPerPage)) == 0 || (nSkymap == length(testSkymapNumbers)))
			fprintf(fid, '</table>\n</body>\n</html>');
			fclose(fid);
		end
	end
end

totalTime = toc;
disp(['total time: ' num2str(totalTime)]);

if saveData
	save([savePath 'data.mat'], 'skymapsPath', 'injectionFile', 'numberOfFreqBins', ...
		'sourceSNR2', 'method', 'testFraction', 'polarizationErrStep', ...
		'channels', 'testSkymapNumbers', 'totalTime', ...
		'thetaErrs', 'phiErrs', 'pointingErrs', 'minPointingErrs');
end
