% $Id$

% This script calculates the pdf for a grid of theta, phi, assuming psi

% script to test the known skymap model version of the bayesian formulation for source direction localization
% loop over sky positions & polarizations
% - calculate predicted skymap along rings of constant time delay
% - calculate probability that source direction matches test direction
% result: contour plot of the probability distribution

% predictSkyMap - type analysis.  Use some simple signal model (predictSkyMap assumes
%	a linearly polarized delta pulse) to predict the sky map as a function of true source position.
%	Compute the match between the predicted L_pred(omega,omega_test) and the measured L(omega), which will be approximately
% 
%   mismatch(omega_test) = \sum_omega | L_pred(omega,omega_test) - L(omega) |^2
% 
% The source location is estimated as the omega_test with the smallest mismatch value.
% 
% References:
% Your mathematica notebook.
% 
% Advantages:
% - If the signal model is valid, you can use the full sky map, averaging over all the noise
%	and including antenna responses correctly.
% 
% Disadvantages:
% - We're back to assuming a particular signal model.  For example, predictSkyMap may go very wrong
%	 if the signal is oscillatory (it may get the destructive and constructive interference mixed up).

% clear workspace
% clear
format short

savePath = '../surface_101/';

% ---- Path to the skymaps
skymapsPath = '../DFM_A1B2G1inj10/';
% skymapsPath = '../SG235inj1e-21/';
% skymapsPath = '/archive/home/psutton/coherent-network/Porter/DFM_A1B2G1inj10/';
% skymapsPath = '/archive/home/psutton/coherent-network/Porter/SG235inj1e-21/';

% ---- Injections info
injectionFile = '../earthbasedcoordinates.txt';

% ---- Number of time-frequency pixels summed over to make each sky map value
numberOfFreqBins = 61;

% pointing errors for loop over test sky positions
maxPointingErr = 30.0 * pi/180;
pointingErrStep = 1.0 * pi/180;

% ---- Scan all polarization angles from 0-90 deg in this step
polarizationErrStep = 0; % assume correct polarization
% polarizationErrStep = 10.0 * pi/180;

% ---- Specify network.  Has to match what's specified in parameterFile!
channels = {'H1','L1','V1'};

% ---- Skymap to test
% testSkymapNumbers = 1;
testSkymapNumbers = 500;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(savePath);
mkdir(savePath);

disp('Reading injection file...');
startTime = 0;
duration = 1e10;
injectionParameters = readinjectionfile(startTime,duration,injectionFile);

numberOfInjections = length(injectionParameters);

disp('Parsing injection file...');
[gps_s, gps_ns, phiInjs, thetaInjs, psiInjs, name, parameters] = ...
	parseinjectionparameters(injectionParameters);

% ---- Storage for the pointing errors
thetaErrs = NaN(numberOfInjections,1);
phiErrs = NaN(numberOfInjections,1);
psiErrs = NaN(numberOfInjections,1);
pointingErrs = NaN(numberOfInjections,1);

% ---- Given the test sky positions, what is the minimum pointing error we could get?
% ---- That is, how close is the closest test direction to the true source direction
% ---- This is important because it tells us two things:
% ---- 1. If the pointingErrs are larger than the minPointingErrs, method not optimal
% ---- 2. If the minPointingErrs are large, the method never had a good chance
%			because the match works best the test position is very close
%			to the source position
minPointingErrs = NaN(numberOfInjections,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate autocorrelation functions for each pair of baselines
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sampleFrequency = 16384;
nyquistFrequency = sampleFrequency/2;
F = [1:nyquistFrequency]';
numberOfDetectors = length(channels);

[t,h] = xmakewaveform('DFM','1~signal_A1B2G1_R.dat',1,0.5,sampleFrequency);
% [t,h] = xmakewaveform('SG',[1e-20 5e-3 820],1,0.5,sampleFrequency);
% [t,h] = xmakewaveform('CG',[1e-21 15/(2^0.5*pi*235) 235],1,0.5,sampleFrequency);
% ---- good approximation for any sharp burst < ~2ms
% [t,h] = xmakewaveform('G',[1e-20 1e-4],1,0.5,sampleFrequency);

% ---- storage
S = NaN(nyquistFrequency,numberOfDetectors);
sourceSNR2 = NaN(numberOfDetectors,1);
autocorr = [];
warning off; % -- SRD spits out a warning. Ignore it.

% ---- calculate power spectrums
for iChannel = 1:numberOfDetectors
	if isequal(channels{iChannel}, 'V1')
		S(:,iChannel) = SRD('Virgo-LV',F);
	else
		S(:,iChannel) = SRD('LIGO-LV',F);
	end
	[SNR, h_rss, h_peak, Fchar, bw, Tchar, dur] = ...
		xoptimalsnr(h,0,sampleFrequency,S(:,iChannel),F(1),1,10,nyquistFrequency);
	sourceSNR2(iChannel) = SNR;
end

% ---- calculate autocorrelations
for iChannel = 1:numberOfDetectors-1
    for jChannel = iChannel+1:numberOfDetectors
		iAutocorr = InnerProduct(h,h,sampleFrequency,(S(:,iChannel).*S(:,jChannel)).^0.5,F(1),1,10,nyquistFrequency);
		iAutocorr = fftshift(iAutocorr)/max(iAutocorr);
		autocorr = [autocorr iAutocorr];
	end
end

warning on;
middleSample = length(iAutocorr)/2 + 1;
autocorrTime = ([1:length(iAutocorr)].'-middleSample)/sampleFrequency;

% ---- step function
% signalPeriod = 5e-4;
% autocorr = abs(autocorrTime) < signalPeriod;
% autocorr = repmat(autocorr, 1, 3);

% plot(autocorrTime, autocorr(:,1), autocorrTime, autocorr(:,3));
% legend('LIGO-LIGO','LIGO-Virgo');
% xlim([-0.03 0.03]);
% break;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loop over skymap files
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for nSkymap = 1:length(testSkymapNumbers);
	iSkymap = testSkymapNumbers(nSkymap);
	
	disp(['opening results_' num2str(iSkymap-1) '.mat']);
	
	try
		f = open([skymapsPath 'results_' num2str(iSkymap-1) '.mat']);
	catch
        warning([skymapsPath 'results_' num2str(iSkymap-1) '.mat missing']);
        continue;
    end
	
	% ---- get skyPositions
	thetas = f.skyPositions(:, 1);
	phis = f.skyPositions(:, 2);
	dOmegas = f.skyPositions(:,4); % solid angles
	nSkyPositions = size(f.skyPositions, 1);
	
	% ---- the sourcePositions variable in the skymaps is wrong.
	%	   must read from our injection file.
	theta0 = thetaInjs(iSkymap);
	phi0 = phiInjs(iSkymap);
	psi0 = psiInjs(iSkymap);
	
	Enull0 = squeeze(f.nullEnergyMap);
	Einc0 = squeeze(f.incoherentEnergyMap);
	measuredMap = Enull0./Einc0 - 1;
	
	% figure;
	% subplot(1,2,1);
	% xproject(f.skyPositions,measuredMap,'sphere',channels);
	% subplot(1,2,2);
	% xproject(f.skyPositions,measuredMap,'mollweid',channels);
	% set(gca,'XTick',[]);
	% set(gca,'YTick',[]);
	% % xlabel('\phi');
	% % ylabel('\theta','Rotation',0);
	% hc = colorbar;
	% set(hc,'OuterPosition',[0.02 0.5/2 0.15 0.5]);
	% xlabel(hc,'null/inc-1');
	% drawnow;

	% saveas(gcf, [savePath 'skymap.png']);
	% saveas(gcf, [savePath 'skymap.eps'], 'epsc2');

	% storage
	logPDFs = [];

	% ---- Test sky positions: Square grid centered on correct position.
	[theta,phi] = meshgrid( ...
	    theta0 + [-maxPointingErr:pointingErrStep:maxPointingErr], ...
	    phi0 + [-maxPointingErr:pointingErrStep:maxPointingErr]);
	testSourcePositions = [theta(:), phi(:)];
	
	
	% ---- Vector of test source polarizations.  Note that changing the
	%      polarization by pi/2 is equivalent to changing the sign of the
	%      waveform; therefore need only test [0,pi/2).
	if polarizationErrStep == 0
		testSourcePolarizations = psi0;
	else
		testSourcePolarizations = [0:polarizationErrStep:pi/2];
	end

	[Enull1, Einc1, Etot1] = predictSkyMap(testSourcePositions, ...
		testSourcePolarizations,sourceSNR2,channels,numberOfFreqBins,f.skyPositions, ...
		autocorr, autocorrTime);

	predictedMap = Enull1./Einc1 - 1;
	
	match = zeros(1,size(predictedMap,2),size(predictedMap,3));
	
	measuredUnitMap = transpose(measuredMap) / norm(measuredMap);
	for jPol = 1:size(predictedMap,3)
	    match(1,:,jPol) = (measuredUnitMap * predictedMap(:,:,jPol)) ...
	    					./ sum(predictedMap(:,:,jPol).^2,1).^(0.5);
	end
	
	logPDFs = match(1,:,1);

	maxLogPDF = max(logPDFs);
	PDFs = exp(logPDFs - maxLogPDF);
	PDFmatrix = reshape(PDFs,length(theta),length(phi));

	figure;
	title('Bayesian probability distribution');
	%colorbar;
	subplot(1,2,1);
	surf((theta-theta0)*180/pi,(phi-phi0)*180/pi,PDFmatrix);
	xlim([-maxPointingErr maxPointingErr]*180/pi);
	ylim([-maxPointingErr maxPointingErr]*180/pi);
	xlabel('\theta error (deg)');
	ylabel('\phi  error (deg)');
	shading('interp');
	view([0 0 1]);
	axis image;
	subplot(1,2,2);
	surf((theta-theta0)*180/pi,(phi-phi0)*180/pi,PDFmatrix);
	xlim([-maxPointingErr maxPointingErr]*180/pi);
	ylim([-maxPointingErr maxPointingErr]*180/pi);
	xlabel('\theta error (deg)');
	ylabel('\phi  error (deg)');
	zlabel('pdf','rotation',0);
	shading('interp');
	% view([0 -1 0]);
	axis square;
	
	saveas(gcf, [savePath 'surface.png']);
	saveas(gcf, [savePath 'surface.fig']);
	saveas(gcf, [savePath 'surface.eps'], 'epsc2');
	
	% xproject complains about arguments, so this only works
	%  if you uncomment the check in xproject
	if 0 && maxPointingErr == 180
		figure;
		subplot(1,2,1);
		xproject([theta(:) phi(:)],PDFmatrix,'sphere',channels);
		subplot(1,2,2);
		xproject([theta(:) phi(:)],PDFmatrix,'mollweid',channels);
		set(gca,'XTick',[]);
		set(gca,'YTick',[]);
		% xlabel('\phi');
		% ylabel('\theta','Rotation',0);
		hc = colorbar;
		set(hc,'OuterPosition',[0.02 0.5/2 0.15 0.5]);
		xlabel(hc,'pdf');
		drawnow;
    
		saveas(gcf, [savePath 'skymap_pdf.png']);
		saveas(gcf, [savePath 'skymap_pdf.eps'], 'epsc2');
	end
	
end

save([savePath 'data.mat'], 'savePath', 'skymapsPath', 'testSkymapNumbers', ...
	'maxPointingErr', 'pointingErrStep', ...
	'theta', 'phi', 'theta0', 'phi0', 'PDFmatrix');

