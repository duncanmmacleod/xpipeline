%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script to plot distributions and other things for comparing
% two different source direction estimation methods.
% Uses the output of bayesianSourceDirection_singleParemeter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% display theta, phi, psi errors on one plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

prefix = '../singleVariable_101';
legendStrs = {'\theta','\phi','\psi'};

titleStr = 'DFM\_A1B2G1inj10 waveform, injection #500';

fp = open([prefix '/data.mat']);
figure;
colorOrder = get(gca,'ColorOrder');
title(titleStr);
ylabel('Bayesian probability distribution');
xlabel(['error (deg)']);
hold on;

for i = 1:3	
	iVarName = fp.variables{i};
	varRange = fp.ranges{i};
	optimalValue = fp.optimalValues(i);
	PDF = fp.PDFs{i};
	plot((varRange-optimalValue)*180/pi, PDF, 'o-', 'Color', colorOrder(mod(i,length(colorOrder))+1,:,:));
end

legend(legendStrs);
xlim([-20 20]);

saveas(gcf, [prefix '/errors_combined.png']);
saveas(gcf, [prefix '/errors_combined.eps'], 'epsc2');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compare a single theta, phi, or psi error from multiple runs on one plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

prefix = '../singleVariable_test';
plotNums = [9 10];

% which variable in the PDF cell array
PDFindex = 3;

% legendStrs = {'injection scale 30', 'injection scale 20', 'injection scale 10'};
legendStrs = {'Enull', 'Enull/Einc'};

% titleStr = 'test\_lazarus waveform, Enull/Einc';
titleStr = 'lazarus\_set2 waveform';

for i = 1:length(plotNums)
	iNum = plotNums(i);
	fp = open([prefix num2str(iNum) '/data.mat']);
	if(i == 1)
		figure;
		colorOrder = get(gca,'ColorOrder');
	end
	
	iVarName = variables{PDFindex};
	varRange = fp.ranges{PDFindex};
	optimalValue = fp.optimalValues(PDFindex);
	PDF = fp.PDFs{PDFindex};
	plot((varRange-optimalValue)*180/pi, PDF, '-', 'Color', colorOrder(mod(i,length(colorOrder))+1,:,:));
	
	if(i == 1)
		title(titleStr);
		ylabel('Bayesian probability distribution');
		xlabel([iVarName ' error (deg)']);
		hold on;
	end
	
	% legendStrs{i} = ['test ' num2str(plotNums(i))];
end

legend(legendStrs);