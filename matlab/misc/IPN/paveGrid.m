function [alphaLeft,alphaRight,decLeft,decRight,edgeFlag] = paveGrid(dec,ALPHA,DEC,RADIUS)
% October 18, 2010
% I. Leonor

EDGETOL = 10.0;

cosDelAlpha = (cosd(RADIUS) - sind(dec)*sind(DEC))./(cosd(dec)*cosd(DEC));

%=== Make sure absolute value of cosine is less than or equal to 1 ===%
ifind       = find(abs(cosDelAlpha) <= 1.00005);
cosDelAlpha = cosDelAlpha(ifind);
dec         = dec(ifind);
delAlpha    = acosd(cosDelAlpha);

%=== Make sure range of dec is from -90 to +90 ===%
ifind           = find(dec > 90);
dec(ifind)      = 180.0 - dec(ifind);
delAlpha(ifind) = 180.0 + delAlpha(ifind);

ifind           = find(dec < -90);
dec(ifind)      = -180.0 - dec(ifind);
delAlpha(ifind) = 180.0 + delAlpha(ifind);

%=== Construct the other half of the circle ===%
iuniq      = find(abs(delAlpha > 0.001));
alphaPlus  =  delAlpha + ALPHA;
alphaMinus = -delAlpha(iuniq) + ALPHA;

alphaAll = [alphaPlus fliplr(alphaMinus)];
decAll   = [dec fliplr(dec(iuniq))];

%=== Make sure range of alpha is from 0 to 360 ===%
ifind = find(alphaAll > 360);
alphaAll(ifind) = alphaAll(ifind) - 360;

ifindNeg = find(alphaAll < 0);
ifindPos = find(alphaAll >= 0);
alphaAll(ifindNeg) = alphaAll(ifindNeg) + 360;

edgeFlag = 0;
if abs(360.0 - (max(alphaAll) + min(alphaAll))) < EDGETOL
  edgeFlag = 3;
end

%if edgeFlag == 3
%  ifindLeft  = find(alphaAll < 180.0);
%  ifindRight = find(alphaAll > 180.0);
%
%  alphaLeft  = alphaAll(ifindLeft);
%  alphaRight = alphaAll(ifindRight);
%  decLeft    = decAll(ifindLeft);
%  decRight   = decAll(ifindRight);
%end

rHalf = mod(length(alphaAll),2);
nHalf = length(alphaAll)/2;
if rHalf > 0
  nHalf = ceil(nHalf);
end

alphaLeft  = alphaAll(1:nHalf);
alphaRight = alphaAll(nHalf+1:end);
decLeft    = decAll(1:nHalf);
decRight   = decAll(nHalf+1:end);

if max(alphaLeft) > max(alphaRight)
  alphaLeft  = alphaAll(nHalf+1:end);
  alphaRight = alphaAll(1:nHalf);
  decLeft    = decAll(nHalf+1:end);
  decRight   = decAll(1:nHalf);
end

alphaLeft  = real(alphaLeft);
alphaRight = real(alphaRight);
decLeft    = real(decLeft);
decRight   = real(decRight);



%=== End paveGrid ===%
