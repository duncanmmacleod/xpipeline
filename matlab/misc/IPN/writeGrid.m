function writeGrid(gridFile,alphaGrid,decGrid,binProb,binArea)
% WRITEGRID - write IPN grid into a given file
%
% usage : 
%
%   writeGrid(gridFile,alphaGrid,decGrid,binProb,binArea)
%
%       gridFile : string with the grid file name
%       alphaGrid : vector of right ascenscion positions
%       decGrid : vector of declination positions
%       binProb : vector of probability position for each RA-dec position
%       binArea : area covered by each sky position
                                              
% written by I. Leonor
% imported by N. Leroy                                         
                                                              

error(nargchk(3,5,nargin));

nArgs = nargin;

%=== Set default bin probability ===%
BINPROB = 1.0;

%=== Open output grid file ===%
ofp = fopen(gridFile,'w');


  alphaAll = alphaGrid;
  decAll   = decGrid;
  if nArgs == 5
    rProbAll = binProb;
    areaAll  = binArea;
  else
    if nArgs == 4
      areaAll = binArea;
    end
  end

  %=== Write grid data to output file ===%
  nPhi = length(alphaAll);
  if nArgs == 5
    for iphi = 1:nPhi
      fprintf(ofp,'%11.7f %11.7f %9.7f %11.7f\n', ...
                   alphaAll(iphi), decAll(iphi), rProbAll(iphi), areaAll(iphi));
    end
  else
    if nArgs == 4
      for iphi = 1:nPhi
        fprintf(ofp,'%11.7f %11.7f %9.7f %11.7f\n', ...
                     alphaAll(iphi), decAll(iphi), BINPROB, areaAll(iphi));
      end
    else
      for iphi = 1:nPhi
        fprintf(ofp,'%11.7f %11.7f\n', ...
                     alphaAll(iphi), decAll(iphi));
      end
    end
  end



fclose(ofp);

% ---- done
return
