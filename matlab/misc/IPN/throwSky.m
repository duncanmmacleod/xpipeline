function [alphaThrow,decThrow] = throwSky(nThrows,ALPHA,DEC,alphaGrid,decGrid,rProbGrid,DELRAD,RMIN,RMAX)
%==============================================================%
% December 12, 2010                                            %
% I. Leonor        
% Imported by N. Leroy, July 2012                                            %
%                                                              %
% Note:  nThrows should be number of throws per square degree. %
%==============================================================%

DELTHETA = DELRAD;

%=== Number of throws will be different for each theta value ===%
%===                 for constant density:                   ===%
nThrowsTheta = ceil(nThrows*(DELTHETA^2));

alphaThrow = [];
decThrow   = [];

%=== Initialize random number generator ===%
%pause(2*runid);
rTime = sum(1000*clock);
rand('state',rTime);

%=== Perform transformation to source coordinates ===%


  alphaAll = alphaGrid;
  decAll   = decGrid;
  rProbAll = rProbGrid;

  xEq = cosd(decAll).*cosd(alphaAll);
  yEq = cosd(decAll).*sind(alphaAll);
  zEq = sind(decAll);

  xSource =  xEq.*cosd(ALPHA)*sind(DEC) + yEq.*sind(ALPHA)*sind(DEC) - zEq.*cosd(DEC);
  ySource = -xEq.*sind(ALPHA) + yEq.*cosd(ALPHA);
  zSource =  xEq.*cosd(ALPHA)*cosd(DEC) + yEq.*sind(ALPHA)*cosd(DEC) + zEq.*sind(DEC);

  phiGrid_1   = atan2(ySource,xSource)*180.0/pi;
  thetaGrid_1 = acosd(zSource);

  phiGrid_1   = real(phiGrid_1);
  thetaGrid_1 = real(thetaGrid_1);

  ifind = find(phiGrid_1 < 0);
  phiGrid_1(ifind)  = phiGrid_1(ifind) + 360.0;
 % [phiGrid,isort] = sort(phiGrid);
 % thetaGrid       = thetaGrid(isort);
 % rProbAll        = rProbAll(isort);

  %nGrid  = length(phiGrid);

  angleDiff = spaceAngle(ALPHA,DEC,alphaGrid,decGrid);
  angleDiff=round(angleDiff.*10)/10;
  anglediff_u=unique(angleDiff);
  nrad=length(anglediff_u);

  ranPhiAll   = [];
  ranThetaAll = [];

  ranAlpha = [];
  ranDec   = [];
 
  for jj=1:nrad
   irad=find(angleDiff==anglediff_u(jj));
   
  thetaGrid=thetaGrid_1(irad);
  phiGrid=phiGrid_1(irad);
  
  nGrid  = length(phiGrid);
  %=== Number of throws will be weighted by the error box probability ===%
  nThrowsBin = ceil(nThrowsTheta.*rProbAll(irad));
  nThrowsSum = sum(nThrowsBin);
  %nThrowsSum   = nGrid*nThrowsTheta;

  %=== Generate random numbers ===%
  rNum = rand(2,nThrowsSum);

  istart = 1;
  for ig = 1:nGrid
  %for ig = 1:1

    iend = istart + nThrowsBin(ig) - 1;

    ranTheta = thetaGrid(ig) - DELTHETA/2.0 + rNum(1,istart:iend).*DELTHETA;
    delPhi   = DELTHETA./sind(ranTheta);
    ranPhi   = phiGrid(ig) - delPhi./2.0 + rNum(2,istart:iend).*delPhi;

    istart = iend + 1;

    %=== Make sure the simulated positions are within the IPN annulus ===%
    ifind = find(ranTheta >= 0 & ranTheta < 180.0);
%    ifind = find(ranTheta >= RMIN & ranTheta < RMAX);
    ranTheta = ranTheta(ifind);
    ranPhi   = ranPhi(ifind);

    if length(ranTheta) == 0
      continue;
    end

    ifindNeg = find(ranPhi < 0);
    ranPhi(ifindNeg) = ranPhi(ifindNeg) + 360.0;

    ifindPlus = find(ranPhi > 360);
    ranPhi(ifindPlus) = ranPhi(ifindPlus) - 360.0;

    ranPhiAll   = [ranPhiAll ranPhi];
    ranThetaAll = [ranThetaAll ranTheta];

  end
  end
  
  xSource = sind(ranThetaAll).*cosd(ranPhiAll);
  ySource = sind(ranThetaAll).*sind(ranPhiAll);
  zSource = cosd(ranThetaAll);

  %=== Do transformation to equatorial coordinates ===%
  xEq =  xSource.*cosd(ALPHA)*sind(DEC) - ySource.*sind(ALPHA) + zSource.*cosd(ALPHA)*cosd(DEC);
  yEq =  xSource.*sind(ALPHA)*sind(DEC) + ySource.*cosd(ALPHA) + zSource.*sind(ALPHA)*cosd(DEC);
  zEq = -xSource.*cosd(DEC) + zSource.*sind(DEC);

  ranDec   = asind(zEq);
  ranAlpha = atan2(yEq,xEq)*180.0/pi;

  ranDec   = real(ranDec);
  ranAlpha = real(ranAlpha);

  %=== Make sure range of alpha is from 0 to 360 ===%
  ifindNeg = find(ranAlpha < 0);
  ranAlpha(ifindNeg) = ranAlpha(ifindNeg) + 360;

  alphaThrow = ranAlpha;
  decThrow   = ranDec;



%=== End throwSky.m ===%
