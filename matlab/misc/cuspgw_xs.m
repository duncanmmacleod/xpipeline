function h = cuspgw(f0)
% CUSPGW_XS - Generate a cosmic string cusp waveform using Xavi's roll-off
% Written 2 Feb 2005 by Peter Shawhan
% Takes a single parameter: the cutoff frequency in Hz
% Reference: T. Damour and A. Vilenkin, Prys. Rev. D 64, 064008 (2001)

%-- Hard-coded parameters
samprate = 16384;  %-- Sampling rate, in Hz
timeblock = 1.0;   %-- Length of output time series, in seconds
toffset = 0.5;     %-- Offset within output time series, in seconds

%-- Band-pass filtering parameters
hpfreq = 5;   %-- Corner frequency of filter
hppow = 4/3;   %-- Controls strength of filter; a little artificial
lpfreq = 8000;
lppow = 1;

%-- Some derived parameters
%-- We actually generate a time series twice as long as needed
ndata = samprate * timeblock * 2;
%-- Vector of times for each data point
times = (1:ndata) / samprate;

%-- Constructe vector of frequencies for freq-domain representation, following
%-- Matlab convention.  Works for any arbitrary number of data points
npfreqs = floor((ndata-1)/2);
pfreqs = (1:npfreqs) * samprate / ndata ;
if mod(ndata,2) == 0
  nyquist = samprate / 2;
  freqs = [ 0 pfreqs nyquist -pfreqs(npfreqs:-1:1) ];
else
  freqs = [ 0 pfreqs -pfreqs(npfreqs:-1:1) ];
end

%-- Avoid divide-by-zero in the filtering operation
freqs(1) = 1.0e-20;

%-- Cosmic string cusp waveform in frequency domain, with exponential
%-- roll-off above the frequency cutoff f0
hf = [ 0 abs(freqs(2:ndata)).^(-4/3) ];
rolloff = 2 ./ (1 + exp(abs(freqs)/f0) );
hf = hf .* rolloff;

%-- Apply a time offset and a band-pass filter
dataf = hf .* exp(-2*pi*i*freqs*toffset) ...
   ./ (1+(hpfreq./abs(freqs)).^2).^(hppow/2) ...
   ./ (sqrt(1+(abs(freqs)./lpfreq).^2)).^lppow ;

%-- Get the time-domain waveform
data = real(ifft(dataf));

%-- Return just the first half of it, making sure that the DC level is zero
h = data(1:ndata/2) - data(1);

%-- Normalize to h_rss = 1
hrss = sqrt(sum(h.*h)/samprate);
h = h / hrss;
