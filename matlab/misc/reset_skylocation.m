injScales = [0];
myFolders = {'./output'};

% ---- for testing
%myFolders = {'test_u/simulations_ebbh-d_'};

for i=1:length(myFolders)
        for j=1:length(injScales)
                myFolder = [myFolders{i}];
                if ~isdir(myFolder)
                  errorMessage = sprintf('Error: The following folder does not exist:\n%s', myFolder);
                  disp(errorMessage);
                  return;
                end
                filePattern = fullfile(myFolder, '*merged.mat');
                matFiles = dir(filePattern);
                for k = 1:length(matFiles)
                  baseFileName = matFiles(k).name;
                  disp(baseFileName);
                  fullFileName = fullfile(myFolder, baseFileName);
                  fprintf(1, 'Now reading %s\n', fullFileName);
                  s = load(fullFileName);
                  s.skyPositions = [0.7854 -0.9269];
                  save(fullFileName,'-struct','s');;
                  fprintf(1, 'Done reading %s\n', fullFileName);
                end
        end
end
