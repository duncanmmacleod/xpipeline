#!/usr/bin/env python


import os
import subprocess
from subprocess import call
import fileinput
from tempfile import mkstemp
from shutil import move
from os import remove, close
import numpy as np
import numpy
import pylab
from pylab import *
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import ast
import scipy.stats as sp
import matplotlib.mlab as mlab
import matplotlib.patches as patches
import random
from random import choice
from optparse import OptionParser
from itertools import groupby

#Injection triggers
###################

sig = [float(line) for line in open('adi-e_significance_0.txt')]
scale = [float(line) for line in open('adi-e_scale_0.txt')]
f1 = [float(line) for line in open('adi-e_flag1_0.txt')]
f2 = [float(line) for line in open('adi-e_flag2_0.txt')]
f3 = [float(line) for line in open('adi-e_flag3_0.txt')]
f4 = [float(line) for line in open('adi-e_flag4_0.txt')]
npixels = [float(line) for line in open('adi-e_npixels_0.txt')]
d = [float(line) for line in open('adi-e_duration_0.txt')]
b = [float(line) for line in open('adi-e_bw_0.txt')]
nullE = [float(line) for line in open('adi-e_nullE_0.txt')]
nullEinc = [float(line) for line in open('adi-e_nullEinc_0.txt')]

index1 = [i for i, x in enumerate(f1) if x == 1]
index2 = [i for i, x in enumerate(f2) if x == 1]
index3 = [i for i, x in enumerate(f3) if x == 1]
index4 = [i for i, x in enumerate(f4) if x == 1]

nonindex1 = [i for i, x in enumerate(f1) if x == 0]
nonindex2 = [i for i, x in enumerate(f2) if x == 0]
nonindex3 = [i for i, x in enumerate(f3) if x == 0]
nonindex4 = [i for i, x in enumerate(f4) if x == 0]

sig_pass1 = [sig[t] for t in index1]
sig_pass2 = [sig[t] for t in index2]
sig_pass3 = [sig[t] for t in index3]
sig_pass4 = [sig[t] for t in index4]

scale_pass1 = [scale[t] for t in index1]
scale_pass2 = [scale[t] for t in index2]
scale_pass3 = [scale[t] for t in index3]
scale_pass4 = [scale[t] for t in index4]

sig_fail1 = [sig[t] for t in nonindex1]
sig_fail2 = [sig[t] for t in nonindex2]
sig_fail3 = [sig[t] for t in nonindex3]
sig_fail4 = [sig[t] for t in nonindex4]

scale_fail1 = [scale[t] for t in nonindex1]
scale_fail2 = [scale[t] for t in nonindex2]
scale_fail3 = [scale[t] for t in nonindex3]
scale_fail4 = [scale[t] for t in nonindex4]

sf1 = [len(list(group)) for key, group in groupby(scale_fail1)]
sf2 = [len(list(group)) for key, group in groupby(scale_fail2)]
sf3 = [len(list(group)) for key, group in groupby(scale_fail3)]
sf4 = [len(list(group)) for key, group in groupby(scale_fail4)]

nonindex = np.unique(nonindex1+nonindex2+nonindex3+nonindex4)
sigfail = [sig[j] for j in nonindex]
scalefail = [scale[j] for j in nonindex]
c12 = list(set(index1).intersection(index2))
c23 = list(set(c12).intersection(index3))
indexall = list(set(c23).intersection(index4))
sigpass = [sig[j] for j in indexall]
scalepass = [scale[i] for i in indexall]
npixelspass = [npixels[i] for i in indexall]
dpass = [d[i] for i in indexall]
bpass = [b[i] for i in indexall]
nullEpass = [nullE[i] for i in indexall]
nullEincpass = [nullEinc[i] for i in indexall]

print('Injections adi-e')
print('--------------------------')
print('Cumulative number of injections (scales x no. inj.): %i ' % len(sig))
print('-------------------------------------------------------------------------------------------')
print('Number of UL injections: %i' % len(sig))
print('-------------------------------------------------------------------------------------------')
print('Fraction of injections that fail VetoSegs: %f' % (float(len(sig_fail1))/float(len(sig))))
print('Fraction of injections that fail InjCoinc: %f' % (float(len(sig_fail2))/float(len(sig))))
print('Fraction of injections that fail Ratio: %f' % (float(len(sig_fail3))/float(len(sig))))
print('Fraction of injections that fail FixedRatio: %f' % (float(len(sig_fail4))/float(len(sig))))
print('-------------------------------------------------------------------------------')
print('Fraction of injections that PASS VetoSegs: %f' % (float(len(sig_pass1))/float(len(sig))))
print('Fraction of injections that PASS InjCoinc: %f' % (float(len(sig_pass2))/float(len(sig))))
print('Fraction of injections that PASS Ratio: %f' % (float(len(sig_pass3))/float(len(sig))))
print('Fraction of injections that PASS FixedRatio: %f' % (float(len(sig_pass4))/float(len(sig))))
print('-------------------------------------------------------------------------------')
print('Maximum significance of PASS ALL cuts: %f' % max(sigpass))
print('Number of injections that PASS ALL cuts: %i' % len(sigpass))
print('Number of injections that fail any or all cuts (discarded): %i' % len(nonindex))
#print('Fraction of injections with significance >207.551 that fail ALL cuts: %f' % (float(len(sigfail))/float(len(sig))))
#print('Fraction of injections with significance >207.551 that PASS ALL cuts: %f' % (float(len(sigpass))/float(len(sig))))
#print('-------------------------------------------------------------------------------')


# Backgroun triggers
#####################
sig_all = [float(line) for line in open('offsource_significance.txt')]
freq_all = [float(line) for line in open('offsource_t1.txt')]
f1_all = [float(line) for line in open('offsource_t1.txt')]
f2_all = [float(line) for line in open('offsource_t2.txt')]
f3_all = [float(line) for line in open('offsource_t3.txt')]
npix_all = [int(float(line)) for line in open('offsource_npixels.txt')]
d_all = [float(line) for line in open('offsource_duration.txt')]
b_all = [float(line) for line in open('offsource_bw.txt')]
nullE_all = [float(line) for line in open('offsource_nullE.txt')]
nullEinc_all = [float(line) for line in open('offsource_nullEinc.txt')]

sig_sorted = np.sort(sig_all)[len(sig_all) - 1000:len(sig_all)]
idex = [[i for i, x in enumerate(sig_all) if x == a][0] for a in sig_sorted]
f1 = [f1_all[m] for m in idex]
f2 = [f2_all[m] for m in idex]
f3 = [f3_all[m] for m in idex]
sig = [sig_all[m] for m in idex]
#print(max(sig))
freq = [freq_all[m] for m in idex]
npix = [npix_all[m] for m in idex]
dur = [d_all[m] for m in idex]
bw = [b_all[m] for m in idex]
nE = [nullE_all[m] for m in idex]
nI = [nullEinc_all[m] for m in idex]

#Combined statistics
####################
#Injections louder than loudest offsource
loudestinjsig = [z for z in sigpass if z>np.sort(sig)[-2]]
loudestindex = [i for i, x in enumerate(sigpass) if x>max(sig)]
loudestinjnpix = [npixelspass[j] for j in loudestindex]
#print('Before New Statistic Applied')
print('------------------------------------')
print('Number of injections that PASS ALL cuts and are LOUDER than loudest OFF: %i' % len(loudestinjsig))
print('---------------------------------------------------------------------------------------------------------')

#offsource first
#npix = [s for s in npix if s>30.0]
#offsource_index = [i for i, x in enumerate(npix) if x>30.0]
#sig = [sig[j] for j in offsource_index]
#freq = [freq[m] for m in offsource_index]
#npix = [npix[m] for m in offsource_index]
#dur = [dur[m] for m in offsource_index]
#bw = [bw[m] for m in offsource_index]
#nE = [nE[m] for m in offsource_index]
#nI = [nI[m] for m in offsource_index]
#then injections
#npixelspass = [s for s in npixelspass if s>30.0]
#inj_index = [i for i, x in enumerate(npixelspass) if x>30.0]
#sigpass = [sigpass[j] for j in inj_index]
#freq = [freq[m] for m in offsource_index]
#npix = [npix[m] for m in offsource_index]
#dpass = [dpass[m] for m in inj_index]
#bpass = [bw[m] for m in inj_index]
#nullEpass = [nullEpass[m] for m in inj_index]
#nullIpass = [nI[m] for m in inj_index]

offsource_statistic = [(float(a)*((1.0 - np.sqrt(np.log(float(a))/float(b)))**2.0)*((float(c)/np.log(float(a)))**2.0)*((1.0 - np.sqrt(np.log(float(a))/float(m)))**2.0)*((1.-np.sqrt(np.log(a)/s))**2.0)*(((float(c)*float(m))*float(b)))**1.0)/1000. for a,b,c,m,s in zip(sig, npix, dur, bw,nE)] #nE nI
prf1off = [float(a)*((1.0 - np.sqrt(np.log(float(a))/float(b)))**2.0) for a,b in zip(sig, npix)]
prf2off = [float(a)*((float(c)/np.log(float(a)))**2.0) for a,c in zip(sig,dur)]
prf3off = [float(a)*((1.0 - np.sqrt(np.log(float(a))/float(m)))**2.0) for a,m in zip(sig,bw)]
prf4off = [float(a)*((1.-np.sqrt(np.log(a)/s))**2.0) for a,s in zip(sig, nE)]
prf5off = [float(a)*float(c)*float(d)*float(s) for a,c,d,s in zip(sig,dur,bw,npix)]
p5off = [float(c)*float(d)*float(s) for c,d,s in zip(dur,bw,npix)]

inj_statistic = [(float(a)*((1.0 - np.sqrt(np.log(float(a))/float(b)))**2.0)*((float(c)/np.log(float(a)))**2.0)*((1.0 - np.sqrt(np.log(float(a))/float(m)))**2.0)*((1.0-np.sqrt(np.log(a)/s)))**2.0*(((float(c)*float(m))*float(b)))**1.0)/1000. for a,b,c,m,s in zip(sigpass, npixelspass, dpass, bpass,nullEpass)] # nullEpass nullEincpass
prf1inj = [float(a)*((1.0 - np.sqrt(np.log(float(a))/float(b)))**2.0) for a,b in zip(sigpass, npixelspass)]
prf2inj = [float(a)*((float(c)/np.log(float(a)))**2.0) for a,c in zip(sigpass,dpass)]
prf3inj = [float(a)*((1.0 - np.sqrt(np.log(float(a))/float(m)))**2.0) for a,m in zip(sigpass,bpass)]
prf4inj = [float(a)*((1.-np.sqrt(np.log(a)/s))**2.0) for a,s in zip(sigpass, nullEpass)]
prf5inj = [float(a)*float(c)*float(d)*float(s) for a,c,d,s in zip(sigpass,dpass,bpass,npixelspass)]
p5inj = [float(c)*float(d)*float(s) for c,d,s in zip(dpass,bpass,npixelspass)]

passAll = [h for h in inj_statistic if h>np.sort(offsource_statistic)[-2]]
pass1 = [h for h in prf1inj if h>np.sort(prf1off)[-2]]
pass2 = [h for h in prf2inj if h>np.sort(prf2off)[-2]]
pass3 = [h for h in prf3inj if h>np.sort(prf3off)[-2]]
pass4 = [h for h in prf4inj if h>np.sort(prf4off)[-2]]
pass5 = [h for h in prf5inj if h>np.sort(prf5off)[-2]]
print('Before clustering')
print('------------------')
print('prf1234')
print(len(passAll))
print('prf1')
print(len(pass1))
print('prf2')
print(len(pass2))
print('prf3')
print(len(pass3))
print('prf4')
print(len(pass4))
print('prf5')
print(len(pass5))

#let's do clustering here


#offsource_statistic = [float(a)*((np.log(float(a))/float(b))**1.0) for a,b in zip(offsource_sig_cut, offsource_npix_cut)]
#print('OFFS: After boosted clustering on pixels')
#print('------------------------------------')
#print('Maximum number of pixels (check): %i' % max(offsource_npix_cut))
#print('Maximum significance (check): %f' % max(offsource_sig_cut))
#print('Maximum new statistic (check): %i' % max(offsource_statistic))
#then injections
#inj_npix_cut = [s for s in npixelspass if s>0.0]
#inj_index = [i for i, x in enumerate(npixelspass) if x>0.0]
#inj_sig_cut = [sigpass[j] for j in inj_index]
#inj_statistic = [float(a)*(1.0 - np.log(float(a))/float(b)) for a,b in zip(inj_sig_cut, inj_npix_cut)]
#inj_statistic = [float(a)*((np.log(float(a))/float(b))**1.0) for a,b in zip(inj_sig_cut, inj_npix_cut)]
#apply loudest event cut
#loudest_inj_statistic = [z for z in inj_statistic if z>max(offsource_statistic)]
#loudest_inj_index = [i for i, x in enumerate(inj_statistic) if x>max(offsource_statistic)]
#loudest_inj_npix = [inj_npix_cut[j] for j in loudest_inj_index]
#loudest_inj_sig = [inj_sig_cut[j] for j in loudest_inj_index]

#quietest_inj_statistic = [z for z in inj_statistic if z<max(offsource_statistic)]
#quietest_inj_index = [i for i, x in enumerate(inj_statistic) if x<max(offsource_statistic)]
#quietest_inj_npix = [inj_npix_cut[j] for j in quietest_inj_index]
#quietest_inj_sig = [inj_sig_cut[j] for j in quietest_inj_index]

#quietest_inj_index = loudest_inj_statistic.index(min(loudest_inj_statistic))
#quitest_inj_npix = loudest_inj_npix[quietest_inj_index]

#print('\n')
#print('After NEW Statistic Applied')
#print('------------------------------------------')
#print('Number of pixels of min(new statistic): %i' % quitest_inj_npix)
#print('Minimum significance (check): %f' % min(loudest_inj_sig))
#print('Minimum new statistic (check): %i' % min(loudest_inj_statistic))
#print('---------------------------------------------------------------------------------------------------------')
#print('Number of injections that PASS ALL cuts and are LOUDER than loudest OFF new statistic: %i' % (len(loudest_inj_statistic)))
#print('---------------------------------------------------------------------------------------------------------')

#Plotting
##########
#plt.figure()

#plt.subplot(141)
#plt.semilogy()
#plt.scatter(nullEpass,prf4inj,marker='v',color='red',label='INJ')
#plt.scatter(nE,prf4off,marker='x',color='black',label='OFF')
#plt.scatter(loudest_inj_npix,loudest_inj_statistic,marker='v',color='blue',label='INJ>max(OFF)')
#plt.scatter(scalefail,sigfail,marker='x',color='black',label='fail')
#plt.xlabel('Null Energy')
#plt.ylabel('New Significance')
#plt.title('H1L1V1 Unclustered RUN3')
#plt.axvline(30,color='black',linestyle='--')
#plt.axhline(max(prf4off),color='black',linestyle='--')
#plt.xlim(-5.0,70.)
#plt.ylim(1.e-1, 1.e4)
#plt.grid()
#plt.legend(loc=2)

#plt.subplot(142)
#plt.semilogy()
#plt.scatter(nullEpass,sigpass,marker='v',color='red',label='INJ')
#plt.scatter(nE,sig,marker='x',color='black',label='OFF')
#plt.scatter(loudest_inj_npix,loudest_inj_statistic,marker='v',color='blue',label='INJ>max(OFF)')
#plt.scatter(scalefail,sigfail,marker='x',color='black',label='fail')
#plt.xlabel('Null Energy')
#plt.ylabel('Significance')
#plt.title('H1L1V1 Unclustered RUN3')
#plt.axvline(30,color='black',linestyle='--')
#plt.axhline(max(sig),color='black',linestyle='--')
#plt.xlim(-5.0,70.)
#plt.ylim(1.e-1, 1.e4)
#plt.grid()
#plt.legend(loc=2)

#plt.subplot(151)
#plt.semilogy()
#plt.plot(np.sort(np.array(sigpass)),color='red',label='INJ')
#plt.plot(np.sort(np.array(inj_statistic)),color='red',linestyle='--',label='INJxprf1,2,3')
#plt.plot(np.sort(np.array(sig)),color='black',label='OFF')
#plt.plot(np.sort(np.array(offsource_statistic)),color='black',linestyle='--',label='OFFxprf1,2,3')
#plt.scatter(npix,sig,marker='x',color='black',label='OFF')
#plt.scatter(loudestinjnpix,loudestinjsig,marker='v',color='blue',label='INJ>max(OFF)')
#plt.scatter(scalefail,sigfail,marker='x',color='black',label='fail')
#plt.xlabel('Idx.')
#plt.ylabel('Standardenergy/Standardenergy x (prf1prf2prf3xprf4)')
#plt.title('H1L1V1 adi-e RUN3 UnClustered lmax=60\nscale = 0.053')
#plt.axvline(30,color='black',linestyle='--')
#plt.axhline(max(offsource_statistic),color='green',linestyle='--')
#plt.xlim(-5.0,70.)
#plt.ylim(1.e-1, 1.e6)
#plt.axvline(0.0010)
#plt.axvline(0.0078)
#plt.axvline(0.0156)
#plt.axvline(0.0186)
#plt.axvline(0.0221)
#plt.axvline(0.0263)
#plt.axvline(0.0312)#,color='red',linestyle='--')
#plt.axvline(0.0372)#,color='red',linestyle='--')
#plt.axvline(0.0442)#,color='red',linestyle='--')
#plt.axvline(0.0526)#,color='red',linestyle='--')
#plt.axvline(0.0625)
#plt.axvline(0.0743)#,color='red',linestyle='--')
#plt.axvline(0.0884,color='red',linestyle='--')
#plt.grid()
#plt.legend(loc=4)

#plt.subplot(152)
#plt.semilogy()
#plt.plot(np.sort(np.array(sigpass)),color='red',label='INJ')
#plt.plot(np.sort(np.array(sig)),color='black',label='OFF')
#plt.plot(np.sort(prf1inj),linestyle='--',color='red',label='INJxprf1')
#plt.plot(np.sort(prf1off),linestyle='--',color='black',label='OFFxprf1')
#plt.ylabel('SE*(1 - sqrt[ln(SE)/P])**2')
#plt.xlabel('#')
#plt.title('H1L1V1 adi-e Unclustered lmax=60\nprf1 OFFpass-/INJpass- all')
#plt.axhline(max(prf1off),color='green',linestyle='--')
#plt.xlim(0.0,70.)
#plt.ylim(0., 80.)
#plt.grid()
#plt.legend(loc=4)

#plt.subplot(153)
#plt.semilogy()
#plt.plot(np.sort(np.array(sigpass)),color='red',label='INJ')
#plt.plot(np.sort(np.array(sig)),color='black',label='OFF')
#plt.plot(np.sort(prf2inj),linestyle='--',color='red',label='INJ')
#plt.plot(np.sort(prf2off),linestyle='--',color='black',label='OFF')
#plt.ylabel('SE*(dT/ln(SE))**2')
#plt.xlabel('#')
#plt.title('H1L1V1 adi-e Unclustered lmax=60\nprf2 OFFpass-/INJpass- all')
#plt.axhline(max(prf2off),color='green',linestyle='--')
#plt.xlim(0.0,70.)
#plt.ylim(0., 80.)
#plt.grid()
#plt.legend(loc=4)

#plt.subplot(154)
#plt.semilogy()
#plt.plot(np.sort(np.array(sigpass)),color='red',label='INJ')
#plt.plot(np.sort(np.array(sig)),color='black',label='OFF')
#plt.plot(np.sort(prf3inj),linestyle='--',color='red',label='INJ')
#plt.plot(np.sort(prf3off),linestyle='--',color='black',label='OFF')
#plt.ylabel('SE*(1 - sqrt[ln(SE)/df])')
#plt.xlabel('#')
#plt.title('H1L1V1 adi-e Unclustered lmax=60\nprf3 OFFpass-/INJpass- all')
#plt.axhline(max(prf3off),color='green',linestyle='--')
#plt.xlim(0.0,70.)
#plt.ylim(1., 1e4)
#plt.grid()
#plt.legend(loc=4)

#plt.subplot(143)
#plt.semilogy()
#plt.plot(np.sort(np.array(sigpass)),color='red',label='INJ')
#plt.plot(np.sort(np.array(sig)),color='black',label='OFF')
#plt.hist(p5inj,25,color='red',label='INJ')
#plt.hist(p5off,10,color='black',label='OFF')
#plt.hist(p5inj,25,color='red',label='INJ')
#plt.xlabel('BW*DUR*NPIX')
#plt.ylabel('#')
#plt.title('H1L1V1 adi-e SC4 lmax=60')
#plt.axhline(max(p5off),color='green',linestyle='--')
#plt.xlim(0.0,70.)
#plt.ylim(1., 1e4)
#plt.grid()
#plt.legend()

#plt.subplot(144)
#plt.semilogy()
#plt.plot(np.sort((1.0 - np.sqrt(np.log(np.array(sigpass))/np.array(nullEpass)))**2.0),color='red',label='INJ')
#plt.plot(np.sort((1.0 - np.sqrt(np.log(np.array(sig))/np.array(nE)))**2.0),color='black',label='OFF')
#plt.hist(sigpass,50,color='black',label='lmax=20')
#plt.scatter(scalefail,sigfail,marker='x',color='black',label='fail')
#plt.xlabel('#')
#plt.ylabel('nullE/nullInc')
#plt.title('inj sig < loudest off')
#plt.axvline(0,color='black',linestyle='--')
#plt.axhline(max(offsource_statistic),color='black',linestyle='--')
#plt.xlim(0.0,70.)
#plt.ylim(0., 80.)
#plt.grid()
#plt.legend(loc=2)

#plt.subplot(144)
#plt.loglog()
#plt.hist(quietest_inj_sig,50,alpha=0.5,color='red',label='INJ Sig.')
#plt.hist(npix,20,alpha=0.5,color='black',label='OFF')
#plt.hist(offsource_sig_cut,50,alpha=0.5,color='black',label='OFF Sig.')
#plt.scatter(scalefail,sigfail,marker='x',color='black',label='fail')
#plt.xlabel('Significance')
#plt.ylabel('#')
#plt.title('H1L1V1 Unclustered\nSignificance Inj/OFF')
#plt.axvline(0,color='black',linestyle='--')
#plt.axhline(max(offsource_statistic),color='black',linestyle='--')
#plt.xlim(0.0,70.)
#plt.ylim(0., 80.)
#plt.grid()
#plt.legend(loc=2)

#plt.subplot(122)
#plt.loglog()
#plt.scatter(inj_npix_cut,inj_statistic,marker='v',color='red',label='INJ')
#plt.scatter(offsource_npix_cut,offsource_statistic,marker='x',color='black',label='OFF')
#plt.scatter(loudest_inj_npix,loudest_inj_statistic,marker='v',color='blue',label='INJ>max(OFF)')
#plt.scatter(scalefail,sigfail,marker='x',color='black',label='fail')
#plt.xlabel('Duration(s)')
#plt.ylabel('BoostStatistic (duration x sign.)')
#plt.title('H1L1V1 Boosted (2s,5Hz)\n1000 loudest offs./inj. signif.>0.0')
#plt.axvline(0,color='black',linestyle='--')
#plt.axhline(max(offsource_statistic),color='black',linestyle='--')
#plt.xlim(1.0,1000.)
#plt.ylim(10., 1.e5)
#plt.grid()
#plt.legend(loc=2)

#plt.suptitle('lmax=60 (unclustered, circEnergy, lmax=20 thresholds) EBBH-A')
#plt.show()
