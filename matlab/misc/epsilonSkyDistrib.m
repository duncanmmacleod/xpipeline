%clear coordinates
latlim = [-90  90];                     
lonlim = [0 360];
gratsize = 1 + [diff(latlim), diff(wrapTo360(lonlim))]*2;
[lat, lon] = meshgrat(latlim, lonlim, gratsize);
%coordinates = [90-lat(:) lon(:)]/180*pi;

% [coordinates] = healpix(6);
% coordinates = 0.5:0.1:2.5;
% coordinates = repmat(coordinates',[1 2]);

[FpH, FcH] = ComputeAntennaResponse(coordinates(:,2),coordinates(:,1),0,'H1');
[FpL, FcL] = ComputeAntennaResponse(coordinates(:,2),coordinates(:,1),0,'L1');
[FpV, FcV] = ComputeAntennaResponse(coordinates(:,2),coordinates(:,1),0,'V1');
% -- factor 2 in sensitivity
FpV = FpV/2;
FcV = FcV/2;

FpHL= [ FpH FpL ];
FpHLV= [ FpH FpL FpV];
FpLV= [ FpL FpV ];
FcHL= [ FcH FcL ];
FcHLV= [ FcH FcL FcV];
FcLV= [ FcL FcV ];

clear F
F(:,1) = sqrt(FpH.^2 +  FcH.^2);
F(:,2) = sqrt(FpL.^2 +  FcL.^2);
F(:,3) = sqrt(FpV.^2 +  FcV.^2);

fomHLV = zeros(size(F,1),1);
for iDet = 1:3
  for jDet = 1:iDet-1
    fomHLV = fomHLV + 2*F(:,iDet).^2.*F(:,jDet).^2;
  end
end
%fomHLV = fomHLV./(sum(F.^4,2));
fomHLV = fomHLV./(sum(F.^4,2) + sum(F.^2,2).^2)*(4/2);

fomLV = 2*F(:,2).^2.*F(:,3).^2;
fomLV = fomLV./(sum(F(:,2:3).^2,2).^2+sum(F(:,2:3).^4,2))*(3/1);

fomHV = 2*F(:,1).^2.*F(:,3).^2;
fomHV = fomHV./(sum(F(:,[1 3]).^2,2).^2+sum(F(:,[1 3]).^4,2))*(3/1);

fomHL = 2*F(:,1).^2.*F(:,2).^2;
fomHL = fomHL./(sum(F(:,1:2).^2,2).^2+sum(F(:,1:2).^4,2))*(3/1);
%fomHL = fomHL./(sum(F(:,1:2).^4,2));

penaltyHLV = fomHLV.^0.25;
penaltyLV = fomLV.^0.25;
penaltyHV = fomHV.^0.25;
penaltyHL = fomHL.^0.25;

cohSNRhlv = sqrt(F(:,1).^2 + F(:,2).^2 + F(:,3).^2);
cohSNRlv = sqrt(F(:,2).^2 + F(:,3).^2);
cohSNRhv = sqrt(F(:,1).^2 + F(:,3).^2);
cohSNRhl = sqrt(F(:,1).^2 + F(:,2).^2);

[FpDPhl FcDPhl] = convertToDominantPolarizationFrame(FpHL,FcHL);
[FpDPhlv FcDPhlv] = convertToDominantPolarizationFrame(FpHLV,FcHLV);
[FpDPlv FcDPlv] = convertToDominantPolarizationFrame(FpLV,FcLV);

epsHL = dot(FcDPhl,FcDPhl,2)./ dot(FpDPhl,FpDPhl,2);
epsHLV = dot(FcDPhlv,FcDPhlv,2)./ dot(FpDPhlv,FpDPhlv,2);
epsLV = dot(FcDPlv,FcDPlv,2)./ dot(FpDPlv,FpDPlv,2);

prcEpsHL = prctile(epsHL, [50 80 90]);
prcEpsHLV = prctile(epsHLV, [50 80 90])
prcEpsLV = prctile(epsLV, [50 80 90])

dotHL = abs(dot([FpH FcH],[FpL FcL],2));
dotLV = abs(dot(2*[FpV FcV],[FpL FcL],2));

prcDotHL = prctile(dotHL,[20 50 80])
prcDotLV = prctile(dotLV,[20 50 80])

FpDPhatHL = FpDPhl./repmat(sqrt(dot(FpDPhl,FpDPhl,2)),[1 size(FpDPhl,2)]);
FcDPhatHL = FcDPhl./repmat(sqrt(dot(FcDPhl,FcDPhl,2)),[1 size(FcDPhl,2)]);
FpDPhatLV = FpDPlv./repmat(sqrt(dot(FpDPlv,FpDPlv,2)),[1 size(FpDPlv,2)]);
FcDPhatLV = FcDPlv./repmat(sqrt(dot(FcDPlv,FcDPlv,2)),[1 size(FcDPlv,2)]);

contribPhl = min(FpDPhatHL.^2,[],2);
contribChl = min(FcDPhatHL.^2,[],2);
contribPlv = min(FpDPhatLV.^2,[],2);
contribClv = min(FcDPhatLV.^2,[],2);

prcContribPhl = prctile(contribPhl,[20 50 80])
prcContribPlv = prctile(contribPlv,[20 50 80])


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0
axesm('mollweid');
surfm(lat,lon,reshape(penaltyHL,size(lat)))
colormap('hot');colorbar;caxis([0 1]);
plotm((pi/2-s)*180/pi,s*180/pi,'k+','linewidth',1)
gridm
set(gcf, 'color', 'none');
export_fig penaltyHL -png -r200
end
