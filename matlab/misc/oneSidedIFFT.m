function dataT = oneSidedIFFT( oneSidedDataF )
%oneSidedIFFT    Computes the IFFT of the one-sided frequency domain data
%                corresponding to real time domain data.
%
%    dataT = oneSidedIFFT( oneSidedDataF )
%
% Inputs:
%    oneSidedDataF    Matrix complex values which will be inverse fourier
%                     transformed along the columns.
%
% Outputs:
%    dataT            Matrix real values whose columns are the iffts of the
%                     columns of oneSidedDataF
%
% See also    ifft
%
% Revision History
% 11 July 2005    Leo C. Stein    Initial Write
%
% $Id$

% Number of rows, length of the 1 dimension, number of frequency bins
nFreq = size(oneSidedDataF,1);

% Vertically concatenate the one sided data with the conjugate of the
% mirrored ordering of the data, less the nyquist frequency bin and the bin
% corresponding to the DC component.
dataT = ifft( [ oneSidedDataF ; ...
          conj( oneSidedDataF( nFreq-1:-1:2, : ) ) ] , ...
          'symmetric' ); % Ensure real output ( this should not be an issue
                         % with the explicit conjugation here )