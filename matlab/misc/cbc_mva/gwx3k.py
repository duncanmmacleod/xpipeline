
def gwx3k(hname,lname,coincnames,tstart):
    # The Gravitational Wave Extractor 3000 will take the directory containing hdf
    # files and output the parameters describing the BBH simulations
    # into a .txt file
    # Enter the name of the H1, L1,and Coinc files as a list of 1 or more strings
    # as a set such that the 1st, 2nd, 3rd, etc components of the lists are related
    # to each other in sequential order.

    # A 1 corresponds to L1 and a 2 corresponds to H1

    # Look into numpy.nonzero() for searching for the index of a specific term



    import numpy as np, h5py, os, glob

    if type(hname) is not type([]):
        hname = [hname]
    if type(lname) is not type([]):
        lname = [lname]
    if type(coincnames) is not type([]):
        coincnames = [coincnames]


    with open('Injection_files', 'w') as blah:
        infiles = np.column_stack([hname,lname,coincnames])
        blah.writelines(' '.join(str(j) for j in i) + '\n' for i in infiles)

    if glob.glob('Injection_Parameters'):
        os.remove('Injection_Parameters')

    sets = list(range(0,len(coincnames)))

    for s in sets:

        tempbank = ['H1L1-BANK2HDF-1126051217-3331800.hdf']
        # Change the name of the Bank file to whichever so long as it has the same "tree-like" format as the previous. If the format is different, this code will need t be modified to accomodate

        minsnr = 6.0
        tend = tstart + 2000000
        # Modify minimum SNR and time duration to desired values
        infile = h5py.File(coincnames[s], 'r')
        infile1 = h5py.File(lname[s],'r')
        infile2 = h5py.File(hname[s],'r')

        t1 = np.array(infile['found']['trigger_id1'][:])
        t2 = np.array(infile['found']['trigger_id2'][:])
        # THIS IS A PLACE WHERE THINGS CAN GO WRONG!! For the specific files I am using, trigid1 corresponds to L1 in this SPECIFIC hdf coincidence file. Change it and the branch name(s) as need be.

        snr1_all = np.array(infile1['L1']['snr'][:])
        snr2_all = np.array(infile2['H1']['snr'][:])
        snr1 = snr1_all[t1]
        snr2 = snr2_all[t2]
        # The above extracts all the sets of mergers SNR's as associated by tirgger IDs

        good_snr1 = snr1 >= minsnr
        good_snr2 = snr2 >= minsnr
        good_snr = np.logical_and(good_snr1, good_snr2)
        # The above isolates the sets with SNRs which meet the minimum criteria

        time1 = np.array(infile1['L1']['end_time'])[t1]
        time2 = np.array(infile2['H1']['end_time'])[t2]
        # The above extracts all the sets of mergers with times's as associated by tirgger IDs

        good_time1 = np.logical_and(time1 >= tstart, time1 <= tend)
        good_time2 = np.logical_and(time2 >= tstart, time2 <= tend)
        good_time = np.logical_and(good_time1, good_time2)
        # The above isolates the sets with times which meet the minimum criteria

        goodlist = np.logical_and(good_snr, good_time)
        coinclist = np.where(goodlist)[0]
        # goodlist contains sets which meet both, the minimum snr and time requirements
        # np.where() gives me the indices of the the "True" statements

        for index in range(11,20):
            print(snr1[index], " ", snr2[index], " ", good_snr[index], " ", good_time[index], " ", goodlist[index])


        trigid1 = t1[coinclist]
        trigid2 = t2[coinclist]
        template_id = np.array(infile['found']['template_id'][:])[coinclist]

        infile3 = h5py.File(tempbank[0],'r')
        m1 = np.array(infile3['mass1'][:])[template_id]
        m2 = np.array(infile3['mass2'][:])[template_id]
        s1 = np.array(infile3['spin1z'][:])[template_id]
        s2 = np.array(infile3['spin2z'][:])[template_id]
        # The above lines extract information from the bankfile

        L1_chisq = np.array(infile1['L1']['chisq'][:])[trigid1]
        # x^2 value which describes likelihood of being GW wave
        L1_chisq_dof = np.array(infile1['L1']['chisq_dof'][:])[trigid1]
        # x^2 depth of field... no clue what a depth of
        # field x^2 would describe yet
        L1_coa_phase = np.array(infile1['L1']['coa_phase'][:])[trigid1]
        # coalescence phase; goes from -2*pi to 2*pi
        L1_end_time = np.array(infile1['L1']['end_time'][:])[trigid1]
        # end time of coalescence
        L1_sigmasq = np.array(infile1['L1']['sigmasq'][:])[trigid1]
        # sigma^2 value; describes something
        L1_SNR = np.array(infile1['L1']['snr'][:])[trigid1]
        # signal to noise ratioof event
        L1_template_duration = np.array(infile1['L1']['template_duration'][:])[trigid1]
        # duration of the signal; I personally don't think this
        # should be a factor


        H1_chisq = np.array(infile2['H1']['chisq'][:])[trigid2]
        # x^2 value which describes likelihood of being GW wave
        #H1_chisq_dof = np.array(infile2['H1']['chisq_dof'][:])[trigid2]
        # x^2 depth of field... no clue what a depth of
        # field x^2 would describe yet
        H1_coa_phase = np.array(infile2['H1']['coa_phase'][:])[trigid2]
        # coalescence phase; goes from -2*pi to 2*pi
        H1_end_time = np.array(infile2['H1']['end_time'][:])[trigid2]
        # end time of coalescence
        H1_sigmasq = np.array(infile2['H1']['sigmasq'][:])[trigid2]
        # sigma^2 value; describes something
        H1_SNR = np.array(infile2['H1']['snr'][:])[trigid2]
        # signal to noise ratio of event
        H1_template_duration = np.array(infile2['H1']['template_duration'][:])[trigid2]
        # duration of the signal; I personally don't think this
        # should be a factor, but I still have a lot to learn

        print(m1[10], m2[10], s1[10], s2[10], template_id[10], L1_chisq[10], L1_chisq_dof[10], L1_coa_phase[10], L1_end_time[10], L1_sigmasq[10], L1_SNR[10], L1_template_duration[10], H1_chisq[10],  H1_end_time[10], H1_sigmasq[10], H1_SNR[10], H1_template_duration[10], s, coinclist[10])

        print(L1_chisq[10] == H1_chisq[10], L1_end_time[10] == H1_end_time[10], L1_sigmasq[10] == H1_sigmasq[10], L1_SNR[10] == H1_SNR[10], L1_template_duration[10] == H1_template_duration[10], H1_coa_phase[10] == L1_coa_phase[10])

        print(len(m1), len(m2), len(s1), len(s2), len(template_id), len(L1_chisq), len(L1_chisq_dof), len(L1_coa_phase), len(L1_end_time), len(L1_sigmasq), len(L1_SNR), len(L1_template_duration), len(H1_chisq),  len(H1_end_time), len(H1_sigmasq), len(H1_SNR), len(H1_template_duration))

        injectionparams = np.column_stack([m1, m2, s1, s2, template_id, L1_chisq_dof, L1_template_duration, L1_chisq, L1_coa_phase,  L1_end_time, L1_sigmasq, L1_SNR, H1_chisq, H1_coa_phase, H1_end_time, H1_sigmasq, H1_SNR, [s]*len(coinclist), coinclist])

        header = 'Mass1/F:Mass2/F:Spin1z/F:Spin2z/F:Templateid/I:Chisq_dof/F:Templateduration/F:L1chisq/F:L1coaphase/F:L1endtime/F:L1sigmasq/F:L1snr/F:H1chisq/F:H1coaphase/F:H1endtime/F:H1sigmasq/F:H1snr/F:CoincPage/F:CoincID/F'

        #print injectionparams, injectionparams[10]

        with open('Injection_Parameters','a') as hl:
            if s == 0:
                hl.writelines(header + ' \n')
                hl.writelines(' '.join(str(j) for j in i) + '\n' for i in injectionparams)
            else:
                hl.writelines(' '.join(str(j) for j in i) + '\n' for i in injectionparams)

def noiseminer(hname,lname,coincnames,tstart):
    # Very similar to above procedure except you will give it a list of data files containing noise.

    import numpy as np, h5py, os, glob

    if type(hname) is not type([]):
        hname = [hname]
    if type(lname) is not type([]):
        lname = [lname]
    if type(coincnames) is not type([]):
        coincnames = [coincnames]


    with open('Noise_files', 'w') as blah:
        infiles = np.column_stack([hname,lname,coincnames])
        blah.writelines(' '.join(str(j) for j in i) + '\n' for i in infiles)

    if glob.glob('Noise_Parameters'):
        os.remove('Noise_Parameters')

    sets = list(range(0,len(coincnames)))

    for s in sets:

        tempbank = ['H1L1-BANK2HDF-1126051217-3331800.hdf']

        minsnr = 6.0
        tend = tstart + 2000000
        # Modify minimum SNR value and time duration to desiredvalue

        infile = h5py.File(coincnames[s], 'r')
        infile1 = h5py.File(lname[s],'r')
        infile2 = h5py.File(hname[s],'r')

        t1 = np.array(infile['background']['trigger_id2'][:])
        t2 = np.array(infile['background']['trigger_id1'][:])
        # THIS IS A PLACE WHERE THINGS CAN GO WRONG!! For the specific files I am using, trigid2 corresponds to L1 in this SPECIFIC hdf coincidence file. Change it and the branch name(s) as need be.

        # Even though trigid1 is usually L1 and trigid2 is usually H1, this specific hdf file got them mixed up; for your file, change t1 to point to L1's trig ID and t2 to point to H2's trig ID

        snr1_all = np.array(infile1['L1']['snr'][:])
        snr2_all = np.array(infile2['H1']['snr'][:])
        snr1 = snr1_all[t1]
        snr2 = snr2_all[t2]

        good_snr1 = snr1 >= minsnr
        good_snr2 = snr2 >= minsnr
        good_snr = np.logical_and(good_snr1, good_snr2)

        time1 = np.array(infile1['L1']['end_time'])[t1]
        time2 = np.array(infile2['H1']['end_time'])[t2]

        good_time1 = np.logical_and(time1 >= tstart, time1 <= tend)
        good_time2 = np.logical_and(time2 >= tstart, time2 <= tend)
        #print good_time1, good_time2
        good_time = np.logical_and(good_time1, good_time2)

        goodlist = np.logical_and(good_snr, good_time)
        coinclist = np.where(goodlist)[0]

        # np.where() gives me the indices of the the "True" statements

        for index in range(11,20):
            print(snr1[index], " ", snr2[index], " ", good_snr[index], " ", good_time[index], " ", goodlist[index])

        trigid1 = t1[coinclist]
        trigid2 = t2[coinclist]
        template_id = np.array(infile['background']['template_id'][:])[coinclist]

        infile3 = h5py.File(tempbank[0],'r')
        m1 = np.array(infile3['mass1'][:])[template_id]
        m2 = np.array(infile3['mass2'][:])[template_id]
        s1 = np.array(infile3['spin1z'][:])[template_id]
        s2 = np.array(infile3['spin2z'][:])[template_id]


        L1_chisq = np.array(infile1['L1']['chisq'][:])[trigid1]
        # x^2 value which describes likelihood of being GW wave
        L1_chisq_dof = np.array(infile1['L1']['chisq_dof'][:])[trigid1]
        # x^2 depth of field... no clue what a depth of
        # field x^2 would describe yet
        L1_coa_phase = np.array(infile1['L1']['coa_phase'][:])[trigid1]
        # coalescence phase; goes from -2*pi to 2*pi
        L1_end_time = np.array(infile1['L1']['end_time'][:])[trigid1]
        # end time of coalescence
        L1_sigmasq = np.array(infile1['L1']['sigmasq'][:])[trigid1]
        # sigma^2 value; describes something
        L1_SNR = np.array(infile1['L1']['snr'][:])[trigid1]
        # signal to noise ratioof event
        L1_template_duration = np.array(infile1['L1']['template_duration'][:])[trigid1]
        # duration of the signal; I personally don't think this
        # should be a factor


        H1_chisq = np.array(infile2['H1']['chisq'][:])[trigid2]
        # x^2 value which describes likelihood of being GW wave
        #H1_chisq_dof = np.array(infile2['H1']['chisq_dof'][:])[trigid2]
        # x^2 depth of field... no clue what a depth of
        # field x^2 would describe yet
        H1_coa_phase = np.array(infile2['H1']['coa_phase'][:])[trigid2]
        # coalescence phase; goes from -2*pi to 2*pi
        H1_end_time = np.array(infile2['H1']['end_time'][:])[trigid2]
        # end time of coalescence
        H1_sigmasq = np.array(infile2['H1']['sigmasq'][:])[trigid2]
        # sigma^2 value; describes something
        H1_SNR = np.array(infile2['H1']['snr'][:])[trigid2]
        # signal to noise ratio of event
        H1_template_duration = np.array(infile2['H1']['template_duration'][:])[trigid2]
        # duration of the signal; I personally don't think this
        # should be a factor, but I still have a lot to learn


        print(m1[10], m2[10], s1[10], s2[10], template_id[10], L1_chisq[10], L1_chisq_dof[10], L1_coa_phase[10], L1_end_time[10], L1_sigmasq[10], L1_SNR[10], L1_template_duration[10], H1_chisq[10],  H1_end_time[10], H1_sigmasq[10], H1_SNR[10], H1_template_duration[10])

        print(L1_chisq[10] == H1_chisq[10], L1_end_time[10] == H1_end_time[10], L1_sigmasq[10] == H1_sigmasq[10], L1_SNR[10] == H1_SNR[10], L1_template_duration[10] == H1_template_duration[10], H1_coa_phase[10] == L1_coa_phase[10])

        print(len(m1), len(m2), len(s1), len(s2), len(template_id), len(L1_chisq), len(L1_chisq_dof), len(L1_coa_phase), len(L1_end_time), len(L1_sigmasq), len(L1_SNR), len(L1_template_duration), len(H1_chisq),  len(H1_end_time), len(H1_sigmasq), len(H1_SNR), len(H1_template_duration))

        injectionparams = np.column_stack([m1, m2, s1, s2, template_id, L1_chisq_dof, L1_template_duration, L1_chisq, L1_coa_phase, L1_end_time, L1_sigmasq, L1_SNR, H1_chisq, H1_coa_phase, H1_end_time, H1_sigmasq, H1_SNR, [s+1000]*len(coinclist), coinclist])

        header = 'Mass1/F:Mass2/F:Spin1z/F:Spin2z/F:Templateid/F:Chisq_dof/F:Templateduration/F:L1chisq/F:L1coaphase/F:L1endtime/F:L1sigmasq/F:L1snr/F:H1chisq/F:H1coaphase/F:H1endtime/F:H1sigmasq/F:H1snr/F:CoincPage/F:CoincID/F'

        with open('Noise_Parameters','a') as hl:
            if s == 0:
                hl.writelines(header + ' \n')
                hl.writelines(' '.join(str(j) for j in i) + '\n' for i in injectionparams)
            else:
                hl.writelines(' '.join(str(j) for j in i) + '\n' for i in injectionparams)



def itemsplitter():
    # Splits the collected Injection and Noise data and splits it into files necessitated by by ROOT Loader

    import random as r

    with open('Injection_Parameters','r') as blah:
        ip = blah.readlines()
        header = [ip.pop(0)]
        r.shuffle(ip)
        trainers = ip[:len(ip)/2]
        testers = ip[len(ip)/2:]

        with open('A_SIG_train.txt','w') as tf:
            tf.writelines(header + trainers)

        with open('A_SIG_test.txt', 'w') as tf:
            tf.writelines(header + testers)

    with open('Noise_Parameters','r') as blah:
        np = blah.readlines()
        header = [np.pop(0)]
        r.shuffle(np)
        trainers = np[:len(np)/2]
        testers = np[len(np)/2:]

        with open('A_BG_train.txt','w') as tf:

            tf.writelines(header + trainers[:10**5])

        with open('A_BG_test.txt', 'w') as tf:
            tf.writelines(header + testers[:10**5])
        # Replace the :10**5 with : to tak into account ALL data sets as opposed to the first hundred thousand data sets

# Below is a fake ONSOURCE file for the sake of testing the MVA tool; it is just a copy of Signal Trainer
    with open('A_ON_test.txt','w') as tf:
        tf.writelines(header + trainers[:10**5])




def dataswap(rootname, hdfname):
    # Takes the root-filename as a string and modifies the 'stat' branch in the
    # given hdf coinincidence file with the outut from 'BDT' in the root-file

    import ROOT, h5py, numpy as np
    from shutil import copyfile

    BDT = np.array([])
    CoincPage = np.array([])
    CoincID = np.array([])


    fn = ROOT.TFile.Open(rootname)

    testtree = fn.Get('TestTree')

    for event in testtree:
        BDT.append(event.BDT)
        CoincPage.append(event.CoincPage)
        CoincID.append(event.CoincID)

    datamatrix = np.column_stack([CoincPage, CoincID, BDT])

    inj_CoincPage = np.where(CoincPage[:] >= 0)[0]
    bg_CoincPage = np.where(CoincPage[:] >= 1000)[0]

    inj_sets = datamatrix[inj_CoincPage]
    maxinjindex = inj_sets[:,0].max()

    bg_sets = datamatrix[bg_CoincPage]
    maxbgindex = bg_sets[:,0].max()

    with open('Injection_files','r') as blah:
        inj_files = blah.read().splitlines()
        for j in inj_files:
            j = j.split(' ')

    with open('Noise_files','r') as blah:
        bg_files = blah.read().splitlines()
        for j in inj_files:
            j = j.split(' ')

    for s in range(maxinjindex + 1):
        coincfile = inj_file[s, -1]
        newfname = coincfile+'_MVA'
        copyfile(coincfile, newfname)

        desiredpage = np.where(inj_sets[:, 0] == s)[0]
        desiredsets = inj_sets[desiredpage]

        desiredCoincID = desiredsets[:, 1]
        desiredBDT = desiredsets[:, 2]

        with h5py.File(newfname,'r+') as newfile:

            oldstat = np.array(newfile['found']['stat'][:])
            newstat = np.put(oldstat, desiredCoincID, desiredBDT)



            # PROBLEM: Because we only selected certain elements from the coincidence files, there will be values of stat not determined by the MVA tool still inside the new COincidence file... what do we do?
