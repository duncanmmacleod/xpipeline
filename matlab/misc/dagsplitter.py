#!/usr/bin/env python
"""
dagsplitter

takes a rescue dag in splits it in three dags so three people can
run those two sub-rescue -dags independently

FIXME make this option parsable for however many spewed rescue dags one needs

"""
__author__ = 'Valeriu Predoi < valeriu.predoi@astro.cf.ac.uk>'
__date__ = '$Date$'
__version__ = '$Revision$'
__name__ = 'xdags'

dag = open('grb_off_source.dag.rescue002', 'r')
file1 = open('file1.txt', 'w')
file2 = open('file2.txt', 'w')
file3 = open('file3.txt', 'w')

no_retry_indices=[]
retry_indices = []
lines = [line.strip() for line in dag]

# ---- get all the DONE nodes and place in ALL new rescue files
print('Writing all previously DONE jobs to all spewed rescue dags...')
for line in lines:
    if len(line)>0:
        if line.split()[0]=='DONE':
            if lines.index(line)+2<=len(lines)-1:
                file1.write(lines[lines.index(line)]+'\n')
                file2.write(lines[lines.index(line)]+'\n')
                file3.write(lines[lines.index(line)]+'\n')
                file1.write(lines[lines.index(line)+1]+'\n')
                file2.write(lines[lines.index(line)+1]+'\n')
                file3.write(lines[lines.index(line)+1]+'\n')
                no_retry_indices.append(lines.index(line)+1)
        if line.split()[0]=='RETRY':
            retry_indices.append(lines.index(line))
print(('Found %i all jobs in current dag' % len(retry_indices)))
print(('Found %i jobs previously DONE' % len(no_retry_indices)))

# ---- filter out only the RETRY that are not associated with DONE nodes
# ---- divide indices in three overlapping lists
final_indices = [a for a in retry_indices if a not in no_retry_indices]
print(('Found %i jobs that still need to be done and will be divided amongst spewed dags' % len(final_indices)))
final_indices_1 = final_indices[0:len(final_indices)/3]
final_indices_2 = final_indices[len(final_indices)/3:2*len(final_indices)/3]
final_indices_3 = final_indices[2*len(final_indices)/3::]

# ---- write to files file1, file2 and file3
# ---- interchange RETRY jobs for fileX to be DONE for fileY
print('Splitting main rescue dag, speweng smaller rescue dags...')
for i in final_indices_1:
    file1.write(lines[i]+'\n')
    s23d = 'DONE' + ' '+ lines[i].split()[1] +'\n'
    s23r = 'RETRY' + ' '+ lines[i].split()[1] + ' '+lines[i].split()[2] +'\n'
    file2.write(s23d)
    file2.write(s23r)
    file3.write(s23d)
    file3.write(s23r)
print(('...just wrote DAG 1 containing %i jobs still to be done' % len(final_indices_1)))
for i in final_indices_2:
    file2.write(lines[i]+'\n')
    s13d = 'DONE' + ' '+ lines[i].split()[1] +'\n'
    s13r = 'RETRY' + ' '+ lines[i].split()[1] + ' '+lines[i].split()[2] +'\n'
    file1.write(s13d)
    file1.write(s13r)
    file3.write(s13d)
    file3.write(s13r)
print(('...just wrote DAG 2 containing %i jobs still to be done' % len(final_indices_2)))
for i in final_indices_3:
    file3.write(lines[i]+'\n')
    s12d = 'DONE' + ' '+ lines[i].split()[1] +'\n'
    s12r = 'RETRY' + ' '+ lines[i].split()[1] + ' '+lines[i].split()[2] +'\n'
    file1.write(s12d)
    file1.write(s12r)
    file2.write(s12d)
    file2.write(s12r)
print(('...just wrote DAG 3 containing %i jobs still to be done...Done!' % len(final_indices_3)))










