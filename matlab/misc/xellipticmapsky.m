function [nullEnergyMap, incoherentEnergyMap, totalEnergyMap, degreesOfFreedom] = ...
    xellipticmapsky(channelNames, conditionedData, sampleFrequency, ...
            amplitudeSpectra, skyPositions, elementLength, elementOffset, ...
            transientLength, frequencyBands, analysisMode, verboseFlag)
% XELLIPTICMAPSKY Construct maps of null-stream energy statistics as a
% function of sky position under the assumption of an ellipticly polarized
% gravitational-wave burst being present.
% 
% XELLIPTICMAPSKY generates a sky map describing the consistency of the signals observed
% in a network of three of more non-aligned detectors with the assumption that
% they are due to a gravitational-wave burst from a particular direction on the
% sky.  The sky map is generated for the requested list of sky positions and the
% requested analysis time scale and frequency bands.  To do so, the conditioned
% data is first partitioned into overlapping elements of the specified length.
% For each element, the signals from all detectors are then examined in the
% frequency domain for consistency with a burst coming from the specified sky
% positions.
%
% usage:
% 
%   [nullEnergyMap, incoherentEnergyMap, totalEnergyMap, degreesOfFreedom] = ...
%       xellipticmapsky(channelNames, conditionedData, sampleFrequency, ...
%               amplitudeSpectra, skyPositions, elementLength, ...
%               transientLengths, frequencyBands, verboseFlag);
%
%    channelNames         cell array of channel name strings
%    conditionedData      matrix time domain conditioned data
%    sampleFrequency      sampling frequency of conditioned data [Hz]
%    amplitudeSpectra     matrix amplitude spectral densities [1/sqrt(Hz)]
%    skyPositions         matrix of sky positions [radians]
%    elementLength        transform length of fourier analyses [samples]
%    elementOffset        Scalar.  Number of samples between start of 
%                         consecutive elements.
%    transientLength      duration of conditioning transients [samples]
%    frequencyBands       matrix of frequency bands to search over [Hz]
%    analysisMode         string specifying 'search', 'veto', or 'frankenveto' analysis
%    verboseFlag          boolean flag to control status output (default 0)
% 
%    nullEnergyMap        resulting null energy map array (see below)
%    incoherentEnergyMap  resulting incoherent energy map array (see below)
%    totalEnergyMap       resulting total energy map array (see below)
%    degreesOfFreedom     number of degrees of freedom for statistical testing
%
% The resulting sky maps are three dimensional array with the following form.
%
%   nullEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex)
%   incoherentEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex)
%   totalEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex)
%
% The conditioned data should be provided as a matrix of time domain data with
% each channel in a separate column.
%
% The amplitude spectral densities should be provided as a matrix of one-sided
% frequency domain data at a frequency resolution corresponding to the desired
% element length and with each channel in a separate column.
%
% The desired sky position should be provided as a two column matrix of the form
% [theta phi], where theta is the geocentric colatitude running from 0 at the
% North pole to pi at the South pole and phi is the geocentric longitude in
% Earth fixed coordinates with 0 on the prime meridian.
%
% The desired frequency bands should be provided as a two column matrix of
% the form [lowFrequency highFrequency].
%
% See also XPIPELINE, XMAPSKY, XREADDATA, XINJECTSIGNAL, XCONDITION, XTILESKY, and
% XINTERPRET.
%
% See also ComputeAntennaResponse, LoadDetectorData.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        process command line arguments                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check for sufficient command line arguments
error(nargchk(10, 11, nargin));

% default arguments
if nargin == 10,
  verboseFlag = 0;
end

% check that analysisMode is a recognized type
if (~(strcmp(analysisMode,'veto') || strcmp(analysisMode,'search') || strcmp(analysisMode,'frankenveto')))
  error('analysisMode is not a recognized type');
end

% force one dimensional cell array
channelNames = channelNames(:);

% number of detectors
numberOfChannels = length(channelNames);

% validate number of detectors
if numberOfChannels < 3,
  error('data is required from three or more non-aligned detectors');
end

% % number of null sums
% numberOfNullSums = numberOfChannels - 2;

% validate conditioned data
if size(conditionedData, 2) ~= numberOfChannels,
  error('conditioned data is inconsistent with number of detectors');
end

% block length in samples
blockLength = size(conditionedData, 1);

% validate sky positions
if size(skyPositions, 2) < 2,
  error('sky positions must be a at least a two column matrix');
end
if any((skyPositions(:, 1) < 0) | (skyPositions(:, 1) > pi)),
  error('theta outside of [0, pi]');
end
if any((skyPositions(:, 2) < -pi) | (skyPositions(:, 2) >= pi)),
  error('phi outside of [-pi, pi)');
end

% number of sky positions
numberOfSkyPositions = size(skyPositions, 1);

% nyquist frequency
nyquistFrequency = sampleFrequency / 2;

% validate frequency bands
if size(frequencyBands, 2) ~= 2,
  error('frequency bands must be a two column matrix');
end
if max(max(frequencyBands)) > nyquistFrequency,
  error('requested frequency bands exceed nyquist frequency');
end
if any(diff(frequencyBands, 1, 2) <= 0),
  error('frequency bands have negative bandwidth');
end

% number of frequency bands to consider
numberOfFrequencyBands = size(frequencyBands, 1);

% validate integer power of two element length
if bitand(elementLength, elementLength - 1),
  error('element length is not an integer power of two');
end

% determine half element lengths
halfElementLength = elementLength / 2 + 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  partition block into overlapping elements                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% calculate the indices of the endpoints of the elements of length
% elementLength with specified overlap
elementIndices = chunkIndices(blockLength, elementLength, transientLength, elementOffset);

% in 'veto' mode keep only the element overlapping the middle of the data 
% segment
if (strcmp(analysisMode,'veto'))
    elementIndices = elementIndices(floor((size(elementIndices,1)+1)/2),:);
end

% Attack of the FrankenVeto monster!
if (strcmp(analysisMode,'frankenveto'))
    % Make sky maps for a small set of chunks centered on the event time.
    % Later combine over chunks into FrankenMaps!
    %
    % Compute chunkIndices using transient time to eliminate all but an  
    % interval of duration 3*elementLength at the center of the block. 
    elementIndices = chunkIndices(blockLength, elementLength, ...
        floor(0.5*(blockLength-3*elementLength)), elementOffset);
end

% number of elements
numberOfElements = size(elementIndices, 1);

% one-sided frequency vector for elements
elementFrequencies = nyquistFrequency * (0 : halfElementLength - 1) / ...
                     (halfElementLength - 1);

% frequency band indices into one-sided frequency domain element
frequencyIndices = cell(numberOfFrequencyBands, 1);
for frequencyBandNumber = 1 : numberOfFrequencyBands,
  frequencyIndices{frequencyBandNumber} = ...
    [find(elementFrequencies >= frequencyBands(frequencyBandNumber, 1),1) ...
     find(elementFrequencies <= frequencyBands(frequencyBandNumber, 2),1,'last')];
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         determine degrees of freedom                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% number of degrees of freedom in each frequency band
degreesOfFreedom = zeros(numberOfFrequencyBands, 1);
for frequencyBandNumber = 1 : numberOfFrequencyBands,
  degreesOfFreedom(frequencyBandNumber) = ...
      diff(frequencyIndices{frequencyBandNumber});
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         preload detector information                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for iDet = 1:numberOfChannels
    % The detector is matched by the first character of the channel name.
    detData      = LoadDetectorData(channelNames{iDet}(1));
    % the position vectors
    rDet(iDet,:) = detData.V';
    % the cell array of detector response tensors
    dDet{iDet}   = detData.d;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  list "internal angle" values to test                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Loop over unknown parameters: relative amplitude 
%      (-1<=eta<=1) and polarisation (0<=psi<pi/2).
eta = [-1:1/4:1];
psi = [0:1/8:0.999]*pi/2;     
[X,Y] = meshgrid(psi,eta);
internalAngles = [X(:) Y(:)]; % (psi,eta)
numberOfInternalAngles = size(internalAngles,1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               precompute detector responses, time delays                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compute detector responses, time delays for each detector and sky
% position.  Arrays are size numberOfSkyPositions x numberOfChannels.
[Fp, Fc] = antennaPatterns(dDet, skyPositions);
timeShifts = computeTimeShifts(rDet, skyPositions);
timeShiftLengths = timeShifts * sampleFrequency;
integerTimeShiftLengths = round(timeShiftLengths);
% residualTimeShiftLengths = timeShiftLengths - integerTimeShiftLengths;
residualTimeShifts = (timeShiftLengths - integerTimeShiftLengths)/sampleFrequency;

% Noise spectra: re-format arrays.
inverseAmplitudeSpectra = amplitudeSpectra.^(-1);
% amplitudeSpectra = permute(repmat(amplitudeSpectra,[1,1,numberOfInternalAngles]),[3,2,1]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        begin loop over sky positions                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% assign storage for energy maps
nullEnergyMap = ...
    zeros(numberOfFrequencyBands, numberOfElements, numberOfSkyPositions);
incoherentEnergyMap = ...
    zeros(numberOfFrequencyBands, numberOfElements, numberOfSkyPositions);
totalEnergyMap = ...
    zeros(numberOfFrequencyBands, numberOfElements, numberOfSkyPositions);

% assign storage for "Full" Fourier-Transformed data series from one
% channel.
elementFFD = zeros(elementLength,1);

realF = cell(numberOfChannels);
imagF = cell(numberOfChannels);
whitenedRealF = cell(numberOfChannels);
whitenedImagF = cell(numberOfChannels);

% begin loop over sky positions
for skyPositionNumber = 1 : numberOfSkyPositions,


    if verboseFlag && (mod(skyPositionNumber, numberOfSkyPositions / 100) < 1)
    fprintf(1, 'processing sky position %d of %d (%d%% complete)...\n', ...
        skyPositionNumber, numberOfSkyPositions, ...
        round(100 * skyPositionNumber / numberOfSkyPositions));            
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %               compute detector responses                                     %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    normF = zeros(size(inverseAmplitudeSpectra,1),numberOfInternalAngles);
    for channelNumber = 1:numberOfChannels
        % Detector response for each internal angle
        realF{channelNumber} = cos(2*internalAngles(:,1))*Fp(skyPositionNumber,channelNumber) ...
            + sin(2*internalAngles(:,1))*Fc(skyPositionNumber,channelNumber);
        imagF{channelNumber} = -(internalAngles(:,2).*sin(2*internalAngles(:,1)))*Fp(skyPositionNumber,channelNumber) ...
            + (internalAngles(:,2).*cos(2*internalAngles(:,1)))*Fc(skyPositionNumber,channelNumber);
        % Whitened responses for each frequency x internal angle. 
        whitenedRealF{channelNumber} = inverseAmplitudeSpectra(:,channelNumber) * (realF{channelNumber})';
        whitenedImagF{channelNumber} = inverseAmplitudeSpectra(:,channelNumber) * (imagF{channelNumber})';
        % Squared modulus of whitened responses for each frequency x internal angle. 
        normF = normF + realpow(whitenedRealF{channelNumber},2)+realpow(whitenedImagF{channelNumber},2);
    end
    % Normalize whitened responses
    normF = realsqrt(normF);
    for channelNumber = 1:numberOfChannels
        whitenedRealF{channelNumber} = whitenedRealF{channelNumber} ./ normF;
        whitenedImagF{channelNumber} = whitenedImagF{channelNumber} ./ normF;
    end

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           residual phase shifts for this sky position                        %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    residualTimeShiftPhases = exp(sqrt(-1) * 2 * pi * ...
    elementFrequencies' * residualTimeShifts(skyPositionNumber,:));


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                         begin loop over elements                             %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % begin loop over elements
    for elementNumber = 1 : numberOfElements
  
        % Initialize GW stream projected data and energies.
        % Z = zeros(halfElementLength,numberOfInternalAngles);
        realZ = zeros(halfElementLength,numberOfInternalAngles);
        imagZ = zeros(halfElementLength,numberOfInternalAngles);
        gwEnergy = zeros(halfElementLength,numberOfInternalAngles);
        % incoherentEnergy = zeros(halfElementLength,numberOfInternalAngles);
        totalEnergy = zeros(halfElementLength,1);

        % start and stop indices of element
        elementStartIndex = elementIndices(elementNumber, 1);
        elementStopIndex = elementIndices(elementNumber, 2);
        
        % begin loop over detectors
        for channelNumber = 1 : numberOfChannels

            % extract time-domain data and FFT
            elementFFD = fft(conditionedData( ...
                (elementStartIndex + integerTimeShiftLengths(skyPositionNumber,channelNumber)) : ...
                (elementStopIndex + integerTimeShiftLengths(skyPositionNumber,channelNumber)), ...
                channelNumber));

            % take one-sided transform
            elementFD = elementFFD(1 : halfElementLength, :);

            % apply residual time shift
            elementFD = elementFD .* residualTimeShiftPhases(:,channelNumber);

            % total energy at each sky position and frequency bin.  
            %   Size: halfElementLength x 1
            totalEnergy = totalEnergy + real(elementFD).^2 + imag(elementFD).^2;

            % Project out GW component.
            % Z = Z + (whitenedRealF{channelNumber} - sqrt(-1)*whitenedImagF{channelNumber}) .* repmat(elementFD,[1,numberOfInternalAngles]);
            elementFDrep = repmat(elementFD,[1,numberOfInternalAngles]);
            % Z = Z + (whitenedRealF{channelNumber} - sqrt(-1)*whitenedImagF{channelNumber}) .* elementFDrep;
            realZ = realZ + whitenedRealF{channelNumber}.*real(elementFDrep) + whitenedImagF{channelNumber}.*imag(elementFDrep);
            imagZ = imagZ + whitenedRealF{channelNumber}.*imag(elementFDrep) - whitenedImagF{channelNumber}.*real(elementFDrep);

        end
        % GW = total - null energy at each internal angle and frequency
        % bin. 
        %   Size: numberOfInternalAngles x 1 x halfElementLength            
        % gwEnergy = realZ.^2 + imagZ.^2;
        % What do we do with the multiple gwEnergy values for different 
        % internal angles? KLUDGE: For each freq bin select energy from
        % internal angle giving largest energy.
        %   Size: halfElementLength x 1
        % NOTE:  NOT REQUIRING SAME INTERNAL ANGLE FOR EACH T-F BIN!!!
        gwEnergy = max(realZ.^2 + imagZ.^2,[],2); 

        % ---- Cumulative sum of energies over frequency.
        gwEnergy = [0; cumsum(gwEnergy)];
        totalEnergy = [0; cumsum(totalEnergy)];
    
        % ---- Extract energies summed over analysis frequency bands and 
        %      insert results into sky map arrays.
        for frequencyBandNumber = 1 : numberOfFrequencyBands,
            nullEnergyMap(frequencyBandNumber, elementNumber, skyPositionNumber) = ...
                diff(gwEnergy(frequencyIndices{frequencyBandNumber}));
%             incoherentEnergyMap(frequencyBandNumber, elementNumber, skyCells{jSkyCells,2}) = ...
%                 diff(incoherentEnergy(frequencyIndices{frequencyBandNumber}-1,:));
            totalEnergyMap(frequencyBandNumber, elementNumber, skyPositionNumber) = ...
                diff(totalEnergy(frequencyIndices{frequencyBandNumber}));
        end
    

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                          end loop over elements                              %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
    % end loop over elements
    end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         end loop over sky positions                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% end loop over sky cells
end
nullEnergyMap = totalEnergyMap - nullEnergyMap;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         frankenmaps                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (strcmp(analysisMode,'frankenveto'))

    %----- Each type of map: combine over chunks into FrankenMaps!

    %----- Sum over frequencies, and make the singleton dimension last (gone)
    tEM = permute(sum(totalEnergyMap,1),[2 3 1]);

    %----- For each sky pixel, find segment with maximum total energy.
    [valtot, indextot] = max(tEM,[],1);

    %----- Make FrankenMaps based on total energy.
    for frequencyBandNumber = 1:numberOfFrequencyBands,
        sindex = sub2ind(...
            [numberOfFrequencyBands numberOfElements numberOfSkyPositions],...
            frequencyBandNumber*ones(1,numberOfSkyPositions),...
            indextot,1:numberOfSkyPositions);

        frankenNullEnergyMap(frequencyBandNumber,1,:) = nullEnergyMap(sindex);
        frankenIncoherentEnergyMap(frequencyBandNumber,1,:) = incoherentEnergyMap(sindex);
        frankenTotalEnergyMap(frequencyBandNumber,1,:) = totalEnergyMap(sindex);
    end
    nullEnergyMap = frankenNullEnergyMap;
    incoherentEnergyMap = frankenIncoherentEnergyMap;
    totalEnergyMap = frankenTotalEnergyMap;

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          return to calling function                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% return to calling function
return;
