function [x_val] = lowerbound(x,y,y_val)
% LOWERBOUND - minimum XI such that Y(X)>=YI for all X>=XI.
%
%   XI = lowerbound(X,Y,YI)
%
%   X       Vector of values of independent variable "x".
%   Y       Vector of values of dependent variable "y".  Must be of same length
%           as X.
%   YI      Scalar of value of y at which x value is desired.
%
%   XI      Interpolated value of x at YI. 
%
% The vectors X,Y must have the same length but need not be the same size 
% (row/column vectors).  
%
% LOWERBOUND interpolates linearly in (log10(X),Y).  It returns the 
% smallest XI such that Y>=YI for all X>=XI.  (This asymmetric approach and
% the use of log10 are intended for gravitational-wave upper limit analyses.)
%
% $Id$

% ---- Check input arguments.
error(nargchk(3,3,nargin));

% ---- Check for vector inputs.
if ~(isvector(x) && isvector(y) && isscalar(y_val))
    error('Input arguments X,Y,YI must be scalars or vectors.')
end

% ---- Check length of x and y vectors matches.
if length(x) ~= length(y)
    disp(['x length is ' num2str(length(x))])
    disp(['y length is ' num2str(length(y))])
    error('x and y vectors must have the same length');
end

% ---- Sort vectors using x values.
[x,is] = sort(x);
y = y(is);

% ---- Work through all the other possibilities.  Tricky!
if max(y) < y_val
    warning(['Largest sampled efficiency (' num2str(max(y)) ') is below ' ...
        'requested value of ' num2str(y_val) '.']);
    x_val = NaN;
elseif min(y)>=y_val
    % ---- All sampled efficiencies are >= target value, so return lowest x.
    warning(['Smallest sampled efficiency (' num2str(min(y)) ') is above ' ...
        'requested value of ' num2str(y_val) '.']);
    x_val = min(x);
else
    % ---- Find last point with y<y_val.
    index = find(y<y_val,1,'last');
    if index==length(x)
        x_val = NaN;
    else
        x_val = 10.^interp1(y(index:(index+1)),log10(x(index:(index+1))),y_val);
    end
end

% ---- Done.
return

