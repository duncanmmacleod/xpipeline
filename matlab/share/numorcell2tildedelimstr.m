function outstring = numorcell2tildedelimstr(inarray)
% numorcell2tildedelimstr: Convert a double or cell 
% array to a tilde-delimited string.
% 
%   outstring = tildedelimstr2numorcell(inarray)
%
%   inarray    Nx1 cell or double array. 
%
%   outstring  Tilde-delimited string containing the N elemets
% 
% 

% ---- check input
if not(isvector(inarray))
  error('Input array must be either a row or column vector')
end

% ---- transform to cell array for unified processing
if not(iscell(inarray))
  inarray = num2cell(inarray);
end

% ---- construct output string
outstring = '';
for iCell = 1:length(inarray)
  if isnumeric(inarray{iCell}) && isscalar(inarray{iCell})
    outstring = [ outstring num2str(inarray{iCell})  '~'];
  elseif ischar(inarray{iCell})
    outstring = [ outstring inarray{iCell}  '~'];
  else
    error('Only numbers and strings can be processed');
  end
end

% remove trailing tilde
if not(isempty(outstring))
  outstring = outstring(1:end-1);
end
