function mergedSet = xclustermerge(clusterSet1,clusterSet2)
% XCLUSTERMERGE - Merge sets of clusters into a single cluster struct.
%
% usage:
%
%   mergedSet = xclustermerge(clusterSet1,clusterSet2)
%
%   clusterSet1 1x1 Struct of clusters.  Each field must contain only
%               numeric data, with the same number of rows.
%   clusterSet2 Same as clusterSet1.
%
%   mergedSet   1x1 Struct of clusters.  Contains the clusters from both
%               clusterSet1 and clusterSet2.  The clusters are not sorted;
%               those from clusterSet1 appear first. 
% 
% Each field in the input cluster sets must contain only numeric data.
% The X-Pipeline convention is that all fields in a cluster struct should
% have the same number of rows; however, xclustermerge does not check this
% (it is not needed for the merge operation).  The fields in mergedSet will be
% in the same order as in clusterSet1. 
%
% An alternative usage is to merge a struct array of clusters (as used for
% storing injection clusters)into a single struct.
%
%   mergedSet = xclustermerge(clusterArray)
%
%   clusterArray Struct array of clusters. 
%
%   mergedSet    As above.
%
% $Id$

% ---- Checks.
error(nargchk(1,2,nargin));

if nargin==1

    if ~isstruct(clusterSet1)
        error('Input argument clusterArray must be a struct.')
    end

    % ---- Procedure: No ordering checks are needed because we're dealing with a
    %      single struct. Initialise output to first element of array, then loop
    %      over other elements and fields, appending all data into output struct. 
    
    % ---- Initialise output.
    mergedSet = clusterSet1(1);
    clusterFields = fieldnames(mergedSet);

    % ---- This is probably a very inefficient procedure! Best to pre-allocate
    %      storage since we know the final size of the merged set.
    for jj = 2:numel(clusterSet1)
        % ---- Loop over all fields, merging.
        for ii = 1:length(clusterFields)
            % ---- Get the data for this field.
            fieldValue1 = getfield(mergedSet,clusterFields{ii});
            fieldValue2 = getfield(clusterSet1(jj),clusterFields{ii});
            % ---- Check that number of columns matches.
            if (size(fieldValue1,2) ~= size(fieldValue2,2) && ...
                  size(fieldValue2,2) ~= 0 && size(fieldValue1,2) ~= 0)
              size(fieldValue1)
              size(fieldValue2)
                error(['The two input clusterSets must have the same number '...
                    'of columns for each field.'])
            end
            % ---- Write the merged data field back into mergedSet.
            mergedSet = setfield(mergedSet,clusterFields{ii},[fieldValue1; fieldValue2]);
        end
    end
    
elseif nargin==2
    
    if ~isstruct(clusterSet1) || ~isstruct(clusterSet2) 
        error('Input argument clusterSets must be a 1x1 struct.')
    end
    if max(size(clusterSet1))>1 || max(size(clusterSet2))>1
        error('Input argument clusterSets must be a 1x1 struct.')
    end

    % ---- Procedure: Order the fields in the structs to match.  Initialize
    %      mergedSet to clusterSet1.  Loop over fields, appending all data from
    %      clusterSet2 into mergedSet. 

    % ---- Order fields in set 2 to match those in set 1. This operation
    %      also automatically verifies that both structs have the same fields;
    %      however the error message in this case is not very revealing, so we put
    %      it in a try statement to allow us to report a useful verbose message for
    %      this likely error mode. 
    try
        clusterSet2 = orderfields(clusterSet2,clusterSet1);
    catch
        % ---- Verify that field names are the same. 
        if ~isequal(fieldnames(orderfields(clusterSet1)),fieldnames(orderfields(clusterSet2))) 
            error('Input structs contain different fields; cannot be merged.');
        end
    end

    % ---- Initialise output.
    mergedSet = clusterSet1;
    clusterFields = fieldnames(mergedSet);

    % ---- Loop over all fields, merging.
    for ii = 1:length(clusterFields)
        % ---- Get the data for this field.
        fieldValue1 = getfield(mergedSet,clusterFields{ii});
        fieldValue2 = getfield(clusterSet2,clusterFields{ii});
        % ---- Check that number of columns matches.
        if (size(fieldValue1,2) ~= size(fieldValue2,2) && ...
              size(fieldValue2,2) ~= 0 && size(fieldValue1,2) ~= 0)
          size(fieldValue1)
          size(fieldValue2)
            error(['The two input clusterSets must have the same number '...
                'of columns for each field.'])
        end
        % ---- Write the merged data field back into mergedSet.
        mergedSet = setfield(mergedSet,clusterFields{ii},[fieldValue1; fieldValue2]);
    end
    
end

% ---- Done.
return
