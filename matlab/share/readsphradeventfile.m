function [clusterArray, likelihoodType] = readsphradeventfile(fileName,verbose,T,overlap)
% READSPHRADEVENTFILE - Read events from a sphrad.c formatted event file.
%
% usage:
%
%  [clusterArray, likelihoodType] = readsphradeventfile(fileName)
%
% fileName         String. Name of file to be read.
% verbose          Optional flag. if true/nonzero then extra info dumped to 
%                  screen. Default false.
% T                Optional scalar. FFT length [sec]. Default 1.
% overlap          Optional scalar. Fractional overlap between consecutive 
%                  timebins. Default 0.5.
%
% clusterArray     Numeric array of events. See clusterTFmapNew for
%                  details of the format.
% likelihoodType   Cell array of strings (column format). Names of the 
%                  likelihood types computed for the events in clusterArray.
%
% Note: Two KLUDGEs are applied to the sphrad triggers:
% 1) The shenergy, shinc likelhoods are redefined as "circ" likelihoods, as 
%    follows:
%        circenergy := shenergy + shinc
%        circinc    := shinc
%    This KLUDGE allows allows the post-processing to make cuts on this 
%    likelihood pair.
% 2) The likelihoods are rescaled to approximately correct for the normalisation
%    differences between X-Pipeline and SphRad. The scale factors are as follows:
%        scale 1 = 1e4 * T^2 (12,14)
%        scale 2 = 1e6 * T^2 (16, 17, 18, 19, 20, 21, 22, 23)
%        scale 3 = 1e11 * T^4 (24)
% 
% $Id$

% ---- Assign default arguments.
if (nargin<4)
    overlap = 0.5;
end
if (nargin<3)
    T = 1;
end
if (nargin<2)
    verbose = false;
end

% ---- Debug 
disp(['FileName : ' fileName])

% ---- Read file.
fid = fopen(fileName,'r');
formatSpec = '%s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f';
C = textscan(fid,formatSpec);
fclose(fid);

% ---- KLUDGE: The likelihoods are rescaled to approximately correct for the 
%      normalisation differences between X-Pipeline and SphRad. The scale 
%      factors are as follows:
%        scale 1 = 1e4 * T^2 (12,14)
%        scale 2 = 1e6 * T^2 (16, 17, 18, 19, 20, 21, 22, 23)
%        scale 3 = 1e11 * T^4 (24)
scaleFactor = ones(1,24);
scaleFactor([12,14]) = 1e4 * T^2;
scaleFactor([16:23]) = 1e6 * T^2;
scaleFactor(24)      = 1e11 * T^4;
for ii=12:24
    C{ii} = C{ii} * scaleFactor(ii);
end
 
% ---- Columns in file, according to the comments and code of the function 
%      output_omega_event at line 275 of r4318 of 
%      branches/sphrad/dependencies/omega/src/SphRad/sphrad_misc.c
%
%  1 network-(H1, L1, V1) 
%  2 blockStartTime (full 256 sec)
%  3 blockStopTime 
%  4 livetime (248 s)
tmp_time      = C{5};
tmp_frequency = C{6}; 
tmp_duration  = C{7}; 
tmp_bandwidth = C{8};
%  9 modeTheta 
% 10 modePhi 
% 11 logSignal (0)
% 12 logGlitch (shE)
tmp_nPix      = C{13}; % logOdds (numPix)
% 14 probSignal (shI)
% 15 probGlitch (0)
% 16 crossEnergy 
% 17 crossIncoherentEnergy 
% 18 plusEnergy 
% 19 plusIncoherentEnergy
% 20 nullEnergy 
% 21 nullIncoherentEnergy 
% 22 standardEnergy 
% 23 standardIncoherentEnergy
% 24 clusterEnergy

% ---- Likelihood types, in X-standard names were possible. Output as column 
%      array to match format in xdetection.
likelihoodType = {'clusterenergy','standardenergy','standardinc', ...
    'plusenergy','plusinc','crossenergy','crossinc','nullenergy','nullinc', ...
    'shenergy','shinc','skypositiontheta','skypositionphi'}';
% ---- Corresponding column numbers in "C".
colidx = [24,22,23,18,19,16,17,20,21,12,14,9,10];

% ---- Number of events.
Ntrig = size(C{1},1);
% ---- Optional verbosity.
if verbose
    disp(['readsphradeventfile: reading ' num2str(Ntrig) ' events from ' fileName '.']);
end
% ---- Assign storage for output.
clusterArray = zeros(Ntrig,7+13);
% ---- Fill time-frequency information.
clusterArray(:,1) = tmp_time - 0.5*tmp_duration - T*(1-overlap);
clusterArray(:,2) = tmp_time + T*(overlap-0.5);
clusterArray(:,3) = tmp_time + 0.5*tmp_duration + T*overlap;
clusterArray(:,4) = tmp_frequency - 0.5*tmp_bandwidth - 0.5/T;
clusterArray(:,5) = tmp_frequency;
clusterArray(:,6) = tmp_frequency + 0.5*tmp_bandwidth + 0.5/T;
clusterArray(:,7) = tmp_nPix;
% ---- Fill likelihoods.
for ii=1:length(colidx)
    clusterArray(:,7+ii) = C{colidx(ii)};
end

% ---- KLUDGE: define likelhoods that the post-processing will know how to make
%      cuts with:
%        circenergy := shenergy + shinc
%        circinc    := shinc
%      With these re-definitions the post-processing will "recognize" the new 
%      likelihoods and attempt to make background rejection cuts with them. 
%      The definition of circenergy gives the  
%      E ~ I behaviour expected for glitches by the post-processing. 
clusterArray(:,17) = clusterArray(:,17) + clusterArray(:,18);
likelihoodType{10} = 'circenergy';
likelihoodType{11} = 'circinc';

% ---- Done.
return
