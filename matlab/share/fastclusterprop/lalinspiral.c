/* Based on LALPSpinInspiralRDTest.c by Riccardo Sturani, 
   addapted and mex file wrapped by Michal Was */

/*
*  Copyright (C) 2010 Riccardo Sturani
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with with program; see the file COPYING. If not, write to the
*  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
*  MA  02111-1307  USA
*  
* Compile with 
* mex `pkg-config --cflags lal lalsupport lalinspiral` `pkg-config --libs lal lalsupport lalinspiral` lalinspiral.c
*
*/

#include "mex.h"
#include <math.h>
#include <lal/LALStdlib.h>
#include <lal/LALInspiral.h>
#include <lal/GeneratePPNInspiral.h>
#include <lal/GenerateInspiral.h>

#include <lal/Units.h>
#include <lal/SeqFactories.h>
#include <lal/NRWaveInject.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    static LALStatus    mystatus;

    CoherentGW          thewaveform;
    SimInspiralTable    injParams; /* Class defined in LIGOMetadataTables.h */
    PPNParamStruc       ppnParams; /* Defined in GeneratePPNInspiral.h */

    const char  *filename = "wave1.dat";
    FILE        *outputfile;
    INT4        i,length;
    REAL8       dt;
    double      *t, *hp, *hc, *freq;
    double      *a1, *a2, *phi, *shift ;
    
    const REAL8 omf=0.058;
    REAL8 ff;

    CHAR waveformName[256];

    int nDims;
    const int *dimArray;
    const mxArray *cellContent;
 
    memset( &mystatus, 0, sizeof(LALStatus) );
    memset( &thewaveform, 0, sizeof(CoherentGW) );
    memset( &injParams, 0, sizeof(SimInspiralTable) );
    memset( &ppnParams, 0, sizeof(PPNParamStruc) );

    /* --- first we fill the SimInspiral structure with defaults--- */

    injParams.mass1 = 1.4;
    injParams.mass2 = 1.4;

    /*Inclination sets the angle between the line of site and initial J*/
    injParams.inclination  = 0.;

    /* this is given in Mpc */
    injParams.distance = 1.;

    /* Waveform family */
    snprintf(injParams.waveform,LIGOMETA_WAVEFORM_MAX*sizeof(CHAR),"TaylorT2threePN");

    /* this is given in Hz*/
    injParams.f_lower  = 40.;
    /*ff=omf/(injParams.mass1+injParams.mass2)/LAL_MTSUN_SI/LAL_PI;*/

    /* Polar angles of the source arrival direction and the usual polarization 
       angle enters the pattern functions, they do not matter for waveform 
       construction, so they won't be st here.*/ 

    injParams.spin1x = 0.;
    injParams.spin1y = 0.;
    injParams.spin1z = 0.;

    injParams.spin2x = 0.;
    injParams.spin2y = 0.;
    injParams.spin2z = 0.;

    /*Spin units are such that multiplying spini by m_i^2 one obtains the physical spin */

    ppnParams.deltaT = 1.0 / 4096.0 /2.;
    /* fStop is set for debugging purposes. The working version of 
       LALPSpinInspiralRD simply ignores its value 
       ppnParams.fStop  = ff;*/
    /* Initial shift in the phase*/
    ppnParams.phi    = 0.;
    injParams.coa_phase=ppnParams.phi;

    /* --- Read input parameters --- */
    if (nrhs > 0)
      {
	/* Number of dimensions and size */
	nDims=mxGetNumberOfDimensions(prhs[0]);
	dimArray=mxGetDimensions(prhs[0]);
	const int nParams=dimArray[0]*dimArray[1];
	/* copy parameters one by one assuming order below*/
	for( i=0 ; i < nParams ; i++)
	  {
	    cellContent = mxGetCell(prhs[0],i);
	    switch (i) 
	      {
	      case 0:
		ppnParams.deltaT = 1.0 / *mxGetPr(cellContent);break;
	      case 1:
		injParams.mass1 = *mxGetPr(cellContent);break;
	      case 2:
		injParams.mass2 = *mxGetPr(cellContent);break;
	      case 3:
		injParams.inclination = *mxGetPr(cellContent);break;
	      case 4:
		injParams.distance = *mxGetPr(cellContent);break;
	      case 5:
		mxGetString(cellContent,injParams.waveform,LIGOMETA_WAVEFORM_MAX);break;
	      case 6:
		injParams.f_lower = *mxGetPr(cellContent);break;
	      case 7:
		injParams.spin1x = *mxGetPr(cellContent);break;
	      case 8:
		injParams.spin1y = *mxGetPr(cellContent);break;
	      case 9:
		injParams.spin1z = *mxGetPr(cellContent);break;
	      case 10:
		injParams.spin2x = *mxGetPr(cellContent);break;
	      case 11:
		injParams.spin2y = *mxGetPr(cellContent);break;
	      case 12:
		injParams.spin2z = *mxGetPr(cellContent);break;
	      case 13:
		ppnParams.phi = *mxGetPr(cellContent);
		injParams.coa_phase = ppnParams.phi;break;
	      default:
		fprintf(stderr,"More parameters than expected. Expected at most 14, obtained %d.",nParams);
		return;
	      }
	  }
      }


    /* --- now we can call the injection function --- */
    LALGenerateInspiral( &mystatus, &thewaveform, &injParams, &ppnParams );
    if ( mystatus.statusCode )
    {
      fprintf( stderr, "LALGenerateInspiral: error generating waveform %d\n",mystatus.statusCode );
      exit( 1 );
    }

    /* --- save in output matlab arrays --- */
    length  = thewaveform.a->data->length;

    /* allocate output matrix, the matrix is filled with zeros */
    plhs[0] = mxCreateDoubleMatrix(length, 1, mxREAL); 
    t = mxGetPr(plhs[0]);
    plhs[1] = mxCreateDoubleMatrix(length, 1, mxREAL); 
    hp = mxGetPr(plhs[1]);
    plhs[2] = mxCreateDoubleMatrix(length, 1, mxREAL); 
    hc = mxGetPr(plhs[2]);
    plhs[3] = mxCreateDoubleMatrix(length, 1, mxREAL); 
    freq = mxGetPr(plhs[3]);
    plhs[4] = mxCreateDoubleMatrix(length, 1, mxREAL); 
    a1 = mxGetPr(plhs[4]);
    plhs[5] = mxCreateDoubleMatrix(length, 1, mxREAL); 
    a2 = mxGetPr(plhs[5]);
    plhs[6] = mxCreateDoubleMatrix(length, 1, mxREAL); 
    phi = mxGetPr(plhs[6]);
    plhs[7] = mxCreateDoubleMatrix(length, 1, mxREAL); 
    shift = mxGetPr(plhs[7]);

    /* Computing h+ and hx based on this documentation:
      We therefore write the waveforms in terms of
      two polarization amplitudes $A_1(t)$ and $A_2(t)$, a single phase
      function $\phi(t)$, and a polarization shift $\Phi(t)$:
      \begin{eqnarray}
      \label{eq:quasiperiodic-hplus}
      h_+(t) & = & A_1(t)\cos\Phi(t)\cos\phi(t)
      - A_2(t)\sin\Phi(t)\sin\phi(t) \; , \				\
      \label{eq:quasiperiodic-hcross}
      h_\times(t) & = & A_1(t)\sin\Phi(t)\cos\phi(t)
      + A_2(t)\cos\Phi(t)\sin\phi(t) \; .
      \end{eqnarray}
      The physical meaning of these functions is shown in
      Fig.~\ref{fig:phase-diagram}.

      \item[\texttt{REAL4 psi}] The polarization angle $\psi$, in radians,
      as defined in Appendix~B of~\cite{Anderson_W:2000}.
      
      \item[\texttt{REAL4TimeVectorSeries *h}] A time-sampled
      two-dimensional vector storing the waveforms $h_+(t)$ and
      $h_\times(t)$, in dimensionless strain.
      
      \item[\texttt{REAL4TimeVectorSeries *a}] A time-sampled
      two-dimensional vector storing the amplitudes $A_1(t)$ and $A_2(t)$,
      in dimensionless strain.

      \item[\texttt{REAL4TimeSeries *f}] A time-sampled sequence storing the
      instantaneous frequency $f(t)$, in Hz.
      
      \item[\texttt{REAL8TimeSeries *phi}] A time-sampled sequence storing
      the phase function $\phi(t)$, in radians.
      
      \item[\texttt{REAL4TimeSeries *shift}] A time-sampled sequence storing
      the polarization shift $\Phi(t)$, in radians.
      \end{description}
    */

    dt      = thewaveform.phi->deltaT;
    for(i = 0; i < length; i++) {
      t[i]   = i*dt;
      a1[i]  = thewaveform.a->data->data[2*i];
      a2[i]  = thewaveform.a->data->data[2*i+1];
      freq[i]  = thewaveform.f->data->data[i];
      phi[i]  = thewaveform.phi->data->data[i];
      if (thewaveform.shift == NULL)
	shift[i] = 0;
      else
	shift[i]  = thewaveform.shift->data->data[i];
      hp[i] = a1[i]*cos(shift[i])*cos(phi[i]) - a2[i]*sin(shift[i])*sin(phi[i]);
      hc[i] = a1[i]*sin(shift[i])*cos(phi[i]) + a2[i]*cos(shift[i])*sin(phi[i]);
    }

    return ;
}
