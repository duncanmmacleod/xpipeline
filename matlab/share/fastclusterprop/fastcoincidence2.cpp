// Compute the coincidence between two list of segments.  Each list of
// segment is described by a 2 column matrix with start time and
// duration. It is assumed that in the first list the segments are not
// overlapping. The output is a 2 column matrix of segment
// intersection with index in first list and index in second for each
// intersection


// Authors:
//
//    Dorota Was
//    Michal Was    michal.was@ligo.org

#include "math.h"
#include "mex.h"   
#include<cstdio>
#include<set>
#include<list>
#include<algorithm>
#include<cmath>

using namespace std;

class Segment{
public:
  double beg, end;
  int num;
  list<int> L;
  
  Segment() {
    beg = end = 0;
    num = -1;
    L.clear();
  }
  Segment(double b, double e, int n) {
    beg = b; end = e; num = n;
    L.clear();
  }
  bool operator < (const Segment & A) const {
    if(beg != A.beg)
      return beg < A.beg;
    if(end != A.end)
      return end < A.end;
    return num < A.num;
  }
};
class Event{
public:
  double x;
  int num;
  bool start;
  
  Event() {
    x = 0;
    num = 0;
    start = false;
  }
  Event(double x2, int n, bool s) {
    x = x2; num = n; start = s;
  }
  bool operator < (const Event & A) const {
    if(x != A.x)
      return x < A.x;
    if(start != A.start)
      return start;
    return num < A.num;
  }
};

void update(double beg, double end, set<int> &S, Event* T, int &idx, int maxIdx) { // ()
  while(idx < maxIdx && T[idx].x <= beg) // We should receive a idx, such that T[idx].x > beg;
    idx++;
  while(idx < maxIdx && T[idx].x < end) {
    if(T[idx].start)
      S.insert(T[idx].num);
    else {
      S.erase(S.find(T[idx].num));
    }
    idx++;
  }
}
void updateWithL(double beg, double end, set<int> &S, list<int> &L, Event* T, int &idx, int maxIdx) { // []
  for(set<int>::iterator i=S.begin(); i!=S.end(); i++)
    L.push_back(*i);
  while(idx < maxIdx && T[idx].x < beg) // We should receive a idx, such that T[idx].x >= beg;
    idx++;
  while(idx < maxIdx && T[idx].x <= end) {
    if(T[idx].start) {
      S.insert(T[idx].num);
      L.push_back(T[idx].num);
    }
    else {
      S.erase(S.find(T[idx].num));
    }
    idx++;
  }
}

Segment* process(int N, int M, const double* Ntab, const double* Mtab) { // Remember to do delete[] on the result!
  Segment* ListOne = new Segment[N];
  Event* ListTwo = new Event[2*M];
  double minX = Mtab[0];
  double maxX = Mtab[1];
  for(int i=0; i<N; i++)
    ListOne[i] = Segment(Ntab[i], Ntab[i] + Ntab[i+N], i);
  for(int i=0; i<M; i++) {
    ListTwo[i] = Event(Mtab[i], i, true);
    ListTwo[i+M] = Event(Mtab[i] + Mtab[i+M], i, false);
    minX = min(minX, Mtab[i]);
    maxX = max(maxX, Mtab[i+M]);
  }
  sort(ListOne, ListOne+N);
  sort(ListTwo, ListTwo+2*M);
  for(int i=0; i+1 < N; i++)
    {
      if (ListOne[i].end >= ListOne[i+1].beg)
	{
	  mexErrMsgTxt("Error the segments (first input variable) are not disjoint");
	}
    }
  set<int> S;
  S.clear();
  int idx = 0;
  update(minX-1, ListOne[0].beg, S, ListTwo, idx, 2*M);
  for(int i=0; i<N; i++) {
    updateWithL(ListOne[i].beg, ListOne[i].end, S, ListOne[i].L, ListTwo, idx, 2*M);
    if(i+1 < N)
      update(ListOne[i].end, ListOne[i+1].beg, S, ListTwo, idx, 2*M);
  }
  update(ListOne[N-1].end, maxX+1, S, ListTwo, idx, 2*M);
    
  delete[] ListTwo;
  return ListOne;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  //Declaration
  const mxArray *segmentsArray;
  const mxArray *eventsArray;
  const double *segments;
  const double *events;
  double *Result;
  int nDims;
  const int *dimArray;

  //Copy to pointer
  segmentsArray=prhs[0];
  eventsArray=prhs[1];
  
  // transform arrays to doubles
  segments=mxGetPr(segmentsArray);
  events=mxGetPr(eventsArray);
  
  // Number of dimesnion and size
  nDims=mxGetNumberOfDimensions(segmentsArray);
  if (2 != nDims)  {
    printf("%s\n","Error the number of dimension for segments is not 2 ");
    return ;
  }
  dimArray=mxGetDimensions(segmentsArray);
  const int nSegments=dimArray[0];
  if(dimArray[1] != 2) {
    printf("%s\n","Error the number of columns for segments is not 2");
    return;
  }
  nDims=mxGetNumberOfDimensions(eventsArray);
  if (2 != nDims)  {
    printf("%s\n","Error the number of dimension for events is not 2 ");
    return ;
  }
  dimArray=mxGetDimensions(eventsArray);
  const int nEvents=dimArray[0];
  if(dimArray[1] != 2) {
    printf("%s\n","Error the number of columns for events is not 2");
    return;
  }

  Segment* ListOne = process(nSegments, nEvents, segments, events);

  // copy over result to matrix
  int sizeResult = 0;
  for(int i=0; i<nSegments; i++)
    sizeResult += ListOne[i].L.size();
  // allocate output matrix, the matrix is filled with zeros
  plhs[0] = mxCreateDoubleMatrix(sizeResult, 2, mxREAL); 
  Result = mxGetPr(plhs[0]);

  int idx = 0;
  for(int i=0; i<nSegments; i++)
    for(list<int>::iterator j=ListOne[i].L.begin(); j!=ListOne[i].L.end(); j++) {
      // shift indices by 1 because in matlab indices start from 1
      Result[idx] = ListOne[i].num + 1;
      Result[idx+sizeResult] = (*j) + 1;
      idx++;
    }

  delete[] ListOne;

  return;
}
    
