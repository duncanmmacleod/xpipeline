// fast replacement for regionprops.m :
// input : lablledMap, likelihoodMap
// output: clusterArray
// for details see clusterTFmapNew 


#include "math.h"
#include "mex.h"   
#include "stdio.h"
#include <vector>

inline double max(double a,double b) 
{
  if(a > b) {return a;}
  return b;
}

inline double min(double a,double b) 
{
  if(a < b) {return a;}
  return b;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  //Declaration
  const mxArray *labelledMapArray;
  const mxArray *likelihoodMapArray;
  const double *labelledMap;
  const double *likelihoodMap;

  double *statSumMap;
  int nDims;
  const int *dimArray;

  //Copy to pointer
  labelledMapArray=prhs[0];
  likelihoodMapArray=prhs[1];
  
  // transform arrays to doubles
  labelledMap=mxGetPr(labelledMapArray);
  likelihoodMap=mxGetPr(likelihoodMapArray);
  
  // Number of dimesnion and size
  nDims=mxGetNumberOfDimensions(likelihoodMapArray);
  if (3 != nDims && 2 != nDims)  {
    printf("%s\n","Error the number of dimension for likelihoodMap is not 2 or 3");
    return ;
  }
  dimArray=mxGetDimensions(likelihoodMapArray);
  int colLen=dimArray[0];
  int rowLen=dimArray[1];
  int nLikelihoods;
  if( 3== nDims) {
    nLikelihoods=dimArray[2]; }
  else {
    nLikelihoods=1;}
    


  int nClusters=0;
  for(int i=0;i<rowLen;i++){
    for(int j=0;j<colLen;j++){
      nClusters=(int)max(double(nClusters),labelledMap[i*colLen + j]);
    }
  }

  // allocate output matrix, the matrix is filled with zeros
  plhs[0] = mxCreateDoubleMatrix(colLen, rowLen, mxREAL); 
  statSumMap = mxGetPr(plhs[0]);
  std::vector<double> clusterArray( nClusters );

  for(int i=0;i<rowLen;i++){
    for(int j=0;j<colLen;j++){
      int label=int(labelledMap[i*colLen + j])-1;
      if( -1 == label) {continue;}
      if( label >= nClusters )
	{
	  printf("The label is to large %d >= %d \n",label,nClusters);
	  return;
	}
	
      //compute sum for first likelihood
      clusterArray[label]= clusterArray[label]+
	likelihoodMap[i*colLen + j];
      
     
    }
  }
  
  // replace labels with sum of first likelihood
  for(int i=0;i<rowLen;i++){
    for(int j=0;j<colLen;j++){
      int label=int(labelledMap[i*colLen + j])-1;
      if( -1 == label) {continue;}
      //compute sum for first likelihood
      if(int(labelledMap[i*colLen + j])-1<0 || int(labelledMap[i*colLen + j])-1 >=nClusters)
	{
	  printf("Label: %d out of range %d \n",int(labelledMap[i*colLen + j])-1,nClusters);
	  return;
	}
      statSumMap[i*colLen + j] = 
	clusterArray[int(labelledMap[i*colLen + j])-1];
     
    }
  }

  
  return;
}
    
