function [likelihoodTimeFrequencyMap, skyPositionIndex, ...
    detectorSpectrogram,likelihoodType,skyPositionClusterCell,likelihood_powerlaw] = ...
    xbayesiantimefrequencymap2(channelNames, conditionedData, sampleFrequency, ...
        amplitudeSpectra, skyPositions, integrationLength, offsetLength, ...
        transientLength, frequencyBand, windowType, likelihoodType, ...
        outputType, verboseFlag)
% XBAYESIANTIMEFREQUENCYMAP - initially copied from xtimefrequencymap.m r2782
% this function will be expanded to perform
%  i) marginalisation over sky position and then
%  ii) marginalisation over GW signal amplitude.
% The existing xtimefrequencymap.m will be retained for searches where we
% do not wish to perform this marginalization.
%
%  ** Procedure for marginalizing over sky positions. ** 
%  Identify likelihood we will use for detection statistic. 
%  Loop over sky positions:
%  - Calc map of [likelihood map multiply by prob(skyPos)]. prob(skyPos)
%    should be stored in the skyPositions array?
%  Create following maps:
%  1. Map of sum of probs from all skyPos.
%  2. Map of largest probs for each pixel.
%  3. Map listing skyPos which gave largest probs for each pixel 
%    (i.e., content of previous map).  
%  Use clustering routine to find loudest clusters in summed map (map 1).
%  Loop over clusters:
%  - Identify skyPos which contributed the largest prob to current
%    cluster using maps 2 and 3.
%  - Using these skyPos calculate the remaining likelihood maps (i.e.,
%    those used for consistency checks rather than as detection statistics).
%
%  ** Procedure for marginalizing over GW signal amplitudes **
%  When calculating detection statistic:
%    Calc bayesian* likelihoods for many amplitude.
%    Multiple by precomputed prior on amplitude.
%    Create summed map.
%
% ** Plan of action **
% - We will need to do two passes of the massive likelihoodType case 
%   statement, split this into helper function xlikelihoodcalc.m and then
%   check no functionality broken.
%
%
% ------------- END OF NOTES FOR XBAYESIANTIMEFREQUENCYMAP ----------------
%
% XTIMEFREQUENCYMAP Construct time-frequency map of the likelihood ratio
% for GWB signal hypothesis vs. noise hypothesis, maximized over signal
% parameters.
% 
% XTIMEFREQUENCYMAP computes a select set of likelihood ratios or
% statistics at each time-frequency pixel for a requested set of sky
% positions.  For example, the standard likelihood is, roughly speaking,
% the natural logarithm of the ratio of the probability of the observed
% data being due to a GWB plus stationary Gaussian background noise to the
% probability of the data being due to stationary Gaussian background noise
% only.  When testing multiple sky positions, the likelihood at each
% time-frequency pixel in the returned map is the likelihood measured for
% that pixel for the sky position (and any other parameters) that gave the
% largest total likelihood for that time, summed over all frequency bins
% (or smallest for null energy). 
%
% Optionally, xtimefrequencymap will return event clusters identified in
% the time-frequency maps instead of the maps directly.
%
% Usage:
% 
% [likelihoodMap, skyPositionIndex, detectorSpectrogram] = ...
%     xtimefrequencymap(channelNames, conditionedData, sampleFrequency, ...
%         amplitudeSpectra, skyPositions, integrationLength, offsetLength, ...
%         transientLength, frequencyBand, windowType, likelihoodType, ...
%         outputType, verboseFlag)
%
%    channelNames         Cell array of channel name strings.
%    conditionedData      Matrix of time-domain conditioned data, with one
%                         column per channel (detector).
%    sampleFrequency      Scalar.  Sampling frequency of conditioned data
%                         [Hz].
%    amplitudeSpectra     Matrix of amplitude spectral densities of the
%                         detector noise [1/sqrt(Hz)], with one column per
%                         channel.  Must be sampled at frequencies 
%                         [0:1/integrationLength:1/2]*sampleFrequency.
%    skyPositions         Matrix of sky positions [radians] to be tested.
%                         First column is polar angle and second column is
%                         azimuthal angle, both in radians in Earth-based
%                         coordinates.
%    integrationLength    Scalar.  Number of samples of data to use for each
%                         Fourier transform.  Must be a power of 2.
%    offsetLength         Scalar.  Number of samples between start of 
%                         consecutive FFTs.  Must be a power of 2.
%    transientLength      Scalar.  Duration of conditioning transients, in
%                         samples.  This is the number of samples at the
%                         beginning and end (rows of conditionedData) to
%                         ignore when computing likelihood maps.
%    frequencyBand        Two-component vector.  Elements are minimum and 
%                         maximum frequencies (Hz) to include in the
%                         time-frequency maps.
%    windowType           Optional string.  Window type to be used in FFTs;
%                         one of 'none', 'bartlett', 'hann', or
%                         'modifiedhann'.  Default 'modifiedhann'.
%    likelihoodType       String or cell array specifying types of 
%                         likelihoods to compute.  Recognized values are
%                         'crossenergy', 'crossinc', 'elliptic',
%                         'hardconstraint', 'H1H2nullenergy', 'H1H2nullinc', 
%                         'incoherentenergy', 'nullenergy', 'nullinc', 
%                         'plusenergy', 'plusinc', 'softconstraint', 'standard',
%                         'totalenergy', 'unbiasedsoft', 'bayesian1e-23', 
%                         'bayesian1e-22', 'bayesian1e-21', 'glitch', 'bg'.
%                         Additional types currently under development are
%                         'aglitchhardconstraint', 'dev',
%                         'glitchhardconstraint', 'glitchstandard', 
%                         'maxglitchhardconstraint', 'maxglitchstandard'.    
%    outputType           If non-zero, then likelihoodMap output variable 
%                         will contain event clusters rather than
%                         likelihood maps.  See below.
%    verboseFlag          Boolean flag to control output of status messages
%                         (default 0).
%
%    likelihoodMap        Array.  Time-frequency maps each likelihood type, 
%                         maximized over the sky (except for the null 
%                         energy, which is minimised).
%                         size(likelihoodMap,3)=length(likelihoodType), or
%                         array of clusters if outputType is non-zero.
%    skyPositionIndex     Array.  Index in skyPositions array for which 
%                         the likelihood at the corresponding time in 
%                         likelihoodMap is computed.  Not meaningful if
%                         'outputType' is non-zero.
%    detectorSpectrogram  Array.  Time-frequency maps of the energy in the  
%                         individual detectors, with no time shifts.
%                         size(detectorSpectrogram,3)=length(channelNames).
%
% The conditioned data should be provided as a matrix of time-domain data
% with each channel in a separate column.
%
% The amplitude spectral densities should be provided as a matrix of
% one-sided frequency domain data at a frequency resolution corresponding
% to the desired integration length and with each channel in a separate
% column.
%
% The desired sky positions should be provided as a two column matrix of
% the form [theta phi], where theta is the geocentric colatitude running
% from 0 at the North pole to pi at the South pole and phi is the
% geocentric longitude in Earth fixed coordinates with 0 on the prime
% meridian.
%
% Options for reporting clusters instead of time-frequency maps:
%
%   outputType == 1 
%     Output is largest single cluster for each sky direction tested.  Then
%     size(likelihoodMap) = number of sky positions x (7+number of
%     likelihood types).  Columns are:
%       1) start time of cluster relative to start of data (sec)
%       2) weighted central time of cluster relative to start of data (sec)
%       3) end time of cluster relative to start of data (sec)
%       4) minimum frequency of cluster (Hz)
%       5) weighted peak frequency of cluster (Hz)
%       6) maximum frequency of cluster (Hz)
%       7) number of pixels in the cluster
%       8+) likelihood values for the cluster, in the order of
%           likelihoodType.
%     See clusterTFmapNew for more details.
%
%   outputType == 2, 3, or 4 
%     Output is ???
%
% In the present implementation, the detector site of the first channel is
% used as the reference position when performing time shifts.  Therefore,
% when comparing likelihood maps to features in the data from individual
% detectors, the appropriate time shift is the time delay between the first
% detector and the detector being considered.
%
% See also XDETECTION, XMAPSKY, XREADDATA, XINJECTSIGNAL, XCONDITION,
% XTILESKY, and XINTERPRET, ComputeAntennaResponse, LoadDetectorData.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Stephen Poprocki    poprocki@caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Michal Was          michal.was@ens.fr

% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      process command line arguments                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for sufficient command line arguments
error(nargchk(11, 13, nargin));

% ---- Default arguments
if nargin < 13
    verboseFlag = 0;
end
if nargin < 12,
    outputType = 'timefrequencymap';  %-- output time-frequency maps
end
if isempty(windowType)
    windowType = 'modifiedhann';
end

% ---- Set column indexes of clusterArray.  These control the ordering of
%      cluster properties when outputType > 0. 
minTcol=1; meanTcol=2; maxTcol=3;
minFcol=4; meanFcol=5; maxFcol=6;
nPixelsCol=7;
likelihoodColOffset = 7;

% ---- Convert likelihoodType to cell array if necessary
if ischar(likelihoodType)
    temp_likelihoodType = likelihoodType;
    clear likelihoodType
    likelihoodType{1} = temp_likelihoodType;
end

% ---- Check that we will not use skypositiontheta or skypositionphi
%      as detection statistic.
if strcmp(likelihoodType{1},'skypositiontheta') || ...
   strcmp(likelihoodType{1},'skypositionphi')
    disp(['The first likelihood listed in the params file is used as the ' ...
        'detection statistic.']);
    error(['It does not make sense to use skypositiontheta or skypositionphi '...
        'as the detection statistic. '])
end

% ---- We will store the sky position corresponding to each trigger alongside
%      the likelihood values in our loudestCluster array. 
if ~max(strcmp(likelihoodType,'skypositiontheta'))
    likelihoodType = [likelihoodType; 'skypositiontheta'];
end
if ~max(strcmp(likelihoodType,'skypositionphi'))
    likelihoodType = [likelihoodType; 'skypositionphi'];
end

% ---- Number of likelihood types to test
numberOfLikelihoods = length(likelihoodType);
% ---- Check that each likelihoodType is a recognized type
for jLikelihoodType = 1:numberOfLikelihoods
    if (~( strcmp(likelihoodType{jLikelihoodType},'aglitchhardconstraint') ...
        || strcmp(likelihoodType{jLikelihoodType},'bayesian1e-21') ...
        || strcmp(likelihoodType{jLikelihoodType},'bayesian3e-22') ...
        || strcmp(likelihoodType{jLikelihoodType},'bayesian1e-22') ...
        || strcmp(likelihoodType{jLikelihoodType},'bayesian3e-23') ...
        || strcmp(likelihoodType{jLikelihoodType},'bayesian1e-23') ...
        || strcmp(likelihoodType{jLikelihoodType},'loghbayesian') ...
        || strcmp(likelihoodType{jLikelihoodType},'bg') ...
        || strcmp(likelihoodType{jLikelihoodType},'circenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'circinc') ...
        || strcmp(likelihoodType{jLikelihoodType},'crossenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'crossinc') ...
        || strcmp(likelihoodType{jLikelihoodType},'dev') ...
        || strcmp(likelihoodType{jLikelihoodType},'energyitf1') ...
        || strcmp(likelihoodType{jLikelihoodType},'energyitf2') ...
        || strcmp(likelihoodType{jLikelihoodType},'energyitf3') ...
        || strcmp(likelihoodType{jLikelihoodType},'elliptic') ...
        || strcmp(likelihoodType{jLikelihoodType},'hardconstraint') ...
        || strcmp(likelihoodType{jLikelihoodType},'geometric') ...
        || strcmp(likelihoodType{jLikelihoodType},'glitch') ...
        || strcmp(likelihoodType{jLikelihoodType},'glitchhardconstraint') ...
        || strcmp(likelihoodType{jLikelihoodType},'glitchstandard') ...
        || strcmp(likelihoodType{jLikelihoodType},'H1H2nullenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'H1H2nullinc') ...
        || strcmp(likelihoodType{jLikelihoodType},'incoherentenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'maxglitchhardconstraint') ...
        || strcmp(likelihoodType{jLikelihoodType},'maxglitchstandard') ...
        || strcmp(likelihoodType{jLikelihoodType},'nullenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'nullinc') ...
        || strcmp(likelihoodType{jLikelihoodType},'plusenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'plusinc') ...
        || strcmp(likelihoodType{jLikelihoodType},'powerlaw') ...
        || strcmp(likelihoodType{jLikelihoodType},'softconstraint') ...
        || strcmp(likelihoodType{jLikelihoodType},'standard') ...
        || strcmp(likelihoodType{jLikelihoodType},'totalenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'unbiasedsoft') ...
        || strcmp(likelihoodType{jLikelihoodType},'skypositiontheta') ...
        || strcmp(likelihoodType{jLikelihoodType},'skypositionphi') ...
        ))
        error(['likelihoodType ' likelihoodType{jLikelihoodType} ...
            ' is not a recognized type']);
    end
end

% ---- Force one dimensional cell array for channel list
channelNames = channelNames(:);

% ---- Number of detectors
numberOfChannels = length(channelNames);

% ---- Verify that number of data streams matches number of detectors
if size(conditionedData, 2) ~= numberOfChannels,
  error('conditioned data is inconsistent with number of detectors');
end

% ---- Block length in samples
blockLength = size(conditionedData, 1);

% ---- Verify that integration length is an integer power of two
if bitand(integrationLength, integrationLength - 1),
  error('integration length is not an integer power of two');
end

% ---- We expect skyPosition to have either 2 or 4 columns
%      containing:
%      theta   Polar angle in the range [0, pi] with 0 at the 
%              North Pole/ z axis.
%      phi     Azimuthal angle in the range [-pi, pi) with 0 at 
%              Greenwich / x axis.
%      pOmega  A priori probability associated with each sky 
%              position. [Optional] 
%      dOmega  Solid angle associated with each sky position.
%              [Optional]
%      (From sinusoidalMap.)

% ---- Verify that skyPositions array has at least two columns
if size(skyPositions, 2) < 2,
    error('sky positions must be a at least a two column matrix');
elseif size(skyPositions, 2) == 2
    warning('Setting pOmega to unity')
    pOmega = ones(size(skyPositions(:, 1)));
elseif size(skyPositions,2) == 4
    pOmega = skyPositions(:, 3); 
else
    error('Unknown format of skyPositions, expect either 2 or 4 cols.')
end

% ---- Verify that skyPositions polar coordinates are in range [0,pi].
if any((skyPositions(:, 1) < 0) | (skyPositions(:, 1) > pi)),
  error('theta outside of [0, pi]');
end
% ---- Verify that skyPositions azimutal coordinates are in range [-pi,pi)
if any((skyPositions(:, 2) < -pi) | (skyPositions(:, 2) >= pi)),
  error('phi outside of [-pi, pi)');
end

% ---- Number of sky positions
numberOfSkyPositions = size(skyPositions, 1);

% ---- Nyquist frequency
nyquistFrequency = sampleFrequency / 2;

% ---- Vector of one-sided frequencies
oneSidedFrequencies = nyquistFrequency * ( 0 : integrationLength / 2 ) / ...
                     ( integrationLength / 2 );

% ---- Frequencies in desired analysis band
frequencyIndex = find( (oneSidedFrequencies>=frequencyBand(1)) & ...
    (oneSidedFrequencies<=frequencyBand(2)) );
inbandFrequencies = oneSidedFrequencies(frequencyIndex);

% ---- Number of in-band frequency bins
numberOfFrequencyBins = length(inbandFrequencies);

% ---- Truncate amplitude spectra to desired frequency band
amplitudeSpectra = amplitudeSpectra(frequencyIndex,:);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               partition block into overlapping segments                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Calculate the indices of the endpoints of the segments of length
%      integrationLength with no overlapping.  (Overlapping will be imposed 
%      later when FFTing.)
% ---- Analyse all data except for start and end transient intervals.
segmentIndices = chunkIndices(blockLength, integrationLength, ...
    transientLength, integrationLength);

% ---- Number of segments
numberOfSegments = size(segmentIndices, 1);

% ---- Fractional offset of consecutive segments
offsetFraction = offsetLength/integrationLength;

% ---- Number of time bins in time-frequency maps.
%      NOTE THAT WE DO NOT ANALYSE THE LAST ELEMENT; this is convenient for
%      handling overlapping.  The 'max' test covers the case in which there 
%      is only one segment.
numberOfTimeBins = max(numberOfSegments-1,1)/offsetFraction;

% ---- Record info on physical dimensions of time-frequency maps for later
%      use.
% ---- The time stamp of this data point is the central time of the first
%      time bin to be analysed.
firstTimeBinCenterIndex = segmentIndices(1, 1) + integrationLength/2;
% ---- Convert to time relative to startTime of data (which is not known to
%      xtimefrequencymap).
firstTimeBinCenterTime = (firstTimeBinCenterIndex - 1)/ sampleFrequency;
% ---- Time-frequency map dimension info as used by clusterTFmapNew:
%        [central time of first bin, ...
%         spacing between consecutive time bins, ...
%         central frequency of first bin, ...
%         spacing between consecutive frequency bins ]
mapDim = [ firstTimeBinCenterTime, ...
    offsetLength / sampleFrequency, ... 
    inbandFrequencies(1), ...
    sampleFrequency / integrationLength ];
clear firstTimeBinCenterIndex firstTimeBinCenterTime

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       preload detector information                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Loop over detectors.
for iDet = 1:numberOfChannels
    % ---- The detector is matched by the first character of the channel name.
    detData = LoadDetectorData(channelNames{iDet}(1));
    % ---- Extract the position vector
    rDet(iDet,:) = detData.V';
    % ---- Extract the detector response tensors
    dDet{iDet}   = detData.d;
end

% ---- Check for aligned network (i.e., check to see if network consists
%      solely of H1 and H2).
alignedNetwork = 0;
if (numberOfChannels==2) 
    if (strcmp(channelNames{1}(1:2),'H1') && strcmp(channelNames{2}(1:2),'H2')) || ...
       (strcmp(channelNames{1}(1:2),'H2') && strcmp(channelNames{2}(1:2),'H1'))
        alignedNetwork = 1;
    end
end

% ---- Check to see if network contains H1 and H2 (possibly in addition to other 
%      detectors). 
indexH1 = strmatch('H1',channelNames);
indexH2 = strmatch('H2',channelNames);
if (~isempty(indexH1) & ~isempty(indexH2))
    H1H2network = 1;
else
    H1H2network = 0;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             precompute detector responses, time delays                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Compute antenna responses and time delays for each detector and sky
%      position.  Arrays are size numberOfSkyPositions x numberOfChannels.
[Fp, Fc] = antennaPatterns(dDet, skyPositions);
timeShifts = computeTimeShifts(rDet, skyPositions);

% ---- RESET REFERENCE POSITION TO FIRST DETECTOR
timeShifts = timeShifts - repmat(timeShifts(:,1),[1 size(timeShifts,2)]);
timeShiftLengths = timeShifts * sampleFrequency;
integerTimeShiftLengths = round(timeShiftLengths);
% residualTimeShiftLengths = timeShiftLengths - integerTimeShiftLengths;
residualTimeShifts = (timeShiftLengths - integerTimeShiftLengths)/sampleFrequency;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       assign storage                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Storage for likelihood time-frequency maps and other output.
likelihoodTimeFrequencyMap = zeros(numberOfFrequencyBins, numberOfTimeBins, numberOfLikelihoods);
skyPositionIndex = zeros(1, numberOfTimeBins, numberOfLikelihoods);

switch outputType
    case 'allclusters'
        % ---- Array holding all clusters for each sky position (!).
        loudestCluster = {};        
end

% ---- Assign storage for time-frequency maps of single-detector data.
timeFrequencyMapFull = cell(numberOfChannels,1); %-- contains all frequencies
timeFrequencyMap = cell(numberOfChannels,1); %-- contains only non-negative frequencies
detectorSpectrogram = zeros(numberOfFrequencyBins,numberOfTimeBins,numberOfChannels);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             initialize time-frequency maps at zero delay                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- FFT the entire data stream to be analysed for each detector.
%      Note that we drop the last segment to make overlapping easier.  (If
%      there is only one segment it is not dropped!)
%      Start and stop indices for the FFT are the start and stop indices of
%      the first and last segments to be analysed:
segmentStartIndex = segmentIndices(1, 1);
segmentStopIndex = segmentIndices(max(numberOfSegments-1,1), 2);

% ---- Construct window.
switch windowType
    case 'none'
        windowData = ones(integrationLength, 1);
    case 'bartlett'
        windowData = bartlett(integrationLength);
    case 'hann'
        windowData = hann(integrationLength);
    case 'modifiedhann'
        windowData = modifiedhann(integrationLength);
    otherwise
        disp('Unknown windowType. Using none.');
        windowData = ones(integrationLength, 1);
end
% ---- Rescale window to have unity mean square.
windowMeanSquare = mean(windowData.^2);
windowData = windowData / windowMeanSquare^0.5;
% ---- Make window data into array matching size of data to be FFTed.
windowData = repmat(windowData,[1,max(numberOfSegments-1,1)]);

% ---- Loop over detectors and FFT the data from each.  Use overlapping as
%      requested.
for channelNumber = 1 : numberOfChannels

    if (offsetFraction==1)
        % ---- FFT with no overlapping - this code is faster than the more 
        %      general version below.
        % ---- Extract data for this detector.
        data = conditionedData(segmentStartIndex:segmentStopIndex,channelNumber);
        % ---- Reshape into array of size integrationLength x numberOfTimeBins, 
        %      so that each segment to be FFTed occupies one column.
        dataArray = reshape(data,integrationLength, numberOfTimeBins);
        % ---- Apply window and FFT.
        timeFrequencyMapFull{channelNumber} = fft( windowData .* dataArray );
    else
        % ---- Reshape the data into rectangular array, where consecutive
        %      columns contain data according to the requested overlap. 
        for j=1:1/offsetFraction
            % ---- Find start and stop indices.
            offsetStartIndex = segmentStartIndex + integrationLength*(j-1)*offsetFraction;
            offsetStopIndex = segmentStopIndex + integrationLength*(j-1)*offsetFraction;
            % ---- Extract data for this detector.
            data = conditionedData(offsetStartIndex:offsetStopIndex,channelNumber);
            % ---- Reshape into array of size integrationLength x numberOfTimeBins, 
            %      so that each segment to be FFTed occupies one column.
            dataArray = reshape(data,integrationLength,[]);
            % ---- Time bins in full TF map that these segments correspond to.
            timeBins = [j:1/offsetFraction:numberOfTimeBins];
            % ---- Copy these FFTs into TF map for this channel.
            timeFrequencyMapFull{channelNumber}(:,timeBins) = ...
                fft( windowData .* dataArray );
        end
    end

    % ---- Extract in-band frequencies.
    timeFrequencyMap{channelNumber} = ...
        timeFrequencyMapFull{channelNumber}(frequencyIndex,:);

    % ---- Save the squared magnitudes as detectorSpectrograms.
    detectorSpectrogram(:,:,channelNumber) = ...
        real(timeFrequencyMap{channelNumber}).^2 + ...
        imag(timeFrequencyMap{channelNumber}).^2;

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   begin loop over sky positions                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Note: When comparing different sky positions, we keep all pixels in
%      any time bin for which the likelihood summed over that time bin is 
%      larger than the current largest summed likelihood recorded for that 
%      time bin for any other sky position.
% ---- Prepare storage for largest summed-over-frequency likelihoods.
maxSummedLikelihood = ones(1,numberOfTimeBins,numberOfLikelihoods)*-inf;

% ---- Initialise arrays which get modified as we loop over sky positions.
likelihoodMapSumOverSkyLogProb = -Inf * ones(numberOfFrequencyBins,numberOfTimeBins);
likelihoodMapMaxOverSky        = zeros(numberOfFrequencyBins,numberOfTimeBins);
skyPositionMapMaxOverSky       = zeros(numberOfFrequencyBins,numberOfTimeBins);


for skyPositionNumber = 1 : numberOfSkyPositions,

    % ---- Status report.
    if (verboseFlag && (mod(skyPositionNumber, numberOfSkyPositions / 100) < 1))
        fprintf(1, 'processing sky position %d of %d (%d%% complete)...\n', ...
            skyPositionNumber, numberOfSkyPositions, ...
            round(100 * skyPositionNumber / numberOfSkyPositions));            
    end

    [wFp, wFc, wFpDP, wFcDP, wFpTimeFrequencyMap, wFcTimeFrequencyMap, ...
        Mpp, Mcc, timeFrequencyMap] = xcalctimefreqmaps(...
        numberOfTimeBins,numberOfFrequencyBins,numberOfChannels, numberOfSegments, ...
        Fp, Fc, skyPositionNumber, conditionedData, windowData, amplitudeSpectra, ...
        integrationLength, offsetFraction, frequencyIndex, inbandFrequencies, ...
        residualTimeShifts, segmentIndices, integerTimeShiftLengths, timeFrequencyMap);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                compute likelihood maps                      %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- compute 50 and 99 percentile single detector energies only
    % once, they do not depend on the sky position
    if (skyPositionNumber == 1)
      for iChannel = 1:numberOfChannels
        timeFrequencyMapP99{iChannel} = prctile(abs(timeFrequencyMap{iChannel}),99,2); 
        timeFrequencyMapP50{iChannel} = prctile(abs(timeFrequencyMap{iChannel}),50,2); 
      end
    end


    % ---- Only calc full map for the detection statistic.
    for jLikelihoodType = 1:numberOfLikelihoods

    [likelihood,likelihood1,likelihood2] = xlikelihoodcalc( ...
        amplitudeSpectra, likelihoodType{jLikelihoodType}, ...
        timeFrequencyMap, timeFrequencyMapP50, timeFrequencyMapP99,...
        wFpTimeFrequencyMap, wFcTimeFrequencyMap, ...
        numberOfChannels, numberOfTimeBins, numberOfFrequencyBins, ...
        Mpp, Mcc, ...
        wFp, wFc, wFpDP, wFcDP, ...
        alignedNetwork, H1H2network, ...
        indexH1,indexH2);

    switch outputType
      
     case {'allclusters'}
      
        % ---- Process time-frequency map to find clusters.
        if (jLikelihoodType == 1)
            % ---- Find clusters of map pixels with large likelihood in the
            %      first likelihood map (the first likelihood type is the 
            %      "detection statistic").  
            % ---- Compute likelihood threshold to give black-pixel probability
            %      of 1% for the detection statistic likelihood.
            % ---- Calculating Prob = P(x) * P(Omega), where 
            %      likelihood = log(P(x)).
            %      log(Prob) = log(P(x) * P(Omega)) 
            %                = log(P(x))  + log(P(Omega))
            %                = likelihood + log(P(Omega))
            % ---- If pOmega = 1, logProb = likelihood.
            likelihood = likelihood + log(pOmega(skyPositionNumber));
            % clustering needs positive weigths to compute correctly peak
            % time and peak frequency
            blackThresh = max(prctile(likelihood(:),99),0);
            % ---- For cluster properties in physical units:
            % ---- Call clustering algorithm.  Ordinary connected-4 clustering
            %      (no generalized clustering) using 'fast' algorithm.  Output
            %      cluster properties in physical units (sec, Hz).
            [clusterArray, clusterStruct, labelledMap] = clusterTFmapNew( ...
                likelihood,1,'fastconnected',blackThresh,8,0,0,0,0,mapDim);
            nClusters = size(clusterArray,1);
            % ---- Desired output: all clusters for this sky position.
            loudestCluster{skyPositionNumber} = [clusterArray zeros(size(clusterArray,1),numberOfLikelihoods-1)];
            % ---- What is this next line doing??? GJ
            %likelihoodTimeFrequencyMap(:,:,jLikelihoodType) + ...
            %(likelihood(:,:)).^2 * sin(skyPositions(skyPositionNumber,1));
        else
          % ---- Compute "auxiliary" likelihood for pre-defined
          %      clusters in labelledMap.
          if( strcmp('powerlaw',likelihoodType{jLikelihoodType}))
            newLabelledMap = bwlabel(likelihood > ...
                                     prctile(likelihood(:), 99), 8);
            statSumMap = statisticSumLabelledMap(newLabelledMap, ...
                                                 likelihood);
            clusterArray = fastclustermaxprop(labelledMap, ...
                                              statSumMap);
          elseif strcmp('circenergy',likelihoodType{jLikelihoodType})
            clusterArray1  = ...
                fastclusterprop(labelledMap, likelihood1);
            clusterArray2  = ...
                fastclusterprop(labelledMap, likelihood2);
            clear likelihood1 likelihood2
            clusterArray = max(clusterArray1,clusterArray2);
            clear clusterArray1 clusterArray2
          elseif strcmp('skypositiontheta',likelihoodType{jLikelihoodType}) 
            clusterArray = repmat(skyPositions(skyPositionNumber,1),nClusters,1);
          elseif strcmp('skypositionphi',likelihoodType{jLikelihoodType}) 
            clusterArray = repmat(skyPositions(skyPositionNumber,2),nClusters,1);
          else
            clusterArray = fastclusterprop(labelledMap, likelihood);
          end
          % ---- Record only likelihood value from clusterArray.
            loudestCluster{skyPositionNumber}(end-size(clusterArray,1)+1:end,likelihoodColOffset+jLikelihoodType) = ...
                clusterArray(:,end);
        end
        
    end

    end % -- loop over likelihoods

    
end  % -- end loop over sky positions





if verboseFlag
  disp('Decimate clusters ...')
end

for skyPositionNumber = 1 : numberOfSkyPositions,
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %                    cluster decimation
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % ---- TODO: Split this off as a separate function
            %      xdecimatecluster.m.  This function should take as input
            %      clusters in the struct format.  

            % ---- MAGIC NUMBERS.  These should be read from the parameter
            %      file. 
            % ---- Fraction of clusters to keep.
            clusterProportion = 0.1;
            
            % ---- Cluster decimation.  Keep only loudest
            %      'clusterProportion' fraction of event clusters (for
            %      manageable I/O).
            % ---- Loop over analysis time scales.
                % ---- Compute significance of detection statistic.
                detectionSignificance = likelihoodsignificance( ...
                    loudestCluster{skyPositionNumber}(:,likelihoodColOffset+1), ...
                    likelihoodType{1}, ...
                    loudestCluster{skyPositionNumber}(:,nPixelsCol), ...
                    length(channelNames));
                % ---- Compute and apply threshold on significance to keep
                %      only clusterProportion fraction of clusters. 
                significanceThreshold = prctile(detectionSignificance, ...
                    (1-clusterProportion)*100);
                loudEventMask = (detectionSignificance >= ...
                                 significanceThreshold);
                if size(loudestCluster{skyPositionNumber},1) ~= size(loudestCluster{skyPositionNumber},1)
                  error(['Oups, the assumption that loudestCluster{skyPositionNumber} and ' ...
                         'loudestCluster{skyPositionNumber} have the same number of clusters is wrong. The code ' ...
                         'below will not work properly. loudestCluster{skyPositionNumber}: ' ...
                        num2str(size(loudestCluster{skyPositionNumber},1)) ' loudestCluster{skyPositionNumber}: ' ...
                        num2str(size(loudestCluster{skyPositionNumber},1))])
                end
                loudestCluster{skyPositionNumber} = loudestCluster{skyPositionNumber}(loudEventMask,:);
            clear detectionSignificance significanceThreshold 
end  % -- end loop over sky positions

% --- remove empty cluster array from loudestCluster, empty lists break
% super clustering
offsetSky = 0;
for skyPositionNumber = 1:numberOfSkyPositions
  if not(isempty(loudestCluster{skyPositionNumber}))
    offsetSky = offsetSky + 1;
    loudestClusterClean{offsetSky} = loudestCluster{skyPositionNumber};
  end
end
loudestCluster = loudestClusterClean;
clear loudestClusterClean;

if verboseFlag
  disp('Super cluster across sky positions')
end
loudestCluster = xsupercluster(loudestCluster,likelihoodType);


numberOfClusters = size(loudestCluster,1);
if verboseFlag
  disp(['numberOfClusters       ' num2str(numberOfClusters)]);
end




% ---- Replace time-frequency likelihood maps with alternate output if
%      alternate output is requested.
switch outputType
    case {'allclusters'}
        % ---- Output loudest cluster at each sky position.
        likelihoodTimeFrequencyMap = loudestCluster;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          return to calling function                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Return to calling function
return;
