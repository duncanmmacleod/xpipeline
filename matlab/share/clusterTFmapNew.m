function [clusterArray, clusterStruct, labelledMap] = clusterTFmapNew( ...
    likelihoodMap, likelihoodIndex, clusterType, blackThresh, ... 
    connectivityNumber, sizeThresh, generalizedClustering, mergingThresh, ...
    maximalDistance, mapDim)
% clusterTFmapNew - Find clusters of large pixel values in time-frequency maps.
% 
%  [clusterArray, clusterStruct, labelledMap] = clusterTFmapNew( ...
%   likelihoodMap, mapIndex, clusterType, blackThresh, connectivity, ...
%   sizeThresh, genClustering, mergingThresh, maximalDistance, ... 
%   mapDim)
%
%   likelihoodMap   Three-dimensional array of time-frequency likelihood  
%                   values. Dimension 1 = frequency, dimension 2 = time, 
%                   dimension 3 = likelihood type.
%   mapIndex        Scalar.  Index of the likelihood type to be used for 
%                   identifying clusters (i.e., which time-frequency slice 
%                   of likelihoodMap in which to look for clusters).
%   clusterType     String. Clustering method. See below.
%   blackThresh     Scalar. Threshold on likelihoods for pixels that are 
%                   "black" in the 'connected' method.
%   connectivity    Scalar.  Either 4 or 8 (default 4). See bwlabel.
%   sizeThresh      IGNORED. Scalar.  Minimum size in pixels for any cluster.
%                   Clusters smaller than this size are discarded.  
%   genClustering   0 or 1. If set to 1 and clusterType is connected,
%                   then clusters which are sufficiently close together are
%                   merged ("generalized clustering").
%   mergingThresh   Threshold on cluster energy for it to be merged in
%                   generalized clustering. 
%   maximalDistance Maximum pixel distance between merged clusters in
%                   generalized clustering. 
%   mapDim          Optional.  4-element vector [T0, dT, F0, dF] where: 
%                   T0 is the central time (sec) of the first column of map, 
%                   dT is the time (sec) between consecutive columns of map, 
%                   F0 is the central frequency (Hz) of the first row of map, 
%                   dF is the frequency interval (Hz) between rows of map. 
%
%   clusterArray    mx8+ matrix (m is # of clusters)
%                   column 1: minimum time of cluster
%                   column 2: weighted center time of cluster
%                   column 3: maximum time of cluster
%                   column 4: minimum frequency of cluster
%                   column 5: weighted center frequency of cluster
%                   column 6: maximum frequency of cluster
%                   column 7: number of pixels in cluster
%                   column 8-?: sum-over-cluster map values for each likelihood 
%                   All times and frequencies are in units of bins, unless 
%                   mapDim specified.  For example, a single-pixel cluster
%                   at (time,freq) bin = (100,20) would have clusterArray
%                   values
%                     [ 99.5 100 100.5 19.5 20 20.5 1 ... likelihoods ... ]
%                   If mapDim is specified, the output is in seconds and 
%                   Hz.
%   clusterStruct   Struct array containing regionprops properties for
%                   clusters.  See regionprops command.  Non-empty only for
%                   'connected' clustering type.
%   labelledMap     Array of the same size as likelihoodMap(:,:,1) in which
%                   each pixel contains the integer label of the cluster it
%                   belongs to or zero if it is not in any cluster.
%                   Non-empty only for 'connected', 'fastconnected'
%                   clustering types.  
%
% Usage details:
%
% The clusters are identified as groups of large pixel values in 
% likelihoodMap(:,:,mapIndex).  Once the pixels for each cluster are
% identified, the likelihood from each of the other maps
% likelihoodMap(:,:,jj~=mapIndex) are summed over the same pixels.
%
% The first three input arguments are REQUIRED (likelihoodMap,
% likelihoodIndex, clusterType).  The others are optional (blackThresh,
% connectivity, sizeThresh, genClustering, mergingThresh, maximalDistance,
% mapDim), and may be omitted or specified as '[]' for default values.
%
% clusterTFmap(map,index,'maxOverFrequency') outputs one event cluster per time
% bin in the input map.  Each cluster is constructed from the single pixel
% with the largest map value in that time bin.
%
% clusterTFmap(map,index,'sumOverFrequency') outputs one event cluster per time
% bin in the input map.  Each cluster is constructed from the sum of all
% pixels in that time bin.
%
% clusterTFmap(map,index,'connected',blackThresh,connectivity, ... 
%      sizeThresh, genClustering, mergingThresh, maximalDistance)
% First, the map is converted to a binary map with (map > blackThresh).
% Then, black pixels are clustered together using bwlabelfunction with the
% specified connectivityNumber.  Generalized clustering is performed with
% the specified thresholds on cluster size and distance.  Finaly, cluster
% proprieties are computed using regionprops.  (Note: sizeThresh is
% currently ignored.) 
%
% clusterTFmap(map,index,'fastconnected',...) is the same as 'connected' up
% to rounding error for mean frequency and time, but uses a faster
% algorithm. 
%
% [1] Julien Sylvestre, Physical Review D 66, 102004 (2002)
%
% $Id$

% TODO : replace bwdist by optimized code

% ---- Check for valid number commmand arguments
error(nargchk(3, 10, nargin));

% ---- Set output parameter order.
minTcol=1; meanTcol=2; maxTcol=3;
minFcol=4; meanFcol=5; maxFcol=6;
areaCol=7;
likelihoodColOffset = 7;
nLikelihoods=size(likelihoodMap,3);
% ---- Likelihood to be used for finding clusters.
lCol=likelihoodColOffset+likelihoodIndex; 

% ---- Setting clustering map
if (length(size(likelihoodMap)) == 3)
    map = likelihoodMap(:,:,likelihoodIndex);
else
    map = likelihoodMap;
end

% ---- Choose a clustering method
switch(clusterType)
   
    
    case 'fastconnected'
    
        % ---- Set defaults.
        if (nargin < 9 || isempty(maximalDistance))
           maximalDistance = 3; 
        end
        if (nargin < 8 || isempty(mergingThresh))
           mergingThresh = 0; 
        end
        if (nargin < 7 || isempty(generalizedClustering))
            generalizedClustering = 0;
        end
        % ---- Set default size threshold.
        if (nargin < 6 || isempty(sizeThresh))
           sizeThresh = 0; 
        end
        % ---- Connectivity number.
        if (nargin < 5 || isempty(connectivityNumber)) 
            connectivityNumber = 4; % default as chosen in [1]
        end
        if (4 ~= connectivityNumber && 8 ~= connectivityNumber)
            error('connectivity number must be 4 or 8. See bwlabel.');
        end        
        % ---- Black pixel threshold (step (ii) of [1])
        if (nargin < 4 || isempty(blackThresh))
            blackThresh = prctile(map(:),99);
        end

        % ---- Apply black pixel threshold.
        binaryMap = map > blackThresh;

        % ---- Find clusters of 'black' pixels (step (iii) of [1])
        [labelledMap,nClusters] = bwlabel(binaryMap, connectivityNumber);
       
        clusterArray=fastclusterprop(labelledMap, likelihoodMap);

        % ---- Generalized clustering.
        if (generalizedClustering == 1)
            mergedClusterMask = zeros( nClusters, 1);
            % delta
            for iCluster=1:nClusters
                % ---- A cluster that has been merged no longer exists.
                if (0 == mergedClusterMask(iCluster) && clusterArray(iCluster,lCol)>=mergingThresh)
                    neighborhood = labelledMap( ...
                        round(max(clusterArray(iCluster,minFcol)-maximalDistance,1)+0.5): ...
                        round(min(clusterArray(iCluster,maxFcol)+maximalDistance,size(labelledMap,1))-0.5), ...
                        round(max(clusterArray(iCluster,minTcol)-maximalDistance,1)+0.5): ...
                        round(min(clusterArray(iCluster,maxTcol)+maximalDistance,size(labelledMap,2))-0.5) );
                    otherPixelList = find( (0 < neighborhood) .* (iCluster ~= neighborhood))';
                    if(~isempty(otherPixelList))
                        neighDistMap = bwdist((neighborhood == iCluster),'cityblock');
                    end
                    for iPixel=otherPixelList
                        % merge with clusters not yet merged into a 
                        % generalized cluster and being near the current cluster
                        if( (0 == mergedClusterMask(neighborhood(iPixel))) && (neighDistMap(iPixel) <= maximalDistance) ...
                                && clusterArray(neighborhood(iPixel),lCol) >= mergingThresh )
                            oldCluster = neighborhood(iPixel);
                            mergedClusterMask(oldCluster)=1;
                            labelledMap(labelledMap==oldCluster) = iCluster;

                            clusterArray(iCluster,minTcol) = min(clusterArray(iCluster,minTcol),clusterArray(oldCluster,minTcol));
                            clusterArray(iCluster,meanTcol) = clusterArray(iCluster,meanTcol)*clusterArray(iCluster,lCol) + ...
                                clusterArray(oldCluster,meanTcol)*clusterArray(oldCluster,lCol);
                            clusterArray(iCluster,maxTcol) = max(clusterArray(iCluster,maxTcol),clusterArray(oldCluster,maxTcol));
                            clusterArray(iCluster,minFcol) = min(clusterArray(iCluster,minFcol),clusterArray(oldCluster,minFcol));
                            clusterArray(iCluster,meanFcol) = clusterArray(iCluster,meanFcol)*clusterArray(iCluster,lCol) + ...
                                clusterArray(oldCluster,meanFcol)*clusterArray(oldCluster,lCol);
                            clusterArray(iCluster,maxFcol) = max(clusterArray(iCluster,maxFcol),clusterArray(oldCluster,maxFcol));
                            clusterArray(iCluster,areaCol) = clusterArray(iCluster,areaCol)+clusterArray(oldCluster,areaCol);
                            for iL=1:nLikelihoods
                                clusterArray(iCluster,likelihoodColOffset+iL) = ....
                                    clusterArray(iCluster,likelihoodColOffset+iL)+clusterArray(oldCluster,likelihoodColOffset+iL);
                            end
                            % --- normalize means
                            clusterArray(iCluster,meanTcol) = clusterArray(iCluster,meanTcol)/clusterArray(iCluster,lCol);
                            clusterArray(iCluster,meanFcol) = clusterArray(iCluster,meanFcol)/clusterArray(iCluster,lCol);
                            
                        end
                    end % --- loop on other black pixels

                end %  ---- is cluster existing 
            end % ---- loop on clusters
        
        clusterArray = clusterArray(~logical(mergedClusterMask),:);
        
        end % ---- generalized clustering
                
        clusterStruct = [];
    

    case 'connected'
      
        % ---- Set defaults.
        if (nargin < 9 || isempty(maximalDistance))
           maximalDistance = 3; 
        end
        if (nargin < 8 || isempty(mergingThresh))
           mergingThresh = 0; 
        end
        if (nargin < 7 || isempty(generalizedClustering))
            generalizedClustering = 0;
        end
        % ---- Set default size threshold.
        if (nargin < 6 || isempty(sizeThresh))
           sizeThresh = 0; 
        end
        % ---- Connectivity number.
        if (nargin < 5 || isempty(connectivityNumber)) 
            connectivityNumber = 4; % default as chosen in [1]
        end
        if (4 ~= connectivityNumber && 8 ~= connectivityNumber)
            error('connectivity number must be 4 or 8. See bwlabel.');
        end        
        % ---- Black pixel threshold (step (ii) of [1])
        if (nargin < 4 || isempty(blackThresh))
            blackThresh = prctile(map(:),99);
        end

        % ---- Apply black pixel threshold.
        binaryMap = map > blackThresh;
        
        % ---- Find clusters of 'black' pixels (step (iii) of [1])
        [labelledMap,nClusters] = bwlabel(binaryMap, connectivityNumber);
        
        % ---- Compute cluster proprieties
        %clusterArea = regionprops(labelledMap,'Area');
        %indexBigClusters = find(reshape([clusterArea.Area],1,[])>=sizeThresh);
        
        clusterStruct=regionprops(labelledMap,'Area','BoundingBox', ...
            'PixelIdxList','PixelList');
                            
        %clusterStruct = clusterStruct(indexBigClusters);
        %nClusters = length(indexBigClusters);
                            
        clusterArray=zeros(nClusters,likelihoodColOffset+nLikelihoods);
        clusterBoundingBox = reshape([clusterStruct.BoundingBox],4,[])';

        clusterArray(:,minTcol)= clusterBoundingBox(:,1);
        clusterArray(:,maxTcol)= clusterBoundingBox(:,1) + clusterBoundingBox(:,3); 
        clusterArray(:,minFcol)= clusterBoundingBox(:,2);
        clusterArray(:,maxFcol)= clusterBoundingBox(:,2) + clusterBoundingBox(:,4);
        

        clusterArray(:,areaCol) = reshape([clusterStruct.Area],1,[])';
                                    
        for iCluster=1:nClusters
            
            tPixelList=clusterStruct(iCluster).PixelList(:,1);
            fPixelList=clusterStruct(iCluster).PixelList(:,2);
            linPixelList=clusterStruct(iCluster).PixelIdxList;
            
            for iL=1:nLikelihoods
                clusterArray(iCluster,likelihoodColOffset+iL) = ...
                    sum(likelihoodMap(linPixelList+(iL-1)*size(likelihoodMap,1)*size(likelihoodMap,2)));
            end
            clusterArray(iCluster,meanTcol) = sum(map(linPixelList) .* tPixelList) / ...
               clusterArray(iCluster,lCol);
            clusterArray(iCluster,meanFcol) = sum(map(linPixelList) .* fPixelList) / ...
               clusterArray(iCluster,lCol);
            
        end
        
        % ---- Generalized clustering.
        if (generalizedClustering == 1)
            mergedClusterMask = zeros( nClusters, 1);
            % delta
            for iCluster=1:nClusters
                % ---- A cluster that has been merged no longer exists.
                if (0 == mergedClusterMask(iCluster) && clusterArray(iCluster,lCol)>=mergingThresh)
                    neighborhood = labelledMap( ...
                        round(max(clusterArray(iCluster,minFcol)-maximalDistance,1)+0.5): ...
                        round(min(clusterArray(iCluster,maxFcol)+maximalDistance,size(labelledMap,1))-0.5), ...
                        round(max(clusterArray(iCluster,minTcol)-maximalDistance,1)+0.5): ...
                        round(min(clusterArray(iCluster,maxTcol)+maximalDistance,size(labelledMap,2))-0.5) );
                    otherPixelList = find( (0 < neighborhood) .* (iCluster ~= neighborhood))';
                    if(~isempty(otherPixelList))
                        neighDistMap = bwdist((neighborhood == iCluster),'cityblock');
                    end
                    for iPixel=otherPixelList
                        % merge with clusters not yet merged into a 
                        % generalized cluster and being near the current cluster
                        if( (0 == mergedClusterMask(neighborhood(iPixel))) && (neighDistMap(iPixel) <= maximalDistance) ...
                                && clusterArray(neighborhood(iPixel),lCol) >= mergingThresh )
                            oldCluster = neighborhood(iPixel);
                            mergedClusterMask(oldCluster)=1;
                            labelledMap(clusterStruct(oldCluster).PixelIdxList) = iCluster;

                            clusterArray(iCluster,minTcol) = min(clusterArray(iCluster,minTcol),clusterArray(oldCluster,minTcol));
                            clusterArray(iCluster,meanTcol) = clusterArray(iCluster,meanTcol)*clusterArray(iCluster,lCol) + ...
                                clusterArray(oldCluster,meanTcol)*clusterArray(oldCluster,lCol);
                            clusterArray(iCluster,maxTcol) = max(clusterArray(iCluster,maxTcol),clusterArray(oldCluster,maxTcol));
                            clusterArray(iCluster,minFcol) = min(clusterArray(iCluster,minFcol),clusterArray(oldCluster,minFcol));
                            clusterArray(iCluster,meanFcol) = clusterArray(iCluster,meanFcol)*clusterArray(iCluster,lCol) + ...
                                clusterArray(oldCluster,meanFcol)*clusterArray(oldCluster,lCol);
                            clusterArray(iCluster,maxFcol) = max(clusterArray(iCluster,maxFcol),clusterArray(oldCluster,maxFcol));
                            clusterArray(iCluster,areaCol) = clusterArray(iCluster,areaCol)+clusterArray(oldCluster,areaCol);
                            for iL=1:nLikelihoods
                                clusterArray(iCluster,likelihoodColOffset+iL) = ....
                                    clusterArray(iCluster,likelihoodColOffset+iL)+clusterArray(oldCluster,likelihoodColOffset+iL);
                            end
                            % --- normalize means
                            clusterArray(iCluster,meanTcol) = clusterArray(iCluster,meanTcol)/clusterArray(iCluster,lCol);
                            clusterArray(iCluster,meanFcol) = clusterArray(iCluster,meanFcol)/clusterArray(iCluster,lCol);
                            
                            clusterStruct(iCluster).PixelIdxList = [clusterStruct(iCluster).PixelIdxList; clusterStruct(oldCluster).PixelIdxList];
                            clusterStruct(iCluster).PixelList = [clusterStruct(iCluster).PixelList; clusterStruct(oldCluster).PixelList];
                            clusterStruct(iCluster).BoundingBox(1)=min(clusterStruct(iCluster).BoundingBox(1),clusterStruct(oldCluster).BoundingBox(1));
                            clusterStruct(iCluster).BoundingBox(2)=min(clusterStruct(iCluster).BoundingBox(2),clusterStruct(oldCluster).BoundingBox(2));
                            clusterStruct(iCluster).BoundingBox(3)=max(clusterStruct(iCluster).BoundingBox(3),clusterStruct(oldCluster).BoundingBox(3));
                            clusterStruct(iCluster).BoundingBox(4)=max(clusterStruct(iCluster).BoundingBox(4),clusterStruct(oldCluster).BoundingBox(4));
                            clusterStruct(iCluster).Area=clusterStruct(iCluster).Area+clusterStruct(oldCluster).Area;
                        end
                    end % --- loop on other black pixels

                end %  ---- is cluster existing 
            end % ---- loop on clusters
        
        clusterArray = clusterArray(~logical(mergedClusterMask),:);
        clusterStruct = clusterStruct(~logical(mergedClusterMask),:);
        end % ---- generalized clustering


    case 'sumOverFrequency'

        % ---- One "cluster" per time bin, consisting of all frequency bins
        %      at this time slice.

        nFreqBins = size(map,1);
        freqBinArray = 1:nFreqBins;

        nTimeBins = size(map,2);
        timeBinArray = 1:nTimeBins;
        
        summedLikelihood = sum(map,1);
        summedLikelihoodMap = squeeze(sum(likelihoodMap,1));
        % ---- The squeeze command will leave summedLikelihoodMap as a row
        %      vector is size(likelihoodMap,3)=1, but each likelihood will 
        %      be a column vector if size(likelihoodMap,3)>1.  Make sure 
        %      column vector.
        if (size(likelihoodMap,3)==1)
            summedLikelihoodMap = summedLikelihoodMap(:);
        end
 
        minFreq =  0.5 * ones(1, nTimeBins);
        maxFreq =  (nFreqBins + 0.5)* ones(1, nTimeBins);
        meanFreq = (freqBinArray * map)./summedLikelihood;
        
        area = nFreqBins * ones(1, nTimeBins);
        % size(minFreq)
        % size(summedLikelihoodMap')
        clusterArray = [timeBinArray-0.5; timeBinArray; timeBinArray+0.5; ...
                        minFreq; meanFreq; maxFreq; ...
                        area; summedLikelihoodMap']';
        clusterStruct = [];
        labelledMap = [];


    case 'maxOverFrequency'
   
        nTimeBins = size(map,2);
        timeBinArray = 1:nTimeBins;
        
        % ---- Find largest pixel in each time bin of map.
        [maxLikelihood, freqOfMax] = max(map,[],1);
        
        % ---- Pull out corresponding pixel values in the other likelihood
        %      maps. 
        for iL = 1:nLikelihoods
            maxLikelihoodMap(:,iL) = likelihoodMap(freqOfMax+ ...
                (0:(size(map,2)-1))*size(map,1) + (iL-1)*size(map,1)*size(map,2));
        end
        clusterArray = [timeBinArray-0.5; timeBinArray; timeBinArray+0.5; ...
                        freqOfMax-0.5; freqOfMax; freqOfMax+0.5; ...
                        ones(1,nTimeBins); maxLikelihoodMap']';
        clusterStruct = [];
        labelledMap = [];


    otherwise
        
        error('Cluster Type unrecognized');


end


% ---- Scale output units to seconds and Hertz if requested.
if (nargin==10 && length(mapDim)==4)
    T0 = mapDim(1);
    dT = mapDim(2);
    F0 = mapDim(3);
    dF = mapDim(4);
    clusterArray(:,1:3) = (clusterArray(:,1:3) - 1) * dT + T0;
    clusterArray(:,4:6) = (clusterArray(:,4:6) - 1) * dF + F0;
end


% ---- Done .
return


