function [cc_val, cc_idx, idx_H, idx_L, normH, normL] = xccfast(data,fs,transientTime, ...
    coincidenceWindow,correlationTime,offsetFactor,windowType,triggerRate)
% XCCFAST - Cross-correlate two data streams with large time offset for transient detection.
%
% usage:
%   [cc_val, cc_idx, idx1, idx2, norm1, norm2] = xccfast(data,sampleRate,transientTime, ...
%       coincidenceWindow,correlationTime,offsetFactor,windowType,triggerRate)
%
% data              Matrix of time-domain conditioned data, with two
%                   columns (one per data stream).
% sampleRate        Scalar. Sampling rate of data [Hz].
% transientTime     Scalar.  Duration of conditioning transients [s]. This
%                   is the duration at the beginning and end (rows of data)
%                   to ignore when computing correlations.
% coincidenceWindow Scalar. Maximum delay over which to compute correlations [s].
% correlationTime   Scalar. Duration of each correlation measurement [s].
% offsetFactor      Scalar. Fractional offset of (N+1)th correlation from (N)th;
%                   i.e., overlapping of consecutive correlation measurements.
% windowType        String. Window type for FFTs. Must be one of 'hann' or
%                   'box' (square/no window).
% triggerRate       Optional scalar. Rate [Hz] at which triggers to be cross-
%                   correlated are generated in each of the two input data
%                   streams. Default 0.25 Hz.
%
% cc_val            Vector. Un-normalised cross-correlation measurements.
% cc_idx            Vector. Sample offset at which maximum abs(cross-correlation) 
%                   occurs. 
% idx1              Vector. Start index in data(:,1) at which corresponding
%                   cc_val maximum correlation is measured. 
% idx2              Vector. Start index in data(:,2) at which corresponding
%                   cc_val maximum correlation is measured. 
% norm1, norm2, 	Vectors. Norm of data(:,1), data(:2) where corresponding
%                   cc_val maximum correlation is measured.
%
% ADD DEFINITIONS AND SIZES OF OUTPUTS AND SUMMARISE ALGORITHM.
%
% Only data in the interval [(start+transientTime+coincidenceWindow),
% (end-transientTime-coincidenceWindow)] is fully analysed for
% correlations.
%
%  $Id$

% -------------------------------------------------------------------------

% ---- Check number of input arguments.
narginchk(7,8);

% ---- Assign default arguments.
if nargin < 8
    triggerRate = 0.25;
end

% ---- Derived parameters.

% ---- Dummy value: start time of data segment.
segStart = 0;
% ---- Duration of data segment.
segDur = size(data,1)/fs;
segStop = segStart + segDur;
segLength = size(data,1);

% ---- Interval of L data that will be analysed by this job.
analysis_start = segStart + (transientTime + coincidenceWindow);  
analysis_end   = segStop  - (transientTime + coincidenceWindow);
analysis_dur   = analysis_end - analysis_start;

% ---- Correlation parameters.
correlationLength = fs * correlationTime;
stepSize = correlationLength * offsetFactor;

% ---- Make window for FFTs.
switch windowType
    case 'hann'
        wind = hann(correlationLength);
    case 'box'
        wind = ones(correlationLength,1);
    otherwise
        error('windowType not recognised.');
end


% -------------------------------------------------------------------------

% ---- Divide data into overlapping sections of length correlationLength,
%      window, and compute the norm to select the loudest sections in each
%      detector.  

% ---- Number of "triggers" (data sections) to be cross-correlated in each
%      detector.
numTrigH = ceil(triggerRate * (analysis_dur+2*coincidenceWindow));
numTrigL = ceil(triggerRate * analysis_dur);

% ---- Prepare storage for outputs. 
cc_val = zeros(1,numTrigH*numTrigL);
cc_idx = zeros(1,numTrigH*numTrigL);
idx_H  = zeros(1,numTrigH*numTrigL);
idx_L  = zeros(1,numTrigH*numTrigL);
normH  = zeros(1,numTrigH*numTrigL);
normL  = zeros(1,numTrigH*numTrigL);

% ---- Use index notation to efficiently reshape timeseries into arrays of
%      overlapping segments.
% % ---- This elegant single-line form only works in MatLab2016+.
% corrIdxH = transientTime*fs + ((0:stepSize:segLength-2*transientTime*fs-correlationLength)+(1:correlationLength)');
% corrIdxL = (transientTime+coincidenceWindow)*fs+ + ((0:stepSize:segLength-2*(transientTime+coincidenceWindow)*fs-correlationLength)+(1:correlationLength)');
% ---- This more cumbersome version using repmat works in MatLab2015a.
x = (0:stepSize:segLength-2*transientTime*fs-correlationLength);
y = (1:correlationLength)';
corrIdxH = transientTime*fs + repmat(x,length(y),1) + repmat(y,1,length(x));
%
x = (0:stepSize:segLength-2*(transientTime+coincidenceWindow)*fs-correlationLength);
y = (1:correlationLength)';
corrIdxL = (transientTime+coincidenceWindow)*fs + repmat(x,length(y),1) + repmat(y,1,length(x));

% ---- Save start index of each section for timing.
corrStartIdxH = corrIdxH(1,:);
corrStartIdxL = corrIdxL(1,:);

% ---- Reshape H data, window, and find loudest "triggers".
H         = data(:,1);
H         = H(corrIdxH) .* repmat(wind,1,size(corrIdxH,2));
Hnorm2    = sum(H.^2,1);
[val,idx] = sort(Hnorm2,'descend');
H         = H(:,idx(1:numTrigH));
Hnorm     = (Hnorm2(idx(1:numTrigH))).^0.5;
corrStartIdxH = corrStartIdxH(idx(1:numTrigH));

% ---- Reshape L data, window, and find loudest "triggers".
L         = data(:,2);
L         = L(corrIdxL) .* repmat(wind,1,size(corrIdxL,2));
Lnorm2    = sum(L.^2,1);
[val,idx] = sort(Lnorm2,'descend');
L         = L(:,idx(1:numTrigL));
Lnorm     = (Lnorm2(idx(1:numTrigL))).^0.5;
corrStartIdxL = corrStartIdxL(idx(1:numTrigL));


% -------------------------------------------------------------------------

% ---- Loop over pairs of H-L "triggers" and cross-correlate.

% ---- FFT both data streams.
fH = fft(H);
cfL = conj(fft(L));

% ---- Can process all H triggers simultaneously for one L trigger using matrix
%      maths. 
for iL = 1:numTrigL

    % ---- Compute cross-correlation for all time shifts.
    cc_tmp = ifft(fH .* repmat(cfL(:,iL),1,size(fH,2))); 
    % ---- Identify index of largest abs(cc) for each H trigger.
    [~,idx] = max(abs(cc_tmp));
    % ---- Extract corresponding (signed) cc value.
    cc_val((iL-1)*numTrigH+(1:numTrigH)) = cc_tmp(sub2ind([correlationLength,numTrigH],idx,[1:numTrigH]));
    % ---- Record index of max(abs(cc)) value.
    cc_idx((iL-1)*numTrigH+(1:numTrigH)) = idx;
    
    % ---- Record index and norm of current L trigger.
    idx_L((iL-1)*numTrigH+(1:numTrigH))  = corrStartIdxL(iL);
    normL((iL-1)*numTrigH+(1:numTrigH))  = Lnorm(iL);

end

% ---- Record index and norm of H triggers.
idx_H = repmat(corrStartIdxH,1,numTrigL);
normH = repmat(Hnorm,1,numTrigL);


% -------------------------------------------------------------------------

% ---- Remove any triggers that are separated in time by more than the
%      coincidenceWindow.
%      KLUDGE: this check does not account for the sub-correlationLength shift
%      between H and L represented by cc_idx. This shoud not be a concern as 
%      coincidenceWindow >> correlationLength.
%      KLUDGE: always return at least one trigger. If none are in coincidence
%      then return one with cc_val set to zero (so zero significance).
removeIdx = find(abs(idx_H-idx_L)>coincidenceWindow*fs);
if length(removeIdx) == numTrigH*numTrigL
    cc_val = 0;
    cc_idx = cc_idx(1);
    idx_H  = idx_H(1);
    idx_L  = idx_L(1);
    normH  = normH(1);
    normL  = normL(1);
elseif ~isempty(removeIdx)
    cc_val(removeIdx) = [];
    cc_idx(removeIdx) = [];
    idx_H(removeIdx)  = [];
    idx_L(removeIdx)  = [];
    normH(removeIdx)  = [];
    normL(removeIdx)  = [];
end


% -------------------------------------------------------------------------

% ---- Done.
return

