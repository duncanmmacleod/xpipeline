function data = xapplycalibrationcorrections(data,channelNames,frameTypes,centerTime)
% XAPPLYCALIBRATIONCORRECTIONS - Correct h(t) data for known calibration errors.
%
% usage:
%
% cdata = xapplycalibrationcorrections(data,channelNames,frameTypes,centerTime)
%
%   data            Cell array of h(t) data; each element contains one 
%                   calibrated data stream to be corrected.
%   channelNames    Cell array of channel names.
%   frameTypes      Cell array of frame types.
%   centerTime      Scalar.  GPS center time of data (assumed the same for
%                   all detectors).
%
%   cdata           Cell array of h(t) data with known calibration 
%                   corrections applied.  These consist of overall (DC)
%                   scaling, and time-shifting by an integer number of
%                   samples. The time-shifting assumes the data is at the 
%                   same sampling rate as in the frame.
%
% The inputs data, channelNames, and frameTypes must all be of the same
% length. A warning is printed if the status of calibration for a given
% detector and frametype is not known for the time specified.
%
% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              process and validate command line arguments                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for sufficient command line arguments
error(nargchk(4, 4, nargin));

% ---- Start and end times of various science runs.
% tStartS5 = 815155213; %-- (Nov 04 2005 16:00:00 UTC) Official start of S5
% ---- Redefine tStartS5 to include GRB 051103 (815045155.190, Nov 03 2005 09:25:42.190 UTC)
tStartS5   = 815011213; %-- (Nov 03 2005 00:00:00 UTC)
tStartVSR1 = 863557214; %-- (May 18 2007 21:00:00 UTC)
tEndVSR1   = 875250014; %-- (Oct 01 2007 05:00:00 UTC) 

tStartA5   = 875250014; %-- (Oct 01 2007 00:00:00 UTC)
tEndA5     = 927849614; %-- (Jun 01 2009 00:00:00 UTC)

tStartVSR2 = 931035615; %-- (Jul 07 2009 21:00:00 UTC)
tEndVSR2   = 947023215; %-- (Jan 08 2010 22:00:00 UTC)

tStartVSR3 = 965599215; %-- (Aug 11 2010 22:00:00 UTC)
tEndVSR3   = 971654415; %-- (Oct 21 2010 00:00:00 UTC) 

tStartS6   = 931035615; %-- (Jul 07 2009 21:00:00 UTC)
tEndS6     = 971654415; %-- (Oct 21 2010 00:00:00 UTC) 

tStartS6eVSR4 = 991170015; %-- (Jun 03 2011 21:00:00 UTC)
tEndS6eVSR4 = 999234015; %-- (Sep 05 2011 05:00:00 UTC)

tStartA6 = 999234015; %-- (Sep 05 2011 05:00:00 UTC)
tEndA6 = 1104105616; %-- (Jan 01 2015 00:00:00 UTC)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             S5/VSR1 data                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Apply calibration corrections to S5/VSR1 data.
% ---- Check centerTime is in S5/VSR1.
if and((centerTime >= tStartS5),centerTime<=tEndVSR1) 

   % ---- Loop over channelNames
   for iCh = 1:length(channelNames)

      % ---- Set scaleFactor to 1 and time shift to 0 as default.
      scaleFactor = 1;
      nShift = 0;

      % ---- Get IFO name from channelNames, assume IFO name
      %      are the first two characters, e.g., H1:LSC-STRAIN.
      ifoName = channelNames{iCh}(1:2);

      % ---- Check if IFO is a LIGO detector
      if strcmp(ifoName,'H1')|strcmp(ifoName,'H2')|strcmp(ifoName,'L1')

         % ---- Get calibration version from frameTypes.
         %      Version should be between the 2nd and 3rd 
         %      underscore; e.g., for frameType H1_RDS_C03_L2
         %      calibration version is C03.
         underIdx = strfind(frameTypes{iCh},'_');   
         calibVersion = frameTypes{iCh}(underIdx(2)+1:underIdx(3)-1);

         % ---- If calibVersion is C03 we will apply corrections as
         %      described here: 
         %        https://wiki.ligo.org/Bursts/XPipelineS5CalibrationUncertainties
         if strcmp(calibVersion,'C03')
            % ---- correction depends on which ifo data is from
            switch ifoName
               case 'H1'
                  disp('Correcting H1 ifo data for C03 calibration error');
                  scaleFactor = 1.068;
                  nShift = -3;
               case 'H2' 
                  disp('Correcting H2 ifo data for C03 calibration error');
                  scaleFactor = 1.098;
                  nShift = -3;
               case 'L1'
                  disp('Correcting L1 ifo data for C03 calibration error');
                  scaleFactor = 0.963;
                  nShift = -3;
               otherwise
                  error(['Unexpected ifoName: ' ifoName ' with calib version: ' ...
                          calibVersion '. '...
                         'We expect ifoName to be one of H1, H2 or L1']);  
            end
         elseif strcmp(calibVersion,'C04')
            scaleFactor = 1.000;  %-- last calibration version; no known corrections 
         else
            warning(['Calibration corrections not known for channel ' ...
	        channelNames{iCh} ', frametype ' frameTypes{iCh} '.']);
         end
           
      elseif strcmp(ifoName,'V1')

         if strcmp(frameTypes{iCh}(1:6),'HrecV2') | strcmp(frameTypes{iCh}(1:6),'HrecV3')
            scaleFactor = 1.000;  %-- last calibration version; no known corrections 
         else
            warning(['Calibration corrections not known for channel ' ...
	        channelNames{iCh} ', frametype ' frameTypes{iCh} '.']);
         end

      elseif strcmp(ifoName,'G1')

         warning(['Calibration corrections not known for channel ' ...
            channelNames{iCh} ', frametype ' frameTypes{iCh} '.']);

      else

         % ---- Don't know anything about calibration of this detector.
         warning(['Calibration corrections not known for channel ' ...
            channelNames{iCh} ', frametype ' frameTypes{iCh} '.']);

      end %-- end of known S5/VSR1 corrections

      % ---- Rescale data by scaleFactor, this will equal 1 if no correction
      %      has been applied.
      data{iCh} = data{iCh} * scaleFactor;

      % ---- Time shift data if nShift is nonzero.  This will corrupt one
      %      end of the data stream, which will be negligible if any sort
      %      of conditioning is performed before the data is processed,
      %      because the ends of the data stream will be dropped to avoid
      %      filter effects.
      n = abs(nShift);
      if nShift<0
          data{iCh} = data{iCh}(n+1:end);
          data{iCh}(end+1:end+n) = 0;
      elseif nShift>0
          data{iCh}(n+1:end) = data{iCh}(1:end-n);
          data{iCh}(1:n) = 0;
      end

   end %-- end loop over channelNames

end %-- end if-S5 statement


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                A5 data                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Apply calibration corrections to A5 data.
% ---- Check centerTime is in A5.
if and((centerTime>=tStartA5),centerTime<=tEndA5) 

   % ---- Loop over channelNames.
   for iCh = 1:length(channelNames)

      % ---- Set scaleFactor to 1 as default.
      scaleFactor = 1;

      % ---- Get IFO name from channelNames, assume IFO name
      %      are the first two characters, e.g., H1:LSC-STRAIN.
      ifoName = channelNames{iCh}(1:2);

      % ---- Check if IFO is a LIGO detector.
      if strcmp(ifoName,'H1')|strcmp(ifoName,'H2')|strcmp(ifoName,'L1')

         % ---- Get calibration version from frameTypes.
         %      Version should be between the 2nd and 3rd 
         %      underscore; e.g., for frameType H2_RDS_C01_L2
         %      calibration version is C01.
         underIdx = strfind(frameTypes{iCh},'_');   
         calibVersion = frameTypes{iCh}(underIdx(2)+1:underIdx(3)-1);

         if strcmp(calibVersion,'C01') & strcmp(ifoName,'H2')
            scaleFactor = 1.000; 
         else 
            error(['Unexpected IFO: ' ifoName ' with calibration ' ...
                    'version: ' calibVersion '. We expect H2/C01.']);  
         end 
           
      elseif strcmp(ifoName,'V1')
            scaleFactor = 1.000; 

      %  ---- GEO has scaleFactor = 1.2
      %  ---- see email, J. Leong, Oct 8 2010
      elseif strcmp(ifoName,'G1')
            scaleFactor = 1.200; 

      else

         % ---- Don't know anything about calibration of this detector.
         warning(['Calibration corrections not known for channel ' ...
            channelNames{iCh} ', frametype ' frameTypes{iCh} '.']);

      end %-- end of known A5 corrections

      % ---- Rescale data by scaleFactor, this will equal 1 if no correction
      %      has been applied.
      data{iCh} = data{iCh} * scaleFactor;

   end %-- end loop over channelNames

end %-- end if-A5 statement

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            S6/VSR2-3 data                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Apply calibration corrections to S6/VSR2 data.
% ---- Check centerTime is in S6/VSR2.
if and(centerTime>=tStartS6,centerTime<=tEndS6) 

   % ---- Loop over channelNames
   for iCh = 1:length(channelNames)

      % ---- Set scaleFactor to 1 as default
      scaleFactor = 1;

      % ---- Get IFO name from channelNames, assume IFO name
      %      are the first two characters, e.g., H1:LSC-STRAIN.
      ifoName = channelNames{iCh}(1:2);

      % ---- Check if IFO is a LIGO detector.
      if strcmp(ifoName,'H1')|strcmp(ifoName,'H2')|strcmp(ifoName,'L1')

         % ---- Get calibration version from frameTypes.
         %      Version should be between the 2nd and 3rd 
         %      underscore; e.g., for frameType H2_RDS_C01_L2
         %      calibration version is C01.
         underIdx = strfind(frameTypes{iCh},'_');   
         calibVersion = frameTypes{iCh}(underIdx(2)+1:underIdx(3)-1);
      
         % ---- From Xavier Siemens and Keita Kawabe, 16 July 2009; see 
         %        <https://www.lsc-group.phys.uwm.edu/twiki/bin/view/DAC/S6VSR2Calibration>
         %      "Recently the LLO frequency domain calibration model was updated
         %      to incorporate the new DC calibration measurement results.  This
         %      is not yet propagated to the official calibration product, i.e. 
         %      h(t), but if we propagate this, the time domain strain data will
         %      be scaled by a factor of 0.89.  This is strictly an amplitude 
         %      effect, and phase is not altered. 
         if strcmp(calibVersion,'C00')
            switch ifoName
               case 'H1'
                  disp('No correction required for H1 ifo data with C00 calibration');
               case 'H2' 
                  disp('No correction required for H2 ifo data with C00 calibration');
               case 'L1'
                  disp('Correcting L1 ifo data for C00 calibration error');
                  scaleFactor = 0.89; 
             end 
         % ---- From Keita Kawabe, 17 Dec 2009, email subject:
	 %        Re: [DASWG] [Calibration] information on C02 calibration
         %      "There's no error budget for V2 published yet, but it's safe to
         %      use the old error budget for now.  Also, 10%-ish L1 DC 
	 %      correction was folded into V2 model."  See 
         %        <https://www.lsc-group.phys.uwm.edu/twiki/bin/view/DAC/S6VSR2Calibration>
         elseif strcmp(calibVersion,'C02')
            switch ifoName
               case 'H1'
                  disp('No correction required for H1 ifo data with C02 calibration');
               case 'H2' 
                  disp('No correction required for H2 ifo data with C02 calibration');
               case 'L1'
                  disp('No correction required for L1 ifo data with C02 calibration');
            end
         else
            warning(['Calibration corrections not known for channel ' ...
	       channelNames{iCh} ', frametype ' frameTypes{iCh} '.']);
         end %-- switch over LIGO detectors

      elseif strcmp(ifoName,'V1')

         if centerTime >= tStartVSR2 & centerTime <= tEndVSR2 
           if strcmp('HrecV3',frameTypes{iCh})
             % http://wwwcascina.virgo.infn.it/DataAnalysis/Calibration/Reconstruction/Runs/VSR2/index.html
             disp(['No correction required for V1 ifo data with V3 calibration ' ...
                   'in VSR2']);
           else
            warning(['Calibration corrections not known for channel ' ...
               channelNames{iCh} ', frametype ' frameTypes{iCh} '.']);
           end
         elseif centerTime >= tStartVSR3 & centerTime <= tEndVSR3 & ...
             strcmp('HrecV2',frameTypes{iCh})
             disp(['No correction required for V1 ifo data with V2 calibration ' ...
                   'in VSR3']);
         elseif centerTime>=963705615 & centerTime<=967560315 & strcmp('HrecOnline',frameTypes{iCh})
            % ---- Correct to Virgo data for VSR3.  See 
	    %        <http://wwwcascina.virgo.infn.it/DataAnalysis/Calibration/Reconstruction/Runs/VSR3/index.html>
            scaleFactor = 1.33; 
         else
            warning(['Calibration corrections not known for channel ' ...
               channelNames{iCh} ', frametype ' frameTypes{iCh} '.']);
         end 

      elseif strcmp(ifoName,'G1')

         %  ---- PJS: I think GEO may still have scaleFactor = 1.2.
         warning(['Calibration corrections not known for channel ' ...
            channelNames{iCh} ', frametype ' frameTypes{iCh} '.']);

      else

         % ---- Don't know anything about calibration of this detector.
         warning(['Calibration corrections not known for channel ' ...
            channelNames{iCh} ', frametype ' frameTypes{iCh} '.']);

      end %-- end of known S6/VSR2-3 corrections

      % ---- Rescale data by scaleFactor, this will equal 1 if no correction
      %      has been applied.
      data{iCh} = data{iCh} * scaleFactor;

   end %-- end loop over channelNames

end %-- end if-S6 statement

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            S6e/VSR4 data                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Apply calibration correction to S6e/VSR4 data.
if and(centerTime>=tStartS6eVSR4,centerTime<=tEndS6eVSR4)

   % ---- Loop over channelNames
   for iCh = 1:length(channelNames)

      % ---- Set scaleFactor to 1 as default
      scaleFactor = 1;

      % ---- Get IFO name from channelNames, assume IFO name
      %      are the first two characters, e.g., H1:LSC-STRAIN.
      ifoName = channelNames{iCh}(1:2);

      % ---- For VSR4 calibration notes see
      % ---- https://wwwcascina.virgo.infn.it/DataAnalysis/Calibration/Reconstruction/Runs/VSR4/index.html
      if strcmp(ifoName,'V1')
         scaleFactor = 1.000;

      %  ---- see GEOHF logbook page 1246
      elseif strcmp(ifoName,'G1')
         scaleFactor = 1.000;

      else

         % ---- Don't know anything about calibration of this detector.
         warning(['Calibration corrections not known for channel ' ...
            channelNames{iCh} ', frametype ' frameTypes{iCh} '.']);

      end %-- end of known S6e/VSR4 corrections

      % ---- Rescale data by scaleFactor, this will equal 1 if no correction
      %      has been applied.
      data{iCh} = data{iCh} * scaleFactor;

   end %-- end loop over channelNames

end %-- end if-S6eVSR4 statement

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            A6 data                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Apply calibration correction to A6 data.
if and(centerTime>=tStartA6,centerTime<=tEndA6)

   % ---- Loop over channelNames
   for iCh = 1:length(channelNames)

      % ---- Set scaleFactor to 1 as default
      scaleFactor = 1;

      % ---- Get IFO name from channelNames, assume IFO name
      %      are the first two characters, e.g., H1:LSC-STRAIN.
      ifoName = channelNames{iCh}(1:2);

      if strcmp(ifoName,'G1')
         scaleFactor = 1.000;

      else

         % ---- Don't know anything about calibration of this detector.
         warning(['Calibration corrections not known for channel ' ...
            channelNames{iCh} ', frametype ' frameTypes{iCh} '.']);

      end %-- end of known A6 corrections

      % ---- Rescale data by scaleFactor, this will equal 1 if no correction
      %      has been applied.
      data{iCh} = data{iCh} * scaleFactor;

   end %-- end loop over channelNames

end %-- end if-A6 statement

% ---- Issue warning if data is not from any known run.
if not( ...
   and((centerTime >= tStartS5),centerTime<=tEndVSR1) | ...
   and((centerTime>=tStartA5),centerTime<=tEndA5) | ...
   and(centerTime>=tStartS6,centerTime<=tEndS6) )
   warning(['Calibration corrections not known' ...
      ' for GPS ' num2str(centerTime) '.']);
end


% ---- Done.
return

