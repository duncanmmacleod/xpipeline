function [likelihoodTimeSeries, internalAngleIndex, detectorSpectrogram] = ...
    xtimeseries(channelNames, conditionedData, sampleFrequency, ...
        amplitudeSpectra, skyPositions, integrationLength, offsetLength, ...
        transientLength, frequencyBand, likelihoodType, verboseFlag, ...
        windowType)
% XTIMESERIES - Compute timeseries of likelihoods for a list of sky
% positions.
%
% XTIMESERIES computes various likelihood ratios (summed over a specified 
% frequency band) for a requested set of sky
% positions.  This likelihood is, roughly speaking, the natural logarithm
% of the ratio of the probability of the observed data being due to a GWB
% plus stationary Gaussian background noise to the probability of the data
% being due to stationary Gaussian background noise only. 
%
% usage:
% 
% [likelihoodTimeSeries, internalAngleIndex, detectorSpectrogram] = ...
%     xtimeseries(channelNames, conditionedData, sampleFrequency, ...
%         amplitudeSpectra, skyPositions, integrationLength, offsetLength, ...
%         transientLength, frequencyBand, likelihoodType, verboseFlag, ...
%         windowType);
%
%    channelNames         Cell array of channel name strings.
%    conditionedData      Matrix of time-domain conditioned data, with one
%                         column per channel (detector).
%    sampleFrequency      Scalar.  Sampling frequency of conditioned data
%                         [Hz].
%    amplitudeSpectra     Matrix of amplitude spectral densities
%                         [1/sqrt(Hz)], with one column per channel.
%    skyPositions         Matrix of sky positions [radians] to be tested.
%                         First column is polar angle and second column is
%                         azimuthal angle, both in radians in Earth-based
%                         coordinates.
%    integrationLength    Scalar.  Number of samples of data to use for each
%                         Fourier transform.  Must be a power of 2.
%    offsetLength         Scalar.  Number of samples between start of 
%                         consecutive FFTs.  Must be a power of 2.
%    transientLength      Scalar.  Duration of conditioning transients, in
%                         samples.  This is the number of samples at the
%                         beginning and end (rows of conditionedData) to
%                         ignore when computing likelihood maps.
%    frequencyBand        Two-component vector.  Elements are minimum and 
%                         maximum frequencies (Hz) to include in the
%                         time-frequency maps.
%    likelihoodType       String or cell array specifying types of 
%                         likelihoods to compute.  Recognized values are
%                         'elliptic', 'hardconstraint', 'incoherentenergy',
%                         'softconstraint', 'standard', 'totalenergy'.  The
%                         'nullenergy' can be computed as 'totalenergy' -
%                         'standard' for non-aligned networks and as 
%                         'totalenergy' - 'hardconstraint' for aligned 
%                         networks (e.g., for a network consisting only of
%                         H1,H2.  [TODO: Code this in so user does not have
%                         to do the math.]
%    verboseFlag          Boolean flag to control status output (default
%                         0).
%    windowType           String. One of 'none', 'bartlett', 'hann', 
%                         'modifiedhann'.
%
%
%    likelihoodTimeSeries Array.  Time series of the likelihood ratio for
%                         each likelihood type and requested sky position.  
%                         Size is numberOfTimeBins x number of sky positions 
%                         x number of likelihood types.
%    internalAngleIndex   Array.  Index of the internalAngle vector for which
%                         the elliptic likelihood is maximised in a given time 
%                         bin and sky position.  Zero if elliptic likelihood 
%                         not computed. 
%                         Size is numberOfTimeBins x number of sky positions.
%    detectorSpectrogram  Array.  Time-frequency maps of the energy in the  
%                         individual detectors, with no time shifts.
%                         size(detectorSpectrogram,3)=length(channelNames).
%
% The conditioned data should be provided as a matrix of time domain data
% with each channel in a separate column.
%
% The amplitude spectral densities should be provided as a matrix of
% one-sided frequency domain data at a frequency resolution corresponding
% to the desired integration length and with each channel in a separate column.
%
% The desired sky position should be provided as a two column matrix of the
% form [theta phi], where theta is the geocentric colatitude running from 0
% at the North pole to pi at the South pole and phi is the geocentric
% longitude in Earth fixed coordinates with 0 on the prime meridian.
%
% In the present implementation, the detector site of the first channel is
% used as the reference position when performing time shifts.  Therefore,
% when comparing likelihood maps to features in the data from individual
% detectors, the appropriate time shift is the time delay between the first
% detector and the detector being considered.
%
% See also XPIPELINE, XMAPSKY, XREADDATA, XINJECTSIGNAL, XCONDITION,
% XTILESKY, and XINTERPRET.
%
% See also ComputeAntennaResponse, LoadDetectorData.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Stephen Poprocki    poprocki@caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      process command line arguments                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for sufficient command line arguments
error(nargchk(10, 12, nargin));

% ---- Default arguments
if nargin < 12,
  windowType = 'modifiedhann';
  if nargin < 11,
      verboseFlag = 0;
  end
end

% ---- Convert likelihoodType to cell array if necessary
if ischar(likelihoodType)
    temp_likelihoodType = likelihoodType;
    clear likelihoodType
    likelihoodType{1} = temp_likelihoodType;
end
% ---- Number of likelihood types to test
numberOfLikelihoods = length(likelihoodType);
% ---- Check that each likelihoodType is a recognized type
for jLikelihoodType = 1:numberOfLikelihoods
    if (~( strcmp(likelihoodType{jLikelihoodType},'elliptic') ...
        || strcmp(likelihoodType{jLikelihoodType},'hardconstraint') ...
        || strcmp(likelihoodType{jLikelihoodType},'incoherentenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'softconstraint') ...
        || strcmp(likelihoodType{jLikelihoodType},'standard') ...
        || strcmp(likelihoodType{jLikelihoodType},'totalenergy') ...
        ))
        error(['likelihoodType ' likelihoodType{jLikelihoodType} ...
            ' is not a recognized type']);
    end
end

% ---- Force one dimensional cell array for channel list
channelNames = channelNames(:);

% ---- Number of detectors
numberOfChannels = length(channelNames);

% ---- Verify that number of data streams matches number of detectors
if size(conditionedData, 2) ~= numberOfChannels,
  error('conditioned data is inconsistent with number of detectors');
end

% ---- Block length in samples
blockLength = size(conditionedData, 1);

% ---- Verify that integration length is an integer power of two
if bitand(integrationLength, integrationLength - 1),
  error('integration length is not an integer power of two');
end

% ---- Verify that skyPositions array has at least two columns
if size(skyPositions, 2) < 2,
  error('sky positions must be a at least a two column matrix');
end
% ---- Verify that skyPositions polar coordinates are in range [0,pi].
if any((skyPositions(:, 1) < 0) | (skyPositions(:, 1) > pi)),
  error('theta outside of [0, pi]');
end
% ---- Verify that skyPositions azimutal coordinates are in range [-pi,pi)
if any((skyPositions(:, 2) < -pi) | (skyPositions(:, 2) >= pi)),
  error('phi outside of [-pi, pi)');
end

% ---- Number of sky positions
numberOfSkyPositions = size(skyPositions, 1);

% ---- Nyquist frequency
nyquistFrequency = sampleFrequency / 2;

% ---- Vector of one-sided frequencies
oneSidedFrequencies = nyquistFrequency * ( 0 : integrationLength / 2 ) / ...
                     ( integrationLength / 2 );

% ---- Frequencies in desired analysis band
frequencyIndex = find( (oneSidedFrequencies>=frequencyBand(1)) & ...
    (oneSidedFrequencies<=frequencyBand(2)) );
inbandFrequencies = oneSidedFrequencies(frequencyIndex);

% ---- Number of in-band frequency bins
numberOfFrequencyBins = length(inbandFrequencies);

% ---- Truncate amplitude spectra to desired frequency band
amplitudeSpectra = amplitudeSpectra(frequencyIndex,:);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               partition block into overlapping segments                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Calculate the indices of the endpoints of the segments of length
%      integrationLength with no overlapping.  (Overlapping will be imposed 
%      later when FFTing.)
segmentIndices = chunkIndices(blockLength, integrationLength, ...
    transientLength, integrationLength);

% ---- Number of segments
numberOfSegments = size(segmentIndices, 1);

% ---- Fractional offset of consecutive segments
offsetFraction = offsetLength/integrationLength;

% ---- Number of time bins in time-frequency maps.
%      NOTE THAT WE DO NOT ANALYSE THE LAST ELEMENT; this is convenient for
%      handling overlapping.  The 'max' test covers the case in which there 
%      is only one segment.
numberOfTimeBins = max(numberOfSegments-1,1)/offsetFraction;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              choose "internal angle" values to test                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Loop over unknown parameters: relative amplitude 
%      (-1<=eta<=1) and polarisation (0<=psi<pi/2).
%      internalAngles = [X(:) Y(:)]; % (psi,eta)
%
% ---- Use very coarse set of 10 points for computational speed.   
%      This set has been tested for overlap with elliptically polarized
%      GWBs with isotropic sky distribution, uniform cos(inclination_angle)
%      distribution, and uniform polarization angle distribution.  It was
%      found to have a mean SNR^2 overlap of 0.9317+/-0.0002.  The best
%      set of 10 found to date has a mean overlap of approximately 0.976.
% internalAngles = [ ...
%     0       1   ; ...
%     0       0.5 ; ...
%     0.5     0.5 ; ...
%     0       0   ; ...
%     0.25    0  ; ...
%     0.5     0  ; ...
%     0.75    0  ; ...
%     0       -0.5 ; ...
%     0.5     -0.5 ; ...
%     0       -1 ...
%     ];
% internalAngles(:,1) = internalAngles(:,1) * pi/2;
%
% ---- Use quasi-regular grid of 32 points in (psi, eta) space.  This set 
%      has been tested for overlap with elliptically polarized GWBs with 
%      isotropic sky distribution, uniform cos(inclination_angle)
%      distribution, and uniform polarization angle distribution.  It was
%      found to have a mean SNR^2 overlap of 0.99098+/-0.00002.  The best
%      set of 32 found to date has a mean overlap of approximately 0.993.
internalAngles = [];
IA_Neta = 10;
IA_maxNumberOfPsi = 5; 
IA_eta = -1:2/(IA_Neta-1):1;
IA_theta = acos(IA_eta);
IA_numberOfPsi = floor(IA_maxNumberOfPsi * sin(IA_theta));
for j=1:length(IA_eta)
    if (IA_numberOfPsi(j)>0)
        IA_dpsi = (pi/2) / IA_numberOfPsi(j);
        IA_psi = [0:IA_dpsi:pi/2]';
        IA_psi(end) = [];  % psi=pi/2 is redundant
        internalAngles = [internalAngles; IA_psi , IA_eta(j)*ones(length(IA_psi),1) ];
    else
        internalAngles = [internalAngles; 0 , IA_eta(j) ];
    end
end
%
numberOfInternalAngles = size(internalAngles,1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       preload detector information                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Loop over detectors.
for iDet = 1:numberOfChannels
    % ---- The detector is matched by the first character of the channel name.
    detData = LoadDetectorData(channelNames{iDet}(1));
    % ---- Extract the position vector
    rDet(iDet,:) = detData.V';
    % ---- Extract the detector response tensors
    dDet{iDet}   = detData.d;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             precompute detector responses, time delays                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Compute antenna responses and time delays for each detector and sky
%      position.  Arrays are size numberOfSkyPositions x numberOfChannels.
[Fp, Fc] = antennaPatterns(dDet, skyPositions);
timeShifts = computeTimeShifts(rDet, skyPositions);

% ---- RESET REFERENCE POSITION TO FIRST DETECTOR
timeShifts = timeShifts - repmat(timeShifts(:,1),[1 size(timeShifts,2)]);
timeShiftLengths = timeShifts * sampleFrequency;
integerTimeShiftLengths = round(timeShiftLengths);
% residualTimeShiftLengths = timeShiftLengths - integerTimeShiftLengths;
residualTimeShifts = (timeShiftLengths - integerTimeShiftLengths)/sampleFrequency;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       assign storage                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % ---- Storage for likelihood time-frequency maps and other output.
% likelihoodTimeFrequencyMap = zeros(numberOfFrequencyBins, numberOfTimeBins, numberOfLikelihoods);
% skyPositionIndex = zeros(1, numberOfTimeBins, numberOfLikelihoods);

% ---- Storage for likelihood time-series and other output.
likelihoodTimeSeries = zeros(numberOfTimeBins, numberOfSkyPositions, numberOfLikelihoods);
internalAngleIndex = zeros(numberOfTimeBins, numberOfSkyPositions);

% ---- Assign storage for time-frequency maps of single-detector data.
timeFrequencyMapFull = cell(numberOfChannels,1); % contains all frequencies
timeFrequencyMap = cell(numberOfChannels,1); % contains only non-negative frequencies
detectorSpectrogram = zeros(numberOfFrequencyBins,numberOfTimeBins,numberOfChannels);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             initialize time-frequency maps at zero delay                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- FFT the entire data stream to be analysed for each detector.
%      Note that we drop the last segment to make overlapping easier.  (If
%      there is only one segment it is not dropped!)
%      Start and stop indices for the FFT are the start and stop indices of
%      the first and last segments to be analysed:
segmentStartIndex = segmentIndices(1, 1);
segmentStopIndex = segmentIndices(max(numberOfSegments-1,1), 2);

% ---- Construct window.
switch windowType
    case 'none'
        windowData = ones(integrationLength, 1);
    case 'bartlett'
        windowData = bartlett(integrationLength);
    case 'hann'
        windowData = hann(integrationLength);
    case 'modifiedhann'
        windowData = modifiedhann(integrationLength);
    otherwise
        disp('Unknown windowType. Using none.');
        windowData = ones(integrationLength, 1);
end
% ---- Rescale window to have unity mean square.
windowMeanSquare = mean(windowData.^2);
windowData = windowData / windowMeanSquare^0.5;
% ---- Make window data into array matching size of data to be FFTed.
windowData = repmat(windowData,[1,max(numberOfSegments-1,1)]);

% ---- Loop over detectors and FFT the data from each.  Use overlapping as
%      requested.
for channelNumber = 1 : numberOfChannels

    if (offsetFraction==1)
        % ---- FFT with no overlapping - this code is faster than the more 
        %      general version below.
        % ---- Extract data for this detector.
        data = conditionedData(segmentStartIndex:segmentStopIndex,channelNumber);
        % ---- Reshape into array of size integrationLength x numberOfTimeBins, 
        %      so that each segment to be FFTed occupies one column.
        dataArray = reshape(data,integrationLength, numberOfTimeBins);
        % ---- Apply window and FFT.
        timeFrequencyMapFull{channelNumber} = fft( windowData .* dataArray );
    else
        % ---- Reshape the data into rectangular array, where consecutive
        %      columns contain data according to the requested overlap. 
        for j=1:1/offsetFraction
            % ---- Find start and stop indices.
            offsetStartIndex = segmentStartIndex + integrationLength*(j-1)*offsetFraction;
            offsetStopIndex = segmentStopIndex + integrationLength*(j-1)*offsetFraction;
            % ---- Extract data for this detector.
            data = conditionedData(offsetStartIndex:offsetStopIndex,channelNumber);
            % ---- Reshape into array of size integrationLength x numberOfTimeBins, 
            %      so that each segment to be FFTed occupies one column.
            dataArray = reshape(data,integrationLength,[]);
            % ---- Time bins in full TF map that these segments correspond to.
            timeBins = [j:1/offsetFraction:numberOfTimeBins];
            % ---- Copy these FFTs into TF map for this channel.
            timeFrequencyMapFull{channelNumber}(:,timeBins) = ...
                fft( windowData .* dataArray );
        end
    end

    % ---- Extract in-band frequencies.
    timeFrequencyMap{channelNumber} = ...
        timeFrequencyMapFull{channelNumber}(frequencyIndex,:);

    % ---- Save the squared magnitudes as detectorSpectrograms.
    detectorSpectrogram(:,:,channelNumber) = ...
        real(timeFrequencyMap{channelNumber}).^2 + ...
        imag(timeFrequencyMap{channelNumber}).^2;

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   begin loop over sky positions                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % ---- Note: When comparing different sky positions, we keep all pixels in
% %      any time bin for which the likelihood summed over that time bin is 
% %      larger than the current largest summed likelihood recorded for that 
% %      time bin for any sky position.
% % ---- Prepare storage for largest summed-over-frequency likelihoods.
% maxSummedLikelihood = ones(1,numberOfTimeBins,numberOfLikelihoods)*-inf;

for skyPositionNumber = 1 : numberOfSkyPositions,


    if verboseFlag && (mod(skyPositionNumber, numberOfSkyPositions / 100) < 1)
    fprintf(1, 'processing sky position %d of %d (%d%% complete)...\n', ...
        skyPositionNumber, numberOfSkyPositions, ...
        round(100 * skyPositionNumber / numberOfSkyPositions));            
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %               compute detector responses
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- Whitened responses for each detector, frequency x detector. 
    wFp = repmat(Fp(skyPositionNumber,:),[numberOfFrequencyBins 1]) ./ amplitudeSpectra;
    wFc = repmat(Fc(skyPositionNumber,:),[numberOfFrequencyBins 1]) ./ amplitudeSpectra;
    
    % ---- Convert to dominant polarization (DP) frame.
    [wFpDP, wFcDP, psiDP] = convertToDominantPolarizationFrame(wFp,wFc);
    
    % ---- Matrix M_AB components.  These are the dot products of wFp, with
    %      themselves and each other, for each frequency, computed in the 
    %      DP frame.
    Mpp = sum(wFpDP.*wFpDP,2);
    Mcc = sum(wFcDP.*wFcDP,2);
    epsilon = Mcc./Mpp;


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           residual phase shifts for this sky position 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    residualTimeShiftPhases = exp(sqrt(-1) * 2 * pi * ...
        inbandFrequencies' * residualTimeShifts(skyPositionNumber,:));


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %          construct time-frequency maps for each channel   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % ---- Note that due to the time shifts the edges of the time-frequency
    %      map will contain small amounts of data from the transient 
    %      periods.

    % ---- Loop over detectors.  Skip the first detector since it is used
    %      as the reference position, and therefore will always be at zero
    %      delay.  The advantage is that we only have to FFT its data once.
    for channelNumber = 2 : numberOfChannels

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %                      FFT version.                           %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Start and stop indices of the segment
        segmentStartIndex = segmentIndices(1, 1) ...
            + integerTimeShiftLengths(skyPositionNumber,channelNumber);
        segmentStopIndex = segmentIndices(max(numberOfSegments-1,1), 2) ...
            + integerTimeShiftLengths(skyPositionNumber,channelNumber);

        % ---- Separate-FFT-for-every-delay version
        if (offsetFraction==1)
            % ---- FFT with no overlapping - this code is faster than the more 
            %      general version below.
            % ---- Extract data for this detector.
            data = conditionedData(segmentStartIndex:segmentStopIndex,channelNumber);
            % ---- Reshape into array of size integrationLength x numberOfTimBins, 
            %      so that each segment to be FFTed occupies one column.
            dataArray = reshape(data,integrationLength, numberOfTimeBins);
            % ---- Apply window and FFT.
            timeFrequencyMapFull{channelNumber} = fft( windowData .* dataArray );
        else
            % ---- Reshape the data into rectangular array, where consecutive
            %      columns contain data according to the requested overlap. 
            for j=1:1/offsetFraction
                % ---- Find start and stop indices.
                offsetStartIndex = segmentStartIndex + integrationLength*(j-1)*offsetFraction;
                offsetStopIndex = segmentStopIndex + integrationLength*(j-1)*offsetFraction;
                % ---- Extract data for this detector.
                data = conditionedData(offsetStartIndex:offsetStopIndex,channelNumber);
                % ---- Reshape into array of size integrationLength x numberOfTimeBins, 
                %      so that each segment to be FFTed occupies one column.
                dataArray = reshape(data,integrationLength,[]);
                % ---- Time bins in full TF map that these segments correspond to.
                timeBins = [j:1/offsetFraction:numberOfTimeBins];
                % ---- Copy these FFTs into TF map for this channel.
                timeFrequencyMapFull{channelNumber}(:,timeBins) = ...
                    fft( windowData(:,1:size(dataArray,2)) .* dataArray );
            end
        end

        % ---- Retain only positive frequencies.
        timeFrequencyMap{channelNumber} = ...
            timeFrequencyMapFull{channelNumber}(frequencyIndex,:);

        % ---- Apply residual time shift.
        timeFrequencyMap{channelNumber} = timeFrequencyMap{channelNumber} ...
            .* repmat(residualTimeShiftPhases(:,channelNumber),[1,numberOfTimeBins]); 

    end

    % ---- Compute antenna-response weighted time-frequency maps.
    channelNumber = 1;
    wFpTimeFrequencyMap = timeFrequencyMap{channelNumber} .* ...
        repmat(wFpDP(:,channelNumber),[1,numberOfTimeBins]); 
    wFcTimeFrequencyMap = timeFrequencyMap{channelNumber} .* ...
        repmat(wFcDP(:,channelNumber),[1,numberOfTimeBins]); 
    for channelNumber = 2 : numberOfChannels
        wFpTimeFrequencyMap = wFpTimeFrequencyMap + ...
            timeFrequencyMap{channelNumber} .* ...
            repmat(wFpDP(:,channelNumber),[1,numberOfTimeBins]); 
        wFcTimeFrequencyMap = wFcTimeFrequencyMap + ... 
            timeFrequencyMap{channelNumber} .* ...
            repmat(wFcDP(:,channelNumber),[1,numberOfTimeBins]);
    end

    % ---- Compute each of the requested likelihoodType maps
    for jLikelihoodType = 1:numberOfLikelihoods
        switch likelihoodType{jLikelihoodType}

            case 'dev'  
                % ---- Elliptic likelihood coherent detection statistic, 
                %      with regularization.
                % ---- Regularizer: different for each frequency bin and sky 
                %      position.
                maxNormF2 = sum(wFp.^2+wFc.^2,2);
                % ---- Temporary storage.
                likelihood = zeros(numberOfTimeBins,numberOfInternalAngles);
                % ---- Loop over elliptic "templates" (internalAngle
                %      values).
                for internalAngleNumber = 1:numberOfInternalAngles
                    % ---- Template parameters.  Work in Earth-based
                    %      polarization frame, NOT dominant polarization
                    %      frame.  The Earth-based frame is the same for
                    %      all frequencies.
                    psi = internalAngles(internalAngleNumber,1);
                    eta = internalAngles(internalAngleNumber,2);
                    % ---- Construct elliptic projection operator.
                    %      Elliptic projection operator is conj(F)/norm(F) where 
                    %          conj(F) = (\cos(2\psi)+i\eta\sin(2\psi)) F_+ 
                    %                    + (\sin(2\psi)-i\eta\cos(2\psi)) F_x
                    %          norm(F) = sum_over_channels[
                    %                        real(conj(F))^2+imag(conj(F))^2
                    %                    ]^(0.5);
                    coeffFp = cos(2*psi) + sqrt(-1)*sin(2*psi) .* eta;
                    coeffFc = sin(2*psi) - sqrt(-1)*cos(2*psi) .* eta;
                    projF = coeffFp * wFp + coeffFc * wFc;
                    %normF = sum(real(projF).^2+imag(projF).^2,2).^0.5;
                    % ---- Include regulator with hard-wired strength.
                    normF = (sum(real(projF).^2+imag(projF).^2,2) + 0.1*maxNormF2).^0.5;
                    projF = projF ./ repmat(normF,[1,numberOfChannels]); 
                    % ---- Compute |F.d|^2 / |F|^2.
                    templikelihood = zeros(numberOfFrequencyBins,numberOfTimeBins);
                    for channelNumber = 1 : numberOfChannels
                        templikelihood = templikelihood + ...
                            repmat(projF(:,channelNumber),[1,numberOfTimeBins]) ...
                            .* timeFrequencyMap{channelNumber};
                    end
                    likelihood(:,internalAngleNumber) = ...
                        sum(real(templikelihood).^2 + imag(templikelihood).^2,1);
                end
                % ---- For each time bin keep largest likelihood.  Record
                %      corresponding internal angles.
                [likelihoodTimeSeries(:,skyPositionNumber,jLikelihoodType), ...
                    internalAngleIndex(:,skyPositionNumber) ] = max(likelihood,[],2);
            case 'elliptic'
                % ---- Elliptic likelihood coherent detection statistic.
                % ---- Temporary storage.
                likelihood = zeros(numberOfTimeBins,numberOfInternalAngles);
                % ---- Loop over elliptic "templates" (internalAngle
                %      values).
                for internalAngleNumber = 1:numberOfInternalAngles
                    % ---- Template parameters.  Work in Earth-based
                    %      polarization frame, NOT dominant polarization
                    %      frame.  The Earth-based frame is the same for
                    %      all frequencies.
                    psi = internalAngles(internalAngleNumber,1);
                    eta = internalAngles(internalAngleNumber,2);
                    % ---- Construct elliptic projection operator.
                    %      Elliptic projection operator is conj(F)/norm(F) where 
                    %          conj(F) = (\cos(2\psi)+i\eta\sin(2\psi)) F_+ 
                    %                    + (\sin(2\psi)-i\eta\cos(2\psi)) F_x
                    %          norm(F) = sum_over_channels[
                    %                        real(conj(F))^2+imag(conj(F))^2
                    %                    ]^(0.5);
                    coeffFp = cos(2*psi) + sqrt(-1)*sin(2*psi) .* eta;
                    coeffFc = sin(2*psi) - sqrt(-1)*cos(2*psi) .* eta;
                    projF = coeffFp * wFp + coeffFc * wFc;
                    normF = sum(real(projF).^2+imag(projF).^2,2).^0.5;
                    projF = projF ./ repmat(normF,[1,numberOfChannels]); 
                    % ---- Compute |F.d|^2 / |F|^2.
                    templikelihood = zeros(numberOfFrequencyBins,numberOfTimeBins);
                    for channelNumber = 1 : numberOfChannels
                        templikelihood = templikelihood + ...
                            repmat(projF(:,channelNumber),[1,numberOfTimeBins]) ...
                            .* timeFrequencyMap{channelNumber};
                    end
                    likelihood(:,internalAngleNumber) = ...
                        sum(real(templikelihood).^2 + imag(templikelihood).^2,1);
                end
                % ---- For each time bin keep largest likelihood.  Record
                %      corresponding internal angles.
                [likelihoodTimeSeries(:,skyPositionNumber,jLikelihoodType), ...
                    internalAngleIndex(:,skyPositionNumber) ] = max(likelihood,[],2);
            case 'hardconstraint'
                % ---- Hard constraint likelihood detection statistic
                likelihoodTimeSeries(:,skyPositionNumber,jLikelihoodType) = ...
                    transpose(Mpp).^(-1) * ( ...
                        real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2 ...
                    );
            case 'incoherentenergy'
                % ---- Incoherent energy statistic: total energy minus 
                %      diagonal terms in standard likelihood.
                incoherentEnergy = zeros(1,numberOfTimeBins); 
                for channelNumber = 1 : numberOfChannels
                    incoherentEnergy = incoherentEnergy ...
                        + transpose( 1 - wFpDP(:,channelNumber).^2 ./ Mpp - wFcDP(:,channelNumber).^2 ./ Mcc ) ...
                        * ( real(timeFrequencyMap{channelNumber}).^2 + imag(timeFrequencyMap{channelNumber}).^2 );
                end
                likelihoodTimeSeries(:,skyPositionNumber,jLikelihoodType) = incoherentEnergy;                    
            case 'softconstraint'
                % ---- Soft constraint detection statistic.
                likelihood = repmat(Mpp.^(-1),[1,numberOfTimeBins]) .* ( ...
                        real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2 ...
                      + real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2 ...
                    );
                % ---- Noise-only distribution of soft constraint
                %      likelihood varies with sky position.  To compare
                %      different sky positions, convert likelihood to
                %      significance(E0) := -ln(P(E>=E0)).
                % likelihoodTimeSeries(:,skyPositionNumber,jLikelihoodType) = ...
                %     transpose(sum(xsoftconstraintsignificance( ...
                %     2*likelihood,epsilon),1));
                % ---- Noise-only distribution of soft constraint
                %      likelihood varies with sky position.  To compare
                %      different sky positions, convert "likelihood" to
                %      ln(probability density).
                likelihood = xsoftconstraintsignificance(2*likelihood,epsilon);
                % ---- Normalize so the for epsilon->0 we get hard
                %      constraint.
                likelihood = -(likelihood + log(2));
                % ---- Sum log(probability) over frequency bins.
                likelihoodTimeSeries(:,skyPositionNumber,jLikelihoodType) = ...
                    transpose(sum(likelihood,1));
            case 'standard'
                % ---- Standard likelihood detection statistic.
                likelihoodTimeSeries(:,skyPositionNumber,jLikelihoodType) = ...
                    transpose(Mpp).^(-1) * (real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2) ...
                    + transpose(Mcc).^(-1) * (real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2);
            case 'totalenergy'
                % ---- Total energy (incoherent) detection statistic.
                for channelNumber = 1 : numberOfChannels
                    likelihoodTimeSeries(:,skyPositionNumber,jLikelihoodType) = ...
                        likelihoodTimeSeries(:,skyPositionNumber,jLikelihoodType) ...
                        + sum(real(timeFrequencyMap{channelNumber}).^2,1)' ...
                        + sum(imag(timeFrequencyMap{channelNumber}).^2,1)' ;
                end
            otherwise
                error(['No algorithm for computing likelihoodType ' ...
                    likelihoodType{jLikelihoodType} ]);
                
        end  % -- end switch likelihoodType

    end  % -- end loop over likelihood types

end  % -- end loop over sky positions


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          return to calling function                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Return to calling function
return;
