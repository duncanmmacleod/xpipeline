function [injScale,eff] = extractEffCurveFromFig(filepath)
    % ---- Helper function to read figfile and output injscale and
    %      efficiency data points.
    % ---- Clear variables.
    X=[];
    Y=[];
    C=[];
    S=[];
    handle = open(filepath);
    [X,Y,C,S] = datafromplot(gca);
    close(handle);
    % ---- For xpipeline eff_* figfiles we expect injScale and eff
    %      data to be in 4th child.
    injScale = X{4};
    eff      = Y{4};
    % ---- Do some sanity checking.
    if length(injScale) ~= length(eff)
        error('injScale and eff should both have same number of elements')
    end
end

