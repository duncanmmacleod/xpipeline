function [freq,spectra] = extractContributionSpectraFromFig(filepath)
    % ---- Helper function to read figfile and output contribution spectra and
    %      frequency data points.
    % ---- Clear variables.
    X=[];
    Y=[];
    C=[];
    S=[];
    handle = open(filepath);
    [X,Y,C,S] = datafromplot(gca);
    close(handle);
    % ---- For xpipeline eff_* figfiles we expect injScale and eff
    %      data to be in 4th child.
    freq    = X;
    spectra = Y;
    % ---- Do some sanity checking.
    if ~isequal(size(freq),size(spectra))
        error('freq and spectra should both have same number of elements')
    end

    if ~isequal(size(freq{1}),size(spectra{1}))
        error('freq{1} and spectra{1} should both have same number of elements')
    end

end

