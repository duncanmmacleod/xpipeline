function [y,x] = roc(a, b, str)
% ROC(A, B) - Compute and plot the Receiver-Operator Characteristic curve.
%
% usage:
%
%  [y,x] = roc(a, b, str)
%
%  a     Vector of sample statistic values when the null hypothesis is
%        true. 
%  b     Vector of sample statistic values when the null hypothesis is
%        false. 
%  str   Optional string. If supplied, a loglog plot of the ROC curve is
%        added to the current figure.  The linestyle used is 'str'. 
%
%  y     Vector of false alarm probabilities.
%  x     Vector of detection probabilities.
%
% Zero values of 'a' and 'b' are ignored.
%
% original write: Antony Searle
%
% $Id$

% ---- Ignore zero values and sort.
a = sort(nonzeros(a));
b = sort(nonzeros(b));

% ---- Antony's original ROC algorithm.  Wrong?
if (0)

    ai = 1;
    bi = 1;

    c = 1;

    x(c) = 0;
    y(c) = 0;

    while(ai <= length(a) && bi <= length(b))
        if (a(ai) < b(bi))
            x(c + 1) = x(c);
            while (ai <= length(a) && a(ai) < b(bi))
                x(c + 1) = x(c + 1) + 1 / length(a);
                ai = ai + 1;
            end
            y(c + 1) = y(c) + 1 / length(b);
            bi = bi + 1;
            c  = c  + 1;
        else
            y(c + 1) = y(c);
            while (bi <= length(b) && a(ai) >= b(bi))
                y(c + 1) = y(c + 1) + 1 / length(b);
                bi = bi + 1;
            end
            x(c + 1) = x(c) + 1 / length(a);
            ai = ai + 1;
            c  = c  + 1;
        end
    end

    x(c) = 1;
    y(c) = 1;

else
    
    % ---- Vectorized ROC code.
    
    % ---- Estimate cumulative distribution of statistic under each
    %      hypothesis at sample statistic values.
    ca = cumsum(ones(size(a)))/length(a);
    cb = cumsum(ones(size(b)))/length(b);
    % ---- Interpolate signal hypothesis distribution to the sampled values
    %      for the null hypothesis.
    cb_i = robustloginterp(b,cb,a);
    % ---- The robustloginterp function returns NaNs where the
    %      interpolation fails.  Remove these points.
    k = find(isnan(cb_i));
    cb_i(k) = [];
    ca(k) = [];
    % ---- Convert from probability of statistic < threshold to
    %      probability of statistic > threshold.
    y = 1-ca;
    x = 1-cb_i;

end

% --- Make plot if desired.
if (nargin==3)
    loglog(y, x,str);
    grid on
    axis image;
    xlabel('false alarm probability')
    ylabel('detection probability')
end

return
