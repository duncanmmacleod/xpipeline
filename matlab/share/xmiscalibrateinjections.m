function [data] = xmiscalibrateinjections(data,misCalibFileName,... 
                  channelNames, sampleFrequencies)
%
% XMISCALIBRATEINJECTIONS Apply amplitude scaling and time shift to injections
%                         to mimic miscalibration of ifo data
%
% usage:
%
% [data] = xmiscalibrateinjections(data,misCalibFileName,channelNames)
%
%   data                 Cell array, injection data time series
%
%   misCalibFileName     String, file containing parameters describing the 
%                        miscalibration to be applied.
%                        This file will contain a row corresponding to
%                        each if data stream specified in channels.txt
%                        Each row will contain 2 columns: 
%       scaleFactor      Double, the scaling factor to be applied to the 
%                        amplitude
%       nShiftPoints     Integer, number of sample points to shift the 
%                        injections
%   channelNames         Cell array, names of the channels of the input data 
%   sampleFrequencies    Vector, sample frequencies of corresponding data 
%                        channel

% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 process and validate command line arguments                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check for sufficient command line arguments
error(nargchk(4, 4, nargin));

scaleFactor  = [];
nShiftPoints = [];

% ---- read in parameters from misCalibFileName
if(exist(misCalibFileName,'file'))
   % ---- Might as well replace all of these lines by a load statement?
   % ---- It would be nice to check number of lines is equal to number
   %      of data channels?
   fmis = fopen(misCalibFileName,'r'); 
   for iCh = 1:length(data)
      scaleFactor(iCh)  = str2num(fscanf(fmis,'%s',1)); 
      nShiftPoints(iCh) = str2num(fscanf(fmis,'%s',1)); 
   end
   fclose(fmis);
else
   error(['File: ' misCalibFileName ' is required in xmiscalibrateinjections.m '... 
          'but does not exist']);
end

% ---- Loop over channels in data
for iCh = 1:length(data)

   % ---- Check that data is a column vector
   if size(data{iCh},2)>1
      % ---- It looks like we have a row vector...
      if size(data{iCh},1)>1
         % ---- If we have a 2 dimensional array then quit
         error(['data should be a cell array where each array '...
         'is a one-dimensional column vector']);
      else
        % ---- If we have a row vector then force column vector
        warning(['data should be a cell array where each array '...
         'is a one-dimensional column vector, transforming row '...
         'vector into a column vector']);
        data{iCh}=data{iCh}(:);
      end
   end

   disp(['Scaling injections into ' channelNames{iCh} ' by '... 
         num2str(scaleFactor(iCh))]);
   % ---- Scale amplitude of data
   data{iCh} = data{iCh} * scaleFactor(iCh);

   % ---- Shift data points from one end of the column vector
   %      to another to simulate time (phase) error in calibration

   % ---- Check we are going to do a shift
   if nShiftPoints(iCh)

      % ---- Check nShiftPoints is an integer
      if (nShiftPoints(iCh) - round(nShiftPoints(iCh)))  
         disp(['nShiftPoints(' num2str(iCh) ') is not an integer: ' ...
               num2str(nShiftPoints(iCh)) ]);
         error(['Please ensure nShiftPoints values specified in the ' ...
                'misCalibFileName e.g., ' misCalibFileName ' are integers' ]);
      end

      % ---- Check that nShiftPoints is less than length of data
      if abs(nShiftPoints(iCh)) > length(data{iCh})
         error(['nShiftPoints must be less than length of data']);
      end 

      % ---- shift data forwards or backwards in time depending on nShiftPoints
      if nShiftPoints(iCh) > 0
         % ---- if nShiftPoints is +ve we will shift data forwards in time  
         disp(['Shifting injections into ' channelNames{iCh} ' forwards in '...
               'time by ' num2str(nShiftPoints(iCh)) ' data points ' ...
               'corresponding to ' ...
               num2str(nShiftPoints(iCh)/sampleFrequencies(iCh)) ' seconds' ]);
         % ---- get last nShiftPoints of data 
         tempEnd = data{iCh}(length(data{iCh})-nShiftPoints(iCh)+1:length(data{iCh}));
         % ---- get remainder of data
         tempStart = data{iCh}(1:length(data{iCh})-nShiftPoints(iCh));
 
         % ---- rewrite data with tempEnd moved to the start
         data{iCh} = [tempEnd;tempStart];
      else
         % ---- if nShiftPoints is -ve we will shift data backwards in time  
         disp(['Shifting injections into ' channelNames{iCh} ' forwards in '...
               'time by ' num2str(abs(nShiftPoints(iCh))) ' data points ' ...
               'corresponding to ' ...
               num2str(abs(nShiftPoints(iCh))/sampleFrequencies(iCh)) ' seconds' ]);
         % ---- get first nShiftPoints of data 
         tempStart = data{iCh}(1:abs(nShiftPoints(iCh)));
         % ---- get remainder of data
         tempEnd = data{iCh}(abs(nShiftPoints(iCh))+1:length(data{iCh}));
 
         % ---- rewrite data with tempEnd moved to the start
         data{iCh} = [tempEnd;tempStart];
      end 
   
   end %-- end if nShiftPoints(iCh)

end %-- end loop over data channels

%-- done
