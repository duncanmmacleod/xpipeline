function [t, hpc, hcc] = longbar(D, M, L, R, f, tau, phi, theta)
% LONGBAR - make GW waveform for rotating cylindrical bar. 
% 
% usage:
%
%   [t,hp,hc] = longbar(D, M, L, R, f, tau, phi, theta)
%
% D         Scalar.  Distance to source [kpc].
% M         Scalar. Mass of bar [solar masses].  Canonical protoneutron
%           star masses of 0.15 to 1.5 are sensible.
% L         Scalar. End-to-end length of bar [cm].  Plausible values are
%           2e6 - 6e6 cm.  
% R         Scalar.  Bar radius [cm]. Plausible vales are 5e5 - 2e6 cm.
%           Must have R < L. 
% f         Scalar.  Bar rotational frequency [Hz]. Plausible values are
%           200 - 1000 Hz. 
% tau       Scalar.  Duration parameter [s] of bar mode.  Use T<=0.25 to
%           avoid turn-on/off discontinuities. 
% phi       Scalar.  Azimuthal angle of observer in bar frame [rad].
% theta     Scalar.  Polar angle of observer in bar frame [rad].
%
% LONGBAR computes h_+ and h_x for a rotating bar as a function of the bar 
% parameters and the observer angle in the source coordinate system.  It is
% based on calculations by Christian D. Ott, LIGO-T1000553-v1, 
% https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?docid=21139
%
% The output data stream will have a duration of 1 sec and a sampling rate
% of 16384 Hz.  The waveform envelope is a cosine-squared function with a
% peak at t = 0.5 and which is zero outside of 0.5+/-2*tau.
% 
% Example: 
%
% [t hp hc] = longbar(1, 0.2, 60e5, 10e5, 400, 0.01, 0, pi/4);

% -------------------------------------------------------------------------
%    Preparatory.
% -------------------------------------------------------------------------

% ---- Validate input arguments.
error(nargchk(8, 8, nargin));
if tau > 0.25
    warning('Decay parameter tau > 0.25 s.');
end
    
% ---- Constants.
msun = 1.99e33;     %-- g
clight = 2.9979e10; %-- cm/s
ggrav = 6.6726e-8;  %-- cgs units
factor = ggrav / clight^4;

% ---- Unit conversion: Bar mass [g].
M = M * msun;

% ---- Unit conversion: Distance to source [cm].
D = D * 3.08568e18;

% ---- Parameters.
T = 1;           %-- waveform duration [s]
fs = 16384;      %-- sampling rate [Hz]
dt = 1/fs;       %-- sampling t [s]

% ---- Time samples.
t = (0:dt:T-dt)';


% -------------------------------------------------------------------------
%    Compute quadrupole moment of bar.
% -------------------------------------------------------------------------

% ---- The bar is assumed to be rotating in the x-y plane, with rotational
%      axis along the z (coordinate 3) direction. 

% ---- Amplitude prefactor for quadrupole moment.
cfac = 1/6 * M * (L^2 - 3*R^2) * (2.0*pi*f)^2;

% ---- Matrix part of second t-derviative of quadrupole moment.
Idotdot{1,1} = - cos(4.0*pi*f*t);
Idotdot{1,2} = sin(4.0*pi*f*t);
Idotdot{1,3} = 0;
Idotdot{2,1} = sin(4.0*pi*f*t);
Idotdot{2,2} = cos(4.0*pi*f*t);
Idotdot{2,3} = 0;
Idotdot{3,1} = 0;
Idotdot{3,2} = 0;
Idotdot{3,3} = 0;

% ---- Apply amplitude prefactor.
for k=1:3
    for j=1:3
        Idotdot{k,j} = Idotdot{k,j}*cfac;
    end
end


% -------------------------------------------------------------------------
%    Compute GW.
% -------------------------------------------------------------------------

% ---- Compute l = 2 modes only.
h = 0;
for m = [-2,-1,0,1,2]
    cylm = complex(re_Y2m(m,theta,phi), im_Y2m(m,theta,phi));
    Hlm  = complex(re_Hlm(m,Idotdot,factor), im_Hlm(m,Idotdot,factor));
    h = h + cylm * Hlm;
end
hp = real(h)./D;
hc = imag(h)./D;

% ---- Apply a cosine-squared envelope to both polarisations.
cos2fac = zeros(size(t));
ind = find(abs(t-T/2) <= 2*tau);
cos2fac(ind) = (cos((pi/(4*tau))*(t(ind)-T/2))).^2;
hpc = hp.*cos2fac;
hcc = hc.*cos2fac;

% ---- Done.
return

