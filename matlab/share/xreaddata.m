function [data, sampleFrequencies] = xreaddata(channelNames, frameTypes, ...
    frameCacheFile, startTime, stopTime, timeOffsets, verboseFlag)
%
% XREADDATA Read multiple channels of data from frame files.
%
% XREADDATA finds and retrieves multiple channels of time series data from a set
% of frame files.  The data is specified by the frame file type, channel name,
% start time, and duration or stop time.  The necessary frame files are located
% using a file name caching scheme.
%
% XREADDATA is build on top of the READFRAMEDATA set of high level functions
% for reading data from frame files.
%
% usage:
%
%   [data, sampleFrequencies] = xreaddata(channelNames, frameTypes, ...
%       frameCacheFile, startTime, stopTime, timeOffsets);
%
%   channelNames         Cell array of channel names.
%   frameTypes           Cell array of frame types.
%   frameCacheFile       String.  File name of frame file cache.
%   startTime            Scalar.  GPS start time of data to extract.
%   stopTime             Scalar.  GPS stop time (or duration) of data to 
%                        extract.  If stopTime < startTime then it is treated as
%                        a duration.
%   timeOffsets          Vector of offset times [seconds]. 
%   verboseFlag          Enable verbose output.
%   
%   data                 Cell array of extracted data.
%   sampleFrequencies    Vector of sample frequencies for each channel in data.
%
% The data is returned as a cell array of column vectors in the same order
% as in the cell array of channel names.
%
% XREADDATA retrieves data from the requested start time up to, but not
% including, the requested stop time, such that stop minus start seconds are
% retrieved.  Alternatively, the desired duration in seconds may be specified
% instead of the GPS stop time parameter.
%
% The optional time offset argument should be a vector with one element per
% channel that specifies an additional time shift to apply to the start time
% for each detector.  A positive offset corresponds to reading in data from
% a later time; i.e., the data interval read is [startTime + timeOffsets, 
% stopTime + timeOffsets).  If no time offsets are specified, a default value of
% zero is assumed for all detectors.
%
% See also READFRAMEDATA, LOADFRAMECACHE, CREATEFRAMECACHE, and FRGETVECT.

% Shourov K. Chatterji
% shourov@ligo.caltech.edu
% 2005-Jul-22

% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 process and validate command line arguments                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check for sufficient command line arguments
error(nargchk(5, 7, nargin));

% if channel names is not a cell array,
if ~iscell(channelNames),

  % insert data into a single cell
  channelNames = mat2cell(channelNames, size(channelNames, 1), ...
                          size(channelNames, 2));

% otherwise, continue
end

% if frame types is not a cell array,
if ~iscell(frameTypes),

  % insert data into a single cell
  frameTypes = mat2cell(frameTypes, size(frameTypes, 1), size(frameTypes, 2));

% otherwise, continue
end

% force one dimensional cell arrays
channelNames = channelNames(:);
frameTypes = frameTypes(:);

% determine number of channels
numberOfChannels = length(channelNames);

% validate frame types
if length(frameTypes) ~= numberOfChannels,
  error('number of frame types is inconsistent with number of channels');
end

% default arguments
if nargin == 5,
  timeOffsets = zeros(numberOfChannels, 1);
end

if nargin < 7
  verboseFlag = 0;
end

% convert duration to absolute stop time
if stopTime < startTime,
  stopTime = startTime + stopTime;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         initialize result structures                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initialize result structures
data = cell(numberOfChannels, 1);
sampleFrequencies = zeros(numberOfChannels, 1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                  read data                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% read frame file cache
frameCache = loadframecache(frameCacheFile);

% begin loop over channels
for channelNumber = 1 : numberOfChannels,

  % read data
  [data{channelNumber}, sampleFrequencies(channelNumber), readTimeStamps] = ...
      readframedata(frameCache, channelNames{channelNumber}, ...
                    frameTypes{channelNumber}, ...
		    startTime + timeOffsets(channelNumber), ...
                    stopTime + timeOffsets(channelNumber), ...
                    [], ...
		    verboseFlag+1);

  % ---- If readframedata encounters an error such as missing data then it 
  %      returns an empty data array. Check for this and exit with meaningful 
  %      error message if required.
  if isempty(data{channelNumber})
    error(['Missing data for channel ' channelNames{channelNumber}])
  end

  % Check whether read time stamps are correct
  readTimeSteps = unique(readTimeStamps(2:end)-readTimeStamps(1:end-1));
  % protect againt roundoff false alarms
  if max(readTimeSteps) > min(readTimeSteps)*(1+1e-2)
    error(['Time steps are not unique, probably some dropped samples. The ' ...
           'list of time steps is: ' num2str(readTimeSteps)])
  end

  % Check whether read and desired time starts are compatible
  readTimeDiscrepency = ...
      startTime + timeOffsets(channelNumber)- readTimeStamps(1);
  if abs(readTimeDiscrepency) > 1e-6
    warning(['The discrepency between desired and read times is ' ...
             num2str(readTimeDiscrepency) ' seconds'])
    if abs(readTimeDiscrepency) > 2e-5
      error(['This discripency is large, a phase correction has to be ' ...
             'added to the code'])
    end
  end

  % force column vector data
  data{channelNumber} = data{channelNumber}(:);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          return to calling function                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% return to calling function
return;
