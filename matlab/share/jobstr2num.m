function jobNumbers = jobstr2num(jobNumberString)
% jobstr2num - parse a string of job numbers to a vector.
%
%    jobNumbers = jobstr2num(jobNumberString)
%
%    jobNumberString   String of comma separated, non-negative integers.
%                      A range of two non-negative integers is specified as
%                      a-b. 
%
%    jobNumbers        Vector of numbers listed in jobNumberString.  The
%                      elements are unique and sorted. 
%
% Example:
%   jobstr2num('12,0-5,7,3')
%     ans =
%         0     1     2     3     4     5     7    12
%
% See also xpipeline, xdetection.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id$

% ---- Initialize jobNumbers.
jobNumbers = [];

% ---- Tokenize on commas and whitespace.
[tokens] = dataread('string',jobNumberString,'%s','delimiter',' \b\t,');

nTok = length(tokens);

% ---- Step through tokens.
for iTok = 1:nTok,

    % ---- Split into range.
    [rstart, rend] = dataread('string',tokens{iTok},'%d%d','delimiter','-');

    % ---- If the end does not exist, this means that it was not a range.
    if (isempty(rend))
        jobNumbers = [jobNumbers rstart];
    else
        jobNumbers = [jobNumbers rstart:rend];
    end

end

% ---- Uniqueify and sort.
jobNumbers = unique(jobNumbers);

return;
