function Y = integerintervalunion(X)
% INTEGERINTERVALUNION - take the union of intervals of integers
% 
%  Y = integerintervalunion(X)
% 
%  X    Cell array. Each element is a list of integer intervals, one interval
%       per row, with columns [start, stop]. The lists do not need to be 
%       sorted and can contain overlapping intervals.
% 
%  Y    Numeric array. The rows are the intevals formed by taking the union
%       of all intervals from all lists in X.
%
% Example:
%
% >> X = {[10 12; 17 18; 25 30; 9 11],[-6 -5; 11 13; 27 28],[-5 1]}
% >> Y = integerintervalunion(X)
%
% Y =
%  
%     -6     1
%      9    13
%     17    18
%     25    30
%
% $Id$

% ---- First check to see if any of the input X arrays contain data, in which 
%      case we have data to process. Otherwise set Y=[] and exit.
dataToProcess = false;
for ii=length(X):-1:1  %-- go backwards for easy determination of firstNonEmptyX
    if ~isempty(X{ii})
        dataToProcess = true;
        firstNonEmptyX = ii;  %-- first non-empty element of X; used below.
    end
end

if dataToProcess

    % ---- We concatenate all the elements of X into a single array.
    % ---- Start by initialising the output to the first interval in the first input list.
    Y = X{firstNonEmptyX}(1,:);
    X{firstNonEmptyX}(1,:) = [];
    % ---- Check all other intervals in all lists for overlap with Y.
    for iList = firstNonEmptyX:length(X)
        if ~isempty(X{iList})
            for iInt = 1:size(X{iList},1)
                % ---- Find all intervals in Y that touch/overlap this interval.
                %      The +1 added to the interval ends account for the case of 
                %      adjacent intervals, e.g. [113,117] and [118,125] -> [113,125].
                idx = find(Y(:,1)  <=X{iList}(iInt,2)+1 ...
                         & Y(:,2)+1>=X{iList}(iInt,1));
                % ---- Combine these intervals.
                if ~isempty(idx)
                    combinedStart = min([X{iList}(iInt,1); Y(idx,1)]);
                    combinedEnd   = max([X{iList}(iInt,2); Y(idx,2)]);
                    % ---- Replace overlapping intervals with combined interval.
                    Y(idx,:) = [];
                    Y = [Y; combinedStart, combinedEnd];
                else
                    % ---- Current interval does not overlap any in Y, so 
                    %      add it to the combined interval list unaltered.
                    Y = [Y; X{iList}(iInt,:)];
                end
            end
        end
    end
    % ---- Sort the final list of intervals.
    Y = sortrows(Y);

else

    % ---- All of the input X arrays are empty, so set the output to empty.
    Y = [];

end

% ---- Done.
return

