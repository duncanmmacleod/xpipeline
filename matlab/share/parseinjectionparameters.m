function [injParams, gps_s, gps_ns, phi, theta, psi, type, parameters] = ... 
    parseinjectionparameters(injectionParameters,verbose)
% PARSEINJECTIONPARAMETERS - convert a cell array of injection parameters to numeric arrays
%
% [params, gps_s, gps_ns, phi, theta, psi, type, parameters] = parseinjectionparameters(injParams,verbose)
%
% injParams   Cell array of strings returned by READINJECTIONPARAMETERS.
% verbose     Optional flag. If specified, the number of injections parsed
%             is reported to standard output. Default true.
%
% params      Struct with fields gps_s, gps_ns, phi, theta, psi, type,
%             parameters. Each field is an array (cell array for type,
%             parameters) with one row per injection. Each field has one
%             column per detector if the parameters are detector dependent
%             (eg for miscalibrated GWB injections or glitch injections),
%             or only a single column if the same parameters are used for
%             all detectors in the network. See below for field
%             definitions.
%
% The following outputs are parameters for the first detector ONLY:
%
% gps_s       Vector.  Integer part of peak injection time (nanosec).
% gps_ns      Vector.  Non-integer part of peak injection time (nanosec).
% phi         Vector.  Azimuthal sky position angle (radians).
% theta       Vector.  Polar sky position angle (radians).
% psi         Vector.  Polarization angle (radians).
% type        Cell array. Each element is a 1x1 cell array containing a
%             string.  Waveform type used by xmakewaveform. 
% parameters  Cell array. Each element is a 1x1 cell array containing a
%             string.  Parameters used by xmakewaveform. 
%
% This function is intended as a wrapper for READINJECTIONFILE.
%
% $Id$

% ---- Test number of arguments
error(nargchk(1,2,nargin));

% ---- Preparatory.
nInjection = length(injectionParameters);


% -------------------------------------------------------------------------
%     Legacy code: Extract parameters for the first detector ONLY.
% -------------------------------------------------------------------------

% ---- Prepare storage.
gps_s = zeros(nInjection, 1);
gps_ns = zeros(nInjection, 1);
phi = zeros(nInjection, 1);
theta = zeros(nInjection, 1);
psi = zeros(nInjection, 1);
type = cell(nInjection, 1);
parameters = cell(nInjection, 1);

% ---- Loop over injections, extractig parameters for the first detector.
for injectionNumber = 1:nInjection
    [gps_s(injectionNumber), ...
    gps_ns(injectionNumber), ...
    phi(injectionNumber), ...
    theta(injectionNumber), ...
    psi(injectionNumber), ...
    type{injectionNumber}, ...
    parameters{injectionNumber} ...
    ] = strread(injectionParameters{injectionNumber}, '%f %f %f %f %f %s %s',1);
end


% -------------------------------------------------------------------------
%     Extract parameters for all detectors into a struct.
% -------------------------------------------------------------------------

% ---- Parse the first parameter string to determine how many columns of
%      data it contains. This must be a multiple of 7.
x = injectionParameters{1}; 
elem = 0;
while ~isempty(x)
    [junk,x] = strtok(x);
    if ~isempty(junk)
        elem = elem + 1;
    end
end
if mod(elem,7)==0
    nDetector = elem/7;
else
    error(['Injection file appears to have ' num2str(elem) ' columns. ' ...
        'The number of columns must be a multiple of 7.']);
end

% ---- Separate each line (a string) into separate elements in a cell 
%      array.  For actual GWB injections (instead of glitches) elements for
%      jParam > 7 may be empty.
for jParam=1:(7*nDetector)
    for iCell=1:nInjection
        [currentParameters{jParam}{iCell} injectionParameters{iCell}] = ...
            strtok(injectionParameters{iCell});
    end
end

% ---- Initialise storage for parameters.
injParams.gps_s      = zeros(nInjection,nDetector);
injParams.gps_ns     = zeros(nInjection,nDetector);
injParams.phi        = zeros(nInjection,nDetector);
injParams.theta      = zeros(nInjection,nDetector);
injParams.psi        = zeros(nInjection,nDetector);
injParams.type       =  cell(nInjection,nDetector);
injParams.parameters =  cell(nInjection,nDetector);

% ---- Loop over detectors and injections and parse parameters. 
for jDetector=1:nDetector
    for jInjection=1:nInjection
        % ---- Extract parameters for current injection.
        if (isempty(currentParameters{(jDetector-1)*7+1}{jInjection}))
            % ---- Use parameters for first detector instead of current
            %      detector.
            offset = 0;
        else
            % ---- Use parameters for current detector.
            offset = (jDetector-1)*7;
        end
        injParams.gps_s(jInjection,jDetector)      = str2num(currentParameters{offset+1}{jInjection});
        injParams.gps_ns(jInjection,jDetector)     = str2num(currentParameters{offset+2}{jInjection});
        injParams.phi(jInjection,jDetector)        = str2num(currentParameters{offset+3}{jInjection});
        injParams.theta(jInjection,jDetector)      = str2num(currentParameters{offset+4}{jInjection});
        injParams.psi(jInjection,jDetector)        = str2num(currentParameters{offset+5}{jInjection});
        injParams.type{jInjection,jDetector}       =         currentParameters{offset+6}{jInjection};
        injParams.parameters{jInjection,jDetector} =         currentParameters{offset+7}{jInjection};
    end
end

% ---- Optional verbosity.
if nargin>1 && ~verbose
    % ---- Be quiet.
else
    % ---- Be verbose.
    disp(['Parsed ' num2str(nInjection) ' injections.']);
end

% ---- Done
return

