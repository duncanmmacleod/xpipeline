function [x, t] = simulateddetectornoise(DET,T,fs,fmin,fmax,seed)
% SIMULATEDDETECTORNOISE - Simulate Gaussian colored noise for an IFO.
% 
% SIMULATEDDETECTORNOISE generates simulated Gaussian noise with spectrum 
% matching the design sensitivity curve for a specified gravitational-wave
% detector, or matching a user-supplied noise curve.
%  
%    [n, t] = simulateddetectornoise(DET,T,fs,fmin,fmax)
%  
%    DET   String or Vector. String: name of the detector to be simulated.
%          Must be one of those recognized by the function SRD, or 'white'
%          (not case sensitive). Vector: one-sided PSD sampled at 
%          frequencies [1/T:1/T:fs/2] (i.e. all positive frequencies up to
%          the Nyquist of the desired output data).
%    T     Scalar. Duration (s) of noise data stream.  T and fs should
%          satisfy T*fs=integer.
%    fs    Scalar. Sampling frequency (Hz) of data stream.  T and fs should
%          satisfy T*fs=integer.
%    fmin  Scalar. Minimum desired frequency (Hz).
%    fmax  Scalar. Maximum desired frequency (Hz).
%    seed  Optional scalar. Seed for randn generator used to produce the
%          noise data.  See randn.
%
%    n     Column vector of length round(fs*T).  Noise timeseries (strain).
%    t     Column vector of length round(fs*T).  Times (s) at which noise
%          vector is sampled, starting from t=0.
%  
% The design noise spectra are generated from the function SRD.  The noise
% power spectrum of the simulated data drops as f^(-2) outside the range 
% [fmin,fmax].  For 'white', the output data is zero-mean unit-variance
% Gaussian noise and white over the full frequency band.  
%    
% For information on the conventions used for discrete Fourier transforms,
% see FourierTransformConventions.m.  For examples of how to use this
% function, do "type simulateddetectornoise.m" and see the comments below
% this help information.
%
% original version: 
%   Patrick J. Sutton, 2005.04.09 <patrick.sutton@astro.cf.ac.uk>
%  
%  $Id$

% Note: The Virgo collaboration document VIR-NOT-ROM-1390-090, authors S. Frasca
% and M. A. Papa, discuss a technique for constructing simulated Gaussian noise
% based on a spectrum.  Of particular interest is a technique for constructing 
% lines in the time domain which are then added to the data.  From page 6, the 
% line contribution is the real part of the process y_i, where
%
%    y_i = x_i + w y_{i-1},
%
% x is a white complex Gaussian noise process, w is the complex number 
%
%    w = exp{i theta} exp{-dt/tau},
%
% and dt is the sampling time.  Three input parameters are required:
% i) the frequency of the line peak, f_0 = theta / (2 pi dt)
% ii) the bandwidth of the peak, df = 1/(2 pi tau)
% iii) the peak height of the spectrum, sigma_x^2 = S_{peak} 2 pi (1-|w|^2)/tau.

% % ---- Test/demonstration code for making simulated data.
% ifo = {'aLIGO','aVirgo', 'ETB', 'ETC', 'GEO', 'GEO-HF', 'LIGO', ...
%        'LIGO-LV', 'LHO', 'LLO', 'LISA', 'TAMA', 'Virgo', 'Virgo-LV'};
% df = 1;
% fmin = 40;
% fmax = 2000;
% f0 = [fmin:df:fmax]';
% T = 16;
% fs = 16384;
% seed = 12345;
% for ii = 1:length(ifo)
%     S0 = SRD(ifo{ii},f0);
%     [x, t] = simulateddetectornoise(ifo{ii},T,fs,fmin,fmax,seed);
%     [S, F] = medianmeanaveragespectrum(x,fs,fs/df);
%     figure; set(gca,'fontsize',16)
%     loglog(F,S.^0.5,'b-')
%     hold on; grid on
%     loglog(f0,S0.^0.5,'k-','linewidth',2)
%     xlabel('frequency (Hz)');
%     ylabel('strain noise amplitude');
%     title(ifo{ii})
%     legend('simulated data','design spectrum',2)
% end

% ---- Checks.
narginchk(5,6);
if ~(T>0 & fs>0 & fmin>0 & fmax>0)
    error(['Input duration, sampling rate, and minimum and maximum ' ... 
        'frequencies must be positive.'])
end
if (fmax<=fmin)
    error('Maximum frequency must be > minimum frequency.')
end
if (fmax>fs/2)
    error('Maximum frequency must be <= Nyquist frequency fs/2.')
end
if ~ischar(DET) 
    % ---- If not a string, it must be a vector of noise PSD values at
    %      frequencies [1/T:1/T:fs/2]. Verify that it is a vector with the
    %      correct length. 
    if ~isvector(DET) || length(DET) ~= T*fs/2
        disp(['Size of DET variable: ' num2str(size(DET)) ]);
        disp(['Expected number of elements: ' num2str(T*fs/2) ]);
        error('The PSD vector does not have the correct size');
    end
end
if (nargin==6)
    % if (~isscalar(seed))  % -- isscalar, isvector not in MatLab R13
    if (max(size(seed))>1)
        error('Seed value must be a scalar.')
    else
        % ---- Set state of randn. 
        randn('state',seed);
    end
end

% ---- Number of data points, frequency spacing.
N = round(fs*T);

% ---- Time at each sample, starting from zero.
t = [0:N-1]'/fs;

% ---- If user has requested white noise, make that and exit.
if ischar(DET) && strcmp(lower(DET),'white')
    x = randn(N,1);
    return
end

% ---- Make vector of positive frequencies up to Nyquist.
f = [1/T:1/T:fs/2]'; 

% ---- Get one-sided SRD (science requirements design) power spectrum (PSD)
%      and minimum frequency at which that design is valid. 
if ~ischar(DET)
  PSD = DET(:);
  fstop = [1/T fs/2];
else
  % ---- This call will extend beyond frequency range over which most SRD
  %      spectra are valid. Those "bad" frequencies will be addressed
  %      explicitly below, so shut off warning here.
  warning off
  [PSD, fstop] = SRD(DET,f);
  warning on
end

% ---- Print warning if requested frequencies go below fstop.
if (fmin<fstop(1))
    warning(['Truncating data at ' num2str(fstop(1)) ' Hz.']);
end
% ---- Print warning if requested frequencies go below fstop.
if (fmax>fstop(2))
    warning(['Truncating data at ' num2str(fstop(2)) ' Hz.']);
end

% ---- Convert to two-sided power spectrum.
PSD = PSD/2;

% ---- Force noise spectrum to go to zero smoothly outside desired band.
k = find(f<fmin|f<fstop(1));
if (length(k)>0)
    PSD(k) = PSD(k(end)+1).*(f(k)/f(k(end))).^2;
end
k = find(f>fmax|f>fstop(2));
if (length(k)>0)
    PSD(k) = PSD(k(1)-1)*2./(1+(f(k)/f(k(1)-1)).^2);
end

% ---- Make white Gaussian random noise in frequency domain, at positive
%      frequencies (real and imaginary parts).
reXp = randn(length(f),1);
imXp = randn(length(f),1);

% ---- Color noise by desired amplitude noise spectrum.
Xp = ((T*PSD).^0.5).*(reXp + i*imXp)/2^0.5; 

% ---- Make noise at DC and negative frequencies, pack into vector in usual
%      screwy FFT order: 
%         vector element:   [ 1  2  ...  N/2-1  N/2    N/2+1            N/2+2   ... N-1  N ]
%         frequency (df):   [ 0  1  ...  N/2-2  N/2-1  (N/2 or -N/2)   -N/2+1  ... -2   -1 ]
%         F = [ 0:N/2 , -N/2+1:-1 ]'*df;
X = [ 0; Xp; conj(Xp(end-1:-1:1))];

% ---- Inverse fft back to time domain, casting off small imaginary part
%      (from roundoff error).
x = real(ifft(fs*X));

% ---- Done
return
