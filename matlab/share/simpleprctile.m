function y = simpleprctile(x,p)
%SIMPLEPRCTILE Optimization of default prctile function in matlab
%   Y = PRCTILE(X,P) returns percentiles of the values in X.  P is a scalar,
%   X is a vector.    

error(nargchk(2,2,nargin))

if ~isscalar(p) 
    error('P must be a scalar');
elseif any(p < 0 | p > 100) || ~isreal(p)
    error('P must take real values between 0 and 100');
elseif ~isvector(x)
    error('X bust be a row or column vector');
end

% -- Reformat x to be a column vector
x = x(:);
n = length(x);

% If X is empty, return all NaNs.
if isempty(x)
  y = nan;
else
  if any(isnan(x))
    error('Some of the X values are NaNs')
  end
  
  if not(issorted(x))
      x = sort(x);
  end
  nearest = p/100*n+0.5;
  index = [floor(nearest) ceil(nearest)];
  index = min(n,max(1,index));
  xx = x(index);
  if numel(unique(index)) == 1
    y = xx(1);
  else
    q = (index-0.5)/n*100;
    y = interp1q(q,xx,p);
  end

end
