function [D] = distanceLowerLimit(EGW_frac,f0,hrss)
    % ---- Helper function to calc distance lower limit from hrss UL.

    % ---- Physical constants in SI units.
    G = 6.67300e-11;   % m3 kg-1 s-2.
    c = 299792458;     % m s-1.
    Msol = 1.98892e30; % kilograms.
    Mpc = 3.08568025e22; % m.

    % ---- Energy of GWs in Joules.
    EGW = EGW_frac * Msol * c^2;  % Joules.

    % ---- Distance lower limit in Mpc.
    D = (1/Mpc) * ( (G * EGW) ./ ( (pi^2) * (c^3) * (f0^2) * (hrss.^2)) ).^0.5;
end

