 function [likelihoodTimeFrequencyMap, skyPositionIndex, ...
    detectorSpectrogram, internalAngleIndex] = ...
    xsphericalTFmap(channelNames, conditionedData, sampleFrequency, ...
        amplitudeSpectra, skyPositions, integrationLength, offsetLength, ...
        transientLength, frequencyBand, likelihoodType, verboseFlag, ...
        windowType,outputClusters)
% XTIMEFREQUENCYMAP Construct time-frequency map of the likelihood ratio
% for GWB signal hypothesis vs. null hypothesis, maximized over signal
% parameters.
% 
% XTIMEFREQUENCYMAP computes the likelihood ratio (maximized over signal
% waveforms) at each time-frequency pixel for a requested set of sky
% positions.  This likelihood is, roughly speaking, the natural logarithm
% of the ratio of the probability of the observed data being due to a GWB
% plus stationary Gaussian background noise to the probability of the data
% being due to stationary Gaussian background noise only. The likelihood at
% each time-frequency pixel in the returned map is the likelihood measured
% for that pixel for the sky position (and any other parameters) that gave
% the largest total likelihood for that time, summed over all frequency
% bins. 
%
% Usage:
% 
% [likelihoodMap, skyPositionIndex, detectorSpectrogram, internalAngleIndex] = ...
%     xtimefrequencymap(channelNames, conditionedData, sampleFrequency, ...
%         amplitudeSpectra, skyPositions, integrationLength, offsetLength, ...
%         transientLength, frequencyBand, likelihoodType, verboseFlag, ...
%         windowType);
%
%    channelNames         Cell array of channel name strings.
%    conditionedData      Matrix of time-domain conditioned data, with one
%                         column per channel (detector).
%    sampleFrequency      Scalar.  Sampling frequency of conditioned data
%                         [Hz].
%    amplitudeSpectra     Matrix of amplitude spectral densities
%                         [1/sqrt(Hz)], with one column per channel.
%    skyPositions         Matrix of sky positions [radians] to be tested.
%                         First column is polar angle and second column is
%                         azimuthal angle, both in radians in Earth-based
%                         coordinates.
%    integrationLength    Scalar.  Number of samples of data to use for each
%                         Fourier transform.  Must be a power of 2.
%    offsetLength         Scalar.  Number of samples between start of 
%                         consecutive FFTs.  Must be a power of 2.
%    transientLength      Scalar.  Duration of conditioning transients, in
%                         samples.  This is the number of samples at the
%                         beginning and end (rows of conditionedData) to
%                         ignore when computing likelihood maps.
%    frequencyBand        Two-component vector.  Elements are minimum and 
%                         maximum frequencies (Hz) to include in the
%                         time-frequency maps.
%    likelihoodType       String or cell array specifying types of 
%                         likelihoods to compute.  Recognized values are
%                         'elliptic', 'hardconstraint', 'incoherentenergy',
%                         'nullenergy', 'softconstraint', 'standard',
%                         'totalenergy', 'unbiasedsoft', 'bayesian',
%                         'glitch', 'bg'
%    verboseFlag          Boolean flag to control status output (default
%                         0).
%    windowType           String. One of 'none', 'bartlett', 'hann', 
%                         'modifiedhann'.
%
%    likelihoodMap        Array.  Time-frequency maps of the likelihood 
%                         ratio for each likelihood type, maximized over
%                         all signal parameters (except for the null 
%                         energy, which is minimised).
%                         size(likelihoodMap,3)=length(likelihoodType).
%    skyPositionIndex     Array.  Index in skyPositions array for which 
%                         the likelihood at the corresponding time in 
%                         likelihoodMap is computed.  
%    detectorSpectrogram  Array.  Time-frequency maps of the energy in the  
%                         individual detectors, with no time shifts.
%                         size(detectorSpectrogram,3)=length(channelNames).
%    internalAngleIndex   Array.  Index in internalAngles array (an 
%                         internal xtimefrequencymap variable) for which 
%                         the likelihood at the corresponding time in 
%                         likelihoodMap is computed.  Nonzero only for
%                         elliptic - type likelihoods.
%
% The conditioned data should be provided as a matrix of time domain data
% with each channel in a separate column.
%
% The amplitude spectral densities should be provided as a matrix of
% one-sided frequency domain data at a frequency resolution corresponding
% to the desired integration length and with each channel in a separate column.
%
% The desired sky position should be provided as a two column matrix of the
% form [theta phi], where theta is the geocentric colatitude running from 0
% at the North pole to pi at the South pole and phi is the geocentric
% longitude in Earth fixed coordinates with 0 on the prime meridian.
%
% In the present implementation, the detector site of the first channel is
% used as the reference position when performing time shifts.  Therefore,
% when comparing likelihood maps to features in the data from individual
% detectors, the appropriate time shift is the time delay between the first
% detector and the detector being considered.
%
% See also XPIPELINE, XMAPSKY, XREADDATA, XINJECTSIGNAL, XCONDITION,
% XTILESKY, and XINTERPRET.
%
% See also ComputeAntennaResponse, LoadDetectorData.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Stephen Poprocki    poprocki@caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      process command line arguments                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for sufficient command line arguments
error(nargchk(10, 13, nargin));

% ---- Default arguments
if nargin < 13
    outputClusters=0;
    if nargin < 12,
        windowType = 'modifiedhann';
        if nargin < 11,
            verboseFlag = 0;
        end
    end
end

% set column indexes of clusterArray
minTcol=1;meanTcol=2;maxTcol=3;
minFcol=4;meanFcol=5;maxFcol=6;
likelihoodColOffset = 7;

% ---- Convert likelihoodType to cell array if necessary
if ischar(likelihoodType)
    temp_likelihoodType = likelihoodType;
    clear likelihoodType
    likelihoodType{1} = temp_likelihoodType;
end
% ---- Number of likelihood types to test
numberOfLikelihoods = length(likelihoodType);
% ---- Check that each likelihoodType is a recognized type
for jLikelihoodType = 1:numberOfLikelihoods
    if (~( strcmp(likelihoodType{jLikelihoodType},'elliptic') ...
        || strcmp(likelihoodType{jLikelihoodType},'hardconstraint') ...
        || strcmp(likelihoodType{jLikelihoodType},'incoherentenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'nullenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'softconstraint') ...
        || strcmp(likelihoodType{jLikelihoodType},'standard') ...
        || strcmp(likelihoodType{jLikelihoodType},'totalenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'unbiasedsoft') ...
        || strcmp(likelihoodType{jLikelihoodType},'bayesian') ...
        || strcmp(likelihoodType{jLikelihoodType},'glitch') ...
        || strcmp(likelihoodType{jLikelihoodType},'bg') ...
        || strcmp(likelihoodType{jLikelihoodType},'dev') ...
        || strcmp(likelihoodType{jLikelihoodType},'sumcrosscorrelation') ...
        ))
        error(['likelihoodType ' likelihoodType{jLikelihoodType} ...
            ' is not a recognized type']);
    end
end

% ---- Force one dimensional cell array for channel list
channelNames = channelNames(:);

% ---- Number of detectors
numberOfChannels = length(channelNames);
if(2 ~= numberOfChannels)
   error('spherical analysis work only in the two detector case') 
end

% ---- Verify that number of data streams matches number of detectors
if size(conditionedData, 2) ~= numberOfChannels,
  error('conditioned data is inconsistent with number of detectors');
end

% ---- Block length in samples
blockLength = size(conditionedData, 1);

% ---- Verify that integration length is an integer power of two
if bitand(integrationLength, integrationLength - 1),
  error('integration length is not an integer power of two');
end

% ---- Verify that skyPositions array has at least two columns
if size(skyPositions, 2) < 2,
  error('sky positions must be a at least a two column matrix');
end
% ---- Verify that skyPositions polar coordinates are in range [0,pi].
if any((skyPositions(:, 1) < 0) | (skyPositions(:, 1) > pi)),
  error('theta outside of [0, pi]');
end
% ---- Verify that skyPositions azimutal coordinates are in range [-pi,pi)
if any((skyPositions(:, 2) < -pi) | (skyPositions(:, 2) >= pi)),
  error('phi outside of [-pi, pi)');
end

% ---- Number of sky positions
numberOfSkyPositions = size(skyPositions, 1);

% ---- Nyquist frequency
nyquistFrequency = sampleFrequency / 2;

% ---- Vector of one-sided frequencies
oneSidedFrequencies = nyquistFrequency * ( 0 : integrationLength / 2 ) / ...
                     ( integrationLength / 2 );

% ---- Frequencies in desired analysis band
frequencyIndex = find( (oneSidedFrequencies>=frequencyBand(1)) & ...
    (oneSidedFrequencies<=frequencyBand(2)) );
inbandFrequencies = oneSidedFrequencies(frequencyIndex);

numberOfFrequencyBins=size(amplitudeSpectra,1);

% ---- Truncate amplitude spectra to desired frequency band
% amplitudeSpectra = amplitudeSpectra(frequencyIndex,:);
% WARNING to truncationg done


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               partition block into overlapping segments                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Calculate the indices of the endpoints of the segments of length
%      integrationLength with no overlapping.  (Overlapping will be imposed 
%      later when FFTing.)
segmentIndices = chunkIndices(blockLength, integrationLength, ...
    transientLength, integrationLength);

% ---- Number of segments
numberOfSegments = size(segmentIndices, 1);

% ---- Fractional offset of consecutive segments
offsetFraction = offsetLength/integrationLength;

% ---- Number of time bins in time-frequency maps.
%      NOTE THAT WE DO NOT ANALYSE THE LAST ELEMENT; this is convenient for
%      handling overlapping.  The 'max' test covers the case in which there 
%      is only one segment.
numberOfTimeBins = max(numberOfSegments-1,1)/offsetFraction;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              choose "internal angle" values to test                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Loop over unknown parameters: relative amplitude 
%      (-1<=eta<=1) and polarisation (0<=psi<pi/2).
%      internalAngles = [X(:) Y(:)]; % (psi,eta)
%
% ---- Use very coarse set of 10 points for computational speed.   
%      This set has been tested for overlap with elliptically polarized
%      GWBs with isotropic sky distribution, uniform cos(inclination_angle)
%      distribution, and uniform polarization angle distribution.  It was
%      found to have a mean SNR^2 overlap of 0.9317+/-0.0002.  The best
%      set of 10 found to date has a mean overlap of approximately 0.976.
% internalAngles = [ ...
%     0       1   ; ...
%     0       0.5 ; ...
%     0.5     0.5 ; ...
%     0       0   ; ...
%     0.25    0  ; ...
%     0.5     0  ; ...
%     0.75    0  ; ...
%     0       -0.5 ; ...
%     0.5     -0.5 ; ...
%     0       -1 ...
%     ];
% internalAngles(:,1) = internalAngles(:,1) * pi/2;
%
% ---- Use quasi-regular grid of 32 points in (psi, eta) space.  This set 
%      has been tested for overlap with elliptically polarized GWBs with 
%      isotropic sky distribution, uniform cos(inclination_angle)
%      distribution, and uniform polarization angle distribution.  It was
%      found to have a mean SNR^2 overlap of 0.99098+/-0.00002.  The best
%      set of 32 found to date has a mean overlap of approximately 0.993.
internalAngles = [];
IA_Neta = 10;
IA_maxNumberOfPsi = 5; 
IA_eta = -1:2/(IA_Neta-1):1;
IA_theta = acos(IA_eta);
IA_numberOfPsi = floor(IA_maxNumberOfPsi * sin(IA_theta));
for j=1:length(IA_eta)
    if (IA_numberOfPsi(j)>0)
        IA_dpsi = (pi/2) / IA_numberOfPsi(j);
        IA_psi = [0:IA_dpsi:pi/2]';
        IA_psi(end) = [];  % psi=pi/2 is redundant
        internalAngles = [internalAngles; IA_psi , IA_eta(j)*ones(length(IA_psi),1) ];
    else
        internalAngles = [internalAngles; 0 , IA_eta(j) ];
    end
end
%
numberOfInternalAngles = size(internalAngles,1);

% ---- Pre-compute coefficients in elliptic projection operator.
%      Elliptic projection operator is conj(F)/norm(F) where 
%          conj(F) = (\cos(2\psi)+i\eta\sin(2\psi)) F_+ + (\sin(2\psi)-i\eta\cos(2\psi)) F_x
%                  = ( complex1 ) F_+ + ( complex2 ) F_x
%      where 
% complex1 = cos(2*internalAngles(:,1)) + sqrt(-1)*sin(2*internalAngles(:,1)) .* internalAngles(:,2);
% complex2 = sin(2*internalAngles(:,1)) - sqrt(-1)*cos(2*internalAngles(:,1)) .* internalAngles(:,2);
%      The normalization is 
%          norm(F)^2 = (\cos(2\psi)^2+\eta^2\sin(2\psi)^2) F_+^2 
%              + (\sin(2\psi)^2+\eta^2\cos(2\psi)^2) F_x^2
%              = ( complex1 ) F_+^2 + ( complex2 ) F_x^2
%      where 
% const1 = cos(2*internalAngles(:,1)).^2 + (sin(2*internalAngles(:,1)) .* internalAngles(:,2)).^2;
% const2 = sin(2*internalAngles(:,1)).^2 + (cos(2*internalAngles(:,1)) .* internalAngles(:,2)).^2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       preload detector information                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Loop over detectors.
for iDet = 1:numberOfChannels
    % ---- The detector is matched by the first character of the channel name.
    detData = LoadDetectorData(channelNames{iDet}(1));
    % ---- Extract the position vector
    rDet(iDet,:) = detData.V';
    % ---- Extract the detector response tensors
    dDet{iDet}   = detData.d;
end

% ---- Check for aligned network (i.e., check to see if network consists
%      solely of H1 and H2).
alignedNetwork = 0;
if (numberOfChannels==2) 
    if (strcmp(channelNames{1}(1:2),'H1') && strcmp(channelNames{2}(1:2),'H2')) || ...
       (strcmp(channelNames{1}(1:2),'H2') && strcmp(channelNames{2}(1:2),'H1'))
        alignedNetwork = 1;
        error('spherical analysis do not work in the aligned case yet')
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             precompute detector responses, time delays                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Compute antenna responses and time delays for each detector and sky
%      position.  Arrays are size numberOfSkyPositions x numberOfChannels.
[Fp, Fc] = antennaPatterns(dDet, skyPositions);
timeShifts = computeTimeShifts(rDet, skyPositions);

% ---- RESET REFERENCE POSITION TO FIRST DETECTOR
timeShifts = timeShifts - (repmat(timeShifts(:,1),[1 size(timeShifts,2)]) + repmat(timeShifts(:,2),[1 size(timeShifts,2)]))/2 ;
timeShiftLengths = timeShifts * sampleFrequency;
integerTimeShiftLengths = round(timeShiftLengths);
% residualTimeShiftLengths = timeShiftLengths - integerTimeShiftLengths;
residualTimeShifts = (timeShiftLengths - integerTimeShiftLengths)/sampleFrequency;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       assign storage                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Storage for likelihood time-frequency maps and other output.
skyPositionIndex = zeros(1, numberOfTimeBins, numberOfLikelihoods);
internalAngleIndex = zeros(1, numberOfTimeBins, numberOfLikelihoods);
% internalAngleIndex = zeros(1, numberOfTimeBins);
loudestCluster = zeros(numberOfSkyPositions,likelihoodColOffset+ numberOfLikelihoods);
% ---- Assign storage for time-frequency maps of single-detector data.
timeFrequencyMapFull = cell(numberOfChannels,1); % contains all frequencies
dataFull = cell(numberOfChannels,1); % contains all frequencies
timeFrequencyMap = cell(numberOfChannels,1); % contains only non-negative frequencies
detectorSpectrogram = zeros(integrationLength,numberOfTimeBins,numberOfChannels);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             initialize time-frequency maps at zero delay                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- FFT the entire data stream to be analysed for each detector.
%      Note that we drop the last segment to make overlapping easier.  (If
%      there is only one segment it is not dropped!)
%      Start and stop indices for the FFT are the start and stop indices of
%      the first and last segments to be analysed:
segmentStartIndex = segmentIndices(1, 1);
segmentStopIndex = segmentIndices(max(numberOfSegments-1,1), 2);

% ---- Construct window.
switch windowType
    case 'none'
        windowData = ones(integrationLength, 1);
    case 'bartlett'
        windowData = bartlett(integrationLength);
    case 'hann'
        windowData = hann(integrationLength);
    case 'modifiedhann'
        windowData = modifiedhann(integrationLength);
    otherwise
        disp('Unknown windowType. Using none.');
        windowData = ones(integrationLength, 1);
end
% ---- Rescale window to have unity mean square.
windowMeanSquare = mean(windowData.^2);
windowData = windowData / windowMeanSquare^0.5;

meanFpDPoverMpp11=zeros(numberOfFrequencyBins,1);
meanFpDPoverMpp12=zeros(numberOfFrequencyBins,1);
meanFpDPoverMpp22=zeros(numberOfFrequencyBins,1);
for skyPositionNumber = 1 : numberOfSkyPositions
    % ---- Whitened responses for each detector, frequency x detector.
    wFp = repmat(Fp(skyPositionNumber,:),[numberOfFrequencyBins 1]) ./ amplitudeSpectra;
    wFc = repmat(Fc(skyPositionNumber,:),[numberOfFrequencyBins 1]) ./ amplitudeSpectra;

    % ---- Convert to dominant polarization (DP) frame.
    [wFpDP, wFcDP, psiDP] = convertToDominantPolarizationFrame(wFp,wFc);

    % ---- Matrix M_AB components.  These are the dot products of wFp, with
    %      themselves and each other, for each frequency, computed in the
    %      DP frame.
    Mpp = sum(wFpDP.*wFpDP,2);
    Mcc = sum(wFcDP.*wFcDP,2);
    meanFpDPoverMpp11 = meanFpDPoverMpp11 + wFpDP(:,1).*wFpDP(:,1)./Mpp;
    meanFpDPoverMpp12 = meanFpDPoverMpp12 + wFpDP(:,1).*wFpDP(:,2)./Mpp;
    meanFpDPoverMpp22 = meanFpDPoverMpp22 + wFpDP(:,2).*wFpDP(:,2)./Mpp;
end
meanFpDPoverMpp11 = meanFpDPoverMpp11/numberOfSkyPositions
meanFpDPoverMpp12 = meanFpDPoverMpp12/numberOfSkyPositions
meanFpDPoverMpp22 = meanFpDPoverMpp22/numberOfSkyPositions

segmentStartIndex = segmentIndices(1, 1) ;
segmentStopIndex = segmentIndices(max(numberOfSegments-1,1), 2) ;

if(offsetFraction ~= 1.0)
   error('offset fraction must be 1 for spherical analysis'); 
end


if(numberOfChannels ~= 2)
   error(['spherical analysis works in the 2 detector case only, you suplied '...
       num2str(numberOfChannels) ' detectors']) 
end





[likelihoodTimeFrequencyMap oneDlength]= matlabcross(conditionedData,windowData, ...
    integrationLength,numberOfTimeBins,segmentStartIndex,min(frequencyIndex),max(frequencyIndex),...
    meanFpDPoverMpp11,meanFpDPoverMpp12,meanFpDPoverMpp22,rDet');



likelihoodTimeFrequencyMap = reshape(likelihoodTimeFrequencyMap,integrationLength,numberOfTimeBins,oneDlength);

%save llDump.mat likelihoodTimeFrequencyMap
likelihoodTimeFrequencyMap = likelihoodTimeFrequencyMap(frequencyIndex,:,:);
likelihoodTimeFrequencyMap = squeeze(sqrt(sum(abs(likelihoodTimeFrequencyMap).^2,3)));
%likelihoodTimeFrequencyMap = reshape(likelihoodTimeFrequencyMap,integrationLength,numberOfTimeBins);
%likelihoodTimeFrequencyMap = sqrt(likelihoodTimeFrequencyMap(frequencyIndex,:));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          return to calling function                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% save condData.mat conditionedData


% ---- Return to calling function
return;
