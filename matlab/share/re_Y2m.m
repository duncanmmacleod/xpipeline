function rey = re_Y2m(m,theta,phi)
% RE_Y2M: Real part of the l=2 spin-2 weighted spherical harmonics.
%
% use:
%
%   rey = re_Y2m(m,theta,phi)
%
% m      Scalar. Spherical harmonic "m" value.  Must be -2, -1, 0, 2, or 2.
% theta  Array. Polar angle [rad] at which to evaluate harmonic.
% phi    Array. Azimuthal angle [rad] at which to evaluate harmonic.
%
% Calculates the real part of the l=2 spin-2 weighted spherical harmonics,
% for the specified m and (theta, phi) angles.  Theta and phi must have the
% same size.  See one of many references for s=-2 harmonics; e.g. 
% http://arxiv.org/abs/0709.0093v3 

% ---- Check input arguments.
error(nargchk(3, 3, nargin));
if numel(m) > 1
    error('Input m must be a scalar equal.');
end
if size(theta) ~= size(phi)
    error('Inputs theta and phi must have the same size');
end

% ---- Compute output.
if m < -2 || m > 2
	error('m must be one of -2, -1, 0, 1, 2.');
elseif m == 0
        rey = sqrt(15.0/32.0/pi) .* sin(theta).^2;
elseif m == 1
        rey = sqrt(5.0/16.0/pi) .* (sin(theta) + (1.0+cos(theta))) .* cos(phi);
elseif m == 2
        rey = sqrt(5.0/64.0/pi) .* (1.0+cos(theta)).^2 .* cos(2.0*phi);
elseif m == -1
        rey = sqrt(5.0/16.0/pi) .* (sin(theta) + (1.0-cos(theta))) .* cos(phi);
elseif m == -2
        rey = sqrt(5.0/64.0/pi) .* (1.0-cos(theta)).^2 .* cos(2.0*phi);
else
	error('something went very wrong, check the function'); 
end

% -- Return to calling function   
return
