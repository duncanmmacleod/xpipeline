function c = circulartimeslides(Ndet,Nstep)
% Compute all allowed circular time slides for Ndet detectors with Nstep steps.
%
% CIRCULARTIMESLIDES returns all allowed circular time slide for a given number
% of detectors with a given maximum allowed number of steps. It works for any
% number of detectors.  
%
% usage:
%
%   c = circulartimeslides(Ndet,Nstep)
%
%  Ndet     Natural number. Number of detectors. 
%  Nstep    Natural number. Maximum number of steps between any pair of
%           detectors. For example, for steps of size 2 sec on a 100 sec
%           interval of data, Nstep = 50.
%
%  c        Array of whole numbers. Each row is a unique set of circular time 
%           slides for each detector. 
%
% The slides are guaranteed to be unique in the sense that the relative delay
% between each detector pair is never repeated for that pair. The total number
% of slides returned is 
%
%      floor((Nstep-1)/M)*M+1
%
% where M is the largest multiple of N such that M+1 <= Nstep and N is the
% lowest common multiple of 1:(Ndet-1). For example, for Ndet = 4 and Nstep =
% 50 there are 49 unique slides.
%
% $Id$

% ---- Check inputs.
if Ndet~=round(Ndet) || Ndet<1
    error(['Number of detectors (' num2str(Ndet) ') must be a natural number.'])
end
if (Nstep~=round(Nstep)) || (Nstep<1)
    error('Input Nstep must be a natural number.')
end

% ---- Calculation depends on number of detectors.
if Ndet==1
    c = 0;
else
    % ---- Reset Nstep to be 1 + X where X is the largest multiple of the lowest
    %      common multiple of 1:(Ndet-1) such that (X+1) <= (original NStep).
    M = 1;
    for ii=1:(Ndet-1)
        M = lcm(M,ii);
    end
    Nstep = floor((Nstep-1)/M)*M+1;

    % ---- Circular time steps.
    c = mod([0:(Nstep-1)]' * [0:(Ndet-1)],Nstep);
end


% ------------------------------------------------------------------------------
%    Test code.
% ------------------------------------------------------------------------------

% ---- The following code generates circular time slides up to Nstep = MaxTrial
%      for Ndet detectors and issues a warning if the number of time slides is
%      incorrect or if any delay between a given pair of detectors is repeated
%      for those detectors.
% 
% Ndet = 5;
% MaxTrial = 16384;
% 
% M = 1;
% for ii=1:(Ndet-1)
%     M = lcm(M,ii);
% end
% disp(['Lowest common multiple: ' num2str(M) ])
% 
% for ii = 1:MaxTrial
%     c = circulartimeslides(Ndet,ii);
%     N = size(c,1);
%     if N~=M*floor((ii-1)/M)+1
%         warning(['Unexpected number of slides for ii = ' num2srtr(ii)])
%     end
%     for jj = 1:(Ndet-1)
%         for kk = (jj+1):Ndet
%             N1 = length(unique(c(:,jj)-c(:,kk)));
%             if N1~=N
%                 warning(['Error for ii = ' num2srtr(ii)])
%             end
%         end
%     end
% end


