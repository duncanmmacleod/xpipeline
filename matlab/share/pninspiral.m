function [hp, hx, t, t_insp, phi, amp, freq, tplunge, fisco] = ...
    pninspiral(mass1,mass2,iota,distance,fs,ttotal,tc)
% PNINSPIRAL - Generate Post-Newtonian inspiral waveform.
%
% This function generates a post-Newtonian inspiral waveform.  It is
% quadrupole in amplitude, but higher-order in phase.  The waveform cuts
% off abruptly (no smoothing) at the earlier of the coalescence time or the
% time that the frequency derivative becomes negative.
%
% usage:
%
% [hp, hx, t, t_insp, phi, amp, freq, tplunge, fisco] = ...
%    pninspiral(mass1,mass2,iota,distance,fs,ttotal,tc)
%
% mass1 	component mass 1 [solar masses]
% mass2     component mass 2 [solar masses]
% iota      inclination angle [rad]
% distance	distance [Mpc]
% fs        sampling rate [s^-1] 
% ttotal    waveform duration [s] 
% tc        coalescence time [s]
%
% credits: This function is adapted from "inspmergring.m", by an unknown
% author.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Hardwired constants.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

G = 6.673*1e-11;            %-- the gravitational constant in m^3/(kg*s^2)
hplanck = 6.626068e-34;     %-- Planck's constant in m^2*kg/s
c = 299792458;              %-- the speed of light in m/s
pc = 3.0856775807*1e16;     %-- a parsec in m
Mpc = pc*1e6;               %-- a mega-parsec in m
ms = 1.98892*1e30*G/c^3;    %-- a solar mass in s


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Derived parameters.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Mass of the two objects [s]
m1 = mass1*ms;
m2 = mass2*ms;
% ---- Total mass of the system [s].
M = m1+m2;
% ---- Reduced mass of the system [s].
mu = m1*m2/M;
% ---- Mass ratio.
eta = mu/M;
% ---- Chirp mass.
Mchirp = mu^(3/5) * M^(2/5);
% % ---- Quality factor of the BH ringdown.
% Q = 2*(1-a)^(-9/20);
% ---- Distance from the source to the detector [s].
r = distance*Mpc/c;  

% ---- Time before collapse when separation reaches that of a test particle
%      in the innermost stable circular orbit of Schwarzschild mass M. 
tplunge = (405*M/(16*eta));
% ---- ISCO frequency?
fisco = 1/(6^(3/2)*pi*M);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Times and frequencies.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Waveform duration in samples.
N = floor(ttotal*fs);
% ---- Sample times.
t = [0:N-1].' / fs;

% ---- Assign storage for output waveforms.
hp = zeros(size(t));
hx = zeros(size(t));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Construct inspiral.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Time samples of inspiral portion of waveform.
index = find(t<tc);  %-- don't want to include t=tc since things diverge.
t_insp = t(index);

% ---- Rescaled time parameter.
tau = (eta/(5*M)*(tc-t_insp)).^(-1/8);

% ---- GW phase.
Phi0 = 0;  %-- phase at coalescence time
phic0 = -2/eta.*tau.^(-5);
phic2 = -2/eta*(3715/8064+55*eta/96).*tau.^(-3);
phic3 = -2/eta*(-3*pi/4).*tau.^(-2);
phic4 = -2/eta*(9275495/14450688+284875*eta/258048+1855*eta^2/2048).*tau.^(-1);
% phic5 = -2/eta*(38645/21504+15*nu/256)*pi*ln(tau/tau0);
% phig = 8*pi^2*r*M/(lambdag^2).*tau.^(-3);
phi = Phi0 + phic0+phic2+phic3+phic4;
freq = [diff(phi);0]/(2*pi/fs);

% ---- Check for non-monotonic frequency evolution and cut off waveform
%      sharply there.
badEvoln = find(diff(freq)<0);
if ~isempty(badEvoln)
    index(badEvoln+1:end) = [];
    t_insp(badEvoln+1:end) = [];
    tau(badEvoln+1:end) = [];
    phi(badEvoln+1:end) = [];
    freq(badEvoln+1:end) = [];
end

% ---- Inspiral GW amplitude. 
amp = 2*mu*tau.^2/(4*r);

% ---- Construct plus and cross waveforms.
hp(index) = amp * (1+cos(iota)^2) .* cos(phi);   
hx(index) = 2 * amp *cos(iota) .* sin(phi);   

% ---- Done.
return
