function [data, hpfSOS, hpfResponse] = xhpf(data, sampleFrequency, hpfCutoffFrequency, hpfOrder)
% XHPF high pass filter time series data.
%
% XHPF high pass filters time series data with zero-phase high pass filtering.
% Code copied from XCONDITION@5849.
%
% usage:
%
%   [fdata,hpfSOS,hpfResponse] = xhpf(data, sampleFrequency, hpfCutoffFrequency, hpfOrder);
%
%   data                 Vector of input time series data.
%   sampleFrequency      Scalar. Sampling rate [Hz] of the data. 
%   hpfCutoffFrequency   Scalar. Cutoff frequency [Hz] of high-pass filter to be
%                        applied to the data. 
%   hpfOrder             Optional scalar. Order of the high-pass filter to be
%                        applied to the data. Default 6.
%
%   fdata                Column vector of high-pass-filtered time-domain data.  
%   hpfSOS               High-pass filter as applied to the data by SOSFILTFILT.
%   hpfResponse          Column vector. Magnitude of the impulse response
%                        (output of filtering a unit impulse with the HPF), in
%                        the frequency domain. Non-negative frequencies only.
%                        Note: length(hpfResponse)=length(data)/2+1. 
%
% The filter is made using BUTTER, converted from zeros and poles to SOS using
% ZP2SOS, and applied to the data using SOSFILTFILT.
%   
% $Id$


% ------------------------------------------------------------------------------
%    process and validate command line arguments
% ------------------------------------------------------------------------------

% ---- Check for sufficient command line arguments and assign defaults.
narginchk(3, 4);
if nargin < 4
    % ---- High pass filter order.
    hpfOrder = 6;
end

% ---- Force one dimensional (column vector) data array.
data = data(:);

% ---- Determine Nyquist frequency of analysis.
nyquistFrequency = sampleFrequency / 2;
if (hpfCutoffFrequency > nyquistFrequency)
    error('high pass filter cutoff frequency exceeds Nyquist frequency');
end


% ------------------------------------------------------------------------------
%    high pass filter
% ------------------------------------------------------------------------------

% ---- Hard-coded high-pass filter.  It is applied forwards and backwards
%      to minimize phase distortion.  With these parameters, the magnitude
%      of the impulse response falls below 1e-3 in about 30 ms.  So, data
%      corruption at the edges of the data segment is minimal. 

% ---- Design high pass filter.
[hpfZeros, hpfPoles, hpfGain] = butter(hpfOrder, hpfCutoffFrequency/nyquistFrequency, 'high');
hpfSOS = zp2sos(hpfZeros, hpfPoles, hpfGain);

% ---- Apply high pass filter.
data = sosfiltfilt(hpfSOS, data);


% ------------------------------------------------------------------------------
%    Estimate frequency response of high-pass filter.
% ------------------------------------------------------------------------------

if nargout>2
    % ---- Estimate frequency response of high-pass filter, so we can account
    %      for it in the PSD estimation.  We estimate the response by filtering
    %      a unit impulse, then FFTing the filtered data.  Use each of the 
    %      frequency resolutions of the output amplitudeSpectra. 
    impulseData = zeros(size(data));
    impulseData(end/2) = 1;
    impulseData = sosfiltfilt(hpfSOS, impulseData);
    hpfResponse = abs(fft(impulseData));
    % ---- Keep non-negative frequencies only.
    hpfResponse = hpfResponse(1:end/2+1);
end


% ---- Done.
return


