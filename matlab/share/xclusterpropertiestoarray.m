function clusterArray = xclusterpropertiestoarray(boundingBox, centralTime, ...
    centralFrequency, nPixels, likelihood);
%
% xclusterpropertiestoarray - Convert a set of separate event cluster variables 
% into a single 2-D array.
%
% usage:
%
% clusterArray = xclusterpropertiestoarray(boundingBox, centralTime, ...
%     centralFrequency, nPixels, likelihood);
%
% boundingBox       Array with columns
%                     column 1: minimum time of cluster
%                     column 2: minimum frequency of cluster
%                     column 3: duration of cluster
%                     column 4: bandwidth of cluster
% centralTime       Vector of weighted central time of clusters.
% centralFrequency  Vector of weighted central frequency of clusters.
% nPixels           Vector of number of time-frequency pixels in each cluster.
% likelihood        Array of likelihood values for each cluster.
%
% clusterArray      Array of event clusters as produced by clusterTFmapNew.
%
% See clusterTFmapNew.
%
% $Id$

% ---- Hardcoded parameters reflecting ouput from clusterTFmapNew.
% ---- Number of columns in output from clusterTFmapNew.
minTcol = 1; meanTcol = 2; maxTcol = 3;
minFcol = 4; meanFcol = 5; maxFcol = 6;
areaCol = 7;
% ---- These first N columns are not likelihoods.
likelihoodColOffset = 7; 

% ---- Argument checks.
narginchk(5, 5);
numberOfEvents = size(boundingBox,1);
if (numberOfEvents ~= size(centralTime,1) | ...
    numberOfEvents ~= size(centralFrequency,1) | ...
    numberOfEvents ~= size(nPixels,1) | ...
    numberOfEvents ~= size(likelihood,1))
    error('Input arrays must have the same number of rows.');
end
if (size(boundingBox,2)~=4)
    error('boundingBox must have 4 columns.');
end
if (size(centralTime,2)~=1)
    error('centralTime must have 1 column.');
end
if (size(centralFrequency,2)~=1)
    error('centralFrequency must have 1 column.');
end
if (size(nPixels,2)~=1)
    error('nPixels must have 1 column.');
end

% ---- Combine separate input arrays into single output array.
clusterArray = zeros(numberOfEvents,7+size(likelihood,2));
% ---- Bounding box.
clusterArray(:,minTcol) = boundingBox(:,1);
clusterArray(:,minFcol) = boundingBox(:,2);
clusterArray(:,maxTcol) = clusterArray(:,minTcol) + boundingBox(:,3);
clusterArray(:,maxFcol) = clusterArray(:,minFcol) + boundingBox(:,4);
% ---- Central time.
clusterArray(:,meanTcol) = centralTime;
% ---- Central frequency.
clusterArray(:,meanFcol) = centralFrequency;
% ---- Number of time-frequency pixels.
clusterArray(:,areaCol) = nPixels;
% ---- Likelihoods.
clusterArray(:,likelihoodColOffset+1:end) = likelihood;

% ---- Done.
return


