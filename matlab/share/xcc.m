function [cc_val, idx_H, idx_L, normH, normL, other] = xcc(data,fs,transientTime, ...
    coincidenceWindow,correlationTime,offsetFactor,windowType,injectionPeakIndex)
% XCC - Cross-correlate two data streams with large time offset for transient detection.
%
% usage:
%   [cc_val, idx1, idx2, norm1, norm2, other] = xcc(data,sampleRate,transientTime, ...
%       coincidenceWindow,correlationTime,offsetFactor,windowType,injPeakIndex)
%
% data              Matrix of time-domain conditioned data, with two
%                   columns (one per data stream).
% sampleRate        Scalar. Sampling rate of data [Hz].
% transientTime     Scalar.  Duration of conditioning transients [s]. This
%                   is the duration at the beginning and end (rows of data)
%                   to ignore when computing correlations.
% coincidenceWindow Scalar. Maximum delay over which to compute correlations [s].
% correlationTime   Scalar. Duration of each correlation measurement [s].
% offsetFactor      Scalar. Fractional offset of (N+1)th correlation from
%                   (N)th; i.e., overlapping of consecutive correlation measurements.
% windowType        String. Window type for FFTs. Muyst be one of 'hann' or
%                   'box' (square/no window).
% injPeakIndex      Optional scalar. Sample index at which the injection
%                   (if any) peaks in the SECOND channel (data(:,2)). If
%                   supplied then only correlation intervals containing
%                   this sample will be processed. Default [] - all data
%                   analysed. 
%
% cc_val            Vector. Un-normalised cross-correlation measurements.
% idx1              Vector. Index in data(:,1) at which corresponding
%                   cc_val maximum correlation is measured. 
% idx2              Vector. Index in data(:,2) at which corresponding
%                   cc_val maximum correlation is measured. 
% norm1, norm2, 	Vectors. Norm of data(:,1), data(:2) where corresponding
%                   cc_val maximum correlation is measured.
% other             Matrix of diagnistic data, with three columns: 
%                     max_idx
%                     offset
%                     idx(1)
%
% ADD DEFINITIONS AND SIZES OF OUTPUTS AND SUMMARISE ALGORITHM.
%
% Only data in the interval [(start+transientTime+coincidenceWindow),
% (end-transientTime-coincidenceWindow)] is fully analysed for
% correlations.
%
%  $Id$

% -------------------------------------------------------------------------

% ---- Check number of input arguments.
narginchk(7,8);

% ---- Assign default arguments.
injectionRun = false;
if nargin < 8
    injectionPeakIndex = [];
end
if ~isempty(injectionPeakIndex)
    injectionRun = true;
    injectionPeakTime = injectionPeakIndex / fs;
end

% ---- Derived parameters.

% ---- Dummy value: start time of data segment.
segStart = 0;
% ---- Duration of data segment.
segDur = size(data,1)/fs;
segStop = segStart + segDur;
%
% ---- Interval of L data that will be analysed by this job.
analysis_start = segStart + (transientTime + coincidenceWindow)  
analysis_end   = segStop  - (transientTime + coincidenceWindow)
analysis_dur   = analysis_end - analysis_start
%
% ---- FFT parameters.
Tfft = 2^ceil(log2(2*coincidenceWindow))
Nfft = fs * Tfft
%
% ---- Block parameters.
blockDur = Tfft - 2*coincidenceWindow
if blockDur==0
    error('coincidenceWindow cannot be a power of 2.');
end
Nblock   = ceil(analysis_dur/(blockDur-1));
%
% ---- Correlation parameters.
correlationLength = fs * correlationTime;
stepSize = correlationLength * offsetFactor;
Nstep = (blockDur-1)/correlationTime/offsetFactor  %-- all corrlns for blockDur-1 sec of data
%
% ---- Storage for cross-correlations. Record only largest cross correlation
%      over entire data(:,1) ('H') stretch for each correlationLength of data(:,2) ('L'). 
cc_val = zeros(Nstep*Nblock,1);
idx_H  = zeros(Nstep*Nblock,1);
idx_L  = zeros(Nstep*Nblock,1);
normH  = zeros(Nstep*Nblock,1);
normL  = zeros(Nstep*Nblock,1);
other  = zeros(Nstep*Nblock,3);

% -------------------------------------------------------------------------

% ---- Make window for FFTs.
switch windowType
    case 'hann'
        wind = hann(correlationLength);
    case 'box'
        wind = ones(correlationLength,1);
    otherwise
        error('windowType not recognised.');
end

% -------------------------------------------------------------------------

% ---- Unpack input data.
H_full = data(:,1);
L_full = data(:,2);

% -------------------------------------------------------------------------

% ---- Walk through SECOND data stream in blocks.
iblock = 1;
blockStart = analysis_start;
while (blockStart < analysis_end)
    
    % ---- If this is an injection run, skip to the next block if this one
    %      does not contain the injection. 
    if injectionRun && (injectionPeakTime<blockStart | injectionPeakTime>(blockStart+blockDur))
        % ---- Iterate to next block (with 1 sec overlap).
        iblock = iblock + 1;
        blockStart = blockStart + blockDur - 1;
        continue;
    end
    
    % ---- Extract data to be analysed.
    idx = (blockStart-coincidenceWindow-segStart)*fs + [1:Nfft]';
    H = H_full(idx);
    L = L_full(idx);

    % ---- Window and FFT the H data.
    H(1:correlationLength/2)           = H(1:correlationLength/2)           .* wind(1:correlationLength/2);
    H((end-correlationLength/2+1):end) = H((end-correlationLength/2+1):end) .* wind((correlationLength/2+1):end);
    fH = fft(H);

    % ---- Loop over the L data and compute correlations.
    for istep = 1:Nstep
        % ---- Indices of samples to be analysed, relative to L.
        offset   = fs*coincidenceWindow + (istep-1)*stepSize;
        idx2     = [(offset+1):(offset+correlationLength)]';
        %
        % ---- If this is an injection run, skip to the next step if this
        %      one does not contain the injection.        
        % ---- Indices of samples to be analysed, relative to L_full.
        L_start_idx = offset+idx(1);
        L_end_idx   = L_start_idx + correlationLength - 1;
        if injectionRun && (injectionPeakIndex<L_start_idx | injectionPeakIndex>L_end_idx)
            continue;
        end
        %
        % ---- Mask out L data outside correlation window and FFT.
        Lw       = zeros(Nfft,1); %mask;
        Lw(idx2) = L(idx2) .* wind;
        %
        % ---- Cut for speed: only compute correlations if normL > some threshold.
        normL((iblock-1)*Nstep+istep) = norm(Lw); 
        if normL < 1.0
            continue;
        end
        %
        % ---- FFT L data.
        fL = fft(Lw);
        %
        % ---- Compute cross-correlation for all time shifts.
        cc_tmp = ifft(fH .* conj(fL)); % cc_tmp = circshift(ifft(fH .* conj(fL)),offset);
        % ---- Record largest |cc| and index in original (full) H timeseries.
        [~,max_idx] = max(abs(cc_tmp));
        cc_val((iblock-1)*Nstep+istep) = cc_tmp(max_idx);
        %
        H_first_idx = offset+max_idx; %-- first index in FFT'ed data
        if H_first_idx > Nfft
            H_first_idx = H_first_idx - Nfft; %-- circular wrap around
        end
        % ---- Index of START of interval of max cross correlation in H.
        idx_H((iblock-1)*Nstep+istep) = idx(1)-1 + H_first_idx;
        H_norm_indices = (H_first_idx-1) + [1:correlationLength]';
        H_norm_indices = mod(H_norm_indices-1,Nfft)+1; %-- indices may extend past Nfft; if so wrap around to 1.
        normH((iblock-1)*Nstep+istep)  = norm(H(H_norm_indices));
        % ---- Record timing data. This is used below.        
        other((iblock-1)*Nstep+istep,1) = max_idx;
        other((iblock-1)*Nstep+istep,2) = offset;
        other((iblock-1)*Nstep+istep,3) = idx(1);
        % normL((iblock-1)*Nstep+istep)   = norm(Lw); 
    end

    % ---- Iterate to next block (with 1 sec overlap).
    iblock = iblock + 1;
    blockStart = blockStart + blockDur - 1;

end

% ---- Index of START of interval of max cross correlation in L.
idx_L = other(:,2)+other(:,3);

% -------------------------------------------------------------------------

% ---- Delete any empty rows of output. These will occur in injection runs, 
%      in which only a few of the rows are non-zero.
emptyRows = find(normH.*normL==0); 
cc_val(emptyRows)  = [];
idx_H(emptyRows)   = [];
idx_L(emptyRows)   = [];
normH(emptyRows)   = [];
normL(emptyRows)   = [];
other(emptyRows,:) = [];

% -------------------------------------------------------------------------

% ---- Not output so commented out ...
% % ---- Convert indices to times.
% % ---- Time in L corresponding to the CENTER of each correlation segment. 
% t_L_center = (idx_L + correlationLength/2) / fs;
% % ---- CENTER time in H at which the maximum correlation was measured. 
% t_H_center = (idx_H + correlationLength/2) / fs;

% -------------------------------------------------------------------------

return
