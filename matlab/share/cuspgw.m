function [h, A] = cuspgw(timeblock,toffset,samprate,hrss,f0,gwtype)
% CUSPGW - Generates a GW waveform from a cosmic string cusp or kink
%
% usage:
%
%   [h, A] = cuspgw(timeblock,toffset,samprate,hrss,f0,gwtype)
%
% timeblock   Scalar. Duration [s] of output time series.
% toffset     Scalar. Signal peak time [s] relative to start of output time series.
% samprate    Scalar. Sampling rate [Hz] of output time series.
% hrss        Scalar. RSS Signal amplitude [s^{1/2}].
% f0          Scalar. Cutoff frequency [Hz].
% gwtype      Optional string. Recognised values are 'cusp', 'kink', and
%             'kink-kink'. Default 'cusp'. Determines the nature of the GW
%             signal.
%
% h           Column vector. Strain GW signal.
% A           Scalar. Signal amplitude [s^{1-q}] (see reference [2]).
%
% The quantity q determines the spectral shape of the signal. Its value is fixed
% by the signal type:  
%   cusp:  q_c = 4/3
%   kink:  q_k = 5/3
%   kink-kink collision:  q_kk = 2
% The output signal is high-pass filtered with a cutoff frequency of 5 Hz.
%
% For backwards compatibility, the following usage is also supported:
%
%   [h, A] = cuspgw(f0)
% 
% in this case the following defaults are used: timeblock = 1, toffset = 0.5,
% samprate = 16384, hrss = 1, gwtype = 'cusp'. 
%
% References: 
% [1] T. Damour and A. Vilenkin, Prys. Rev. D 64, 064008 (2001),
% [2] R. Abbott et al., Phys. Rev. Lett. 126, 241102 (2021),
%     https://arxiv.org/abs/2101.12248 
%
% Original version written 23 Sep 2004 by Peter Shawhan.
%
% $Id$

% ---- Assign default arguments.
if nargin<6
    gwtype = 'cusp';
end
if nargin==1
    f0 = timeblock; %-- only input is f0
    timeblock = 1;
    toffset = 0.5;
    samprate = 16384;
    hrss = 1;
end

% ---- Determine power law index.
switch lower(gwtype)
    case 'cusp'
        q = 4/3;
    case 'kink'
        q = 5/3;
    case 'kink-kink'
        q = 2;
    otherwise
        error(['Input gwtype = ' gwtype ' not recognised.'])
end

% ---- Band-pass filtering parameters.
hpfreq = 5;    %-- Corner frequency of filter
hppow = 4/3;   %-- Controls strength of filter; a little artificial
lpfreq = 8000/8192*samprate/2;  %-- 8000 Hz for 16384 Hz samprate
lppow = 1;
% ---- Exponential roll-off power.
rollpow = 1;

% ---- Some derived parameters.
% ---- We actually generate a time series twice as long as needed.
ndata = samprate * timeblock * 2;
% ---- Vector of times for each data point
times = (1:ndata) / samprate;

% ---- Construct a vector of frequencies for freq-domain representation,
%      following Matlab convention.  Works for an arbitrary number of data
%      points.
npfreqs = floor((ndata-1)/2);
pfreqs = (1:npfreqs) * samprate / ndata ;
if mod(ndata,2) == 0
  nyquist = samprate / 2;
  freqs = [ 0 pfreqs nyquist -pfreqs(npfreqs:-1:1) ];
else
  freqs = [ 0 pfreqs -pfreqs(npfreqs:-1:1) ];
end

% ---- Avoid divide-by-zero in the filtering operation.
freqs(1) = 1.0e-20;

% ---- Cosmic string cusp waveform in frequency domain, with exponential
%      roll-off above the frequency cutoff f0.
hf = [ 0 abs(freqs(2:ndata)).^(-q) ];
rolloff = exp( -rollpow * ( abs(freqs)/f0 - 1 ) );
rolloff(find(rolloff>1)) = 1;
hf = hf .* rolloff;

% ---- Apply the time offset and a band-pass filter.
dataf = hf .* exp(-2*pi*i*freqs*toffset) ...
   ./ (sqrt(1+(hpfreq./abs(freqs)).^2)).^hppow ...
   ./ (sqrt(1+(abs(freqs)./lpfreq).^2)).^lppow ;

% ---- Get the time-domain waveform
data = real(ifft(dataf));

% ---- Return just the first half of it, making sure that the DC level is zero.
h = data(1:ndata/2) - data(1);

% ---- We have constructed h using A=1. Normalize to desired hrss and record
%      final A value. 
hrss0 = sqrt(sum(h.*h)/samprate);
h = h * hrss / hrss0;
A = hrss / hrss0;

% ---- Enforce column vector output.
h = h(:);

% ---- Done.
return

