function [coordinates, solidAngles, probabilities] = healpix(n)
% HEALPIX - Equal-area tiling of the sky with a specified number of points.
%
% usage:
%
%   [coordinates, solidAngles, probabilities] = healpix(n)
%
%   n               Scalar determining number of sky positions in the grid.
%                   The sky will be tiled with 12*N^2 points, where N=2^n. 
%
%   coordinates     12*N^2 x 2 array of sky coordinates [radians]
%   solidAngles     12*N^2 x 1 vector of solid angles [steradians]
%   probabilities   12*N^2 x 1 vector of a priori uniform probabilities
%
% The output coordinates are in standard Earth-fixed format, [theta,phi], 
% where theta is the geocentric colatitude running from [0, pi], with 0 at
% the North pole to pi at the South pole, and phi is the geocentric
% longitude running from (0, 2 pi] (note odd limits!), with 0 on the prime
% meridian. 
%
% Within the output array, the points are arranged as follows: 
%   z>0 : Rows 1:(3*2^(2*n+1)-2^(n+1)) contain the northern hemisphere (z>0
%     or theta<pi/2), with the highest latitudes appearing first.  Points
%     on lines of the same latitude appear consecutively with phi
%     increasing.  
%   z=0 : Rows ((3*2^(2*n+1)-2^(n+1))+1):(3*2^(2*n+1)+2^(n+1)) contain the
%     points on the equator (z=0), with phi increasing with consecutive
%     points.  
%   z<0 : Rows (3*2^(2*n+1)+2^(n+1)+1):end contain the southern hemisphere
%     (z<0).  Points on lines of the same latitude appear consecutively
%     with phi DECREASING.  With this format, the points in
%     rows M and end+1-M are mirror-images on reflection through z=0
%     (except for the z=0 points). 
%
% Note that the number of sky positions returned increases very quickly
% with n:
%                number of
%            n   positions
%           --   ---------
%            0          12
%            1          48
%            2         192
%            3         768
%            4        3072
%            5       12288
%            6       49152
%            7      196608
%            8      786432
%            9     3145728
%           10    12582912
%           
% HEALPIX also returns the approximate differential solid angle associated
% with each sky position, and its corresponding a priori uniform
% probability density.
%
% HEALPIX uses the HEALPix algorithm (http://healpix.jpl.nasa.gov/).
%
% Yoichi Aso <aso@astro.columbia.edu>
% Patrick J. Sutton <patrick.sutton@astro.cf.ac.uk>

% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   process command line arguments                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% verify correct number of input arguments
narginchk(1, 1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   healpix tiling of sky                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Initialize empty variables.
solidAngles = [];
probabilities = [];

% ---- The tiling is specified by a parameter n, which is required to be a
%      non-negative integer.  The number of pixels on the sky is 12*N^2,
%      where N=2^n.  The solid angle of each pixel is pi/(3*N^2).
% 
%      General strategy for the coding:
% 
%      In HEALPix tiling the sky is divided into four parts: the north
%      polar cap, north equatorial band, south equatorial band, and south
%      polar cap. For the definition of those regions, please refer to the
%      reference above.  Since the formula for the index-to-coordinates
%      conversion differs from region to region, we divide the indices
%      vector p into 4 parts according to the values.

% ---- Preparatory.
N = 2^n;
Npix = 12*N^2;

% ---- Prepare a sky position index vector
p=[0:Npix-1];
p=p(:);

% ---- Divide the indices p into 4 parts according to the values.

% ---- North Polar cap.
indices1=find(p<2*N*(N-1));
p1=p(indices1);  % Extract p in this region
ph=(p1+1)/2;
i=floor(sqrt(ph-sqrt(floor(ph))))+1;
j=p1+1-2*i.*(i-1);
z1 = 1-(i.^2)/(3*N^2);
phi1 = pi./(2*i).*(j-1/2);

% ---- North Equatorial belt.
indices2=find((p>=2*N*(N-1)).*(p<Npix/2));
p2=p(indices2);
ph=p2-2*N*(N-1);
i=floor(ph/(4*N))+N;
j=mod(ph, 4*N)+1;
s=mod(i-N+1, 2);
z2 = 4/3 - 2*i/(3*N);
phi2 = pi./(2*N).*(j-s/2);

% ---- South Equatorial belt.  Points are obtain by reflecting the 
%      North Equatorial belt in the z=0 (equatorial) plane.
z3 = flipud(-z2);
phi3 = flipud(phi2);
% ---- Special case: reflection doesn't work for the points that lie
%      precisely in the z=0 plane.  We need to handle these separately.
%      The north equatorial belt construction only makes 1/2 of the z=0
%      ring.  We get the other half in the south belt by altering phi3. 
% ---- Find pixels in the z=0 plane.  Test for points that are much closer
%      to z=0 than the typical linear size of a pixel.
k = find(abs(z3)<0.01*(4*pi/Npix)^0.5);
% ---- There should be exactly 2^(n+1) points in the z=0 plane.
if (length(k)~=2^(n+1))
    error('Help!  Help! I have screwed something up.');
end
if n==0
    % ---- Will have phi2=[pi/2,pi]. So, recalling that we've already
    %      applied flipud(), our desired phi3 are:
    phi3(k) = 2.5*pi - phi3(k);
else
    % ---- For n>0 the points phi2 will be of the form [0.5:1:m-0.5]*pi/m,
    %      where m is some integer.  So, recalling that we've already
    %      applied flipud(), our desired phi3 are
    phi3(k) = 2*pi - phi3(k); 
end

% ---- South Polar cap.  Obtain reflecting the North Polar cap in the z=0
%      plane. 
z4 = flipud(-z1);
phi4 = flipud(phi1);

% ---- Combine the results from each region.
z = [z1;z2;z3;z4];
theta = acos(z);
phi = [phi1;phi2;phi3;phi4];
% ---- Force phi in range [0, 2 pi).
%phi = mod(phi, 2*pi);

% ---- Final set of sky positions.
coordinates = [theta(:) phi(:)];

% ---- Solid angle (constant).
solidAngles = pi/(3*N^2)*ones(size(theta));

% ---- A priori probability.
probabilities = 1 / (4*pi) * solidAngles;

return
