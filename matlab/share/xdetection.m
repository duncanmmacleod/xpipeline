function [skyPositions, likelihoodMap, skyPositionIndex, sourcePositions, ...
    sourceLikelihoodMap, sourcePositionIndex, spectrogram] = ...
    xdetection(parameterFileName, jobNumberString, outputDirectory, ...
    injectionNumberString)
% XDETECTION Coherent network search for gravitational-wave bursts.
%
% XDETECTION searches for a direction on the sky for which the signals
% observed in a network two or more gravitational-wave detectors is
% consistent with the properties of a gravitational-wave burst.  XDETECTION
% is a generalization of the method of Gursel and Tinto, "Solution of the
% inverse problem for gravitational wave bursts", Phys. Rev D40 3884 (1989)
% to arbitrary networks of detectors with colored noise.
%
% usage:
%
% [skyPositions, likelihoodMap, skyPositionIndex, sourcePositions, ...
%     sourceLikelihoodMap, sourcePositionIndex, spectrogram] = ...
%     xdetection(parameterFileName, jobNumberString, outputDirectory, ...
%     injNumberString);
%
%  parameterFileName    String containing name of parameters file for use
%                       by pipeline.  Defaults to 'parameters.txt'.
%  jobNumberString      String containing a list of non-negative integers
%                       specifying which lines of the event list file are
%                       to be analysed.  See jobstr2num.  For compatibility
%                       with Condor, a job number of 0 corresponds to the
%                       first line in the event list file.  Defaults to
%                       '0'.
%  outputDirectory      String path to directory in which to save results.
%                       Defaults to '.'.
%  injNumberString      String containing list of non-negative integers 
%                       specifying which injections from the injection file are
%                       to be injected into the data.  This number is the
%                       row number, starting from one.  If '0' or not specified,
%                       then all injections are used. See xinjectsignal.
% 
%  skyPositions         Matrix of sky positions tested.
%                       First column is polar angle and second column is
%                       azimuthal angle, both in radians in Earth-based
%                       coordinates.
%  likelihoodMap        Array.  Time-frequency maps of the likelihood 
%                       ratio for each likelihood type, maximized over
%                       all signal parameters (minimized for null energy).
%                       (Note: The 'outputType' parameter may be set to
%                       make this variable holds event clusters rather than
%                       time-frequency maps.)
%  skyPositionIndex     Array.  Index in skyPositions array for which the
%                       summed-over-frequencies likelihood is extremized
%                       in each time-frequency likelihoodMap.  Size is
%                       1 x size(likelihoodMap,2) x size(likelihoodMap,3);
%                       i.e., one value for each time bin and likelihood
%                       type in likelihoodMap.
%  sourcePositions      Matrix of sky positions at which simulated signals 
%                       (if any) were added.  Same format as skyPositions.
%  sourceLikelihoodMap  Array.  Same as likelihoodMap, except evaluated 
%                       over sourcePositions.
%  sourcePositionIndex  Array.  Same as skyPositionIndex, except refers 
%                       to sourcePositions array.
%  spectrogram          3D array.  Time-frequency maps of the energy in the  
%                       individual detectors, with no time shifts. (The third
%                       dimension loops over detectors.) Produced by 
%                       XTIMEFREQUENCYMAP.
%
% See also XREADDATA, XINJECTSIGNAL, XCONDITION, XTIMEFREQUENCYMAP.
%
% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Stephen Poprocki    poprocki@caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Michal Was          michal.was@ens.fr

% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    process command line arguments                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Test number of arguments
narginchk(0,4);

% ---- Assign default values to missing input arguments.
if (nargin < 4)
    injectionNumberString = '0';  % -- analyse any and all injections.
    if (nargin < 3)
        outputDirectory = '.';  % -- output to current directory.
        if (nargin < 2)
            jobNumberString = '0';    % -- analyse first time in event file.
            if (nargin < 1)
                parameterFileName = 'parameters.txt';  % -- default file.
            end
        end
    end
end

% ---- Strip off any trailing '/' in outputDirectory.
if strcmp(outputDirectory(end),'/')
    outputDirectory(end) = [];
end

% ---- Numeric value of job number(s).
jobNumbers = jobstr2num(jobNumberString);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            assign default values to optional parameters                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- A much more graceful check is implemented below, but it requires the
%      'exist' command which does NOT work properly in compiled R13
%      (despite what the Matlab Compiler home page may say).  Hence, we
%      assign the default values first and then let the parameter file
%      parser overwrite them later.
% ---- Flag indicating whether to apply corrections to known calibration
%      errors
applyCalibCorrection = 0;
% ---- Apply autogating? Default false.
autoGateParameters = [];
% ---- Black pixel percentile level used to determine which fraction of
%      pixels to discard from cluster construction
blackPixelPrctile = 99;
% ---- Path to directory containing cataloged waveforms.
catalogDirectory = '';
% ---- Time step to use for circular time slides. A value of zero means no 
%      circular time slides will be used. 
circTimeSlideStep = 0;
% ---- Trigger rate used for per analysis time, sky position decimate in Hz
decimateRate = 1;
% ---- Detection statistic to use when comparing/ranking triggers.
detectionStat = [];
% ---- Filter file for using pregenerated filters in xcondition.
filterFile = '';
% ---- Flag (0/1) to determine if we perform "unrecorded" injections into 
%      zero-lag data (on-source or off-source) as a detection test.
detectiontestflag = 0;
% ---- Name of file listing names of gating files for each detector.
%      Default to empty (no such file, no gating).
gatingFileName = '';
% ---- Threshold for generalized clustering, if above 0 then clusters
%      with less energy than this times the black pixel threshold are
%      rejected and not used in generalized clustering (connectivity 24).
genClusteringThresh = 0;
% ---- Path to injection file.
injectionFileName = '';
% ---- Sample at which injection peaks.
injectionPeakIdx = [];
% ---- Scale factor applied to all injections.
injectionScales = 0;
% ---- Optional to keep temporary sphrad text files.
keepSphradTextFiles = 0;
% ---- Use the line removal tool
lineRemovalFlag = 0;
freqLineRemoval = [];
% ---- If nonempty replace frame data with simulated noise of the indicated type.
makeSimulatedNoise = '';
% ---- Path to file listing parameters used to miscalibrate injections.
misCalibFileName = '';
% ---- Coincidence window for non-GW signals when calling xcc, xccfast.
nonGWCoincidenceWindow = 60; % -- [s]
% ---- If nonzero take coincidence of clusters with injection list 
%      and report coincidences instead of all clusters.
postProcessInjections = 0;
% ---- Rescale injections by sum of Fp^2 over detectors (use with care).
rescaleByAntennaResponse = 0;
% ---- Parameter string for optional seedless clustering analysis.
seedlessParams = '';
% ---- Trigger rate used for final decimation after super clustering in Hz
superDecimateRate = 0.25;
% ---- Location of temporary directory for intermediate files. Should be locally
%      mounted on the compute node.
for tempDirectory = {getenv('TMPDIR'), getenv('TEMP'), getenv('TMP'), '/tmp'};
    tempDirectory = char(tempDirectory);
    if ~isempty(tempDirectory)
        tempDirectory = strcat(tempDirectory, '/');
        break
    end
end
% ---- A subdirectory name inside the temporary directory for temporary files.
%      Default is the username of the user; Where this already exists in the
%      tempDirectory it is stripped out Be careful to strip out any newlines
%      (character string 10) from system call result.
[status,tempSubDirectory] = system('echo $USER');
tempSubDirectory(strfind(tempSubDirectory,char(10))) = [];
tempDirectory = strrep(tempDirectory, [tempSubDirectory, '/'], '');
% ---- Compute sky map quantities at exact locations of injected signals.
testSourcePosition = 0;
% ---- Use xfindchirpcondition instead of xcondition for data conditioning.
%      THIS OPTION IS NOW DEPRECATED - the value is ignored.
useXFindChirpCondition = 0;
% ---- 1 for extra verbosity.
verboseFlag = 1;
% ---- Length of whitening filters to be made by xcondition.  Should only 
%      change from 1 with changes to xcondition.
whiteningTime = 1;
% ---- Window for FFTs.
windowType = 'modifiedhann';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 determine SVN version of the code                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Try and get svnversion of processing codes using 
%      report-svnversion-xdetection.sh, this bash script is created
%      when running make install-xdetection and is copied to the INSTDIR
%      which should be in the users path
[status,result] = system('sh $XPIPE_INSTALL_BIN/report-svnversion-xdetection.sh');
if status==0
    svnversion_xdetection = result;
else
    warning('report-svnversion-xdetection.sh has failed')
    disp(status);
    disp(result);
    warning('setting svnversion_xdetection to Unknown')
    svnversion_xdetection = 'Unknown';
end

disp(['For processing we used svnversion      : ' ...
    svnversion_xdetection ]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 ensure output arguments are defined                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Make sure all possible outputs are defined.

% ---- Output argument list for interactive use.
skyPositions = [];
likelihoodMap = [];
skyPositionIndex = []; 
sourcePositions = [];
sourceLikelihoodMap = [];
sourcePositionIndex = [];
spectrogram = [];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  read analysis parameters from file                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----- Read each line into separate element of a cell array. 
disp(['Reading parameters from ' parameterFileName])
fileID = fopen(parameterFileName);
paramLines = textscan(fileID,'%s %s','Delimiter',':');
fclose(fileID);
%----- Number of parameters read.
nParams = length(paramLines{1});

%----- Loop over parameters.
for iParam=1:nParams

    %----- Split line into name: value strings
    paramName   = paramLines{1}{iParam};
    paramValStr = paramLines{2}{iParam};
    %----- Parse name: value strings and assign parameter values.
    switch lower(paramName)
        
        case 'analysismode'
            analysisMode = paramValStr;
        case 'analysistimes'
            analysisTimes = str2num(paramValStr); %#ok<ST2NM>
        case 'applycalibcorrection'
            applyCalibCorrection = str2double(paramValStr);
        case 'autogateparameters'
            autoGateParameters = str2num(paramValStr); %#ok<ST2NM>
        case 'blackpixelprctile'
            blackPixelPrctile = str2double(paramValStr);
        case 'blocktime'
            blockTime = str2double(paramValStr);
        case 'catalogdirectory'
            catalogDirectory = paramValStr;
        case 'channelfilename'
            channelFileName = paramValStr;
        case 'circtimeslidestep'
            circTimeSlideStep = str2double(paramValStr);
        case 'decimaterate'
            decimateRate = str2double(paramValStr);
        case 'detectionstat'
            detectionStat = paramValStr;
        case 'detectiontestfile'
            detectiontestflag = 1;
            detectiontestfile = paramValStr;
        case 'donotinject'
            doNotInject = str2double(paramValStr);
        case 'eventfilename'
            eventFileName = paramValStr;
        case 'extractparamfilename'
            extractParamFileName = paramValStr;
        case 'filterfile'
            filterFile = paramValStr;
        case 'framecachefile'
            frameCacheFile = paramValStr;
        case 'freqlineremoval'
            FreqLineRemoval = str2double(paramValStr);
            lineRemovalFlag = 1;
        case 'frequencybands'
            frequencyBands = str2num(paramValStr); %-- not used anywhere. Is this dummy entry 
        case 'gatingfile'
            gatingFileName = paramValStr;
        case 'genclusteringthresh'
            genClusteringThresh = str2num(paramValStr);
        case 'injectionfilename'
            injectionFileName = paramValStr;
        case {'injectionscale','injectionscales'}
            injectionScales = str2num(paramValStr); 
        case 'keepsphradtextfiles'
            keepSphradTextFiles = str2double(paramValStr);
        case 'likelihoodtype'
            likelihoodType = textscan(paramValStr,'%s','delimiter',',');
            likelihoodType = likelihoodType{1};
        case 'makesimulatednoise'
            makeSimulatedNoise = paramValStr;
        case 'maximumfrequency'
            maximumFrequency = str2double(paramValStr);
        case 'maximumtimingerror'
            maximumTimingError = str2double(paramValStr);
        case 'minimumfrequency'
            minimumFrequency = str2double(paramValStr);
        case 'miscalibfilename'
            misCalibFileName = paramValStr;
        case 'multipleinjections'
            multipleInjections = str2double(paramValStr);
        case 'nongwcoincidencewindow'
            nonGWCoincidenceWindow = str2double(paramValStr);
        case 'offsetfraction'
            offsetFraction = str2double(paramValStr);
        case {'onsourcebeginoffset','onsourceendoffset'}
            % ---- Do nothing. The variables onsourcebeginoffset and 
            %      onsourceendoffset are used by the web pages, but we
            %      ignore them here. 
        case 'outputtype'
            outputType = paramValStr;
        case 'postprocessinjections' 
            postProcessInjections = str2double(paramValStr);
        case 'rescalebyantennaresponse'
            rescaleByAntennaResponse = str2double(paramValStr);
        case 'samplefrequency'
            sampleFrequency = str2double(paramValStr);
        case 'savevariables'
            saveVariables = textscan(paramValStr,'%s','delimiter',',');
            saveVariables = saveVariables{1};
        case 'seed'
            seed = paramValStr;
        case 'seedlessparams'
            seedlessParams = paramValStr;
        case 'skycoordinatesystem'
            skyCoordinateSystem = paramValStr;
        case 'skypositionlist'
            skyPositionList = paramValStr;
        case 'sphradparameterfile'
            sphradParameterFile = paramValStr;
        case 'superdecimaterate'
            superDecimateRate = str2double(paramValStr);
        case 'tempdirectory'
            tempDirectory = paramValStr;
        case 'tempsubdirectory'
            tempSubDirectory = paramValStr;
        case 'testsourceposition'
            testSourcePosition = str2double(paramValStr);
        case 'usexfindchirpcondition'
            useXFindChirpCondition = str2double(paramValStr);
        case 'verboseflag'
            verboseFlag = str2double(paramValStr);
        case 'whiteningtime'
            whiteningTime = str2double(paramValStr);
        case 'windowtype'
            windowType = paramValStr;
        otherwise
            fprintf(1, 'ERROR: unknown parameter %s\n', paramName); 
            error('ERROR: unknown parameter %s\n', paramName);
            
    end;

end;

% ---- If no detection statistic specified, use the first specified
%      likelihood. This is backward compatible with xpipeline before r3470.
if isempty(detectionStat)
    detectionStat = likelihoodType{1};
elseif sum(strcmp(detectionStat,likelihoodType)) ~= 1
    likelihoodType
    error(['Detection statistic: ' detectionStat ' does not appear exactly once ' ...
           'in the list of computed likelihoods.']);
end

%----- Numeric value of injection number(s).
injectionNumbers = jobstr2num(injectionNumberString);
if length(injectionNumbers)>1
    warning('LINEAR test code: length(injectionNumbers)>1.');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 read channel information from files                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Data channels.
[channelNames, channelVirtualNames, frameTypes, detectorList] = readchannelfile(channelFileName);
numberOfChannels = length(channelNames);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 begin loop over events / job numbers                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----- Start timer.
pipelineStartTime = clock;

%----- Log starting time.
fprintf(1, 'Coherent burst network analysis pipeline\n%s\n', ...
    datestr(now, 31));

%----- Begin loop over events
nJobs = length(jobNumbers);
for iJob = 1:nJobs

    jobNumber = jobNumbers(iJob);

    % ---- Status message.
    disp(['Processing job ' num2str(jobNumber) ' ...']);

    % ---- Clear/initialize additional output arguments for clustering.
    % clusterInj = []; 
    peakTime = [];

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                 read event information from file                    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %----- Check and read eventFileName.  Extract the single event
    %      specified by the xdetection jobNumber argument.
    if (~isequal(eventFileName,''))

        %----- Read each line (for one event) into a cell array.
        fileID = fopen(eventFileName);
        eventLines = textscan(fileID,'%s','delimiter','\n');
        eventLines = eventLines{1};
        fclose(fileID);
        %----- Number of events.
        nEvLines = length(eventLines);

        %----- Check that the requested event is available.
        if (jobNumber + 1 > nEvLines)
            error(['Event file does not contain number of events implied ' ...
                'by job number.  Pick a smaller job number!']);
        end;

        %----- Extract data for specified event.
        eventData = str2num(eventLines{jobNumber + 1});

        %----- Extract center time for specified event.
        centerTime = eventData(1);

        %----- Extract time offsets for specified event.
        if (length(eventData)>1)
            timeOffsets = eventData(2:end);
        else
            timeOffsets = zeros(1,numberOfChannels);
        end;

    end;

    %----- Start and stop time for this event.
    startTime = centerTime - blockTime / 2;
    stopTime = centerTime + blockTime / 2;


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                        status report                                %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %----- Optional verbosity.  Dump to stdout all of the required and
    %      default parameters, and some related info.
    if (verboseFlag)
        fprintf(1, 'xdetection %s %s %s %s\n', parameterFileName,...
          jobNumberString, outputDirectory, injectionNumberString);
        disp('Parameters:');
        fprintf(1, [ ...
            '            minimumFrequency %f\n' ...
            '            maximumFrequency %f\n'...
            '             sampleFrequency %f\n' ...
            '               whiteningTime %f\n'...
            '          makeSimulatedNoise %s\n' ],...
            minimumFrequency, maximumFrequency,...
            sampleFrequency, whiteningTime,...
            makeSimulatedNoise);
        fprintf(1, '               analysisTimes ');
        fprintf(1, '%f ',analysisTimes);
        fprintf(1,'\n');
        fprintf(1, '             injectionScales ');
        fprintf(1, '%f ',injectionScales);
        fprintf(1,'\n');
        fprintf(1, [ ...
            '        applyCalibCorrection %f\n' ...
            '                   blockTime %f\n' ...
            '    rescaleByAntennaResponse %f\n'...
            '           injectionFileName %s\n'...
            '              frameCacheFile %s\n'...
            '             channelFileName %s\n'...
            '            misCalibFileName %s\n'...
            '               eventFileName %s\n'],...
            applyCalibCorrection, blockTime, ...
            rescaleByAntennaResponse, injectionFileName, frameCacheFile, ...
            channelFileName, misCalibFileName, eventFileName);
        disp('              likelihoodType');
        disp(likelihoodType);
        fprintf(1, [ ...
            '             skyPositionList %s\n' ...
            '              offsetFraction %f\n' ...
            '                  outputType %s\n'...
            '         skyCoordinateSystem %s\n'...
            '                  filterFile %s\n'...
            '                  windowType %s\n'...
            '          testSourcePosition %f\n'], ...
            skyPositionList,offsetFraction, outputType, skyCoordinateSystem, filterFile, ...
            windowType, testSourcePosition);
        disp('Channels:');
        for iCh = 1:numberOfChannels 
            fprintf(1,'Channel %d name %s type %s virtual %s\n',...
            iCh,channelNames{iCh},frameTypes{iCh},...
            channelVirtualNames{iCh});
        end;
        fprintf(1, 'centerTime %f\n', centerTime);
        disp('timeOffsets');
        disp(timeOffsets);
        fprintf(1, 'windowType %s\n', windowType);
        if detectiontestflag==1 & max(abs(timeOffsets))==0 & injectionScales==0
            disp(['WARNING: Will add unrecorded injections from ' detectiontestfile ' to zero-lag data.']);
        end
    end;
    

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %               prepare output directory 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %---- Create output directory  
    if ~ispc
        unix(['mkdir -p ' outputDirectory]);
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                determine set of sky positions to test               %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- This calculation is done inside the job loop because it may need
    %      to know the GPS time of the data to convert from RA,Dec to
    %      Earth-fixed coordinates.

    % ---- First determine whether skyPositionList variable is a file name,
    %      Do this by using the exist command, a result of 2 indicates a file.
    if exist(skyPositionList,'file')~=2
        % ---- skyPositionList is a numeric array.  Compute skyPositions.
        outarray = str2num(skyPositionList); %#ok<ST2NM>
        if (size(outarray)==[1,2]) %#ok<BDSCA>
            skyPositions = outarray;
        elseif (size(outarray)==[1,1]) %#ok<BDSCA>
            %skyPositions = sinusoidalMap(outarray);
            [coordinates, solidAngles, probabilities] = healpix(outarray);
            skyPositions = [coordinates, probabilities, solidAngles];
            k = find(skyPositions(:,2)>=pi);
            skyPositions(k,2) = skyPositions(k,2) - 2*pi;
        else
            error(['Numerical skyPositionList argument must be a scalar ' ...
                'or two-element row vector.']);
        end
    else
        % ---- skyPositionList points to a file of sky positions.  Read it.
        fprintf(1,'reading skyPositions from %s ... ', skyPositionList)  
        skyPositions = load(skyPositionList);
        fprintf(1,'done! \n')  
    end

    % ---- Convert to Earth-based coordinates if skyPositions specified
    %      in celestial coordinates (right ascension, declination).
    switch skyCoordinateSystem
        case 'radec'
            [skyPositions(:,2), skyPositions(:,1)] = radectoearth( ...
                skyPositions(:,2),skyPositions(:,1),centerTime);
        case 'earthfixed'
            % ---- Do nothing.
            
        otherwise
            error(['skyCoordinateSystem value ' skyCoordinateSystem ...
                ' not recognized.  Recognized values are radec and' ...
                ' earthfixed.']);
    end

    if strcmpi(outputType,'sphericalclusters')
         if(length(skyPositions)<100)
            error('Not enough sky positions, try with a finer grid.') 
         end
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                 read/create IFO and injection data                  %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %----- Read data from frame files or generate simulated noise.
    if isempty(makeSimulatedNoise) || strcmp(makeSimulatedNoise,'real')
        
        %----- Read data from frame files.
        if verboseFlag
            disp('reading IFO data from frames ...');
        end
        fprintf(1,'%f %f %f\n',startTime,stopTime,timeOffsets);
        try 
            [data, sampleFrequencies] = xreaddata(channelNames, frameTypes, ...
                frameCacheFile, startTime, stopTime, timeOffsets, verboseFlag);
        catch
            % ---- Frame reading errors can be caused by temporary file system 
            %      issues, including too many X-Pipeline jobs simultaneously 
            %      attempting to access a given frame. Pause for a minute or two
            %      and try one more time.
            % ---- Ensure pausing is allowed.
            state = pause('on');  
            % ---- Pause for a random interval of 10s - 200s. Reset random
            %      number generator seed to a value based on the current
            %      time so that different condor jobs will get different
            %      outputs from rand(). Other parts of xdetection make
            %      calls to legacy code randn('state',...) so we reset the
            %      number generator to its original value when done.
            rng('default');
            rng('shuffle');
            waitTime = 10+190*rand(1);
            disp(['Warning: error in attempting to read frame. Will pause for ' ...
                num2str(waitTime) ' seconds and try a second time.']); 
            pause(waitTime);
            % ---- Return to original pause state.
            pause(state);
            % ---- Try to read the data again.
            [data, sampleFrequencies] = xreaddata(channelNames, frameTypes, ...
                frameCacheFile, startTime, stopTime, timeOffsets, verboseFlag);
            rng('default');
        end

        % ---- Check that reported sampleFrequency is nonzero and that
        %      length of data equals interval * sampleFrequency
        for channelNumber = 1 : numberOfChannels
            if (sampleFrequencies(channelNumber)==0) || ...
                (length(data{channelNumber}) ~= ...
                (sampleFrequencies(channelNumber) * ...
                (stopTime - startTime)))
                error(['Data is missing in channel ' ...
                    channelNames{channelNumber} ' of frame ' ...
                    frameTypes{channelNumber} ' somewhere between ' ...
                    num2str(startTime) ' and ' num2str(stopTime) ]);
            end  
        end

        if (applyCalibCorrection)
            [data] = xapplycalibrationcorrections(data,channelNames, ...
                frameTypes, centerTime);
        end

        if strcmp(makeSimulatedNoise,'real')
            for channelNumber = 1 : numberOfChannels
                [PSD Fraw] = medianmeanaveragespectrum(...
                    data{channelNumber},...
                    sampleFrequencies(channelNumber),...
                    whiteningTime*sampleFrequencies(channelNumber));
                % ---- Interpolate from coarse frequency spacing 1/whiteningLength to
                %      fine resolution 1/dataTime.
                PSD = interp1(Fraw,PSD,[0:1/blockTime:sampleFrequencies(channelNumber)/2]');
                % ---- Set the seed value for the random number generator to a
                %      reproducible value.  Seed is a function of detector and
                %      GPS time of block only.
                randn('state', centerTime + timeOffsets(channelNumber) + channelNumber);
                % ---- Generate simulated noise. Skip the zero frequency in
                %      the PSD
                data{channelNumber} = simulateddetectornoise(...
                    PSD(2:end),blockTime,sampleFrequencies(channelNumber), ...
                    minimumFrequency*0.75,sampleFrequencies(channelNumber)/2);
            end          
        end

    else
        
        % ---- Generate simulated noise data.
        if verboseFlag
            disp('generating simulated IFO data ...');
        end
        sampleFrequencies = sampleFrequency * ones(numberOfChannels,1);
        for channelNumber = 1 : numberOfChannels
            % ---- Set the seed value for the random number generator to a
            %      reproducible value.  Seed is a function of detector and
            %      GPS time of block only.
            randn('state', centerTime + timeOffsets(channelNumber) + channelNumber);
            % ---- Generate simulated noise.
            data{channelNumber} = simulateddetectornoise(makeSimulatedNoise, ...
                blockTime,sampleFrequency,minimumFrequency*0.75,sampleFrequency/2);
        end

    end
    
    % ---- Optionally read gating information from file (using autogate.m output format).
    if ~isempty(gatingFileName)
        % ---- Read gating data into cell array.
        if verboseFlag
            disp('reading gate file ...');
        end
        [allGates, allGatesXCond] = readgatefile(gatingFileName,startTime,stopTime,sampleFrequency,timeOffsets);
    else
        % ---- Initialise gates as empty cell arrays.
        allGates{numberOfChannels} = [];
        allGatesXCond{numberOfChannels} = [];
    end

    % ---- Loop over injection scales.
    for injectionScale = injectionScales

        % ---- Loop over injections.
        for injectionNumber = injectionNumbers


            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %                      prepare injection data                     %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            %----- Prepare temporary copy of the data to which we will add
            %      injections. 
            analysisData = cell(size(data));
            for channelNumber = 1 : numberOfChannels
                analysisData{channelNumber} = data{channelNumber};
            end

            % ---- Create software injection timeseries.
            % ---- Set injection parameter arrays to empty; they get filled
            %      only if a software injection occurs.  (Tested later if
            %      testSourcePosition==1).
            injectionGPS_s = [];
            injectionGPS_ns = [];
            injectionPhi = []; 
            injectionTheta = [];
            injectionPsi = [];
            % ---- Create injection timeseries, if an injection file has been
            %      specified.
            if (~isempty(injectionFileName))

                if verboseFlag
                    disp(['generating software injection ' num2str(injectionNumber) ' ...']);
                end

                % ---- Create injection timeseries.  Output is cell array of
                %      timeseries data, plus record of where and when each
                %      injection occurs.
                [softwareInjectionData, injectionGPS_s, injectionGPS_ns, injectionPhi, ...
                    injectionTheta, injectionPsi] = xinjectsignal(startTime, ...
                    blockTime, channelVirtualNames, sampleFrequencies, ...
                    injectionFileName, rescaleByAntennaResponse, ...
                    injectionNumber,'catalogDirectory',catalogDirectory);

                % ---- Compute injection peak time; we normally only analyze a 
                %      small interval of data around the injection.
                % ---- There should be exactly one injection.
                if (length(injectionGPS_s) ~= 1)
                    error('There should only be one injection in the data.');
                end
                % ---- Peak time of injection at first detector
                %      (reference position).
                delay = computeTimeShifts(channelVirtualNames(1), [injectionTheta,injectionPhi]);
                injectionPeakTime = injectionGPS_s + 1e-9*injectionGPS_ns + delay;

                if (~isequal(misCalibFileName,''))              
                    % ---- Miscalibrate injections to simulate errors in ifo data
                    %      calibration 
                    [softwareInjectionData] = xmiscalibrateinjections(...
                        softwareInjectionData, misCalibFileName, ...
                        channelVirtualNames, sampleFrequencies);
                end

                % ---- Rescale software injection data and add to IFO "noise" data.
                for channelNumber = 1 : numberOfChannels
                    analysisData{channelNumber} = analysisData{channelNumber} ...
                        + injectionScale*softwareInjectionData{channelNumber} ;
                end

                clear softwareInjectionData;

            end

            % KLUDGE:TEST CODE -------------------------------------------------
            % Perform unrecorded injections into zero-lag data as a detection test.
            % ------------------------------------------------------------------
            if detectiontestflag==1 & max(abs(timeOffsets))==0 & injectionScales==0
                % ---- Note: We should only be able to enter this "if" statement
                %      when no other type of injection is being performed.
                % ---- WARNING: This code will activate for any zero-lag, 
                %      zero-injection run. That means it will try to inject into
                %      zero-lag off-source runs too. The contents of 
                %      detectiontestfile will determine when injections actually
                %      occur.
                disp(['WARNING: Performing unrecorded injections from ' detectiontestfile ' to zero-lag data.']);
                % ---- Create injection timeseries.  Output is cell array of
                %      timeseries data, plus record of where and when each
                %      injection occurs.
                % ---- Note that we don't record any of the injection log info;
                %      this injection is kept 'secret' from the rest of the code.
                softwareInjectionData = xinjectsignal(startTime, ...
                    blockTime, channelVirtualNames, sampleFrequencies, ...
                    detectiontestfile, rescaleByAntennaResponse, ...
                    injectionNumber,'catalogDirectory',catalogDirectory);

                % ---- Add software injection data to raw IFO data.
                for channelNumber = 1 : numberOfChannels
                    analysisData{channelNumber} = analysisData{channelNumber} ...
                        + softwareInjectionData{channelNumber} ;
                end

            end
            % END:KLUDGE -------------------------------------------------------


            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %                 loop over analysisTimes (FFT times)                 %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % ---- We wish to be able to specify multiple analysis times.  To
            %      avoid over-writing results from one analysis time with
            %      results from another, pack all appropriate data into cell
            %      arrays.  Each variable in question has a corresponding cell
            %      array that holds the same data for all analysis times.
            % ---- KLUDGE: Make *Cell copies of relevant variables.
            amplitudeSpectraCell    = cell(length(analysisTimes),1);
            analysisLengthCell      = cell(length(analysisTimes),1);
            analysisTimesCell       = cell(length(analysisTimes),1);
            gaussianityCell         = cell(length(analysisTimes),1);
            sourceAngleIndexCell    = cell(length(analysisTimes),1);
            sourceLikelihoodMapCell = cell(length(analysisTimes),1);
            sourcePositionIndexCell = cell(length(analysisTimes),1);
            % ---- Don't output these for 'clusters' case to save on memory.
            if (~strcmp(outputType,'clusters') && ...
                ~strcmp(outputType,'sphrad') && ...
                ~strcmp(outputType,'injectionclusters') && ...
                ~strcmp(outputType,'glitch'))
                spectrogramCell         = cell(length(analysisTimes),1);
                internalAngleIndexCell  = cell(length(analysisTimes),1);
                skyPositionIndexCell    = cell(length(analysisTimes),1);
            end


            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %                        condition data                           %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            if strcmp(lower(makeSimulatedNoise),'white') %#ok<STCI>
                % ---- Skip data conditioning if input is already ideal white
                %      noise (for testing).
                conditionedData = cell2mat(analysisData);
                % ---- Loop over analysis time scales and make amplitudeSpectra.
                for ii = 1:length(analysisTimes)
                    amplitudeSpectraCell{ii} = (2/sampleFrequency)^0.5 * ones( ...
                        sampleFrequency/2*analysisTimes(ii)+1,numberOfChannels);
                end
                analysisLengths = analysisTimes*sampleFrequency;
                transientLength = 4*sampleFrequency;
                normalizationFactor = (analysisTimes(1)./(analysisTimes(:))).^0.5;
            else
                % ---- Apply standard data conditioning.
                % ---- First check to see if we have a pre-computed
                %      whitening filter, or if we are constructing one on
                %      the fly.  
                if ~isempty(filterFile)
                    % ---- Load pre-computed filter from file.
                    lpefData = load(filterFile);
                    trainingMode = lpefData.lpefCoefficients;
                    if verboseFlag
                        disp('conditioning using pregenerated LPEF ...');
                    end
                else
                    % ---- Use whitening filter made on-the-fly.
                    trainingMode = 'search';
                    if verboseFlag
                        disp('conditioning with LPEF made on-the-fly ...');
                    end
                end
                % ---- Resample, high pass filter, and whiten data.
                [conditionedData, amplitudeSpectraCell, analysisLengths, ...
                    transientLength, normalizationFactor] = xcondition( ...
                    analysisData, sampleFrequencies, minimumFrequency, ...
                    analysisTimes, whiteningTime, sampleFrequency, ...
                    verboseFlag, trainingMode, allGatesXCond);
            end
            
            % ---- Apply line removal algorithm if requested.
            if lineRemovalFlag
                for lineFreq = FreqLineRemoval
                    %Line removal edit
                    for ii = 1:numberOfChannels
                        conditionedData(:,ii) = lineremoval(conditionedData(:,ii),sampleFrequency,lineFreq);
                    end
                end
            end
            transientTime = transientLength/sampleFrequency;

            % ---- Apply autogating, if requested.
            if ~isempty(autoGateParameters)
                filterLength   = autoGateParameters(1) * sampleFrequency;
                sigmaThreshold = autoGateParameters(2);
                windowLength   = autoGateParameters(3) * sampleFrequency;
                if length(autoGateParameters)>3
                    vetoLength = autoGateParameters(4) * sampleFrequency;
                else
                    vetoLength = [];
                end
                if length(autoGateParameters)>4
                    vetoThreshold = autoGateParameters(5);
                else
                    vetoThreshold = sigmaThreshold;
                end
                % ---- Compute and apply autogates by windowing conditionedData.
                [conditionedData, autoGates] = autogate(conditionedData,transientLength,'rectwin', ...
                    filterLength,sigmaThreshold,'tukeywin',windowLength,vetoLength,vetoThreshold);
                for ii = 1:numberOfChannels
                    if ~isempty(autoGates{ii})
                        disp(['Autogates activated for detector ' channelVirtualNames{ii}(1:2)]);
                        disp('The following sample intervals have been zeroed out:');
                        autoGates{ii}
                    end
                end
                % % ---- TEST CODE : NOT FUNCTIONAL SO COMMENTED OUT.
                % % ---- Compute autogates but do not apply them; rather they will be used later when
                % %      clustering in xtimefrequencymap.
                % [ ~ , autoGates] = autogate(conditionedData,transientLength,'rectwin', ...
                %     filterLength,sigmaThreshold,'tukeywin',windowLength,vetoLength,vetoThreshold);
                % for ii = 1:numberOfChannels
                %     if ~isempty(autoGates{ii})
                %         disp(['Autogates activated for detector ' channelVirtualNames{ii}(1:2)]);
                %         disp('The following (zero-lag) sample intervals will be gated: ');
                %         autoGates{ii}
                %     end
                % end
                % % ---- Next, combine autoGates into allGates (read from file), if the latter exists.
                % %      Otherwise define allGates as autoGates (which may be empty).
                % if exist('allGates','var')
                %     for ii = 1:numberOfChannels
                %         allGates{ii} = integerintervalunion({allGates{ii},autoGates{ii}});
                %     end
                % else
                %     allGates = autoGates;
                % end
            end

            % % ---- TEST CODE : NOT FUNCTIONAL SO COMMENTED OUT.
            % % ---- Over-write genClusteringThresh with union of allGates, autoGates 
            % %      for passing to xtimefrequencymap. Note that this happens if we
            % %      request either type of gating, even if the gate list happens to 
            % %      be empty. This is required since we need all data segments to be 
            % %      processed the same way (i.e., none using generalised clustering 
            % %      or all using it) whether or not a particular segment happens to 
            % %      include gates.
            % if exist('allGates','var')
            % warning('Over-writing genClusteringThresh with allGates for passing to xtimefrequencymap.')
            % genClusteringThresh = allGates;
            % end
            
            % ---- Circular time slides.
            if circTimeSlideStep
                % ---- xtimefrequencymap does not analyse the last analysisTime
                %      interval in the data. We need to account for this when
                %      choosing circular time slides to avoid indexing errors.
                endBuffer      = max([analysisTimes,1]);
                numberOfSteps  = floor((blockTime-2*transientTime-endBuffer) / circTimeSlideStep);
                circTimeSlides = circTimeSlideStep * circulartimeslides(numberOfChannels,numberOfSteps);
            else
                circTimeSlides = zeros(1,numberOfChannels);
            end
            likelihoodMapCell = cell(length(analysisTimes),size(circTimeSlides,1));
            if verboseFlag
                disp('Circular time slides being used:');
                circTimeSlides
            end

            % ---- Loop over analysis time scales.
            for analysisTime = analysisTimes


                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %            Prepare data for this analysisTime
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                % ---- Pull out analysis-time variables for the current
                %      analysisTime. 
                ii = find(analysisTimes == analysisTime);
                amplitudeSpectra = amplitudeSpectraCell{ii};
                analysisLength = analysisLengths(ii);
                % ---- Rescale conditioned data to normalize it appropriately
                %      for this analysisTime. 
                if (ii>1)
                    conditionedData = conditionedData * ...
                        normalizationFactor(ii) / normalizationFactor(ii-1);
                end


                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %            compute statistics for trial sky positions           %
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                % ---- Make sure these are defined.
                internalAngleIndex = [];
                sourceAngleIndex = [];

                switch outputType

                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    % ---- WARNING: The following output types are       %
                    %      experimental / development versions.  Caveat  %
                    %      emptor!                                       %
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    case 'spherical'

                        % ---- compute time-frequency map and save loudest
                        %      cluster for each sky direction
                        disp('computing time-frequency likelihood map and clusters ...');
                        [likelihoodMap, skyPositionIndex, spectrogram, ...
                            internalAngleIndex] = xsphericalTFmap( ...
                            channelVirtualNames, conditionedData, sampleFrequency, ...
                            amplitudeSpectra, skyPositions, analysisLength, ...
                            floor(offsetFraction*analysisLength), transientLength, ...
                            [minimumFrequency, maximumFrequency], windowType, ...
                            likelihoodType, 1, verboseFlag);

                    case 'spherical3'

                        % ---- compute time-frequency map and save loudest
                        %      cluster for each sky direction
                        disp('computing time-frequency likelihood map and clusters ...');
                        [likelihoodMap, skyPositionIndex, spectrogram, ...
                            internalAngleIndex] = xsphericalTFmap3( ...
                            channelVirtualNames, conditionedData, sampleFrequency, ...
                            amplitudeSpectra, skyPositions, analysisLength, ...
                            floor(offsetFraction*analysisLength), transientLength, ...
                            [minimumFrequency, maximumFrequency], windowType, ...
                            likelihoodType, 1, verboseFlag);

                    case {'sphrad'}

                        % ---- Try it!
                        disp('WARNING: Trying xsphrad - EXPERIMENTAL CODE.');
                        % ---- Load sphrad parameters file.
                        disp(['WARNING: Loading sphrad analysis parameters from ' sphradParameterFile]);
                        disp('         No checks are performed for consistency with the parameters ');
                        disp('         used by xdetection. ');
                        sphradparam = xparameters(sphradParameterFile);
                        % ---- Forcing sphradparam.randomSeed to use user-specified input seed from 
                        %      parameters file (if supplied). This appears not to be used by sphrad 
                        %      code, but better safe than sorry...!
                        if exist('seed','var')
                            sphradparam.randomSeed = seed;
                        end
                        % ---- Make vector of circular time shifts for second detector. First detector 
                        %      will be held at zero lag, third detector will have shift that is the 
                        %      negative of second detector.
                        timeShiftVector =  circTimeSlides(:,2);
                        % ---- Make temporary directory to hold output files where they will not 
                        %      be over-written by another job.  
                        %      The first and last characters returned as result in the system call
                        %      may be line feeds (ASCII code 10); these are problematic. Drop them.
                        [status,tag] = system('uuidgen');
                        tag(strfind(tag,char(10))) = [];
                        [status,location] = system('echo $USER');
                        location(strfind(location,char(10))) = [];
                        [status,cmdout] = system('hostname')
                        % ---- Set temporary folder.
                        tmpdirname = [tempDirectory tempSubDirectory '/tmp_' tag];
                        [status,result] = system(['mkdir -p ' tmpdirname]);
                        fprintf(1, 'Tmp file location:  %s\n', tmpdirname);
                        sphradparam.outputDirectoryFinal = outputDirectory;
                        sphradparam.outputDirectory = tmpdirname;
                        % ---- Determine startTime string that will appear in output file name.
                        if startTime < 1000000000
                            startTimeString = ['0' num2str(startTime)];
                        else
                            startTimeString = num2str(startTime);
                        end
                        % ---- Call sphrad code with empty pointing map.
                        xsphrad(conditionedData, amplitudeSpectra, startTime, stopTime, channelVirtualNames, sphradparam, {}, timeShiftVector);
                        %xsphrad(conditionedData, amplitudeSpectra, startTime, stopTime, channelVirtualNames, sphradparam, {});
                        %xsphrad(conditionedData, amplitudeSpectra, startTime, stopTime, channelVirtualNames, sphradparam, stuff.pointingMap);
                        likelihoodMap = [];
                        for ic = 1:length(timeShiftVector)
                            % ---- Read events from sphrad output file back into matlab. The clumsy 
                            %      two-line syntax is needed for compatibility with matlab r2007a.
                            %      Loop through the array containing the list of detectors; not 
                            %      'clean' but had to work around matlab 'cell' class. 
                            detlist = [];
                            for ii = 1:numberOfChannels
                                detlist = [detlist char(detectorList(ii))];
                            end
                            tmp_fileName = ['SPHRAD_EVENTS_SHIFT_' ...
                                num2str(timeShiftVector(ic)) '-' startTimeString '-' ...
                                num2str(blockTime) '.txt'];
                            [tmp_likelihoodMap sphrad_likelihoodType] = readsphradeventfile( ...
                                [sphradparam.outputDirectory '/' tmp_fileName],true);
                            % ---- KLUDGE: for two-detector case zero out the nullenergy, nullinc 
                            %      likelihoods as the numbers returned by sphrad are garbage.
                            if numberOfChannels==2
                                tmp_likelihoodMap(:,7+[8,9]) = 0;
                            end
                            likelihoodMap{ic} = tmp_likelihoodMap;
                            % ---- Verify that likelihoods returned by sphrad match those requested 
                            %      by xdetection, else we'll have trouble down the line.
                            if ~all(size(likelihoodType)==size(sphrad_likelihoodType)) || ...
                                ~all(strcmp(likelihoodType,sphrad_likelihoodType))
                                disp('likelihoodType in xdetection:');
                                disp(likelihoodType);
                                disp('likelihoodType in xsphrad:   ');
                                disp(sphrad_likelihoodType);
                                error('mismatch between likelihoodType in xdetection and sphrad.');
                            end
                        end 
                        % ---- Clean up: optionally move .txt files to a permanent directory, and delete temporary directory.
                        if keepSphradTextFiles
                            [status,result] = system(['mkdir -p' sphradparam.outputDirectoryFinal]);
                            [status,result] = system(['mv --force ' tmpdirname '/* ' sphradparam.outputDirectoryFinal]);
                        end
                        [status,result] = system(['rm -rf ' tmpdirname]);
                        % ---- Assign dummy values to other variables.
                        skyPositionIndex = [];
                        spectrogram = [];
                        internalAngleIndex = [];
                        disp('Done.');

                    case 'timeseries'

                        %----- Compute likelihood timeseries for each sky position.
                        disp('computing likelihood timeseries ...');
                        [likelihoodMap, internalAngleIndex] = xtimeseries( ...
                            channelVirtualNames, conditionedData, sampleFrequency, ...
                            amplitudeSpectra, skyPositions, analysisLength, ...
                            floor(offsetFraction*analysisLength), transientLength, ...
                            [minimumFrequency, maximumFrequency], likelihoodType, ...
                            verboseFlag, windowType);

                    case 'xbayesiantimefrequencymap'
                        
                        % ---- Experimental bayesian code version of outputType
                        %      'clusters'.
                        disp(['computing time-frequency likelihood map using xbayesiantimefrequencymap2 ...']);
                        [likelihoodMap, skyPositionIndex, spectrogram, likelihoodType] ...
                            = xbayesiantimefrequencymap2( ...
                            channelVirtualNames, conditionedData, sampleFrequency, ...
                            amplitudeSpectra, skyPositions, analysisLength, ...
                            floor(offsetFraction*analysisLength), transientLength, ...
                            [minimumFrequency, maximumFrequency], windowType, ...
                            likelihoodType, 'clusters', verboseFlag);
           
                    case {'xcc','xccfast'}
                        
                        % ---- Perform cross-correlation for non-GW
                        %      signals with v<<c.
                        % ---- First determine peak time of injection, if any.
                        if (~isempty(injectionFileName))
                            % ---- Read and parse the parameters of the injection being analysed.
                            injectionParameters = readinjectionfile(injectionFileName,injectionNumber);
                            injectionStruct     = parseinjectionparameters(injectionParameters);
                            % ---- xcc's fast analysis option for injections requires the data sample at which the 
                            %      injection peaks at the SECOND detector.
                            delay = computeTimeShifts(channelVirtualNames(2), [injectionStruct.theta(1,2),injectionStruct.phi(1,2)]);
                            injectionPeakTime = injectionStruct.gps_s(1,2) + 1e-9*injectionStruct.gps_ns(1,2) + delay;
                            injectionPeakIdx = round((injectionPeakTime-startTime)*sampleFrequency);
                        end
                        % ---- Call cross-correlation code.
                        switch outputType
                            case 'xcc'
                                disp('Calling xcc ...');
                                [cc_val, idx1, idx2, norm1, norm2] = xcc(conditionedData, ...
                                    sampleFrequency,transientTime,nonGWCoincidenceWindow, ...
                                    analysisTime,offsetFraction,windowType,injectionPeakIdx);
                            case 'xccfast'
                                disp('Calling xccfast ...');
                                [cc_val, cc_idx, idx1, idx2, norm1, norm2] = xccfast(conditionedData, ...
                                    sampleFrequency,transientTime,nonGWCoincidenceWindow, ...
                                    analysisTime,offsetFraction,windowType,superDecimateRate);
                        end
                        % ---- Repack output into standard form expected
                        %      for likelihoodMap variable. 
                        %      KLUDGE: Use first detector as reference for
                        %      time of event.
                        % ---- Clear likelihoodMap because it will have a different size for each 
                        %      analysisTime for on/off-source runs.
                        clear likelihoodMap
                        likelihoodMap(:,1)  = startTime + idx1 / sampleFrequency;
                        likelihoodMap(:,2)  = likelihoodMap(:,1) + 0.5*analysisTime;
                        likelihoodMap(:,3)  = likelihoodMap(:,1) + analysisTime;
                        likelihoodMap(:,4)  = minimumFrequency;
                        likelihoodMap(:,5)  = mean(minimumFrequency, maximumFrequency);
                        likelihoodMap(:,6)  = maximumFrequency;
                        likelihoodMap(:,7)  = analysisLength;
                        likelihoodMap(:,8)  = abs(cc_val);
                        likelihoodMap(:,9)  = cc_val;
                        likelihoodMap(:,10) = norm1;
                        likelihoodMap(:,11) = norm2;
                        likelihoodMap(:,12) = startTime + idx1 / sampleFrequency; %-- time in IFO 1; redundant but defined for symmetry
                        likelihoodMap(:,13) = startTime + idx2 / sampleFrequency; %-- time in IFO 2; not recorded elsewhere
                        if strcmp(outputType,'xccfast')
                            likelihoodMap(:,14) = cc_idx;
                        end
                        % KLUDGE: add check that likelihoodType matches this:
                        % likelihoodType = {'abscc','cc','norm1','norm2','starttime1','starttime2','cc_idx'};
                        % ---- Assign dummy values to other variables expected by xdetection.
                        skyPositionIndex = [];
                        spectrogram = [];
                        internalAngleIndex = [];
                        disp(['   ... finished call to ' outputType '.']);

                        
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    % ---- PRODUCTION RUNNING: These are the "standard" 
                    %      output types used in production analyses. Set
                    %      default variables as needed and call
                    %      xtimefrequencymap(). 
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    
                    otherwise
                        
                        % ---- Determine start time of data analysed by
                        %      xtimefrequencymap.
                        switch outputType
                            case {'timefrequencymap'}
                                %---- Do nothing.
                                ;
                            case {'seedless'}
                                minClusterStart = startTime + transientTime;
                            otherwise
                                minClusterStart = startTime;
                        end
                        % ---- Set injectionPeakIdx variable if needed.
                        switch outputType
                            case 'injectionclusters'
                                % ---- Index at which the injection peak time falls. 
                                injectionPeakIdx = round((injectionPeakTime-startTime)*sampleFrequency);
                            case 'glitch'
                                % ---- Index at which the glitch peak time falls. 
                                injectionPeakIdx = round((centerTime-startTime)*sampleFrequency);
                        end
 
                        % ---- Compute likelihood time-frequency map or event clusters.
                        % ---- Move these comments into xtimefrequencymap:
                        %      clusters: Compute time-frequency map and save 
                        %        all clusters (for every sky position and
                        %        time), with some throttling
                        %      followup: Compute time-frequency map and save loudest
                        %        single cluster for each sky direction.
                        %      sparseclusters: Compute time-frequency map and save 
                        %        loudest single cluster in each second.
                        %      injectionclusters, glitch: Compute time-frequency map 
                        %        for restricted time interval around
                        %        the injection or glitch time.
                        disp(['Calling xtimefrequencymap with output type ' outputType]);
                        [likelihoodMap, skyPositionIndex, spectrogram, ...
                            internalAngleIndex] = xtimefrequencymap( ...
                            channelVirtualNames, conditionedData, sampleFrequency, ...
                            amplitudeSpectra, skyPositions, analysisLength, ...
                            floor(offsetFraction*analysisLength), transientLength, ...
                            [minimumFrequency, maximumFrequency], windowType, ...
                            likelihoodType, outputType, verboseFlag, circTimeSlides, ...
                            blackPixelPrctile, genClusteringThresh, ...
                            decimateRate, detectionStat, injectionPeakIdx, seedlessParams);

                        % ---- Reset cluster times from time since start of map to GPS time.
                        if not(isempty(likelihoodMap)) && not(strcmp(outputType,'timefrequencymap'))
                            for iCell = 1:length(likelihoodMap)
                                if not(isempty(likelihoodMap{iCell})) 
                                    likelihoodMap{iCell}(:,1:3) = likelihoodMap{iCell}(:,1:3) + minClusterStart;
                                end
                            end
                        end

                end


                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %   compute statistics for actual source locations, if desired    %
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                % ---- Only do if requested and if we've actually done an injection.
                if (testSourcePosition && length(injectionPhi))

                    % ---- Use list of all sky positions at which injections
                    %      were done as grid to test.
                    injectionPhi = mod(injectionPhi+pi,2*pi)-pi;
                    sourcePositions = [injectionTheta injectionPhi];
                    sourcePositions(:,3) = 1/size(sourcePositions,1);
                    sourcePositions(:,4) = 4*pi/size(sourcePositions,1);

                    switch outputType

                        case {'timefrequencymap'}

                            % ---- Compute likelihood time-frequency map extremized
                            %      over sky positions.
                            disp(['computing time-frequency likelihood map at ' ...
                                'source locations ...']);
                            [sourceLikelihoodMap, sourcePositionIndex, ~, ...
                                sourceAngleIndex] = xtimefrequencymap( ...
                                channelVirtualNames, conditionedData, sampleFrequency, ...
                                amplitudeSpectra, sourcePositions, analysisLength, ...
                                floor(offsetFraction*analysisLength), transientLength, ...
                                [minimumFrequency, maximumFrequency], windowType, ...
                                likelihoodType, outputType, verboseFlag);

                        case {'seedless'}

                            % ---- Compute likelihood time-frequency map extremized
                            %      over sky positions.
                            disp('computing time-frequency likelihood map ...');
                            [sourceLikelihoodMap] = xtimefrequencymap( ...
                                channelVirtualNames, conditionedData, sampleFrequency, ...
                                amplitudeSpectra, sourcePositions, analysisLength, ...
                                floor(offsetFraction*analysisLength), transientLength, ...
                                [minimumFrequency, maximumFrequency], windowType, ...
                                likelihoodType, 'seedless', verboseFlag, circTimeSlides, ...
                                blackPixelPrctile, genClusteringThresh, ...
                                decimateRate, detectionStat, '');

                            % ---- Reset cluster times from time since start to GPS time.
                            sourceLikelihoodMap(:,1:3) = sourceLikelihoodMap(:,1:3) ...
                                + startTime;

                        case {'clusters'}

                            % ---- clusters: Original term for 'allclusters', used by 
                            %      GRB search codes.  Identical to 'allclusters'; to
                            %      be replaced by same.
                            % ---- allclusters: Compute time-frequency map and save 
                            %      all clusters (for every sky position and time).
                            disp('computing clusters at source location ...');
                            [sourceLikelihoodMap] = xtimefrequencymap( ...
                                channelVirtualNames, conditionedData, sampleFrequency, ...
                                amplitudeSpectra, sourcePositions, analysisLength, ...
                                floor(offsetFraction*analysisLength), transientLength, ...
                                [minimumFrequency, maximumFrequency], windowType, ...
                                likelihoodType, 'allclusters', verboseFlag, decimateRate);
                            % ---- Reset cluster times from time since start to GPS time.
                            sourceLikelihoodMap(:,1:3) = sourceLikelihoodMap(:,1:3) ...
                                + startTime;

                        case {'followup','sparseclusters','allclusters'}

                            % ---- followup: Compute time-frequency map and save loudest
                            %      single cluster for each sky direction.
                            % ---- sparseclusters: Compute time-frequency map and save 
                            %      loudest single cluster in each second.
                            % ---- allclusters: Compute time-frequency map and save 
                            %      all clusters (for every sky position and time).
                            disp(['computing clusters at source locations ...']); 
                            [sourceLikelihoodMap] = xtimefrequencymap( ...
                                channelVirtualNames, conditionedData, sampleFrequency, ...
                                amplitudeSpectra, sourcePositions, analysisLength, ...
                                floor(offsetFraction*analysisLength), transientLength, ...
                                [minimumFrequency, maximumFrequency], windowType, ...
                                likelihoodType, outputType, verboseFlag, decimateRate);
                            % ---- Reset cluster times from time since start to GPS time.
                            sourceLikelihoodMap(:,1:3) = sourceLikelihoodMap(:,1:3) ...
                                + startTime;

                    end

                end  %-- if testSourcePosition


                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %   Copy analysis-time dependent results to cell arrays    
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                % ---- Copy analysis-time dependent results to cell arrays.
                ii = find(analysisTimes == analysisTime);
                % amplitudeSpectraCell{ii}    = amplitudeSpectra;
                analysisLengthCell{ii}      = analysisLength;
                analysisTimesCell{ii}       = analysisTime;
                gaussianityCell{ii}         = squeeze(mean(spectrogram.^2,2) ...
                                                  ./(mean(spectrogram,2)).^2);
                if (~strcmp(outputType,'clusters') && ...
                    ~strcmp(outputType,'sphrad') && ...
                    ~strcmp(outputType,'injectionclusters') && ...
                    ~strcmp(outputType,'glitch') && ...
                    ~strcmp(outputType,'seedless'))
                    likelihoodMapCell{ii}     = likelihoodMap;
                else
                    likelihoodMapCell(ii,:)   = likelihoodMap;
                end
                sourceLikelihoodMapCell{ii} = sourceLikelihoodMap;
                % ---- Additional output desired for certain cases.
                switch outputType
                    case 'timefrequencymap'
                        internalAngleIndexCell{ii}  = internalAngleIndex;
                        sourceAngleIndexCell{ii}    = sourceAngleIndex;
                        sourcePositionIndexCell{ii} = sourcePositionIndex;            
                        spectrogramCell{ii}         = spectrogram;
                        skyPositionIndexCell{ii}    = skyPositionIndex;
                    case 'timeseries'
                        internalAngleIndexCell{ii}  = internalAngleIndex;
                        sourceAngleIndexCell{ii}    = sourceAngleIndex;
                end

            end  %-- end loop over analysis time scales


            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %   Output or post-process results for this job+injection number.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % ---- Write raw cluster/map data to .mat file OR pass into
            %      post-processing code, based on outputType. 
            if (strcmp(outputType,'clusters') || ...
                strcmp(outputType,'glitch') || ...
                strcmp(outputType,'injectionclusters') || ...
                strcmp(outputType,'sphrad') || ...
                strcmp(outputType,'seedless') || ...
                strcmp(outputType,'xcc') || ...
                strcmp(outputType,'xccfast'))


                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %                  Post-process clusters.
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                % ---- Post-process separately each circular time slide
                cluster = [];
                for iCircSlide = 1:size(circTimeSlides,1)


                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %                      superclustering
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    % ---- Merge event lists from different analysis times, convert
                    %      from flat array to more flexible struct format. 
                    [superClusters, tmpCluster.boundingBox, tmpCluster.nPixels, ...
                     tmpCluster.likelihood, tmpCluster.significance, ...
                     tmpCluster.listIndex] = xsupercluster(likelihoodMapCell(:,iCircSlide),likelihoodType,detectionStat);
                    % ---- Decimate super clusters
                    % ---- Fraction of clusters to keep. Should ensure that
                    %      we don't decimate the injection triggers.
                    minClusterRate = superDecimateRate;
                    maxClusterRate = minClusterRate;
                    % ---- The proportion below doesn't affect the decimation when min
                    %      and max rate are equal.
                    superClusterProportion =  0.1*(500-64)/(maximumFrequency-minimumFrequency);

                    % ---- Convert rates into number of clusters.
                    switch outputType
                        case {'clusters','glitch','seedless','sphrad','xcc','xccfast'}
                            windowDuration = blockTime-2*transientTime;
                        case 'injectionclusters'
                            windowDuration = 16;
                        otherwise
                            error(['Unrecognized outputType: ' outputType])
                    end
                    lowerClusterNb = round(minClusterRate*windowDuration);
                    upperClusterNb = round(maxClusterRate*windowDuration);

                    % ---- Apply decimation.
                    [superClusters decimationMask] = xdecimatecluster(...
                        superClusters, superClusterProportion, lowerClusterNb, ...
                        upperClusterNb, likelihoodType, detectionStat);
                    % ---- Update struct format.
                    tmpCluster.boundingBox = tmpCluster.boundingBox(decimationMask,:);
                    tmpCluster.nPixels = tmpCluster.nPixels(decimationMask,:);
                    tmpCluster.likelihood = tmpCluster.likelihood(decimationMask,:);
                    tmpCluster.significance = tmpCluster.significance(decimationMask,:);
                    tmpCluster.listIndex = tmpCluster.listIndex(decimationMask,:);
                    % ---- Record extra event info into struct format.
                    nClust = length(tmpCluster.significance);
                    aT = cell2mat(analysisTimesCell);
                    tmpCluster.analysisTime  = aT(tmpCluster.listIndex);
                    tmpCluster.jobNumber     = jobNumber*ones(nClust,1);
                    % ---- Create fake job number for book-keeping (starts at 0).
                    tmpCluster.fakeJobNumber = jobNumber*ones(nClust,1)* ...
                        size(circTimeSlides,1) + iCircSlide - 1;
                    if isempty(superClusters)
                        tmpCluster.peakFrequency = [];
                        tmpCluster.peakTime      = [];
                    else
                        tmpCluster.peakFrequency = superClusters(:,5);
                        tmpCluster.peakTime      = superClusters(:,2);
                    end
                    tmpCluster.timeOffsets    = repmat(timeOffsets,[nClust,1]);
                    tmpCluster.circTimeSlides = repmat(circTimeSlides(iCircSlide,:),[nClust,1]);
                    tmpCluster.centerTime     = repmat(centerTime,[nClust,1]);
                    tmpCluster.startTime      = repmat(startTime,[nClust,1]);
                    clear aT nClust loudEventMask superClusters

                    % ---- Keep only loudest trigger around glitch time for
                    %      glitch output type, +/- 1 sec.
                    if strcmp(outputType,'glitch')
                        % ---- Time-frequency box of interest for glitch study.
                        glitchBox = [centerTime-1, minimumFrequency, ...
                                     2,  maximumFrequency-minimumFrequency];
                        % ---- Find all clusters whose bounding box overlaps that 
                        %      of the injection.
                        if isempty(tmpCluster.boundingBox)
                            mask = [];
                        else
                            mask = find(rectint(glitchBox,tmpCluster.boundingBox) > 0);
                            mask = mask(:);
                        end
                        % ---- Keep all clusters in coincidence.  Pack into a
                        %      struct array.
                        tmpCluster = xclustersubset(tmpCluster,mask);
                        % ---- Find loudest trigger in coincidence and keep only
                        %      that one. 
                        [tmp loudestIdx] = max(tmpCluster.significance);
                        tmpCluster = xclustersubset(tmpCluster,loudestIdx);              
                    end

                    % ---- Append clusters from this circular time slide to full list.
                    if isempty(cluster)
                        cluster = tmpCluster;
                    else
                        cluster = xclustermerge(cluster,tmpCluster);
                    end

                end % -- end loop over circular time slides


                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %                    injection coincidence
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                % ---- Do coincidence with injections, if applicable.
                if (injectionScale)

                    % ---- Find all clusters in coincidence with this injection. 

                    % ---- MAGIC NUMBERS.  These should be read from the
                    %      parameter file. 
                    % ---- Systematic offset of expected peak time of event 
                    %      BEFORE injection peak time.  Useful for, e.g.,
                    %      inspirals, where nominal peak time is actually
                    %      coalescence time.
                    % ---- Should we rename this variable to injTimeOffset??
                    onSourceTimeOffset = 0;
                    % ---- Event must peak within +/- this amount of injection
                    %      (peak time - offset).
                    onSourceTimeWidth = 5;

                    % ---- Find all clusters in coincidence with this 
                    %      injection.  Coincidence is by time only.  Frequency 
                    %      coincidence can also be imposed easily with the
                    %      rectint function.
                    if (injectionNumber)
                        % ---- There should be exactly one injection.
                        if (length(injectionGPS_s) ~= 1)
                            error('There should only be one injection in the data.');
                        end
                        % ---- Peak time of injection at first detector
                        %      (reference position).
                        delay = computeTimeShifts(channelVirtualNames(1), [injectionTheta,injectionPhi]);
                        injectionPeakTime = injectionGPS_s + 1e-9*injectionGPS_ns + delay;
                        % ---- Record peak time of injection at first detector.
                        peakTime(injectionNumber) = injectionPeakTime;
                        % ---- Bounding box of injection in the time-frequency plane.
                        injectionBox = [(injectionPeakTime-onSourceTimeWidth-onSourceTimeOffset), ...
                            minimumFrequency, 2*onSourceTimeWidth, maximumFrequency-minimumFrequency];
                        % ---- Find all clusters whose bounding box overlaps that 
                        %      of the injection.
                        if isempty(cluster.boundingBox)
                            mask = [];
                        else
                            mask = find(rectint(injectionBox,cluster.boundingBox) > 0);
                            mask = mask(:);
                        end
                        % ---- Keep all clusters in coincidence.  Pack into a
                        %      struct array.
                        % if strcmp(outputType,'seedless')
                        %     clusterInj(injectionNumber,:) = xclustersubset(cluster,1:length(cluster.significance));
                        % else
                        %     clusterInj(injectionNumber,:) = xclustersubset(cluster,mask);
                        % end
                        % ---- KLUDGE: Keep all clusters (i.e., skip coincident step).  Current 
                        %      test is not sensible for injections of 10-100 sec duration!
                        clusterInj(injectionNumber,:) = xclustersubset(cluster,1:length(cluster.significance));
                        % ---- Mark this injection number as having been
                        %      processed. 
                        injectionProcessedMask(injectionNumber,:) = 1;
                    else
                        % ---- injectionNumber = 0 means we may have many injections 
                        %      in the data simultaneously.  Abort.
                        error(['Current version of xdetection does not support ' ...
                               ' simultaneous post-processing of multiple injections.']);
                    end  %-- injectionNumber
                end  %-- injectionScale

                % ---- Dump to stdout info on variables in workspace (useful
                %      for debugging). 
                if (1 == verboseFlag)
                    whos
                end

            else

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %              Save raw clusters to output file.
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                % ---- Save cell arrays with results for all analysisTimes to a
                %      .mat file. 
                disp('writing results...');
                % ---- Determine file name based on job number and injection
                %      number.
                if (injectionNumber)
                    resultsFile = [outputDirectory '/results_' num2str(jobNumber) '_' ...
                        num2str(injectionNumber) '.mat'];
                else
                    resultsFile = [outputDirectory '/results_' num2str(jobNumber) '.mat'];
                end
                % ---- Write raw cluster/map data to .mat file.
                save(resultsFile, ...
                    'amplitudeSpectraCell','analysisLengthCell','analysisTimesCell', ...
                    'blockTime','centerTime','channelNames','channelVirtualNames', ...
                    'gaussianityCell','injectionFileName','injectionNumber', ...
                    'injectionPhi','injectionScale','injectionTheta','internalAngleIndexCell', ...
                    'likelihoodMapCell','likelihoodType', ...
                    'maximumFrequency','minimumFrequency', ...
                    'offsetFraction','sampleFrequency','skyPositionIndexCell', ...
                    'skyPositions','sourceAngleIndexCell','sourceLikelihoodMapCell', ...
                    'sourcePositionIndexCell','sourcePositions','spectrogramCell', ...
                    'startTime','stopTime','transientLength','windowType');           
            end  %-- end check outputType


        end  %-- end loop over injection numbers


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %              Output post-processed results 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Save post-processed results.
        if (strcmp(outputType,'clusters') || ...
            strcmp(outputType,'glitch') || ...
            strcmp(outputType,'injectionclusters') || ...
            strcmp(outputType,'seedless') || ...
            strcmp(outputType,'sphrad') || ...
            strcmp(outputType,'xcc') || ...
            strcmp(outputType,'xccfast'))
            % ---- Create name of output file after post processing.
            tmp  = textscan(parameterFileName ,'%s','delimiter','/');  %-- strip off directory
            tmp = tmp{1};
            tmp1 = textscan(tmp{end},'%s','delimiter','.');            %-- strip off .txt
            tmp1 = tmp1{1};
            tmp2 = textscan(tmp1{1},'%s','delimiter','_');             %-- split by "_"
            tmp2 = tmp2{1};
            if length(injectionScales)==1
                % ---- On-source / off-source / single injection scale.
                extractedLikelihoodFilePrefix=[outputDirectory '/'];
                for i=2:length(tmp2)
                    extractedLikelihoodFilePrefix = [extractedLikelihoodFilePrefix tmp2{i} '_'];
                end
                if isempty(injectionFileName) 
                    % ---- On-source / off-source.
                    extractedLikelihoodFile = [extractedLikelihoodFilePrefix ...
                        num2str(jobNumber) '_' injectionNumberString '.mat'];
                else
                    % ---- KLUDGE: For on-the-fly injections set jobnumber = 0 in the
                    %      file name to fool xmergegrbfiles.  This is needed because 
                    %      the injection parameters list is not split into one file per
                    %      segment.
                    extractedLikelihoodFile = [extractedLikelihoodFilePrefix ...
                                    '0_' injectionNumberString '.mat'];
                end
            else 
                % ---- Have length(injectionScales)>1; i.e., multiple injection 
                %      scales.  Get the current injection scale number (robustly).
                [val,ind] = min(abs(injectionScales-injectionScale)); 
                extractedLikelihoodFilePrefix=[outputDirectory num2str(ind-1) '/'];
                % ---- Drop last two parts of parameter file name.  Second-last 
                %      part is meant to hold the injection scale but reads zero 
                %      from sn.py.  Correct this here.
                for i=2:(length(tmp2)-2)
                    extractedLikelihoodFilePrefix = [extractedLikelihoodFilePrefix tmp2{i} '_'];
                end
                extractedLikelihoodFilePrefix = [extractedLikelihoodFilePrefix num2str(ind-1) '_0_'];
                % ---- Repeat KLUDGE for on-the-fly injection jobnumbers.
                extractedLikelihoodFile = [extractedLikelihoodFilePrefix ...
                    '0_' injectionNumberString '.mat'];
            end
            % ---- Write results file.
            if (0 == injectionScale)
                % ---- Save processed data on noise-only clusters (``False Alarms''),
                %      as well as extra information on the noise spectrum.
                save(extractedLikelihoodFile, ...
                    'amplitudeSpectraCell','analysisTimesCell','blockTime', ...
                    'cluster','detectorList','gaussianityCell', ...
                    'injectionScale','likelihoodType','maximumFrequency', ...
                    'minimumFrequency', 'outputType', 'sampleFrequency','skyPositions', ...
                    'startTime', 'stopTime', 'svnversion_xdetection',...
                    'transientTime');
            else
                % ---- Check for pre-existing injection results file.
                if exist(extractedLikelihoodFile,'file')==2
                    warning('Injection results file already exists. Will attempt to merge triggers into it.');
                    % ---- Collect all the data we intend to save into a struct and pass this to the append function.
                    verificationParameters.analysisTimesCell      = analysisTimesCell;
                    verificationParameters.blockTime              = blockTime;
                    verificationParameters.clusterInj             = clusterInj;
                    verificationParameters.detectorList           = detectorList;
                    verificationParameters.injectionProcessedMask = injectionProcessedMask;
                    verificationParameters.injectionScale         = injectionScale;
                    verificationParameters.likelihoodType         = likelihoodType;
                    verificationParameters.maximumFrequency       = maximumFrequency;
                    verificationParameters.minimumFrequency       = minimumFrequency;
                    verificationParameters.outputType             = outputType;
                    verificationParameters.peakTime               = peakTime;
                    verificationParameters.sampleFrequency        = sampleFrequency;
                    verificationParameters.skyPositions           = skyPositions;
                    verificationParameters.startTime              = startTime;
                    verificationParameters.stopTime               = stopTime;
                    verificationParameters.svnversion_xdetection  = svnversion_xdetection;
                    verificationParameters.transientTime          = transientTime;
                    verificationParameters.onSourceTimeOffset     = onSourceTimeOffset;
                    % ---- Append injection data to existing file, requiring consistency checks. Force overwrite of existing file if needed.
                    disp(extractedLikelihoodFile);
                    xappendinjectionresults(extractedLikelihoodFile,clusterInj,verificationParameters,true);
                else 
                    % ---- Save processed data on efficiencies.
                    save(extractedLikelihoodFile, ...
                        'analysisTimesCell','blockTime','clusterInj', ...
                        'detectorList','injectionProcessedMask', ...
                        'injectionScale','likelihoodType', ...
                        'maximumFrequency','minimumFrequency','outputType','peakTime', ...
                        'sampleFrequency','skyPositions','startTime', ...
                        'stopTime','svnversion_xdetection','transientTime',...
                        'onSourceTimeOffset' );
                end
            end
        end  %-- if outputType
    end  %-- loop over injectionScales
end  %-- end loop over job numbers

%----- Log duration
fprintf(1, '%d seconds elapsed\n', round(etime(clock, pipelineStartTime)));

%----- Log ending time
fprintf(1, 'Pipeline ends\n%s\n\n', datestr(now, 31));

