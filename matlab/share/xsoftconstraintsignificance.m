function map = xsoftconstraintsignificance(likelihoodMap,epsilon,mode)
% xsoftconstraintsignificance - Convert a time-frequency map of the soft 
% constraint likelihood into a map of the significance or probability of
% the likelihood.
%
%   map = xsoftconstraintsignificance(likelihoodMap,epsilon,mode)
%
%   likelihoodMap   Array of soft constraint likelihood values, with each
%                   row containing one frequency bin and each column one
%                   time bin.
%   epsilon         Vector holding the ratio |F_x|^2/|F_+|^2 for each
%                   frequency bin in likelihoodMap.
%   mode            Optional string.  If 'significance', the output array
%                   holds the significance of the corresponding likelihood
%                   measurement.  If 'logprobability' (default), the output
%                   is the natural log of the corresponding probability
%                   density.
%
%   map             Array of the same size as likelihoodMap.  Each bin
%                   holds the significance or log probability of the
%                   corresponding likelihood measurement.
%
% The probability density P of drawing a soft constraint "likelihood" E in 
% a single time-frequency bin from unit normal Gaussian noise is
% 
%   P(E|epsilon) = 1/(2*(1-epsilon))*(exp(-E/2)-epsilon*exp(-E/(2*epsilon)))
%
% Note that P>=0.  
%
% The significance S is defined as the negative natural log of the
% cumulative probability CDF (CDF = \int dE P(E)) of drawing a likelihood
% greater than or equal to the specified value from unit normal Gaussian
% noise:
% 
%   S(E) := - log(1-CDF(E))
%         = - log( 1/(1-epsilon) * ( exp(-1/2*likelihoodMap) ...
%               - epsilon*exp(-1/2*likelihoodMap/epsilon) ...
%           ))
%
% Note that S>=0.  Larger significances indicate events less likely to have
% arisen from Gaussian noise.
%
% This function will return NaN for epsilon=1.
%
% $Id$

% ---- Checks and preliminaries.
error(nargchk(2,3,nargin));
if (nargin==2)
    mode = 'logprobability';
end

% ---- Expand epsilon vector into array of the same size as likelihoodMap.
epsilonArray = repmat(epsilon,[1 size(likelihoodMap,2)]);

switch mode
    case 'logprobability'
        % ---- Compute log probability.
        map = exp(-1/2*likelihoodMap) - exp(-1/2*likelihoodMap./epsilonArray);
        map = log( 1/2 * map ./ (1-epsilonArray) );
    case 'significance'
        % ---- Compute significance.
        map = exp(-1/2*likelihoodMap) ...
            - epsilonArray .* exp(-1/2*likelihoodMap./epsilonArray) ;
        map = log( 1-epsilonArray ) - log( map );
    otherwise
        error('Value of mode argument not recognized.');
end

% ---- Done
return
