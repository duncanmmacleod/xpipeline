function subSet = xclustersubset(clusterSet,index)
% xclustersubset - Extract a subset of clusters from a cluster struct.
%
% usage:
%
% subSet = xclustersubset(clusterSet,index)
%
%   clusterSet  1x1 Struct of clusters.  Each field must contain a numeric
%               array or cell array with the same number of rows.   
%   index       Vector of indices of clusters to keep.  May be empty ([]).
%
%   subSet      Struct of clusters.  Contains only the clusters from
%               clusterSet specified by index.  If index=[], then subset is
%               an empty struct with the same fields as clusterSet.

% ---- Checks.
narginchk(2,2);
if ~isstruct(clusterSet)
    error('Input argument clusterSet must be a 1x1 struct.')
end
if max(size(clusterSet))>1
    error('Input argument clusterSet must be a 1x1 struct.')
end
if isempty(index)
    % ---- Keep none of the clusters.  Return empty struct with same
    %      fieldnames.  Do this by looping over all fields in input
    %      clusterSet and emptying each.  
    clusterFields = fieldnames(clusterSet);
    for ii = 1:length(clusterFields)
        % ---- Overwrite with an empty matrix.
        clusterSet = setfield(clusterSet,clusterFields{ii},[]);
    end
    % ---- Copy results to output argument. 
    subSet = clusterSet;
    % ---- Done.
    return
else
    % ---- index is not empty.  Check that index values are valid.
    if ~isvector(index)
        error('Input argument index must be a vector with positive integer elements.')
    end
    if min(index<=0)
        error('Input argument index must be a vector with positive integer elements.')
    end
end

% ---- Loop over all fields in input clusterSet, retaining only desired
%      clusters. 
clusterFields = fieldnames(clusterSet);
nRows = zeros(length(clusterFields),1);
for ii = 1:length(clusterFields)
    % ---- Get the data for this field.
    fieldValue = getfield(clusterSet,clusterFields{ii});
    % ---- Check number of rows.  Must be the same for each field, and must
    %      at least as large as the largest index value requested.
    nRows(ii) = size(fieldValue,1);
    if (nRows(ii)~=nRows(1))
        error('Each field in clusterSet must have the same number of rows.')
    end
    if (max(index)>nRows(ii))
        error('Input clusterSet does not have enough rows as specified in index.')
    end
    % ---- Write only desired clusters back.
    clusterSet = setfield(clusterSet,clusterFields{ii},fieldValue(index,:));
end        

% ---- Copy results to output argument. 
subSet = clusterSet;

% ---- Done.
return
