function clusterComb = xsuperclustercombineoffsource(cluster,likelihoodType,dT,dF,verbose)
% XSUPERCLUSTERCOMBINEOFFSOURCE - apply xsuperclustercombine to off-source triggers
% 
% usage:
%
%   Y = xsuperclustercombineoffsource(X,L,dT,dF,V)
%
%  X      X-Pipeline cluster struct. Must have (at least) fields 'boundingBox' 
%         'significance', 'jobNumber', and 'pass'.
%  L      Cell array of strings. Names of the likelihoods in X.
%  dT     Optional scalar. Time coincidence window [sec] to be used in computing
%         cluster overlaps. Default 0.
%  dF     Optional scalar. Freq coincidence window [Hz] to be used in computing
%         cluster overlaps. Default 0.
%  V      Optional boolean. Additional verbosity if true. Default false.
%
%  Y      X-Pipeline cluster struct composed of clusters obtained by combining 
%         clusters from X that overlap as defined in XSUPERCLUSTERCOMBINE. The 
%         combination is done by XCOMBINECLUSTERS.
%
% Despite the name, XSUPERCLUSTERCOMBINEOFFSOURCE will also cluster on-source 
% and injection triggers. The function acts as a wrapper to XSUPERCLUSTERCOMBINE
% by segregating triggers from different background trials (i.e., with different
% jobNumber values) and triggers with pass=0/1 so that they are superclustered
% separately.
%
% $Id$

% ---- Checks on inputs.
narginchk(2,5);

% ---- Assign defaults if needed.
if nargin<5
    verbose = false;
end
if nargin<4
    dF = 0;
end
if nargin<3
    dT = 0;
end

% ---- Requirements: 
%        1. We don't want to cluster across jobNumbers (background trials;
%           e.g., different time lags) and 
%        2. We don't want to cluster between pass=0 and pass=1 triggers. 
%      The pass=0 are of no interest so we won't waste computing time on them. 
%      So, we will separate out the pass=0 triggers and divide the pass=1 
%      triggers by jobNumber for super-clustering.

% ---- Separate the pass=0 and pass=1 triggers.
Ncluster = length(cluster.significance);
passIdx = find(cluster.pass);
clusterPass = xclustersubset(cluster,passIdx);
clusterFail = xclustersubset(cluster,setdiff([1:Ncluster],passIdx));

% ---- Perform superclustering on pass=1 triggers, by jobNumber.
% ---- Storage for final list of sets of overlapping triggers. We don't know 
%      how many there will be but it can't be larger than the total number of
%      input triggers.
master = cell(length(clusterPass.significance),1);
imaster = 0; %-- temporary counter
% ---- Loop over unique jobNumber values, clustering each separately.
uniqJobNum = unique(clusterPass.jobNumber);
for ii=1:length(uniqJobNum)
    % ---- Find all triggers with this jobNumber.
    idx = find(clusterPass.jobNumber==uniqJobNum(ii));
    % ---- Run clustering, without actually combining overlapping triggers.
    [~,sets] = xsuperclustercombine(xclustersubset(clusterPass,idx),likelihoodType,dT,dF,false);
    % ---- Loop over each set of clustered triggers, translating index values
    %      from index within jobNumber to index within off.cluster array.
    for jj=1:length(sets)
        imaster = imaster+1;
        master{imaster} = idx(sets{jj});
    end
end
% ---- Truncate master list to remove unused elements at the end.
master = master(1:imaster);
% ---- Combine overlapping triggers.
[clusterPass,removedFields] = xclustercombine(clusterPass,likelihoodType,master,verbose); 

% ---- Merge superclustered pass=1 triggers with unclustered pass=0 triggers.
%      To do this we first remove any ignored fields from the pass=0 triggers
%      to allow them to be merged with the pass=1 triggers.
clusterFail = rmfield(clusterFail,removedFields);

% ---- Now perform the merger.
clusterComb = xclustermerge(clusterPass,clusterFail);

% ---- Done.
return


