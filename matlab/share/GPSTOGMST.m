function [gmst, gmst_hms] = GPSTOGMST(gps)
%
% Convert from GPS time to Greenwich Mean Sidereal Time.
%
%   [gmst, gmst_hms] = GPSTOGMST(gps)
%
% gps       Input GPS time.  May be a scalar or a vector.  If a vector, 
%           then the output variables will be arrays with one row per gps 
%           value.
% 
% gmst      Greenwich Mean Sidereal Time (s)
% gmst_hms  Greenwich Mean Sidereal Time (h:m:s)
%
% orignal write: Patrick J. Sutton 2004.07.25
%
% $Id$

% ---- Note: If gps is a vector then gmst_h, etc will have same dimensions.
%      Make sure we covert to column vectors before assembling gmst_hms
%      array.
if (min(size(gps))==0)
    gmst = [];
    gmst_hms = [];
    return;
elseif (min(size(gps))==1)
    if (size(gps,2)>1)
        gps = gps';
    end
else
    error('Input time must be a scalar or vector.');
end

% ---- GPS time of J2000 epoch (approx 2000 Jan 12, 12:00 UTC)
gps0 = 630763213;

% ---- (float) Days since J2000
D = (gps-gps0)/86400;

% ---- (int+1/2) Days between J2000 and last 0h at Greenwich (always 
%      integer + 1/2)
d_u = floor(D)+0.5;
d_u(logical((D-floor(D))<0.5)) = d_u(logical((D-floor(D))<0.5)) - 1;

% ---- (float) Fraction of day since last 0h at Greenwich 
df_u = D-d_u;

% ---- GMST (s) at Greenwich at last 0h:
T_u = d_u/36525;
gmst0h = 24110.54841 + 8640184.812866*T_u + 0.093104*T_u.^2 - 6.2e-6*T_u.^3;

% ---- Current GMST (s)
gmst = gmst0h + 1.00273790935*86400*df_u;

% ---- Remove any integer days
if (gmst>=86400)
    gmst = gmst - floor(gmst/86400)*86400;
end

% ---- In hours:min:sec
gmst_h = floor(gmst/3600);
gmst_m = floor((gmst-gmst_h*3600)/60);
gmst_s = (gmst-gmst_h*3600-gmst_m*60);
gmst_hms = [gmst_h gmst_m gmst_s];

% ---- Done
return
