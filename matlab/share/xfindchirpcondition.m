function [data, amplitudeSpectra, analysisLength, transientLength, ...
    powerSpectra, whiteningFilter] = xfindchirpcondition(data, ...
    sampleFrequencies, sampleFrequency, minimumFrequency, analysisTime, ...
    whiteningTime, verboseFlag)
% XFINDCHIRPCONDITION Resample, high pass filter, and whiten time series.
%
% XFINDCHIRPCONDITION resamples, high pass filters and whitens time series
% data with minimal phase shifting.  The data from all channels are first
% resampled to a common sample frequency.  The resampled data is then
% detrended by zero-phase high pass filtering with a cutoff at 0.75 times
% the specified minimum analysis frequency.  A robust median-spectrogram
% based algorithm is used to estimate the power spectrum.  The power
% spectrum is used to whiten the data in the frequency domain after first
% truncating a time-domain representation to limit the duration of filter
% transients. The whitened data is inverse Fourier transformed back to the
% time domain.  Finally, the whitened data is normalised.
%
% usage:
%
% [conditionedData, amplitudeSpectra, analysisLength, transientLength, ...
%    powerSpectra, whiteningFilter] = ...
%    xfindchirpcondition(data, sampleFrequencies, sampleFrequency, ...
%    minimumFrequency, analysisTime, whiteningTime, verboseFlag)
%
%   data                 Cell array of input time series data.
%   sampleFrequencies    Vector of sample frequencies of input data [Hz].
%   sampleFrequency      Scalar.  Sample frequency of output data [Hz].
%   minimumFrequency     Scalar.  Minimum frequency of subsequent analysis 
%                        [Hz].
%   analysisTime         Scalar.  FFT integration time of subsequent
%                        analysis [seconds]. 
%   whiteningTime        Scalar.  Length of whitening filter [seconds].
%   verboseFlag          Boolean flag for verbose status output.
%
%   conditionedData      Matrix of conditioned time-domain data.
%   amplitudeSpectra     Matrix of amplitude spectral densities
%                        [1/sqrt(Hz)]; see below.
%   analysisLength       Scalar.  Transform length of subsequent analysis 
%                        [samples].
%   transientLength      Scalar. Approximate duration of filter transients
%                        [samples].
%   powerSpectra         Matrix.  Power spectra for each channel (after
%                        resampling and high-pass filtering) estimated at a
%                        fixed frequency resolution of 1/8Hz.  This is used
%                        internally to construct the whitening filter.   
%   whiteningFilter      Matrix.  Frequency-domain filter used to whiten
%                        the data, at a resolution of 1/(duration of output
%                        timeseries). 
%
% The resulting conditioned data is returned as a matrix of time-domain
% data with each channel in a separate column.  XFINDCHIRPCONDITION also
% returns a matrix whose columns contain the one-sided amplitude spectral
% densities of the high pass filtered data at a frequency resolution
% corresponding to the requested analysis time.
%
% The conditioned data are also normalized such that a Fourier transform
% analysis with the specified analysis time results in frequency-domain
% coefficients with a mean squared amplitude of unity.  Equivalently, the
% conditioned data will have (ignoring loss of bandwidth due to the
% high-pass filter) a variance of
%
%   variance = 1 / (sampleFrequency * analysisTime)
%
% The amplitude spectra and power spectra estimated for the input data are
% one-sided, in Hz.  For example, input white noise of variance sigma2 has 
%
%   amplitudeSpectra = (2 * sigma2 / samplingFrequency)^0.5.
% 
% With these conventions, the conditioned data will have in-band one-sided
% power spectrum of  
%
%   power spectrum = 2 * variance / sampleFrequency
%                  = 2 / (sampleFrequency^2 * analysisTime)
%
% Finally, XFINDCHIRPCONDITION returns the required Fourier transform
% length for subsequent analysis (simply sampleFrequency*analysisTime) and
% the estimated length of filter transients which should be excluded from
% the analysis.  Note that, due to use of zero-phase filtering, transients
% are present at both the beginning and the end of the data stream.
%
% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   hardcoded parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- High pass filter order.
hpfOrder = 6;

% ---- Ratio of high pass filter frequency to minimum analysis frequency.
hpfCutoffFactor = 0.75;

% ---- FFT length used for inital PSD estimation.  This FFT length is long
%      enough to resolve lines, but short enough to allow many (>>10) time
%      bins for typical use, to make the median algorithm robust against
%      glitches.
%      MUST BE >= 2 sec to avoid clashing with HPF transient factor.
fftTime = 8;  


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   process and validate command line arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for sufficient command line arguments.
error(nargchk(6, 7, nargin));

% ---- Check optional arguments.
if (nargin < 7) 
    % ---- Disable debug output by default.
    verboseFlag = false;
end

% ---- Verify that data is a cell array,
if ~iscell(data)
    error('Input data must be a cell array.');
end

% ---- Force one dimensional (column vector) cell array
data = data(:);

% ---- Determine number of channels.
numberOfChannels = length(data);

% ---- Force row vector of sample frequencies.
sampleFrequencies = sampleFrequencies(:)';

% ---- Verify that number of sample frequencies matches number of channels.
if length(sampleFrequencies) ~= numberOfChannels,
  error('Number of sample frequencies and channels are inconsistent.');
end

% ---- Force data into column vectors and determine data lengths.
dataLengths = zeros(1, numberOfChannels);
for channelNumber = 1 : numberOfChannels,
  data{channelNumber} = data{channelNumber}(:);
  dataLengths(channelNumber) = length(data{channelNumber});
end

% ---- Determine data durations.
dataTimes = dataLengths ./ sampleFrequencies;

% ---- Verify that data durations are equal for all channels.
if ~all(dataTimes == dataTimes(1)),
  error('data durations are not equivalent');
end

% ---- Determine length of resampled data.
dataTime = dataTimes(1);
dataLength = dataTime * sampleFrequency;
if (dataTime/fftTime ~= round(dataTime/fftTime))
    error('dataTime must be a multple of fftTime.');
end

% ---- Verify that resampled data length is an integer power of 2.
if bitand(dataLength,dataLength-1),
  error('data length is not an integer power of two');
end

% ---- Determine analysis length (FFT length of subsequent analysis).
analysisLength = analysisTime * sampleFrequency;

% validate integer power of two analysis length
if bitand(analysisLength,analysisLength-1),
  error('analysis length is not an integer power of two');
end

% ---- Verify that whitening time scale is at least as long as FFT 
%      scale for subsequent analysis.
if (whiteningTime < analysisTime),
  error('whitening time scale must exceed analysis time scale');
end

% ---- Verify that whitening time scale is no longer than fftLength 
%      used for PSD estimation.
if (whiteningTime > fftTime),
  error(['whiteningTime must be no larger than ' num2str(fftTime) ' sec']);
end

% ---- Verify that internal fftLength variable is set to >= 2 (twice the
%      length of the window to suppress high-pass filter transients).
if (fftTime < 2),
  error('fftTime must be >=2 sec.');
end

% ---- Determine whitening length.
whiteningLength = whiteningTime * sampleFrequency;

% ---- High pass filter cutoff frequency
hpfCutoffFrequency = hpfCutoffFactor * minimumFrequency;

% ---- Verify that high pass filter cutoff frequency is physically sane.
if (hpfCutoffFrequency < 1 / whiteningTime),
  error('high pass filter cutoff frequency must exceed 1/whiteningTime');
end
if (hpfCutoffFrequency > sampleFrequency/2),
  error('high pass filter cutoff frequency exceeds Nyquist frequency');
end

% ---- Compute transient length.  This determines the indices of valid
%      output data (i.e., after cutting off filter transient regions).
%      This is 1/2 length of whitening filter (1/2 because it is symmetric)
%      plus the generous 1 sec allowance used for suppression of high-pass
%      filter transients.
transientLength = (whiteningTime/2+1)*sampleFrequency;
% ---- For historical reasons, we round this up to a power of 2 samples.
transientLength = 2.^ceil(log2(transientLength));

% ---- Verify that we have some data length after dropping
%      transient-polluted edges. 
if analysisLength > (dataLength - 2 * transientLength),
  error('analysis length exceeds usable data length');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   start timer
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% start timer
if verboseFlag,
  globalStartTime = clock;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   resample data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if verboseFlag,
  fprintf(1, 'resampling data ...');
  localStartTime = clock;
end

% ---- Resample data.  
%      WARNING: The default low-pass filtering used in matlab is weak, with
%      an amplitude attenuation of something like 0.5 at the new Nyquist
%      frequency. DO NOT USE DATA NEAR THE RESAMPLED NYQUIST FREQUENCY!
for channelNumber = 1 : numberOfChannels,
  upSampleFactor = sampleFrequency / ...
      gcd(sampleFrequencies(channelNumber), sampleFrequency);
  downSampleFactor = sampleFrequencies(channelNumber) / ...
      gcd(sampleFrequencies(channelNumber), sampleFrequency);
  data{channelNumber} = resample(data{channelNumber}, upSampleFactor, ...
      downSampleFactor);
end

% ---- All channels now have the same length, so we can repack them from a
%      cell array into a much more convenient numeric array (one column per
%      channel). 
data = [data{:}];

if verboseFlag,
  fprintf(1, ' done. (%.1f seconds)\n', etime(clock, localStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   high pass filter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Hard-coded high-pass filter.  It is applied forwards and backwards
%      to minimize phase distortion.  With these parameters, the magnitude
%      of the impulse response falls below 1e-3 in about 30 ms.  So, data
%      corruption at the edges of the data segment is minimal. 

if verboseFlag,
  fprintf(1, 'applying high pass filter ...');
  localStartTime = clock;
end

% ---- Design high pass filter.
[hpfZeros, hpfPoles, hpfGain] = butter(hpfOrder, hpfCutoffFrequency / ...
    (sampleFrequency/2), 'high');
hpfSOS = zp2sos(hpfZeros, hpfPoles, hpfGain);

% ---- Apply high pass filter.
data = sosfiltfilt(hpfSOS, data);

% ---- Suppress high pass filter transients, smoothly.  This window
%      zeroes out the first 0.5s, and suppresses the next 0.5s
%      (conservative).
hpfWindow = [zeros(sampleFrequency/2,1); hann(sampleFrequency); ...
    zeros(sampleFrequency/2,1)]; 
hpfWindow = repmat(hpfWindow,[1,numberOfChannels]);
data(1:sampleFrequency,:) = data(1:sampleFrequency,:) .* ...
    hpfWindow(1:sampleFrequency,:);
data(end-sampleFrequency+1:end,:) = data(end-sampleFrequency+1:end,:) .* ...
    hpfWindow(sampleFrequency+1:end,:);

% ---- Estimate frequency response of high-pass filter, so we can include
%      this effect in the PSD estimation.  Use frequency resolution to
%      match powerSpectra variable (below).
impulseData = zeros(fftTime*sampleFrequency,1);
impulseData(end/2) = 1;
impulseData = sosfiltfilt(hpfSOS, impulseData);
hpfResponse = abs(fft(impulseData));
% ---- Keep non-=negative frequencies only.
hpfResponse = hpfResponse(1:end/2+1);

if verboseFlag,
  fprintf(1, ' done. (%.1f seconds)\n', etime(clock,localStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   determine power spectra at default resolution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if verboseFlag,
  fprintf(1, 'computing amplitude spectra of high pass filtered data ...');
  localStartTime = clock;
end

% ---- Compute amplitude spectra of data, ignoring first and last
%      fftLength/2 data points which are corrupted by transients.
fftLength = fftTime * sampleFrequency;
windowVector = hann(fftLength);
for channelNumber = 1:numberOfChannels
    [powerSpectra(:,channelNumber), F] = medianmeanaveragespectrum( ...
        data(fftLength/2+1:end-fftLength/2,channelNumber), ...
        sampleFrequency,fftLength,windowVector);
    % ---- Correct PSD for high-pass filter response.
    powerSpectra(:,channelNumber) = powerSpectra(:,channelNumber) ./ ...
        hpfResponse.^2;
end

if verboseFlag,
  fprintf(1, ' done. (%.1f seconds)\n', etime(clock,localStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   prepare whitening filters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if verboseFlag,
  fprintf(1, 'preparing coarse-grained spectrum for whitening ...');
  localStartTime = clock;
end

% ---- Prepare storage for 2-sided PSD at highest frequency resolution.
whiteningFilter = zeros(dataLength,numberOfChannels);

% ---- Use FFT interpolation to get higher-resolution PSD.
for channelNumber = 1:numberOfChannels
    % ---- One-sided inverse amplitude spectrum.
    invAmplSpectrum = powerSpectra(:,channelNumber).^(-0.5);
    % ---- Note: High-pass filtering makes invAmplSpectrum very large at
    %      low frequencies.  This is at least partially compensated for by
    %      including hpfResponse in the estimation of powerSpectra.  For
    %      added safety, apply extra suppression of low frequencies (and do
    %      it smoothly!).
    weight = 0.5*(1+tanh(8*(F/hpfCutoffFrequency-1)));
    invAmplSpectrum = weight .* invAmplSpectrum;    
    % ---- Convert to two-sided.  Ignore 1/2 factor since it will cancel
    %      out below.
    invAmplSpectrum = [invAmplSpectrum; conj(invAmplSpectrum(end-1:-1:2))];
    % ---- Compute inverse FFT of inverse amplitude spectrum, dropping any
    %      small imaginary part due to numerical precision errors.  This
    %      quantity is called "q" in the FINDCHIRP paper.
    q = real(ifft(invAmplSpectrum));
    % ---- Now truncate time lags to the desired filter length, and pad
    %      with zeros out to full data length.  Mind the normalization of 
    %      the total power!
    sumq2before = sum(q.^2);
    N = whiteningLength;
    q = [q(1:N/2+1); zeros(dataLength-N,1); q(end-N/2+2:end)];
    sumq2after = sum(q.^2);
    q = (sumq2before/sumq2after)^0.5 * q;
    % ---- The FFT of q will now be approximately the amplitude spectrum at
    %      the highest frequency resolution (1/dataTime), but with
    %      structure only on scales larger than 1/whiteningTime. 
    whiteningFilter(:,channelNumber) = abs(real(fft(q)));
end

if verboseFlag,
  fprintf(1, ' done. (%.1f seconds)\n', etime(clock,localStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   whiten data in the frequency domain
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if verboseFlag,
  fprintf(1, 'whitening the data ...');
  localStartTime = clock;
end

% ---- Fourier transform all data as a single block.
data = fft(data);

% ---- Whiten using over-sampled low-resolution PSD.
data = data .* whiteningFilter;

% ---- Return to the time domain, discarding any small imaginary part from 
%      numerical error.
data = real(ifft(data));

if verboseFlag,
  fprintf(1, ' done. (%.1f seconds)\n', etime(clock, localStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   normalize conditioned data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Enforce desired normalization of conditioned data.
if verboseFlag,
  fprintf(1, 'normalizing conditioned data...');
  localStartTime = clock;
end

% ---- X-Pipeline expects the whitened data to be normalized such that the
%      expectation value of |data|^2 is 1 in each time-frequency bin, or
%      equivalently a variance of 1/N in the time domain.  We enforce the
%      normalization by FFTing the data, computing the avergae pixel power
%      in the analysis band (away from the edges), and rescaling.

% ---- Indices of valid data (i.e., after cutting off filter transient
%      regions).
validDataStart = transientLength + 1;
validDataEnd = dataLength - transientLength;
% ---- Select in-band frequencies.
frequencies = [0 : sampleFrequency/analysisLength : sampleFrequency/2];
inBandIndices = find((frequencies > 1.2 * hpfCutoffFrequency) & ...
                     (frequencies < 0.8 * sampleFrequency/2));
for channelNumber = 1:numberOfChannels
    Snorm = medianmeanaveragespectrum( ...
        data(validDataStart:validDataEnd,channelNumber), ...
        sampleFrequency,analysisLength);
    % ---- Normalization factor needed to make average value of power
    %      spectrum = 1/analysisLength over all in-band frequencies for
    %      each detector.  
    normalization = 2 / (analysisLength*sampleFrequency* ...
        mean(Snorm(inBandIndices)));    
    data(:,channelNumber) = data(:,channelNumber) * normalization^0.5;
end

% ---- Report status.
if verboseFlag,
  fprintf(1, 'done. (%.1f seconds)\n', etime(clock, localStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   estimate PSD a lower resolution of subsequent analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if verboseFlag,
  fprintf(1, 'computing amplitude spectra of high pass filtered data ...');
  localStartTime = clock;
end

% ---- Have powerSpectra at resolution corresponding to 1/fftLength.  Here 
%      average together neighboring bins until we're at the lower
%      resolution desired for subsequent analysis (corresponding to
%      1/analysisLength).  We do this by averaging the *inverse* power
%      spectrum instead of the power spectrum; this gives better accounting
%      of the effects of lines (features sharp compared to the frequency
%      resolution of final analysis). 

% ---- Number of consecutive bins to be averaged together to give PSD at
%      target frequency resolution.
numberBinAverage = fftLength / analysisLength;

% ---- We average together consecutive bins in groups of numberBinAverage,
%      except for the first and last sample, which contain only half the
%      number of bins. 
invPowerSpectra = zeros(analysisLength/2+1,numberOfChannels);

% ---- First bin.
invPowerSpectra(1,:) = mean(powerSpectra(1:numberBinAverage/2,:).^(-1),1);

% ---- Middle bins.  This part is a bit dense.  First determine indices of
%      bins which are not in the first or last sample.  
indexRange = numberBinAverage/2+1 : size(powerSpectra,1)-numberBinAverage/2-1;
% ---- Then take mean by reshaping the array so that all bins to be
%      averaged together are in a single column. 
tempArray  = reshape(powerSpectra(indexRange,:).^(-1),numberBinAverage,[]); 
tempArray  = mean(tempArray,1);
% ---- Finally reshape back to the correct format.  Note that we%  
%      automatically test that the dimensions are right by virtue of
%      assigning the reshaped data to specific elements of invPowerSpectra.
invPowerSpectra(2:end-1,:) = reshape(tempArray,[],numberOfChannels);

% ---- Last bin.
invPowerSpectra(end,:) = mean( ...
    powerSpectra(end-numberBinAverage/2:end,:).^(-1),1);

% ---- Convert from one-sided PSD to one-sided amplitude spectrum.
amplitudeSpectra = invPowerSpectra.^(-0.5); 

if verboseFlag,
  fprintf(1, ' done. (%.1f seconds)\n', etime(clock,localStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   report total elapsed time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if verboseFlag,
  fprintf(1, 'total elapsed time: %.1f\n', etime(clock, globalStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   return to calling function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Done.
return;

