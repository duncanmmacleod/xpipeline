; ---- Draft X-Pipeline parameter file for O3b GRB offline searches.
;      See https://trac.ligo.caltech.edu/xpipeline/wiki/Documentation/Searches/grb/grb.iniDoc
;      Note that some of the files pointed to here are actually for O3a and need to be updated.


[tags] 
; ---- Optional tag for this file/run. This is dumped to stdout by grb.py but otherwise is not used.
version = $Id$


[background]
; ---- Information on data to be used for background estimation.
; ---- Duration of background period in seconds. This length of time, by default centered on 
;      the trigger, is used for background event generation. 
backgroundPeriod = 3600
; ---- This is needed by the online search when using asymmetric background estimation.
;      If doAsymmetricBackground = 0 then the background period is symmetric about the trigger time.
;      Otherwise backgroundAsymmetryFactor is the fraction of the background period before the trigger time.
;      Specifically, the background period is [start_time,end_time] where
;        start_time = int(trigger_time - background_period*backgroundAsymmetryFactor)
;          end_time = int(trigger_time + background_period*(1.0 - backgroundAsymmetryFactor))
doAsymmetricBackground = 0
backgroundAsymmetryFactor = 0.9
; ---- Full path of files listing time lags to be used for background estimation.
;      Specify one lag file for each network type. (Note that lag files
;      are ignored for single-detector networks.) 
;      The format is one set of time lags per row, with each column being the time shift applied 
;      to the corresponding detector in the [input] detectorList below. With variable names of 
;      the form "lags_2det1site" etc., grb.py will automatically select the right file based on 
;      what detectors are being used. 
;      TIP: It's good practice to point to a file in a repository if running an analysis to be reviewed.
;      All coincidence segments for all listed time lags will be used to generate
;      background. The actual number of background trials you get isn't determined 
;      advance, as it depends on the duty cycle of each detector, and also on the value of 
;      circtimeslidestep in the [parameters] section.
; ---- Use the same lag files as in O3a:
lags_2det2site = lags_2det2site_2.txt
lags_3det3site = lags_3det3site_2.txt 


[parameters]
; ---- xdetection parameters. See 
;      https://trac.ligo.caltech.edu/xpipeline/wiki/Documentation/xdetection
;      Particularly important parameters that are not obvious are:
;        onSourceBeginOffset,onSourceEndOffset - these define the on source region to be analyzed as 
;          [grbTriggerTime+onSourceBeginOffset,grbTriggerTime+onSourceEndOffset], where grbTriggerTime 
;          is the GPS time supplied by the -g option of grb.py 
;        circtimeslidestep - this is the step size of circular time shifts applied within each 
;        background job. Smaller values give more circular time shifts and more background trials.
;        The value of circtimeslidestep must be a multiple of all of the analysisTimes values. For 
;        this example it must be a multiple of 2. 
analysisTimes = 0.5
blockTime = 64
onSourceBeginOffset = -28
onSourceEndOffset = 28
likelihoodType_2det2site = circenergy,circinc,circnullenergy,circnullinc
likelihoodType_3det3site = circenergy,circinc,circnullenergy,circnullinc,nullenergy,nullinc
minimumFrequency = 64
maximumFrequency = 500
offsetFraction = 0.5
outputType = clusters
sampleFrequency = 1024
verboseFlag = 1
whiteningTime = 1
seed = 1235
applyCalibCorrection = 0
makeSimulatedNoise = aLIGO
circtimeslidestep = 6
; genclusteringthresh = 2


[output]
; ---- These variables set how many injections or off-source blocks are processed by each condor job.
;      Injections are processed in groups of up to maxInjNum/(number_of_sky_positions) injections per condor job. 
;      Off-source jobs are also grouped together in batches of this number if maxOffNum variable is not set. 
;      Results from separate jobs are later merged using xmergegrbresults.m. 
;      This option is useful to control how long condor jobs take. 
;      A larger number means fewer but longer condor jobs.
;      Jobs that run for 1-2 hours are the most efficient for large-scale analyses.
maxInjNum = 50
maxOffNum = 1


[input]
; ---- This section specifies the information on the raw data frames to be analysed. 
; ---- Detectors in the O3a LIGO-Virgo network.
detectorList = H1,L1,V1
; ---- List of channels for the O3a analysis. 
;      The order of the channels must match the order in which the detectors were specified in 
;      detectorList. The list of channels found in a particular frame (.gwf) file can be found 
;      using FrDump -i <name of frame file>  
channelList = DCS-CALIB_STRAIN_CLEAN_C01,DCS-CALIB_STRAIN_CLEAN_C01,Hrec_hoft_16384Hz
; ---- List of frametypes for the O3a analysis. 
;      The type of frame holding the specified channel. The order of the frame types must match the 
;      order in which the detectors were specified in detectorList. The list of available frame types 
;      can be found using your datafind_exec exectuable (see [datafind]), e.g gw_data_find --show-types. 
frameTypeList = H1_HOFT_C01,L1_HOFT_C01,V1Online
; ---- Optionally specify a pre-generated frame cache file in the X-Pipeline format. For instructions 
;      see https://trac.ligo.caltech.edu/xpipeline/wiki/Documentation/Searches/FrameCacheFile 
;      If framecacheFile is not specified, X-Pipeline will generate its own. 
frameCacheFile = empty.txt


[skygrid]
; ---- Parameters used to construct the sky grid searched over. 
;        numSigmaSkyPos - Amount of the trigger error box to cover in a multiple sky-position search, 
;          as a multiple of the estimated 1-sigma uncertainty radius. This should be considered as a 
;          one-dimensional Gaussian; for example, numSigmaSkyPos=1 means that the sky grid will cover 
;          the 68% containment region of the GRB error box. 
;        delayTol - maximum allowed time delay error [sec] in the grid. Smaller values give a denser 
;          sky grid.
;      The settings below tile the 2-sigma error region with a time delay tolerance of 0.5ms. 
numSigmaSkyPos = 1.65
delayTol = 5e-4


[datafind]
; ---- X-Pipeline can generate its own frame cache file and find segment lists of good and bad data times. 
;      This requires a server to query and the names of the executables that return the frame and segment infomration.
; ---- Data find server used for calls to datafind_exec.
datafind_server      = ldr.ldas.cit
; ---- Executable for finding data frames.
datafind_exec        = gw_data_find
; ---- Executable for obtaining science segments ("good" data times). 
segfind_exec         = ligolw_segment_query_dqsegdb
; ---- Executable for obtaining category 2-5 veto segment files ("bad" data times). 
segs_from_cats_exec  = ligolw_segments_from_cats_dqsegdb
; ---- Executable to convert xml segment files into plain text format. 
ligolw_print_exec    = ligolw_print


[segfind]
; ---- Options to be used with segfind_exec (segment finding tool) and 
;      segs_from_cats_exec (veto segment finding tool)
; ---- Required flag. Generate new segment lists (1) or use premade files (0).
generateSegs = 0
; ---- If you are going to generate segment lists on the fly then you need to 
;      specify segment-url and veto-file.
; ---- Segment database URL.
segment-url = https://segments.ligo.org
; ---- Category definer used by ligolw_segments_from_cats in order to generate
;      category 2-4 veto segment files.
; ---- This is the C01 Burst veto definer for O3a.
veto-file = /home/ronaldas.macas/grb_runs/x/o3a_offline/input/burst_O3_H1L1V1-HOFT_C01_V1ONLINE_O3_BURST.xml
; ---- Optional flag. Only specify to use DMT files if running online, otherwise 
;      using segdb is significantly faster.
useDMTFile = 0
; ---- If 1, apply gates if listed for any detector. If 0 then do not apply gates.
applyGates = 1


; ---- Segment information.  There should be one section per detector in 
;      detectorList. Give one of the following:
;        i)  a comma separated-list of segment types (data quality flags)
;            for each detector; or
;        ii) a pre-made segment list file in segwizard format, with an optional 
;            veto-list file and an optional gating-list file. Gating is only applied 
;            if applyGates=1 in the [segfind] section.
[H1]
; ---- Segment list and veto list files. 
segment-list = segments_H1.txt

[L1]
; ---- Segment list and veto list files.
segment-list = segments_L1.txt

[V1]
; ---- Segment list and veto list files.
segment-list = segments_V1.txt


[condor]
; ---- Condor parameters. You should only need to update the 'Era' and 'SearchType' 
;      parameters for your search. They don't affect the analysis but are used by 
;      sysadmins for counting CPU time used by the various searches. 
universe = vanilla
ProdDevSim = dev
Era = o3
Group = burst
SearchType = grb.xoffline
; ---- Failed condor jobs are rerun this number of times (or until they succeed).
;      retryNumber = 5 is usually good enough for job failures that are "random" 
;      (eg temporary file system problems). Jobs which fail persistently indicate 
;      a probable bug in the parameters, missing data, or some other problem that 
;      you will have to fix yourself. 
retryNumber = 5
; ---- These are cluster (scratch disk) specific. /local/$ENV(USER)/ works on CIT.
;      Warning: Log file problems are a common cause of failed jobs. You must specify a directory 
;      which already exists and to which you have write permission. These log files need to be 
;      local for the head node from which you submit jobs, not NFS mounted. 
dagman_log_on_source   = /local/$ENV(USER)/grb_jobs_on_source.logs
dagman_log_off_source  = /local/$ENV(USER)/grb_jobs_off_source.logs
dagman_log_simulations = /local/$ENV(USER)/grb_jobs_simulations.logs


[injection]
; ---- This section specifies properties common to all injection waveform sets, 
;      such as amplitude scales and whether to account for calibration uncertainties.
; ---- Amplitude scales applied to all injections in the [waveforms] section.
injectionScales = 0.169,0.332,0.649,1.271,2.488,4.870,9.532,18.657
; ---- This parameter controls the number or spacing of injections.
;      Positive value: time interval between injections. The number of injections at each 
;        injection scale is approximately blockTime/injectionInterval.
;      Negative value: on-the-fly injections are spaced so that exactly 
;        abs(injectionInterval) injections are performed at each injection scale.
injectionInterval = -100
; ---- Apply estimated calibration uncertainties to injections. This should 
;      always be 1 for production analyses.
miscalibrateInjections = 0


[waveforms]
; ---- Sets of on-the-fly injections to be performed. Fixed amplitudes.
;      Format: set_name = waveform_type!waveform_parameters
;          or: set_name = waveform_type1!waveform_parameters1,waveform_type2!waveform_parameters2,...
;        set_name - an arbitrary string of letters and numbers only (no underscores, 
;          punctuation marks, etc.).  Make sure that each name is unique and not a sub-string of 
;          another name. This is because some book-keeping codes use "ls" to find files associated 
;          with a given injection set. So, for example, having sets dfm1, dfm2, dfm3 is fine, but 
;          sets dfm, dfm2, dfm3 will cause failures because a command like "ls dfm*" for the first 
;          injection set will also return files associated with dfm2, dfm3. And be careful if you 
;          have many sets: dfm1, ..., dfm10! 
;        waveform_type - can be any of the types recognized by xmakewaveform; type
;          "help xmakewaveform" in matlab.
;        waveform_parameters - To understand the parameters of a given type, e.g. 'chirplet', 
;          do "xmakewaveform('chirplet')" in matlab. A parameter of the form 'number;number;word' 
;          (e.g. -1;1;linear) will cause the value of the parameter to be generated randomly for 
;          each injection. For allowed syntax see the 'assignparameter' helper function of 
;          https://trac.ligo.caltech.edu/xpipeline/browser/xpipeline/trunk/matlab/share/xmakegwbinjectionfile.m
;      If there is more than one waveform in the comma-separated list, then for each injection one 
;      waveform is chosen randomly from the list.
;      Note that some waveforms are loaded from catalogs; in these cases you 
;      must use the -c option with grb.py to specify a location. All valid
;      catalogs are stored in branches/waveforms/ .
; sgc70Q9  = chirplet!1.0e-22~0.0143~70~0~0~-1;-0.996;linear
sgc100Q9 = chirplet!1.0e-22~0.01~100~0~0~0.996;1;linear
; sgc150Q9 = chirplet!1.0e-22~0.006667~150~0~0~-1;-0.996;linear
; sgc300Q9 = chirplet!1.0e-22~0.00333~300~0~0~0.996;1;linear
; bns = inspiral!1.4;0.2;1;3;1.4;0.2;1;3;2;6;mass~0.866;1;linear~10
; nsbh = inspiral!1.4;0.2;1;3;10;6;2;25;3;25;mass~-1;-0.866;linear~20
; adi-a  = adi-a!10~0.996;1;linear
; adi-b  = adi-b!20~-1;-0.996;linear
; adi-c  = adi-c!10~0.996;1;linear
; adi-d  = adi-d!10~-1;-0.996;linear
; adi-e  = adi-e!10~0.996;1;linear


