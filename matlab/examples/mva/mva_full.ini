; X-Pipeline parameter file for an example full-scale GRB analysis
; on simulated data using MVA.


[tags] 
; ---- Tags for this file/run.
version = $Id: grb_full.ini 4719 2015-01-14 14:29:08Z patrick.sutton@LIGO.ORG $
user-tag =


[background]
; ---- Information on data to be used for background estimation.
; ---- Duration of background period in seconds.
backgroundPeriod = 10800
; ---- Files listing time lags to be used for background estimation.
;      Specify one lag file for each network type.
lags_3det3site = lags_3det3site_60.txt
lags_2det2site = lags_2det2site_60.txt
; ---- Number of background jobs to perform. Omit this option if
;      you do not wish to set a (lower) limit on number of background
;      jobs.
;numJobs = 500


[parameters]
; ---- xdetection parameters.
analysisTimes = 1.0,0.5,0.25,0.125,0.0625,0.03125,0.015625,0.0078125
blockTime = 256 
likelihoodType_1det1site = loghbayesian,energyitf1,skypositiontheta,skypositionphi
likelihoodType_2det2site = loghbayesiancirc,standard,circenergy,circinc,circnullenergy,circnullinc,powerlaw,energyitf1,energyitf2,skypositiontheta,skypositionphi
likelihoodType_3det3site = loghbayesiancirc,standard,circenergy,circinc,circnullenergy,circnullinc,nullenergy,nullinc,powerlaw,energyitf1,energyitf2,energyitf3,skypositiontheta,skypositionphi
minimumFrequency = 64
maximumFrequency = 500
offsetFraction = 0.5
outputType = clusters
sampleFrequency = 1024
verboseFlag = 1
whiteningTime = 1
seed = 1235
applyCalibCorrection = 0
onSourceBeginOffset = -600
onSourceEndOffset = 60
makeSimulatedNoise = aLIGO
circtimeslidestep = 3

[output]
; ---- This variable sets how many injections are processed by each
;      simulation job, and how many off-source segments are analysed
;      by each off-source job.  A larger number means fewer but longer
;      condor jobs.
maxInjNum = 50
maxOffNum = 5
; ---- This variable specifies the maximum number of off-source segments 
;      each xdetection job should process when doing MDC injections in 
;      the off-source times.  Typically 5 is a good choice.
maxMDCSegNum = 5
; ---- Parameters controlling whether job output files are sprayed across
;      the cluster instead of returned to the local output/ directory.  
;      DO NOT TURN ON FOR S6/VSR2 GRB ANALYSIS - merging and post-processing 
;      codes will not be able to find them.  
distributeOnSource = 0
distributeOffSource = 0
distributeSimulation = 0
; ---- If distribute* = 0 then these variables are not read.
; nodePath = /data/node
; nNodes = 4
; numberOfFirstNode = 100
; jobNodeFileOnSource = distribonsource.txt
; onNodeOffSourcePath = gjones/fullsizetest/
; jobNodeFileOffSource = distriboffsource.txt
; onNodeSimulationPath = gjones/fullsizetest/
; jobNodeFileSimulationPrefix = distribsimulation


[input]
; ----- Note: Because we have specified [parameters] makeSimulatedNoise = LIGO,
;       simulated data will be substituted in place of real data for this 
;       analysis.  Therefore the channelList, frameTypeList, and frameCacheFile
;       entries below will be ignored.  They must still be specified.
; ----- LIGO-Virgo network for S6/VSR2.
detectorList = H1,L1,V1
; ---- List of channels for S6/VSR2.
channelList = LDAS-STRAIN,LDAS-STRAIN,h_4096Hz
; ---- List of frame types for S6/VSR2.
frameTypeList = H1_LDAS_C02_L2,L1_LDAS_C02_L2,HrecV3
; ---- Dummy frame cache file. 
frameCacheFile = empty.txt
; ---- Parameters used to construct the sky grid searched over.
numSigmaSkyPos = 1.65
delayTol = 5e-4
usexchooseskylocations = 1


; ---- Segment information.  There should be one section per detector.
;      Give one of the following:
;        i)  a comma separated-list of segment types (data quality flags)
;            for each detector; or
;        ii) a pre-made segment list file in segwizard format.
;      There may optionally be a veto-list file.

[H1]
; ---- Pre-made lists supplied by the user.
segment-list = segments_H1.txt
;veto-list    = empty.txt

[L1]
; ---- Pre-made lists supplied by the user.
segment-list = segments_L1.txt
;veto-list    = empty.txt

[V1]
; ---- Pre-made lists supplied by the user.
segment-list = segments_V1.txt
;veto-list    = empty.txt


[datafind]
; ---- Frame finding server.
datafind_server     = ldr.ligo.caltech.edu
;datafind_server     = ldr.atlas.local:80
; ---- Specify executables for frame and segment finding.
datafind_exec       = ligo_data_find
segfind_exec        = ligolw_segment_query
segs_from_cats_exec = ligolw_segments_from_cats
ligolw_print_exec   = ligolw_print


; ---- Options to be used with ligolw_segment_query (segment finding tool)
[segfind]
; ---- Only specify to use DMT files if running online, otherwise 
;      using segdb is significantly faster.
;dmt-file = 
; ---- Generate new segment lists (1) or use premade files (0).
generateSegs = 0


; ---- Options to be used with ligolw_segments_from_cats (veto segment 
;      finding tool)
[segs_from_cats]
; ---- Category definer used by ligolw_segments_from_cats in order to generate
;      category 2-5 veto segment files.
; ---- S6a
;veto-file = https://www.lsc-group.phys.uwm.edu/bursts/public/runs/s6/dqv/category_definer/H1L1V1-S6A_BURST_ALLSKY_OFFLINE-930960015-5011200.xml
; ---- S6b
;veto-file = https://www.lsc-group.phys.uwm.edu/bursts/public/runs/s6/dqv/category_definer/H1L1V1-S6B_BURST_ALLSKY_ONLINE-937526415-0.xml
; ---- WHY IS THIS SPECIFIED AGAIN?
;segment-url = https://segdb.ligo.caltech.edu


[condor]
; ---- Condor parameters.
universe = vanilla
dagman_log_on_source =   /usr1/psutton/mva_jobs.logs 
dagman_log_off_source =  /usr1/psutton/mva_jobs.logs
dagman_log_simulations = /usr1/psutton/mva_jobs.logs
dagman_log_mdcs =        /usr1/psutton/mva_jobs.logs
retryNumber = 5
; Production, Simulation or Development? Must be of the form prod,dev,sim.
ProdDevSim = dev
; Era of LIGO you are running for? Can be s6 o1 o2 o3.
Era = o1
; Group you are running for. This should be Burst so just leave this one.
Group = burst
; Specific tag to your X area. These can be (I think these are all of them:
; snews.x, grb.xoffline, grb.xonline, sn.xoffline, jrt.x
SearchType = grb.xoffline
; Change this to your albert.einstein username. Needed for shared accounts like
; X-pipeline
UserName = patrick.sutton


[injection]
; ---- Amplitude scales applied to all injections (MDC and on-the-fly).
;      The spacing of the scales should match the range over which the
;      injection amplitudes or distances are chosen in the [waveforms]
;      section. In this case we use 2^0.5 spacing.
injectionScales = 0.00781,0.0110,0.0156,0.0221,0.0312,0.0442,0.0625,0.0884,0.1250,0.1768,0.2500,0.3536,0.5000,0.7071,1.0,1.4142,2.0,2.8284,4.0,5.6569,8.0,11.3137,16.0,22.6274,32.0,45.2548,64.0,90.5097,128.0
; ---- Spacing of on-the-fly injections (ignored for MDCs).  A value N<0
;      means perform precisely |N| injections.
injectionInterval = -600
miscalibrateInjections = 0


[waveforms]
; ---- Sets of on-the-fly injections to be performed.
; ---- Elliptically polarised sine-Gaussians. log-mean amplitude 1e-22, duration
;      10-100 ms,  frequency 60-500 Hz, uniform peak phase, uniform inclination.
sge  = chirplet!8.4090e-23;1.1892e-22;log~0.01;0.1;log~60;500;log~0~0;6.283;linear~-1;1;linear
; ---- NS-NS inspirals. Component masses 1.35+/-0.1, total mass in [2,5],
;      cos(inclination) uniform up to 30 deg, log-mean distance of 100 Mpc.
nsns = inspiral!1.35;0.1;1;3;1.35;0.1;1;3;2;5;mass~0.866;1;linear~84.1;119;log
; ---- NS-BH inspirals. Component masses 1.35+/-0.1 + 5+/-1, total mass in [6,12],
;      cos(inclination) uniform over [150,180] deg, log-mean distance of 200 Mpc.
nsbh = inspiral!1.35;0.1;1;3;5;1;4;10;6;12;mass~-1;-0.866;linear~168;237;log
; ---- Five standard injection sets used in S6 searches: Q9 sine-Gaussians 
;      at 100, 150, 300 Hz, and two sets of inspirals with fixed masses.
;      We don't bother with the 5 deg inclination jittering for the sine-
;      Gaussians as the effect is completely negligible. We do include the 
;      30 deg jitter of the inspirals, either all left- or all right-circularly 
;      polarised.
sgc100 = chirplet!1.0e-22~0.01~100~0~0~1
sgc150 = chirplet!1.0e-22~0.00667~150~0~0~1
sgc300 = chirplet!1.0e-22~0.00333~300~0~0~1
insp1414 = inspiral!1.35~1.35~0.866;1;linear~100
insp1450 = inspiral!1.35~5~-1;-0.866;linear~200
; ---- One more standard injection set with fixed non-random parameters:
;      WNB centered at 100 Hz (white over [50,150]Hz).
wnb = wnb!1e-23~100~50~0.1

[mdc]
; ---- Sets of MDC injections to be performed. 

[xtmva]
; ---- The XTMVA analysis requires the following three variable to be specified.
;      WFtrain must be one of the waveform sets specified in [waveforms].
;      The tmvadir is the directory that olds the xtmva scripts. 
WFtrain    = sge
classifier = BDT
tmvadir    = /home/psutton/xpipeline/trunk/xtmvapy


