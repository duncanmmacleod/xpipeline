function gM = gRand(a, b, params)
% function gM = gRand(a, b, params)

if params.doGPU
  gM = gpuArray.rand(a,b,params.gpu.precision);
else 
  gM = rand(a,b);
end

return
