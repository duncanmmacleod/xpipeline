function params = initParallel(params)
% function initParallel(params)
% M Coughlin.Initialize parallel code if requested

try
  params.doParallel;
catch
  params.doParallel = false;
end

% set matlabpool variable for standard LDG resources
if params.doParallel
  if matlabpool('size') == 0
    if params.doGPU
      fprintf('setting matlabpool open local 4\n');
      matlabpool open local 4
    else
      fprintf('setting matlabpool open local 8\n');
      matlabpool open local 8
    end
  end
end

return
