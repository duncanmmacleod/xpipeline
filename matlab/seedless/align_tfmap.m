function outputmap = align_tfmap(timefreqmap, scales, freqs, fpeak)
   
%%% """
%%% Align the time-frequency map such that the wavelet scale which corresponds
%%% to the fpeak lies at 0.25x the maximum scale.

%%% In other words, 'squeeze' the TF map so that the fpeak is aligned to a
%%% common value (in this case a quarter of the maximum scale)
%%% """

outputmap = timefreqmap;
[junk,index] = min(abs(freqs-fpeak));
peak_scale = scales(index);

%%% shift columns

[xsize,ysize] = size(outputmap);
outputmap = shift_vec(outputmap, scales, peak_scale, 0.25*max(scales));

